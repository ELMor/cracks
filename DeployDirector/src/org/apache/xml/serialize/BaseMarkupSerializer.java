// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 

package org.apache.xml.serialize;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.Hashtable;
import java.util.Vector;
import org.apache.xerces.dom.DOMErrorImpl;
import org.apache.xerces.dom.DOMLocatorImpl;
import org.apache.xerces.dom.DOMMessageFormatter;
import org.apache.xerces.dom3.DOMError;
import org.apache.xerces.dom3.DOMErrorHandler;
import org.apache.xerces.util.XMLChar;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.ls.DOMWriterFilter;
import org.w3c.dom.traversal.NodeFilter;
import org.xml.sax.AttributeList;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.DTDHandler;
import org.xml.sax.DocumentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DeclHandler;
import org.xml.sax.ext.LexicalHandler;

// Referenced classes of package org.apache.xml.serialize:
//            ElementState, IndentPrinter, Printer, DOMSerializer, 
//            Serializer, OutputFormat, EncodingInfo

public abstract class BaseMarkupSerializer
    implements ContentHandler, DocumentHandler, LexicalHandler, DTDHandler, DeclHandler, DOMSerializer, Serializer
{

    protected Hashtable fFeatures;
    protected DOMErrorHandler fDOMErrorHandler;
    protected final DOMErrorImpl fDOMError = new DOMErrorImpl();
    protected DOMWriterFilter fDOMFilter;
    protected EncodingInfo _encodingInfo;
    private ElementState _elementStates[];
    private int _elementStateCount;
    private Vector _preRoot;
    protected boolean _started;
    private boolean _prepared;
    protected Hashtable _prefixes;
    protected String _docTypePublicId;
    protected String _docTypeSystemId;
    protected OutputFormat _format;
    protected Printer _printer;
    protected boolean _indenting;
    protected final StringBuffer fStrBuffer = new StringBuffer(40);
    private Writer _writer;
    private OutputStream _output;
    private Node fCurrentNode;

    protected BaseMarkupSerializer(OutputFormat outputformat)
    {
        fCurrentNode = null;
        _elementStates = new ElementState[10];
        for(int i = 0; i < _elementStates.length; i++)
            _elementStates[i] = new ElementState();

        _format = outputformat;
    }

    public DocumentHandler asDocumentHandler()
        throws IOException
    {
        prepare();
        return ((DocumentHandler) (this));
    }

    public ContentHandler asContentHandler()
        throws IOException
    {
        prepare();
        return ((ContentHandler) (this));
    }

    public DOMSerializer asDOMSerializer()
        throws IOException
    {
        prepare();
        return ((DOMSerializer) (this));
    }

    public void setOutputByteStream(OutputStream outputstream)
    {
        if(outputstream == null)
        {
            String s = DOMMessageFormatter.formatMessage("http://apache.org/xml/serializer", "ArgumentIsNull", new Object[] {
                "output"
            });
            throw new NullPointerException(s);
        } else
        {
            _output = outputstream;
            _writer = null;
            reset();
            return;
        }
    }

    public void setOutputCharStream(Writer writer)
    {
        if(writer == null)
        {
            String s = DOMMessageFormatter.formatMessage("http://apache.org/xml/serializer", "ArgumentIsNull", new Object[] {
                "writer"
            });
            throw new NullPointerException(s);
        } else
        {
            _writer = writer;
            _output = null;
            reset();
            return;
        }
    }

    public void setOutputFormat(OutputFormat outputformat)
    {
        if(outputformat == null)
        {
            String s = DOMMessageFormatter.formatMessage("http://apache.org/xml/serializer", "ArgumentIsNull", new Object[] {
                "format"
            });
            throw new NullPointerException(s);
        } else
        {
            _format = outputformat;
            reset();
            return;
        }
    }

    public boolean reset()
    {
        if(_elementStateCount > 1)
        {
            String s = DOMMessageFormatter.formatMessage("http://apache.org/xml/serializer", "ResetInMiddle", ((Object []) (null)));
            throw new IllegalStateException(s);
        } else
        {
            _prepared = false;
            fCurrentNode = null;
            fStrBuffer.setLength(0);
            return true;
        }
    }

    protected void prepare()
        throws IOException
    {
        if(_prepared)
            return;
        if(_writer == null && _output == null)
        {
            String s = DOMMessageFormatter.formatMessage("http://apache.org/xml/serializer", "NoWriterSupplied", ((Object []) (null)));
            throw new IOException(s);
        }
        _encodingInfo = _format.getEncodingInfo();
        if(_output != null)
            _writer = _encodingInfo.getWriter(_output);
        if(_format.getIndenting())
        {
            _indenting = true;
            _printer = ((Printer) (new IndentPrinter(_writer, _format)));
        } else
        {
            _indenting = false;
            _printer = new Printer(_writer, _format);
        }
        _elementStateCount = 0;
        ElementState elementstate = _elementStates[0];
        elementstate.namespaceURI = null;
        elementstate.localName = null;
        elementstate.rawName = null;
        elementstate.preserveSpace = _format.getPreserveSpace();
        elementstate.empty = true;
        elementstate.afterElement = false;
        elementstate.afterComment = false;
        elementstate.doCData = elementstate.inCData = false;
        elementstate.prefixes = null;
        _docTypePublicId = _format.getDoctypePublic();
        _docTypeSystemId = _format.getDoctypeSystem();
        _started = false;
        _prepared = true;
    }

    public void serialize(Element element)
        throws IOException
    {
        reset();
        prepare();
        serializeNode(((Node) (element)));
        _printer.flush();
        if(_printer.getException() != null)
            throw _printer.getException();
        else
            return;
    }

    public void serialize(DocumentFragment documentfragment)
        throws IOException
    {
        reset();
        prepare();
        serializeNode(((Node) (documentfragment)));
        _printer.flush();
        if(_printer.getException() != null)
            throw _printer.getException();
        else
            return;
    }

    public void serialize(Document document)
        throws IOException
    {
        reset();
        prepare();
        serializeNode(((Node) (document)));
        serializePreRoot();
        _printer.flush();
        if(_printer.getException() != null)
            throw _printer.getException();
        else
            return;
    }

    public void startDocument()
        throws SAXException
    {
        try
        {
            prepare();
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Throwable) (ioexception)).toString());
        }
    }

    public void characters(char ac[], int i, int j)
        throws SAXException
    {
        try
        {
            ElementState elementstate = content();
            if(elementstate.inCData || elementstate.doCData)
            {
                if(!elementstate.inCData)
                {
                    _printer.printText("<![CDATA[");
                    elementstate.inCData = true;
                }
                int k = _printer.getNextIndent();
                _printer.setNextIndent(0);
                for(int i1 = 0; i1 < j; i1++)
                {
                    char c = ac[i1];
                    if(c == ']' && i1 + 2 < j && ac[i1 + 1] == ']' && ac[i1 + 2] == '>')
                    {
                        _printer.printText("]]]]><![CDATA[>");
                        i1 += 2;
                    } else
                    if(!XMLChar.isValid(((int) (c))))
                    {
                        if(++i1 < j)
                            surrogates(((int) (c)), ((int) (ac[i1])));
                        else
                            fatalError("The character '" + c + "' is an invalid XML character");
                    } else
                    if(c >= ' ' && _encodingInfo.isPrintable(((int) (c))) && c != '\367' || c == '\n' || c == '\r' || c == '\t')
                    {
                        _printer.printText(c);
                    } else
                    {
                        _printer.printText("]]>&#x");
                        _printer.printText(Integer.toHexString(((int) (c))));
                        _printer.printText(";<![CDATA[");
                    }
                }

                _printer.setNextIndent(k);
            } else
            if(elementstate.preserveSpace)
            {
                int l = _printer.getNextIndent();
                _printer.setNextIndent(0);
                printText(ac, i, j, true, elementstate.unescaped);
                _printer.setNextIndent(l);
            } else
            {
                printText(ac, i, j, false, elementstate.unescaped);
            }
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    public void ignorableWhitespace(char ac[], int i, int j)
        throws SAXException
    {
        try
        {
            content();
            if(_indenting)
            {
                _printer.setThisIndent(0);
                for(int k = i; j-- > 0; k++)
                    _printer.printText(ac[k]);

            }
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    public final void processingInstruction(String s, String s1)
        throws SAXException
    {
        try
        {
            processingInstructionIO(s, s1);
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    public void processingInstructionIO(String s, String s1)
        throws IOException
    {
        ElementState elementstate = content();
        int i = s.indexOf("?>");
        if(i >= 0)
            fStrBuffer.append("<?").append(s.substring(0, i));
        else
            fStrBuffer.append("<?").append(s);
        if(s1 != null)
        {
            fStrBuffer.append(' ');
            int j = s1.indexOf("?>");
            if(j >= 0)
                fStrBuffer.append(s1.substring(0, j));
            else
                fStrBuffer.append(s1);
        }
        fStrBuffer.append("?>");
        if(isDocumentState())
        {
            if(_preRoot == null)
                _preRoot = new Vector();
            _preRoot.addElement(((Object) (fStrBuffer.toString())));
        } else
        {
            _printer.indent();
            printText(fStrBuffer.toString(), true, true);
            _printer.unindent();
            if(_indenting)
                elementstate.afterElement = true;
        }
        fStrBuffer.setLength(0);
    }

    public void comment(char ac[], int i, int j)
        throws SAXException
    {
        try
        {
            comment(new String(ac, i, j));
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    public void comment(String s)
        throws IOException
    {
        if(_format.getOmitComments())
            return;
        ElementState elementstate = content();
        int i = s.indexOf("-->");
        if(i >= 0)
            fStrBuffer.append("<!--").append(s.substring(0, i)).append("-->");
        else
            fStrBuffer.append("<!--").append(s).append("-->");
        if(isDocumentState())
        {
            if(_preRoot == null)
                _preRoot = new Vector();
            _preRoot.addElement(((Object) (fStrBuffer.toString())));
        } else
        {
            if(_indenting && !elementstate.preserveSpace)
                _printer.breakLine();
            _printer.indent();
            printText(fStrBuffer.toString(), true, true);
            _printer.unindent();
            if(_indenting)
                elementstate.afterElement = true;
        }
        fStrBuffer.setLength(0);
        elementstate.afterComment = true;
        elementstate.afterElement = false;
    }

    public void startCDATA()
    {
        ElementState elementstate = getElementState();
        elementstate.doCData = true;
    }

    public void endCDATA()
    {
        ElementState elementstate = getElementState();
        elementstate.doCData = false;
    }

    public void startNonEscaping()
    {
        ElementState elementstate = getElementState();
        elementstate.unescaped = true;
    }

    public void endNonEscaping()
    {
        ElementState elementstate = getElementState();
        elementstate.unescaped = false;
    }

    public void startPreserving()
    {
        ElementState elementstate = getElementState();
        elementstate.preserveSpace = true;
    }

    public void endPreserving()
    {
        ElementState elementstate = getElementState();
        elementstate.preserveSpace = false;
    }

    public void endDocument()
        throws SAXException
    {
        try
        {
            serializePreRoot();
            _printer.flush();
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    public void startEntity(String s)
    {
    }

    public void endEntity(String s)
    {
    }

    public void setDocumentLocator(Locator locator)
    {
    }

    public void skippedEntity(String s)
        throws SAXException
    {
        try
        {
            endCDATA();
            content();
            _printer.printText('&');
            _printer.printText(s);
            _printer.printText(';');
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    public void startPrefixMapping(String s, String s1)
        throws SAXException
    {
        if(_prefixes == null)
            _prefixes = new Hashtable();
        _prefixes.put(((Object) (s1)), ((Object) (s != null ? ((Object) (s)) : "")));
    }

    public void endPrefixMapping(String s)
        throws SAXException
    {
    }

    public final void startDTD(String s, String s1, String s2)
        throws SAXException
    {
        try
        {
            _printer.enterDTD();
            _docTypePublicId = s1;
            _docTypeSystemId = s2;
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    public void endDTD()
    {
    }

    public void elementDecl(String s, String s1)
        throws SAXException
    {
        try
        {
            _printer.enterDTD();
            _printer.printText("<!ELEMENT ");
            _printer.printText(s);
            _printer.printText(' ');
            _printer.printText(s1);
            _printer.printText('>');
            if(_indenting)
                _printer.breakLine();
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    public void attributeDecl(String s, String s1, String s2, String s3, String s4)
        throws SAXException
    {
        try
        {
            _printer.enterDTD();
            _printer.printText("<!ATTLIST ");
            _printer.printText(s);
            _printer.printText(' ');
            _printer.printText(s1);
            _printer.printText(' ');
            _printer.printText(s2);
            if(s3 != null)
            {
                _printer.printText(' ');
                _printer.printText(s3);
            }
            if(s4 != null)
            {
                _printer.printText(" \"");
                printEscaped(s4);
                _printer.printText('"');
            }
            _printer.printText('>');
            if(_indenting)
                _printer.breakLine();
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    public void internalEntityDecl(String s, String s1)
        throws SAXException
    {
        try
        {
            _printer.enterDTD();
            _printer.printText("<!ENTITY ");
            _printer.printText(s);
            _printer.printText(" \"");
            printEscaped(s1);
            _printer.printText("\">");
            if(_indenting)
                _printer.breakLine();
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    public void externalEntityDecl(String s, String s1, String s2)
        throws SAXException
    {
        try
        {
            _printer.enterDTD();
            unparsedEntityDecl(s, s1, s2, ((String) (null)));
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    public void unparsedEntityDecl(String s, String s1, String s2, String s3)
        throws SAXException
    {
        try
        {
            _printer.enterDTD();
            if(s1 == null)
            {
                _printer.printText("<!ENTITY ");
                _printer.printText(s);
                _printer.printText(" SYSTEM ");
                printDoctypeURL(s2);
            } else
            {
                _printer.printText("<!ENTITY ");
                _printer.printText(s);
                _printer.printText(" PUBLIC ");
                printDoctypeURL(s1);
                _printer.printText(' ');
                printDoctypeURL(s2);
            }
            if(s3 != null)
            {
                _printer.printText(" NDATA ");
                _printer.printText(s3);
            }
            _printer.printText('>');
            if(_indenting)
                _printer.breakLine();
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    public void notationDecl(String s, String s1, String s2)
        throws SAXException
    {
        try
        {
            _printer.enterDTD();
            if(s1 != null)
            {
                _printer.printText("<!NOTATION ");
                _printer.printText(s);
                _printer.printText(" PUBLIC ");
                printDoctypeURL(s1);
                if(s2 != null)
                {
                    _printer.printText(' ');
                    printDoctypeURL(s2);
                }
            } else
            {
                _printer.printText("<!NOTATION ");
                _printer.printText(s);
                _printer.printText(" SYSTEM ");
                printDoctypeURL(s2);
            }
            _printer.printText('>');
            if(_indenting)
                _printer.breakLine();
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    protected void serializeNode(Node node)
        throws IOException
    {
        fCurrentNode = node;
        switch(node.getNodeType())
        {
        case 2: // '\002'
        case 6: // '\006'
        case 10: // '\n'
        default:
            break;

        case 3: // '\003'
            String s = node.getNodeValue();
            if(s != null)
                if(fDOMFilter != null && (fDOMFilter.getWhatToShow() & 4) != 0)
                {
                    short word2 = ((NodeFilter) (fDOMFilter)).acceptNode(node);
                    switch(word2)
                    {
                    default:
                        characters(s);
                        break;

                    case 2: // '\002'
                    case 3: // '\003'
                        break;
                    }
                } else
                if(!_indenting || getElementState().preserveSpace || s.replace('\n', ' ').trim().length() != 0)
                    characters(s);
            break;

        case 4: // '\004'
            String s1 = node.getNodeValue();
            if(s1 == null)
                break;
            if(fDOMFilter != null && (fDOMFilter.getWhatToShow() & 8) != 0)
            {
                short word3 = ((NodeFilter) (fDOMFilter)).acceptNode(node);
                switch(word3)
                {
                case 2: // '\002'
                case 3: // '\003'
                    return;
                }
            }
            startCDATA();
            characters(s1);
            endCDATA();
            break;

        case 8: // '\b'
            if(_format.getOmitComments())
                break;
            String s2 = node.getNodeValue();
            if(s2 == null)
                break;
            if(fDOMFilter != null && (fDOMFilter.getWhatToShow() & 0x80) != 0)
            {
                short word4 = ((NodeFilter) (fDOMFilter)).acceptNode(node);
                switch(word4)
                {
                case 2: // '\002'
                case 3: // '\003'
                    return;
                }
            }
            comment(s2);
            break;

        case 5: // '\005'
            endCDATA();
            content();
            if(fDOMFilter != null && (fDOMFilter.getWhatToShow() & 0x10) != 0)
            {
                short word5 = ((NodeFilter) (fDOMFilter)).acceptNode(node);
                switch(word5)
                {
                case 2: // '\002'
                    return;

                case 3: // '\003'
                    for(Node node1 = node.getFirstChild(); node1 != null; node1 = node1.getNextSibling())
                        serializeNode(node1);

                    return;
                }
                Node node2 = node.getFirstChild();
                if(node2 != null)
                {
                    _printer.printText("&");
                    _printer.printText(node.getNodeName());
                    _printer.printText(";");
                }
                return;
            }
            Node node3 = node.getFirstChild();
            if(node3 == null || fFeatures != null && getFeature("entities"))
            {
                _printer.printText("&");
                _printer.printText(node.getNodeName());
                _printer.printText(";");
                break;
            }
            for(; node3 != null; node3 = node3.getNextSibling())
                serializeNode(node3);

            break;

        case 7: // '\007'
            if(fDOMFilter != null && (fDOMFilter.getWhatToShow() & 0x40) != 0)
            {
                short word0 = ((NodeFilter) (fDOMFilter)).acceptNode(node);
                switch(word0)
                {
                case 2: // '\002'
                case 3: // '\003'
                    return;
                }
            }
            processingInstructionIO(node.getNodeName(), node.getNodeValue());
            break;

        case 1: // '\001'
            if(fDOMFilter != null && (fDOMFilter.getWhatToShow() & 0x40) != 0)
            {
                short word1 = ((NodeFilter) (fDOMFilter)).acceptNode(node);
                switch(word1)
                {
                case 2: // '\002'
                    return;

                case 3: // '\003'
                    for(Node node5 = node.getFirstChild(); node5 != null; node5 = node5.getNextSibling())
                        serializeNode(node5);

                    return;
                }
            }
            serializeElement((Element)node);
            break;

        case 9: // '\t'
            DocumentType documenttype = ((Document)node).getDoctype();
            if(documenttype != null)
            {
                org.w3c.dom.DOMImplementation domimplementation = ((Document)node).getImplementation();
                try
                {
                    _printer.enterDTD();
                    _docTypePublicId = documenttype.getPublicId();
                    _docTypeSystemId = documenttype.getSystemId();
                    String s3 = documenttype.getInternalSubset();
                    if(s3 != null && s3.length() > 0)
                        _printer.printText(s3);
                    endDTD();
                }
                catch(NoSuchMethodError nosuchmethoderror)
                {
                    Class class1 = ((Object) (documenttype)).getClass();
                    String s4 = null;
                    String s5 = null;
                    try
                    {
                        Method method = class1.getMethod("getPublicId", ((Class []) (null)));
                        if(((Object) (method.getReturnType())).equals(((Object) (((Object) (java.lang.String.class))))))
                            s4 = (String)method.invoke(((Object) (documenttype)), ((Object []) (null)));
                    }
                    catch(Exception exception) { }
                    try
                    {
                        Method method1 = class1.getMethod("getSystemId", ((Class []) (null)));
                        if(((Object) (method1.getReturnType())).equals(((Object) (((Object) (java.lang.String.class))))))
                            s5 = (String)method1.invoke(((Object) (documenttype)), ((Object []) (null)));
                    }
                    catch(Exception exception1) { }
                    _printer.enterDTD();
                    _docTypePublicId = s4;
                    _docTypeSystemId = s5;
                    endDTD();
                }
            }
            // fall through

        case 11: // '\013'
            for(Node node4 = node.getFirstChild(); node4 != null; node4 = node4.getNextSibling())
                serializeNode(node4);

            break;
        }
    }

    protected ElementState content()
        throws IOException
    {
        ElementState elementstate = getElementState();
        if(!isDocumentState())
        {
            if(elementstate.inCData && !elementstate.doCData)
            {
                _printer.printText("]]>");
                elementstate.inCData = false;
            }
            if(elementstate.empty)
            {
                _printer.printText('>');
                elementstate.empty = false;
            }
            elementstate.afterElement = false;
            elementstate.afterComment = false;
        }
        return elementstate;
    }

    protected void characters(String s)
        throws IOException
    {
        ElementState elementstate = content();
        if(elementstate.inCData || elementstate.doCData)
        {
            if(!elementstate.inCData)
            {
                _printer.printText("<![CDATA[");
                elementstate.inCData = true;
            }
            int j = _printer.getNextIndent();
            _printer.setNextIndent(0);
            printCDATAText(s);
            _printer.setNextIndent(j);
        } else
        if(elementstate.preserveSpace)
        {
            int i = _printer.getNextIndent();
            _printer.setNextIndent(0);
            printText(s, true, elementstate.unescaped);
            _printer.setNextIndent(i);
        } else
        {
            printText(s, false, elementstate.unescaped);
        }
    }

    protected abstract String getEntityRef(int i);

    protected abstract void serializeElement(Element element)
        throws IOException;

    protected void serializePreRoot()
        throws IOException
    {
        if(_preRoot != null)
        {
            for(int i = 0; i < _preRoot.size(); i++)
            {
                printText((String)_preRoot.elementAt(i), true, true);
                if(_indenting)
                    _printer.breakLine();
            }

            _preRoot.removeAllElements();
        }
    }

    protected final void printCDATAText(String s)
        throws IOException
    {
        int i = s.length();
        for(int j = 0; j < i; j++)
        {
            char c = s.charAt(j);
            if(c == ']' && j + 2 < i && s.charAt(j + 1) == ']' && s.charAt(j + 2) == '>')
            {
                if(fFeatures != null && fDOMErrorHandler != null)
                    if(!getFeature("split-cdata-sections"))
                    {
                        String s1 = DOMMessageFormatter.formatMessage("http://apache.org/xml/serializer", "EndingCDATA", ((Object []) (null)));
                        modifyDOMError(s1, (short)2, fCurrentNode);
                        boolean flag = fDOMErrorHandler.handleError(((DOMError) (fDOMError)));
                        if(!flag)
                            throw new IOException();
                    } else
                    {
                        String s2 = DOMMessageFormatter.formatMessage("http://apache.org/xml/serializer", "SplittingCDATA", ((Object []) (null)));
                        modifyDOMError(s2, (short)0, fCurrentNode);
                        fDOMErrorHandler.handleError(((DOMError) (fDOMError)));
                    }
                _printer.printText("]]]]><![CDATA[>");
                j += 2;
            } else
            if(!XMLChar.isValid(((int) (c))))
            {
                if(++j < i)
                    surrogates(((int) (c)), ((int) (s.charAt(j))));
                else
                    fatalError("The character '" + c + "' is an invalid XML character");
            } else
            if(c >= ' ' && _encodingInfo.isPrintable(((int) (c))) && c != '\367' || c == '\n' || c == '\r' || c == '\t')
            {
                _printer.printText(c);
            } else
            {
                _printer.printText("]]>&#x");
                _printer.printText(Integer.toHexString(((int) (c))));
                _printer.printText(";<![CDATA[");
            }
        }

    }

    protected final void surrogates(int i, int j)
        throws IOException
    {
        if(XMLChar.isHighSurrogate(i))
        {
            if(!XMLChar.isLowSurrogate(j))
            {
                fatalError("The character '" + (char)j + "' is an invalid XML character");
            } else
            {
                int k = XMLChar.supplemental((char)i, (char)j);
                if(!XMLChar.isValid(k))
                    fatalError("The character '" + (char)k + "' is an invalid XML character");
                else
                if(content().inCData)
                {
                    _printer.printText("]]>&#x");
                    _printer.printText(Integer.toHexString(k));
                    _printer.printText(";<![CDATA[");
                } else
                {
                    _printer.printText("&#x");
                    _printer.printText(Integer.toHexString(k));
                    _printer.printText(";");
                }
            }
        } else
        {
            fatalError("The character '" + (char)i + "' is an invalid XML character");
        }
    }

    protected void printText(char ac[], int i, int j, boolean flag, boolean flag1)
        throws IOException
    {
        if(flag)
            while(j-- > 0) 
            {
                char c = ac[i];
                i++;
                if(c == '\n' || c == '\r' || flag1)
                    _printer.printText(c);
                else
                    printEscaped(((int) (c)));
            }
        else
            while(j-- > 0) 
            {
                char c1 = ac[i];
                i++;
                if(c1 == ' ' || c1 == '\f' || c1 == '\t' || c1 == '\n' || c1 == '\r')
                    _printer.printSpace();
                else
                if(flag1)
                    _printer.printText(c1);
                else
                    printEscaped(((int) (c1)));
            }
    }

    protected void printText(String s, boolean flag, boolean flag1)
        throws IOException
    {
        if(flag)
        {
            for(int i = 0; i < s.length(); i++)
            {
                char c = s.charAt(i);
                if(c == '\n' || c == '\r' || flag1)
                    _printer.printText(c);
                else
                    printEscaped(((int) (c)));
            }

        } else
        {
            for(int j = 0; j < s.length(); j++)
            {
                char c1 = s.charAt(j);
                if(c1 == ' ' || c1 == '\f' || c1 == '\t' || c1 == '\n' || c1 == '\r')
                    _printer.printSpace();
                else
                if(flag1)
                    _printer.printText(c1);
                else
                    printEscaped(((int) (c1)));
            }

        }
    }

    protected void printDoctypeURL(String s)
        throws IOException
    {
        _printer.printText('"');
        for(int i = 0; i < s.length(); i++)
            if(s.charAt(i) == '"' || s.charAt(i) < ' ' || s.charAt(i) > '\177')
            {
                _printer.printText('%');
                _printer.printText(Integer.toHexString(((int) (s.charAt(i)))));
            } else
            {
                _printer.printText(s.charAt(i));
            }

        _printer.printText('"');
    }

    protected void printEscaped(int i)
        throws IOException
    {
        String s = getEntityRef(i);
        if(s != null)
        {
            _printer.printText('&');
            _printer.printText(s);
            _printer.printText(';');
        } else
        if(i >= 32 && _encodingInfo.isPrintable(i) && i != 247 || i == 10 || i == 13 || i == 9)
        {
            if(i < 0x10000)
            {
                _printer.printText((char)i);
            } else
            {
                _printer.printText((char)((i - 0x10000 >> 10) + 55296));
                _printer.printText((char)((i - 0x10000 & 0x3ff) + 56320));
            }
        } else
        {
            _printer.printText("&#x");
            _printer.printText(Integer.toHexString(i));
            _printer.printText(';');
        }
    }

    protected void printEscaped(String s)
        throws IOException
    {
        for(int i = 0; i < s.length(); i++)
        {
            int j = ((int) (s.charAt(i)));
            if((j & 0xfc00) == 55296 && i + 1 < s.length())
            {
                char c = s.charAt(i + 1);
                if((c & 0xfc00) == 56320)
                {
                    j = (0x10000 + (j - 55296 << 10) + c) - 56320;
                    i++;
                }
            }
            printEscaped(j);
        }

    }

    protected ElementState getElementState()
    {
        return _elementStates[_elementStateCount];
    }

    protected ElementState enterElementState(String s, String s1, String s2, boolean flag)
    {
        if(_elementStateCount + 1 == _elementStates.length)
        {
            ElementState aelementstate[] = new ElementState[_elementStates.length + 10];
            for(int i = 0; i < _elementStates.length; i++)
                aelementstate[i] = _elementStates[i];

            for(int j = _elementStates.length; j < aelementstate.length; j++)
                aelementstate[j] = new ElementState();

            _elementStates = aelementstate;
        }
        _elementStateCount++;
        ElementState elementstate = _elementStates[_elementStateCount];
        elementstate.namespaceURI = s;
        elementstate.localName = s1;
        elementstate.rawName = s2;
        elementstate.preserveSpace = flag;
        elementstate.empty = true;
        elementstate.afterElement = false;
        elementstate.afterComment = false;
        elementstate.doCData = elementstate.inCData = false;
        elementstate.unescaped = false;
        elementstate.prefixes = _prefixes;
        _prefixes = null;
        return elementstate;
    }

    protected ElementState leaveElementState()
    {
        if(_elementStateCount > 0)
        {
            _prefixes = null;
            _elementStateCount--;
            return _elementStates[_elementStateCount];
        } else
        {
            String s = DOMMessageFormatter.formatMessage("http://apache.org/xml/serializer", "Internal", ((Object []) (null)));
            throw new IllegalStateException(s);
        }
    }

    protected boolean isDocumentState()
    {
        return _elementStateCount == 0;
    }

    protected String getPrefix(String s)
    {
        if(_prefixes != null)
        {
            String s1 = (String)_prefixes.get(((Object) (s)));
            if(s1 != null)
                return s1;
        }
        if(_elementStateCount == 0)
            return null;
        for(int i = _elementStateCount; i > 0; i--)
            if(_elementStates[i].prefixes != null)
            {
                String s2 = (String)_elementStates[i].prefixes.get(((Object) (s)));
                if(s2 != null)
                    return s2;
            }

        return null;
    }

    protected DOMError modifyDOMError(String s, short word0, Node node)
    {
        fDOMError.reset();
        fDOMError.fMessage = s;
        fDOMError.fSeverity = word0;
        fDOMError.fLocator = new DOMLocatorImpl(-1, -1, -1, node, ((String) (null)));
        return ((DOMError) (fDOMError));
    }

    private boolean getFeature(String s)
    {
        return ((Boolean)fFeatures.get(((Object) (s)))).booleanValue();
    }

    protected void fatalError(String s)
        throws IOException
    {
        if(fDOMErrorHandler != null)
        {
            modifyDOMError(s, (short)2, fCurrentNode);
            fDOMErrorHandler.handleError(((DOMError) (fDOMError)));
        } else
        {
            throw new IOException(s);
        }
    }

    public abstract void endElement(String s, String s1, String s2)
        throws SAXException;

    public abstract void startElement(String s, String s1, String s2, Attributes attributes)
        throws SAXException;

    public abstract void endElement(String s)
        throws SAXException;

    public abstract void startElement(String s, AttributeList attributelist)
        throws SAXException;
}
