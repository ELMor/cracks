// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 

package org.apache.xml.serialize;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public class EncodingInfo
{

    String name;
    String javaName;
    int lastPrintable;

    public EncodingInfo(String s, String s1, int i)
    {
        name = s;
        javaName = s1 != null ? s1 : s;
        lastPrintable = i;
    }

    public EncodingInfo(String s, int i)
    {
        this(s, s, i);
    }

    public String getName()
    {
        return name;
    }

    public Writer getWriter(OutputStream outputstream)
        throws UnsupportedEncodingException
    {
        if(javaName == null)
            return ((Writer) (new OutputStreamWriter(outputstream)));
        else
            return ((Writer) (new OutputStreamWriter(outputstream, javaName)));
    }

    public boolean isPrintable(int i)
    {
        return i <= lastPrintable;
    }
}
