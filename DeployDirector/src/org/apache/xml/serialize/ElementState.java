// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 

package org.apache.xml.serialize;

import java.util.Hashtable;

public class ElementState
{

    public String rawName;
    public String localName;
    public String namespaceURI;
    public boolean preserveSpace;
    public boolean empty;
    public boolean afterElement;
    public boolean afterComment;
    public boolean doCData;
    public boolean unescaped;
    public boolean inCData;
    public Hashtable prefixes;

    public ElementState()
    {
    }
}
