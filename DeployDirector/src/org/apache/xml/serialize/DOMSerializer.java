// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 

package org.apache.xml.serialize;

import java.io.IOException;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;

public interface DOMSerializer
{

    public abstract void serialize(Element element)
        throws IOException;

    public abstract void serialize(Document document)
        throws IOException;

    public abstract void serialize(DocumentFragment documentfragment)
        throws IOException;
}
