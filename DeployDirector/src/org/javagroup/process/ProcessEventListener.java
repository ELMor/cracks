// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ProcessEventListener.java

package org.javagroup.process;


// Referenced classes of package org.javagroup.process:
//            JProcess

public interface ProcessEventListener
{

    public abstract void processCreated(JProcess jprocess);

    public abstract void processDestroyed(JProcess jprocess);
}
