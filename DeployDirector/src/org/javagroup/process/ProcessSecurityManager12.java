// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ProcessSecurityManager12.java

package org.javagroup.process;

import java.io.PrintWriter;
import java.security.Permission;

// Referenced classes of package org.javagroup.process:
//            ProcessSecurityManager, ProcessManager

public class ProcessSecurityManager12 extends ProcessSecurityManager
{

    public ProcessSecurityManager12(ProcessManager manager)
    {
        super(manager);
    }

    ProcessSecurityManager12()
    {
    }

    public void checkPermission(Permission perm)
    {
        if(super.DEBUG_ON)
            ProcessSecurityManager.safeStream.println("[SecurityManager: checkPermission(Permission '" + perm + "')]");
    }

    public void checkPermission(Permission perm, Object context)
    {
        if(super.DEBUG_ON)
            ProcessSecurityManager.safeStream.println("[SecurityManager: checkPermission(Permission '" + perm + "', Object '" + context + "')]");
    }

    public void checkLink(String lib)
    {
        if(super.DEBUG_ON)
            ProcessSecurityManager.safeStream.println("[SecurityManager: checkLink(String '" + lib + "')]");
    }
}
