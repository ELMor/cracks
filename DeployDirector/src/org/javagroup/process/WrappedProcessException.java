// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   WrappedProcessException.java

package org.javagroup.process;

import java.io.PrintStream;

public class WrappedProcessException extends RuntimeException
{

    Throwable source;

    public WrappedProcessException(Throwable _source)
    {
        source = null;
        source = _source.fillInStackTrace();
    }

    public String getMessage()
    {
        return source.getMessage();
    }

    public String getLocalizedMessage()
    {
        return source.getLocalizedMessage();
    }

    public void printStackTrace()
    {
        source.printStackTrace();
    }

    public String toString()
    {
        return source.toString();
    }

    public void printStackTrace(PrintStream s)
    {
        source.printStackTrace(s);
    }
}
