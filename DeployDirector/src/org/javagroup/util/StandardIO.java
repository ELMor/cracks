// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StandardIO.java

package org.javagroup.util;

import java.io.BufferedInputStream;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

// Referenced classes of package org.javagroup.util:
//            StandardInputStream, Namespace

public class StandardIO
{

    protected static boolean _systemWrappersInstalled = false;
    protected PrintStream _out;
    protected PrintStream _err;
    protected InputStream _in;
    protected static PrintStream __out;
    protected static PrintStream __err;
    protected static InputStream __in;

    public StandardIO(PrintStream out, InputStream in, PrintStream err)
    {
        _out = out;
        _err = err;
        _in = in;
        __out = System.out;
        __in = ((InputStream) (new StandardInputStream()));
        __err = System.err;
    }

    public static StandardIO getInstance()
    {
        return (StandardIO)Namespace.getNamespace().getInstanceForClass(org.javagroup.util.StandardIO.class);
    }

    public static synchronized void ensureSystemWrappersInstalled()
    {
        if(!_systemWrappersInstalled)
            installSystemWrappers();
        try
        {
            System.setOut(__out);
            System.setIn(__in);
            System.setErr(__err);
        }
        catch(SecurityException e) { }
    }

    public static synchronized void installSystemWrappers()
    {
        if(!_systemWrappersInstalled)
        {
            _systemWrappersInstalled = true;
            PrintStream out = new PrintStream(((java.io.OutputStream) (new FileOutputStream(FileDescriptor.out))), true);
            PrintStream err = new PrintStream(((java.io.OutputStream) (new FileOutputStream(FileDescriptor.out))), true);
            InputStream in = ((InputStream) (new BufferedInputStream(((InputStream) (new FileInputStream(FileDescriptor.in))))));
            StandardIO stdio = new StandardIO(out, in, err);
            Namespace.getNamespace();
            Namespace.registerDefaultInstanceForClass(org.javagroup.util.StandardIO.class, ((Object) (stdio)));
        }
    }

    public PrintStream getOut()
    {
        return _out;
    }

    public InputStream getIn()
    {
        return _in;
    }

    public PrintStream getErr()
    {
        return _err;
    }

    static 
    {
        try
        {
            ensureSystemWrappersInstalled();
        }
        catch(RuntimeException e) { }
    }
}
