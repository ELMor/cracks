// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ResourceDisposalListener.java

package org.javagroup.util;


// Referenced classes of package org.javagroup.util:
//            Resource

public interface ResourceDisposalListener
{

    public abstract void resourceDisposed(Resource resource);
}
