/* URLClassLoader - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package org.javagroup.util;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class URLClassLoader extends ClassLoader
{
    protected Vector _urlClassPath;
    private boolean DEBUG_ON = false;
    private boolean VERBOSE_DEBUG_ON = false;
    private boolean LOAD_FROM_SYSTEM = false;
    private static boolean isJDK11 = true;
    private Hashtable jarFiles = new Hashtable();
    private Hashtable systemFiles = new Hashtable();
    private Hashtable missingFiles = new Hashtable();
    private Hashtable jarFilePointers = new Hashtable();
    protected static final Integer PACKAGE_FOUND = new Integer(1);
    protected static final Integer PACKAGE_NOT_FOUND = new Integer(2);
    protected static final Integer CLASS_NOT_FOUND = new Integer(3);
    protected static final PrintWriter safeStream;

    public URLClassLoader() {
        _urlClassPath = new Vector();
        String is_debug = System.getProperty("deploy.debug");
        if (is_debug != null && is_debug.indexOf("classloader") != -1) {
            safeStream
                .println("[CAM CL - (deploy.debug == classloader) == TRUE");
            DEBUG_ON = true;
        } else
            DEBUG_ON = false;
        if (is_debug != null && is_debug.indexOf("verboseclassloader") != -1) {
            safeStream.println
                ("[CAM CL - (deploy.debug == verboseclassloader) == TRUE");
            VERBOSE_DEBUG_ON = true;
        } else
            VERBOSE_DEBUG_ON = false;
        if (Boolean.getBoolean("deploy.classloader.usesystem"))
            LOAD_FROM_SYSTEM = true;
        else
            LOAD_FROM_SYSTEM = false;
        if (DEBUG_ON)
            safeStream.println
                ("[CAM CL - Value of deploy.classloader.usesystem is "
                 + LOAD_FROM_SYSTEM);
    }

    public URLClassLoader(URL classpath) {
        this();
        addClassPath(classpath);
    }

    public URLClassLoader(URL[] classpath) {
        this();
        addClassPath(classpath);
    }

    protected void finalize() {
        unlockJarFiles();
    }

    public void unlockJarFiles() {
        Enumeration e = jarFilePointers.keys();
        while (e.hasMoreElements()) {
            try {
                ZipFile f = (ZipFile) jarFilePointers.remove(e.nextElement());
                if (f != null) {
                    f.close();
                    Object object = null;
                }
            } catch (Exception exception) {
                /* empty */
            }
        }
    }

    public void addClassPath(URL[] classpath) {
        for (int i = 0; i < classpath.length; i++)
            addClassPath(classpath[i]);
    }

    public void addClassPath(URL classpath) {
        _urlClassPath.addElement(classpath);
    }

    public Class loadMainClass(String name) throws ClassNotFoundException {
        return loadClass(name, true, true);
    }

    public synchronized Class loadClass(String name, boolean resolve)
        throws ClassNotFoundException {
        return loadClass(name, resolve, false);
    }

    protected Integer isClassSystemLoadable(String name, String packageName) {
        if (LOAD_FROM_SYSTEM)
            return PACKAGE_FOUND;
        if (name.startsWith("java."))
            return PACKAGE_FOUND;
        Integer result = (Integer) systemFiles.get(packageName);
        if (result != null)
            return result;
        return PACKAGE_NOT_FOUND;
    }

    public synchronized Class loadClass
        (String name, boolean resolve, boolean mainClass)
        throws ClassNotFoundException {
        if (missingFiles.get(name) == CLASS_NOT_FOUND)
            throw new ClassNotFoundException("Class not found: " + name);
        Class klass = null;
        String packageName = extractPackageName(name);
        if (DEBUG_ON)
            safeStream.println("[CAM CL looking for: " + name + "]");
        try {
            klass = this.findLoadedClass(name);
            if (klass != null) {
                if (DEBUG_ON)
                    safeStream.println("\t[CAM CL using cached " + name + "]");
                return klass;
            }
            if (isClassSystemLoadable(name, packageName) == PACKAGE_FOUND) {
                try {
                    klass = this.findSystemClass(name);
                    if (klass != null) {
                        systemFiles.put(packageName, PACKAGE_FOUND);
                        if (DEBUG_ON)
                            safeStream.println("\t[CAM CL system(1) loaded "
                                               + name + "]");
                        return klass;
                    }
                } catch (Exception e) {
                    /* empty */
                } catch (Error error) {
                    /* empty */
                }
            }
            if (name.startsWith("javax.")
                && systemFiles.get(packageName) != PACKAGE_NOT_FOUND) {
                try {
                    klass = this.findSystemClass(name);
                    if (klass != null) {
                        systemFiles.put(packageName, PACKAGE_FOUND);
                        if (DEBUG_ON)
                            safeStream.println("\t[CAM CL system(2) loaded "
                                               + name + "]");
                        return klass;
                    }
                } catch (Exception e) {
                    /* empty */
                } catch (Error error) {
                    /* empty */
                }
            }
            byte[] data = readClassFile(name);
            if (data != null) {
                systemFiles.put(packageName, PACKAGE_NOT_FOUND);
                if (mainClass)
                    forcePublic(data);
                if (DEBUG_ON)
                    safeStream
                        .println("\t[CAM CL custom loaded " + name + "]");
                klass = this.defineClass(name, data, 0, data.length);
            }
            if (klass == null) {
                try {
                    klass = this.findSystemClass(name);
                    if (klass != null) {
                        systemFiles.put(extractPackageName(name),
                                        PACKAGE_FOUND);
                        if (DEBUG_ON)
                            safeStream.println("\t[CAM CL system(2) loaded "
                                               + name + "]");
                        return klass;
                    }
                } catch (Exception e) {
                    /* empty */
                } catch (Error error) {
                    /* empty */
                }
            }
            if (klass == null) {
                if (DEBUG_ON)
                    safeStream.println("\t[***CAM CL NOT FOUND " + name + "]");
                missingFiles.put(name, CLASS_NOT_FOUND);
                throw new ClassNotFoundException("Class not found: " + name);
            }
        } finally {
            if (klass != null && resolve)
                this.resolveClass(klass);
        }
        return klass;
    }

    protected byte[] readClassFile(String classname) {
        classname = classname.replace('.', '/') + ".class";
        return readFile(classname);
    }

    public InputStream getResourceAsStream(String name) {
        byte[] data = null;
        if (VERBOSE_DEBUG_ON)
            safeStream.println("[CAM CL searching for resource (as stream) '"
                               + name + "']");
        if (missingFiles.get(name) == CLASS_NOT_FOUND)
            return null;
        InputStream is = super.getResourceAsStream(name);
        if (is != null) {
            if (VERBOSE_DEBUG_ON)
                safeStream.println
                    ("[CAM CL system loaded resource (as stream) '" + name
                     + "']");
            data = readDataFromStream(is, -1L);
        } else
            data = readFile(name);
        if (data == null)
            missingFiles.put(name, CLASS_NOT_FOUND);
        return (data == null ? (ByteArrayInputStream) null
                : new ByteArrayInputStream(data));
    }

    public URL getResource(String name) {
        if (VERBOSE_DEBUG_ON)
            safeStream
                .println("[CAM CL searching for resource '" + name + "']");
        if (missingFiles.get(name) == CLASS_NOT_FOUND)
            return null;
        URL path = super.getResource(name);
        if (path != null) {
            if (VERBOSE_DEBUG_ON)
                safeStream
                    .println("[CAM CL system loaded resource '" + name + "']");
            return path;
        }
        Enumeration classpath = _urlClassPath.elements();
        classpath = _urlClassPath.elements();
        while (classpath.hasMoreElements()) {
            URL base_path = (URL) classpath.nextElement();
            try {
                File tester = new File(base_path.getFile());
                if (tester.canRead()) {
                    if (base_path.getProtocol().equals("file")
                        && base_path.toExternalForm().endsWith(".jar")
                        && isResourceInJar(tester, name)) {
                        if (VERBOSE_DEBUG_ON)
                            safeStream.println("\t[CAM CL loaded resource'"
                                               + name + "' from '"
                                               + base_path.getFile() + "']");
                        return new URL("deployjar://" + base_path.getFile()
                                       + "!/" + name);
                    }
                    path = new URL(base_path.getProtocol() + "://"
                                   + base_path.getFile() + "/" + name);
                    tester = new File(base_path.getFile(), name);
                    if (tester.canRead()) {
                        if (VERBOSE_DEBUG_ON)
                            safeStream.println("\t[CAM CL loaded resource '"
                                               + name + "' from '" + path
                                               + "']");
                        return path;
                    }
                }
            } catch (MalformedURLException mfue) {
                safeStream.println
                    ("JAR URL handler has not been registered. Cannot load "
                     + name);
                break;
            } catch (IOException ioexception) {
                /* empty */
            }
        }
        missingFiles.put(name, CLASS_NOT_FOUND);
        return null;
    }

    protected ZipFile openJarFile(String name) throws IOException {
        ZipFile result = null;
        if ((result = (ZipFile) jarFilePointers.get(name.toLowerCase()))
            != null)
            return result;
        result = new ZipFile(name);
        jarFilePointers.put(name.toLowerCase(), result);
        return result;
    }

    protected void closeJarFile(String name) throws IOException {
        /* empty */
    }

    protected byte[] readFromJarFile(File jar, String name) {
        if (!jar.canRead())
            return null;
        if (!isResourceInJar(jar, name))
            return null;
        byte[] data = null;
        try {
            String jarName = jar.getAbsolutePath();
            ZipFile f = openJarFile(jarName);
            ZipEntry entry = f.getEntry(name);
            InputStream is = null;
            if (entry != null) {
                is = f.getInputStream(entry);
                data = readDataFromStream(is, entry.getSize());
                is.close();
                is = null;
            }
            entry = null;
            closeJarFile(jarName);
        } catch (IOException ioexception) {
            /* empty */
        }
        return data;
    }

    protected byte[] readDataFromStream(InputStream in, long expected_size) {
        ByteArrayOutputStream out_buffer = new ByteArrayOutputStream();
        try {
            BufferedInputStream buffered_in = new BufferedInputStream(in);
            int n = -1;
            byte[] buffer = new byte[8096];
            while ((n = buffered_in.read(buffer, 0, 8096)) != -1)
                out_buffer.write(buffer, 0, n);
            return out_buffer.toByteArray();
        } catch (IOException e) {
            if (expected_size != -1L
                && expected_size == (long) out_buffer.size()) {
                if (DEBUG_ON) {
                    safeStream
                        .println("We got the exception: " + e.getMessage());
                    safeStream.println
                        ("but we also read in exactly the number of bytes we were expecting.");
                    safeStream.println("So I am ignoring this exception");
                }
                return out_buffer.toByteArray();
            }
            if (DEBUG_ON) {
                System.err.print("\n--- DEPLOY DEBUG "
                                 + this.getClass().getName() + ": ");
                e.printStackTrace();
            }
            return null;
        }
    }

    protected boolean isResourceInJar(File jar, String name) {
        if (!jar.canRead())
            return false;
        if (jarFiles.containsKey(jar.getAbsolutePath())) {
            Hashtable hash = (Hashtable) jarFiles.get(jar.getAbsolutePath());
            return hash.containsKey(name);
        }
        Hashtable newJar = new Hashtable();
        try {
            String jarName = jar.getAbsolutePath();
            ZipFile file = openJarFile(jarName);
            Enumeration e = file.entries();
            while (e.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) e.nextElement();
                if (entry != null) {
                    String entryName = entry.getName();
                    newJar.put(entryName, entryName);
                }
            }
            closeJarFile(jarName);
        } catch (IOException ioexception) {
            /* empty */
        }
        jarFiles.put(jar.getAbsolutePath(), newJar);
        return newJar.containsKey(name);
    }

    protected byte[] readFile(String name) {
        Enumeration classpath = _urlClassPath.elements();
        byte[] data = null;
        while (data == null && classpath.hasMoreElements()) {
            URL base_path = (URL) classpath.nextElement();
            if (base_path != null) {
                if (base_path.getProtocol().equalsIgnoreCase("file")) {
                    try {
                        File tester = new File(base_path.getFile());
                        if (tester.exists() && tester.isDirectory())
                            tester = new File(tester, name);
                        if (tester.exists()) {
                            String fullPath
                                = tester.getAbsolutePath().replace('\\', '/');
                            if (fullPath.endsWith(name)) {
                                if (VERBOSE_DEBUG_ON)
                                    safeStream.println("\t[CAM CL loaded '"
                                                       + name + "' from '"
                                                       + tester + "']");
                                InputStream in = new FileInputStream(tester);
                                data = readDataFromStream(in, tester.length());
                                in.close();
                                Object object = null;
                            } else {
                                if (VERBOSE_DEBUG_ON)
                                    safeStream.println("\t[CAM CL checking '"
                                                       + tester + "']");
                                data = readFromJarFile(tester, name);
                                if (VERBOSE_DEBUG_ON && data != null)
                                    safeStream.println("\t[CAM CL found '"
                                                       + name + "' in '"
                                                       + tester + "']");
                            }
                        }
                    } catch (IOException ioexception) {
                        /* empty */
                    }
                } else {
                    try {
                        URL path = new URL(base_path, name);
                        InputStream in = path.openStream();
                        data = readDataFromStream(in, -1L);
                        in.close();
                        Object object = null;
                    } catch (IOException ioexception) {
                        /* empty */
                    }
                }
            }
        }
        return data;
    }

    public static URL[] decodePathString(String classpath) {
        URL base_url = null;
        try {
            base_url = new URL("file:/");
        } catch (MalformedURLException malformedurlexception) {
            /* empty */
        }
        Vector classpath_urls = new Vector();
        if (base_url != null && classpath != null) {
            StringTokenizer tok = new StringTokenizer(classpath, ",");
            while (tok.hasMoreTokens()) {
                String path = tok.nextToken();
                URL path_url = null;
                try {
                    path_url = new URL(path);
                } catch (MalformedURLException e) {
                    try {
                        path_url = new URL(base_url, path);
                    } catch (Exception exception) {
                        /* empty */
                    }
                }
                if (path_url != null)
                    classpath_urls.addElement(path_url);
            }
        }
        URL[] paths = null;
        if (!classpath_urls.isEmpty()) {
            paths = new URL[classpath_urls.size()];
            for (int i = 0; i < paths.length; i++)
                paths[i] = (URL) classpath_urls.elementAt(i);
        }
        return paths;
    }

    public void forcePublic(byte[] theClass) {
        int constant_pool_count
            = (theClass[8] & 0xff) << 8 | theClass[9] & 0xff;
        int currOffset = 10;
        for (int i = 1; i < constant_pool_count; i++) {
            switch (theClass[currOffset] & 0xff) {
            case 7:
            case 8:
                currOffset += 3;
                break;
            case 3:
            case 4:
            case 9:
            case 10:
            case 11:
            case 12:
                currOffset += 5;
                break;
            case 5:
            case 6:
                currOffset += 9;
                i++;
                break;
            case 1: {
                int length = ((theClass[++currOffset] & 0xff) << 8
                              | theClass[++currOffset] & 0xff);
                currOffset += length + 1;
                break;
            }
            default:
                return;
            }
        }
        theClass[currOffset + 1] |= 0x1;
    }

    protected String extractPackageName(String className) {
        int index = className.lastIndexOf(".");
        if (index == -1)
            return "";
        String packageName = className.substring(0, index);
        return packageName;
    }

    static {
        String old_handlers = System.getProperty("java.protocol.handler.pkgs");
        if (old_handlers == null
            || old_handlers.indexOf("com.sitraka.deploy.cam") == -1) {
            if (old_handlers != null && old_handlers != "")
                old_handlers += "|";
            else
                old_handlers = "";
            old_handlers += "com.sitraka.deploy.cam";
            System.getProperties().put("java.protocol.handler.pkgs",
                                       old_handlers);
        }
        String version = System.getProperty("java.version");
        if (version.startsWith("1.1") || version.startsWith("B.1.1"))
            isJDK11 = true;
        else
            isJDK11 = false;
        safeStream
            = new PrintWriter(new FileOutputStream(FileDescriptor.out), true);
    }
}
