// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Namespace.java

package org.javagroup.util;

import java.util.Hashtable;

public class Namespace
{

    protected Namespace _child;
    private Hashtable _classToInstanceMap;
    private static Namespace __defaultNamespace = new Namespace();

    public Namespace()
    {
        _classToInstanceMap = new Hashtable();
    }

    public static Namespace getNamespace()
    {
        return __defaultNamespace;
    }

    public static void registerDefaultInstanceForClass(Class klass, Object instance)
    {
        __defaultNamespace.forceRegisterInstanceForClass(klass, instance);
    }

    public synchronized void registerNamespace(Namespace namespace)
    {
        if(_child == null)
            _child = namespace;
        else
            _child.registerNamespace(namespace);
    }

    public void registerInstanceForClass(Class klass, Object instance)
    {
        if(_child != null)
        {
            _child.registerInstanceForClass(klass, instance);
            return;
        }
        if(!klass.isInstance(instance))
        {
            throw new IllegalArgumentException(instance.toString() + " is not an instance of " + klass.getName());
        } else
        {
            _classToInstanceMap.put(((Object) (klass)), instance);
            return;
        }
    }

    public Object getInstanceForClass(Class klass)
    {
        if(_child != null)
        {
            Object instance = _child.getInstanceForClass(klass);
            if(instance != null)
                return instance;
        }
        return _classToInstanceMap.get(((Object) (klass)));
    }

    void forceRegisterInstanceForClass(Class klass, Object instance)
    {
        _classToInstanceMap.put(((Object) (klass)), instance);
    }

}
