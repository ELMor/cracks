// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AuthorizationInfo.java

package HTTPClient;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.ProtocolException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package HTTPClient:
//            NVPair, MD5, DefaultAuthHandler, GlobalConstants, 
//            AuthSchemeNotImplException, Util, HTTPConnection, AuthorizationHandler, 
//            RoRequest, Codecs, RoResponse

public class AuthorizationInfo
    implements GlobalConstants, Cloneable
{

    private static Hashtable CntxtList;
    private static AuthorizationHandler AuthHandler = new DefaultAuthHandler();
    private String host;
    private int port;
    private String scheme;
    private String realm;
    private String cookie;
    private NVPair auth_params[];
    private Object extra_info;
    private String paths[];

    AuthorizationInfo(String host, int port)
    {
        auth_params = new NVPair[0];
        extra_info = null;
        paths = new String[0];
        this.host = host.trim().toLowerCase();
        this.port = port;
    }

    public AuthorizationInfo(String host, int port, String scheme, String realm, NVPair params[], Object info)
    {
        auth_params = new NVPair[0];
        extra_info = null;
        paths = new String[0];
        this.scheme = scheme.trim();
        this.host = host.trim().toLowerCase();
        this.port = port;
        this.realm = realm;
        cookie = null;
        if(params != null)
            auth_params = Util.resizeArray(params, params.length);
        extra_info = info;
    }

    public AuthorizationInfo(String host, int port, String scheme, String realm, String cookie)
    {
        auth_params = new NVPair[0];
        extra_info = null;
        paths = new String[0];
        this.scheme = scheme.trim();
        this.host = host.trim().toLowerCase();
        this.port = port;
        this.realm = realm;
        if(cookie != null)
            this.cookie = cookie.trim();
        else
            this.cookie = null;
    }

    AuthorizationInfo(AuthorizationInfo templ)
    {
        auth_params = new NVPair[0];
        extra_info = null;
        paths = new String[0];
        scheme = templ.scheme;
        host = templ.host;
        port = templ.port;
        realm = templ.realm;
        cookie = templ.cookie;
        auth_params = Util.resizeArray(templ.auth_params, templ.auth_params.length);
        extra_info = templ.extra_info;
    }

    public static AuthorizationHandler setAuthHandler(AuthorizationHandler handler)
    {
        AuthorizationHandler tmp = AuthHandler;
        AuthHandler = handler;
        return tmp;
    }

    public static AuthorizationHandler getAuthHandler()
    {
        return AuthHandler;
    }

    public static AuthorizationInfo getAuthorization(String host, int port, String scheme, String realm)
    {
        return getAuthorization(host, port, scheme, realm, HTTPConnection.getDefaultContext());
    }

    public static synchronized AuthorizationInfo getAuthorization(String host, int port, String scheme, String realm, Object context)
    {
        Hashtable AuthList = Util.getList(CntxtList, context);
        AuthorizationInfo auth_info = new AuthorizationInfo(host.trim(), port, scheme.trim(), realm, (NVPair[])null, ((Object) (null)));
        return (AuthorizationInfo)AuthList.get(((Object) (auth_info)));
    }

    static AuthorizationInfo queryAuthHandler(AuthorizationInfo auth_info, RoRequest req, RoResponse resp)
        throws AuthSchemeNotImplException
    {
        if(AuthHandler == null)
            return null;
        AuthorizationInfo new_info = AuthHandler.getAuthorization(auth_info, req, resp);
        if(new_info != null)
            if(req != null)
                addAuthorization((AuthorizationInfo)new_info.clone(), req.getConnection().getContext());
            else
                addAuthorization((AuthorizationInfo)new_info.clone(), HTTPConnection.getDefaultContext());
        return new_info;
    }

    static synchronized AuthorizationInfo getAuthorization(AuthorizationInfo auth_info, RoRequest req, RoResponse resp, boolean query_auth_h)
        throws AuthSchemeNotImplException
    {
        Hashtable AuthList;
        if(req != null)
            AuthList = Util.getList(CntxtList, req.getConnection().getContext());
        else
            AuthList = Util.getList(CntxtList, HTTPConnection.getDefaultContext());
        AuthorizationInfo new_info = (AuthorizationInfo)AuthList.get(((Object) (auth_info)));
        if(new_info == null && query_auth_h)
            new_info = queryAuthHandler(auth_info, req, resp);
        return new_info;
    }

    static AuthorizationInfo getAuthorization(String host, int port, String scheme, String realm, boolean query_auth_h)
        throws AuthSchemeNotImplException
    {
        return getAuthorization(new AuthorizationInfo(host.trim(), port, scheme.trim(), realm, (NVPair[])null, ((Object) (null))), ((RoRequest) (null)), ((RoResponse) (null)), query_auth_h);
    }

    public static void addAuthorization(AuthorizationInfo auth_info)
    {
        addAuthorization(auth_info, HTTPConnection.getDefaultContext());
    }

    public static void addAuthorization(AuthorizationInfo auth_info, Object context)
    {
        Hashtable AuthList = Util.getList(CntxtList, context);
        AuthorizationInfo old_info = (AuthorizationInfo)AuthList.get(((Object) (auth_info)));
        if(old_info != null)
        {
            int ol = old_info.paths.length;
            int al = auth_info.paths.length;
            if(al == 0)
            {
                auth_info.paths = old_info.paths;
            } else
            {
                auth_info.paths = Util.resizeArray(auth_info.paths, al + ol);
                System.arraycopy(((Object) (old_info.paths)), 0, ((Object) (auth_info.paths)), al, ol);
            }
        }
        AuthList.put(((Object) (auth_info)), ((Object) (auth_info)));
    }

    public static void addAuthorization(String host, int port, String scheme, String realm, String cookie, NVPair params[], Object info)
    {
        addAuthorization(host, port, scheme, realm, cookie, params, info, HTTPConnection.getDefaultContext());
    }

    public static void addAuthorization(String host, int port, String scheme, String realm, String cookie, NVPair params[], Object info, Object context)
    {
        AuthorizationInfo auth = new AuthorizationInfo(host, port, scheme, realm, cookie);
        if(params != null && params.length > 0)
            auth.auth_params = Util.resizeArray(params, params.length);
        auth.extra_info = info;
        addAuthorization(auth, context);
    }

    public static void addBasicAuthorization(String host, int port, String realm, String user, String passwd)
    {
        addAuthorization(host, port, "Basic", realm, Codecs.base64Encode(user + ":" + passwd), (NVPair[])null, ((Object) (null)));
    }

    public static void addBasicAuthorization(String host, int port, String realm, String user, String passwd, Object context)
    {
        addAuthorization(host, port, "Basic", realm, Codecs.base64Encode(user + ":" + passwd), (NVPair[])null, ((Object) (null)), context);
    }

    public static void addDigestAuthorization(String host, int port, String realm, String user, String passwd)
    {
        addDigestAuthorization(host, port, realm, user, passwd, HTTPConnection.getDefaultContext());
    }

    public static void addDigestAuthorization(String host, int port, String realm, String user, String passwd, Object context)
    {
        AuthorizationInfo prev = getAuthorization(host, port, "Digest", realm, context);
        NVPair params[];
        if(prev == null)
        {
            params = new NVPair[4];
            params[0] = new NVPair("username", user);
            params[1] = new NVPair("uri", "");
            params[2] = new NVPair("nonce", "");
            params[3] = new NVPair("response", "");
        } else
        {
            params = prev.getParams();
            for(int idx = 0; idx < params.length; idx++)
            {
                if(!params[idx].getName().equalsIgnoreCase("username"))
                    continue;
                params[idx] = new NVPair("username", user);
                break;
            }

        }
        String extra[] = {
            (new MD5(((Object) (user + ":" + realm + ":" + passwd)))).asHex(), null
        };
        addAuthorization(host, port, "Digest", realm, ((String) (null)), params, ((Object) (extra)), context);
    }

    public static void removeAuthorization(AuthorizationInfo auth_info)
    {
        removeAuthorization(auth_info, HTTPConnection.getDefaultContext());
    }

    public static void removeAuthorization(AuthorizationInfo auth_info, Object context)
    {
        Hashtable AuthList = Util.getList(CntxtList, context);
        AuthList.remove(((Object) (auth_info)));
    }

    public static void removeAuthorization(String host, int port, String scheme, String realm)
    {
        removeAuthorization(new AuthorizationInfo(host, port, scheme, realm, (NVPair[])null, ((Object) (null))));
    }

    public static void removeAuthorization(String host, int port, String scheme, String realm, Object context)
    {
        removeAuthorization(new AuthorizationInfo(host, port, scheme, realm, (NVPair[])null, ((Object) (null))), context);
    }

    static AuthorizationInfo findBest(RoRequest req)
    {
        String path = Util.getPath(req.getRequestURI());
        String host = req.getConnection().getHost();
        int port = req.getConnection().getPort();
        Hashtable AuthList = Util.getList(CntxtList, req.getConnection().getContext());
        for(Enumeration list = AuthList.elements(); list.hasMoreElements();)
        {
            AuthorizationInfo info = (AuthorizationInfo)list.nextElement();
            if(info.host.equals(((Object) (host))) && info.port == port)
            {
                String paths[] = info.paths;
                for(int idx = 0; idx < paths.length; idx++)
                    if(path.equals(((Object) (paths[idx]))))
                        return info;

            }
        }

        AuthorizationInfo best = null;
        String base = path.substring(0, path.lastIndexOf('/') + 1);
        int min = 0x7fffffff;
        for(Enumeration list = AuthList.elements(); list.hasMoreElements();)
        {
            AuthorizationInfo info = (AuthorizationInfo)list.nextElement();
            if(info.host.equals(((Object) (host))) && info.port == port)
            {
                String paths[] = info.paths;
                for(int idx = 0; idx < paths.length; idx++)
                {
                    String ibase = paths[idx].substring(0, paths[idx].lastIndexOf('/') + 1);
                    if(base.equals(((Object) (ibase))))
                        return info;
                    if(base.startsWith(ibase))
                    {
                        int num_seg = 0;
                        for(int pos = ibase.length() - 1; (pos = base.indexOf('/', pos + 1)) != -1;)
                            num_seg++;

                        if(num_seg < min)
                        {
                            min = num_seg;
                            best = info;
                        }
                    } else
                    if(ibase.startsWith(base))
                    {
                        int num_seg = 0;
                        for(int pos = base.length(); (pos = ibase.indexOf('/', pos + 1)) != -1;)
                            num_seg++;

                        if(num_seg < min)
                        {
                            min = num_seg;
                            best = info;
                        }
                    }
                }

            }
        }

        return best;
    }

    public synchronized void addPath(String resource)
    {
        String path = Util.getPath(resource);
        for(int idx = 0; idx < paths.length; idx++)
            if(paths[idx].equals(((Object) (path))))
                return;

        paths = Util.resizeArray(paths, paths.length + 1);
        paths[paths.length - 1] = path;
    }

    static AuthorizationInfo[] parseAuthString(String challenge, RoRequest req, RoResponse resp)
        throws ProtocolException
    {
        int beg = 0;
        int end = 0;
        char buf[] = challenge.toCharArray();
        int len = buf.length;
        AuthorizationInfo auth_arr[] = new AuthorizationInfo[0];
        for(; Character.isSpace(buf[len - 1]); len--);
        do
        {
            beg = Util.skipSpace(buf, beg);
            if(beg != len)
            {
                end = Util.findSpace(buf, beg + 1);
                int sts;
                try
                {
                    sts = resp.getStatusCode();
                }
                catch(IOException ioe)
                {
                    throw new ProtocolException(((Throwable) (ioe)).toString());
                }
                AuthorizationInfo curr;
                if(sts == 401)
                    curr = new AuthorizationInfo(req.getConnection().getHost(), req.getConnection().getPort());
                else
                    curr = new AuthorizationInfo(req.getConnection().getProxyHost(), req.getConnection().getProxyPort());
                curr.scheme = challenge.substring(beg, end);
                boolean first = true;
                Vector params = new Vector();
                do
                {
                    beg = Util.skipSpace(buf, end);
                    if(beg == len)
                        break;
                    if(!first)
                    {
                        if(buf[beg] != ',')
                            throw new ProtocolException("Bad Authentication header format: '" + challenge + "'\nExpected \",\" at position " + beg);
                        beg = Util.skipSpace(buf, beg + 1);
                        if(beg == len)
                            break;
                        if(buf[beg] == ',')
                        {
                            end = beg;
                            continue;
                        }
                    }
                    int pstart = beg;
                    for(end = beg + 1; end < len && !Character.isSpace(buf[end]) && buf[end] != '=' && buf[end] != ','; end++);
                    if(first && (end == len || buf[end] == '=' && (end + 1 == len || buf[end + 1] == '=' && end + 2 == len)))
                    {
                        curr.cookie = challenge.substring(beg, len);
                        beg = len;
                        break;
                    }
                    String param_name = challenge.substring(beg, end);
                    beg = Util.skipSpace(buf, end);
                    if(beg < len && buf[beg] != '=' && buf[beg] != ',')
                    {
                        beg = pstart;
                        break;
                    }
                    String param_value;
                    if(buf[beg] == '=')
                    {
                        beg = Util.skipSpace(buf, beg + 1);
                        if(beg == len)
                            throw new ProtocolException("Bad Authentication header format: " + challenge + "\nUnexpected EOL after token" + " at position " + (end - 1));
                        if(buf[beg] != '"')
                        {
                            end = Util.skipToken(buf, beg);
                            if(end == beg)
                                throw new ProtocolException("Bad Authentication header format: " + challenge + "\nToken expected at " + "position " + beg);
                            param_value = challenge.substring(beg, end);
                        } else
                        {
                            end = beg++;
                            do
                                end = challenge.indexOf('"', end + 1);
                            while(end != -1 && challenge.charAt(end - 1) == '\\');
                            if(end == -1)
                                throw new ProtocolException("Bad Authentication header format: " + challenge + "\nClosing <\"> for " + "quoted-string starting at position " + beg + " not found");
                            param_value = Util.dequoteString(challenge.substring(beg, end));
                            end++;
                        }
                    } else
                    {
                        param_value = null;
                    }
                    if(param_name.equalsIgnoreCase("realm"))
                        curr.realm = param_value;
                    else
                        params.addElement(((Object) (new NVPair(param_name, param_value))));
                    first = false;
                } while(true);
                if(!params.isEmpty())
                {
                    curr.auth_params = new NVPair[params.size()];
                    params.copyInto(((Object []) (curr.auth_params)));
                }
                if(curr.realm == null)
                    curr.realm = "";
                auth_arr = Util.resizeArray(auth_arr, auth_arr.length + 1);
                auth_arr[auth_arr.length - 1] = curr;
            } else
            {
                return auth_arr;
            }
        } while(true);
    }

    public final String getHost()
    {
        return host;
    }

    public final int getPort()
    {
        return port;
    }

    public final String getScheme()
    {
        return scheme;
    }

    public final String getRealm()
    {
        return realm;
    }

    public final String getCookie()
    {
        return cookie;
    }

    public final void setCookie(String cookie)
    {
        this.cookie = cookie;
    }

    public final NVPair[] getParams()
    {
        return Util.resizeArray(auth_params, auth_params.length);
    }

    public final void setParams(NVPair params[])
    {
        if(params != null)
            auth_params = Util.resizeArray(params, params.length);
        else
            auth_params = new NVPair[0];
    }

    public final Object getExtraInfo()
    {
        return extra_info;
    }

    public final void setExtraInfo(Object info)
    {
        extra_info = info;
    }

    public String toString()
    {
        StringBuffer field = new StringBuffer(100);
        field.append(scheme);
        field.append(" ");
        if(cookie != null)
        {
            field.append(cookie);
        } else
        {
            if(realm.length() > 0)
            {
                field.append("realm=\"");
                field.append(Util.quoteString(realm, "\\\""));
                field.append('"');
            }
            for(int idx = 0; idx < auth_params.length; idx++)
            {
                field.append(',');
                field.append(auth_params[idx].getName());
                field.append("=\"");
                field.append(Util.quoteString(auth_params[idx].getValue(), "\\\""));
                field.append('"');
            }

        }
        return field.toString();
    }

    public int hashCode()
    {
        return (host + scheme.toLowerCase() + realm).hashCode();
    }

    public boolean equals(Object obj)
    {
        if(obj != null && (obj instanceof AuthorizationInfo))
        {
            AuthorizationInfo auth = (AuthorizationInfo)obj;
            if(host.equals(((Object) (auth.host))) && port == auth.port && scheme.equalsIgnoreCase(auth.scheme) && realm.equals(((Object) (auth.realm))))
                return true;
        }
        return false;
    }

    public Object clone()
    {
        AuthorizationInfo ai;
        try
        {
            ai = (AuthorizationInfo)super.clone();
            ai.auth_params = Util.resizeArray(auth_params, auth_params.length);
            try
            {
                ai.extra_info = extra_info.getClass().getMethod("clone", ((Class []) (null))).invoke(extra_info, ((Object []) (null)));
            }
            catch(Throwable t) { }
            ai.paths = new String[paths.length];
            System.arraycopy(((Object) (paths)), 0, ((Object) (ai.paths)), 0, paths.length);
        }
        catch(CloneNotSupportedException cnse)
        {
            throw new InternalError(((Throwable) (cnse)).toString());
        }
        return ((Object) (ai));
    }

    static 
    {
        CntxtList = new Hashtable();
        CntxtList.put(HTTPConnection.getDefaultContext(), ((Object) (new Hashtable())));
    }
}
