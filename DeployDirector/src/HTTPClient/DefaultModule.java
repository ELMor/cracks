// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefaultModule.java

package HTTPClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ProtocolException;

// Referenced classes of package HTTPClient:
//            HTTPClientModule, GlobalConstants, Response, Request, 
//            HttpOutputStream, RoRequest

class DefaultModule
    implements HTTPClientModule, GlobalConstants
{

    private int req_timeout_retries;

    DefaultModule()
    {
        req_timeout_retries = 3;
    }

    public int requestHandler(Request req, Response resp[])
    {
        return 0;
    }

    public void responsePhase1Handler(Response response, RoRequest rorequest)
    {
    }

    public int responsePhase2Handler(Response resp, Request req)
        throws IOException
    {
        int sts = resp.getStatusCode();
        switch(sts)
        {
        case 408: 
            if(req_timeout_retries-- == 0 || req.getStream() != null)
            {
                if(GlobalConstants.DebugMods)
                    System.err.println("DefM:  Status " + sts + " " + resp.getReasonLine() + " not handled - " + "maximum number of retries exceeded");
                return 10;
            }
            if(GlobalConstants.DebugMods)
                System.err.println("DefM:  Handling " + sts + " " + resp.getReasonLine() + " - " + "resending request");
            return 13;

        case 411: 
            if(req.getStream() != null && req.getStream().getLength() == -1)
                return 10;
            try
            {
                resp.getInputStream().close();
            }
            catch(IOException ioe) { }
            if(req.getData() != null)
                throw new ProtocolException("Received status code 411 even though Content-Length was sent");
            if(GlobalConstants.DebugMods)
                System.err.println("DefM:  Handling " + sts + " " + resp.getReasonLine() + " - resending " + "request with 'Content-length: 0'");
            req.setData(new byte[0]);
            return 13;

        case 505: 
            return 10;
        }
        return 10;
    }

    public void responsePhase3Handler(Response response, RoRequest rorequest)
    {
    }

    public void trailerHandler(Response response, RoRequest rorequest)
    {
    }
}
