// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   HTTPClientModuleConstants.java

package HTTPClient;


public interface HTTPClientModuleConstants
{

    public static final int REQ_CONTINUE = 0;
    public static final int REQ_RESTART = 1;
    public static final int REQ_SHORTCIRC = 2;
    public static final int REQ_RESPONSE = 3;
    public static final int REQ_RETURN = 4;
    public static final int REQ_NEWCON_RST = 5;
    public static final int REQ_NEWCON_SND = 6;
    public static final int RSP_CONTINUE = 10;
    public static final int RSP_RESTART = 11;
    public static final int RSP_SHORTCIRC = 12;
    public static final int RSP_REQUEST = 13;
    public static final int RSP_SEND = 14;
    public static final int RSP_NEWCON_REQ = 15;
    public static final int RSP_NEWCON_SND = 16;
}
