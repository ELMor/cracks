// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefaultAuthHandler.java

package HTTPClient;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextComponent;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

// Referenced classes of package HTTPClient:
//            NVPair

class BasicAuthBox extends Frame
{
    private class Close extends WindowAdapter
    {

        public void windowClosing(WindowEvent we)
        {
            (new Cancel()).actionPerformed(((ActionEvent) (null)));
        }

        private Close()
        {
        }

    }

    private class Cancel
        implements ActionListener
    {

        public void actionPerformed(ActionEvent ae)
        {
            done = 0;
            synchronized(BasicAuthBox.this)
            {
                notifyAll();
            }
        }

        private Cancel()
        {
        }

    }

    private class Clear
        implements ActionListener
    {

        public void actionPerformed(ActionEvent ae)
        {
            user.setText("");
            pass.setText("");
            ((Component) (user)).requestFocus();
        }

        private Clear()
        {
        }

    }

    private class Ok
        implements ActionListener
    {

        public void actionPerformed(ActionEvent ae)
        {
            done = 1;
            synchronized(BasicAuthBox.this)
            {
                notifyAll();
            }
        }

        private Ok()
        {
        }

    }


    private static final String title = "Authorization Request";
    private Dimension screen;
    private Label line1;
    private Label line2;
    private Label line3;
    private TextField user;
    private TextField pass;
    private int done;
    private static final int OK = 1;
    private static final int CANCEL = 0;

    BasicAuthBox()
    {
        super("Authorization Request");
        screen = ((Window)this).getToolkit().getScreenSize();
        ((Frame)this).addNotify();
        ((Window)this).addWindowListener(((java.awt.event.WindowListener) (new Close())));
        ((Container)this).setLayout(((java.awt.LayoutManager) (new BorderLayout())));
        Panel p = new Panel(((java.awt.LayoutManager) (new GridLayout(3, 1))));
        ((Container) (p)).add(((Component) (line1 = new Label())));
        ((Container) (p)).add(((Component) (line2 = new Label())));
        ((Container) (p)).add(((Component) (line3 = new Label())));
        ((Container)this).add("North", ((Component) (p)));
        p = new Panel(((java.awt.LayoutManager) (new GridLayout(2, 1))));
        ((Container) (p)).add(((Component) (new Label("Username:"))));
        ((Container) (p)).add(((Component) (new Label("Password:"))));
        ((Container)this).add("West", ((Component) (p)));
        p = new Panel(((java.awt.LayoutManager) (new GridLayout(2, 1))));
        ((Container) (p)).add(((Component) (user = new TextField(30))));
        ((Container) (p)).add(((Component) (pass = new TextField(30))));
        pass.addActionListener(((ActionListener) (new Ok())));
        pass.setEchoChar('*');
        ((Container)this).add("East", ((Component) (p)));
        GridBagLayout gb = new GridBagLayout();
        p = new Panel(((java.awt.LayoutManager) (gb)));
        GridBagConstraints constr = new GridBagConstraints();
        Panel pp = new Panel();
        ((Container) (p)).add(((Component) (pp)));
        constr.gridwidth = 0;
        gb.setConstraints(((Component) (pp)), constr);
        constr.gridwidth = 1;
        constr.weightx = 1.0D;
        Button b;
        ((Container) (p)).add(((Component) (b = new Button("  OK  "))));
        b.addActionListener(((ActionListener) (new Ok())));
        constr.weightx = 1.0D;
        gb.setConstraints(((Component) (b)), constr);
        ((Container) (p)).add(((Component) (b = new Button("Clear"))));
        b.addActionListener(((ActionListener) (new Clear())));
        constr.weightx = 2D;
        gb.setConstraints(((Component) (b)), constr);
        ((Container) (p)).add(((Component) (b = new Button("Cancel"))));
        b.addActionListener(((ActionListener) (new Cancel())));
        constr.weightx = 1.0D;
        gb.setConstraints(((Component) (b)), constr);
        ((Container)this).add("South", ((Component) (p)));
        ((Window)this).pack();
        ((Frame)this).setResizable(false);
    }

    synchronized NVPair getInput(String l1, String l2, String l3)
    {
        line1.setText(l1);
        line2.setText(l2);
        line3.setText(l3);
        ((Component) (line1)).invalidate();
        ((Component) (line2)).invalidate();
        ((Component) (line3)).invalidate();
        ((Frame)this).setResizable(true);
        ((Window)this).pack();
        ((Frame)this).setResizable(false);
        ((Component)this).setLocation((screen.width - ((Container)this).getPreferredSize().width) / 2, (int)((double)((screen.height - ((Container)this).getPreferredSize().height) / 2) * 0.69999999999999996D));
        ((Component) (user)).requestFocus();
        ((Component)this).setVisible(true);
        try
        {
            ((Object)this).wait();
        }
        catch(InterruptedException e) { }
        ((Component)this).setVisible(false);
        NVPair result = new NVPair(((TextComponent) (user)).getText(), ((TextComponent) (pass)).getText());
        user.setText("");
        pass.setText("");
        if(done == 0)
            return null;
        else
            return result;
    }



}
