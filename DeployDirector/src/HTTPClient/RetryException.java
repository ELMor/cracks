// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RetryException.java

package HTTPClient;

import java.io.IOException;

// Referenced classes of package HTTPClient:
//            Request, Response

class RetryException extends IOException
{

    Request request;
    Response response;
    RetryException first;
    RetryException next;
    IOException exception;
    boolean conn_reset;
    boolean restart;

    public RetryException()
    {
        request = null;
        response = null;
        first = null;
        next = null;
        exception = null;
        conn_reset = true;
        restart = false;
    }

    public RetryException(String s)
    {
        super(s);
        request = null;
        response = null;
        first = null;
        next = null;
        exception = null;
        conn_reset = true;
        restart = false;
    }

    void addToListAfter(RetryException re)
    {
        if(re == null)
            return;
        if(re.next != null)
            next = re.next;
        re.next = this;
    }
}
