// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StreamDemultiplexor.java

package HTTPClient;


// Referenced classes of package HTTPClient:
//            GlobalConstants, StreamDemultiplexor

class SocketTimeout extends Thread
    implements GlobalConstants
{
    class TimeoutEntry
    {

        boolean restart;
        boolean hyber;
        boolean alive;
        StreamDemultiplexor demux;
        TimeoutEntry next;
        TimeoutEntry prev;

        void reset()
        {
            hyber = false;
            if(restart)
                return;
            restart = true;
            synchronized(time_list)
            {
                if(!alive)
                    return;
                next.prev = prev;
                prev.next = next;
                next = time_list[current];
                prev = time_list[current].prev;
                prev.next = this;
                next.prev = this;
            }
        }

        void hyber()
        {
            if(alive)
                hyber = true;
        }

        void kill()
        {
            alive = false;
            restart = false;
            hyber = false;
            synchronized(time_list)
            {
                if(prev == null)
                    return;
                next.prev = prev;
                prev.next = next;
                prev = null;
            }
        }

        TimeoutEntry(StreamDemultiplexor demux)
        {
            restart = false;
            hyber = false;
            alive = true;
            next = null;
            prev = null;
            this.demux = demux;
        }
    }


    private TimeoutEntry time_list[];
    private int current;

    SocketTimeout(int secs)
    {
        super("SocketTimeout");
        try
        {
            ((Thread)this).setDaemon(true);
        }
        catch(SecurityException se) { }
        ((Thread)this).setPriority(10);
        time_list = new TimeoutEntry[secs];
        for(int idx = 0; idx < secs; idx++)
        {
            time_list[idx] = new TimeoutEntry(((StreamDemultiplexor) (null)));
            time_list[idx].next = time_list[idx].prev = time_list[idx];
        }

        current = 0;
    }

    public TimeoutEntry setTimeout(StreamDemultiplexor demux)
    {
        TimeoutEntry entry = new TimeoutEntry(demux);
        synchronized(time_list)
        {
            entry.next = time_list[current];
            entry.prev = time_list[current].prev;
            entry.prev.next = entry;
            entry.next.prev = entry;
        }
        return entry;
    }

    public void run()
    {
        TimeoutEntry marked = null;
        do
        {
            try
            {
                Thread.sleep(1000L);
            }
            catch(InterruptedException ie) { }
            synchronized(time_list)
            {
                for(TimeoutEntry entry = time_list[current].next; entry != time_list[current]; entry = entry.next)
                    entry.restart = false;

                current++;
                if(current >= time_list.length)
                    current = 0;
                for(TimeoutEntry entry = time_list[current].next; entry != time_list[current]; entry = entry.next)
                    if(entry.alive && !entry.hyber)
                    {
                        TimeoutEntry prev = entry.prev;
                        entry.kill();
                        entry.next = marked;
                        marked = entry;
                        entry = prev;
                    }

            }
            for(; marked != null; marked = marked.next)
                marked.demux.markForClose(((Response) (null)));

        } while(true);
    }


}
