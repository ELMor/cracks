// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RoResponse.java

package HTTPClient;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

public interface RoResponse
{

    public abstract int getStatusCode()
        throws IOException;

    public abstract String getReasonLine()
        throws IOException;

    public abstract String getVersion()
        throws IOException;

    public abstract String getHeader(String s)
        throws IOException;

    public abstract int getHeaderAsInt(String s)
        throws IOException, NumberFormatException;

    public abstract Date getHeaderAsDate(String s)
        throws IOException, IllegalArgumentException;

    public abstract String getTrailer(String s)
        throws IOException;

    public abstract int getTrailerAsInt(String s)
        throws IOException, NumberFormatException;

    public abstract Date getTrailerAsDate(String s)
        throws IOException, IllegalArgumentException;

    public abstract byte[] getData()
        throws IOException;

    public abstract InputStream getInputStream()
        throws IOException;
}
