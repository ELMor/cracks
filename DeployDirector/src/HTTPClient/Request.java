// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Request.java

package HTTPClient;


// Referenced classes of package HTTPClient:
//            NVPair, RoRequest, HTTPConnection, HttpOutputStream

public final class Request
    implements RoRequest
{

    private static final NVPair empty[] = new NVPair[0];
    private HTTPConnection connection;
    private String method;
    private String req_uri;
    private NVPair headers[];
    private byte data[];
    private HttpOutputStream stream;
    private boolean allow_ui;
    long delay_entity;
    int num_retries;
    boolean dont_pipeline;
    boolean aborted;
    boolean internal_subrequest;

    public Request(HTTPConnection con, String method, String req_uri, NVPair headers[], byte data[], HttpOutputStream stream, boolean allow_ui)
    {
        delay_entity = 0L;
        num_retries = 0;
        dont_pipeline = false;
        aborted = false;
        internal_subrequest = false;
        connection = con;
        this.method = method;
        if("CONNECT".equals(((Object) (method))))
            this.req_uri = req_uri;
        else
            setRequestURI(req_uri);
        setHeaders(headers);
        this.data = data;
        this.stream = stream;
        this.allow_ui = allow_ui;
    }

    public HTTPConnection getConnection()
    {
        return connection;
    }

    public void setConnection(HTTPConnection con)
    {
        connection = con;
    }

    public String getMethod()
    {
        return method;
    }

    public void setMethod(String method)
    {
        this.method = method;
    }

    public String getRequestURI()
    {
        return req_uri;
    }

    public void setRequestURI(String req_uri)
    {
        if(req_uri != null && req_uri.trim().length() > 0)
        {
            req_uri = req_uri.trim();
            if(req_uri.charAt(0) != '/' && !req_uri.equals("*"))
                req_uri = "/" + req_uri;
            this.req_uri = req_uri;
        } else
        {
            this.req_uri = "/";
        }
    }

    public NVPair[] getHeaders()
    {
        return headers;
    }

    public void setHeaders(NVPair headers[])
    {
        if(headers != null)
            this.headers = headers;
        else
            this.headers = empty;
    }

    public byte[] getData()
    {
        return data;
    }

    public void setData(byte data[])
    {
        this.data = data;
    }

    public HttpOutputStream getStream()
    {
        return stream;
    }

    public void setStream(HttpOutputStream stream)
    {
        this.stream = stream;
    }

    public boolean allowUI()
    {
        return allow_ui;
    }

    public void setAllowUI(boolean allow_ui)
    {
        this.allow_ui = allow_ui;
    }

    public String toString()
    {
        return ((Object)this).getClass().getName() + ": " + method + " " + req_uri;
    }

}
