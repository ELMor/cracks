// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AuthorizationModule.java

package HTTPClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ProtocolException;
import java.util.Hashtable;

// Referenced classes of package HTTPClient:
//            AuthorizationInfo, AuthSchemeNotImplException, NVPair, HTTPClientModule, 
//            GlobalConstants, Request, HTTPConnection, Util, 
//            AuthorizationHandler, Response, RoRequest, RoResponse

class AuthorizationModule
    implements HTTPClientModule, GlobalConstants
{

    private static Hashtable proxy_cntxt_list = new Hashtable();
    private int auth_lst_idx;
    private int prxy_lst_idx;
    private int auth_scm_idx;
    private int prxy_scm_idx;
    private AuthorizationInfo auth_sent;
    private AuthorizationInfo prxy_sent;
    private boolean auth_from_4xx;
    private boolean prxy_from_4xx;
    private int num_tries;

    AuthorizationModule()
    {
        auth_lst_idx = 0;
        prxy_lst_idx = 0;
        auth_scm_idx = 0;
        prxy_scm_idx = 0;
        auth_sent = null;
        prxy_sent = null;
        auth_from_4xx = false;
        prxy_from_4xx = false;
        num_tries = 0;
    }

    public int requestHandler(Request req, Response resp[])
    {
        AuthorizationHandler auth_handler;
label0:
        {
            HTTPConnection con = req.getConnection();
            auth_handler = AuthorizationInfo.getAuthHandler();
            if(con.getProxyHost() == null || prxy_from_4xx)
                break label0;
            Hashtable proxy_auth_list = Util.getList(proxy_cntxt_list, req.getConnection().getContext());
            AuthorizationInfo guess = (AuthorizationInfo)proxy_auth_list.get(((Object) (con.getProxyHost() + ":" + con.getProxyPort())));
            if(guess == null)
                break label0;
            if(auth_handler != null)
            {
                try
                {
                    guess = auth_handler.fixupAuthInfo(guess, ((RoRequest) (req)), ((AuthorizationInfo) (null)), ((RoResponse) (null)));
                }
                catch(AuthSchemeNotImplException atnie)
                {
                    break label0;
                }
                if(guess == null)
                    break label0;
            }
            NVPair hdrs[] = req.getHeaders();
            int idx;
            for(idx = 0; idx < hdrs.length; idx++)
                if(hdrs[idx].getName().equalsIgnoreCase("Proxy-Authorization"))
                    break;

            if(idx == hdrs.length)
            {
                hdrs = Util.resizeArray(hdrs, idx + 1);
                req.setHeaders(hdrs);
            }
            hdrs[idx] = new NVPair("Proxy-Authorization", guess.toString());
            prxy_sent = guess;
            prxy_from_4xx = false;
            if(GlobalConstants.DebugMods)
                System.err.println("AuthM: Preemptively sending Proxy-Authorization '" + guess + "'");
        }
label1:
        {
            if(auth_from_4xx)
                break label1;
            AuthorizationInfo guess = AuthorizationInfo.findBest(((RoRequest) (req)));
            if(guess == null)
                break label1;
            if(auth_handler != null)
            {
                try
                {
                    guess = auth_handler.fixupAuthInfo(guess, ((RoRequest) (req)), ((AuthorizationInfo) (null)), ((RoResponse) (null)));
                }
                catch(AuthSchemeNotImplException atnie)
                {
                    break label1;
                }
                if(guess == null)
                    break label1;
            }
            NVPair hdrs[] = req.getHeaders();
            int idx;
            for(idx = 0; idx < hdrs.length; idx++)
                if(hdrs[idx].getName().equalsIgnoreCase("Authorization"))
                    break;

            if(idx == hdrs.length)
            {
                hdrs = Util.resizeArray(hdrs, idx + 1);
                req.setHeaders(hdrs);
            }
            hdrs[idx] = new NVPair("Authorization", guess.toString());
            auth_sent = guess;
            auth_from_4xx = false;
            if(GlobalConstants.DebugMods)
                System.err.println("AuthM: Preemptively sending Authorization '" + guess + "'");
        }
        return 0;
    }

    public void responsePhase1Handler(Response resp, RoRequest req)
        throws IOException
    {
        if(resp.getStatusCode() != 401 && resp.getStatusCode() != 407)
        {
            if(auth_sent != null && auth_from_4xx)
                try
                {
                    AuthorizationInfo.getAuthorization(auth_sent, req, ((RoResponse) (resp)), false).addPath(req.getRequestURI());
                }
                catch(AuthSchemeNotImplException asnie) { }
            num_tries = 0;
        }
        auth_from_4xx = false;
        prxy_from_4xx = false;
        if(resp.getHeader("WWW-Authenticate") == null)
        {
            auth_lst_idx = 0;
            auth_scm_idx = 0;
        }
        if(resp.getHeader("Proxy-Authenticate") == null)
        {
            prxy_lst_idx = 0;
            prxy_scm_idx = 0;
        }
    }

    public int responsePhase2Handler(Response resp, Request req)
        throws IOException, AuthSchemeNotImplException
    {
        AuthorizationHandler h = AuthorizationInfo.getAuthHandler();
        if(h != null)
            h.handleAuthHeaders(resp, ((RoRequest) (req)), auth_sent, prxy_sent);
        int sts = resp.getStatusCode();
        switch(sts)
        {
        case 401: 
        case 407: 
            num_tries++;
            if(num_tries > 10)
                throw new ProtocolException("Bug in authorization handling: server refused the given info 10 times");
            if(GlobalConstants.DebugMods)
                System.err.println("AuthM: Handling status: " + sts + " " + resp.getReasonLine());
            int idx_arr[] = {
                auth_lst_idx, auth_scm_idx
            };
            auth_sent = setAuthHeaders(resp.getHeader("WWW-Authenticate"), req, ((RoResponse) (resp)), "Authorization", idx_arr, auth_sent);
            if(auth_sent != null)
                auth_from_4xx = true;
            auth_lst_idx = idx_arr[0];
            auth_scm_idx = idx_arr[1];
            idx_arr[0] = prxy_lst_idx;
            idx_arr[1] = prxy_scm_idx;
            prxy_sent = setAuthHeaders(resp.getHeader("Proxy-Authenticate"), req, ((RoResponse) (resp)), "Proxy-Authorization", idx_arr, prxy_sent);
            if(prxy_sent != null)
                prxy_from_4xx = true;
            prxy_lst_idx = idx_arr[0];
            prxy_scm_idx = idx_arr[1];
            if(prxy_sent != null)
            {
                HTTPConnection con = req.getConnection();
                Util.getList(proxy_cntxt_list, con.getContext()).put(((Object) (con.getProxyHost() + ":" + con.getProxyPort())), ((Object) (prxy_sent)));
            }
            if(req.getStream() == null && (auth_sent != null || prxy_sent != null))
            {
                try
                {
                    resp.getInputStream().close();
                }
                catch(IOException ioe) { }
                if(GlobalConstants.DebugMods)
                    if(auth_sent != null)
                        System.err.println("AuthM: Resending request with Authorization '" + auth_sent + "'");
                    else
                        System.err.println("AuthM: Resending request with Proxy-Authorization '" + prxy_sent + "'");
                return 13;
            }
            if(auth_sent == null && prxy_sent == null && resp.getHeader("WWW-Authenticate") == null && resp.getHeader("Proxy-Authenticate") == null)
                if(resp.getStatusCode() == 401)
                    throw new ProtocolException("Missing WWW-Authenticate header");
                else
                    throw new ProtocolException("Missing Proxy-Authenticate header");
            if(GlobalConstants.DebugMods)
                if(req.getStream() == null)
                    System.err.println("AuthM: status " + sts + " not " + "handled - request has an output " + "stream");
                else
                    System.err.println("AuthM: No Auth Info found - status " + sts + " not handled");
            return 10;
        }
        return 10;
    }

    public void responsePhase3Handler(Response response, RoRequest rorequest)
    {
    }

    public void trailerHandler(Response resp, RoRequest req)
        throws IOException
    {
        AuthorizationHandler h = AuthorizationInfo.getAuthHandler();
        if(h != null)
            h.handleAuthTrailers(resp, req, auth_sent, prxy_sent);
    }

    private AuthorizationInfo setAuthHeaders(String auth_str, Request req, RoResponse resp, String header, int idx_arr[], AuthorizationInfo prev)
        throws ProtocolException, AuthSchemeNotImplException
    {
        if(auth_str == null)
            return null;
        AuthorizationInfo challenge[] = AuthorizationInfo.parseAuthString(auth_str, ((RoRequest) (req)), resp);
        if(GlobalConstants.DebugMods)
        {
            System.err.println("AuthM: parsed " + challenge.length + " challenges:");
            for(int idx = 0; idx < challenge.length; idx++)
                System.err.println("AuthM: Challenge " + challenge[idx]);

        }
        if(prev != null && prev.getScheme().equalsIgnoreCase("Basic"))
        {
            for(int idx = 0; idx < challenge.length; idx++)
                if(prev.getRealm().equals(((Object) (challenge[idx].getRealm()))) && prev.getScheme().equalsIgnoreCase(challenge[idx].getScheme()))
                    AuthorizationInfo.removeAuthorization(prev, req.getConnection().getContext());

        }
        AuthorizationInfo credentials = null;
        AuthorizationHandler auth_handler = AuthorizationInfo.getAuthHandler();
        while(credentials == null && idx_arr[0] != -1) 
        {
            credentials = AuthorizationInfo.getAuthorization(challenge[idx_arr[0]], ((RoRequest) (req)), resp, false);
            if(auth_handler != null && credentials != null)
                credentials = auth_handler.fixupAuthInfo(credentials, ((RoRequest) (req)), challenge[idx_arr[0]], resp);
            if(++idx_arr[0] == challenge.length)
                idx_arr[0] = -1;
        }
        if(credentials == null)
        {
            for(int idx = 0; idx < challenge.length;)
                try
                {
                    credentials = AuthorizationInfo.queryAuthHandler(challenge[idx_arr[1]], ((RoRequest) (req)), resp);
                    break;
                }
                catch(AuthSchemeNotImplException atnie)
                {
                    if(idx == challenge.length - 1)
                        throw atnie;
                    idx++;
                }

            if(++idx_arr[1] == challenge.length)
                idx_arr[1] = 0;
        }
        if(credentials == null)
            return null;
        NVPair hdrs[] = req.getHeaders();
        int auth_idx;
        for(auth_idx = 0; auth_idx < hdrs.length; auth_idx++)
            if(hdrs[auth_idx].getName().equalsIgnoreCase(header))
                break;

        if(auth_idx == hdrs.length)
        {
            hdrs = Util.resizeArray(hdrs, auth_idx + 1);
            req.setHeaders(hdrs);
        }
        hdrs[auth_idx] = new NVPair(header, credentials.toString());
        return credentials;
    }

}
