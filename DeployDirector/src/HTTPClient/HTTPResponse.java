// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   HTTPResponse.java

package HTTPClient;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.PrintStream;
import java.net.URL;
import java.util.Date;
import java.util.Enumeration;

// Referenced classes of package HTTPClient:
//            URI, ParseException, RetryException, GlobalConstants, 
//            HTTPClientModuleConstants, ModuleException, Request, HTTPConnection, 
//            Response, CIHashtable, HttpOutputStream, HTTPClientModule, 
//            Util

public class HTTPResponse
    implements GlobalConstants, HTTPClientModuleConstants
{

    private HTTPClientModule modules[];
    private int timeout;
    private Request request;
    private Response response;
    private HttpOutputStream out_stream;
    private InputStream inp_stream;
    private int StatusCode;
    private String ReasonLine;
    private String Version;
    private URI OriginalURI;
    private URI EffectiveURI;
    private CIHashtable Headers;
    private CIHashtable Trailers;
    private int ContentLength;
    private byte Data[];
    private boolean initialized;
    private boolean got_trailers;
    private boolean aborted;
    private String method;
    private boolean handle_trailers;
    private boolean trailers_handled;

    HTTPResponse(HTTPClientModule modules[], int timeout, Request orig)
    {
        request = null;
        response = null;
        out_stream = null;
        OriginalURI = null;
        EffectiveURI = null;
        Headers = null;
        Trailers = null;
        ContentLength = -1;
        Data = null;
        initialized = false;
        got_trailers = false;
        aborted = false;
        method = null;
        handle_trailers = false;
        trailers_handled = false;
        this.modules = modules;
        this.timeout = timeout;
        try
        {
            OriginalURI = new URI(orig.getConnection().getProtocol(), orig.getConnection().getHost(), orig.getConnection().getPort(), orig.getRequestURI());
        }
        catch(ParseException pe) { }
        method = orig.getMethod();
    }

    void set(Request req, Response resp)
    {
        request = req;
        response = resp;
        resp.http_resp = this;
        resp.timeout = timeout;
        aborted = resp.final_resp;
    }

    void set(Request req, HttpOutputStream out_stream)
    {
        request = req;
        this.out_stream = out_stream;
    }

    public final int getStatusCode()
        throws IOException, ModuleException
    {
        if(!initialized)
            handleResponse();
        return StatusCode;
    }

    public final String getReasonLine()
        throws IOException, ModuleException
    {
        if(!initialized)
            handleResponse();
        return ReasonLine;
    }

    public final String getVersion()
        throws IOException, ModuleException
    {
        if(!initialized)
            handleResponse();
        return Version;
    }

    /**
     * @deprecated Method getServer is deprecated
     */

    public final String getServer()
        throws IOException, ModuleException
    {
        if(!initialized)
            handleResponse();
        return getHeader("Server");
    }

    public final URI getOriginalURI()
    {
        return OriginalURI;
    }

    /**
     * @deprecated Method getEffectiveURL is deprecated
     */

    public final URL getEffectiveURL()
        throws IOException, ModuleException
    {
        if(!initialized)
            handleResponse();
        if(EffectiveURI != null)
            return EffectiveURI.toURL();
        else
            return null;
    }

    public final URI getEffectiveURI()
        throws IOException, ModuleException
    {
        if(!initialized)
            handleResponse();
        if(EffectiveURI != null)
            return EffectiveURI;
        else
            return OriginalURI;
    }

    public String getHeader(String hdr)
        throws IOException, ModuleException
    {
        if(!initialized)
            handleResponse();
        return (String)Headers.get(hdr.trim());
    }

    public int getHeaderAsInt(String hdr)
        throws IOException, ModuleException, NumberFormatException
    {
        return Integer.parseInt(getHeader(hdr));
    }

    public Date getHeaderAsDate(String hdr)
        throws IOException, IllegalArgumentException, ModuleException
    {
        String raw_date = getHeader(hdr);
        if(raw_date == null)
            return null;
        if(raw_date.toUpperCase().indexOf("GMT") == -1)
            raw_date = raw_date + " GMT";
        Date date;
        try
        {
            date = new Date(raw_date);
        }
        catch(IllegalArgumentException iae)
        {
            long time;
            try
            {
                time = Long.parseLong(raw_date);
            }
            catch(NumberFormatException nfe)
            {
                throw iae;
            }
            if(time < 0L)
                time = 0L;
            date = new Date(time * 1000L);
        }
        return date;
    }

    public Enumeration listHeaders()
        throws IOException, ModuleException
    {
        if(!initialized)
            handleResponse();
        return Headers.keys();
    }

    public String getTrailer(String trailer)
        throws IOException, ModuleException
    {
        if(!got_trailers)
            getTrailers();
        return (String)Trailers.get(trailer.trim());
    }

    public int getTrailerAsInt(String trailer)
        throws IOException, ModuleException, NumberFormatException
    {
        return Integer.parseInt(getTrailer(trailer));
    }

    public Date getTrailerAsDate(String trailer)
        throws IOException, IllegalArgumentException, ModuleException
    {
        String raw_date = getTrailer(trailer);
        if(raw_date == null)
            return null;
        if(raw_date.toUpperCase().indexOf("GMT") == -1)
            raw_date = raw_date + " GMT";
        Date date;
        try
        {
            date = new Date(raw_date);
        }
        catch(IllegalArgumentException iae)
        {
            long time;
            try
            {
                time = Long.parseLong(raw_date);
            }
            catch(NumberFormatException nfe)
            {
                throw iae;
            }
            if(time < 0L)
                time = 0L;
            date = new Date(time * 1000L);
        }
        return date;
    }

    public Enumeration listTrailers()
        throws IOException, ModuleException
    {
        if(!got_trailers)
            getTrailers();
        return Trailers.keys();
    }

    public synchronized byte[] getData()
        throws IOException, ModuleException
    {
        if(!initialized)
            handleResponse();
        if(Data == null)
        {
            try
            {
                readResponseData(inp_stream);
            }
            catch(InterruptedIOException ie)
            {
                throw ie;
            }
            catch(IOException ioe)
            {
                if(GlobalConstants.DebugResp)
                {
                    System.err.println("HResp: (\"" + method + " " + OriginalURI.getPath() + "\")");
                    System.err.print("       ");
                    ((Throwable) (ioe)).printStackTrace();
                }
                try
                {
                    inp_stream.close();
                }
                catch(Exception e) { }
                throw ioe;
            }
            inp_stream.close();
        }
        return Data;
    }

    public synchronized InputStream getInputStream()
        throws IOException, ModuleException
    {
        if(!initialized)
            handleResponse();
        if(Data == null)
        {
            return inp_stream;
        } else
        {
            getData();
            return ((InputStream) (new ByteArrayInputStream(Data)));
        }
    }

    public String toString()
    {
        if(!initialized)
            try
            {
                handleResponse();
            }
            catch(Exception e)
            {
                if(GlobalConstants.DebugResp && !(e instanceof InterruptedIOException))
                {
                    System.err.println("HResp: (\"" + method + " " + OriginalURI.getPath() + "\")");
                    System.err.print("       ");
                    ((Throwable) (e)).printStackTrace();
                }
                return "Failed to read headers: " + e;
            }
        String nl = System.getProperty("line.separator", "\n");
        StringBuffer str = new StringBuffer(Version);
        str.append(' ');
        str.append(StatusCode);
        str.append(' ');
        str.append(ReasonLine);
        str.append(nl);
        if(EffectiveURI != null)
        {
            str.append("Effective-URI: ");
            str.append(((Object) (EffectiveURI)));
            str.append(nl);
        }
        for(Enumeration hdr_list = Headers.keys(); hdr_list.hasMoreElements(); str.append(nl))
        {
            String hdr = (String)hdr_list.nextElement();
            str.append(hdr);
            str.append(": ");
            str.append(Headers.get(hdr));
        }

        return str.toString();
    }

    HTTPClientModule[] getModules()
    {
        return modules;
    }

    synchronized boolean handleResponse()
        throws IOException, ModuleException
    {
        if(initialized)
            return false;
        if(out_stream != null)
        {
            response = out_stream.getResponse();
            response.http_resp = this;
            out_stream = null;
        }
label0:
        do
        {
label1:
            {
                for(int idx = 0; idx < modules.length && !aborted; idx++)
                {
                    try
                    {
                        modules[idx].responsePhase1Handler(response, ((RoRequest) (request)));
                        continue;
                    }
                    catch(RetryException re)
                    {
                        if(!re.restart)
                            throw re;
                    }
                    continue label0;
                }

                for(int idx = 0; idx < modules.length && !aborted; idx++)
                {
                    int sts = modules[idx].responsePhase2Handler(response, request);
                    switch(sts)
                    {
                    case 10: // '\n'
                        continue;

                    case 11: // '\013'
                        idx = -1;
                        continue label0;

                    case 13: // '\r'
                    case 15: // '\017'
                        response.getInputStream().close();
                        if(handle_trailers)
                            invokeTrailerHandlers(true);
                        if(request.internal_subrequest)
                            return true;
                        request.getConnection().handleRequest(request, this, response, true);
                        if(!initialized)
                        {
                            idx = -1;
                            continue label0;
                        }
                        break;

                    case 14: // '\016'
                    case 16: // '\020'
                        response.getInputStream().close();
                        if(handle_trailers)
                            invokeTrailerHandlers(true);
                        if(request.internal_subrequest)
                            return true;
                        request.getConnection().handleRequest(request, this, response, false);
                        idx = -1;
                        continue label0;

                    default:
                        throw new Error("HTTPClient Internal Error: invalid status " + sts + " returned by module " + ((Object) (modules[idx])).getClass().getName());

                    case 12: // '\f'
                        break;
                    }
                    break label1;
                }

                for(int idx = 0; idx < modules.length && !aborted; idx++)
                    modules[idx].responsePhase3Handler(response, ((RoRequest) (request)));

            }
            response.getStatusCode();
            if(!request.internal_subrequest)
                init(response);
            if(handle_trailers)
                invokeTrailerHandlers(false);
            return false;
        } while(true);
    }

    void init(Response resp)
    {
        if(initialized)
        {
            return;
        } else
        {
            StatusCode = resp.StatusCode;
            ReasonLine = resp.ReasonLine;
            Version = resp.Version;
            EffectiveURI = resp.EffectiveURI;
            ContentLength = resp.ContentLength;
            Headers = resp.Headers;
            inp_stream = resp.inp_stream;
            Data = resp.Data;
            initialized = true;
            return;
        }
    }

    void invokeTrailerHandlers(boolean force)
        throws IOException, ModuleException
    {
        if(trailers_handled)
            return;
        if(!force && !initialized)
        {
            handle_trailers = true;
            return;
        }
        for(int idx = 0; idx < modules.length && !aborted; idx++)
            modules[idx].trailerHandler(response, ((RoRequest) (request)));

        trailers_handled = true;
    }

    void markAborted()
    {
        aborted = true;
    }

    private synchronized void getTrailers()
        throws IOException, ModuleException
    {
        if(got_trailers)
            return;
        if(!initialized)
            handleResponse();
        response.getTrailer("Any");
        Trailers = response.Trailers;
        got_trailers = true;
        invokeTrailerHandlers(false);
    }

    private void readResponseData(InputStream inp)
        throws IOException, ModuleException
    {
        if(ContentLength == 0)
            return;
        if(Data == null)
            Data = new byte[0];
        int off = Data.length;
        try
        {
            if(getHeader("Content-Length") != null)
            {
                int rcvd = 0;
                Data = new byte[ContentLength];
                do
                {
                    off += rcvd;
                    rcvd = inp.read(Data, off, ContentLength - off);
                } while(rcvd != -1 && off + rcvd < ContentLength);
            } else
            {
                int inc = 1000;
                int rcvd = 0;
                do
                {
                    off += rcvd;
                    Data = Util.resizeArray(Data, off + inc);
                } while((rcvd = inp.read(Data, off, inc)) != -1);
                Data = Util.resizeArray(Data, off);
            }
        }
        catch(IOException ioe)
        {
            Data = Util.resizeArray(Data, off);
            throw ioe;
        }
        finally
        {
            try
            {
                inp.close();
            }
            catch(IOException ioe) { }
        }
    }

    int getTimeout()
    {
        return timeout;
    }
}
