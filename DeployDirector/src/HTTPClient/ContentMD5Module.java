// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ContentMD5Module.java

package HTTPClient;

import java.io.IOException;
import java.io.PrintStream;

// Referenced classes of package HTTPClient:
//            ParseException, ModuleException, MD5InputStream, VerifyMD5, 
//            HTTPClientModule, GlobalConstants, RoRequest, Response, 
//            Util, Request

class ContentMD5Module
    implements HTTPClientModule, GlobalConstants
{

    ContentMD5Module()
    {
    }

    public int requestHandler(Request req, Response resp[])
    {
        return 0;
    }

    public void responsePhase1Handler(Response response, RoRequest rorequest)
    {
    }

    public int responsePhase2Handler(Response resp, Request req)
    {
        return 10;
    }

    public void responsePhase3Handler(Response resp, RoRequest req)
        throws IOException, ModuleException
    {
        if(req.getMethod().equals("HEAD"))
            return;
        String md5_digest = resp.getHeader("Content-MD5");
        String trailer = resp.getHeader("Trailer");
        boolean md5_tok = false;
        try
        {
            if(trailer != null)
                md5_tok = Util.hasToken(trailer, "Content-MD5");
        }
        catch(ParseException pe)
        {
            throw new ModuleException(((Throwable) (pe)).toString());
        }
        if(md5_digest == null && !md5_tok || resp.getHeader("Transfer-Encoding") != null)
            return;
        if(GlobalConstants.DebugMods)
            if(md5_digest != null)
                System.err.println("CMD5M: Received digest: " + md5_digest + " - pushing md5-check-stream");
            else
                System.err.println("CMD5M: Expecting digest in trailer  - pushing md5-check-stream");
        resp.inp_stream = ((java.io.InputStream) (new MD5InputStream(resp.inp_stream, ((HashVerifier) (new VerifyMD5(((RoResponse) (resp))))))));
    }

    public void trailerHandler(Response response, RoRequest rorequest)
    {
    }
}
