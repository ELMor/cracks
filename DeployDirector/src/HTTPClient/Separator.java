// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CookieModule.java

package HTTPClient;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Panel;

class Separator extends Panel
{

    Separator()
    {
    }

    public void paint(Graphics g)
    {
        int w = ((Component)this).getSize().width;
        int h = ((Component)this).getSize().height / 2;
        g.setColor(Color.darkGray);
        g.drawLine(2, h - 1, w - 2, h - 1);
        g.setColor(Color.white);
        g.drawLine(2, h, w - 2, h);
    }

    public Dimension getMinimumSize()
    {
        return new Dimension(4, 2);
    }
}
