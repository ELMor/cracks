// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Response.java

package HTTPClient;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.PrintStream;
import java.io.SequenceInputStream;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Date;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;

// Referenced classes of package HTTPClient:
//            CIHashtable, URI, ParseException, HttpHeaderElement, 
//            ModuleException, RoResponse, GlobalConstants, Request, 
//            StreamDemultiplexor, NVPair, Util, HTTPConnection, 
//            RespInputStream, HTTPResponse

public final class Response
    implements RoResponse, GlobalConstants
{

    private HTTPConnection connection;
    private StreamDemultiplexor stream_handler;
    HTTPResponse http_resp;
    int timeout;
    public InputStream inp_stream;
    private RespInputStream resp_inp_stream;
    private String method;
    String resource;
    private boolean used_proxy;
    private boolean sent_entity;
    int StatusCode;
    String ReasonLine;
    String Version;
    URI EffectiveURI;
    CIHashtable Headers;
    CIHashtable Trailers;
    int ContentLength;
    int cd_type;
    byte Data[];
    boolean reading_headers;
    boolean got_headers;
    boolean got_trailers;
    private IOException exception;
    boolean final_resp;
    private byte buf[];
    private int buf_pos;
    private StringBuffer hdrs;
    private boolean reading_lines;
    private boolean bol;
    private boolean got_cr;
    boolean trailers_read;
    Request req;
    boolean isFirstResponse;

    Response(Request request, boolean used_proxy, StreamDemultiplexor stream_handler)
        throws IOException
    {
        timeout = 0;
        resp_inp_stream = null;
        StatusCode = 0;
        EffectiveURI = null;
        Headers = new CIHashtable();
        Trailers = new CIHashtable();
        ContentLength = -1;
        cd_type = 1;
        Data = null;
        reading_headers = false;
        got_headers = false;
        got_trailers = false;
        exception = null;
        final_resp = false;
        buf = new byte[7];
        buf_pos = 0;
        hdrs = new StringBuffer(400);
        reading_lines = false;
        bol = true;
        got_cr = false;
        trailers_read = false;
        req = null;
        isFirstResponse = false;
        connection = request.getConnection();
        method = request.getMethod();
        resource = request.getRequestURI();
        this.used_proxy = used_proxy;
        this.stream_handler = stream_handler;
        sent_entity = request.getData() != null;
        stream_handler.register(this, request);
        resp_inp_stream = stream_handler.getStream(this);
        inp_stream = ((InputStream) (resp_inp_stream));
    }

    Response(Request request, InputStream is)
        throws IOException
    {
        timeout = 0;
        resp_inp_stream = null;
        StatusCode = 0;
        EffectiveURI = null;
        Headers = new CIHashtable();
        Trailers = new CIHashtable();
        ContentLength = -1;
        cd_type = 1;
        Data = null;
        reading_headers = false;
        got_headers = false;
        got_trailers = false;
        exception = null;
        final_resp = false;
        buf = new byte[7];
        buf_pos = 0;
        hdrs = new StringBuffer(400);
        reading_lines = false;
        bol = true;
        got_cr = false;
        trailers_read = false;
        req = null;
        isFirstResponse = false;
        connection = request.getConnection();
        method = request.getMethod();
        resource = request.getRequestURI();
        used_proxy = false;
        stream_handler = null;
        sent_entity = request.getData() != null;
        inp_stream = is;
    }

    public Response(String version, int status, String reason, NVPair headers[], byte data[], InputStream is, int cont_len)
    {
        timeout = 0;
        resp_inp_stream = null;
        StatusCode = 0;
        EffectiveURI = null;
        Headers = new CIHashtable();
        Trailers = new CIHashtable();
        ContentLength = -1;
        cd_type = 1;
        Data = null;
        reading_headers = false;
        got_headers = false;
        got_trailers = false;
        exception = null;
        final_resp = false;
        buf = new byte[7];
        buf_pos = 0;
        hdrs = new StringBuffer(400);
        reading_lines = false;
        bol = true;
        got_cr = false;
        trailers_read = false;
        req = null;
        isFirstResponse = false;
        Version = version;
        StatusCode = status;
        ReasonLine = reason;
        if(headers != null)
        {
            for(int idx = 0; idx < headers.length; idx++)
                setHeader(headers[idx].getName(), headers[idx].getValue());

        }
        if(data != null)
            Data = data;
        else
        if(is == null)
        {
            Data = new byte[0];
        } else
        {
            inp_stream = is;
            ContentLength = cont_len;
        }
        got_headers = true;
        got_trailers = true;
    }

    public final int getStatusCode()
        throws IOException
    {
        if(!got_headers)
            getHeaders(true);
        return StatusCode;
    }

    public final String getReasonLine()
        throws IOException
    {
        if(!got_headers)
            getHeaders(true);
        return ReasonLine;
    }

    public final String getVersion()
        throws IOException
    {
        if(!got_headers)
            getHeaders(true);
        return Version;
    }

    int getContinue()
        throws IOException
    {
        getHeaders(false);
        return StatusCode;
    }

    public final URI getEffectiveURI()
        throws IOException
    {
        if(!got_headers)
            getHeaders(true);
        return EffectiveURI;
    }

    public void setEffectiveURI(URI final_uri)
    {
        EffectiveURI = final_uri;
    }

    /**
     * @deprecated Method getEffectiveURL is deprecated
     */

    public final URL getEffectiveURL()
        throws IOException
    {
        return getEffectiveURI().toURL();
    }

    /**
     * @deprecated Method setEffectiveURL is deprecated
     */

    public void setEffectiveURL(URL final_url)
    {
        try
        {
            setEffectiveURI(new URI(final_url));
        }
        catch(ParseException pe)
        {
            throw new Error(((Throwable) (pe)).toString());
        }
    }

    public String getHeader(String hdr)
        throws IOException
    {
        if(!got_headers)
            getHeaders(true);
        return (String)Headers.get(hdr.trim());
    }

    public int getHeaderAsInt(String hdr)
        throws IOException, NumberFormatException
    {
        return Integer.parseInt(getHeader(hdr));
    }

    public Date getHeaderAsDate(String hdr)
        throws IOException, IllegalArgumentException
    {
        String raw_date = getHeader(hdr);
        if(raw_date == null)
            return null;
        if(raw_date.toUpperCase().indexOf("GMT") == -1)
            raw_date = raw_date + " GMT";
        Date date;
        try
        {
            date = new Date(raw_date);
        }
        catch(IllegalArgumentException iae)
        {
            long time;
            try
            {
                time = Long.parseLong(raw_date);
            }
            catch(NumberFormatException nfe)
            {
                throw iae;
            }
            if(time < 0L)
                time = 0L;
            date = new Date(time * 1000L);
        }
        return date;
    }

    public void setHeader(String header, String value)
    {
        Headers.put(header.trim(), ((Object) (value.trim())));
    }

    public void deleteHeader(String header)
    {
        Headers.remove(header.trim());
    }

    public String getTrailer(String trailer)
        throws IOException
    {
        if(!got_trailers)
            getTrailers();
        return (String)Trailers.get(trailer.trim());
    }

    public int getTrailerAsInt(String trailer)
        throws IOException, NumberFormatException
    {
        return Integer.parseInt(getTrailer(trailer));
    }

    public Date getTrailerAsDate(String trailer)
        throws IOException, IllegalArgumentException
    {
        String raw_date = getTrailer(trailer);
        if(raw_date == null)
            return null;
        if(raw_date.toUpperCase().indexOf("GMT") == -1)
            raw_date = raw_date + " GMT";
        Date date;
        try
        {
            date = new Date(raw_date);
        }
        catch(IllegalArgumentException iae)
        {
            long time;
            try
            {
                time = Long.parseLong(raw_date);
            }
            catch(NumberFormatException nfe)
            {
                throw iae;
            }
            if(time < 0L)
                time = 0L;
            date = new Date(time * 1000L);
        }
        return date;
    }

    public void setTrailer(String trailer, String value)
    {
        Trailers.put(trailer.trim(), ((Object) (value.trim())));
    }

    public void deleteTrailer(String trailer)
    {
        Trailers.remove(trailer.trim());
    }

    public synchronized byte[] getData()
        throws IOException
    {
        if(!got_headers)
            getHeaders(true);
        if(Data == null)
        {
            try
            {
                readResponseData(inp_stream);
            }
            catch(InterruptedIOException ie)
            {
                throw ie;
            }
            catch(IOException ioe)
            {
                if(GlobalConstants.DebugResp)
                {
                    System.err.print("Resp:  (" + ((Object) (inp_stream)).hashCode() + ") (" + Thread.currentThread() + ")");
                    ((Throwable) (ioe)).printStackTrace();
                }
                try
                {
                    inp_stream.close();
                }
                catch(Exception e) { }
                throw ioe;
            }
            inp_stream.close();
        }
        return Data;
    }

    public synchronized InputStream getInputStream()
        throws IOException
    {
        if(!got_headers)
            getHeaders(true);
        if(Data == null)
            return inp_stream;
        else
            return ((InputStream) (new ByteArrayInputStream(Data)));
    }

    public synchronized boolean hasEntity()
        throws IOException
    {
        if(!got_headers)
            getHeaders(true);
        return cd_type != 2;
    }

    private synchronized void getHeaders(boolean skip_cont)
        throws IOException
    {
        if(got_headers)
            return;
        if(exception != null)
            throw (IOException)((Throwable) (exception)).fillInStackTrace();
        reading_headers = true;
        try
        {
            do
            {
                ((Hashtable) (Headers)).clear();
                String headers = readResponseHeaders(inp_stream);
                parseResponseHeaders(headers);
            } while(StatusCode == 100 && skip_cont || StatusCode > 101 && StatusCode < 200);
        }
        catch(IOException ioe)
        {
            if(!(ioe instanceof InterruptedIOException))
                exception = ioe;
            if(ioe instanceof ProtocolException)
            {
                cd_type = 3;
                if(stream_handler != null)
                    stream_handler.markForClose(this);
            }
            throw ioe;
        }
        finally
        {
            reading_headers = false;
        }
        if(StatusCode == 100)
            return;
        int cont_len = -1;
        String cl_hdr = (String)Headers.get("Content-Length");
        if(cl_hdr != null)
            try
            {
                cont_len = Integer.parseInt(cl_hdr);
                if(cont_len < 0)
                    throw new NumberFormatException();
            }
            catch(NumberFormatException nfe)
            {
                throw new ProtocolException("Invalid Content-length header received: " + cl_hdr);
            }
        boolean te_chunked = false;
        boolean te_is_identity = true;
        boolean ct_mpbr = false;
        Vector te_hdr = null;
        try
        {
            te_hdr = Util.parseHeader((String)Headers.get("Transfer-Encoding"));
        }
        catch(ParseException pe) { }
        if(te_hdr != null)
        {
            te_chunked = ((HttpHeaderElement)te_hdr.lastElement()).getName().equalsIgnoreCase("chunked");
            for(int idx = 0; idx < te_hdr.size(); idx++)
                if(((HttpHeaderElement)te_hdr.elementAt(idx)).getName().equalsIgnoreCase("identity"))
                    te_hdr.removeElementAt(idx--);
                else
                    te_is_identity = false;

        }
        String hdr;
        try
        {
            if((hdr = (String)Headers.get("Content-Type")) != null)
            {
                Vector phdr = Util.parseHeader(hdr);
                ct_mpbr = phdr.contains(((Object) (new HttpHeaderElement("multipart/byteranges")))) || phdr.contains(((Object) (new HttpHeaderElement("multipart/x-byteranges"))));
            }
        }
        catch(ParseException pe) { }
        if(StatusCode < 200 || StatusCode == 204 || StatusCode == 205 || StatusCode == 304)
            cd_type = 2;
        else
        if(te_chunked)
        {
            cd_type = 5;
            te_hdr.removeElementAt(te_hdr.size() - 1);
            if(te_hdr.size() > 0)
                setHeader("Transfer-Encoding", Util.assembleHeader(te_hdr));
            else
                deleteHeader("Transfer-Encoding");
        } else
        if(cont_len != -1 && te_is_identity)
            cd_type = 4;
        else
        if(ct_mpbr && te_is_identity)
            cd_type = 6;
        else
        if(!method.equals("HEAD"))
        {
            cd_type = 3;
            if(stream_handler != null)
                stream_handler.markForClose(this);
            if(Version.equals("HTTP/0.9"))
            {
                inp_stream = ((InputStream) (new SequenceInputStream(((InputStream) (new ByteArrayInputStream(Data))), inp_stream)));
                Data = null;
            }
        }
        if(cd_type == 4)
            ContentLength = cont_len;
        else
            deleteHeader("Content-Length");
        if(method.equals("HEAD"))
            cd_type = 2;
        if(cd_type == 2)
        {
            ContentLength = 0;
            Data = new byte[0];
            inp_stream.close();
        }
        if(GlobalConstants.DebugResp)
            System.err.println("Resp:  Response entity delimiter: " + (cd_type != 2 ? cd_type != 3 ? cd_type != 4 ? cd_type != 5 ? cd_type != 6 ? "???" : "Multipart" : "Chunked" : "Content-Length" : "Close" : "No Entity") + " (" + ((Object) (inp_stream)).hashCode() + ") (" + Thread.currentThread() + ")");
        if(connection.ServerProtocolVersion >= 0x10001)
        {
            deleteHeader("Proxy-Connection");
        } else
        {
            if(connection.getProxyHost() != null)
                deleteHeader("Connection");
            else
                deleteHeader("Proxy-Connection");
            Vector pco;
            try
            {
                pco = Util.parseHeader((String)Headers.get("Connection"));
            }
            catch(ParseException pe)
            {
                pco = null;
            }
            if(pco != null)
            {
                for(int idx = 0; idx < pco.size(); idx++)
                {
                    String name = ((HttpHeaderElement)pco.elementAt(idx)).getName();
                    if(!name.equalsIgnoreCase("keep-alive"))
                    {
                        pco.removeElementAt(idx);
                        deleteHeader(name);
                        idx--;
                    }
                }

                if(pco.size() > 0)
                    setHeader("Connection", Util.assembleHeader(pco));
                else
                    deleteHeader("Connection");
            }
            try
            {
                pco = Util.parseHeader((String)Headers.get("Proxy-Connection"));
            }
            catch(ParseException pe)
            {
                pco = null;
            }
            if(pco != null)
            {
                for(int idx = 0; idx < pco.size(); idx++)
                {
                    String name = ((HttpHeaderElement)pco.elementAt(idx)).getName();
                    if(!name.equalsIgnoreCase("keep-alive"))
                    {
                        pco.removeElementAt(idx);
                        deleteHeader(name);
                        idx--;
                    }
                }

                if(pco.size() > 0)
                    setHeader("Proxy-Connection", Util.assembleHeader(pco));
                else
                    deleteHeader("Proxy-Connection");
            }
        }
        got_headers = true;
        if(isFirstResponse && !connection.handleFirstRequest(req, this))
        {
            Response resp;
            try
            {
                resp = connection.sendRequest(req, timeout);
            }
            catch(ModuleException me)
            {
                throw new IOException(((Throwable) (me)).toString());
            }
            resp.getVersion();
            StatusCode = resp.StatusCode;
            ReasonLine = resp.ReasonLine;
            Version = resp.Version;
            EffectiveURI = resp.EffectiveURI;
            ContentLength = resp.ContentLength;
            Headers = resp.Headers;
            inp_stream = resp.inp_stream;
            Data = resp.Data;
            req = null;
        }
    }

    private String readResponseHeaders(InputStream inp)
        throws IOException
    {
        if(GlobalConstants.DebugResp)
            if(buf_pos == 0)
                System.err.println("Resp:  Reading Response headers " + ((Object) (inp_stream)).hashCode() + " (" + Thread.currentThread() + ")");
            else
                System.err.println("Resp:  Resuming reading Response headers " + ((Object) (inp_stream)).hashCode() + " (" + Thread.currentThread() + ")");
        if(!reading_lines)
        {
            try
            {
                if(buf_pos == 0)
                {
                    int c;
                    do
                        if((c = inp.read()) == -1)
                            throw new EOFException("Encountered premature EOF while reading Version");
                    while(Character.isSpace((char)(c & 0xff)));
                    buf[0] = (byte)(c & 0xff);
                    buf_pos = 1;
                }
                int got;
                for(; buf_pos < buf.length; buf_pos += got)
                {
                    got = inp.read(buf, buf_pos, buf.length - buf_pos);
                    if(got == -1)
                        throw new EOFException("Encountered premature EOF while reading Version");
                }

            }
            catch(EOFException eof)
            {
                if(GlobalConstants.DebugResp)
                {
                    System.err.print("Resp:  (" + ((Object) (inp_stream)).hashCode() + ") (" + Thread.currentThread() + ")");
                    ((Throwable) (eof)).printStackTrace();
                }
                throw eof;
            }
            for(int idx = 0; idx < buf.length; idx++)
                hdrs.append((char)buf[idx]);

            reading_lines = true;
        }
        if(hdrs.toString().startsWith("HTTP/") || hdrs.toString().startsWith("HTTP "))
            readLines(inp);
        buf_pos = 0;
        reading_lines = false;
        bol = true;
        got_cr = false;
        String tmp = hdrs.toString();
        hdrs.setLength(0);
        return tmp;
    }

    void readTrailers(InputStream inp)
        throws IOException
    {
        try
        {
            readLines(inp);
            trailers_read = true;
        }
        catch(IOException ioe)
        {
            if(!(ioe instanceof InterruptedIOException))
                exception = ioe;
            throw ioe;
        }
    }

    private void readLines(InputStream inp)
        throws IOException
    {
        do
        {
            int b = inp.read();
            switch(b)
            {
            case -1: 
                throw new EOFException("Encountered premature EOF while reading headers:\n" + hdrs);

            case 13: // '\r'
                got_cr = true;
                continue;

            case 10: // '\n'
                if(!bol)
                {
                    hdrs.append('\n');
                    bol = true;
                    got_cr = false;
                } else
                {
                    return;
                }
                continue;

            case 9: // '\t'
            case 32: // ' '
                if(bol)
                {
                    hdrs.setCharAt(hdrs.length() - 1, ' ');
                    bol = false;
                    continue;
                }
                // fall through

            default:
                if(got_cr)
                {
                    hdrs.append('\r');
                    got_cr = false;
                }
                hdrs.append((char)(b & 0xff));
                bol = false;
                break;
            }
        } while(true);
    }

    private void parseResponseHeaders(String headers)
        throws ProtocolException
    {
        String sts_line = null;
        StringTokenizer lines = new StringTokenizer(headers, "\r\n");
        if(GlobalConstants.DebugResp)
            System.err.println("Resp:  Parsing Response headers from Request \"" + method + " " + resource + "\":  (" + ((Object) (inp_stream)).hashCode() + ") (" + Thread.currentThread() + ")\n\n" + headers);
        if(!headers.regionMatches(true, 0, "HTTP/", 0, 5) && !headers.regionMatches(true, 0, "HTTP ", 0, 5))
        {
            Version = "HTTP/0.9";
            StatusCode = 200;
            ReasonLine = "OK";
            Data = new byte[headers.length()];
            headers.getBytes(0, headers.length(), Data, 0);
            return;
        }
        StringTokenizer elem;
        try
        {
            sts_line = lines.nextToken();
            elem = new StringTokenizer(sts_line, " \t");
            Version = elem.nextToken();
            StatusCode = Integer.valueOf(elem.nextToken()).intValue();
            if(Version.equalsIgnoreCase("HTTP"))
                Version = "HTTP/1.0";
        }
        catch(NoSuchElementException e)
        {
            throw new ProtocolException("Invalid HTTP status line received: " + sts_line);
        }
        try
        {
            ReasonLine = elem.nextToken("").trim();
        }
        catch(NoSuchElementException e)
        {
            ReasonLine = "";
        }
        if(StatusCode >= 300 && sent_entity && stream_handler != null)
            stream_handler.markForClose(this);
        parseHeaderFields(lines, Headers);
        if(Headers.get("Trailer") != null && resp_inp_stream != null)
            resp_inp_stream.dontTruncate();
        int vers;
        if(Version.equalsIgnoreCase("HTTP/0.9") || Version.equalsIgnoreCase("HTTP/1.0"))
            vers = 0;
        else
            vers = 1;
        try
        {
            String con = (String)Headers.get("Connection");
            String pcon = (String)Headers.get("Proxy-Connection");
            if((vers == 1 && con != null && Util.hasToken(con, "close") || vers == 0 && (used_proxy || con == null || !Util.hasToken(con, "keep-alive")) && (!used_proxy || pcon == null || !Util.hasToken(pcon, "keep-alive"))) && stream_handler != null)
                stream_handler.markForClose(this);
        }
        catch(ParseException pe) { }
    }

    private synchronized void getTrailers()
        throws IOException
    {
        if(got_trailers)
            return;
        if(exception != null)
            throw (IOException)((Throwable) (exception)).fillInStackTrace();
        if(GlobalConstants.DebugResp)
            System.err.println("Resp:  Reading Response trailers " + ((Object) (inp_stream)).hashCode() + " (" + Thread.currentThread() + ")");
        try
        {
            if(!trailers_read && resp_inp_stream != null)
                resp_inp_stream.readAll(timeout);
            if(trailers_read)
            {
                if(GlobalConstants.DebugResp)
                    System.err.println("Resp:  Parsing Response trailers from Request \"" + method + " " + resource + "\":  (" + ((Object) (inp_stream)).hashCode() + ") (" + Thread.currentThread() + ")\n\n" + hdrs);
                parseHeaderFields(new StringTokenizer(hdrs.toString(), "\r\n"), Trailers);
            }
        }
        finally
        {
            got_trailers = true;
        }
    }

    private void parseHeaderFields(StringTokenizer lines, CIHashtable list)
        throws ProtocolException
    {
        while(lines.hasMoreTokens()) 
        {
            String hdr = lines.nextToken();
            int sep = hdr.indexOf(':');
            if(sep == -1)
                sep = hdr.indexOf(' ');
            if(sep == -1)
                throw new ProtocolException("Invalid HTTP header received: " + hdr);
            String hdr_name = hdr.substring(0, sep).trim();
            int len = hdr.length();
            for(sep++; sep < len && Character.isSpace(hdr.charAt(sep)); sep++);
            String hdr_value = hdr.substring(sep);
            String old_value = (String)list.get(hdr_name);
            if(old_value == null)
                list.put(hdr_name, ((Object) (hdr_value)));
            else
            if(!hdr_name.equalsIgnoreCase("Content-Length"))
                list.put(hdr_name, ((Object) (old_value + ", " + hdr_value)));
        }
    }

    private void readResponseData(InputStream inp)
        throws IOException
    {
        if(ContentLength == 0)
            return;
        if(Data == null)
            Data = new byte[0];
        int off = Data.length;
        try
        {
            if(getHeader("Content-Length") != null)
            {
                int rcvd = 0;
                Data = new byte[ContentLength];
                do
                {
                    off += rcvd;
                    rcvd = inp.read(Data, off, ContentLength - off);
                } while(rcvd != -1 && off + rcvd < ContentLength);
            } else
            {
                int inc = 1000;
                int rcvd = 0;
                do
                {
                    off += rcvd;
                    Data = Util.resizeArray(Data, off + inc);
                } while((rcvd = inp.read(Data, off, inc)) != -1);
                Data = Util.resizeArray(Data, off);
            }
        }
        catch(IOException ioe)
        {
            Data = Util.resizeArray(Data, off);
            throw ioe;
        }
        finally
        {
            try
            {
                inp.close();
            }
            catch(IOException ioe) { }
        }
    }

    void markAsFirstResponse(Request req)
    {
        this.req = req;
        isFirstResponse = true;
    }
}
