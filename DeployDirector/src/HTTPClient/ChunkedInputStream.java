// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ChunkedInputStream.java

package HTTPClient;

import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

// Referenced classes of package HTTPClient:
//            ParseException, Request, Response, Codecs

class ChunkedInputStream extends FilterInputStream
{

    byte one[];
    private int chunk_len;
    private boolean eof;

    ChunkedInputStream(InputStream is)
    {
        super(is);
        one = new byte[1];
        chunk_len = -1;
        eof = false;
    }

    public synchronized int read()
        throws IOException
    {
        int b = read(one, 0, 1);
        if(b == 1)
            return one[0] & 0xff;
        else
            return -1;
    }

    public synchronized int read(byte buf[], int off, int len)
        throws IOException
    {
        if(eof)
            return -1;
        if(chunk_len == -1)
            try
            {
                chunk_len = Codecs.getChunkLength(super.in);
            }
            catch(ParseException pe)
            {
                throw new IOException(((Throwable) (pe)).toString());
            }
        if(chunk_len > 0)
        {
            if(len > chunk_len)
                len = chunk_len;
            int rcvd = super.in.read(buf, off, len);
            if(rcvd == -1)
                throw new EOFException("Premature EOF encountered");
            chunk_len -= rcvd;
            if(chunk_len == 0)
            {
                super.in.read();
                super.in.read();
                chunk_len = -1;
            }
            return rcvd;
        } else
        {
            Request dummy = new Request(((HTTPConnection) (null)), ((String) (null)), ((String) (null)), ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false);
            (new Response(dummy, ((InputStream) (null)))).readTrailers(super.in);
            eof = true;
            return -1;
        }
    }

    public synchronized long skip(long num)
        throws IOException
    {
        byte tmp[] = new byte[(int)num];
        int got = read(tmp, 0, (int)num);
        if(got > 0)
            return (long)got;
        else
            return 0L;
    }

    public synchronized int available()
        throws IOException
    {
        if(eof)
            return 0;
        if(chunk_len != -1)
            return chunk_len + super.in.available();
        else
            return super.in.available();
    }
}
