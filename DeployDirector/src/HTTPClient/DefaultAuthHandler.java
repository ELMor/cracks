// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   DefaultAuthHandler.java

package HTTPClient;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.StringTokenizer;
import java.util.Vector;

// Referenced classes of package HTTPClient:
//            AuthSchemeNotImplException, BasicAuthBox, AuthorizationInfo, NVPair,
//            ParseException, HttpHeaderElement, MD5, URI,
//            VerifyDigest, MD5InputStream, VerifyRspAuth, AuthorizationHandler,
//            GlobalConstants, RoRequest, Codecs, HTTPConnection,
//            Response, Util, RoResponse

class DefaultAuthHandler
    implements AuthorizationHandler, GlobalConstants
{

    private static final byte NUL[] = new byte[0];
    private static final int DI_A1 = 0;
    private static final int DI_A1S = 1;
    private static final int DI_QOP = 2;
    private static byte digest_secret[] = null;
    private BasicAuthBox inp;

    DefaultAuthHandler()
    {
        inp = null;
    }

    public AuthorizationInfo fixupAuthInfo(AuthorizationInfo info, RoRequest req, AuthorizationInfo challenge, RoResponse resp)
        throws AuthSchemeNotImplException
    {
        if(info.getScheme().equalsIgnoreCase("Basic") || info.getScheme().equalsIgnoreCase("SOCKS5"))
            return info;
        if(!info.getScheme().equalsIgnoreCase("Digest"))
            throw new AuthSchemeNotImplException(info.getScheme());
        if(GlobalConstants.DebugAuth)
            System.err.println("Auth:  fixing up Authorization for host " + info.getHost() + ":" + info.getPort() + "; scheme: " + info.getScheme() + "; realm: " + info.getRealm());
        return digest_fixup(info, req, challenge, resp);
    }

    public AuthorizationInfo getAuthorization(AuthorizationInfo challenge, RoRequest req, RoResponse resp)
        throws AuthSchemeNotImplException
    {
        if(GlobalConstants.DebugAuth)
            System.err.println("Auth:  Requesting Authorization for host " + challenge.getHost() + ":" + challenge.getPort() + "; scheme: " + challenge.getScheme() + "; realm: " + challenge.getRealm());
        if(!challenge.getScheme().equalsIgnoreCase("Basic") && !challenge.getScheme().equalsIgnoreCase("Digest") && !challenge.getScheme().equalsIgnoreCase("SOCKS5"))
            throw new AuthSchemeNotImplException(challenge.getScheme());
        AuthorizationInfo cred;
        if(challenge.getScheme().equalsIgnoreCase("Digest"))
        {
            cred = digest_check_stale(challenge, req, resp);
            if(cred != null)
                return cred;
        }
        if(!req.allowUI())
            return null;
        if(inp == null)
            synchronized(((Object)this).getClass())
            {
                if(inp == null)
                    inp = new BasicAuthBox();
            }
        NVPair answer;
        if(challenge.getScheme().equalsIgnoreCase("basic") || challenge.getScheme().equalsIgnoreCase("Digest"))
            answer = inp.getInput("Enter username and password for realm `" + challenge.getRealm() + "'", "on host " + challenge.getHost() + ":" + challenge.getPort(), "Authentication Scheme: " + challenge.getScheme());
        else
            answer = inp.getInput("Enter username and password for SOCKS server on host ", challenge.getHost(), "Authentication Method: username/password");
        if(answer == null)
            return null;
        if(challenge.getScheme().equalsIgnoreCase("basic"))
            cred = new AuthorizationInfo(challenge.getHost(), challenge.getPort(), challenge.getScheme(), challenge.getRealm(), Codecs.base64Encode(answer.getName() + ":" + answer.getValue()));
        else
        if(challenge.getScheme().equalsIgnoreCase("Digest"))
        {
            cred = digest_gen_auth_info(challenge.getHost(), challenge.getPort(), challenge.getRealm(), answer.getName(), answer.getValue(), req.getConnection().getContext());
            cred = digest_fixup(cred, req, challenge, ((RoResponse) (null)));
        } else
        {
            NVPair upwd[] = {
                answer
            };
            cred = new AuthorizationInfo(challenge.getHost(), challenge.getPort(), challenge.getScheme(), challenge.getRealm(), upwd, ((Object) (null)));
        }
        answer = null;
        System.gc();
        if(GlobalConstants.DebugAuth)
            System.err.println("Auth:  Got Authorization");
        return cred;
    }

    public void handleAuthHeaders(Response resp, RoRequest req, AuthorizationInfo prev, AuthorizationInfo prxy)
        throws IOException
    {
        String auth_info = resp.getHeader("Authentication-Info");
        String prxy_info = resp.getHeader("Proxy-Authentication-Info");
        if(auth_info == null && prev != null && hasParam(prev.getParams(), "qop", "auth-int"))
            auth_info = "";
        if(prxy_info == null && prxy != null && hasParam(prxy.getParams(), "qop", "auth-int"))
            prxy_info = "";
        try
        {
            handleAuthInfo(auth_info, "Authentication-Info", prev, resp, req, true);
            handleAuthInfo(prxy_info, "Proxy-Authentication-Info", prxy, resp, req, true);
        }
        catch(ParseException pe)
        {
            throw new IOException(((Throwable) (pe)).toString());
        }
    }

    public void handleAuthTrailers(Response resp, RoRequest req, AuthorizationInfo prev, AuthorizationInfo prxy)
        throws IOException
    {
        String auth_info = resp.getTrailer("Authentication-Info");
        String prxy_info = resp.getTrailer("Proxy-Authentication-Info");
        try
        {
            handleAuthInfo(auth_info, "Authentication-Info", prev, resp, req, false);
            handleAuthInfo(prxy_info, "Proxy-Authentication-Info", prxy, resp, req, false);
        }
        catch(ParseException pe)
        {
            throw new IOException(((Throwable) (pe)).toString());
        }
    }

    private static void handleAuthInfo(String auth_info, String hdr_name, AuthorizationInfo prev, Response resp, RoRequest req, boolean in_headers)
        throws ParseException, IOException
    {
        if(auth_info == null)
            return;
        Vector pai = Util.parseHeader(auth_info);
        HttpHeaderElement elem;
        if(handle_nextnonce(prev, req, elem = Util.getElement(pai, "nextnonce")))
            pai.removeElement(((Object) (elem)));
        if(handle_discard(prev, req, elem = Util.getElement(pai, "discard")))
            pai.removeElement(((Object) (elem)));
        if(in_headers)
        {
            HttpHeaderElement qop = null;
            if(pai != null && (qop = Util.getElement(pai, "qop")) != null && qop.getValue() != null)
                handle_rspauth(prev, resp, req, pai, hdr_name);
            else
            if(prev != null && (Util.hasToken(resp.getHeader("Trailer"), hdr_name) && hasParam(prev.getParams(), "qop", ((String) (null))) || hasParam(prev.getParams(), "qop", "auth-int")))
                handle_rspauth(prev, resp, req, ((Vector) (null)), hdr_name);
            else
            if(pai != null && qop == null && pai.contains(((Object) (new HttpHeaderElement("digest")))) || Util.hasToken(resp.getHeader("Trailer"), hdr_name) && prev != null && !hasParam(prev.getParams(), "qop", ((String) (null))))
                handle_digest(prev, resp, req, hdr_name);
        }
        if(pai.size() > 0)
            resp.setHeader(hdr_name, Util.assembleHeader(pai));
        else
            resp.deleteHeader(hdr_name);
    }

    private static final boolean hasParam(NVPair params[], String name, String val)
    {
        for(int idx = 0; idx < params.length; idx++)
            if(params[idx].getName().equalsIgnoreCase(name) && (val == null || params[idx].getValue().equalsIgnoreCase(val)))
                return true;

        return false;
    }

    private static AuthorizationInfo digest_gen_auth_info(String host, int port, String realm, String user, String pass, Object context)
    {
        String A1 = user + ":" + realm + ":" + pass;
        String info[] = {
            (new MD5(((Object) (A1)))).asHex(), null, null
        };
        AuthorizationInfo prev = AuthorizationInfo.getAuthorization(host, port, "Digest", realm, context);
        NVPair params[];
        if(prev == null)
        {
            params = new NVPair[4];
            params[0] = new NVPair("username", user);
            params[1] = new NVPair("uri", "");
            params[2] = new NVPair("nonce", "");
            params[3] = new NVPair("response", "");
        } else
        {
            params = prev.getParams();
            for(int idx = 0; idx < params.length; idx++)
            {
                if(!params[idx].getName().equalsIgnoreCase("username"))
                    continue;
                params[idx] = new NVPair("username", user);
                break;
            }

        }
        return new AuthorizationInfo(host, port, "Digest", realm, params, ((Object) (info)));
    }

    private static AuthorizationInfo digest_fixup(AuthorizationInfo info, RoRequest req, AuthorizationInfo challenge, RoResponse resp)
        throws AuthSchemeNotImplException
    {
        int ch_domain = -1;
        int ch_nonce = -1;
        int ch_alg = -1;
        int ch_opaque = -1;
        int ch_stale = -1;
        int ch_dreq = -1;
        int ch_qop = -1;
        NVPair ch_params[] = null;
        if(challenge != null)
        {
            ch_params = challenge.getParams();
            for(int idx = 0; idx < ch_params.length; idx++)
            {
                String name = ch_params[idx].getName().toLowerCase();
                if(name.equals("domain"))
                    ch_domain = idx;
                else
                if(name.equals("nonce"))
                    ch_nonce = idx;
                else
                if(name.equals("opaque"))
                    ch_opaque = idx;
                else
                if(name.equals("algorithm"))
                    ch_alg = idx;
                else
                if(name.equals("stale"))
                    ch_stale = idx;
                else
                if(name.equals("digest-required"))
                    ch_dreq = idx;
                else
                if(name.equals("qop"))
                    ch_qop = idx;
            }

        }
        int uri = -1;
        int user = -1;
        int alg = -1;
        int response = -1;
        int nonce = -1;
        int cnonce = -1;
        int nc = -1;
        int opaque = -1;
        int digest = -1;
        int dreq = -1;
        int qop = -1;
        NVPair params[];
        String extra[];
        synchronized(info)
        {
            params = info.getParams();
            for(int idx = 0; idx < params.length; idx++)
            {
                String name = params[idx].getName().toLowerCase();
                if(name.equals("uri"))
                    uri = idx;
                else
                if(name.equals("username"))
                    user = idx;
                else
                if(name.equals("algorithm"))
                    alg = idx;
                else
                if(name.equals("nonce"))
                    nonce = idx;
                else
                if(name.equals("cnonce"))
                    cnonce = idx;
                else
                if(name.equals("nc"))
                    nc = idx;
                else
                if(name.equals("response"))
                    response = idx;
                else
                if(name.equals("opaque"))
                    opaque = idx;
                else
                if(name.equals("digest"))
                    digest = idx;
                else
                if(name.equals("digest-required"))
                    dreq = idx;
                else
                if(name.equals("qop"))
                    qop = idx;
            }

            extra = (String[])info.getExtraInfo();
            if(alg != -1 && !params[alg].getValue().equalsIgnoreCase("MD5") && !params[alg].getValue().equalsIgnoreCase("MD5-sess"))
                throw new AuthSchemeNotImplException("Digest auth scheme: Algorithm " + params[alg].getValue() + " not implemented");
            if(ch_alg != -1 && !ch_params[ch_alg].getValue().equalsIgnoreCase("MD5") && !ch_params[ch_alg].getValue().equalsIgnoreCase("MD5-sess"))
                throw new AuthSchemeNotImplException("Digest auth scheme: Algorithm " + ch_params[ch_alg].getValue() + " not implemented");
            params[uri] = new NVPair("uri", req.getRequestURI());
            String old_nonce = params[nonce].getValue();
            if(ch_nonce != -1 && !old_nonce.equals(((Object) (ch_params[ch_nonce].getValue()))))
                params[nonce] = ch_params[ch_nonce];
            if(ch_opaque != -1)
            {
                if(opaque == -1)
                {
                    params = Util.resizeArray(params, params.length + 1);
                    opaque = params.length - 1;
                }
                params[opaque] = ch_params[ch_opaque];
            }
            if(ch_alg != -1)
            {
                if(alg == -1)
                {
                    params = Util.resizeArray(params, params.length + 1);
                    alg = params.length - 1;
                }
                params[alg] = ch_params[ch_alg];
            }
            if(ch_qop != -1 || ch_alg != -1 && ch_params[ch_alg].getValue().equalsIgnoreCase("MD5-sess"))
            {
                if(cnonce == -1)
                {
                    params = Util.resizeArray(params, params.length + 1);
                    cnonce = params.length - 1;
                }
                if(digest_secret == null)
                    digest_secret = gen_random_bytes(20);
                long l_time = System.currentTimeMillis();
                byte time[] = new byte[8];
                time[0] = (byte)(int)(l_time & 255L);
                time[1] = (byte)(int)(l_time >> 8 & 255L);
                time[2] = (byte)(int)(l_time >> 16 & 255L);
                time[3] = (byte)(int)(l_time >> 24 & 255L);
                time[4] = (byte)(int)(l_time >> 32 & 255L);
                time[5] = (byte)(int)(l_time >> 40 & 255L);
                time[6] = (byte)(int)(l_time >> 48 & 255L);
                time[7] = (byte)(int)(l_time >> 56 & 255L);
                MD5 hash = new MD5(((Object) (digest_secret)));
                hash.Update(time);
                params[cnonce] = new NVPair("cnonce", hash.asHex());
            }
            if(ch_qop != -1)
            {
                if(qop == -1)
                {
                    params = Util.resizeArray(params, params.length + 1);
                    qop = params.length - 1;
                }
                extra[2] = ch_params[ch_qop].getValue();
                String qops[] = splitList(extra[2], ",");
                String p = null;
                for(int idx = 0; idx < qops.length; idx++)
                {
                    if(qops[idx].equalsIgnoreCase("auth-int") && (req.getStream() == null || req.getConnection().ServProtVersKnown && req.getConnection().ServerProtocolVersion >= 0x10001))
                    {
                        p = "auth-int";
                        break;
                    }
                    if(qops[idx].equalsIgnoreCase("auth"))
                        p = "auth";
                }

                if(p == null)
                {
                    for(int idx = 0; idx < qops.length; idx++)
                        if(qops[idx].equalsIgnoreCase("auth-int"))
                            throw new AuthSchemeNotImplException("Digest auth scheme: Can't comply with qop option 'auth-int' because an HttpOutputStream is being used and the server doesn't speak HTTP/1.1");

                    throw new AuthSchemeNotImplException("Digest auth scheme: None of the available qop options '" + ch_params[ch_qop].getValue() + "' implemented");
                }
                params[qop] = new NVPair("qop", p);
            }
            if(qop != -1)
                if(nc == -1)
                {
                    params = Util.resizeArray(params, params.length + 1);
                    nc = params.length - 1;
                    params[nc] = new NVPair("nc", "00000001");
                } else
                if(old_nonce.equals(((Object) (params[nonce].getValue()))))
                {
                    String c = Long.toHexString(Long.parseLong(params[nc].getValue(), 16) + 1L);
                    params[nc] = new NVPair("nc", "00000000".substring(c.length()) + c);
                } else
                {
                    params[nc] = new NVPair("nc", "00000001");
                }
            if(challenge != null && (ch_stale == -1 || !ch_params[ch_stale].getValue().equalsIgnoreCase("true")) && alg != -1 && params[alg].getValue().equalsIgnoreCase("MD5-sess"))
                extra[1] = (new MD5(((Object) (extra[0] + ":" + params[nonce].getValue() + ":" + params[cnonce].getValue())))).asHex();
            info.setParams(params);
            info.setExtraInfo(((Object) (extra)));
        }
        String hash = null;
        if(qop != -1 && params[qop].getValue().equalsIgnoreCase("auth-int") && req.getStream() == null)
        {
            MD5 entity_hash = new MD5();
            entity_hash.Update(req.getData() != null ? req.getData() : NUL);
            hash = entity_hash.asHex();
        }
        if(req.getStream() == null)
            params[response] = new NVPair("response", calcResponseAttr(hash, extra, params, alg, uri, qop, nonce, nc, cnonce, req.getMethod()));
        boolean ch_dreq_val = false;
        if(ch_dreq != -1 && (ch_params[ch_dreq].getValue() == null || ch_params[ch_dreq].getValue().equalsIgnoreCase("true")))
            ch_dreq_val = true;
        AuthorizationInfo new_info;
        if((ch_dreq_val || digest != -1) && req.getStream() == null)
        {
            NVPair d_params[];
            if(digest == -1)
            {
                d_params = Util.resizeArray(params, params.length + 1);
                digest = params.length;
            } else
            {
                d_params = params;
            }
            d_params[digest] = new NVPair("digest", calc_digest(req, extra[0], params[nonce].getValue()));
            if(dreq == -1)
            {
                dreq = d_params.length;
                d_params = Util.resizeArray(d_params, d_params.length + 1);
                d_params[dreq] = new NVPair("digest-required", "true");
            }
            new_info = new AuthorizationInfo(info.getHost(), info.getPort(), info.getScheme(), info.getRealm(), d_params, ((Object) (extra)));
        } else
        if(ch_dreq_val)
            new_info = null;
        else
            new_info = new AuthorizationInfo(info.getHost(), info.getPort(), info.getScheme(), info.getRealm(), params, ((Object) (extra)));
        boolean from_server = challenge != null && challenge.getHost().equalsIgnoreCase(req.getConnection().getHost());
        if(ch_domain != -1)
        {
            URI base = null;
            try
            {
                base = new URI(req.getConnection().getProtocol(), req.getConnection().getHost(), req.getConnection().getPort(), req.getRequestURI());
            }
            catch(ParseException pe) { }
            StringTokenizer tok = new StringTokenizer(ch_params[ch_domain].getValue());
            while(tok.hasMoreTokens())
            {
                URI Uri;
                try
                {
                    Uri = new URI(base, tok.nextToken());
                }
                catch(ParseException pe)
                {
                    continue;
                }
                AuthorizationInfo tmp = AuthorizationInfo.getAuthorization(Uri.getHost(), Uri.getPort(), info.getScheme(), info.getRealm(), req.getConnection().getContext());
                if(tmp == null)
                {
                    params[uri] = new NVPair("uri", Uri.getPath());
                    tmp = new AuthorizationInfo(Uri.getHost(), Uri.getPort(), info.getScheme(), info.getRealm(), params, ((Object) (extra)));
                    AuthorizationInfo.addAuthorization(tmp);
                }
                if(from_server)
                    tmp.addPath(Uri.getPath());
            }
        } else
        if(from_server && challenge != null)
        {
            AuthorizationInfo tmp = AuthorizationInfo.getAuthorization(challenge.getHost(), challenge.getPort(), info.getScheme(), info.getRealm(), req.getConnection().getContext());
            if(tmp != null)
                tmp.addPath("/");
        }
        return new_info;
    }

    private static AuthorizationInfo digest_check_stale(AuthorizationInfo challenge, RoRequest req, RoResponse resp)
        throws AuthSchemeNotImplException
    {
        AuthorizationInfo cred = null;
        NVPair params[] = challenge.getParams();
        for(int idx = 0; idx < params.length; idx++)
        {
            String name = params[idx].getName();
            if(!name.equalsIgnoreCase("stale") || !params[idx].getValue().equalsIgnoreCase("true"))
                continue;
            cred = AuthorizationInfo.getAuthorization(challenge, req, resp, false);
            if(cred != null)
                return digest_fixup(cred, req, challenge, resp);
            break;
        }

        return cred;
    }

    private static boolean handle_nextnonce(AuthorizationInfo prev, RoRequest req, HttpHeaderElement nextnonce)
    {
        if(prev == null || nextnonce == null || nextnonce.getValue() == null)
            return false;
        AuthorizationInfo ai;
        try
        {
            ai = AuthorizationInfo.getAuthorization(prev, req, ((RoResponse) (null)), false);
        }
        catch(AuthSchemeNotImplException asnie)
        {
            ai = prev;
        }
        synchronized(ai)
        {
            NVPair params[] = ai.getParams();
            params = setValue(params, "nonce", nextnonce.getValue());
            params = setValue(params, "nc", "00000000");
            ai.setParams(params);
        }
        return true;
    }

    private static boolean handle_digest(AuthorizationInfo prev, Response resp, RoRequest req, String hdr_name)
        throws IOException
    {
        if(prev == null)
            return false;
        NVPair params[] = prev.getParams();
        VerifyDigest verifier = new VerifyDigest(((String[])prev.getExtraInfo())[0], getValue(params, "nonce"), req.getMethod(), getValue(params, "uri"), hdr_name, ((RoResponse) (resp)));
        if(resp.hasEntity())
        {
            if(GlobalConstants.DebugAuth)
                System.err.println("Auth:  pushing md5-check-stream to verify digest from " + hdr_name);
            resp.inp_stream = ((java.io.InputStream) (new MD5InputStream(resp.inp_stream, ((HashVerifier) (verifier)))));
        } else
        {
            if(GlobalConstants.DebugAuth)
                System.err.println("Auth:  verifying digest from " + hdr_name);
            verifier.verifyHash((new MD5()).Final(), 0L);
        }
        return true;
    }

    private static boolean handle_rspauth(AuthorizationInfo prev, Response resp, RoRequest req, Vector auth_info, String hdr_name)
        throws IOException
    {
        if(prev == null)
            return false;
        NVPair params[] = prev.getParams();
        int uri = -1;
        int alg = -1;
        int nonce = -1;
        int cnonce = -1;
        int nc = -1;
        for(int idx = 0; idx < params.length; idx++)
        {
            String name = params[idx].getName().toLowerCase();
            if(name.equals("uri"))
                uri = idx;
            else
            if(name.equals("algorithm"))
                alg = idx;
            else
            if(name.equals("nonce"))
                nonce = idx;
            else
            if(name.equals("cnonce"))
                cnonce = idx;
            else
            if(name.equals("nc"))
                nc = idx;
        }

        VerifyRspAuth verifier = new VerifyRspAuth(params[uri].getValue(), ((String[])prev.getExtraInfo())[0], alg != -1 ? params[alg].getValue() : null, params[nonce].getValue(), cnonce != -1 ? params[cnonce].getValue() : "", nc != -1 ? params[nc].getValue() : "", hdr_name, ((RoResponse) (resp)));
        HttpHeaderElement qop = null;
        if(auth_info != null && (qop = Util.getElement(auth_info, "qop")) != null && qop.getValue() != null && (qop.getValue().equalsIgnoreCase("auth") || !resp.hasEntity() && qop.getValue().equalsIgnoreCase("auth-int")))
        {
            if(GlobalConstants.DebugAuth)
                System.err.println("Auth:  verifying rspauth from " + hdr_name);
            verifier.verifyHash((new MD5()).Final(), 0L);
        } else
        {
            if(GlobalConstants.DebugAuth)
                System.err.println("Auth:  pushing md5-check-stream to verify rspauth from " + hdr_name);
            resp.inp_stream = ((java.io.InputStream) (new MD5InputStream(resp.inp_stream, ((HashVerifier) (verifier)))));
        }
        return true;
    }

    private static String calcResponseAttr(String hash, String extra[], NVPair params[], int alg, int uri, int qop, int nonce, int nc,
            int cnonce, String method)
    {
        String A1;
        if(alg != -1 && params[alg].getValue().equalsIgnoreCase("MD5-sess"))
            A1 = extra[1];
        else
            A1 = extra[0];
        String A2 = method + ":" + params[uri].getValue();
        if(qop != -1 && params[qop].getValue().equalsIgnoreCase("auth-int"))
            A2 = A2 + ":" + hash;
        A2 = (new MD5(((Object) (A2)))).asHex();
        String resp_val;
        if(qop == -1)
            resp_val = (new MD5(((Object) (A1 + ":" + params[nonce].getValue() + ":" + A2)))).asHex();
        else
            resp_val = (new MD5(((Object) (A1 + ":" + params[nonce].getValue() + ":" + params[nc].getValue() + ":" + params[cnonce].getValue() + ":" + params[qop].getValue() + ":" + A2)))).asHex();
        return resp_val;
    }

    private static String calc_digest(RoRequest req, String A1_hash, String nonce)
    {
        if(req.getStream() != null)
            return "";
        int ct = -1;
        int ce = -1;
        int lm = -1;
        int ex = -1;
        int dt = -1;
        for(int idx = 0; idx < req.getHeaders().length; idx++)
        {
            String name = req.getHeaders()[idx].getName();
            if(name.equalsIgnoreCase("Content-type"))
                ct = idx;
            else
            if(name.equalsIgnoreCase("Content-Encoding"))
                ce = idx;
            else
            if(name.equalsIgnoreCase("Last-Modified"))
                lm = idx;
            else
            if(name.equalsIgnoreCase("Expires"))
                ex = idx;
            else
            if(name.equalsIgnoreCase("Date"))
                dt = idx;
        }

        NVPair hdrs[] = req.getHeaders();
        byte entity_body[] = req.getData() != null ? req.getData() : NUL;
        MD5 entity_hash = new MD5();
        entity_hash.Update(entity_body);
        String entity_info = (new MD5(((Object) (req.getRequestURI() + ":" + (ct != -1 ? hdrs[ct].getValue() : "") + ":" + entity_body.length + ":" + (ce != -1 ? hdrs[ce].getValue() : "") + ":" + (lm != -1 ? hdrs[lm].getValue() : "") + ":" + (ex != -1 ? hdrs[ex].getValue() : ""))))).asHex();
        String entity_digest = A1_hash + ":" + nonce + ":" + req.getMethod() + ":" + (dt != -1 ? hdrs[dt].getValue() : "") + ":" + entity_info + ":" + entity_hash.asHex();
        if(GlobalConstants.DebugAuth)
        {
            System.err.println("Auth:  Entity-Info: '" + req.getRequestURI() + ":" + (ct != -1 ? hdrs[ct].getValue() : "") + ":" + entity_body.length + ":" + (ce != -1 ? hdrs[ce].getValue() : "") + ":" + (lm != -1 ? hdrs[lm].getValue() : "") + ":" + (ex != -1 ? hdrs[ex].getValue() : "") + "'");
            System.err.println("Auth:  Entity-Body: '" + entity_hash.asHex() + "'");
            System.err.println("Auth:  Entity-Digest: '" + entity_digest + "'");
        }
        return (new MD5(((Object) (entity_digest)))).asHex();
    }

    private static boolean handle_discard(AuthorizationInfo prev, RoRequest req, HttpHeaderElement discard)
    {
        if(discard != null && prev != null)
        {
            AuthorizationInfo.removeAuthorization(prev, req.getConnection().getContext());
            return true;
        } else
        {
            return false;
        }
    }

    private static byte[] gen_random_bytes(int num)
    {
        byte data[];
        try
        {
            FileInputStream rnd = new FileInputStream("/dev/random");
            DataInputStream din = new DataInputStream(((java.io.InputStream) (rnd)));
            data = new byte[num];
            din.readFully(data);
            try
            {
                ((FilterInputStream) (din)).close();
            }
            catch(IOException ioe) { }
            return data;
        }
        catch(Throwable t)
        {
            data = new byte[num];
        }
        try
        {
            long fm = Runtime.getRuntime().freeMemory();
            data[0] = (byte)(int)(fm & 255L);
            data[1] = (byte)(int)(fm >> 8 & 255L);
            int h = ((Object) (data)).hashCode();
            data[2] = (byte)(h & 0xff);
            data[3] = (byte)(h >> 8 & 0xff);
            data[4] = (byte)(h >> 16 & 0xff);
            data[5] = (byte)(h >> 24 & 0xff);
            long time = System.currentTimeMillis();
            data[6] = (byte)(int)(time & 255L);
            data[7] = (byte)(int)(time >> 8 & 255L);
        }
        catch(ArrayIndexOutOfBoundsException aioobe) { }
        return data;
    }

    private static final String getValue(NVPair list[], String key)
    {
        int len = list.length;
        for(int idx = 0; idx < len; idx++)
            if(list[idx].getName().equalsIgnoreCase(key))
                return list[idx].getValue();

        return null;
    }

    private static final int getIndex(NVPair list[], String key)
    {
        int len = list.length;
        for(int idx = 0; idx < len; idx++)
            if(list[idx].getName().equalsIgnoreCase(key))
                return idx;

        return -1;
    }

    private static final NVPair[] setValue(NVPair list[], String key, String val)
    {
        int idx = getIndex(list, key);
        if(idx == -1)
        {
            idx = list.length;
            list = Util.resizeArray(list, list.length + 1);
        }
        list[idx] = new NVPair(key, val);
        return list;
    }

    private static String[] splitList(String str, String sep)
    {
        if(str == null)
            return new String[0];
        StringTokenizer tok = new StringTokenizer(str, sep);
        String list[] = new String[tok.countTokens()];
        for(int idx = 0; idx < list.length; idx++)
            list[idx] = tok.nextToken().trim();

        return list;
    }

    static String hex(byte buf[])
    {
        StringBuffer str = new StringBuffer(buf.length * 3);
        for(int idx = 0; idx < buf.length; idx++)
        {
            str.append(Character.forDigit(buf[idx] >>> 4, 16));
            str.append(Character.forDigit(buf[idx] & 0x10, 16));
            str.append(':');
        }

        str.setLength(str.length() - 1);
        return str.toString();
    }

    static final byte[] unHex(String hex)
    {
        byte digest[] = new byte[hex.length() / 2];
        for(int idx = 0; idx < digest.length; idx++)
            digest[idx] = (byte)(0xff & Integer.parseInt(hex.substring(2 * idx, 2 * (idx + 1)), 16));

        return digest;
    }

}
