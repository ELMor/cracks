// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   TransferEncodingModule.java

package HTTPClient;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Vector;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;

// Referenced classes of package HTTPClient:
//            ParseException, ModuleException, HttpHeaderElement, NVPair, 
//            UncompressInputStream, ChunkedInputStream, HTTPClientModule, GlobalConstants, 
//            Request, Util, Response, RoRequest

class TransferEncodingModule
    implements HTTPClientModule, GlobalConstants
{

    TransferEncodingModule()
    {
    }

    public int requestHandler(Request req, Response resp[])
        throws ModuleException
    {
        NVPair hdrs[] = req.getHeaders();
        int idx;
        for(idx = 0; idx < hdrs.length; idx++)
            if(hdrs[idx].getName().equalsIgnoreCase("TE"))
                break;

        Vector pte;
        if(idx == hdrs.length)
        {
            hdrs = Util.resizeArray(hdrs, idx + 1);
            req.setHeaders(hdrs);
            pte = new Vector();
        } else
        {
            try
            {
                pte = Util.parseHeader(hdrs[idx].getValue());
            }
            catch(ParseException pe)
            {
                throw new ModuleException(((Throwable) (pe)).toString());
            }
        }
        HttpHeaderElement all = Util.getElement(pte, "*");
        if(all != null)
        {
            NVPair params[] = all.getParams();
            for(idx = 0; idx < params.length; idx++)
                if(params[idx].getName().equalsIgnoreCase("q"))
                    break;

            if(idx == params.length)
                return 0;
            if(params[idx].getValue() == null || params[idx].getValue().length() == 0)
                throw new ModuleException("Invalid q value for \"*\" in TE header: ");
            try
            {
                if((double)Float.valueOf(params[idx].getValue()).floatValue() > 0.0D)
                    return 0;
            }
            catch(NumberFormatException nfe)
            {
                throw new ModuleException("Invalid q value for \"*\" in TE header: " + ((Throwable) (nfe)).getMessage());
            }
        }
        if(!pte.contains(((Object) (new HttpHeaderElement("deflate")))))
            pte.addElement(((Object) (new HttpHeaderElement("deflate"))));
        if(!pte.contains(((Object) (new HttpHeaderElement("gzip")))))
            pte.addElement(((Object) (new HttpHeaderElement("gzip"))));
        if(!pte.contains(((Object) (new HttpHeaderElement("compress")))))
            pte.addElement(((Object) (new HttpHeaderElement("compress"))));
        hdrs[idx] = new NVPair("TE", Util.assembleHeader(pte));
        return 0;
    }

    public void responsePhase1Handler(Response response, RoRequest rorequest)
    {
    }

    public int responsePhase2Handler(Response resp, Request req)
    {
        return 10;
    }

    public void responsePhase3Handler(Response resp, RoRequest req)
        throws IOException, ModuleException
    {
        String te = resp.getHeader("Transfer-Encoding");
        if(te == null || req.getMethod().equals("HEAD"))
            return;
        Vector pte;
        try
        {
            pte = Util.parseHeader(te);
        }
        catch(ParseException pe)
        {
            throw new ModuleException(((Throwable) (pe)).toString());
        }
        for(; pte.size() > 0; pte.removeElementAt(pte.size() - 1))
        {
            String encoding = ((HttpHeaderElement)pte.lastElement()).getName();
            if(encoding.equalsIgnoreCase("gzip"))
            {
                if(GlobalConstants.DebugMods)
                    System.err.println("TEM:   pushing gzip-input-stream");
                resp.inp_stream = ((java.io.InputStream) (new GZIPInputStream(resp.inp_stream)));
                continue;
            }
            if(encoding.equalsIgnoreCase("deflate"))
            {
                if(GlobalConstants.DebugMods)
                    System.err.println("TEM:   pushing inflater-input-stream");
                resp.inp_stream = ((java.io.InputStream) (new InflaterInputStream(resp.inp_stream)));
                continue;
            }
            if(encoding.equalsIgnoreCase("compress"))
            {
                if(GlobalConstants.DebugMods)
                    System.err.println("TEM:   pushing uncompress-input-stream");
                resp.inp_stream = ((java.io.InputStream) (new UncompressInputStream(resp.inp_stream)));
                continue;
            }
            if(encoding.equalsIgnoreCase("chunked"))
            {
                if(GlobalConstants.DebugMods)
                    System.err.println("TEM:   pushing chunked-input-stream");
                resp.inp_stream = ((java.io.InputStream) (new ChunkedInputStream(resp.inp_stream)));
                continue;
            }
            if(encoding.equalsIgnoreCase("identity"))
            {
                if(GlobalConstants.DebugMods)
                    System.err.println("TEM:   ignoring 'identity' token");
                continue;
            }
            if(GlobalConstants.DebugMods)
                System.err.println("TEM:   Unknown transfer encoding '" + encoding + "'");
            break;
        }

        if(pte.size() > 0)
            resp.setHeader("Transfer-Encoding", Util.assembleHeader(pte));
        else
            resp.deleteHeader("Transfer-Encoding");
    }

    public void trailerHandler(Response response, RoRequest rorequest)
    {
    }

    static 
    {
        try
        {
            new InflaterInputStream(((java.io.InputStream) (null)));
        }
        catch(NullPointerException npe) { }
    }
}
