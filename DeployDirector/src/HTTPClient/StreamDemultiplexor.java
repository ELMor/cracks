// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StreamDemultiplexor.java

package HTTPClient;

import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.PrintStream;
import java.net.Socket;

// Referenced classes of package HTTPClient:
//            LinkedList, ExtBufferedInputStream, RetryException, ResponseHandler, 
//            ParseException, SocketTimeout, GlobalConstants, RespInputStream, 
//            Response, Codecs, HTTPConnection, HTTPResponse, 
//            Request

class StreamDemultiplexor
    implements GlobalConstants
{

    private int Protocol;
    private HTTPConnection Connection;
    private ExtBufferedInputStream Stream;
    private Socket Sock;
    private ResponseHandler MarkedForClose;
    private SocketTimeout.TimeoutEntry Timer;
    private static SocketTimeout TimerThread;
    private LinkedList RespHandlerList;
    private int chunk_len;
    private int cur_timeout;

    StreamDemultiplexor(int protocol, Socket sock, HTTPConnection connection)
        throws IOException
    {
        Sock = null;
        Timer = null;
        cur_timeout = 0;
        Protocol = protocol;
        Connection = connection;
        RespHandlerList = new LinkedList();
        init(sock);
    }

    private void init(Socket sock)
        throws IOException
    {
        if(GlobalConstants.DebugDemux)
            System.err.println("Demux: Initializing Stream Demultiplexor (" + ((Object)this).hashCode() + ")");
        Sock = sock;
        Stream = new ExtBufferedInputStream(sock.getInputStream());
        MarkedForClose = null;
        chunk_len = -1;
        Timer = TimerThread.setTimeout(this);
    }

    void register(Response resp_handler, Request req)
        throws RetryException
    {
        synchronized(RespHandlerList)
        {
            if(Sock == null)
                throw new RetryException();
            RespHandlerList.addToEnd(((Object) (new ResponseHandler(resp_handler, req, this))));
        }
    }

    RespInputStream getStream(Response resp)
    {
        ResponseHandler resph;
        for(resph = (ResponseHandler)RespHandlerList.enumerate(); resph != null; resph = (ResponseHandler)RespHandlerList.next())
            if(resph.resp == resp)
                break;

        if(resph != null)
            return resph.stream;
        else
            return null;
    }

    void restartTimer()
    {
        if(Timer != null)
            Timer.reset();
    }

    int read(byte b[], int off, int len, ResponseHandler resph, int timeout)
        throws IOException
    {
        if(resph.exception != null)
            throw (IOException)((Throwable) (resph.exception)).fillInStackTrace();
        if(resph.eof)
            return -1;
        ResponseHandler head;
        while((head = (ResponseHandler)RespHandlerList.getFirst()) != null && head != resph) 
            try
            {
                head.stream.readAll(timeout);
            }
            catch(IOException ioe)
            {
                if(ioe instanceof InterruptedIOException)
                    throw ioe;
                else
                    throw (IOException)((Throwable) (resph.exception)).fillInStackTrace();
            }
        synchronized(this)
        {
            if(resph.exception != null)
                throw (IOException)((Throwable) (resph.exception)).fillInStackTrace();
            if(GlobalConstants.DebugDemux && resph.resp.cd_type != 1)
                System.err.println("Demux: Reading for stream " + ((Object) (resph.stream)).hashCode() + " (" + Thread.currentThread() + ")");
            if(Timer != null)
                Timer.hyber();
            try
            {
                int rcvd = -1;
                if(timeout != cur_timeout)
                {
                    if(GlobalConstants.DebugDemux)
                        System.err.println("Demux: Setting timeout to " + timeout + " ms");
                    try
                    {
                        Sock.setSoTimeout(timeout);
                    }
                    catch(Throwable t) { }
                    cur_timeout = timeout;
                }
                switch(resph.resp.cd_type)
                {
                case 1: // '\001'
                    rcvd = Stream.read(b, off, len);
                    if(rcvd == -1)
                        throw new EOFException("Premature EOF encountered");
                    break;

                case 2: // '\002'
                    rcvd = -1;
                    close(resph);
                    break;

                case 3: // '\003'
                    rcvd = Stream.read(b, off, len);
                    if(rcvd == -1)
                        close(resph);
                    break;

                case 4: // '\004'
                    int cl = resph.resp.ContentLength;
                    if(len > cl - resph.stream.count)
                        len = cl - resph.stream.count;
                    rcvd = Stream.read(b, off, len);
                    if(rcvd == -1)
                        throw new EOFException("Premature EOF encountered");
                    if(resph.stream.count + rcvd == cl)
                        close(resph);
                    break;

                case 5: // '\005'
                    if(chunk_len == -1)
                        chunk_len = Codecs.getChunkLength(((java.io.InputStream) (Stream)));
                    if(chunk_len > 0)
                    {
                        if(len > chunk_len)
                            len = chunk_len;
                        rcvd = Stream.read(b, off, len);
                        if(rcvd == -1)
                            throw new EOFException("Premature EOF encountered");
                        chunk_len -= rcvd;
                        if(chunk_len == 0)
                        {
                            Stream.read();
                            Stream.read();
                            chunk_len = -1;
                        }
                    } else
                    {
                        resph.resp.readTrailers(((java.io.InputStream) (Stream)));
                        rcvd = -1;
                        close(resph);
                        chunk_len = -1;
                    }
                    break;

                case 6: // '\006'
                    byte endbndry[] = resph.getEndBoundary(Stream);
                    int end_cmp[] = resph.getEndCompiled(Stream);
                    rcvd = Stream.read(b, off, len);
                    if(rcvd == -1)
                        throw new EOFException("Premature EOF encountered");
                    int ovf = Stream.pastEnd(endbndry, end_cmp);
                    if(ovf != -1)
                    {
                        rcvd -= ovf;
                        Stream.reset();
                        close(resph);
                    }
                    break;

                default:
                    throw new Error("Internal Error in StreamDemultiplexor: Invalid cd_type " + resph.resp.cd_type);
                }
                restartTimer();
                int i = rcvd;
                return i;
            }
            catch(InterruptedIOException ie)
            {
                restartTimer();
                throw ie;
            }
            catch(IOException ioe)
            {
                if(GlobalConstants.DebugDemux)
                {
                    System.err.print("Demux: (" + Thread.currentThread() + ") ");
                    ((Throwable) (ioe)).printStackTrace();
                }
                close(ioe, true);
                throw resph.exception;
            }
            catch(ParseException pe)
            {
                if(GlobalConstants.DebugDemux)
                {
                    System.err.print("Demux: (" + Thread.currentThread() + ") ");
                    ((Throwable) (pe)).printStackTrace();
                }
                close(new IOException(((Throwable) (pe)).toString()), true);
                throw resph.exception;
            }
        }
    }

    synchronized long skip(long num, ResponseHandler resph)
        throws IOException
    {
        if(resph.exception != null)
            throw (IOException)((Throwable) (resph.exception)).fillInStackTrace();
        if(resph.eof)
            return 0L;
        byte dummy[] = new byte[(int)num];
        int rcvd = read(dummy, 0, (int)num, resph, 0);
        if(rcvd == -1)
            return 0L;
        else
            return (long)rcvd;
    }

    synchronized int available(ResponseHandler resph)
        throws IOException
    {
        int avail = Stream.available();
        if(resph == null)
            return avail;
        if(resph.exception != null)
            throw (IOException)((Throwable) (resph.exception)).fillInStackTrace();
        if(resph.eof)
            return 0;
        switch(resph.resp.cd_type)
        {
        case 2: // '\002'
            return 0;

        case 1: // '\001'
            return avail <= 0 ? 0 : 1;

        case 3: // '\003'
            return avail;

        case 4: // '\004'
            int cl = resph.resp.ContentLength;
            cl -= resph.stream.count;
            return avail >= cl ? cl : avail;

        case 5: // '\005'
            return avail;

        case 6: // '\006'
            return avail;
        }
        throw new Error("Internal Error in StreamDemultiplexor: Invalid cd_type " + resph.resp.cd_type);
    }

    synchronized void close(IOException exception, boolean was_reset)
    {
        if(Sock == null)
            return;
        if(GlobalConstants.DebugDemux)
            System.err.println("Demux: Closing all streams and socket (" + ((Object)this).hashCode() + ")");
        try
        {
            ((FilterInputStream) (Stream)).close();
        }
        catch(IOException ioe) { }
        try
        {
            Sock.close();
        }
        catch(IOException ioe) { }
        Sock = null;
        if(Timer != null)
        {
            Timer.kill();
            Timer = null;
        }
        Connection.DemuxList.remove(((Object) (this)));
        if(exception != null)
            synchronized(RespHandlerList)
            {
                retry_requests(exception, was_reset);
            }
    }

    private void retry_requests(IOException exception, boolean was_reset)
    {
        RetryException first = null;
        RetryException prev = null;
        for(ResponseHandler resph = (ResponseHandler)RespHandlerList.enumerate(); resph != null; resph = (ResponseHandler)RespHandlerList.next())
        {
            if(resph.resp.got_headers)
            {
                resph.exception = exception;
            } else
            {
                RetryException tmp = new RetryException(((Throwable) (exception)).getMessage());
                if(first == null)
                    first = tmp;
                tmp.request = resph.request;
                tmp.response = resph.resp;
                tmp.exception = exception;
                tmp.conn_reset = was_reset;
                tmp.first = first;
                tmp.addToListAfter(prev);
                prev = tmp;
                resph.exception = ((IOException) (tmp));
            }
            RespHandlerList.remove(((Object) (resph)));
        }

    }

    synchronized void close(ResponseHandler resph)
    {
        if(resph != (ResponseHandler)RespHandlerList.getFirst())
            return;
        if(GlobalConstants.DebugDemux)
            System.err.println("Demux: Closing stream " + ((Object) (resph.stream)).hashCode() + " (" + Thread.currentThread() + ")");
        resph.eof = true;
        RespHandlerList.remove(((Object) (resph)));
        if(resph == MarkedForClose)
            close(new IOException("Premature end of Keep-Alive"), false);
        else
            closeSocketIfAllStreamsClosed();
    }

    synchronized void closeSocketIfAllStreamsClosed()
    {
        synchronized(RespHandlerList)
        {
            for(ResponseHandler resph = (ResponseHandler)RespHandlerList.enumerate(); resph != null && resph.stream.closed; resph = (ResponseHandler)RespHandlerList.next())
                if(resph == MarkedForClose)
                {
                    ResponseHandler tmp;
                    do
                    {
                        tmp = (ResponseHandler)RespHandlerList.getFirst();
                        RespHandlerList.remove(((Object) (tmp)));
                    } while(tmp != resph);
                    close(new IOException("Premature end of Keep-Alive"), false);
                    return;
                }

        }
    }

    synchronized Socket getSocket()
    {
        if(MarkedForClose != null)
            return null;
        if(Timer != null)
            Timer.hyber();
        return Sock;
    }

    synchronized void markForClose(Response resp)
    {
        synchronized(RespHandlerList)
        {
            if(RespHandlerList.getFirst() == null)
            {
                close(new IOException("Premature end of Keep-Alive"), false);
                return;
            }
        }
        if(Timer != null)
        {
            Timer.kill();
            Timer = null;
        }
        ResponseHandler lasth = null;
        for(ResponseHandler resph = (ResponseHandler)RespHandlerList.enumerate(); resph != null; resph = (ResponseHandler)RespHandlerList.next())
        {
            if(resph.resp == resp)
            {
                MarkedForClose = resph;
                if(GlobalConstants.DebugDemux)
                    System.err.println("Demux: stream " + ((Object) (resp.inp_stream)).hashCode() + " marked for close (" + Thread.currentThread() + ")");
                closeSocketIfAllStreamsClosed();
                return;
            }
            if(MarkedForClose == resph)
                return;
            lasth = resph;
        }

        if(lasth == null)
            return;
        MarkedForClose = lasth;
        closeSocketIfAllStreamsClosed();
        if(GlobalConstants.DebugDemux)
            System.err.println("Demux: stream " + ((Object) (lasth.stream)).hashCode() + " marked for close (" + Thread.currentThread() + ")");
    }

    void abort()
    {
        if(GlobalConstants.DebugDemux)
            System.err.println("Demux: Aborting socket (" + ((Object)this).hashCode() + ")");
        synchronized(RespHandlerList)
        {
            for(ResponseHandler resph = (ResponseHandler)RespHandlerList.enumerate(); resph != null; resph = (ResponseHandler)RespHandlerList.next())
            {
                if(resph.resp.http_resp != null)
                    resph.resp.http_resp.markAborted();
                if(resph.exception == null)
                    resph.exception = new IOException("Request aborted by user");
            }

            if(Sock != null)
            {
                try
                {
                    try
                    {
                        Sock.setSoLinger(false, 0);
                    }
                    catch(Throwable t) { }
                    try
                    {
                        ((FilterInputStream) (Stream)).close();
                    }
                    catch(IOException ioe) { }
                    try
                    {
                        Sock.close();
                    }
                    catch(IOException ioe) { }
                    Sock = null;
                    if(Timer != null)
                    {
                        Timer.kill();
                        Timer = null;
                    }
                }
                catch(NullPointerException npe) { }
                Connection.DemuxList.remove(((Object) (this)));
            }
        }
    }

    protected void finalize()
        throws Throwable
    {
        close((IOException)null, false);
        super.finalize();
    }

    public String toString()
    {
        String prot;
        switch(Protocol)
        {
        case 0: // '\0'
            prot = "HTTP";
            break;

        case 1: // '\001'
            prot = "HTTPS";
            break;

        case 2: // '\002'
            prot = "SHTTP";
            break;

        case 3: // '\003'
            prot = "HTTP_NG";
            break;

        default:
            throw new Error("HTTPClient Internal Error: invalid protocol " + Protocol);
        }
        return ((Object)this).getClass().getName() + "[Protocol=" + prot + "]";
    }

    static 
    {
        TimerThread = null;
        TimerThread = new SocketTimeout(60);
        ((Thread) (TimerThread)).start();
    }
}
