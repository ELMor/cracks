// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CookiePolicyHandler.java

package HTTPClient;


// Referenced classes of package HTTPClient:
//            Cookie, RoRequest, RoResponse

public interface CookiePolicyHandler
{

    public abstract boolean acceptCookie(Cookie cookie, RoRequest rorequest, RoResponse roresponse);

    public abstract boolean sendCookie(Cookie cookie, RoRequest rorequest);
}
