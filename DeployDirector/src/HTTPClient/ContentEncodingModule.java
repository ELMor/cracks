// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ContentEncodingModule.java

package HTTPClient;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Vector;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;

// Referenced classes of package HTTPClient:
//            ParseException, ModuleException, HttpHeaderElement, NVPair, 
//            UncompressInputStream, HTTPClientModule, GlobalConstants, Request, 
//            Util, Response, RoRequest

class ContentEncodingModule
    implements HTTPClientModule, GlobalConstants
{

    ContentEncodingModule()
    {
    }

    public int requestHandler(Request req, Response resp[])
        throws ModuleException
    {
        NVPair hdrs[] = req.getHeaders();
        int idx;
        for(idx = 0; idx < hdrs.length; idx++)
            if(hdrs[idx].getName().equalsIgnoreCase("Accept-Encoding"))
                break;

        Vector pae;
        if(idx == hdrs.length)
        {
            hdrs = Util.resizeArray(hdrs, idx + 1);
            req.setHeaders(hdrs);
            pae = new Vector();
        } else
        {
            try
            {
                pae = Util.parseHeader(hdrs[idx].getValue());
            }
            catch(ParseException pe)
            {
                throw new ModuleException(((Throwable) (pe)).toString());
            }
        }
        HttpHeaderElement all = Util.getElement(pae, "*");
        if(all != null)
        {
            NVPair params[] = all.getParams();
            for(idx = 0; idx < params.length; idx++)
                if(params[idx].getName().equalsIgnoreCase("q"))
                    break;

            if(idx == params.length)
                return 0;
            if(params[idx].getValue() == null || params[idx].getValue().length() == 0)
                throw new ModuleException("Invalid q value for \"*\" in Accept-Encoding header: ");
            try
            {
                if((double)Float.valueOf(params[idx].getValue()).floatValue() > 0.0D)
                    return 0;
            }
            catch(NumberFormatException nfe)
            {
                throw new ModuleException("Invalid q value for \"*\" in Accept-Encoding header: " + ((Throwable) (nfe)).getMessage());
            }
        }
        if(!pae.contains(((Object) (new HttpHeaderElement("deflate")))))
            pae.addElement(((Object) (new HttpHeaderElement("deflate"))));
        if(!pae.contains(((Object) (new HttpHeaderElement("gzip")))))
            pae.addElement(((Object) (new HttpHeaderElement("gzip"))));
        if(!pae.contains(((Object) (new HttpHeaderElement("x-gzip")))))
            pae.addElement(((Object) (new HttpHeaderElement("x-gzip"))));
        if(!pae.contains(((Object) (new HttpHeaderElement("compress")))))
            pae.addElement(((Object) (new HttpHeaderElement("compress"))));
        if(!pae.contains(((Object) (new HttpHeaderElement("x-compress")))))
            pae.addElement(((Object) (new HttpHeaderElement("x-compress"))));
        hdrs[idx] = new NVPair("Accept-Encoding", Util.assembleHeader(pae));
        return 0;
    }

    public void responsePhase1Handler(Response response, RoRequest rorequest)
    {
    }

    public int responsePhase2Handler(Response resp, Request req)
    {
        return 10;
    }

    public void responsePhase3Handler(Response resp, RoRequest req)
        throws IOException, ModuleException
    {
        String ce = resp.getHeader("Content-Encoding");
        if(ce == null || req.getMethod().equals("HEAD") || resp.getStatusCode() == 206)
            return;
        Vector pce;
        try
        {
            pce = Util.parseHeader(ce);
        }
        catch(ParseException pe)
        {
            throw new ModuleException(((Throwable) (pe)).toString());
        }
        if(pce.size() == 0)
            return;
        String encoding = ((HttpHeaderElement)pce.firstElement()).getName();
        if(encoding.equalsIgnoreCase("gzip") || encoding.equalsIgnoreCase("x-gzip"))
        {
            if(GlobalConstants.DebugMods)
                System.err.println("CEM:   pushing gzip-input-stream");
            resp.inp_stream = ((java.io.InputStream) (new GZIPInputStream(resp.inp_stream)));
            pce.removeElementAt(pce.size() - 1);
            resp.deleteHeader("Content-length");
        } else
        if(encoding.equalsIgnoreCase("deflate"))
        {
            if(GlobalConstants.DebugMods)
                System.err.println("CEM:   pushing inflater-input-stream");
            resp.inp_stream = ((java.io.InputStream) (new InflaterInputStream(resp.inp_stream)));
            pce.removeElementAt(pce.size() - 1);
            resp.deleteHeader("Content-length");
        } else
        if(encoding.equalsIgnoreCase("compress") || encoding.equalsIgnoreCase("x-compress"))
        {
            if(GlobalConstants.DebugMods)
                System.err.println("CEM:   pushing uncompress-input-stream");
            resp.inp_stream = ((java.io.InputStream) (new UncompressInputStream(resp.inp_stream)));
            pce.removeElementAt(pce.size() - 1);
            resp.deleteHeader("Content-length");
        } else
        if(encoding.equalsIgnoreCase("identity"))
        {
            if(GlobalConstants.DebugMods)
                System.err.println("CEM:   ignoring 'identity' token");
            pce.removeElementAt(pce.size() - 1);
        } else
        if(GlobalConstants.DebugMods)
            System.err.println("CEM:   Unknown content encoding '" + encoding + "'");
        if(pce.size() > 0)
            resp.setHeader("Content-Encoding", Util.assembleHeader(pce));
        else
            resp.deleteHeader("Content-Encoding");
    }

    public void trailerHandler(Response response, RoRequest rorequest)
    {
    }

    static 
    {
        try
        {
            new InflaterInputStream(((java.io.InputStream) (null)));
        }
        catch(NullPointerException npe) { }
    }
}
