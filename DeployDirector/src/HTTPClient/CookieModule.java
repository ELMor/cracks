// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CookieModule.java

package HTTPClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.net.ProtocolException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package HTTPClient:
//            Cookie, Cookie2, NVPair, DefaultCookiePolicyHandler, 
//            HTTPClientModule, GlobalConstants, HTTPConnection, Util, 
//            Request, CookiePolicyHandler, Response, RoRequest

public class CookieModule
    implements HTTPClientModule, GlobalConstants
{

    private static Hashtable cookie_cntxt_list = new Hashtable();
    private static File cookie_jar = null;
    private static Object cookieSaver = null;
    private static CookiePolicyHandler cookie_handler = new DefaultCookiePolicyHandler();
    private static boolean cookie_debug;
    private static boolean save_cookies;
    private static String TAB_CHAR = "\t";

    private static void loadCookies()
    {
        try
        {
            cookie_jar = new File(getCookieJarName());
            if(cookie_debug)
                System.out.println("Got cookie file \"" + getCookieJarName() + "\"");
            if(!cookie_jar.isFile() || !cookie_jar.canRead())
            {
                if(cookie_debug)
                    System.out.println("Cookie file is not accessible!");
                cookie_cntxt_list.put(HTTPConnection.getDefaultContext(), ((Object) (null)));
                return;
            }
        }
        catch(Throwable t)
        {
            if(cookie_debug)
            {
                System.out.println("Error loading cookies file!");
                System.out.println(t.getMessage());
                t.printStackTrace(System.out);
            }
            cookie_jar = null;
            cookie_cntxt_list.put(HTTPConnection.getDefaultContext(), ((Object) (null)));
            return;
        }
        BufferedReader cookie_reader = null;
        try
        {
            cookie_reader = new BufferedReader(((java.io.Reader) (new FileReader(cookie_jar))));
        }
        catch(FileNotFoundException fnfe)
        {
            if(cookie_debug)
            {
                System.out.println("No cookies file (doesn't exist yet?)");
                System.out.println(((Throwable) (fnfe)).getMessage());
            }
            cookie_cntxt_list.put(HTTPConnection.getDefaultContext(), ((Object) (null)));
            return;
        }
        String line = null;
        Hashtable cookie_map = new Hashtable();
        try
        {
            while((line = cookie_reader.readLine()) != null) 
            {
                Cookie cookie = parseCookie(line);
                if(cookie != null)
                    cookie_map.put(((Object) (cookie)), ((Object) (cookie)));
            }
        }
        catch(IOException ioe) { }
        cookie_cntxt_list.put(HTTPConnection.getDefaultContext(), ((Object) (cookie_map)));
        if(cookie_debug)
        {
            System.out.println("Read cookies from file:");
            Enumeration cks = cookie_map.keys();
            for(int i = 0; cks.hasMoreElements(); i++)
            {
                Cookie cookie = (Cookie)cks.nextElement();
                System.out.println("\tCookie " + i + ":  " + cookie.toString());
                i++;
            }

        }
        try
        {
            cookie_reader.close();
        }
        catch(IOException ioe) { }
    }

    private static boolean isValidSaveFile(File target)
    {
        if(target == null)
            return false;
        if(target.exists())
        {
            if(!target.isFile() || !target.canWrite())
                return false;
        } else
        {
            File parent = new File(target.getParent());
            if(parent == null || !parent.exists() || !parent.isDirectory() || !parent.canWrite())
                return false;
        }
        return true;
    }

    private static void saveCookies()
    {
        if(!isValidSaveFile(cookie_jar))
            return;
        Hashtable cookie_list = new Hashtable();
        for(Enumeration enum = Util.getList(cookie_cntxt_list, HTTPConnection.getDefaultContext()).elements(); enum.hasMoreElements();)
        {
            Cookie cookie = (Cookie)enum.nextElement();
            if(!cookie.discard())
                cookie_list.put(((Object) (cookie)), ((Object) (cookie)));
        }

        if(cookie_list.size() == 0)
            return;
        FileWriter cookie_writer = null;
        try
        {
            Enumeration cookie_enum = cookie_list.elements();
            cookie_writer = new FileWriter(cookie_jar);
            ((Writer) (cookie_writer)).write("#\n# This file is managed by HTTPClient\n");
            ((Writer) (cookie_writer)).write("# Please do not edit\n#\n");
            for(; cookie_enum.hasMoreElements(); ((OutputStreamWriter) (cookie_writer)).write(10))
            {
                Cookie cookie = (Cookie)cookie_enum.nextElement();
                ((Writer) (cookie_writer)).write(formatCookie(cookie));
            }

        }
        catch(IOException ioe)
        {
            return;
        }
        finally
        {
            try
            {
                ((OutputStreamWriter) (cookie_writer)).close();
            }
            catch(IOException ioe) { }
        }
    }

    private static String formatCookie(Cookie cookie)
    {
        StringBuffer sb = new StringBuffer();
        String domain = cookie.getDomain();
        sb.append(domain);
        sb.append(TAB_CHAR);
        sb.append(domain.charAt(0) != '.' ? "FALSE" : "TRUE");
        sb.append(TAB_CHAR);
        sb.append(cookie.getPath() != null ? cookie.getPath() : "");
        sb.append(TAB_CHAR);
        sb.append(cookie.isSecure() ? "TRUE" : "FALSE");
        sb.append(TAB_CHAR);
        sb.append(cookie.expires().getTime() / 1000L);
        sb.append(TAB_CHAR);
        sb.append(cookie.getName());
        sb.append(TAB_CHAR);
        sb.append(cookie.getValue() != null ? cookie.getValue() : "");
        return sb.toString();
    }

    private static Cookie parseCookie(String line)
    {
        if(line == null || line.trim().length() == 0 || line.trim().substring(0, 1).equals("#"))
            return null;
        int start = 0;
        int end = -1;
        int last = line.length();
        if((end = line.indexOf(TAB_CHAR)) == -1)
            return null;
        String domain = line.substring(start, end);
        if(domain == null || domain.trim().length() == 0)
            return null;
        start = end + 1;
        if(start == last || (end = line.indexOf(TAB_CHAR, start)) == -1)
            return null;
        String isDomainString = line.substring(start, end);
        boolean isDomain = false;
        if(isDomainString.equalsIgnoreCase("true"))
            isDomain = true;
        else
        if(!isDomainString.equalsIgnoreCase("false"))
            return null;
        start = end + 1;
        if(start == last || (end = line.indexOf(TAB_CHAR, start)) == -1)
            return null;
        String path = line.substring(start, end);
        if(path == null || path.trim().length() == 0)
            path = "/";
        start = end + 1;
        if(start == last || (end = line.indexOf(TAB_CHAR, start)) == -1)
            return null;
        String isSecureString = line.substring(start, end);
        boolean isSecure = false;
        if(isSecureString.equalsIgnoreCase("true"))
            isSecure = true;
        else
        if(!isSecureString.equalsIgnoreCase("false"))
            return null;
        start = end + 1;
        if(start == last || (end = line.indexOf(TAB_CHAR, start)) == -1)
            return null;
        String expiryString = line.substring(start, end);
        Date expiry = null;
        if(expiryString == null || expiryString.trim().length() == 0)
        {
            expiry = null;
        } else
        {
            long expiry_milliseconds = Long.parseLong(expiryString) * 1000L;
            expiry = new Date(expiry_milliseconds);
        }
        start = end + 1;
        if(start == last)
            return null;
        if((end = line.indexOf(TAB_CHAR, start)) == -1)
            end = last;
        String name = line.substring(start, end);
        if(name == null || name.trim().length() == 0)
            return null;
        String value = null;
        start = end + 1;
        if(start < last)
            value = line.substring(start);
        Cookie new_cookie = new Cookie(name, value, domain, path, expiry, isSecure);
        return new_cookie;
    }

    private static String getCookieJarName()
    {
        String file = null;
        try
        {
            file = System.getProperty("HTTPClient.cookies.jar");
        }
        catch(SecurityException se) { }
        if(cookie_debug)
            System.out.println("Cookie file name is \"" + file + "\"");
        if(file == null)
        {
            String os = System.getProperty("os.name");
            String directory = getCookieDirectory(os);
            String filename = getCookieFilename(os);
            file = directory + File.separator + filename;
            if(cookie_debug)
                System.out.println("Using cookie file name \"" + file + "\"");
        }
        return file;
    }

    private static String getCookieDirectory(String os)
    {
        if(os.equalsIgnoreCase("Windows 95") || os.equalsIgnoreCase("16-bit Windows") || os.equalsIgnoreCase("Windows"))
            return System.getProperty("java.home");
        if(os.equalsIgnoreCase("Windows NT") || os.equalsIgnoreCase("OS/2"))
            return System.getProperty("user.home");
        if(os.equalsIgnoreCase("Mac OS") || os.equalsIgnoreCase("MacOS"))
            return "System Folder" + File.separator + "Preferences";
        else
            return System.getProperty("user.home");
    }

    private static String getCookieFilename(String os)
    {
        if(os.equalsIgnoreCase("Windows 95") || os.equalsIgnoreCase("16-bit Windows") || os.equalsIgnoreCase("Windows") || os.equalsIgnoreCase("Windows NT") || os.equalsIgnoreCase("OS/2"))
            return ".httpclient_cookies";
        if(os.equalsIgnoreCase("Mac OS") || os.equalsIgnoreCase("MacOS"))
            return "HTTPClientCookies";
        else
            return ".httpclient_cookies";
    }

    CookieModule()
    {
    }

    public int requestHandler(Request req, Response resp[])
    {
        if(cookie_debug)
            System.out.println("Adding cookies to request header.");
        NVPair hdrs[] = req.getHeaders();
        int length = hdrs.length;
        for(int idx = 0; idx < hdrs.length; idx++)
        {
            int beg = idx;
            for(; idx < hdrs.length && hdrs[idx].getName().equalsIgnoreCase("Cookie"); idx++);
            if(idx - beg > 0)
            {
                length -= idx - beg;
                System.arraycopy(((Object) (hdrs)), idx, ((Object) (hdrs)), beg, length - beg);
            }
        }

        if(length < hdrs.length)
        {
            hdrs = Util.resizeArray(hdrs, length);
            req.setHeaders(hdrs);
        }
        Hashtable cookie_list = Util.getList(cookie_cntxt_list, req.getConnection().getContext());
        if(cookie_list.size() == 0)
        {
            if(cookie_debug)
                System.out.println("No cookies to be added.");
            return 0;
        }
        Vector names = new Vector();
        Vector lens = new Vector();
        boolean cookie2 = false;
        synchronized(cookie_list)
        {
            Enumeration list = cookie_list.elements();
            Vector remove_list = null;
            while(list.hasMoreElements()) 
            {
                Cookie cookie = (Cookie)list.nextElement();
                if(cookie_debug)
                    System.out.println("Checking cookie \"" + cookie + "\"");
                if(cookie.hasExpired())
                {
                    if(remove_list == null)
                        remove_list = new Vector();
                    remove_list.addElement(((Object) (cookie)));
                } else
                if(cookie.sendWith(((RoRequest) (req))) && (cookie_handler == null || cookie_handler.sendCookie(cookie, ((RoRequest) (req)))))
                {
                    int len = cookie.getPath().length();
                    int idx;
                    for(idx = 0; idx < lens.size(); idx++)
                        if(((Integer)lens.elementAt(idx)).intValue() < len)
                            break;

                    names.insertElementAt(((Object) (cookie.toExternalForm())), idx);
                    lens.insertElementAt(((Object) (new Integer(len))), idx);
                    if(cookie instanceof Cookie2)
                        cookie2 = true;
                }
            }
            if(remove_list != null)
            {
                for(int idx = 0; idx < remove_list.size(); idx++)
                    cookie_list.remove(remove_list.elementAt(idx));

            }
        }
        if(!names.isEmpty())
        {
            StringBuffer value = new StringBuffer();
            if(cookie2)
                value.append("$Version=\"1\"; ");
            value.append((String)names.elementAt(0));
            for(int idx = 1; idx < names.size(); idx++)
            {
                value.append("; ");
                value.append((String)names.elementAt(idx));
            }

            hdrs = Util.resizeArray(hdrs, hdrs.length + 1);
            hdrs[hdrs.length - 1] = new NVPair("Cookie", value.toString());
            if(!cookie2)
            {
                int idx;
                for(idx = 0; idx < hdrs.length; idx++)
                    if(hdrs[idx].getName().equalsIgnoreCase("Cookie2"))
                        break;

                if(idx == hdrs.length)
                {
                    hdrs = Util.resizeArray(hdrs, hdrs.length + 1);
                    hdrs[hdrs.length - 1] = new NVPair("Cookie2", "$Version=\"1\"");
                }
            }
            req.setHeaders(hdrs);
            if(GlobalConstants.DebugMods)
                System.err.println("CookM: Sending cookies '" + value + "'");
        }
        return 0;
    }

    public void responsePhase1Handler(Response resp, RoRequest req)
        throws IOException
    {
        String set_cookie = resp.getHeader("Set-Cookie");
        String set_cookie2 = resp.getHeader("Set-Cookie2");
        if(set_cookie == null && set_cookie2 == null)
            return;
        resp.deleteHeader("Set-Cookie");
        resp.deleteHeader("Set-Cookie2");
        if(set_cookie != null)
            handleCookie(set_cookie, false, req, resp);
        if(set_cookie2 != null)
            handleCookie(set_cookie2, true, req, resp);
    }

    public int responsePhase2Handler(Response resp, Request req)
    {
        return 10;
    }

    public void responsePhase3Handler(Response response, RoRequest rorequest)
    {
    }

    public void trailerHandler(Response resp, RoRequest req)
        throws IOException
    {
        String set_cookie = resp.getTrailer("Set-Cookie");
        String set_cookie2 = resp.getHeader("Set-Cookie2");
        if(set_cookie == null && set_cookie2 == null)
            return;
        resp.deleteTrailer("Set-Cookie");
        resp.deleteTrailer("Set-Cookie2");
        if(set_cookie != null)
            handleCookie(set_cookie, false, req, resp);
        if(set_cookie2 != null)
            handleCookie(set_cookie2, true, req, resp);
    }

    private void handleCookie(String set_cookie, boolean cookie2, RoRequest req, Response resp)
        throws ProtocolException
    {
        Cookie cookies[];
        if(cookie2)
            cookies = Cookie2.parse(set_cookie, req);
        else
            cookies = Cookie.parse(set_cookie, req);
        if(GlobalConstants.DebugMods)
        {
            System.err.println("CookM: Received and parsed " + cookies.length + " cookies:");
            for(int idx = 0; idx < cookies.length; idx++)
                System.err.println("CookM: Cookie " + idx + ": " + cookies[idx]);

        }
        Hashtable cookie_list = Util.getList(cookie_cntxt_list, req.getConnection().getContext());
        synchronized(cookie_list)
        {
            for(int idx = 0; idx < cookies.length; idx++)
            {
                Cookie cookie = (Cookie)cookie_list.get(((Object) (cookies[idx])));
                if(cookie != null && cookies[idx].hasExpired())
                    cookie_list.remove(((Object) (cookie)));
                else
                if(cookie_handler == null || cookie_handler.acceptCookie(cookies[idx], req, ((RoResponse) (resp))))
                    cookie_list.put(((Object) (cookies[idx])), ((Object) (cookies[idx])));
            }

        }
    }

    public static void discardAllCookies()
    {
        cookie_cntxt_list.clear();
    }

    public static void discardAllCookies(Object context)
    {
        Hashtable cookie_list = Util.getList(cookie_cntxt_list, context);
        cookie_list.clear();
    }

    public static Cookie[] listAllCookies()
    {
        Cookie acookie[];
        synchronized(cookie_cntxt_list)
        {
            Cookie cookies[] = new Cookie[0];
            int idx = 0;
            for(Enumeration cntxt_list = cookie_cntxt_list.elements(); cntxt_list.hasMoreElements();)
            {
                Hashtable cntxt = (Hashtable)cntxt_list.nextElement();
                synchronized(cntxt)
                {
                    cookies = Util.resizeArray(cookies, idx + cntxt.size());
                    for(Enumeration cookie_list = cntxt.elements(); cookie_list.hasMoreElements();)
                        cookies[idx++] = (Cookie)cookie_list.nextElement();

                }
            }

            acookie = cookies;
        }
        return acookie;
    }

    public static Cookie[] listAllCookies(Object context)
    {
        Hashtable cookie_list = Util.getList(cookie_cntxt_list, context);
        Cookie acookie[];
        synchronized(cookie_list)
        {
            Cookie cookies[] = new Cookie[cookie_list.size()];
            int idx = 0;
            for(Enumeration enum = cookie_list.elements(); enum.hasMoreElements();)
                cookies[idx++] = (Cookie)enum.nextElement();

            acookie = cookies;
        }
        return acookie;
    }

    public static void addCookie(Cookie cookie)
    {
        addCookie(cookie, HTTPConnection.getDefaultContext());
    }

    public static void addCookie(Cookie cookie, Object context)
    {
        Hashtable cookie_list = Util.getList(cookie_cntxt_list, context);
        cookie_list.put(((Object) (cookie)), ((Object) (cookie)));
        saveCookies();
    }

    public static void removeCookie(Cookie cookie)
    {
        removeCookie(cookie, HTTPConnection.getDefaultContext());
    }

    public static void removeCookie(Cookie cookie, Object context)
    {
        Hashtable cookie_list = Util.getList(cookie_cntxt_list, context);
        cookie_list.remove(((Object) (cookie)));
        saveCookies();
    }

    public static synchronized CookiePolicyHandler setCookiePolicyHandler(CookiePolicyHandler handler)
    {
        CookiePolicyHandler old = cookie_handler;
        cookie_handler = handler;
        return old;
    }

    static 
    {
        cookie_debug = false;
        save_cookies = false;
        try
        {
            cookie_debug = Boolean.getBoolean("HTTPClient.cookies.debug");
            save_cookies = Boolean.getBoolean("HTTPClient.cookies.save");
        }
        catch(Exception e)
        {
            save_cookies = false;
        }
        if(cookie_debug)
            System.out.println("Are we saving cookies? (" + save_cookies + ")");
        if(save_cookies)
        {
            if(cookie_debug)
                System.out.println("\tCookies are saved; retrieving...");
            loadCookies();
            cookieSaver = ((Object) (new Object() {

                public void finalize()
                {
                    CookieModule.saveCookies();
                }

            }));
            try
            {
                System.runFinalizersOnExit(true);
            }
            catch(Throwable t) { }
        }
    }

}
