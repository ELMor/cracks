// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CIHashtable.java

package HTTPClient;

import java.util.Enumeration;
import java.util.Hashtable;

// Referenced classes of package HTTPClient:
//            CIString, CIHashtableEnumeration

class CIHashtable extends Hashtable
{

    public CIHashtable(int initialCapacity, float loadFactor)
    {
        super(initialCapacity, loadFactor);
    }

    public CIHashtable(int initialCapacity)
    {
        super(initialCapacity);
    }

    public CIHashtable()
    {
    }

    public Object get(String key)
    {
        return super.get(((Object) (new CIString(key))));
    }

    public Object put(String key, Object value)
    {
        return super.put(((Object) (new CIString(key))), value);
    }

    public boolean containsKey(String key)
    {
        return super.contains(((Object) (new CIString(key))));
    }

    public Object remove(String key)
    {
        return super.remove(((Object) (new CIString(key))));
    }

    public Enumeration keys()
    {
        return ((Enumeration) (new CIHashtableEnumeration(super.keys())));
    }
}
