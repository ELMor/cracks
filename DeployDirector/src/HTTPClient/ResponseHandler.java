// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ResponseHandler.java

package HTTPClient;

import java.io.IOException;
import java.io.PrintStream;

// Referenced classes of package HTTPClient:
//            RespInputStream, GlobalConstants, ParseException, Response, 
//            Util, ExtBufferedInputStream, Request, StreamDemultiplexor

final class ResponseHandler
    implements GlobalConstants
{

    RespInputStream stream;
    Response resp;
    Request request;
    boolean eof;
    IOException exception;
    private byte endbndry[];
    private int end_cmp[];

    ResponseHandler(Response resp, Request request, StreamDemultiplexor demux)
    {
        eof = false;
        exception = null;
        endbndry = null;
        end_cmp = null;
        this.resp = resp;
        this.request = request;
        stream = new RespInputStream(demux, this);
        if(GlobalConstants.DebugDemux)
            System.err.println("Demux: Opening stream " + ((Object) (stream)).hashCode() + " (" + " (" + ((Object) (demux)).hashCode() + ") (" + Thread.currentThread() + ")");
    }

    byte[] getEndBoundary(ExtBufferedInputStream MasterStream)
        throws IOException, ParseException
    {
        if(endbndry == null)
            setupBoundary(MasterStream);
        return endbndry;
    }

    int[] getEndCompiled(ExtBufferedInputStream MasterStream)
        throws IOException, ParseException
    {
        if(end_cmp == null)
            setupBoundary(MasterStream);
        return end_cmp;
    }

    void setupBoundary(ExtBufferedInputStream MasterStream)
        throws IOException, ParseException
    {
        String endstr = "--" + Util.getParameter("boundary", resp.getHeader("Content-Type")) + "--\r\n";
        endbndry = new byte[endstr.length()];
        endstr.getBytes(0, endbndry.length, endbndry, 0);
        end_cmp = Util.compile_search(endbndry);
        MasterStream.initMark();
    }
}
