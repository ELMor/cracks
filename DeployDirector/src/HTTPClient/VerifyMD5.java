// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ContentMD5Module.java

package HTTPClient;

import java.io.IOException;
import java.io.PrintStream;

// Referenced classes of package HTTPClient:
//            HashVerifier, GlobalConstants, RoResponse, Codecs

class VerifyMD5
    implements HashVerifier, GlobalConstants
{

    RoResponse resp;

    public VerifyMD5(RoResponse resp)
    {
        this.resp = resp;
    }

    public void verifyHash(byte hash[], long len)
        throws IOException
    {
        String hdr;
        try
        {
            if((hdr = resp.getHeader("Content-MD5")) == null)
                hdr = resp.getTrailer("Content-MD5");
        }
        catch(IOException ioe)
        {
            return;
        }
        if(hdr == null)
            return;
        hdr = hdr.trim();
        byte ContMD5[] = new byte[hdr.length()];
        hdr.getBytes(0, ContMD5.length, ContMD5, 0);
        ContMD5 = Codecs.base64Decode(ContMD5);
        for(int idx = 0; idx < hash.length; idx++)
            if(hash[idx] != ContMD5[idx])
                throw new IOException("MD5-Digest mismatch: expected " + hex(ContMD5) + " but calculated " + hex(hash));

        if(GlobalConstants.DebugMods)
            System.err.println("CMD5M: hash successfully verified");
    }

    private static String hex(byte buf[])
    {
        StringBuffer str = new StringBuffer(buf.length * 3);
        for(int idx = 0; idx < buf.length; idx++)
        {
            str.append(Character.forDigit(buf[idx] >>> 4 & 0xf, 16));
            str.append(Character.forDigit(buf[idx] & 0xf, 16));
            str.append(':');
        }

        str.setLength(str.length() - 1);
        return str.toString();
    }
}
