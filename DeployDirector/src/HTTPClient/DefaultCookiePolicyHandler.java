// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CookieModule.java

package HTTPClient;


// Referenced classes of package HTTPClient:
//            BasicCookieBox, CookiePolicyHandler, Util, RoRequest, 
//            HTTPConnection, Cookie, RoResponse

class DefaultCookiePolicyHandler
    implements CookiePolicyHandler
{

    private String accept_domains[];
    private String reject_domains[];
    private BasicCookieBox popup;

    DefaultCookiePolicyHandler()
    {
        accept_domains = new String[0];
        reject_domains = new String[0];
        popup = null;
        String list;
        try
        {
            list = System.getProperty("HTTPClient.cookies.hosts.accept");
        }
        catch(Exception e)
        {
            list = null;
        }
        String domains[] = Util.splitProperty(list);
        for(int idx = 0; idx < domains.length; idx++)
            addAcceptDomain(domains[idx].toLowerCase());

        try
        {
            list = System.getProperty("HTTPClient.cookies.hosts.reject");
        }
        catch(Exception e)
        {
            list = null;
        }
        domains = Util.splitProperty(list);
        for(int idx = 0; idx < domains.length; idx++)
            addRejectDomain(domains[idx].toLowerCase());

    }

    public boolean acceptCookie(Cookie cookie, RoRequest req, RoResponse resp)
    {
        String server = req.getConnection().getHost();
        if(server.indexOf('.') == -1)
            server = server + ".local";
        for(int idx = 0; idx < reject_domains.length; idx++)
            if(reject_domains[idx].length() == 0 || reject_domains[idx].charAt(0) == '.' && server.endsWith(reject_domains[idx]) || reject_domains[idx].charAt(0) != '.' && server.equals(((Object) (reject_domains[idx]))))
                return false;

        for(int idx = 0; idx < accept_domains.length; idx++)
            if(accept_domains[idx].length() == 0 || accept_domains[idx].charAt(0) == '.' && server.endsWith(accept_domains[idx]) || accept_domains[idx].charAt(0) != '.' && server.equals(((Object) (accept_domains[idx]))))
                return true;

        if(!req.allowUI())
            return true;
        if(popup == null)
            popup = new BasicCookieBox();
        return popup.accept(cookie, this, server);
    }

    public boolean sendCookie(Cookie cookie, RoRequest req)
    {
        return true;
    }

    void addAcceptDomain(String domain)
    {
        if(domain.indexOf('.') == -1)
            domain = domain + ".local";
        for(int idx = 0; idx < accept_domains.length; idx++)
        {
            if(domain.endsWith(accept_domains[idx]))
                return;
            if(accept_domains[idx].endsWith(domain))
            {
                accept_domains[idx] = domain;
                return;
            }
        }

        accept_domains = Util.resizeArray(accept_domains, accept_domains.length + 1);
        accept_domains[accept_domains.length - 1] = domain;
    }

    void addRejectDomain(String domain)
    {
        if(domain.indexOf('.') == -1)
            domain = domain + ".local";
        for(int idx = 0; idx < reject_domains.length; idx++)
        {
            if(domain.endsWith(reject_domains[idx]))
                return;
            if(reject_domains[idx].endsWith(domain))
            {
                reject_domains[idx] = domain;
                return;
            }
        }

        reject_domains = Util.resizeArray(reject_domains, reject_domains.length + 1);
        reject_domains[reject_domains.length - 1] = domain;
    }
}
