// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   HttpOutputStream.java

package HTTPClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

// Referenced classes of package HTTPClient:
//            ModuleException, NVPair, GlobalConstants, Request, 
//            HTTPConnection, Codecs, Util, Response

public class HttpOutputStream extends OutputStream
    implements GlobalConstants
{

    private static final NVPair empty[] = new NVPair[0];
    private int length;
    private int rcvd;
    private Request req;
    private Response resp;
    private OutputStream os;
    private ByteArrayOutputStream bos;
    private NVPair trailers[];
    private int con_to;
    private boolean ignore;

    public HttpOutputStream()
    {
        rcvd = 0;
        req = null;
        resp = null;
        os = null;
        bos = null;
        trailers = empty;
        con_to = 0;
        ignore = false;
        length = -1;
    }

    public HttpOutputStream(int length)
    {
        rcvd = 0;
        req = null;
        resp = null;
        os = null;
        bos = null;
        trailers = empty;
        con_to = 0;
        ignore = false;
        if(length < 0)
        {
            throw new IllegalArgumentException("Length must be greater equal 0");
        } else
        {
            this.length = length;
            return;
        }
    }

    void goAhead(Request req, OutputStream os, int con_to)
    {
        this.req = req;
        this.os = os;
        this.con_to = con_to;
        if(os == null)
            bos = new ByteArrayOutputStream();
        if(GlobalConstants.DebugConn)
        {
            System.err.println("OutS:  Stream ready for writing");
            if(bos != null)
                System.err.println("OutS:  Buffering all data before sending request");
        }
    }

    void ignoreData(Request req)
    {
        this.req = req;
        ignore = true;
    }

    synchronized Response getResponse()
    {
        while(resp == null) 
            try
            {
                ((Object)this).wait();
            }
            catch(InterruptedException ie) { }
        return resp;
    }

    public int getLength()
    {
        return length;
    }

    public NVPair[] getTrailers()
    {
        return trailers;
    }

    public void setTrailers(NVPair trailers[])
    {
        if(trailers != null)
            this.trailers = trailers;
        else
            this.trailers = empty;
    }

    public void write(int b)
        throws IOException, IllegalAccessError
    {
        byte tmp[] = {
            (byte)b
        };
        write(tmp, 0, 1);
    }

    public synchronized void write(byte buf[], int off, int len)
        throws IOException, IllegalAccessError
    {
        if(req == null)
            throw new IllegalAccessError("Stream not associated with a request");
        if(ignore)
            return;
        if(length != -1 && rcvd + len > length)
        {
            IOException ioe = new IOException("Tried to write too many bytes (" + (rcvd + len) + " > " + length + ")");
            req.getConnection().closeDemux(ioe, false);
            req.getConnection().outputFinished();
            throw ioe;
        }
        try
        {
            if(bos != null)
                bos.write(buf, off, len);
            else
            if(length != -1)
                os.write(buf, off, len);
            else
                os.write(Codecs.chunkedEncode(buf, off, len, ((NVPair []) (null)), false));
        }
        catch(IOException ioe)
        {
            req.getConnection().closeDemux(ioe, true);
            req.getConnection().outputFinished();
            throw ioe;
        }
        rcvd += len;
    }

    public synchronized void close()
        throws IOException, IllegalAccessError
    {
        if(req == null)
            throw new IllegalAccessError("Stream not associated with a request");
        if(ignore)
            return;
        if(bos != null)
        {
            req.setData(bos.toByteArray());
            req.setStream(((HttpOutputStream) (null)));
            bos = null;
            bos = new ByteArrayOutputStream();
            if(trailers.length > 0)
            {
                NVPair hdrs[] = req.getHeaders();
                int len = hdrs.length;
                for(int idx = 0; idx < len; idx++)
                    if(hdrs[idx].getName().equalsIgnoreCase("Trailer"))
                    {
                        System.arraycopy(((Object) (hdrs)), idx + 1, ((Object) (hdrs)), idx, len - idx - 1);
                        len--;
                    }

                hdrs = Util.resizeArray(hdrs, len + trailers.length);
                System.arraycopy(((Object) (trailers)), 0, ((Object) (hdrs)), len, trailers.length);
                req.setHeaders(hdrs);
            }
            if(GlobalConstants.DebugConn)
                System.err.println("OutS:  Sending request");
            try
            {
                resp = req.getConnection().sendRequest(req, con_to);
            }
            catch(ModuleException me)
            {
                throw new IOException(((Throwable) (me)).toString());
            }
            req.setData(((byte []) (null)));
            ((Object)this).notify();
        } else
        {
            if(rcvd < length)
            {
                IOException ioe = new IOException("Premature close: only " + rcvd + " bytes written instead of the " + "expected " + length);
                req.getConnection().closeDemux(ioe, false);
                req.getConnection().outputFinished();
                throw ioe;
            }
            try
            {
                if(length == -1)
                {
                    if(GlobalConstants.DebugConn && trailers.length > 0)
                    {
                        System.err.println("OutS:  Sending trailers:");
                        for(int idx = 0; idx < trailers.length; idx++)
                            System.err.println("       " + trailers[idx].getName() + ": " + trailers[idx].getValue());

                    }
                    os.write(Codecs.chunkedEncode(((byte []) (null)), 0, 0, trailers, true));
                }
                os.flush();
                if(GlobalConstants.DebugConn)
                    System.err.println("OutS:  All data sent");
            }
            catch(IOException ioe)
            {
                req.getConnection().closeDemux(ioe, true);
                throw ioe;
            }
            finally
            {
                req.getConnection().outputFinished();
            }
        }
    }

    public String toString()
    {
        return ((Object)this).getClass().getName() + "[length=" + length + "]";
    }

}
