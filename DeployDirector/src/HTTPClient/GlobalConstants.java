// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   GlobalConstants.java

package HTTPClient;


interface GlobalConstants
{

    public static final boolean DebugAll = false;
    public static final boolean DebugConn = false | false;
    public static final boolean DebugResp = false | false;
    public static final boolean DebugDemux = false | false;
    public static final boolean DebugAuth = false | false;
    public static final boolean DebugSocks = false | false;
    public static final boolean DebugMods = false | false;
    public static final boolean DebugURLC = false | false;
    public static final int HTTP = 0;
    public static final int HTTPS = 1;
    public static final int SHTTP = 2;
    public static final int HTTP_NG = 3;
    public static final int HTTP_1_0 = 0x10000;
    public static final int HTTP_1_1 = 0x10001;
    public static final int CD_NONE = 0;
    public static final int CD_HDRS = 1;
    public static final int CD_0 = 2;
    public static final int CD_CLOSE = 3;
    public static final int CD_CONTLEN = 4;
    public static final int CD_CHUNKED = 5;
    public static final int CD_MP_BR = 6;

}
