// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RedirectionModule.java

package HTTPClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ProtocolException;
import java.util.Hashtable;

// Referenced classes of package HTTPClient:
//            URI, ParseException, HTTPConnection, NVPair, 
//            HTTPClientModule, GlobalConstants, Request, Util, 
//            Response, RoRequest

class RedirectionModule
    implements HTTPClientModule, GlobalConstants
{

    private static Hashtable perm_redir_cntxt_list = new Hashtable();
    private int level;
    private URI lastURI;

    RedirectionModule()
    {
        level = 0;
        lastURI = null;
    }

    public int requestHandler(Request req, Response resp[])
    {
        HTTPConnection con = req.getConnection();
        URI cur_loc;
        try
        {
            cur_loc = new URI(con.getProtocol(), con.getHost(), con.getPort(), req.getRequestURI());
        }
        catch(ParseException pe)
        {
            throw new Error("HTTPClient Internal Error: unexpected exception '" + pe + "'");
        }
        Hashtable perm_redir_list = Util.getList(perm_redir_cntxt_list, req.getConnection().getContext());
        URI new_loc;
        if((new_loc = (URI)perm_redir_list.get(((Object) (cur_loc)))) != null)
        {
            String nres = new_loc.getPath();
            req.setRequestURI(nres);
            try
            {
                lastURI = new URI(new_loc, nres);
            }
            catch(ParseException pe) { }
            if(GlobalConstants.DebugMods)
                System.err.println("RdirM: matched request in permanent redirection list - redoing request to " + lastURI);
            if(!sameServer(con, new_loc))
            {
                try
                {
                    con = new HTTPConnection(new_loc.toURL());
                }
                catch(Exception e)
                {
                    throw new Error("HTTPClient Internal Error: unexpected exception '" + e + "'");
                }
                con.setContext(req.getConnection().getContext());
                req.setConnection(con);
                return 5;
            } else
            {
                return 1;
            }
        } else
        {
            return 0;
        }
    }

    public void responsePhase1Handler(Response resp, RoRequest req)
        throws IOException
    {
        int sts = resp.getStatusCode();
        if((sts < 301 || sts > 307 || sts == 304) && lastURI != null)
            resp.setEffectiveURI(lastURI);
    }

    public int responsePhase2Handler(Response resp, Request req)
        throws IOException
    {
        int sts = resp.getStatusCode();
        switch(sts)
        {
        case 302: 
            if(GlobalConstants.DebugMods)
                System.err.println("RdirM: Received status: " + sts + " " + resp.getReasonLine() + " - treating as 303");
            sts = 303;
            // fall through

        case 301: 
        case 303: 
        case 307: 
            if(GlobalConstants.DebugMods)
                System.err.println("RdirM: Handling status: " + sts + " " + resp.getReasonLine());
            if(!req.getMethod().equals("GET") && !req.getMethod().equals("HEAD") && sts != 303)
            {
                if(GlobalConstants.DebugMods)
                    System.err.println("RdirM: not redirected because method is neither HEAD nor GET");
                if(sts == 301 && resp.getHeader("Location") != null)
                    update_perm_redir_list(((RoRequest) (req)), resLocHdr(resp.getHeader("Location"), ((RoRequest) (req))));
                resp.setEffectiveURI(lastURI);
                return 10;
            }
            // fall through

        case 305: 
        case 306: 
            if(GlobalConstants.DebugMods && (sts == 305 || sts == 306))
                System.err.println("RdirM: Handling status: " + sts + " " + resp.getReasonLine());
            if(sts == 305 && req.getConnection().getProxyHost() != null)
            {
                if(GlobalConstants.DebugMods)
                    System.err.println("RdirM: 305 ignored because a proxy is already in use");
                resp.setEffectiveURI(lastURI);
                return 10;
            }
            if(level == 15 || resp.getHeader("Location") == null)
            {
                if(GlobalConstants.DebugMods)
                    if(level == 15)
                        System.err.println("RdirM: not redirected because of too many levels of redirection");
                    else
                        System.err.println("RdirM: not redirected because no Location header was present");
                resp.setEffectiveURI(lastURI);
                return 10;
            }
            level++;
            URI loc = resLocHdr(resp.getHeader("Location"), ((RoRequest) (req)));
            if(req.getStream() != null && (sts == 306 || sts == 305))
                return 10;
            boolean new_con = false;
            HTTPConnection mvd;
            String nres;
            if(sts == 305)
            {
                mvd = new HTTPConnection(req.getConnection().getProtocol(), req.getConnection().getHost(), req.getConnection().getPort());
                mvd.setCurrentProxy(loc.getHost(), loc.getPort());
                mvd.setContext(req.getConnection().getContext());
                new_con = true;
                nres = req.getRequestURI();
                req.setMethod("GET");
                req.setData(((byte []) (null)));
                req.setStream(((HttpOutputStream) (null)));
            } else
            {
                if(sts == 306)
                    return 10;
                if(sameServer(req.getConnection(), loc))
                {
                    mvd = req.getConnection();
                    nres = loc.getPath();
                } else
                {
                    try
                    {
                        mvd = new HTTPConnection(loc.toURL());
                        nres = loc.getPath();
                    }
                    catch(Exception e)
                    {
                        if(req.getConnection().getProxyHost() == null || !loc.getScheme().equalsIgnoreCase("ftp"))
                            return 10;
                        mvd = new HTTPConnection("http", req.getConnection().getProxyHost(), req.getConnection().getProxyPort());
                        mvd.setCurrentProxy(((String) (null)), 0);
                        nres = loc.toExternalForm();
                    }
                    mvd.setContext(req.getConnection().getContext());
                    new_con = true;
                }
                if(sts == 303 && !req.getMethod().equals("HEAD"))
                {
                    req.setMethod("GET");
                    req.setData(((byte []) (null)));
                    req.setStream(((HttpOutputStream) (null)));
                } else
                if(sts == 301)
                    try
                    {
                        update_perm_redir_list(((RoRequest) (req)), new URI(loc, nres));
                    }
                    catch(ParseException pe) { }
                NVPair hdrs[] = req.getHeaders();
                for(int idx = 0; idx < hdrs.length; idx++)
                {
                    if(!hdrs[idx].getName().equalsIgnoreCase("Referer"))
                        continue;
                    HTTPConnection con = req.getConnection();
                    hdrs[idx] = new NVPair("Referer", con + req.getRequestURI());
                    break;
                }

            }
            req.setConnection(mvd);
            req.setRequestURI(nres);
            try
            {
                resp.getInputStream().close();
            }
            catch(IOException ioe) { }
            if(sts != 305 && sts != 306)
            {
                try
                {
                    lastURI = new URI(loc, nres);
                }
                catch(ParseException pe) { }
                if(GlobalConstants.DebugMods)
                    System.err.println("RdirM: request redirected to " + lastURI + " using method " + req.getMethod());
            } else
            if(GlobalConstants.DebugMods)
                System.err.println("RdirM: resending request using proxy " + mvd.getProxyHost() + ":" + mvd.getProxyPort());
            return !new_con ? 13 : 15;

        case 304: 
        default:
            return 10;
        }
    }

    public void responsePhase3Handler(Response response, RoRequest rorequest)
    {
    }

    public void trailerHandler(Response response, RoRequest rorequest)
    {
    }

    private static void update_perm_redir_list(RoRequest req, URI new_loc)
    {
        HTTPConnection con = req.getConnection();
        URI cur_loc = null;
        try
        {
            cur_loc = new URI(con.getProtocol(), con.getHost(), con.getPort(), req.getRequestURI());
        }
        catch(ParseException pe) { }
        if(!cur_loc.equals(((Object) (new_loc))))
        {
            Hashtable perm_redir_list = Util.getList(perm_redir_cntxt_list, con.getContext());
            perm_redir_list.put(((Object) (cur_loc)), ((Object) (new_loc)));
        }
    }

    private URI resLocHdr(String loc, RoRequest req)
        throws ProtocolException
    {
        try
        {
            return new URI(loc);
        }
        catch(ParseException pe) { }
        try
        {
            URI base = new URI(req.getConnection().getProtocol(), req.getConnection().getHost(), req.getConnection().getPort(), req.getRequestURI());
            return new URI(base, loc);
        }
        catch(ParseException pe2)
        {
            throw new ProtocolException("Malformed URL in Location header: " + loc);
        }
    }

    private boolean sameServer(HTTPConnection con, URI url)
    {
        if(!url.getScheme().equalsIgnoreCase(con.getProtocol()))
            return false;
        if(!url.getHost().equalsIgnoreCase(con.getHost()))
            return false;
        return url.getPort() == con.getPort();
    }

}
