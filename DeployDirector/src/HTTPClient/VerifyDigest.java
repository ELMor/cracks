// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefaultAuthHandler.java

package HTTPClient;

import java.io.IOException;
import java.io.PrintStream;

// Referenced classes of package HTTPClient:
//            ParseException, MD5, HashVerifier, GlobalConstants, 
//            RoResponse, Util, HttpHeaderElement, DefaultAuthHandler

class VerifyDigest
    implements HashVerifier, GlobalConstants
{

    private String HA1;
    private String nonce;
    private String method;
    private String uri;
    private String hdr;
    private RoResponse resp;

    public VerifyDigest(String HA1, String nonce, String method, String uri, String hdr, RoResponse resp)
    {
        this.HA1 = HA1;
        this.nonce = nonce;
        this.method = method;
        this.uri = uri;
        this.hdr = hdr;
        this.resp = resp;
    }

    public void verifyHash(byte hash[], long len)
        throws IOException
    {
        String auth_info = resp.getHeader(hdr);
        if(auth_info == null)
            auth_info = resp.getTrailer(hdr);
        if(auth_info == null)
            return;
        java.util.Vector pai;
        try
        {
            pai = Util.parseHeader(auth_info);
        }
        catch(ParseException pe)
        {
            throw new IOException(((Throwable) (pe)).toString());
        }
        HttpHeaderElement elem = Util.getElement(pai, "digest");
        if(elem == null || elem.getValue() == null)
            return;
        byte digest[] = DefaultAuthHandler.unHex(elem.getValue());
        String entity_info = (new MD5(((Object) (uri + ":" + header_val("Content-Type", resp) + ":" + header_val("Content-Length", resp) + ":" + header_val("Content-Encoding", resp) + ":" + header_val("Last-Modified", resp) + ":" + header_val("Expires", resp))))).asHex();
        hash = (new MD5(((Object) (HA1 + ":" + nonce + ":" + method + ":" + header_val("Date", resp) + ":" + entity_info + ":" + MD5.asHex(hash))))).Final();
        for(int idx = 0; idx < hash.length; idx++)
            if(hash[idx] != digest[idx])
                throw new IOException("MD5-Digest mismatch: expected " + DefaultAuthHandler.hex(digest) + " but calculated " + DefaultAuthHandler.hex(hash));

        if(GlobalConstants.DebugAuth)
            System.err.println("Auth:  digest from " + hdr + " successfully verified");
    }

    private static final String header_val(String hdr_name, RoResponse resp)
        throws IOException
    {
        String hdr = resp.getHeader(hdr_name);
        String tlr = resp.getTrailer(hdr_name);
        return hdr == null ? tlr == null ? "" : tlr : hdr;
    }
}
