// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IdempotentSequence.java

package HTTPClient;

import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Hashtable;

// Referenced classes of package HTTPClient:
//            Request, Util

class IdempotentSequence
{

    private static final int UNKNOWN = 0;
    private static final int HEAD = 1;
    private static final int GET = 2;
    private static final int POST = 3;
    private static final int PUT = 4;
    private static final int DELETE = 5;
    private static final int OPTIONS = 6;
    private static final int TRACE = 7;
    private int m_history[];
    private String r_history[];
    private int m_len;
    private int r_len;
    private boolean analysis_done;
    private Hashtable threads;
    private static final Object INDET = new Object();

    public IdempotentSequence()
    {
        analysis_done = false;
        threads = new Hashtable();
        m_history = new int[10];
        r_history = new String[10];
        m_len = 0;
        r_len = 0;
    }

    public void add(Request req)
    {
        if(m_len >= m_history.length)
            m_history = Util.resizeArray(m_history, m_history.length + 10);
        m_history[m_len++] = methodNum(req.getMethod());
        if(r_len >= r_history.length)
            r_history = Util.resizeArray(r_history, r_history.length + 10);
        r_history[r_len++] = req.getRequestURI();
    }

    public boolean isIdempotent(Request req)
    {
        if(!analysis_done)
            do_analysis();
        return ((Boolean)threads.get(((Object) (req.getRequestURI())))).booleanValue();
    }

    private void do_analysis()
    {
        for(int idx = 0; idx < r_len; idx++)
        {
            Object t_state = threads.get(((Object) (r_history[idx])));
            if(m_history[idx] == 0)
                threads.put(((Object) (r_history[idx])), ((Object) (Boolean.FALSE)));
            else
            if(t_state == null)
            {
                if(methodHasSideEffects(m_history[idx]) && methodIsComplete(m_history[idx]))
                    threads.put(((Object) (r_history[idx])), ((Object) (Boolean.TRUE)));
                else
                    threads.put(((Object) (r_history[idx])), INDET);
            } else
            if(t_state == INDET && methodHasSideEffects(m_history[idx]))
                threads.put(((Object) (r_history[idx])), ((Object) (Boolean.FALSE)));
        }

        for(Enumeration te = threads.keys(); te.hasMoreElements();)
        {
            String res = (String)te.nextElement();
            if(threads.get(((Object) (res))) == INDET)
                threads.put(((Object) (res)), ((Object) (Boolean.TRUE)));
        }

    }

    public static boolean methodIsIdempotent(String method)
    {
        return methodIsIdempotent(methodNum(method));
    }

    private static boolean methodIsIdempotent(int method)
    {
        switch(method)
        {
        case 1: // '\001'
        case 2: // '\002'
        case 4: // '\004'
        case 5: // '\005'
        case 6: // '\006'
        case 7: // '\007'
            return true;

        case 3: // '\003'
        default:
            return false;
        }
    }

    public static boolean methodIsComplete(String method)
    {
        return methodIsComplete(methodNum(method));
    }

    private static boolean methodIsComplete(int method)
    {
        switch(method)
        {
        case 1: // '\001'
        case 2: // '\002'
        case 4: // '\004'
        case 5: // '\005'
        case 6: // '\006'
        case 7: // '\007'
            return true;

        case 3: // '\003'
        default:
            return false;
        }
    }

    public static boolean methodHasSideEffects(String method)
    {
        return methodHasSideEffects(methodNum(method));
    }

    private static boolean methodHasSideEffects(int method)
    {
        switch(method)
        {
        case 1: // '\001'
        case 2: // '\002'
        case 6: // '\006'
        case 7: // '\007'
            return false;

        case 3: // '\003'
        case 4: // '\004'
        case 5: // '\005'
        default:
            return true;
        }
    }

    private static int methodNum(String method)
    {
        if(method.equals("GET"))
            return 2;
        if(method.equals("POST"))
            return 3;
        if(method.equals("HEAD"))
            return 1;
        if(method.equals("PUT"))
            return 4;
        if(method.equals("DELETE"))
            return 5;
        if(method.equals("OPTIONS"))
            return 6;
        return !method.equals("TRACE") ? 0 : 7;
    }

    public static void main(String args[])
    {
        IdempotentSequence seq = new IdempotentSequence();
        seq.add(new Request(((HTTPConnection) (null)), "GET", "/b1", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "PUT", "/b2", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "GET", "/b1", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "PUT", "/b3", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "GET", "/b2", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "PUT", "/b3", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "GET", "/b1", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "TRACE", "/b4", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "LINK", "/b4", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "GET", "/b4", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "PUT", "/b5", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "HEAD", "/b5", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "PUT", "/b5", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "GET", "/b6", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "DELETE", "/b6", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "HEAD", "/b6", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "OPTIONS", "/b7", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "TRACE", "/b7", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "GET", "/b7", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        seq.add(new Request(((HTTPConnection) (null)), "PUT", "/b7", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false));
        if(!seq.isIdempotent(new Request(((HTTPConnection) (null)), ((String) (null)), "/b1", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false)))
            System.err.println("Sequence b1 failed");
        if(!seq.isIdempotent(new Request(((HTTPConnection) (null)), ((String) (null)), "/b2", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false)))
            System.err.println("Sequence b2 failed");
        if(!seq.isIdempotent(new Request(((HTTPConnection) (null)), ((String) (null)), "/b3", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false)))
            System.err.println("Sequence b3 failed");
        if(seq.isIdempotent(new Request(((HTTPConnection) (null)), ((String) (null)), "/b4", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false)))
            System.err.println("Sequence b4 failed");
        if(!seq.isIdempotent(new Request(((HTTPConnection) (null)), ((String) (null)), "/b5", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false)))
            System.err.println("Sequence b5 failed");
        if(seq.isIdempotent(new Request(((HTTPConnection) (null)), ((String) (null)), "/b6", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false)))
            System.err.println("Sequence b6 failed");
        if(seq.isIdempotent(new Request(((HTTPConnection) (null)), ((String) (null)), "/b7", ((NVPair []) (null)), ((byte []) (null)), ((HttpOutputStream) (null)), false)))
            System.err.println("Sequence b7 failed");
        System.out.println("Tests finished");
    }

}
