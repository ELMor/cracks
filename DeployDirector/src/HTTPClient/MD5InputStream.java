// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   MD5InputStream.java

package HTTPClient;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

// Referenced classes of package HTTPClient:
//            MD5, HashVerifier

class MD5InputStream extends FilterInputStream
{

    private HashVerifier verifier;
    private MD5 md5;
    private long rcvd;
    private boolean closed;

    public MD5InputStream(InputStream is, HashVerifier verifier)
    {
        super(is);
        rcvd = 0L;
        closed = false;
        this.verifier = verifier;
        md5 = new MD5();
    }

    public synchronized int read()
        throws IOException
    {
        int b = super.in.read();
        if(b != -1)
            md5.Update((byte)b);
        else
            real_close();
        rcvd++;
        return b;
    }

    public synchronized int read(byte buf[], int off, int len)
        throws IOException
    {
        int num = super.in.read(buf, off, len);
        if(num > 0)
            md5.Update(buf, off, num);
        else
            real_close();
        rcvd += num;
        return num;
    }

    public synchronized long skip(long num)
        throws IOException
    {
        byte tmp[] = new byte[(int)num];
        int got = read(tmp, 0, (int)num);
        if(got > 0)
            return (long)got;
        else
            return 0L;
    }

    public synchronized void close()
        throws IOException
    {
        while(skip(10000L) > 0L) ;
        real_close();
    }

    private void real_close()
        throws IOException
    {
        if(closed)
        {
            return;
        } else
        {
            closed = true;
            super.in.close();
            verifier.verifyHash(md5.Final(), rcvd);
            return;
        }
    }
}
