// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StringSort.java

package com.sitraka.licensing;

import java.io.PrintStream;
import java.text.CollationKey;
import java.text.Collator;

public class StringSort
{

    public StringSort()
    {
    }

    public static void sort(String stringArray[])
    {
        Collator collator = Collator.getInstance();
        CollationKey keys[] = new CollationKey[stringArray.length];
        for(int i = 0; i < stringArray.length; i++)
            keys[i] = collator.getCollationKey(stringArray[i]);

        sort(keys);
        for(int i = 0; i < stringArray.length; i++)
            stringArray[i] = keys[i].getSourceString();

    }

    protected static void sort(CollationKey a[])
    {
        CollationKey T = null;
        int limit = a.length;
        for(int st = -1; st < limit;)
        {
            boolean flipped = false;
            st++;
            limit--;
            for(int j = st; j < limit; j++)
                if(a[j].compareTo(a[j + 1]) > 0)
                {
                    T = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = T;
                    flipped = true;
                }

            if(!flipped)
                return;
            for(int j = limit; --j >= st;)
                if(a[j].compareTo(a[j + 1]) > 0)
                {
                    T = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = T;
                    flipped = true;
                }

            if(!flipped)
                return;
        }

    }

    public static void main(String argv[])
    {
        String test[] = {
            "Gamma", "Alpha", "Beta", "Seven", "Six", "One", "Sitraka", "Gladiator"
        };
        sort(test);
        for(int i = 0; i < test.length; i++)
            System.out.println(test[i]);

    }
}
