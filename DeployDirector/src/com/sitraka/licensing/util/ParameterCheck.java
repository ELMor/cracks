// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ParameterCheck.java

package com.sitraka.licensing.util;


public final class ParameterCheck
{

    public ParameterCheck()
    {
    }

    public static void nonNull(Object anObject)
    {
        if(anObject == null)
            throw new IllegalArgumentException("Object is null");
        else
            return;
    }

    public static void notEmpty(String aString)
    {
        if(aString == null)
            throw new IllegalArgumentException("String is null");
        if(aString.equals(""))
            throw new IllegalArgumentException("String is empty");
        else
            return;
    }

    public static void noLessThan(int anInt, int minimum)
    {
        if(anInt < minimum)
            throw new IllegalArgumentException("Value " + anInt + " is out of range.  It is should be no less than " + minimum);
        else
            return;
    }

    public static void greaterThan(int anInt, int minimum)
    {
        if(anInt <= minimum)
            throw new IllegalArgumentException("Value " + anInt + " is out of range.  It should be greater than " + minimum);
        else
            return;
    }

    public static void noGreaterThan(int anInt, int maximum)
    {
        if(anInt > maximum)
            throw new IllegalArgumentException("Value " + anInt + "is out of " + "range. It should be no greater than " + maximum);
        else
            return;
    }

    public static void lessThan(int anInt, int maximum)
    {
        if(anInt >= maximum)
            throw new IllegalArgumentException("Value " + anInt + " is out of range.  " + "It should be less than " + maximum);
        else
            return;
    }

    public static void noLessThan(double aDouble, double minimum)
    {
        if(aDouble < minimum)
            throw new IllegalArgumentException("Value " + aDouble + " is out of range.  " + "It should be no less than " + minimum);
        else
            return;
    }

    public static void greaterThan(double aDouble, double minimum)
    {
        if(aDouble <= minimum)
            throw new IllegalArgumentException("Value " + aDouble + " is out of range.  " + "It should be greater than " + minimum);
        else
            return;
    }

    public static void noGreaterThan(double aDouble, double maximum)
    {
        if(aDouble > maximum)
            throw new IllegalArgumentException("Value " + aDouble + " is out of range.  " + "It should be no greater than " + maximum);
        else
            return;
    }

    public static void lessThan(double aDouble, double maximum)
    {
        if(aDouble >= maximum)
            throw new IllegalArgumentException("Value " + aDouble + " is out of range.  It should be less than " + maximum);
        else
            return;
    }

    public static void withinRange(int anInt, int minimum, int maximum)
    {
        noLessThan(anInt, minimum);
        noGreaterThan(anInt, maximum);
    }

    public static void withinRange(double aDouble, double minimum, double maximum)
    {
        noLessThan(aDouble, minimum);
        noGreaterThan(aDouble, maximum);
    }

    public static void withinCount(int anInt, int count)
    {
        withinRange(anInt, 0, count - 1);
    }

    public static void isTrue(boolean generalTest, String message)
    {
        if(!generalTest)
            throw new IllegalArgumentException(message);
        else
            return;
    }

    public static void isFalse(boolean generalTest, String message)
    {
        if(generalTest)
            throw new IllegalArgumentException(message);
        else
            return;
    }
}
