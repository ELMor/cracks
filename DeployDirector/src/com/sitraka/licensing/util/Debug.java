// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Debug.java

package com.sitraka.licensing.util;

import java.io.PrintStream;

// Referenced classes of package com.sitraka.licensing.util:
//            AssertionException

public class Debug
{

    protected static boolean enabled = false;
    protected static String tags[] = null;
    protected static int level = 0x7fffffff;
    protected static PrintStream stream;

    public Debug()
    {
    }

    public static void setEnabled(boolean e)
    {
        enabled = e;
    }

    public static boolean isEnabled()
    {
        return enabled;
    }

    public static void setPrintStream(PrintStream pstream)
    {
        stream = pstream;
    }

    public static PrintStream getPrintStream()
    {
        return stream;
    }

    public static void setLevel(int new_level)
    {
        level = new_level;
    }

    public static int getLevel()
    {
        return level;
    }

    public static void setTags(String new_tags[])
    {
        tags = new_tags;
    }

    public static void setTag(String tag)
    {
        if(tag == null)
        {
            setTags(((String []) (null)));
        } else
        {
            String new_tags[] = new String[1];
            new_tags[0] = tag;
            setTags(new_tags);
        }
    }

    public static String[] getTags()
    {
        return tags;
    }

    public static final void assertThat(boolean condition)
    {
        if(enabled && !condition)
            throw new AssertionException();
        else
            return;
    }

    public static final void assertThat(boolean condition, String message)
    {
        if(enabled && !condition)
            throw new AssertionException(message);
        else
            return;
    }

    public static void printStackTrace()
    {
        try
        {
            throw new NullPointerException("Forced Stack Trace");
        }
        catch(NullPointerException e)
        {
            e.printStackTrace(stream);
        }
    }

    public static void printStackTrace(String s)
    {
        try
        {
            throw new NullPointerException(s);
        }
        catch(NullPointerException e)
        {
            e.printStackTrace(stream);
        }
    }

    public static void printStackTrace(String ptag, Throwable t)
    {
        if(!isPrintableTag(ptag))
        {
            return;
        } else
        {
            t.printStackTrace(stream);
            return;
        }
    }

    public static void print(int plevel, String ptag, String text)
    {
        if(isPrintableLevel(plevel) && isPrintableTag(ptag))
            stream.print(ptag + " " + text);
    }

    public static void print(int plevel, String text)
    {
        print(plevel, ((String) (null)), text);
    }

    public static void print(String ptag, String text)
    {
        print(0, ptag, text);
    }

    public static void print(String text)
    {
        print(0, ((String) (null)), text);
    }

    public static void println(int plevel, String ptag, String text)
    {
        if(isPrintableLevel(plevel) && isPrintableTag(ptag))
            stream.println(ptag + ": " + text);
    }

    public static void println(int plevel, String text)
    {
        println(plevel, ((String) (null)), text);
    }

    public static void println(String ptag, String text)
    {
        println(0, ptag, text);
    }

    public static void println(String text)
    {
        println(0, ((String) (null)), text);
    }

    protected static boolean isPrintableLevel(int plevel)
    {
        if(!enabled)
            return false;
        else
            return plevel <= level;
    }

    public static boolean isPrintableTag(String tag)
    {
        if(!enabled)
            return false;
        if(tag == null)
            return true;
        if(tags == null)
            return false;
        for(int i = 0; i < tags.length; i++)
            if("*".equals(((Object) (tags[i]))) || tag.equals(((Object) (tags[i]))))
                return true;

        return false;
    }

    static 
    {
        stream = System.out;
    }
}
