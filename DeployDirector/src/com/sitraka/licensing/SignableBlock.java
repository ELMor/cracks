// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SignableBlock.java

package com.sitraka.licensing;

import java.io.UnsupportedEncodingException;

public class SignableBlock
{

    public SignableBlock()
    {
    }

    public static byte[] createSignableBlock(String properties[])
        throws UnsupportedEncodingException
    {
        StringBuffer catstring = new StringBuffer(256);
        for(int i = 0; i < properties.length; i++)
            catstring.append(properties[i]);

        return catstring.toString().getBytes("UTF-8");
    }
}
