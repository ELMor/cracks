// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PortableDSAPublicKey.java

package com.sitraka.licensing;

import java.math.BigInteger;
import java.security.interfaces.DSAPublicKey;

// Referenced classes of package com.sitraka.licensing:
//            AbstractPortableDSAKey

public class PortableDSAPublicKey extends AbstractPortableDSAKey
    implements DSAPublicKey
{

    public PortableDSAPublicKey(String algorithm, BigInteger p, BigInteger q, BigInteger g, BigInteger y)
    {
        super(algorithm, p, q, g, y);
    }

    public BigInteger getY()
    {
        return super.extra;
    }
}
