// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AbstractPortableDSAKey.java

package com.sitraka.licensing;

import java.math.BigInteger;
import java.security.interfaces.DSAKey;
import java.security.interfaces.DSAParams;

public abstract class AbstractPortableDSAKey
    implements DSAKey, DSAParams
{

    protected String algorithm;
    protected BigInteger p;
    protected BigInteger q;
    protected BigInteger g;
    protected BigInteger extra;

    public AbstractPortableDSAKey(String algorithm, BigInteger p, BigInteger q, BigInteger g, BigInteger extra)
    {
        this.algorithm = algorithm;
        this.p = p;
        this.q = q;
        this.g = g;
        this.extra = extra;
    }

    public String getAlgorithm()
    {
        return algorithm;
    }

    public String getFormat()
    {
        return null;
    }

    public byte[] getEncoded()
    {
        return null;
    }

    public DSAParams getParams()
    {
        return ((DSAParams) (this));
    }

    public BigInteger getP()
    {
        return p;
    }

    public BigInteger getQ()
    {
        return q;
    }

    public BigInteger getG()
    {
        return g;
    }
}
