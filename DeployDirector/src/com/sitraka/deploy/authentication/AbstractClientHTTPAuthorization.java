// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AbstractClientHTTPAuthorization.java

package com.sitraka.deploy.authentication;

import com.sitraka.deploy.ClientAuthentication;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public abstract class AbstractClientHTTPAuthorization
    implements ClientAuthentication
{

    protected boolean dataInitialized;
    protected String username;
    protected String password;
    protected File dataFile;

    public AbstractClientHTTPAuthorization()
    {
        dataInitialized = false;
        username = null;
        password = null;
        dataFile = null;
    }

    public Object getAuthenticationInfo()
    {
        if(username == null && password == null)
            return ((Object) (""));
        if(password == null)
            password = "";
        return ((Object) (username + ":" + password));
    }

    public void setAuthenticationInfo(Object obj)
    {
        String info = obj.toString();
        if(info == null || info.length() == 0)
            return;
        int index = info.indexOf(":");
        String user = null;
        String pass = null;
        if(index >= 0)
        {
            user = info.substring(0, index);
            if(index + 1 < info.length())
                pass = info.substring(index + 1);
        } else
        {
            user = info;
        }
        setData(user, pass);
    }

    public boolean writeAuthenticationInfo()
    {
        if(!usesDataFile() || dataFile == null)
            return false;
        try
        {
            PrintWriter out = new PrintWriter(((java.io.OutputStream) (new FileOutputStream(dataFile))));
            out.println(username);
            out.println(password);
            out.close();
        }
        catch(IOException ioe) { }
        return true;
    }

    public boolean usesDataFile()
    {
        return true;
    }

    public void setDataFile(File data_file)
        throws IOException
    {
        dataFile = data_file;
        if(dataFile.exists())
        {
            BufferedReader in = new BufferedReader(((java.io.Reader) (new InputStreamReader(((java.io.InputStream) (new FileInputStream(dataFile)))))));
            setData(in.readLine(), in.readLine());
        }
    }

    public void setData(String un, String p)
    {
        boolean change = false;
        if(username != un && (un == null || !un.equals(((Object) (username)))))
        {
            username = un;
            change = true;
        }
        if(password != p && (password != null || p != null && !p.equals(((Object) (password)))))
        {
            password = p;
            change = true;
        }
        if(change)
            dataInitialized = true;
    }

    public void setAuthenticationInfoAvailable(boolean value)
    {
        dataInitialized = value;
    }

    public boolean isAuthenticationInfoAvailable()
    {
        return dataInitialized;
    }

    public abstract boolean hasEditor();

    public abstract void initEditor();

    public abstract void commitEdits();

    public abstract Component getEditorComponent();
}
