// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   UsernamePasswordEditor.java

package com.sitraka.deploy.authentication;

import com.sitraka.deploy.authentication.resources.LocaleInfo;
import com.sitraka.deploy.common.jclass.util.swing.JCGridLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextComponent;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.EventObject;
import java.util.ResourceBundle;

// Referenced classes of package com.sitraka.deploy.authentication:
//            ClientUsernamePassword, AbstractClientHTTPAuthentication

public class UsernamePasswordEditor extends Panel
    implements ActionListener, WindowListener, KeyListener, FocusListener
{

    protected static final int TEXTFIELD_WIDTH = 15;
    protected ClientUsernamePassword unp;
    protected TextField username;
    protected TextField password;
    protected Button okay;
    protected Button clear;
    protected Button cancel;
    protected Object lock;
    protected static String ACTION_OKAY = "OK";
    protected static String ACTION_CLEAR = "Clear";
    protected static String ACTION_CANCEL = "Cancel";

    public UsernamePasswordEditor(ClientUsernamePassword unp)
    {
        lock = new Object();
        this.unp = unp;
        ((Container)this).setLayout(((java.awt.LayoutManager) (new JCGridLayout(2, 1, 0, 5))));
        ((Container)this).add(((Component) (new Label(LocaleInfo.li.getString("Username") + ":"))));
        username = new TextField(15);
        ((Component) (username)).addFocusListener(((FocusListener) (this)));
        ((Container)this).add(((Component) (username)));
        ((Container)this).add(((Component) (new Label(LocaleInfo.li.getString("Password") + ":"))));
        password = new TextField(15);
        password.setEchoChar('*');
        ((Component) (password)).addFocusListener(((FocusListener) (this)));
        ((Container)this).add(((Component) (password)));
    }

    public void popup()
    {
        Frame f = new Frame(LocaleInfo.li.getString("EnterUserInfo"));
        ((Component) (f)).setBackground(Color.lightGray);
        ((Window) (f)).addWindowListener(((WindowListener) (this)));
        Panel p = new Panel();
        ((Container) (f)).add(((Component) (p)));
        ((Container) (p)).setLayout(((java.awt.LayoutManager) (new JCGridLayout(2, 1, 0, 10))));
        ((Container) (p)).add(((Component) (this)));
        Panel p2 = new Panel();
        ((Container) (p2)).setLayout(((java.awt.LayoutManager) (new JCGridLayout(1, 3, 10, 0))));
        okay = new Button(LocaleInfo.li.getString("Ok"));
        okay.setActionCommand(ACTION_OKAY);
        okay.addActionListener(((ActionListener) (this)));
        clear = new Button(LocaleInfo.li.getString("Clear"));
        clear.setActionCommand(ACTION_CLEAR);
        clear.addActionListener(((ActionListener) (this)));
        cancel = new Button(LocaleInfo.li.getString("Cancel"));
        cancel.setActionCommand(ACTION_CANCEL);
        cancel.addActionListener(((ActionListener) (this)));
        ((Container) (p2)).add(((Component) (okay)));
        ((Container) (p2)).add(((Component) (clear)));
        ((Container) (p2)).add(((Component) (cancel)));
        ((Container) (p)).add(((Component) (p2)));
        ((Window) (f)).pack();
        centerComponent(((Component) (f)));
        ((Component) (f)).setVisible(true);
        ((Component) (okay)).addKeyListener(((KeyListener) (this)));
        ((Component) (clear)).addKeyListener(((KeyListener) (this)));
        ((Component) (cancel)).addKeyListener(((KeyListener) (this)));
        ((Component) (username)).addKeyListener(((KeyListener) (this)));
        ((Component) (password)).addKeyListener(((KeyListener) (this)));
        unp.initEditor();
        synchronized(lock)
        {
            try
            {
                lock.wait();
            }
            catch(Exception e) { }
        }
        ((Component) (username)).removeKeyListener(((KeyListener) (this)));
        ((Component) (password)).removeKeyListener(((KeyListener) (this)));
        ((Component) (f)).setVisible(false);
        ((Container) (f)).remove(((Component) (this)));
        ((Window) (f)).dispose();
    }

    protected void centerComponent(Component comp)
    {
        Dimension d = comp.getSize();
        Dimension size = comp.getToolkit().getScreenSize();
        int left = (size.width - d.width) / 2;
        int top = (size.height - d.height) / 2;
        comp.setLocation(left, top);
    }

    public void actionPerformed(ActionEvent e)
    {
        String command = e.getActionCommand();
        boolean unlock = true;
        if(command.equals(((Object) (ACTION_OKAY))))
            unp.commitEdits();
        else
        if(command.equals(((Object) (ACTION_CLEAR))))
        {
            username.setText(((String) (null)));
            password.setText(((String) (null)));
            unlock = false;
        } else
        if(command.equals(((Object) (ACTION_CANCEL))))
            ((AbstractClientHTTPAuthentication) (unp)).setAuthenticationInfoAvailable(false);
        if(unlock)
            synchronized(lock)
            {
                lock.notifyAll();
            }
    }

    public void windowOpened(WindowEvent windowevent)
    {
    }

    public void windowClosing(WindowEvent e)
    {
        synchronized(lock)
        {
            lock.notifyAll();
        }
    }

    public void windowClosed(WindowEvent windowevent)
    {
    }

    public void windowIconified(WindowEvent windowevent)
    {
    }

    public void windowDeiconified(WindowEvent windowevent)
    {
    }

    public void windowActivated(WindowEvent windowevent)
    {
    }

    public void windowDeactivated(WindowEvent windowevent)
    {
    }

    public void keyTyped(KeyEvent keyevent)
    {
    }

    public void keyPressed(KeyEvent keyevent)
    {
    }

    public void keyReleased(KeyEvent e)
    {
        Object source = ((EventObject) (e)).getSource();
        boolean unlock = true;
        if(e.getKeyCode() == 10)
        {
            if(source.equals(((Object) (okay))) || (source instanceof TextField))
                unp.commitEdits();
            else
            if(source.equals(((Object) (clear))))
            {
                username.setText(((String) (null)));
                password.setText(((String) (null)));
                unlock = false;
            } else
            if(source.equals(((Object) (cancel))))
                ((AbstractClientHTTPAuthentication) (unp)).setAuthenticationInfoAvailable(false);
            if(unlock)
                synchronized(lock)
                {
                    lock.notifyAll();
                }
        }
    }

    public void focusGained(FocusEvent e)
    {
        TextField tf = (TextField)((EventObject) (e)).getSource();
        String text = ((TextComponent) (tf)).getText();
        if(text != null && text.length() != 0)
            ((TextComponent) (tf)).selectAll();
    }

    public void focusLost(FocusEvent e)
    {
        TextField tf = (TextField)((EventObject) (e)).getSource();
        String text = ((TextComponent) (tf)).getText();
        if(text != null && text.length() != 0)
            ((TextComponent) (tf)).select(0, 0);
    }

}
