// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JNDIAuthentication.java

package com.sitraka.deploy.authentication;

import com.sitraka.deploy.ReadableUserList;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.StringTokenizer;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.InitialDirContext;

// Referenced classes of package com.sitraka.deploy.authentication:
//            AbstractAuthentication, JNDIEditor

public class JNDIAuthentication extends AbstractAuthentication
    implements ReadableUserList
{

    public static final String PASSWORD_SERVICE = "passwordService";
    public static final String PASSWORD_ATTRIBUTE = "passwordAttribute";
    protected String initialContextFactory;
    protected String providerURL;
    protected String passwordService;
    protected String passwordAttribute;

    public JNDIAuthentication()
    {
        initialContextFactory = "";
        providerURL = "";
        passwordService = "";
        passwordAttribute = "";
        super.encryptionEnabled = true;
        setDefaultValues();
    }

    public String getInitialContextFactory()
    {
        return initialContextFactory;
    }

    public void setInitialContextFactory(String nicf)
    {
        if(((AbstractAuthentication)this).unchanged(nicf, initialContextFactory))
        {
            return;
        } else
        {
            initialContextFactory = nicf;
            super.isChanged = true;
            return;
        }
    }

    public String getProviderURL()
    {
        return providerURL;
    }

    public void setProviderURL(String purl)
    {
        if(((AbstractAuthentication)this).unchanged(purl, providerURL))
        {
            return;
        } else
        {
            providerURL = purl;
            super.isChanged = true;
            return;
        }
    }

    public String getPasswordService()
    {
        return passwordService;
    }

    public void setPasswordService(String ps)
    {
        if(((AbstractAuthentication)this).unchanged(ps, passwordService))
        {
            return;
        } else
        {
            passwordService = ps;
            super.isChanged = true;
            return;
        }
    }

    public String getPasswordAttribute()
    {
        return passwordAttribute;
    }

    public void setPasswordAttribute(String pa)
    {
        if(((AbstractAuthentication)this).unchanged(pa, passwordAttribute))
        {
            return;
        } else
        {
            passwordAttribute = pa;
            super.isChanged = true;
            return;
        }
    }

    public void setDataFile(File data_file)
        throws IOException
    {
        super.dataFile = data_file;
        if(super.dataFile == null || !super.dataFile.exists())
        {
            setDefaultValues();
            return;
        }
        if(super.dataFile.lastModified() <= super.lastModified)
            return;
        super.lastModified = super.dataFile.lastModified();
        BufferedReader in = new BufferedReader(((java.io.Reader) (new InputStreamReader(((java.io.InputStream) (new FileInputStream(super.dataFile)))))));
        for(String line = in.readLine(); line != null; line = in.readLine())
            try
            {
                StringTokenizer st = new StringTokenizer(line, "=");
                if(st.hasMoreTokens())
                {
                    String property = st.nextToken();
                    if(st.hasMoreTokens())
                    {
                        String value = st.nextToken();
                        if("java.naming.factory.initial".equals(((Object) (property))))
                            initialContextFactory = value;
                        else
                        if("java.naming.provider.url".equals(((Object) (property))))
                            providerURL = value;
                        else
                        if("passwordService".equals(((Object) (property))))
                            passwordService = value;
                        else
                        if("passwordAttribute".equals(((Object) (property))))
                            passwordAttribute = value;
                    }
                }
            }
            catch(Exception e)
            {
                ((Throwable) (e)).printStackTrace(System.out);
                setDefaultValues();
                line = null;
            }

        in.close();
    }

    protected void setDefaultValues()
    {
        initialContextFactory = "com.sun.jndi.nis.NISCtxFactory";
        providerURL = "";
        passwordService = "system/passwd.byname";
        passwordAttribute = "userPassword";
    }

    public Component getEditorComponent()
    {
        if(super.editor == null)
            super.editor = ((Component) (new JNDIEditor(this)));
        return super.editor;
    }

    public boolean commitChanges()
    {
        if(!((AbstractAuthentication)this).isModified())
            return true;
        try
        {
            PrintWriter out = new PrintWriter(((java.io.OutputStream) (new FileOutputStream(super.dataFile))));
            out.println("java.naming.factory.initial=" + initialContextFactory);
            out.println("java.naming.provider.url=" + providerURL);
            out.println("passwordService=" + passwordService);
            out.println("passwordAttribute=" + passwordAttribute);
            out.close();
        }
        catch(IOException e)
        {
            return false;
        }
        super.isChanged = false;
        return true;
    }

    public java.util.List getUsers()
    {
        ArrayList users = new ArrayList();
        Hashtable env = new Hashtable();
        env.put("java.naming.factory.initial", ((Object) (initialContextFactory)));
        env.put("java.naming.provider.url", ((Object) (providerURL)));
        try
        {
            InitialDirContext ctx = new InitialDirContext(env);
            for(NamingEnumeration passwd = ((InitialContext) (ctx)).list(passwordService); passwd.hasMore(); users.add(((Object) (((NameClassPair)passwd.next()).getName()))));
            ((InitialContext) (ctx)).close();
        }
        catch(NamingException ne) { }
        return ((java.util.List) (users));
    }

    protected String getPasswordForUserID(String user_id)
    {
        Hashtable env = new Hashtable();
        env.put("java.naming.factory.initial", ((Object) (initialContextFactory)));
        env.put("java.naming.provider.url", ((Object) (providerURL)));
        try
        {
            InitialDirContext ctx = new InitialDirContext(env);
            String pwa = getPasswordQuery(passwordService, user_id);
            Attributes attr = ctx.getAttributes(pwa);
            for(NamingEnumeration nenum = attr.getAll(); nenum.hasMore();)
            {
                Attribute at = (Attribute)nenum.next();
                if(at.getID().equals(((Object) (passwordAttribute))))
                {
                    String ups = convertPasswordObjectToString(at.get());
                    return ups;
                }
            }

        }
        catch(NamingException ne) { }
        return null;
    }

    protected String getPasswordQuery(String serviceName, String userID)
    {
        return serviceName + "/" + userID;
    }

    protected String convertPasswordObjectToString(Object obj)
    {
        String s = new String((byte[])obj);
        return s.substring(7);
    }
}
