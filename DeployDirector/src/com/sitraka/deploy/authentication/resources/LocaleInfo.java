// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LocaleInfo.java

package com.sitraka.deploy.authentication.resources;

import java.util.ListResourceBundle;
import java.util.ResourceBundle;

public class LocaleInfo extends ListResourceBundle
{

    public static ResourceBundle li = ResourceBundle.getBundle("com.sitraka.deploy.authentication.resources.LocaleInfo");
    public static final String CANCEL = "Cancel";
    public static final String OK = "Ok";
    public static final String CLEAR = "Clear";
    public static final String RELOAD = "Reload";
    public static final String USERNAME = "Username";
    public static final String PASSWORD = "Password";
    public static final String SERIAL_NO = "Serial#";
    public static final String ENTER_USER_INFO = "EnterUserInfo";
    public static final String ENTER_SERIAL_INFO = "EnterSerialInfo";
    public static final String ENTER_PROXY_USER_INFO = "EnterProxyUserInfo";
    public static final String PROXY_HOST_UNKNOWN = "ProxyHostUnkown";
    public static final String JNDI_EDITOR_CONTEXT_FACTORY = "JNDI Editor Context Factory";
    public static final String JNDI_EDITOR_NAMING_SERVICE = "JNDI Editor Naming Service";
    public static final String JNDI_EDITOR_PASSWORD_SERVICE = "JNDI Editor Password Service";
    public static final String JNDI_EDITOR_PASSWORD_ATTRIBUTE = "JNDI Editor Password Attribute";
    public static final String JNDI_EDITOR_GROUP_SERVICE = "JNDI Editor Group Service";
    public static final String JNDI_EDITOR_GROUP_ATTRIBUTE = "JNDI Editor Group Attribute";
    public static final String JNDI_EDITOR_USER_SERVICE = "JNDI Editor User Service";
    public static final String JNDI_EDITOR_USER_ATTRIBUTE = "JNDI Editor User Attribute";
    public static final String SIMPLE_AUTH_USER_ID = "Simple Auth User ID";
    public static final String SIMPLE_AUTH_PASSWD = "Simple Auth Password";
    public static final String SIMPLE_AUTH_GROUP_NAME = "Simple Auth Group Name";
    public static final String SIMPLE_AUTH_GROUP_MEMBERS = "Simple Auth Group Members";
    public static final String WINDOWS_AUTH_EDITOR_DOMAIN = "Windows Auth Editor Domain";
    static final Object strings[][] = {
        {
            "Cancel", "Cancel"
        }, {
            "Ok", "OK"
        }, {
            "Clear", "Clear"
        }, {
            "Reload", "Reload"
        }, {
            "Username", "Username"
        }, {
            "Password", "Password"
        }, {
            "Serial#", "Serial #"
        }, {
            "EnterUserInfo", "Enter User Information"
        }, {
            "EnterSerialInfo", "Enter Serial Number"
        }, {
            "EnterProxyUserInfo", "Enter proxy user information for host {0}"
        }, {
            "ProxyHostUnkown", "(unknown)"
        }, {
            "JNDI Editor Context Factory", "Initial Context Factory"
        }, {
            "JNDI Editor Naming Service", "Naming Service URL"
        }, {
            "JNDI Editor Password Service", "Password Service Name"
        }, {
            "JNDI Editor Password Attribute", "Password Attribute Name"
        }, {
            "JNDI Editor Group Service", "Group Service Name"
        }, {
            "JNDI Editor Group Attribute", "Group Attribute Name"
        }, {
            "JNDI Editor User Service", "User Service Name"
        }, {
            "JNDI Editor User Attribute", "User Attribute Name"
        }, {
            "Simple Auth User ID", "User ID"
        }, {
            "Simple Auth Password", "Password"
        }, {
            "Simple Auth Group Name", "Group Name"
        }, {
            "Simple Auth Group Members", "Members"
        }, {
            "Windows Auth Editor Domain", "Domain"
        }
    };

    public LocaleInfo()
    {
    }

    public Object[][] getContents()
    {
        return strings;
    }

}
