/* SimpleAuthEditor - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.sitraka.deploy.authentication;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import com.klg.jclass.util.swing.JCAction;
import com.klg.jclass.util.swing.JCSortableTable;
import com.sitraka.deploy.util.Crypt;

public class SimpleAuthEditor extends JPanel
    implements TableModelListener, PropertyChangeListener
{
    protected JCSortableTable table;
    protected SimpleAuthentication da;
    protected boolean isChanged = false;
    protected ActionNewRow newrowAction;
    protected ActionDeleteRow deleterowAction;
    private boolean inTableChanged = false;

    class PasswordTableCellEditor extends DefaultCellEditor
    {
        PasswordTableCellEditor() {
            super(new JPasswordField());
        }

        public Component getTableCellEditorComponent
            (JTable table, Object value, boolean isSelected, int row,
             int column) {
            delegate.setValue("");
            return editorComponent;
        }
    }

    class PasswordTableCellRenderer extends DefaultTableCellRenderer
    {
        protected void setValue(Object value) {
            StringBuffer sb
                = new StringBuffer(value == null ? "" : value.toString());
            for (int i = 0; i < sb.length(); i++)
                sb.setCharAt(i, '*');
            this.setText(sb.toString());
        }
    }

    protected class ActionDeleteRow extends JCAction
        implements ListSelectionListener
    {
        public ActionDeleteRow() {
            super("ActionDelete");
            this.setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {
            int[] rows = table.getSelectedRows();
            table.clearSelection();
            if (table.getCellEditor() != null)
                table.getCellEditor().cancelCellEditing();
            for (int i = rows.length - 1; i >= 0; i--)
                da.data.removeRow(rows[i]);
        }

        public void valueChanged(ListSelectionEvent e) {
            if (((ListSelectionModel) e.getSource()).isSelectionEmpty())
                this.setEnabled(false);
            else
                this.setEnabled(true);
        }
    }

    protected class ActionNewRow extends JCAction
    {
        public ActionNewRow() {
            super("ActionNew");
        }

        public void actionPerformed(ActionEvent e) {
            Object[] row = new Object[2];
            row[0] = "";
            row[1] = "";
            da.data.addRow(row);
        }
    }

    public SimpleAuthEditor(SimpleAuthentication da) {
        this.da = da;
        this.setLayout(new BorderLayout(0, 2));
        JToolBar toolbar = new JToolBar();
        toolbar.setFloatable(false);
        table = new JCSortableTable(da.data);
        table.addPropertyChangeListener(this);
        setupTable();
        da.data.addTableModelListener(this);
        newrowAction = new ActionNewRow();
        toolbar.add(newrowAction);
        deleterowAction = new ActionDeleteRow();
        toolbar.add(deleterowAction);
        table.getSelectionModel().addListSelectionListener(deleterowAction);
        this.add(toolbar, "North");
        this.add(new JScrollPane(table), "Center");
    }

    public void tableChanged(TableModelEvent e) {
        if (!inTableChanged && da.encryptionEnabled
            && (e.getColumn() == 1 && e.getType() != -1)) {
            Object v = da.data.getValueAt(e.getFirstRow(), e.getColumn());
            if (v != null && v instanceof String) {
                String s = (String) v;
                if (s.length() > 2) {
                    String enc = Crypt.crypt(s.substring(0, 2), s);
                    inTableChanged = true;
                    da.data.setValueAt(enc, e.getFirstRow(), e.getColumn());
                    inTableChanged = false;
                }
            }
        }
    }

    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("model"))
            setupTable();
    }

    void setupTable() {
        String name = da.data.getColumnName(1);
        TableColumn col = table.getColumn(name);
        col.setCellEditor(new PasswordTableCellEditor());
        DefaultTableCellRenderer ren = new PasswordTableCellRenderer();
        ren.setHorizontalAlignment(2);
        col.setCellRenderer(ren);
        name = da.data.getColumnName(0);
        col = table.getColumn(name);
        ren = new DefaultTableCellRenderer();
        ren.setHorizontalAlignment(2);
        col.setCellRenderer(ren);
    }
}
