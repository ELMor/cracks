// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   NISAuthGroups.java

package com.sitraka.deploy.authentication;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.naming.Binding;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.InitialDirContext;

// Referenced classes of package com.sitraka.deploy.authentication:
//            AbstractAuthGroups, NISAuthGroupsEditor

public class NISAuthGroups extends AbstractAuthGroups
{

    public static final String GROUP_SERVICE = "groupService";
    public static final String GROUP_ATTRIBUTE = "groupAttribute";
    public static final String USER_SERVICE = "userService";
    public static final String USER_ATTRIBUTE = "userAttribute";
    protected String initialContextFactory;
    protected String providerURL;
    protected String groupService;
    protected String groupAttribute;
    protected String userService;
    protected String userAttribute;
    protected Hashtable queryEnv;
    protected boolean newContext;
    protected InitialDirContext ctx;
    private long lastLoaded;
    private Hashtable groupCache;
    private Hashtable groupIDMap;

    public NISAuthGroups()
    {
        initialContextFactory = "";
        providerURL = "";
        groupService = "";
        groupAttribute = "";
        userService = "";
        userAttribute = "";
        queryEnv = new Hashtable();
        newContext = true;
        ctx = null;
        lastLoaded = 0L;
        setDefaultValues();
    }

    public String getInitialContextFactory()
    {
        return initialContextFactory;
    }

    public void setInitialContextFactory(String nicf)
    {
        if(((AbstractAuthGroups)this).unchanged(nicf, initialContextFactory))
        {
            return;
        } else
        {
            initialContextFactory = nicf;
            super.isChanged = true;
            queryEnv.put("java.naming.factory.initial", ((Object) (initialContextFactory)));
            newContext = true;
            return;
        }
    }

    public String getProviderURL()
    {
        return providerURL;
    }

    public void setProviderURL(String purl)
    {
        if(((AbstractAuthGroups)this).unchanged(purl, providerURL))
        {
            return;
        } else
        {
            providerURL = purl;
            super.isChanged = true;
            queryEnv.put("java.naming.provider.url", ((Object) (providerURL)));
            newContext = true;
            return;
        }
    }

    public String getGroupService()
    {
        return groupService;
    }

    public void setGroupService(String gs)
    {
        if(((AbstractAuthGroups)this).unchanged(gs, groupService))
        {
            return;
        } else
        {
            groupService = gs;
            super.isChanged = true;
            return;
        }
    }

    public String getUserService()
    {
        return userService;
    }

    public void setUserService(String us)
    {
        if(((AbstractAuthGroups)this).unchanged(us, userService))
        {
            return;
        } else
        {
            userService = us;
            super.isChanged = true;
            return;
        }
    }

    public String getGroupAttribute()
    {
        return groupAttribute;
    }

    public void setGroupAttribute(String ga)
    {
        if(((AbstractAuthGroups)this).unchanged(ga, groupAttribute))
        {
            return;
        } else
        {
            groupAttribute = ga;
            super.isChanged = true;
            return;
        }
    }

    public String getUserAttribute()
    {
        return userAttribute;
    }

    public void setUserAttribute(String ua)
    {
        if(((AbstractAuthGroups)this).unchanged(ua, userAttribute))
        {
            return;
        } else
        {
            userAttribute = ua;
            super.isChanged = true;
            return;
        }
    }

    public boolean usesDataFile()
    {
        return true;
    }

    public File getDataFile()
    {
        return super.dataFile;
    }

    public void setDataFile(File data_file)
        throws IOException
    {
        super.dataFile = data_file;
        if(super.dataFile == null || !super.dataFile.exists())
        {
            setDefaultValues();
            return;
        }
        if(super.dataFile.lastModified() <= super.lastModified)
            return;
        super.lastModified = super.dataFile.lastModified();
        BufferedReader in = new BufferedReader(((java.io.Reader) (new FileReader(super.dataFile))));
        for(String line = in.readLine(); line != null; line = in.readLine())
            try
            {
                StringTokenizer st = new StringTokenizer(line, "=");
                if(st.hasMoreTokens())
                {
                    String property = st.nextToken();
                    if(st.hasMoreTokens())
                    {
                        String value = st.nextToken();
                        if("java.naming.factory.initial".equals(((Object) (property))))
                        {
                            queryEnv.put("java.naming.factory.initial", ((Object) (value)));
                            initialContextFactory = value;
                        } else
                        if("java.naming.provider.url".equals(((Object) (property))))
                        {
                            queryEnv.put("java.naming.provider.url", ((Object) (value)));
                            providerURL = value;
                        } else
                        if("groupService".equals(((Object) (property))))
                            groupService = value;
                        else
                        if("groupAttribute".equals(((Object) (property))))
                            groupAttribute = value;
                        else
                        if("userService".equals(((Object) (property))))
                            userService = value;
                        else
                        if("userAttribute".equals(((Object) (property))))
                            userAttribute = value;
                    }
                }
            }
            catch(Exception e)
            {
                ((Throwable) (e)).printStackTrace(System.out);
                setDefaultValues();
                line = null;
            }

        in.close();
    }

    protected void setDefaultValues()
    {
        initialContextFactory = "com.sun.jndi.nis.NISCtxFactory";
        providerURL = "";
        groupService = "system/group.byname";
        groupAttribute = "memberUid";
        userService = "system/passwd.byname";
        userAttribute = "gidNumber";
        queryEnv.put("java.naming.factory.initial", ((Object) (initialContextFactory)));
        queryEnv.put("java.naming.provider.url", ((Object) (providerURL)));
        newContext = true;
    }

    public boolean hasEditor()
    {
        return true;
    }

    public Component getEditorComponent()
    {
        if(super.editor == null)
            super.editor = ((Component) (new NISAuthGroupsEditor(this)));
        return super.editor;
    }

    public boolean commitChanges()
    {
        if(!((AbstractAuthGroups)this).isModified())
            return true;
        try
        {
            PrintWriter out = new PrintWriter(((java.io.OutputStream) (new FileOutputStream(super.dataFile))));
            out.println("java.naming.factory.initial=" + initialContextFactory);
            out.println("java.naming.provider.url=" + providerURL);
            out.println("groupService=" + groupService);
            out.println("groupAttribute=" + groupAttribute);
            out.println("userService=" + userService);
            out.println("userAttribute=" + userAttribute);
            out.close();
        }
        catch(IOException e)
        {
            return false;
        }
        super.isChanged = false;
        return true;
    }

    public Vector listAllMembers(String group)
    {
        return getGroupMembers(group);
    }

    public Vector getGroupMembers(String groupName)
    {
        if(groupName == null || groupName.length() == 0)
            return new Vector();
        if(!isCacheCurrent())
            reloadGroupCache();
        if(!groupCache.containsKey(((Object) (groupName))))
        {
            Vector result = new Vector();
            result.add(((Object) (groupName)));
            return result;
        } else
        {
            return (Vector)groupCache.get(((Object) (groupName)));
        }
    }

    public Vector listAllGroups()
    {
        if(!isCacheCurrent())
            reloadGroupCache();
        Vector group_names = new Vector();
        for(Enumeration group_enum = groupCache.keys(); group_enum.hasMoreElements(); group_names.add(group_enum.nextElement()));
        return group_names;
    }

    public boolean isGroup(String groupName)
    {
        if(!isCacheCurrent())
            reloadGroupCache();
        return groupCache.containsKey(((Object) (groupName)));
    }

    public boolean isUser(String user)
    {
        try
        {
            checkContext();
            String user_query = composeQuery(userService, user);
            Attributes attributes = ctx.getAttributes(user_query);
        }
        catch(NameNotFoundException nnfe)
        {
            return false;
        }
        catch(NamingException ne) { }
        return true;
    }

    protected String getBaseGroup(String userName)
    {
        String group_num = null;
        try
        {
            checkContext();
            String user_query = composeQuery(userService, userName);
            for(NamingEnumeration attr_enum = ctx.getAttributes(user_query).getAll(); attr_enum.hasMore();)
            {
                Attribute attr = (Attribute)attr_enum.next();
                if(attr.getID().equals(((Object) (userAttribute))))
                {
                    group_num = (String)attr.get();
                    break;
                }
            }

            if(group_num == null)
                return null;
        }
        catch(NamingException ne)
        {
            return "nobody";
        }
        if(!isCacheCurrent())
            reloadGroupCache();
        return (String)groupIDMap.get(((Object) (group_num)));
    }

    protected String composeQuery(String serviceName, String objectName)
    {
        return serviceName + "/" + objectName;
    }

    protected Vector buildMemberList(Attribute attr)
    {
        if(attr.size() <= 0)
            return new Vector();
        Vector member_list = new Vector(attr.size());
        try
        {
            for(NamingEnumeration members_enum = attr.getAll(); members_enum.hasMore();)
            {
                String name = (String)members_enum.next();
                if(!member_list.contains(((Object) (name))))
                    member_list.addElement(((Object) (name)));
            }

        }
        catch(NamingException ne) { }
        return member_list;
    }

    protected void checkContext()
    {
        if(newContext)
        {
            InitialDirContext old_ctx = ctx;
            try
            {
                ctx = new InitialDirContext(queryEnv);
            }
            catch(NamingException ne)
            {
                System.out.println(((Throwable) (ne)).getMessage());
                ne.printStackTrace();
                ctx = old_ctx;
            }
            lastLoaded = 0L;
        }
    }

    protected boolean isCacheCurrent()
    {
        return System.currentTimeMillis() - lastLoaded < 0x1b7740L;
    }

    protected void reloadGroupCache()
    {
        groupCache = new Hashtable();
        groupIDMap = new Hashtable();
        checkContext();
        try
        {
            NameClassPair group_entry;
            for(NamingEnumeration group_enum = ((InitialContext) (ctx)).list(groupService); group_enum.hasMore(); groupCache.put(((Object) (group_entry.getName())), ((Object) (new Vector()))))
                group_entry = (NameClassPair)group_enum.next();

        }
        catch(NamingException ne) { }
        for(Enumeration group_names = groupCache.keys(); group_names.hasMoreElements();)
        {
            String name = (String)group_names.nextElement();
            Vector result = null;
            String group_id = null;
            String group_query = composeQuery(groupService, name);
            try
            {
                Attribute attr = ctx.getAttributes(group_query).get(groupAttribute);
                if(attr != null)
                    result = buildMemberList(attr);
                else
                    result = new Vector();
            }
            catch(NamingException ne)
            {
                result = new Vector();
            }
            try
            {
                Attribute attr = ctx.getAttributes(group_query).get(userAttribute);
                if(attr != null)
                    group_id = (String)attr.get();
            }
            catch(NamingException ne) { }
            groupCache.put(((Object) (name)), ((Object) (result)));
            if(group_id != null)
                groupIDMap.put(((Object) (group_id)), ((Object) (name)));
        }

        try
        {
            for(NamingEnumeration user_enum = ((InitialContext) (ctx)).listBindings(userService); user_enum.hasMore();)
            {
                Binding user_entry = (Binding)user_enum.next();
                String user_name = ((NameClassPair) (user_entry)).getName();
                String user_obj = user_entry.getObject().toString();
                int colon = user_obj.indexOf(":") + 1;
                colon = user_obj.indexOf(":", colon) + 1;
                colon = user_obj.indexOf(":", colon) + 1;
                String group_id = user_obj.substring(colon, user_obj.indexOf(":", colon));
                if(groupIDMap.containsKey(((Object) (group_id))))
                {
                    String group_name = (String)groupIDMap.get(((Object) (group_id)));
                    Vector members = (Vector)groupCache.get(((Object) (group_name)));
                    if(!members.contains(((Object) (user_name))))
                        members.addElement(((Object) (user_name)));
                }
            }

        }
        catch(NamingException ne) { }
        lastLoaded = System.currentTimeMillis();
    }
}
