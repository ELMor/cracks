// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AuthenticateAll.java

package com.sitraka.deploy.authentication;

import com.sitraka.deploy.Authentication;
import java.awt.Component;
import java.io.File;
import java.io.IOException;

public class AuthenticateAll
    implements Authentication
{

    public AuthenticateAll()
    {
    }

    public boolean isAuthentic(Object user)
    {
        return true;
    }

    public String getUserID(Object user)
    {
        if(user != null)
        {
            if(user instanceof String)
            {
                int index = ((String)user).indexOf(":");
                if(index == -1)
                    return user.toString();
                else
                    return ((String)user).substring(0, index);
            } else
            {
                return user.toString();
            }
        } else
        {
            return null;
        }
    }

    public boolean usesDataFile()
    {
        return false;
    }

    public File getDataFile()
    {
        return null;
    }

    public void setDataFile(File file)
        throws IOException
    {
    }

    public boolean hasEditor()
    {
        return false;
    }

    public Component getEditorComponent()
    {
        return null;
    }

    public boolean commitChanges()
    {
        return true;
    }

    public boolean isModified()
    {
        return false;
    }
}
