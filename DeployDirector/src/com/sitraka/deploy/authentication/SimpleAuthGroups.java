// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SimpleAuthGroups.java

package com.sitraka.deploy.authentication;

import com.sitraka.deploy.authentication.resources.LocaleInfo;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

// Referenced classes of package com.sitraka.deploy.authentication:
//            AbstractAuthGroups, SimpleAuthGroupEditor

public class SimpleAuthGroups extends AbstractAuthGroups
    implements TableModelListener
{

    protected Hashtable groupMap;
    protected DefaultTableModel data;

    public SimpleAuthGroups()
    {
        groupMap = null;
        data = null;
    }

    public Vector listAllGroups()
    {
        Vector groups = new Vector();
        for(Enumeration keys = groupMap.keys(); keys.hasMoreElements(); groups.addElement(keys.nextElement()));
        return groups;
    }

    public boolean isGroup(String name)
    {
        return name != null && name.length() > 0 && groupMap.containsKey(((Object) (name)));
    }

    public Vector getGroupMembers(String name)
    {
        if(name == null || name.length() == 0)
            return new Vector();
        if(!groupMap.containsKey(((Object) (name))))
        {
            Vector nogroup = new Vector(1);
            nogroup.addElement(((Object) (name)));
            return nogroup;
        } else
        {
            return (Vector)groupMap.get(((Object) (name)));
        }
    }

    public boolean usesDataFile()
    {
        return true;
    }

    public void setDataFile(File data_file)
        throws IOException
    {
        if(data_file != null && data_file.equals(((Object) (super.dataFile))) && super.dataFile.lastModified() <= super.lastModified)
            return;
        if(super.dataFile == null || data_file == null)
            groupMap = null;
        else
        if(super.dataFile.lastModified() > super.lastModified)
            groupMap = null;
        super.dataFile = data_file;
        Vector rows = null;
        if(super.dataFile == null || !super.dataFile.exists())
        {
            data = new DefaultTableModel();
            rows = new Vector();
        } else
        {
            super.lastModified = super.dataFile.lastModified();
            BufferedReader in = new BufferedReader(((java.io.Reader) (new FileReader(super.dataFile))));
            String line = in.readLine();
            int num_rows = 0;
            try
            {
                num_rows = Integer.parseInt(line);
                rows = new Vector(num_rows);
            }
            catch(NumberFormatException nfe)
            {
                num_rows = 0x7fffffff;
                rows = new Vector();
                if(line.indexOf(":") >= 0)
                    rows.addElement(((Object) (parseLine(line))));
            }
            for(int i = 0; i < num_rows; i++)
            {
                line = in.readLine();
                if(line == null || line.length() == 0)
                {
                    num_rows = i;
                    break;
                }
                rows.addElement(((Object) (parseLine(line))));
            }

            in.close();
        }
        Vector columns = new Vector(2);
        columns.addElement(((Object) (LocaleInfo.li.getString("Simple Auth Group Name"))));
        columns.addElement(((Object) (LocaleInfo.li.getString("Simple Auth Group Members"))));
        if(data == null)
        {
            data = new DefaultTableModel(rows, columns);
        } else
        {
            data.setNumRows(0);
            data.setDataVector(rows, columns);
        }
        ((AbstractTableModel) (data)).addTableModelListener(((TableModelListener) (this)));
        buildMap();
    }

    public File getDataFile()
    {
        return super.dataFile;
    }

    public boolean hasEditor()
    {
        return true;
    }

    public Component getEditorComponent()
    {
        if(super.editor == null)
            super.editor = ((Component) (new SimpleAuthGroupEditor(this)));
        return super.editor;
    }

    public boolean commitChanges()
    {
        if(!((AbstractAuthGroups)this).isModified())
            return true;
        try
        {
            PrintWriter out = new PrintWriter(((java.io.OutputStream) (new FileOutputStream(super.dataFile))));
            for(int i = 0; i < data.getRowCount(); i++)
            {
                String group = (String)data.getValueAt(i, 0);
                String members = (String)data.getValueAt(i, 1);
                if((group == null || group.length() <= 0) && (members == null || members.length() <= 0))
                {
                    data.removeRow(i);
                    i--;
                }
            }

            int row_count = data.getRowCount();
            out.println(row_count);
            for(int i = 0; i < row_count; i++)
                out.println((String)data.getValueAt(i, 0) + ":" + (String)data.getValueAt(i, 1));

            out.close();
        }
        catch(IOException ioe)
        {
            return false;
        }
        super.isChanged = false;
        return true;
    }

    public void tableChanged(TableModelEvent e)
    {
        super.isChanged = true;
    }

    protected Vector parseLine(String line)
    {
        StringTokenizer st = new StringTokenizer(line, ":");
        Vector row = new Vector(2);
        try
        {
            row.addElement(((Object) (st.nextToken())));
            row.addElement(((Object) (st.nextToken())));
        }
        catch(NoSuchElementException nsee)
        {
            for(; row.size() < 2; row.addElement(""));
        }
        return row;
    }

    protected void buildMap()
    {
        groupMap = new Hashtable();
        for(int i = 0; i < data.getRowCount(); i++)
        {
            String group = (String)data.getValueAt(i, 0);
            String members = (String)data.getValueAt(i, 1);
            Vector member_list = new Vector();
            for(StringTokenizer st = new StringTokenizer(members, ","); st.hasMoreTokens(); member_list.addElement(((Object) (st.nextToken().trim()))));
            groupMap.put(((Object) (group)), ((Object) (member_list)));
        }

    }
}
