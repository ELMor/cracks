// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   MappedComboBoxEditor.java

package com.sitraka.deploy.admin.panels.addremove;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

public class MappedComboBoxEditor extends AbstractCellEditor
    implements TableCellEditor, TableCellRenderer, ActionListener
{

    protected JComboBox comboBox;
    protected Object displayValues[];
    protected Object dataValues[];

    public MappedComboBoxEditor(Object displayValues[], Object dataValues[])
    {
        if(displayValues.length != dataValues.length)
        {
            throw new IllegalArgumentException("displayValues and dataValues arrays must be the same length.");
        } else
        {
            this.displayValues = displayValues;
            this.dataValues = dataValues;
            comboBox = new JComboBox(displayValues);
            ((JComponent) (comboBox)).putClientProperty("JComboBox.lightweightKeyboardNavigation", "Lightweight");
            comboBox.addActionListener(((ActionListener) (this)));
            return;
        }
    }

    public Object getCellEditorValue()
    {
        int index = comboBox.getSelectedIndex();
        if(index == -1)
            return ((Object) (null));
        else
            return dataValues[index];
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
        for(int i = 0; i < dataValues.length; i++)
        {
            if(!dataValues[i].equals(value))
                continue;
            comboBox.setSelectedItem(displayValues[i]);
            break;
        }

        ((JComponent) (comboBox)).requestFocus();
        return ((Component) (comboBox));
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        Component component = null;
        for(int i = 0; i < dataValues.length; i++)
        {
            if(!dataValues[i].equals(value))
                continue;
            component = table.getDefaultRenderer(value.getClass()).getTableCellRendererComponent(table, displayValues[i], isSelected, hasFocus, row, column);
            break;
        }

        return component;
    }

    public void actionPerformed(ActionEvent e)
    {
        if(((EventObject) (e)).getSource() == comboBox)
            ((AbstractCellEditor)this).stopCellEditing();
    }
}
