// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   EditorFactory.java

package com.sitraka.deploy.admin.auth;

import com.sitraka.deploy.MutableAuthorization;
import com.sitraka.deploy.MutableAuthorizationEditor;
import java.awt.Component;
import java.io.PrintStream;

public class EditorFactory
{

    protected static final String BUNDLE_AUTHORIZATION_EDITOR = "com.sitraka.deploy.admin.auth.BundleAuthorizationEditor";
    protected static final String ADMIN_AUTHORIZATION_EDITOR = "com.sitraka.deploy.admin.auth.AdminAuthorizationEditor";

    public EditorFactory()
    {
    }

    public static Component createBundleAuthorizationEditor(MutableAuthorization authorization)
    {
        return createMutableAuthorizationEditor("com.sitraka.deploy.admin.auth.BundleAuthorizationEditor", authorization);
    }

    protected static Component createAdminAuthorizationEditor(MutableAuthorization authorization)
    {
        return createMutableAuthorizationEditor("com.sitraka.deploy.admin.auth.AdminAuthorizationEditor", authorization);
    }

    protected static Component createMutableAuthorizationEditor(String className, MutableAuthorization authorization)
    {
        MutableAuthorizationEditor editor = null;
        Object obj = getInstanceForName(className);
        if(obj != null && (obj instanceof MutableAuthorizationEditor))
        {
            editor = (MutableAuthorizationEditor)obj;
            editor.setAuthorization(authorization);
        }
        return (Component)editor;
    }

    public static Object getInstanceForName(String className)
    {
        if(className == null || className.trim().length() < 1)
            return ((Object) (null));
        Class c = null;
        try
        {
            c = Class.forName(className);
        }
        catch(ClassNotFoundException cnfe)
        {
            System.err.println("Class " + className + " not found");
            return ((Object) (null));
        }
        if(c == null)
        {
            System.err.println("No class loaded for " + className);
            return ((Object) (null));
        }
        try
        {
            return c.newInstance();
        }
        catch(InstantiationException ie1)
        {
            System.err.println("Could not instantiate instance of " + className);
        }
        catch(IllegalAccessException iae)
        {
            System.err.println("Could not access no-argument constructor for " + className);
        }
        return ((Object) (null));
    }
}
