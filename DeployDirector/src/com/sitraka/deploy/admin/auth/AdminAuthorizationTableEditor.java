// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AdminAuthorizationTableEditor.java

package com.sitraka.deploy.admin.auth;

import com.klg.jclass.util.swing.JCAction;
import com.sitraka.deploy.admin.panels.addremove.MappedComboBoxEditor;
import com.sitraka.deploy.common.roles.RoleTableModel;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.util.EventObject;
import javax.swing.CellEditor;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

// Referenced classes of package com.sitraka.deploy.admin.auth:
//            AdminAuthorization

public class AdminAuthorizationTableEditor extends JPanel
    implements TableModelListener
{
    protected class ActionDeleteRow extends JCAction
        implements ListSelectionListener
    {

        public void actionPerformed(ActionEvent e)
        {
            int rows[] = table.getSelectedRows();
            table.clearSelection();
            if(table.getCellEditor() != null)
                ((CellEditor) (table.getCellEditor())).cancelCellEditing();
            for(int i = rows.length - 1; i >= 0; i--)
                authorization.data.removeRow(rows[i]);

        }

        public void valueChanged(ListSelectionEvent e)
        {
            if(((ListSelectionModel)((EventObject) (e)).getSource()).isSelectionEmpty())
                ((JCAction)this).setEnabled(false);
            else
                ((JCAction)this).setEnabled(true);
        }

        public ActionDeleteRow()
        {
            super("ActionDelete");
            ((JCAction)this).setEnabled(false);
        }
    }

    protected class ActionNewRow extends JCAction
    {

        public void actionPerformed(ActionEvent e)
        {
            authorization.data.addRow();
        }

        public ActionNewRow()
        {
            super("ActionNew");
        }
    }


    protected JTable table;
    protected AdminAuthorization authorization;
    protected boolean isChanged;
    protected ActionNewRow newrowAction;
    protected ActionDeleteRow deleterowAction;

    public AdminAuthorizationTableEditor(AdminAuthorization authorization)
    {
        isChanged = false;
        this.authorization = authorization;
        ((Container)this).setLayout(((java.awt.LayoutManager) (new BorderLayout(0, 2))));
        JToolBar toolbar = new JToolBar();
        toolbar.setFloatable(false);
        table = new JTable(((javax.swing.table.TableModel) (authorization.data)));
        for(int i = 0; i < authorization.data.getColumnCount(); i++)
        {
            String name = authorization.data.getColumnName(i);
            TableColumn col = table.getColumn(((Object) (name)));
            DefaultTableCellRenderer ren = new DefaultTableCellRenderer();
            ((JLabel) (ren)).setHorizontalAlignment(2);
            col.setCellRenderer(((javax.swing.table.TableCellRenderer) (ren)));
        }

        String displayValues[] = {
            "Server Administrator", "Bundle Administrator"
        };
        String dataValues[] = {
            "com.sitraka.deploy.common.roles.ServerAdminRole", "com.sitraka.deploy.common.roles.BundleAdminRole"
        };
        MappedComboBoxEditor roleComboBox = new MappedComboBoxEditor(((Object []) (displayValues)), ((Object []) (dataValues)));
        table.getColumnModel().getColumn(0).setCellEditor(((javax.swing.table.TableCellEditor) (roleComboBox)));
        table.getColumnModel().getColumn(0).setCellRenderer(((javax.swing.table.TableCellRenderer) (roleComboBox)));
        ((AbstractTableModel) (authorization.data)).addTableModelListener(((TableModelListener) (this)));
        newrowAction = new ActionNewRow();
        toolbar.add(((javax.swing.Action) (newrowAction)));
        deleterowAction = new ActionDeleteRow();
        toolbar.add(((javax.swing.Action) (deleterowAction)));
        table.getSelectionModel().addListSelectionListener(((ListSelectionListener) (deleterowAction)));
        ((Container)this).add(((java.awt.Component) (toolbar)), "North");
        ((Container)this).add(((java.awt.Component) (new JScrollPane(((java.awt.Component) (table))))), "Center");
    }

    public void tableChanged(TableModelEvent e)
    {
        isChanged = true;
    }
}
