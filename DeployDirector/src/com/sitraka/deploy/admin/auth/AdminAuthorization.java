// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AdminAuthorization.java

package com.sitraka.deploy.admin.auth;

import com.sitraka.deploy.AuthContext;
import com.sitraka.deploy.AuthGroups;
import com.sitraka.deploy.AuthorizationModel;
import com.sitraka.deploy.MutableAuthorization;
import com.sitraka.deploy.MutableAuthorizationEditor;
import com.sitraka.deploy.NonPublicAuthContext;
import com.sitraka.deploy.common.roles.BundleAdminRole;
import com.sitraka.deploy.common.roles.Role;
import com.sitraka.deploy.common.roles.RoleAuthorization;
import com.sitraka.deploy.common.roles.RoleManager;
import com.sitraka.deploy.common.roles.RoleTableModel;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;
import java.util.Properties;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

// Referenced classes of package com.sitraka.deploy.admin.auth:
//            AdminAuthorizationTableEditor, EditorFactory

public class AdminAuthorization
    implements RoleAuthorization, MutableAuthorization, TableModelListener
{

    protected boolean isChanged;
    protected String defaultAdminName;
    protected AuthGroups groupData;
    private final Object LOCK;
    protected File dataFile;
    protected java.awt.Component editor;
    protected long lastModified;
    protected RoleManager roleManager;
    protected RoleTableModel data;
    protected AuthContext authContext;

    public AdminAuthorization()
    {
        isChanged = false;
        groupData = null;
        LOCK = new Object();
        dataFile = null;
        editor = null;
        lastModified = 0L;
        authContext = null;
        init(((AuthGroups) (null)));
    }

    public AdminAuthorization(AuthGroups groupData)
    {
        isChanged = false;
        this.groupData = null;
        LOCK = new Object();
        dataFile = null;
        editor = null;
        lastModified = 0L;
        authContext = null;
        init(groupData);
    }

    protected void init(AuthGroups groupData)
    {
        roleManager = RoleManager.getInstance();
        data = new RoleTableModel(roleManager);
        ((AbstractTableModel) (data)).addTableModelListener(((TableModelListener) (this)));
        setAuthGroups(groupData);
    }

    public int isAuthorized(String userName, String appName, String version)
    {
        if(userName == null || userName.trim().length() == 0)
            return 2;
        if(isDefaultAdmin(userName))
            return 1;
        return isRoleAuthorized(((Role) (new BundleAdminRole(userName, appName)))) != 1 ? 1 : 1;
    }

    public int isRoleAuthorized(Role role)
    {
        List users = role.getMembers();
        for(int i = 0; i < users.size(); i++)
            if(isDefaultAdmin((String)users.get(i)))
                return 1;

        return !roleManager.isRoleAuthorized(role) ? 2 : 1;
    }

    public int isRoleAuthorized(String user, Role roles[])
    {
        for(int i = 0; i < roles.length; i++)
        {
            roles[i].addMember(user);
            if(isRoleAuthorized(roles[i]) == 1)
                return 1;
        }

        return 2;
    }

    protected boolean isDefaultAdmin(String userName)
    {
        if(authContext != null && (authContext instanceof NonPublicAuthContext) && ((NonPublicAuthContext)authContext).getProperties() != null)
            readProperties();
        else
            readPropertiesFromDisk();
        String name = null;
        synchronized(LOCK)
        {
            name = defaultAdminName;
        }
        if(userName == null || name == null)
            return false;
        return userName.equals(((Object) (name)));
    }

    private void readProperties()
    {
        synchronized(LOCK)
        {
            defaultAdminName = ((NonPublicAuthContext)authContext).getProperty("deploy.admin.username");
        }
    }

    private void readPropertiesFromDisk()
    {
        try
        {
            String basedir = System.getProperty("StandAloneServerBasedir");
            File cluster = new File(basedir, "cluster.properties");
            FileInputStream fis = new FileInputStream(cluster);
            Properties cluster_props = new Properties();
            cluster_props.load(((java.io.InputStream) (fis)));
            fis.close();
            synchronized(LOCK)
            {
                defaultAdminName = PropertyUtils.readDefaultString(cluster_props, "deploy.admin.username", ((String) (null)));
            }
        }
        catch(Exception e) { }
    }

    public void setDataFile(File dataFile)
        throws IOException
    {
        this.dataFile = dataFile;
        if(dataFile == null || !dataFile.exists())
        {
            lastModified = 0L;
            roleManager.removeAllRoles();
            roleManager.fireAuthModelReset();
            return;
        }
        if(lastModified >= dataFile.lastModified())
            return;
        lastModified = dataFile.lastModified();
        roleManager.removeAllRoles();
        BufferedReader in = new BufferedReader(((java.io.Reader) (new InputStreamReader(((java.io.InputStream) (new FileInputStream(dataFile)))))));
        int numRows = 0;
        try
        {
            numRows = Integer.parseInt(in.readLine());
        }
        catch(NumberFormatException nfe) { }
        int i = 0;
        for(String line = in.readLine(); i < numRows; line = in.readLine())
        {
            roleManager.addRoleFromString(line);
            i++;
        }

        in.close();
        roleManager.cleanUp();
        roleManager.fireAuthModelReset();
    }

    public File getDataFile()
    {
        return dataFile;
    }

    public boolean usesDataFile()
    {
        return true;
    }

    public boolean hasEditor()
    {
        return true;
    }

    public java.awt.Component getEditorComponent(String appName)
    {
        if(editor == null)
        {
            editor = EditorFactory.createAdminAuthorizationEditor(((MutableAuthorization) (this)));
            if(editor instanceof MutableAuthorizationEditor)
                ((MutableAuthorizationEditor)editor).setAlternateEditor(((java.awt.Component) (new AdminAuthorizationTableEditor(this))));
        }
        return editor;
    }

    public boolean commitChanges()
    {
        if(!isModified())
            return true;
        try
        {
            PrintWriter out = new PrintWriter(((java.io.OutputStream) (new FileOutputStream(dataFile))));
            int rowCount = roleManager.size();
            out.println(rowCount);
            Role role = null;
            for(int r = 0; r < rowCount; r++)
            {
                role = roleManager.get(r);
                out.println(((Object) (role)));
            }

            out.close();
        }
        catch(IOException ioe)
        {
            return false;
        }
        roleManager.fireAuthModelSaved();
        isChanged = false;
        return true;
    }

    public boolean isModified()
    {
        return isChanged;
    }

    public void setAuthGroups(AuthGroups groupData)
    {
        this.groupData = groupData;
        roleManager.setAuthGroups(this.groupData);
    }

    public AuthGroups getAuthGroups()
    {
        return groupData;
    }

    public void tableChanged(TableModelEvent e)
    {
        isChanged = true;
    }

    public AuthorizationModel getAuthorizationModel()
    {
        return ((AuthorizationModel) (roleManager));
    }

    public void setAuthContext(AuthContext context)
    {
        authContext = context;
    }

    public AuthContext getAuthContext()
    {
        return authContext;
    }
}
