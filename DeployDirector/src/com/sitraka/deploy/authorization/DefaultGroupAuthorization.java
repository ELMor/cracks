// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefaultGroupAuthorization.java

package com.sitraka.deploy.authorization;

import com.sitraka.deploy.AuthContext;
import com.sitraka.deploy.AuthGroups;
import com.sitraka.deploy.AuthModelEvent;
import com.sitraka.deploy.AuthModelListener;
import com.sitraka.deploy.AuthorizationModel;
import com.sitraka.deploy.GroupAuthorization;
import com.sitraka.deploy.MutableAuthorization;
import com.sitraka.deploy.MutableAuthorizationEditor;
import com.sitraka.deploy.admin.auth.EditorFactory;
import com.sitraka.deploy.common.jclass.util.JCStringTokenizer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.swing.event.EventListenerList;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

// Referenced classes of package com.sitraka.deploy.authorization:
//            Version, DefaultGroupEditor

public class DefaultGroupAuthorization
    implements GroupAuthorization, MutableAuthorization, AuthorizationModel, TableModelListener
{
    protected class GroupVersion
    {

        protected String group;
        protected String version;

        public String getGroup()
        {
            return group;
        }

        public String getVersion()
        {
            return version;
        }

        public GroupVersion(String group_name, String version_id)
        {
            group = group_name;
            version = version_id;
        }
    }


    protected DefaultTableModel data;
    protected boolean isChanged;
    protected Hashtable appHash;
    protected File dataFile;
    protected java.awt.Component editor;
    public static final String ANY_APPLICATION = "ANY";
    protected long lastModified;
    protected AuthGroups groupData;
    protected EventListenerList listenerList;
    protected AuthContext authContext;

    public DefaultGroupAuthorization()
    {
        isChanged = false;
        appHash = null;
        dataFile = null;
        editor = null;
        lastModified = 0L;
        groupData = null;
        listenerList = new EventListenerList();
        authContext = null;
    }

    public DefaultGroupAuthorization(AuthGroups groups)
    {
        isChanged = false;
        appHash = null;
        dataFile = null;
        editor = null;
        lastModified = 0L;
        groupData = null;
        listenerList = new EventListenerList();
        authContext = null;
        groupData = groups;
    }

    public void setAuthGroups(AuthGroups groups)
    {
        groupData = groups;
    }

    public AuthGroups getAuthGroups()
    {
        return groupData;
    }

    public int isAuthorized(String user_id, String app_name, String version)
    {
        if(user_id == null || user_id.trim().length() == 0)
            return 2;
        if(app_name == null)
            app_name = "ANY";
        Vector versionList = null;
        if("ANY".equalsIgnoreCase(app_name))
        {
            versionList = getVersionForUserApp(user_id, "ANY");
            if(versionList.size() == 0)
                return 2;
        } else
        {
            versionList = getVersionForUserApp(user_id, app_name);
            if(versionList.size() == 0)
            {
                versionList = getVersionForUserApp(user_id, "ANY");
                if(versionList.size() == 0)
                    return 2;
            }
        }
        Version checkVersion = new Version(version);
        for(int i = 0; i < versionList.size(); i++)
        {
            Version tmp_version = new Version((String)versionList.elementAt(i));
            if(checkVersion.implies(tmp_version))
                return 1;
        }

        return 2;
    }

    public boolean usesDataFile()
    {
        return true;
    }

    public void setDataFile(File data_file)
        throws IOException
    {
        if(dataFile == null || data_file == null)
            appHash = null;
        else
        if(dataFile.lastModified() > lastModified)
            appHash = null;
        dataFile = data_file;
        Vector rows = null;
        if(dataFile == null || !dataFile.exists())
        {
            data = new DefaultTableModel();
            rows = new Vector();
        } else
        {
            if(lastModified >= dataFile.lastModified())
                return;
            lastModified = dataFile.lastModified();
            BufferedReader in = new BufferedReader(((java.io.Reader) (new FileReader(dataFile))));
            String line = in.readLine();
            int num_rows = 0;
            try
            {
                num_rows = Integer.parseInt(line);
                rows = new Vector(num_rows);
            }
            catch(NumberFormatException nfe)
            {
                num_rows = 0x7fffffff;
                rows = new Vector();
                if(line.indexOf(",") >= 0)
                    rows.addElement(((Object) (parseLine(line))));
            }
            for(int i = 0; i < num_rows; i++)
            {
                line = in.readLine();
                if(line == null || line.length() == 0)
                {
                    num_rows = i;
                    break;
                }
                rows.addElement(((Object) (parseLine(line))));
            }

            in.close();
        }
        Vector columns = new Vector(3);
        columns.addElement("Bundle");
        columns.addElement("Version");
        columns.addElement("Group/User");
        if(data == null)
        {
            data = new DefaultTableModel(rows, columns);
        } else
        {
            data.setNumRows(0);
            data.setDataVector(rows, columns);
        }
        ((AbstractTableModel) (data)).addTableModelListener(((TableModelListener) (this)));
        fireAuthModelReset();
    }

    protected Vector parseLine(String line)
    {
        String tokens[] = JCStringTokenizer.parse(line, ',');
        Vector row = new Vector(3);
        for(int i = 0; i < tokens.length && i < 3; i++)
            row.addElement(((Object) (tokens[i])));

        if(tokens.length < 3)
            for(; row.size() < 3; row.addElement(""));
        return row;
    }

    public File getDataFile()
    {
        return dataFile;
    }

    public boolean hasEditor()
    {
        return true;
    }

    public java.awt.Component getEditorComponent(String app_name)
    {
        if(editor == null)
        {
            editor = EditorFactory.createBundleAuthorizationEditor(((MutableAuthorization) (this)));
            if(editor instanceof MutableAuthorizationEditor)
            {
                ((MutableAuthorizationEditor)editor).setAlternateEditor(((java.awt.Component) (new DefaultGroupEditor(this))));
                ArrayList keywords = new ArrayList(1);
                keywords.add(((Object) (Version.ANY_RELEASE)));
                ((MutableAuthorizationEditor)editor).setKeywords(((List) (keywords)));
            }
        }
        return editor;
    }

    public boolean commitChanges()
    {
        if(!isModified())
            return true;
        try
        {
            PrintWriter out = new PrintWriter(((java.io.OutputStream) (new FileOutputStream(dataFile))));
            for(int i = 0; i < data.getRowCount(); i++)
            {
                String app = (String)data.getValueAt(i, 0);
                String version = (String)data.getValueAt(i, 1);
                String group = (String)data.getValueAt(i, 2);
                if((app == null || app.length() <= 0) && (version == null || version.length() <= 0) && (group == null || group.length() <= 0))
                {
                    data.removeRow(i);
                    i--;
                }
            }

            int row_count = data.getRowCount();
            out.println(row_count);
            for(int i = 0; i < row_count; i++)
                out.println((String)data.getValueAt(i, 0) + "," + (String)data.getValueAt(i, 1) + "," + (String)data.getValueAt(i, 2));

            out.close();
        }
        catch(IOException ioe)
        {
            return false;
        }
        isChanged = false;
        buildIndices();
        return true;
    }

    public boolean isModified()
    {
        return isChanged;
    }

    public void tableChanged(TableModelEvent e)
    {
        isChanged = true;
        fireAuthModelChanged();
    }

    public void setAuthContext(AuthContext context)
    {
        authContext = context;
    }

    public AuthContext getAuthContext()
    {
        return authContext;
    }

    public AuthorizationModel getAuthorizationModel()
    {
        return ((AuthorizationModel) (this));
    }

    public List getContexts()
    {
        ArrayList contexts = new ArrayList();
        String c = null;
        for(int i = 0; i < data.getRowCount(); i++)
        {
            c = (String)data.getValueAt(i, 0);
            if(c != null && c.trim().length() > 0 && !contexts.contains(((Object) (c))))
                contexts.add(((Object) (c)));
        }

        return ((List) (contexts));
    }

    public List getUsers(Object context)
    {
        return getValues(findRows(context.toString(), 0), 2);
    }

    public List getAccess(Object context, Object user)
    {
        return getValues(findRows(user.toString(), 2, findRows(context.toString(), 0)), 1);
    }

    public void setAccess(Object context, Object user, List access)
    {
        List rows = findRows(user.toString(), 2, findRows(context.toString(), 0));
        if(access != null && access.size() > 0)
        {
            if(rows.size() == 0)
            {
                data.addRow(new Object[] {
                    context, access.get(0), user
                });
            } else
            {
                data.setValueAt(access.get(0), ((Integer)rows.get(0)).intValue(), 1);
                for(int i = 1; i < rows.size(); i++)
                    data.removeRow(((Integer)rows.get(i)).intValue());

            }
        } else
        {
            for(Iterator it = rows.iterator(); it.hasNext(); data.removeRow(((Integer)it.next()).intValue()));
        }
        isChanged = true;
    }

    private List findRows(String search, int column, List rows)
    {
        ArrayList results = new ArrayList();
        int count = rows != null ? rows.size() : data.getRowCount();
        int row = 0;
        for(int i = 0; i < count; i++)
        {
            row = rows != null ? ((Integer)rows.get(i)).intValue() : i;
            if(((String)data.getValueAt(row, column)).equals(((Object) (search))))
                results.add(((Object) (new Integer(row))));
        }

        return ((List) (results));
    }

    private List findRows(String search, int column)
    {
        return findRows(search, column, ((List) (null)));
    }

    private List getValues(List rows, int column)
    {
        ArrayList results = new ArrayList();
        String value = null;
        for(int i = 0; i < rows.size(); i++)
        {
            value = (String)data.getValueAt(((Integer)rows.get(i)).intValue(), column);
            if(value != null && value.trim().length() > 0 && !results.contains(((Object) (value))))
                results.add(((Object) (value)));
        }

        return ((List) (results));
    }

    public void addAuthModelListener(AuthModelListener aml)
    {
        listenerList.add(((Object) (aml)).getClass(), ((java.util.EventListener) (aml)));
    }

    public void removeAuthModelListener(AuthModelListener aml)
    {
        listenerList.remove(((Object) (aml)).getClass(), ((java.util.EventListener) (aml)));
    }

    public void fireAuthModelChanged()
    {
        AuthModelEvent e = new AuthModelEvent(((com.sitraka.deploy.AuthModel) (this)));
        Object listeners[] = listenerList.getListenerList();
        for(int i = 0; i < listeners.length; i++)
            if(listeners[i] instanceof AuthModelListener)
                ((AuthModelListener)listeners[i]).modelChanged(e);

    }

    public void fireAuthModelReset()
    {
        AuthModelEvent e = new AuthModelEvent(((com.sitraka.deploy.AuthModel) (this)));
        Object listeners[] = listenerList.getListenerList();
        for(int i = 0; i < listeners.length; i++)
            if(listeners[i] instanceof AuthModelListener)
                ((AuthModelListener)listeners[i]).modelReset(e);

    }

    public void fireAuthModelSaved()
    {
        AuthModelEvent e = new AuthModelEvent(((com.sitraka.deploy.AuthModel) (this)));
        Object listeners[] = listenerList.getListenerList();
        for(int i = 0; i < listeners.length; i++)
            if(listeners[i] instanceof AuthModelListener)
                ((AuthModelListener)listeners[i]).modelSaved(e);

    }

    protected void buildIndices()
    {
        appHash = new Hashtable();
        for(int i = 0; i < data.getRowCount(); i++)
        {
            String bundle = (String)data.getValueAt(i, 0);
            bundle = normalizeAppName(bundle).toLowerCase();
            Vector bundle_list = null;
            if(appHash.containsKey(((Object) (bundle))))
            {
                bundle_list = (Vector)appHash.get(((Object) (bundle)));
            } else
            {
                bundle_list = new Vector();
                appHash.put(((Object) (bundle)), ((Object) (bundle_list)));
            }
            String version = (String)data.getValueAt(i, 1);
            String group = (String)data.getValueAt(i, 2);
            if(group != null && group.length() != 0)
            {
                if(version == null || version.length() == 0)
                    version = "0.0.0*";
                bundle_list.addElement(((Object) (new GroupVersion(group, version))));
            }
        }

    }

    protected String normalizeAppName(String appName)
    {
        if(appName == null || appName.equals("") || "ANY".equalsIgnoreCase(appName))
            appName = "ANY";
        return appName;
    }

    protected Vector getVersionForUserApp(String user, String app)
    {
        if(appHash == null)
            buildIndices();
        Vector bundle_list = (Vector)appHash.get(((Object) (app.toLowerCase())));
        if(bundle_list == null)
            return new Vector();
        Vector versions = new Vector();
        for(int i = 0; i < bundle_list.size(); i++)
        {
            GroupVersion gv = (GroupVersion)bundle_list.elementAt(i);
            if(belongsTo(user, gv.getGroup()))
                versions.addElement(((Object) (gv.getVersion())));
        }

        return versions;
    }

    protected boolean belongsTo(String group, String user)
    {
        if(groupData == null)
            return false;
        else
            return groupData.belongsTo(group, user);
    }
}
