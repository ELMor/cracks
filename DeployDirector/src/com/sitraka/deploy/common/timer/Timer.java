// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Timer.java

package com.sitraka.deploy.common.timer;

import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.common.timer:
//            TimerEvent

public class Timer extends Thread
{

    protected Vector queue;
    protected boolean done;
    protected static Timer timer = null;

    public static Timer getTimer()
    {
        if(timer == null)
        {
            timer = new Timer();
            ((Thread) (timer)).setPriority(2);
            ((Thread) (timer)).setDaemon(true);
            ((Thread) (timer)).start();
        }
        return timer;
    }

    public Timer()
    {
        queue = new Vector();
        done = false;
        ((Thread)this).setName("Timed Event Manager");
    }

    public synchronized void stopTimer()
    {
        done = true;
        ((Object)this).notify();
    }

    public Vector getQueue()
    {
        return queue;
    }

    public void setQueue(Vector queue)
    {
        this.queue = queue;
    }

    public synchronized void addTimerEvent(TimerEvent new_event)
    {
        int lo = 0;
        int hi = queue.size();
        long newTime = new_event.getNextEventTime();
        if(done)
            return;
        if(hi == 0)
        {
            queue.addElement(((Object) (new_event)));
        } else
        {
            while(hi - lo > 0) 
            {
                int mid = hi + lo >> 1;
                TimerEvent e = (TimerEvent)queue.elementAt(mid);
                long midTime = e.getNextEventTime();
                if(midTime < newTime)
                {
                    lo = mid + 1;
                    continue;
                }
                if(midTime > newTime)
                {
                    hi = mid;
                    continue;
                }
                lo = mid;
                break;
            }
            if(lo < hi && ((TimerEvent)queue.elementAt(lo)).getNextEventTime() > newTime)
                lo++;
            queue.insertElementAt(((Object) (new_event)), lo);
        }
        ((Object)this).notify();
    }

    public synchronized Object recallTimer(Object timer)
    {
        int lo = 0;
        int hi = queue.size();
        int limit = hi;
        long destTime = ((TimerEvent)timer).getNextEventTime();
        if(hi == 0)
            return ((Object) (null));
        while(hi - lo > 0) 
        {
            int mid = hi + lo >> 1;
            TimerEvent e = (TimerEvent)queue.elementAt(mid);
            long midTime = e.getNextEventTime();
            if(midTime < destTime)
            {
                lo = mid + 1;
                continue;
            }
            if(midTime > destTime)
            {
                hi = mid;
                continue;
            }
            lo = mid;
            for(int i = mid - 1; i >= 0; i--)
            {
                e = (TimerEvent)queue.elementAt(i);
                if(e.getNextEventTime() != midTime)
                    break;
                lo = i;
            }

            break;
        }
        while(lo < limit) 
        {
            TimerEvent e = (TimerEvent)queue.elementAt(lo);
            if(e.getNextEventTime() == destTime)
            {
                if(e == timer)
                {
                    queue.removeElementAt(lo);
                    break;
                }
                lo++;
            } else
            {
                return ((Object) (null));
            }
        }
        if(lo == 0)
            ((Object)this).notify();
        return timer;
    }

    protected synchronized TimerEvent getNextEvent()
    {
        do
        {
            while(queue.size() == 0) 
            {
                if(done)
                    return null;
                try
                {
                    ((Object)this).wait();
                }
                catch(InterruptedException e) { }
            }
            TimerEvent e = (TimerEvent)queue.elementAt(0);
            long now = System.currentTimeMillis();
            long dt = e.getNextEventTime() - now;
            if(dt <= 0L)
            {
                queue.removeElementAt(0);
                return e;
            }
            try
            {
                ((Object)this).wait(dt);
            }
            catch(InterruptedException ie) { }
        } while(true);
    }

    public synchronized void flushQueue()
    {
        TimerEvent e;
        for(; queue.size() > 0; e.flushEvent())
        {
            done = true;
            e = (TimerEvent)queue.elementAt(0);
            queue.removeElementAt(0);
        }

    }

    public void run()
    {
        Thread.currentThread().setPriority(2);
        do
        {
            TimerEvent e = getNextEvent();
            if(done)
                break;
            try
            {
                e.processEvent();
            }
            catch(Exception ex) { }
            catch(Error ex) { }
            if(e.getNextEventTime() != 0L)
                addTimerEvent(e);
        } while(true);
        timer = null;
    }

}
