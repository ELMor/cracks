// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ConnectionPolicy.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.CommonEnums;
import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.util.PropertyUtils;
import java.util.Date;
import org.w3c.dom.Node;

public class ConnectionPolicy
{

    protected int when;
    protected int accessPolicy;
    protected Date schedule;
    protected String interval;

    public ConnectionPolicy()
    {
        when = 1;
        accessPolicy = 1;
        schedule = null;
    }

    public ConnectionPolicy(Node connect_node)
        throws XmlException
    {
        String when_string = XmlSupport.getAttribute(connect_node, "WHEN");
        when = PropertyUtils.toEnum(when_string, CommonEnums.CONNECTION_POLICY_NAMES, CommonEnums.CONNECTION_POLICY_VALUES);
        if(when == -1)
            when = 1;
        String access_string = XmlSupport.getAttribute(connect_node, "ACCESS");
        accessPolicy = PropertyUtils.toEnum(access_string, CommonEnums.ACCESS_POLICY_NAMES, CommonEnums.ACCESS_POLICY_VALUES);
        if(accessPolicy == -1)
            accessPolicy = 1;
        String time_string = XmlSupport.getAttribute(connect_node, "SCHEDULE");
        if(time_string != null)
        {
            int index = time_string.indexOf(";");
            if(index > 0)
            {
                long time = PropertyUtils.parseLongDate(time_string);
                if(time != 0L)
                    schedule = new Date(time);
                else
                    throw new XmlException("XML Error: unable to parse time string '" + time_string + "'");
            }
            if(index + 1 < time_string.length())
                interval = time_string.substring(index + 1);
        }
        if(when == 2 && time_string == null)
            throw new XmlException("XML Error: connection policy is 'scheduled' and no schedule was provided");
        else
            return;
    }

    public int getAccessPolicy()
    {
        return accessPolicy;
    }

    public void setAccessPolicy(int access)
    {
        accessPolicy = access;
    }

    public int getWhen()
    {
        return when;
    }

    public void setWhen(int when)
    {
        this.when = when;
    }

    public Date getStartScheduleDate()
    {
        return schedule;
    }

    public void setStartScheduleDate(Date date)
    {
        schedule = date;
    }

    public String getScheduleInterval()
    {
        return interval;
    }

    public void setScheduleInterval(String interval)
    {
        this.interval = interval;
    }

    public String toString()
    {
        StringBuffer rep = new StringBuffer("access=");
        String value = PropertyUtils.fromEnum(accessPolicy, CommonEnums.ACCESS_POLICY_NAMES, CommonEnums.ACCESS_POLICY_VALUES);
        if(value == null)
            value = "--unknown--";
        rep.append(value + "\n");
        rep.append("connect=");
        value = PropertyUtils.fromEnum(when, CommonEnums.CONNECTION_POLICY_NAMES, CommonEnums.CONNECTION_POLICY_VALUES);
        if(value == null)
            value = "--unknown--";
        rep.append(value + "\n");
        if(interval != null)
        {
            rep.append("\nconnect.schedule=");
            if(schedule != null)
                rep.append(PropertyUtils.writeLongDate(schedule));
            rep.append(";" + interval);
        }
        return rep.toString();
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<CONNECTION");
        String value = PropertyUtils.fromEnum(when, CommonEnums.CONNECTION_POLICY_NAMES, CommonEnums.CONNECTION_POLICY_VALUES);
        if(value != null)
            sb.append(" WHEN=\"" + XmlSupport.toCDATA(value) + "\"");
        value = PropertyUtils.fromEnum(accessPolicy, CommonEnums.ACCESS_POLICY_NAMES, CommonEnums.ACCESS_POLICY_VALUES);
        if(value != null)
            sb.append(" ACCESS=\"" + XmlSupport.toCDATA(value) + "\"");
        if(interval != null)
        {
            sb.append(" SCHEDULE=\"");
            if(schedule != null)
                sb.append(XmlSupport.toCDATA(PropertyUtils.writeLongDate(schedule)));
            sb.append(";" + XmlSupport.toCDATA(interval) + "\"");
        }
        sb.append("/>\n");
        return sb.toString();
    }
}
