// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CheckNames.java

package com.sitraka.deploy.common.app;


public class CheckNames
{

    public static final String INVALID_FILE = "`!#$%^&*()+{}[]\\|;:'\",<>/?";
    public static final String INVALID_VERSION = "`!#$%^&*()+{}[]\\|;:'\",<>/?";
    public static final String INVALID_PLATFORM = "`!#$%^&*()+{}[]\\|;:'\",<>/?";
    public static final String INVALID_JRE = "`!#$%^&*()+{}[]\\|;:'\",<>/?";
    public static final String INVALID_BUNDLE = "`!#$%^&*()+{}[]\\|;:'\",<>/?";
    public static final String INVALID_INITIAL = ".";
    public static final String INVALID_FINAL = ".";
    public static final String INVALID_FINAL_JRE = ".+*-";

    public CheckNames()
    {
    }

    public static boolean checkBundleName(String name)
    {
        return name.equals(((Object) (fixString(name, "`!#$%^&*()+{}[]\\|;:'\",<>/?", ".", "."))));
    }

    public static String fixBundleName(String name)
    {
        return fixString(name, "`!#$%^&*()+{}[]\\|;:'\",<>/?", ".", ".").trim();
    }

    public static boolean checkVersionName(String name)
    {
        return name.equals(((Object) (fixString(name, "`!#$%^&*()+{}[]\\|;:'\",<>/?", ".", "."))));
    }

    public static String fixVersionName(String name)
    {
        return fixString(name, "`!#$%^&*()+{}[]\\|;:'\",<>/?", ".", ".").trim();
    }

    public static boolean checkPlatformName(String name)
    {
        return name.equals(((Object) (fixString(name, "`!#$%^&*()+{}[]\\|;:'\",<>/?", ".", "."))));
    }

    public static String fixFileName(String name)
    {
        return fixString(name, "`!#$%^&*()+{}[]\\|;:'\",<>/?", ".", ".");
    }

    public static boolean checkFileName(String name)
    {
        return name.equals(((Object) (fixString(name, "`!#$%^&*()+{}[]\\|;:'\",<>/?", ".", "."))));
    }

    public static String fixPlatformName(String name)
    {
        return fixString(name, "`!#$%^&*()+{}[]\\|;:'\",<>/?", ".", ".").trim();
    }

    public static boolean checkJREName(String name)
    {
        return name.equals(((Object) (fixString(name, "`!#$%^&*()+{}[]\\|;:'\",<>/?", ".", ".+*-"))));
    }

    public static String fixJREName(String name)
    {
        return fixString(name, "`!#$%^&*()+{}[]\\|;:'\",<>/?", ".", ".+*-").trim();
    }

    public static String fixString(String name, String invalid, String invalidStart, String invalidFinal)
    {
        name = name.trim();
        if(name.length() == 0)
            return name;
        int size = invalid.length();
        for(int i = 0; i < size; i++)
            name = name.replace(invalid.charAt(i), '_');

        if(invalidStart.indexOf(((int) (name.charAt(0)))) != -1)
            name = "_" + name.substring(1, name.length());
        if(invalidFinal.indexOf(((int) (name.charAt(name.length() - 1)))) != -1)
            name = name.substring(0, name.length() - 1) + "_";
        return name;
    }
}
