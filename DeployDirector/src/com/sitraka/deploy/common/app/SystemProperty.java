// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SystemProperty.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import org.w3c.dom.Node;

// Referenced classes of package com.sitraka.deploy.common.app:
//            CommonInterface

public class SystemProperty
    implements CommonInterface
{

    protected String name;
    protected String value;

    public SystemProperty()
    {
        name = "";
        value = "";
    }

    public SystemProperty(Node prop_node)
        throws XmlException
    {
        name = "";
        value = "";
        name = XmlSupport.getAttribute(prop_node, "NAME");
        value = XmlSupport.getAttribute(prop_node, "VALUE");
        if(name == null)
            throw new XmlException("XML Error: NAME is missing in SYSTEMPROPERTY tag");
        if(value == null)
            throw new XmlException("XML Error: VALUE is missing in SYSTEMPROPERTY tag");
        else
            return;
    }

    public String getPropertiesFile(int base)
    {
        StringBuffer sb = new StringBuffer();
        sb.append("systemprop" + base + ".name=" + name + "\n");
        sb.append("systemprop" + base + ".value=" + value + "\n");
        return sb.toString();
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }

    public boolean isValid()
    {
        return getName() != null && !"".equals(((Object) (getName()))) && getValue() != null && !"".equals(((Object) (getValue())));
    }

    public boolean equals(Object obj)
    {
        if(!(obj instanceof SystemProperty))
            return false;
        SystemProperty other = (SystemProperty)obj;
        String otherPropFile = other.getPropertiesFile(-1, ((String) (null)));
        String ourPropFile = getPropertiesFile(-1, ((String) (null)));
        return ourPropFile.equalsIgnoreCase(otherPropFile);
    }

    public boolean isChanged(Object obj)
    {
        if(!(obj instanceof SystemProperty))
            return true;
        if(equals(obj))
            return false;
        SystemProperty other = (SystemProperty)obj;
        String otherName = other.name.toLowerCase();
        String otherValue = other.value;
        return name.equalsIgnoreCase(otherName) && !value.equals(((Object) (otherValue)));
    }

    public String getPropertiesFile(int base, String prefix)
    {
        StringBuffer sb = new StringBuffer();
        String my_prefix = prefix != null ? prefix + "." : "";
        String my_base = base != -1 ? "" + base : "";
        sb.append(my_prefix + "systemprop" + my_base + ".name=" + name + "\n");
        sb.append(my_prefix + "systemprop" + my_base + ".value=" + value + "\n");
        return sb.toString();
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<SYSTEMPROP");
        sb.append(" NAME=\"" + XmlSupport.toCDATA(name) + "\"");
        sb.append(" VALUE=\"" + XmlSupport.toCDATA(value) + "\"");
        sb.append("/>\n");
        return sb.toString();
    }
}
