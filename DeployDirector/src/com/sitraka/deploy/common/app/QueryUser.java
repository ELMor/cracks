// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   QueryUser.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.XmlSupport;
import org.w3c.dom.Node;

// Referenced classes of package com.sitraka.deploy.common.app:
//            CommonInterface

public class QueryUser
    implements CommonInterface
{

    protected String installdir;
    protected String shortcuts;
    protected String camConfig;

    public QueryUser()
    {
        installdir = "false";
        shortcuts = "false";
        camConfig = "false";
    }

    public QueryUser(Node shortcut_node)
    {
        installdir = "false";
        shortcuts = "false";
        camConfig = "false";
        installdir = XmlSupport.getAttribute(shortcut_node, "INSTALLDIR");
        shortcuts = XmlSupport.getAttribute(shortcut_node, "SHORTCUTS");
        camConfig = XmlSupport.getAttribute(shortcut_node, "CAMCONFIG");
        if(installdir == null)
            installdir = "false";
        if(shortcuts == null)
            shortcuts = "false";
        if(camConfig == null)
            camConfig = "false";
    }

    public boolean isInstallDir()
    {
        return installdir.equalsIgnoreCase("true");
    }

    public String getInstallDir()
    {
        return installdir;
    }

    public void setInstallDir(boolean installDir)
    {
        if(installDir)
            installdir = "true";
        else
            installdir = "false";
    }

    public void setInstallDir(String installDir)
    {
        if(installDir.equalsIgnoreCase("true"))
            installdir = "true";
        else
            installdir = "false";
    }

    public boolean isShortcuts()
    {
        return shortcuts.equalsIgnoreCase("true");
    }

    public String getShortcuts()
    {
        return shortcuts;
    }

    public void setShortcuts(boolean shortcuts)
    {
        if(shortcuts)
            this.shortcuts = "true";
        else
            this.shortcuts = "false";
    }

    public void setShortcuts(String shortcuts)
    {
        if(shortcuts.equalsIgnoreCase("true"))
            this.shortcuts = "true";
        else
            this.shortcuts = "false";
    }

    public boolean isCAMConfig()
    {
        return camConfig.equalsIgnoreCase("true");
    }

    public String getCAMConfig()
    {
        return camConfig;
    }

    public void setCAMConfig(boolean camConfig)
    {
        if(camConfig)
            this.camConfig = "true";
        else
            this.camConfig = "false";
    }

    public void setCAMConfig(String camConfig)
    {
        if(camConfig.equalsIgnoreCase("true"))
            this.camConfig = "true";
        else
            this.camConfig = "false";
    }

    public boolean equals(Object obj)
    {
        if(!(obj instanceof QueryUser))
            return false;
        QueryUser other = (QueryUser)obj;
        String otherPropFile = other.getPropertiesFile(-1, ((String) (null)));
        String ourPropFile = getPropertiesFile(-1, ((String) (null)));
        return ourPropFile.equalsIgnoreCase(otherPropFile);
    }

    public boolean isChanged(Object obj)
    {
        return false;
    }

    public String getPropertiesFile(int base, String prefix)
    {
        String my_prefix = prefix != null ? prefix + "." : "";
        StringBuffer sb = new StringBuffer();
        sb.append(my_prefix + "query.installdir=" + installdir + "\n");
        sb.append(my_prefix + "query.shortcuts=" + shortcuts + "\n");
        sb.append(my_prefix + "query.camconfig=" + camConfig + "\n");
        return sb.toString();
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<QUERYUSER INSTALLDIR=\"" + XmlSupport.toCDATA(installdir) + "\"");
        sb.append(" SHORTCUTS=\"" + XmlSupport.toCDATA(shortcuts) + "\"");
        sb.append(" CAMCONFIG=\"" + XmlSupport.toCDATA(camConfig) + "\"/>\n");
        return sb.toString();
    }
}
