// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AppFile.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.common.jar.JarEntry;
import com.sitraka.deploy.common.jar.JarFile;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.Hashcode;
import com.sitraka.deploy.util.SortInterface;
import com.sitraka.deploy.util.SortUtils;
import java.io.File;
import java.util.Enumeration;
import java.util.Vector;
import org.w3c.dom.Node;

// Referenced classes of package com.sitraka.deploy.common.app:
//            CommonInterface

public class AppFile
    implements CommonInterface, SortInterface
{

    protected String name;
    protected String source;
    protected boolean directory;
    protected Vector archiveDetails;
    protected String hash;
    protected Long fileSize;
    protected String parent;

    public AppFile()
    {
        name = null;
        source = "";
        archiveDetails = new Vector();
        hash = null;
        parent = null;
    }

    public AppFile(String name)
    {
        this.name = null;
        source = "";
        archiveDetails = new Vector();
        hash = null;
        parent = null;
        setName(name);
    }

    public AppFile(File file)
    {
        this(file, ((String) (null)), false);
    }

    public AppFile(File file, String source)
    {
        this(file, source, false);
    }

    public AppFile(File file, String source, boolean explicit)
    {
        this(file, source, explicit, true);
    }

    public AppFile(File file, String source, boolean explicit, boolean crackArchive)
    {
        name = null;
        this.source = "";
        archiveDetails = new Vector();
        hash = null;
        parent = null;
        String fileString = file.getAbsolutePath();
        if(source == null)
        {
            fileString = file.getName();
            source = "";
        } else
        {
            fileString = fileString.substring(source.length());
            if(fileString.startsWith(File.separator))
                fileString = fileString.substring(File.separator.length());
        }
        setName(fileString);
        setSource(source);
        setExplicitSource(explicit);
        if(file.isDirectory())
        {
            setDirectory(true);
            setHash("");
            setSize(new Long(0L));
        } else
        {
            setDirectory(false);
            setHash(Hashcode.computeHash(file));
            setSize(new Long(file.length()));
        }
        if(crackArchive && isArchive())
            expandJarFile(file);
    }

    public static void validateNode(Node file_node)
        throws XmlException
    {
        validateName(XmlSupport.getAttribute(file_node, "NAME"));
        org.w3c.dom.traversal.TreeWalker treeWalker = XmlSupport.createTreeWalker(file_node);
        for(Node node = XmlSupport.getNextElementNamed(treeWalker, "FILE"); node != null; node = XmlSupport.getNextElementNamed(treeWalker, "FILE"))
        {
            if(node.getParentNode() != file_node)
                break;
            validateNode(node);
        }

    }

    public static void validateName(String text)
        throws XmlException
    {
        if(text == null)
            throw new XmlException("XML error. Missing filname in AppFile object");
        if(text.indexOf(",") != -1)
            throw new XmlException("XML error. ',' is not allowed in file name " + text);
        if(text.indexOf("%") != -1)
            throw new XmlException("XML error. '%' is not allowed in file name " + text);
        if(text.indexOf("|") != -1)
            throw new XmlException("XML error. '|' is not allowed in file name " + text);
        else
            return;
    }

    public AppFile(Node file_node)
        throws XmlException
    {
        name = null;
        source = "";
        archiveDetails = new Vector();
        hash = null;
        parent = null;
        setName(XmlSupport.getAttribute(file_node, "NAME"));
        validateName(getName());
        setSource(XmlSupport.getAttribute(file_node, "SOURCE"));
        if(source == null)
            setSource("");
        String dir = XmlSupport.getAttribute(file_node, "DIRECTORY");
        if(dir.equals("true"))
            directory = true;
        else
            directory = false;
        setDirectory(directory);
        String size = XmlSupport.getAttribute(file_node, "SIZE");
        if(size == null)
            fileSize = new Long(0L);
        else
            fileSize = new Long(size);
        hash = XmlSupport.getAttribute(file_node, "HASH");
        org.w3c.dom.traversal.TreeWalker treeWalker = XmlSupport.createTreeWalker(file_node);
        for(Node node = XmlSupport.getNextElementNamed(treeWalker, "FILE"); node != null; node = XmlSupport.getNextElementNamed(treeWalker, "FILE"))
            if(node.getParentNode() == file_node)
            {
                AppFile f = new AppFile(node);
                if(archiveDetails == null)
                    archiveDetails = new Vector();
                SortUtils.insertSorted(archiveDetails, ((SortInterface) (f)), true);
            }

    }

    public void setName(String name)
    {
        this.name = FileUtils.makeInternalPath(name);
        setDirectory(isDirectory());
    }

    public String getName()
    {
        return name;
    }

    public void setSource(String newSource)
    {
        if(newSource == null)
            source = "";
        else
        if(source.endsWith("*"))
        {
            if(newSource.endsWith("*"))
                source = FileUtils.makeInternalPath(newSource);
            else
                source = FileUtils.makeInternalPath(newSource) + "*";
        } else
        {
            source = FileUtils.makeInternalPath(newSource);
        }
    }

    public String getSource()
    {
        if(source.endsWith("*"))
            return source.substring(0, source.length() - 1);
        else
            return source;
    }

    public File getSourceFile()
    {
        File sourceFile = new File(getSource() + File.separator + getName());
        if(sourceFile.exists())
            return sourceFile;
        else
            return null;
    }

    public boolean isExplicitSource()
    {
        return !source.endsWith("*");
    }

    public void setExplicitSource(boolean explicit)
    {
        if(explicit)
        {
            if(source.endsWith("*"))
                source = source.substring(0, source.length() - 1);
        } else
        if(!source.endsWith("*"))
            source = source + "*";
    }

    public void setDirectory(boolean dir)
    {
        directory = dir;
        if(dir)
        {
            if(name != null && !name.endsWith("/"))
                name = name + "/";
        } else
        if(name != null && name.endsWith("/"))
            name = name.substring(0, name.length() - 1);
    }

    public boolean isDirectory()
    {
        return directory;
    }

    public void setArchiveDetails(Vector files)
    {
        archiveDetails = files;
    }

    public Vector getArchiveDetails()
    {
        return archiveDetails;
    }

    public void setHash(String hash)
    {
        this.hash = hash;
    }

    public String getHash()
    {
        return hash;
    }

    public void setSize(Long fileSize)
    {
        this.fileSize = fileSize;
    }

    public Long getSize()
    {
        return fileSize;
    }

    public void setParent(String parent)
    {
        this.parent = parent;
    }

    public String getParent()
    {
        return parent;
    }

    public boolean isArchive()
    {
        return getName().toLowerCase().endsWith(".jar") || getName().toLowerCase().endsWith(".zip");
    }

    public boolean isSystem()
    {
        return getName().toLowerCase().endsWith(".exe") || getName().toLowerCase().endsWith(".dll") || getName().toLowerCase().endsWith(".bat");
    }

    public boolean expandJarFile(File file)
    {
        archiveDetails.removeAllElements();
        JarFile jarFile = new JarFile(file, true);
        AppFile subAppFile;
        for(Enumeration entryList = jarFile.listClasses(); entryList.hasMoreElements(); SortUtils.insertSorted(archiveDetails, ((SortInterface) (subAppFile)), true))
        {
            String entryString = (String)entryList.nextElement();
            JarEntry entry = jarFile.getEntry(entryString);
            subAppFile = new AppFile();
            subAppFile.setName(entry.getName());
            subAppFile.setDirectory(entry.isDirectory());
            subAppFile.setSource(getName());
            subAppFile.setHash(entry.getHashcode());
            subAppFile.setSize(new Long(entry.getSize()));
        }

        return true;
    }

    public String toString()
    {
        return getPropertiesFile(-1, ((String) (null)));
    }

    public boolean isCompoundObject()
    {
        boolean has_kids = archiveDetails != null ? archiveDetails.size() != 0 : false;
        return isArchive() && has_kids;
    }

    public void expandInto(Vector vec)
    {
        if(!isCompoundObject())
        {
            SortUtils.insertSorted(vec, ((SortInterface) (this)), true);
            return;
        }
        int size = archiveDetails.size();
        for(int i = 0; i < size; i++)
        {
            AppFile file = (AppFile)archiveDetails.elementAt(i);
            file.setParent(name);
            SortUtils.insertSorted(vec, ((SortInterface) (file)), true);
        }

    }

    public int getArchivePropertiesFile(StringBuffer sb, int base, String prefix)
    {
        if(!isCompoundObject())
        {
            sb.append(getPropertiesFile(base, prefix));
            return base + 1;
        }
        String my_prefix = prefix != null ? prefix + "." : "";
        int size = archiveDetails.size();
        for(int i = 0; i < size; i++)
        {
            AppFile file = (AppFile)archiveDetails.elementAt(i);
            sb.append(file.getPropertiesFile(base + i, prefix));
            sb.append(my_prefix + "file" + (base + i) + ".parent=" + name + "\n\n");
        }

        return base + size;
    }

    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if((obj instanceof String))
            return FileUtils.compareIgnoreCaseAndSep(name, (String)obj);
        if(!(obj instanceof AppFile))
            return false;
        AppFile other = (AppFile)obj;
        return FileUtils.compareIgnoreCaseAndSep(name, other.getName()) && getSource().equalsIgnoreCase(other.getSource()) && isExplicitSource() == other.isExplicitSource() && directory == other.isDirectory() && hash.equals(((Object) (other.getHash())));
    }

    public boolean isEqualFile(Object obj)
    {
        if(obj == null || !(obj instanceof AppFile))
        {
            return false;
        } else
        {
            AppFile other = (AppFile)obj;
            return FileUtils.compareIgnoreCaseAndSep(name, other.getName()) && directory == other.isDirectory() && hash.equals(((Object) (other.getHash())));
        }
    }

    public boolean isChanged(Object obj)
    {
        if(!(obj instanceof AppFile))
            return true;
        if(equals(obj))
            return false;
        AppFile other = (AppFile)obj;
        String otherName = other.getName().toLowerCase();
        String otherHash = other.getHash();
        return name.equalsIgnoreCase(otherName) && !hash.equals(((Object) (otherHash)));
    }

    public String getPropertiesFile(int base, String prefix)
    {
        StringBuffer sb = new StringBuffer();
        String my_prefix = prefix != null ? prefix + "." : "";
        String my_base = base != -1 ? "" + base : "";
        sb.append(my_prefix + "file" + my_base + ".name=" + name + "\n");
        if(source != null)
            sb.append(my_prefix + "file" + my_base + ".source=" + source + "\n");
        if(directory)
            sb.append(my_prefix + "file" + my_base + ".directory=true\n");
        else
            sb.append(my_prefix + "file" + my_base + ".directory=false\n");
        if(fileSize != null)
            sb.append(my_prefix + "file" + my_base + ".size=" + fileSize.toString() + "\n");
        if(parent != null)
            sb.append(my_prefix + "file" + my_base + ".parent=" + parent + "\n");
        if(hash != null && !hash.equals("noHASH"))
            sb.append(my_prefix + "file" + my_base + ".hash=" + hash + "\n");
        return sb.toString();
    }

    public String getXML(String indent)
    {
        return getXML(indent, true);
    }

    public String getXML(String indent, boolean includeJarContents)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<FILE");
        sb.append(" NAME=\"" + XmlSupport.toCDATA(name) + "\"");
        if(source != null)
            sb.append(" SOURCE=\"" + XmlSupport.toCDATA(source) + "\"");
        if(directory)
            sb.append(" DIRECTORY=\"true\"");
        else
            sb.append(" DIRECTORY=\"false\"");
        if(fileSize != null)
            sb.append(" SIZE=\"" + XmlSupport.toCDATA(fileSize.toString()) + "\"");
        sb.append(" HASH=\"" + XmlSupport.toCDATA(hash) + "\"");
        if(!includeJarContents || archiveDetails == null || archiveDetails.size() == 0)
        {
            sb.append("/>\n");
        } else
        {
            sb.append(">\n");
            int size = archiveDetails.size();
            for(int i = 0; i < size; i++)
            {
                AppFile file = (AppFile)archiveDetails.elementAt(i);
                sb.append(file.getXML(indent_string));
            }

            sb.append(indent_string + "</FILE>\n");
        }
        return sb.toString();
    }

    public int compareTo(Object obj, boolean increasing)
    {
        if(obj == null || !(obj instanceof AppFile))
            throw new IllegalArgumentException("Asked to compare to null object or non-AppFile object");
        AppFile other = (AppFile)obj;
        String left = null;
        String right = null;
        if(getParent() == null)
            left = getName();
        else
            left = getParent() + "%%" + getName();
        if(other.getParent() == null)
            right = other.getName();
        else
            right = other.getParent() + "%%" + other.getName();
        if(increasing)
            return left.compareTo(right);
        else
            return left.compareTo(right) * -1;
    }
}
