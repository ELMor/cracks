// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   EntryPoint.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import org.w3c.dom.Node;

public class EntryPoint
{

    protected String name;
    protected String class_name;
    protected String arguments;

    public EntryPoint()
    {
        name = null;
        class_name = null;
        arguments = null;
    }

    public EntryPoint(Node entry_node)
        throws XmlException
    {
        name = null;
        class_name = null;
        arguments = null;
        name = XmlSupport.getAttribute(entry_node, "NAME");
        if(name == null)
            throw new XmlException("XML Error: no name specified for entry point");
        class_name = XmlSupport.getAttribute(entry_node, "CLASS");
        if(class_name == null)
        {
            throw new XmlException("XML Error: no class name specified for entry point");
        } else
        {
            arguments = XmlSupport.getAttribute(entry_node, "ARGUMENTS");
            return;
        }
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setClassName(String className)
    {
        class_name = className;
    }

    public String getClassName()
    {
        return class_name;
    }

    public String getArguments()
    {
        return arguments;
    }

    public void setArguments(String arg)
    {
        arguments = arg;
    }

    public boolean isValid()
    {
        return getName() != null && !"".equals(((Object) (getName()))) && getClassName() != null && !"".equals(((Object) (getClassName())));
    }

    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("entry.name=" + (name != null ? name : ""));
        sb.append("entry.class=" + (class_name != null ? class_name : ""));
        sb.append("entry.arguments=" + (arguments != null ? arguments : ""));
        return sb.toString();
    }

    public String getPropertiesFile(int base)
    {
        return getPropertiesFile(base, "entry");
    }

    public String getPropertiesFile(int base, String prefix)
    {
        StringBuffer sb = new StringBuffer();
        sb.append(prefix + base + ".name=" + (name != null ? name : "") + "\n");
        sb.append(prefix + base + ".class=" + (class_name != null ? class_name : "") + "\n");
        sb.append(prefix + base + ".arguments=" + (arguments != null ? arguments : "") + "\n");
        return sb.toString();
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<ENTRY");
        if(name != null)
            sb.append(" NAME=\"" + XmlSupport.toCDATA(name) + "\"");
        if(class_name != null)
            sb.append(" CLASS=\"" + XmlSupport.toCDATA(class_name) + "\"");
        if(arguments != null)
            sb.append(" ARGUMENTS=\"" + XmlSupport.toCDATA(arguments) + "\"");
        sb.append("/>\n");
        return sb.toString();
    }
}
