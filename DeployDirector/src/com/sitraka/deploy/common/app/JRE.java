// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JRE.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import java.util.Vector;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.TreeWalker;

// Referenced classes of package com.sitraka.deploy.common.app:
//            ClassPath, EntryPoint

public class JRE
{

    protected String vendor;
    protected String version;
    protected String share_vm;
    protected String searchNativeJREs;
    protected Vector classpath;
    protected Vector entryPoints;

    public JRE()
    {
        vendor = null;
        version = null;
        share_vm = "false";
        searchNativeJREs = "false";
        classpath = new Vector();
        entryPoints = new Vector();
    }

    public JRE(Node jre_node)
        throws XmlException
    {
        vendor = null;
        version = null;
        share_vm = "false";
        searchNativeJREs = "false";
        classpath = new Vector();
        entryPoints = new Vector();
        Node node = null;
        share_vm = XmlSupport.getAttribute(jre_node, "SHAREVM");
        if(share_vm == null)
            share_vm = "false";
        vendor = XmlSupport.getAttribute(jre_node, "VENDOR");
        if(vendor != null && vendor.trim().length() == 0)
            vendor = "";
        version = XmlSupport.getAttribute(jre_node, "VERSION");
        if(version != null && version.trim().length() == 0)
            version = "";
        searchNativeJREs = XmlSupport.getAttribute(jre_node, "SEARCHNATIVEJRES");
        if(searchNativeJREs == null || searchNativeJREs.length() == 0)
            searchNativeJREs = "false";
        TreeWalker treeWalker = XmlSupport.createTreeWalker(jre_node);
        Node currentNode = treeWalker.getRoot();
        for(node = XmlSupport.getNextElementNamed(treeWalker, "CLASSPATH"); node != null; node = XmlSupport.getNextElementNamed(treeWalker, "CLASSPATH"))
            if(node.getParentNode() == jre_node)
            {
                currentNode = node;
                ClassPath p = new ClassPath(node);
                if(classpath == null)
                    classpath = new Vector();
                classpath.addElement(((Object) (p)));
            }

        treeWalker.setCurrentNode(currentNode);
        for(node = XmlSupport.getNextElementNamed(treeWalker, "ENTRY"); node != null; node = XmlSupport.getNextElementNamed(treeWalker, "ENTRY"))
            if(node.getParentNode() == jre_node)
            {
                currentNode = node;
                EntryPoint p = new EntryPoint(node);
                if(entryPoints == null)
                    entryPoints = new Vector();
                entryPoints.addElement(((Object) (p)));
            }

    }

    public void setVendor(String vendor)
    {
        this.vendor = vendor;
    }

    public String getVendor()
    {
        return vendor;
    }

    public Vector getEntryPoints()
    {
        return entryPoints;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getVersion()
    {
        return version;
    }

    public void setShareVm(boolean share)
    {
        if(share)
            share_vm = "true";
        else
            share_vm = "false";
    }

    public void setShareVm(String share)
    {
        if(share.equalsIgnoreCase("true"))
            share_vm = "true";
        else
            share_vm = "false";
    }

    public boolean isShareVm()
    {
        return share_vm.equalsIgnoreCase("true");
    }

    public String getShareVm()
    {
        return share_vm;
    }

    public void setSearchNativeJREs(boolean search)
    {
        if(search)
            searchNativeJREs = "true";
        else
            searchNativeJREs = "false";
    }

    public void setSearchNativeJREs(String search)
    {
        if(search.equalsIgnoreCase("true"))
            searchNativeJREs = "true";
        else
            searchNativeJREs = "false";
    }

    public boolean isSearchNativeJREs()
    {
        return searchNativeJREs.equalsIgnoreCase("true");
    }

    public String getSearchNativeJREs()
    {
        return searchNativeJREs;
    }

    public Vector listEntryPoints()
    {
        return entryPoints;
    }

    public void setEntryPoints(Vector entryPoints)
    {
        this.entryPoints = entryPoints;
    }

    public void addEntryPoint(EntryPoint entrypoint)
    {
        entryPoints.add(((Object) (entrypoint)));
    }

    public String getPropertiesFile()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("# JRE  Vendor and version\n");
        sb.append("jre.vendor=" + (vendor != null ? vendor : "") + "\n");
        sb.append("jre.version=" + (version != null ? version : "") + "\n");
        sb.append("jre.sharevm=" + share_vm + "\n");
        sb.append("jre.searchnativejres=" + searchNativeJREs + "\n");
        return sb.toString();
    }

    public void setClassPaths(Vector classpaths)
    {
        classpath = classpaths;
    }

    public void addClassPath(ClassPath classpath)
    {
        this.classpath.add(((Object) (classpath)));
    }

    public Vector getClassPaths()
    {
        return classpath;
    }

    public int getClassPaths(StringBuffer sb, int base)
    {
        int size = classpath.size();
        for(int i = 0; i < size; i++)
        {
            ClassPath p = (ClassPath)classpath.elementAt(i);
            sb.append(p.getPropertiesFile(base, ((String) (null))) + "\n");
            base++;
        }

        return base;
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<JAVA");
        sb.append(" SHAREVM=\"" + XmlSupport.toCDATA(share_vm) + "\"");
        sb.append(" SEARCHNATIVEJRES=\"" + XmlSupport.toCDATA(searchNativeJREs) + "\"");
        if(vendor != null)
            sb.append(" VENDOR=\"" + XmlSupport.toCDATA(vendor) + "\"");
        else
            sb.append(" VENDOR=\"\"");
        if(version != null)
            sb.append(" VERSION=\"" + XmlSupport.toCDATA(version) + "\"");
        else
            sb.append(" VERSION=\"\"");
        sb.append(" >\n");
        if(classpath != null)
        {
            int size = classpath != null ? classpath.size() : 0;
            for(int i = 0; i < size; i++)
            {
                ClassPath p = (ClassPath)classpath.elementAt(i);
                sb.append(p.getXML(indent_string));
            }

        }
        if(entryPoints != null)
        {
            int size = entryPoints.size();
            for(int i = 0; i < size; i++)
            {
                EntryPoint e = (EntryPoint)entryPoints.elementAt(i);
                sb.append(e.getXML(indent_string));
            }

        }
        sb.append(indent_string + "</JAVA>\n");
        return sb.toString();
    }
}
