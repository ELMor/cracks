/* ProgressBar - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.sitraka.deploy.common.awt;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.StringTokenizer;
import java.util.Vector;

public class ProgressBar extends Canvas
{
    public static final int STRING_LEFT = 0;
    public static final int STRING_RIGHT = 1;
    public static final int STRING_TOP = 2;
    public static final int STRING_BOTTOM = 3;
    public static final int STRING_CENTER = 4;
    public static final int LEFT = 0;
    public static final int RIGHT = 1;
    public static final int TOP = 2;
    public static final int BOTTOM = 3;
    public static final int MIDDLE = 4;
    public static final int NOVALUE = -999;
    private static final String base = "progressbar";
    private static int nameCounter = 0;
    protected int pref_width = -999;
    protected int pref_height = -999;
    private int value = 0;
    private int value_min = 0;
    private int value_max = 100;
    private Color bar_color = Color.red;
    private int bar_count = 10;
    private long starttime;
    private int bar_spacing = 2;
    private int farthest_right_chunk = 0;
    private String label;
    private int label_width_ext;
    private int label_width;
    private int label_position = 1;
    private boolean label_auto = true;
    private boolean label_show = true;
    private double chunk_width;
    protected transient boolean realized = false;
    protected int label_inset = 5;
    protected Rectangle bar_area = new Rectangle();
    protected Rectangle bar_rect = new Rectangle();
    protected int bar_height = 25;
    protected int bar_vert_inset = 3;
    protected int bar_horiz_inset = 2;
    protected int bar_shadow = 1;
    protected Insets insets = new Insets(0, 0, 0, 0);
    protected transient boolean needs_layout = true;
    protected transient Image dblbuffer_image;
    protected transient Graphics dblbuffer_image_gc;

    public ProgressBar() {
        this(0, 0, 0);
    }

    public ProgressBar(int value, int min, int max) {
        insets = new Insets(5, 5, 5, 5);
        if (min == max)
            setValues(value, value_min, value_max);
        else
            setValues(value, min, max);
        setAutoLabel(true);
        this.setName("progressbar" + nameCounter++);
    }

    private final String str(int v) {
        return v < 10 ? "0" + v : "" + v;
    }

    private static int stringWidth(FontMetrics fm, Font font, String s) {
        return (fm.stringWidth(s)
                + (font.isItalic() ? font.getSize() / 3 + 1 : 0));
    }

    private static int getHeight(Object value, Component comp, Font font) {
        if (value == null)
            return 0;
        if (value instanceof Image)
            return ((Image) value).getHeight(null);
        if (value instanceof Vector) {
            Vector v = (Vector) value;
            int height = 0;
            for (int i = 0; i < v.size(); i++)
                height
                    = Math.max(height, getHeight(v.elementAt(i), comp, font));
            return height;
        }
        int lines = 1;
        String val = value.toString();
        for (int i = 0; i < val.length(); i++) {
            if (val.charAt(i) == '\n')
                lines++;
        }
        return comp.getToolkit().getFontMetrics(font).getHeight() * lines;
    }

    private static void drawInBorder(Graphics gc, int size, int x, int y,
                                     int width, int height, Color bright,
                                     Color dark) {
        Color old_color = gc.getColor();
        gc.setColor(dark);
        for (int i = 0; i < size; i++) {
            gc.drawLine(x + i, y + i, x + width - (i + 1), y + i);
            gc.drawLine(x + i, y + i + 1, x + i, y + height - (i + 1));
        }
        gc.setColor(bright);
        for (int i = 1; i <= size; i++) {
            gc.drawLine(x + i - 1, y + height - i, x + width - i,
                        y + height - i);
            gc.drawLine(x + width - i, y + i - 1, x + width - i,
                        y + height - i);
        }
        gc.setColor(old_color);
    }

    private static boolean isTop(int align) {
        return align == 2;
    }

    private static boolean isMiddle(int align) {
        return align == 0 || align == 4 || align == 1;
    }

    private static boolean isBottom(int align) {
        return align == 3;
    }

    private static boolean isLeft(int align) {
        return align == 0;
    }

    private static boolean isCenter(int align) {
        return align == 2 || align == 4 || align == 3;
    }

    private static boolean isRight(int align) {
        return align == 1;
    }

    public static int toHorizAlignment(int align) {
        return isCenter(align) ? 4 : isRight(align) ? 1 : 0;
    }

    public synchronized int getValue() {
        return value;
    }

    public synchronized int getValuePercentage() {
        long denominator = (long) value_max - (long) value_min;
        if (denominator <= 0L)
            return 0;
        long calc = 100L * ((long) value - (long) value_min) / denominator;
        return value_max > value_min ? new Long(calc).intValue() : 0;
    }

    public synchronized void setValue(int v) {
        v = Math.max(value_min, Math.min(v, value_max));
        if (value != v) {
            value = v;
            repaint();
            this.getToolkit().sync();
        }
    }

    public int getMinimum() {
        return value_min;
    }

    public void setMinimum(int v) {
        setValues(value, v, value_max);
    }

    public int getMaximum() {
        return value_max;
    }

    public void setMaximum(int v) {
        setValues(value, value_min, v);
    }

    public synchronized void setValues(int value, int min, int max) {
        setValue(value);
        value_min = min;
        value_max = max;
        starttime = System.currentTimeMillis();
    }

    public Color getBarColor() {
        return bar_color;
    }

    public synchronized void setBarColor(Color v) {
        bar_color = v;
        repaint();
    }

    public int getBarCount() {
        return bar_count;
    }

    public synchronized void setBarCount(int v) {
        bar_count = v;
        doLayout();
        repaint();
    }

    public int getBarSpacing() {
        return bar_spacing;
    }

    public synchronized void setBarSpacing(int v) {
        bar_spacing = v;
        doLayout();
        repaint();
    }

    public String getLabel() {
        return label;
    }

    public synchronized void setLabel(String s) {
        label = s;
        repaint();
    }

    public boolean getAutoLabel() {
        return label_auto;
    }

    public synchronized void setAutoLabel(boolean v) {
        label_auto = v;
        if (label_width_ext == 0)
            label_width = v ? 4 : 10;
        repaint();
    }

    public boolean getShowLabel() {
        return label_show;
    }

    public synchronized void setShowLabel(boolean v) {
        label_show = v;
        repaint();
    }

    public int getLabelPosition() {
        return label_position;
    }

    public synchronized void setLabelPosition(int v) {
        label_position = v;
        doLayout();
        repaint();
    }

    public int getLabelWidth() {
        return label_width_ext;
    }

    public synchronized void setLabelWidth(int v) {
        label_width_ext = label_width = v;
        doLayout();
        repaint();
    }

    public long getTimeElapsed() {
        return System.currentTimeMillis() - starttime;
    }

    public long getTimeToCompletion() {
        if (value == value_min)
            return 2147483647L;
        long time = getTimeElapsed();
        return (long) (((double) (value_max - value_min)
                        / (double) (value - value_min) * (double) time)
                       - (double) time);
    }

    public String getTimeToCompletionString() {
        if (value == value_min)
            return "?";
        int seconds = (int) getTimeToCompletion() / 1000;
        int hours = seconds / 3600;
        seconds -= hours * 3600;
        int minutes = seconds / 60;
        seconds -= minutes * 60;
        return str(hours) + ":" + str(minutes) + ":" + str(seconds);
    }

    protected int preferredWidth() {
        int width = 0;
        if (label_show) {
            Font f = this.getFont();
            FontMetrics fm = this.getToolkit().getFontMetrics(f);
            if (label != null && label_width_ext != label_width) {
                StringTokenizer toker = new StringTokenizer(label, "\n");
                int longest = 0;
                while (toker.hasMoreTokens()) {
                    String sample = toker.nextToken();
                    if (sample != null) {
                        int length = stringWidth(fm, f, sample);
                        if (length > longest)
                            longest = length;
                    }
                }
                width = longest;
            } else
                width = label_width * this.getToolkit().getFontMetrics
                                          (this.getFont()).charWidth('N');
            if (label_position == 0 || label_position == 1)
                width *= 2;
        }
        return width + label_inset + 2 * bar_shadow + 2 * bar_horiz_inset;
    }

    protected int preferredHeight() {
        int height = 0;
        if (label_show) {
            if (label == null)
                height = getHeight("N", this, this.getFont());
            else
                height = getHeight(label, this, this.getFont());
            if (label_position == 2 || label_position == 3)
                height += bar_height;
        }
        return height + label_inset + 2 * bar_vert_inset + 2 * bar_shadow;
    }

    public Dimension getPreferredSize() {
        int w = pref_width != -999 ? pref_width : preferredWidth();
        w = w < 0 ? 50 : w + insets.left + insets.right;
        int h = pref_height != -999 ? pref_height : preferredHeight();
        h = h < 0 ? 50 : h + insets.top + insets.bottom;
        return new Dimension(w, h);
    }

    public Dimension getMinimumSize() {
        return getPreferredSize();
    }

    public void doLayout() {
        if (isShowing()) {
            needs_layout = false;
            bar_area = getDrawingArea(new Rectangle());
            if (label_show) {
                if (label_position == 2 || label_position == 3) {
                    int current_label_height = preferredHeight() - bar_height;
                    if (label_position == 2)
                        bar_area.y += current_label_height;
                    bar_area.height -= current_label_height;
                } else if (label_position == 0 || label_position == 1) {
                    int width = preferredWidth();
                    if (label_position == 0)
                        bar_area.x += width / 2;
                    bar_area.width -= width / 2;
                }
            }
            chunk_width = 0.0;
            if (bar_count > 0 && bar_spacing > 0) {
                int h_margin = bar_horiz_inset + bar_shadow;
                chunk_width = ((double) (bar_area.width - 2 * h_margin
                                         - (bar_count - 1) * bar_spacing)
                               / (double) bar_count);
                if (chunk_width < 1.0)
                    chunk_width = 0.0;
            }
        }
    }

    public void setBounds(int x, int y, int width, int height) {
        synchronized (this) {
            boolean resized = (this.getSize().width != width
                               || this.getSize().height != height);
            if (!resized && this.getLocation().x == x
                && this.getLocation().y == y)
                return;
        }
        super.setBounds(x, y, width, height);
    }

    protected static synchronized Image createImage(Component comp, int width,
                                                    int height) {
        width = Math.max(1, Math.min(width,
                                     comp.getToolkit().getScreenSize().width));
        height
            = Math.max(1, Math.min(height,
                                   comp.getToolkit().getScreenSize().height));
        return comp.createImage(width, height);
    }

    protected void drawShadow(Graphics gc) {
        drawInBorder(gc, bar_shadow, bar_area.x, bar_area.y, bar_area.width,
                     bar_area.height, this.getBackground().brighter(),
                     this.getBackground().darker());
    }

    protected void drawBar(Graphics gc) {
        int h_margin = bar_horiz_inset + bar_shadow;
        int v_margin = bar_vert_inset + bar_shadow;
        bar_rect.setBounds(bar_area.x + h_margin, bar_area.y + v_margin,
                           bar_area.width - 2 * h_margin,
                           bar_area.height - 2 * v_margin);
        int bar_width = bar_rect.width;
        bar_rect.width *= (double) getValuePercentage() / 100.0;
        if (chunk_width == 0.0)
            gc.fillRect(bar_rect.x, bar_rect.y, bar_rect.width,
                        bar_rect.height);
        else {
            int remaining = 0;
            int num_chunks = 0;
            int x = bar_rect.x;
            int[] chunk_x = new int[bar_count];
            int[] chunk_w = new int[bar_count];
            synchronized (this) {
                int i = 0;
                while (i < bar_count) {
                    chunk_x[i] = x;
                    chunk_w[i] = (int) chunk_width;
                    if (i == bar_count - 1)
                        remaining
                            = bar_rect.x + bar_width - (x + (int) chunk_width);
                    if (chunk_x[i] + chunk_w[i] - bar_rect.x <= bar_rect.width)
                        num_chunks++;
                    i++;
                    x += (int) chunk_width + bar_spacing;
                }
            }
            if (remaining > 0) {
                for (int i = 0; i < num_chunks; i++) {
                    if (i < remaining) {
                        chunk_x[i] += i;
                        chunk_w[i]++;
                    } else
                        chunk_x[i] += remaining;
                }
            }
            for (int i = 0; i < num_chunks; i++)
                gc.fillRect(chunk_x[i], bar_rect.y, chunk_w[i],
                            bar_rect.height);
            farthest_right_chunk
                = chunk_x[num_chunks - 1] + chunk_w[num_chunks - 1];
        }
    }

    protected void drawLabel(Graphics gc, String label) {
        Rectangle rect = getDrawingArea(new Rectangle());
        int align = 4;
        switch (label_position) {
        case 0:
            align = 1;
            rect.width = bar_area.x - label_inset - rect.x;
            break;
        case 1:
            align = 0;
            rect.x = bar_area.x + bar_area.width + label_inset;
            break;
        case 2:
            align = 4;
            rect.height = bar_area.y - label_inset - rect.y;
            break;
        case 3:
            align = 4;
            rect.y = bar_area.y + bar_area.height / 2 + label_inset;
            break;
        }
        draw(this, gc, label, align, rect);
        if (label_position == 4) {
            if (farthest_right_chunk != 0)
                bar_rect.width = farthest_right_chunk - bar_rect.x;
            gc = gc.create();
            gc.clipRect(bar_rect.x, bar_area.y, bar_rect.width,
                        bar_area.height);
            gc.setColor(this.getBackground());
            draw(this, gc, label, align, rect);
            farthest_right_chunk = 0;
        }
    }

    public static synchronized void draw(Component comp, Graphics gc,
                                         Object value, int alignment,
                                         Rectangle draw_rect) {
        if (value != null) {
            int align = toHorizAlignment(alignment);
            if (value instanceof Image)
                gc.drawImage((Image) value, draw_rect.x, draw_rect.y, null);
            else {
                String string = value.toString();
                if (string != null && string.length() != 0) {
                    FontMetrics fm = gc.getFontMetrics();
                    Font font = gc.getFont();
                    int height = fm.getHeight();
                    int offset = 0;
                    int line_space = height - fm.getAscent();
                    int y = draw_rect.y + height - line_space;
                    int str_height
                        = isTop(alignment) ? 0 : getHeight(string, comp, font);
                    if (isBottom(alignment))
                        y += draw_rect.height - str_height;
                    else if (isMiddle(alignment))
                        y += (draw_rect.height - str_height) / 2;
                    if (string.indexOf('\n') != -1) {
                        int start = 0;
                        int x = draw_rect.x;
                        int end;
                        while ((end = string.indexOf('\n', start)) != -1) {
                            String s = string.substring(start, end);
                            if (align == 4)
                                offset = (draw_rect.width
                                          - stringWidth(fm, font, s)) / 2;
                            else if (align == 1)
                                offset
                                    = draw_rect.width - stringWidth(fm, font,
                                                                    s);
                            gc.drawString(s, draw_rect.x + offset, y);
                            start = end + 1;
                            y += height;
                        }
                        String s = string.substring(start, string.length());
                        if (align == 4)
                            offset = (draw_rect.width
                                      - stringWidth(fm, font, s)) / 2;
                        else if (align == 1)
                            offset
                                = draw_rect.width - stringWidth(fm, font, s);
                        gc.drawString(s, draw_rect.x + offset, y);
                    } else {
                        if (align == 4)
                            offset = (draw_rect.width
                                      - stringWidth(fm, font, string)) / 2;
                        else if (align == 1)
                            offset = draw_rect.width - stringWidth(fm, font,
                                                                   string);
                        gc.drawString(string, draw_rect.x + offset, y);
                    }
                }
            }
        }
    }

    public void paint(Graphics gc) {
        synchronized (this.getTreeLock()) {
            Rectangle rect = new Rectangle();
            Graphics draw_gc = gc;
            Image old_image = dblbuffer_image;
            if (gc == null || this.getBackground() == null) {
                /* empty */
            } else {
                Rectangle paint_rect = gc.getClipBounds();
                dblbuffer_image = createImage(this, this.getSize().width,
                                              this.getSize().height);
                if (dblbuffer_image == null)
                    dblbuffer_image_gc = null;
                else if (dblbuffer_image != old_image)
                    dblbuffer_image_gc = dblbuffer_image.getGraphics();
                if (gc == null)
                    gc = draw_gc;
                else
                    gc = dblbuffer_image_gc;
                if (paint_rect != null)
                    gc.setClip(paint_rect);
                if (paint_rect == null) {
                    paint_rect = new Rectangle(this.getSize());
                    gc.setClip(paint_rect);
                }
                rect.setBounds(0, 0, this.getSize().width,
                               this.getSize().height);
                gc.setColor(this.getBackground());
                gc.fillRect(0, 0, this.getSize().width, this.getSize().height);
                gc.setFont(this.getFont());
                gc.setColor(this.getForeground());
                getDrawingArea(rect);
                gc.clipRect(rect.x, rect.y, rect.width, rect.height);
                old_image = dblbuffer_image;
                if (needs_layout)
                    doLayout();
                drawShadow(gc);
                gc.setColor(bar_color);
                drawBar(gc);
                if (label_show) {
                    String templabel = label;
                    if (templabel == null && label_auto)
                        templabel = "" + getValuePercentage() + "%";
                    gc.setColor(this.getForeground());
                    drawLabel(gc, templabel);
                }
                if (dblbuffer_image != null)
                    draw_gc.drawImage(dblbuffer_image, 0, 0, null);
                dblbuffer_image = old_image;
                draw_gc = null;
                Object object = null;
            }
        }
    }

    protected Rectangle getDrawingArea(Rectangle rect) {
        rect.setBounds(insets.left, insets.top,
                       Math.max(0, this.getSize().width - (insets.left
                                                           + insets.right)),
                       Math.max(0, this.getSize().height - (insets.top
                                                            + insets.bottom)));
        return rect;
    }

    public boolean isShowing() {
        boolean javaOS = System.getProperty("os.name").indexOf("JavaOS") > -1;
        return javaOS ? true : super.isShowing();
    }

    public void repaint() {
        this.repaint(0, 0, this.getSize().width, this.getSize().height);
    }

    public void update(Graphics g) {
        paint(g);
    }

    public void updateParent() {
        if (this.getParent() != null) {
            this.invalidate();
            this.getParent().invalidate();
            this.getParent().validate();
        }
    }

    public void validate() {
        if (!this.isValid() && isRealized()) {
            super.validate();
            doLayout();
        }
    }

    public boolean isRealized() {
        return realized;
    }

    public void addNotify() {
        if (!realized) {
            super.addNotify();
            realized = true;
        }
        this.enableEvents(28L);
    }

    public void removeNotify() {
        super.removeNotify();
        realized = false;
    }
}
