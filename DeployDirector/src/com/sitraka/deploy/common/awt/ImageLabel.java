// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ImageLabel.java

package com.sitraka.deploy.common.awt;

import java.awt.Canvas;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;

public class ImageLabel extends Canvas
{

    protected Image image;
    protected int width;
    protected int height;

    public ImageLabel(Image image)
    {
        width = -1;
        height = -1;
        setImage(image);
    }

    protected void setImage(Image image)
    {
        if(image == null)
            return;
        MediaTracker tracker = new MediaTracker(((Component) (this)));
        tracker.addImage(image, 0);
        try
        {
            tracker.waitForID(0);
        }
        catch(InterruptedException e)
        {
            this.image = null;
            width = height = -1;
            return;
        }
        ((Component)this).invalidate();
        this.image = image;
        width = height = -1;
        changeSize(image.getWidth(((java.awt.image.ImageObserver) (this))), image.getHeight(((java.awt.image.ImageObserver) (this))));
        ((Component)this).repaint();
    }

    protected void changeSize(int w, int h)
    {
        if(w == -1 || h == -1)
        {
            return;
        } else
        {
            width = w;
            height = h;
            ((Component)this).setSize(width, height);
            return;
        }
    }

    public void paint(Graphics graphics)
    {
        if(image != null)
            graphics.drawImage(image, 0, 0, ((java.awt.image.ImageObserver) (this)));
    }

    public Dimension getPreferredSize()
    {
        return getMinimumSize();
    }

    public Dimension getMinimumSize()
    {
        return new Dimension(width, height);
    }
}
