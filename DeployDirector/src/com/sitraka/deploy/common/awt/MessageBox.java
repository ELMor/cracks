// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   MessageBox.java

package com.sitraka.deploy.common.awt;

import com.sitraka.deploy.common.awt.resources.LocaleInfo;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.EventObject;
import java.util.ResourceBundle;

// Referenced classes of package com.sitraka.deploy.common.awt:
//            ImageLabel, MultiLineLabel, Brace, Box

public class MessageBox extends Dialog
    implements ActionListener, KeyListener
{

    protected static Frame frame = new Frame();
    protected boolean modal;
    public static final String ERROR = "DDCAError";
    public static final String MESSAGE = "DDCAMessage";
    public static final String NO = "DDCANo";
    public static final String QUESTION = "DDCAQuestion";
    public static final String WARNING = "DDCAWarning";
    public static final String YES = "DDCAYes";
    protected static final String MESSAGE_ICON_NAME = "/com/sitraka/deploy/common/images/message.gif";
    protected static final String QUESTION_ICON_NAME = "/com/sitraka/deploy/common/images/question.gif";
    protected static final String ERROR_ICON_NAME = "/com/sitraka/deploy/common/images/error.gif";
    protected static final String WARNING_ICON_NAME = "/com/sitraka/deploy/common/images/warning.gif";
    protected static final int TYPE_MESSAGE = 1;
    protected static final int TYPE_QUESTION = 2;
    protected static final int TYPE_ERROR = 3;
    protected static final int TYPE_WARNING = 4;
    public static final int ANSWER_YES = 1;
    public static final int ANSWER_NO = 2;
    public static final int ANSWER_CANCEL = 3;
    protected int answer;
    protected boolean showCancel;

    protected MessageBox(String title, String message, int type, boolean show_cancel)
    {
        super(frame, title);
        modal = false;
        answer = 1;
        showCancel = true;
        showCancel = show_cancel;
        switch(type)
        {
        case 2: // '\002'
            buildQuestionBox(message);
            break;

        case 3: // '\003'
            buildGenericBox(message, "/com/sitraka/deploy/common/images/error.gif");
            break;

        case 4: // '\004'
            buildGenericBox(message, "/com/sitraka/deploy/common/images/warning.gif");
            break;

        case 1: // '\001'
        default:
            buildGenericBox(message, "/com/sitraka/deploy/common/images/message.gif");
            break;
        }
    }

    public static void showMessage(String title, String message, boolean modal)
    {
        MessageBox box = new MessageBox(title, message, 1, true);
        box.display(modal);
    }

    public static void showMessage(String message)
    {
        showMessage(LocaleInfo.li.getString("DDCAMessage"), message, false);
    }

    public static void showMessage(String title, String message)
    {
        showMessage(title, message, false);
    }

    public static void showBlockingNonModalMessage(String title, String message)
    {
        MessageBox box = new MessageBox(title, message, 1, false);
        box.display();
        box.getAnswer();
    }

    public static void showWarning(String title, String message, boolean modal)
    {
        MessageBox box = new MessageBox(title, message, 4, true);
        box.display(modal);
    }

    public static void showWarning(String message)
    {
        showWarning(LocaleInfo.li.getString("DDCAWarning"), message, true);
    }

    public static void showWarning(String title, String message)
    {
        showWarning(title, message, true);
    }

    public static void showError(String title, String message, boolean modal)
    {
        MessageBox box = new MessageBox(title, message, 3, true);
        box.display(modal);
    }

    public static void showError(String message)
    {
        showError(LocaleInfo.li.getString("DDCAError"), message, true);
    }

    public static void showError(String title, String message)
    {
        showError(title, message, true);
    }

    public static int showQuestion(String title, String message, boolean modal, boolean show_cancel)
    {
        MessageBox box = new MessageBox(title, message, 2, show_cancel);
        box.display(modal);
        return box.getAnswer();
    }

    public static int showQuestion(String title, String message, boolean modal)
    {
        return showQuestion(title, message, modal, true);
    }

    public static int showQuestion(String message)
    {
        return showQuestion(LocaleInfo.li.getString("DDCAQuestion"), message, true);
    }

    public static int showQuestion(String title, String message)
    {
        return showQuestion(title, message, true);
    }

    public synchronized int getAnswer()
    {
        if(!modal)
            try
            {
                ((Object)this).wait();
            }
            catch(InterruptedException e) { }
        return answer;
    }

    public void toFront()
    {
    }

    protected void display(boolean b)
    {
        ((Window)this).pack();
        ((Container)this).validate();
        modal = b;
        centerWindow();
        ((Dialog)this).setModal(modal);
        ((Component)this).setVisible(true);
    }

    protected void display()
    {
        display(false);
    }

    protected void buildGenericBox(String message, String icon_name)
    {
        ((Component)this).setBackground(Color.lightGray);
        ((Dialog)this).setResizable(false);
        GridBagLayout gridbag = new GridBagLayout();
        ((Container)this).setLayout(((java.awt.LayoutManager) (gridbag)));
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(5, 5, 5, 5);
        c.fill = 1;
        c.weightx = 1.0D;
        Box box = Box.createVerticalBox();
        java.net.URL url = ((Object)this).getClass().getResource(icon_name);
        if(url != null)
        {
            java.awt.Image image = ((Window)this).getToolkit().getImage(url);
            ImageLabel img_label = new ImageLabel(image);
            ((Container) (box)).add(((Component) (img_label)));
        }
        gridbag.setConstraints(((Component) (box)), c);
        ((Container)this).add(((Component) (box)));
        MultiLineLabel mll = new MultiLineLabel(message);
        c.gridwidth = 0;
        gridbag.setConstraints(((Component) (mll)), c);
        ((Container)this).add(((Component) (mll)));
        Button button = new Button(LocaleInfo.li.getString("DDCAOk"));
        button.setActionCommand("YES");
        Dimension d = ((Component) (button)).getSize();
        d.width = d.width + d.width / 2;
        ((Component) (button)).setSize(d);
        c.insets = new Insets(5, 25, 5, 25);
        c.gridwidth = 2;
        gridbag.setConstraints(((Component) (button)), c);
        button.addActionListener(((ActionListener) (this)));
        ((Container)this).add(((Component) (button)));
    }

    protected void buildQuestionBox(String message)
    {
        ((Component)this).setBackground(Color.lightGray);
        ((Dialog)this).setResizable(false);
        GridBagLayout gridbag = new GridBagLayout();
        ((Container)this).setLayout(((java.awt.LayoutManager) (gridbag)));
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(5, 5, 5, 5);
        c.fill = 1;
        c.weightx = 1.0D;
        Box box = Box.createVerticalBox();
        java.net.URL url = ((Object)this).getClass().getResource("/com/sitraka/deploy/common/images/question.gif");
        if(url != null)
        {
            java.awt.Image image = ((Window)this).getToolkit().getImage(url);
            ImageLabel img_label = new ImageLabel(image);
            ((Container) (box)).add(((Component) (img_label)));
        }
        gridbag.setConstraints(((Component) (box)), c);
        ((Container)this).add(((Component) (box)));
        MultiLineLabel mll = new MultiLineLabel(message);
        c.gridwidth = 0;
        gridbag.setConstraints(((Component) (mll)), c);
        ((Container)this).add(((Component) (mll)));
        Box h_box = Box.createHorizontalBox();
        Button button = new Button(LocaleInfo.li.getString("DDCAYes"));
        button.setActionCommand("YES");
        button.addActionListener(((ActionListener) (this)));
        ((Container) (h_box)).add(((Component) (button)));
        Brace brace = new Brace(15, 0);
        ((Container) (h_box)).add(((Component) (brace)));
        button = new Button(LocaleInfo.li.getString("DDCANo"));
        button.setActionCommand("NO");
        button.addActionListener(((ActionListener) (this)));
        ((Container) (h_box)).add(((Component) (button)));
        brace = new Brace(15, 0);
        ((Container) (h_box)).add(((Component) (brace)));
        if(showCancel)
        {
            button = new Button(LocaleInfo.li.getString("DDCACancel"));
            button.setActionCommand("CANCEL");
            button.addActionListener(((ActionListener) (this)));
            ((Container) (h_box)).add(((Component) (button)));
        }
        c.insets = new Insets(5, 20, 5, 20);
        c.gridwidth = 2;
        gridbag.setConstraints(((Component) (h_box)), c);
        ((Container)this).add(((Component) (h_box)));
    }

    protected void centerWindow()
    {
        Dimension d = ((Component)this).getSize();
        Dimension size = ((Window)this).getToolkit().getScreenSize();
        int left = size.width / 2 - d.width / 2;
        int top = size.height / 2 - d.height / 2;
        ((Component)this).setLocation(left, top);
    }

    public void keyPressed(KeyEvent keyevent)
    {
    }

    public void keyReleased(KeyEvent e)
    {
        String command = ((Button)((EventObject) (e)).getSource()).getLabel();
        doDialogAction(command);
    }

    public void keyTyped(KeyEvent keyevent)
    {
    }

    public synchronized void actionPerformed(ActionEvent e)
    {
        String command = e.getActionCommand();
        doDialogAction(command);
    }

    protected void doDialogAction(String command)
    {
        ((Component)this).setVisible(false);
        if(command.equalsIgnoreCase("YES"))
            answer = 1;
        else
        if(command.equalsIgnoreCase("NO"))
            answer = 2;
        else
        if(command.equalsIgnoreCase("CANCEL"))
            answer = 3;
        else
            answer = 1;
        if(!modal)
            ((Object)this).notify();
    }

}
