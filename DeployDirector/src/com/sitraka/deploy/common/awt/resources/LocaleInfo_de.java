// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LocaleInfo_de.java

package com.sitraka.deploy.common.awt.resources;

import java.util.ListResourceBundle;

public class LocaleInfo_de extends ListResourceBundle
{

    public static final String CANCEL = "DDCACancel";
    public static final String OK = "DDCAOk";
    static final Object strings[][] = {
        {
            "DDCACancel", "Abbrechen"
        }, {
            "DDCAOk", "OK"
        }, {
            "DDTimeoutError", "Unterbrechung"
        }, {
            "DDTimeoutMessage", "Konnte zu keinem Server verbinden. Folgende Server versucht: {0}"
        }, {
            "DDCADone", "Schlie\337en"
        }, {
            "DDCAWait", "Bitte warten ..."
        }, {
            "DDCAInstallTo", "Installieren nach:"
        }, {
            "DDCACurrDir", "Selektiertes Verzeichnis:"
        }, {
            "DDCAInstallDir", "Installationsverzeichnis: {0}"
        }, {
            "DDCAError", "Fehler"
        }, {
            "DDCAMessage", "Meldung"
        }, {
            "DDCANo", "Nein"
        }, {
            "DDCAQuestion", "Frage"
        }, {
            "DDCAWarning", "Warnung"
        }, {
            "DDCAYes", "Ja"
        }, {
            "Finish", "Abschlie\337en"
        }, {
            "Help", "Hilfe"
        }, {
            "Next", "Weiter"
        }, {
            "Previous", "Zur\374ck"
        }
    };

    public LocaleInfo_de()
    {
    }

    public Object[][] getContents()
    {
        return strings;
    }

}
