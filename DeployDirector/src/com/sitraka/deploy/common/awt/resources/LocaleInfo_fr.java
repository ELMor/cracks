// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LocaleInfo_fr.java

package com.sitraka.deploy.common.awt.resources;

import java.util.ListResourceBundle;

public class LocaleInfo_fr extends ListResourceBundle
{

    public static final String CANCEL = "DDCACancel";
    public static final String OK = "DDCAOk";
    static final Object strings[][] = {
        {
            "DDCACancel", "Annuler"
        }, {
            "DDCAOk", "OK"
        }, {
            "DDTimeoutError", "Expiration du d\351lai"
        }, {
            "DDTimeoutMessage", "Echec de la connexion avec le(s) serveur(s): {0}"
        }, {
            "DDCADone", "Termin\351"
        }, {
            "DDCAWait", "Veuillez patienter..."
        }, {
            "DDCAInstallTo", "Installer Vers:"
        }, {
            "DDCACurrDir", "R\351pertoire actuellement s\351lectionn\351:"
        }, {
            "DDCAInstallDir", "R\351pertoire d''installation: {0}"
        }, {
            "DDCAError", "Erreur"
        }, {
            "DDCAMessage", "Message"
        }, {
            "DDCANo", "Non"
        }, {
            "DDCAQuestion", "Question"
        }, {
            "DDCAWarning", "Avertissement"
        }, {
            "DDCAYes", "Oui"
        }, {
            "Finish", "Terminer"
        }, {
            "Help", "Aide"
        }, {
            "Next", "Suivant"
        }, {
            "Previous", "Pr\351c\351dent"
        }
    };

    public LocaleInfo_fr()
    {
    }

    public Object[][] getContents()
    {
        return strings;
    }

}
