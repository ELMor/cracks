// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ElasticLayout.java

package com.sitraka.deploy.common.awt;

import com.sitraka.deploy.common.jclass.util.swing.JCElastic;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.io.Serializable;
import java.util.Hashtable;

public class ElasticLayout
    implements LayoutManager2, Serializable
{
    static class NullConstraint
    {

        public NullConstraint()
        {
        }
    }

    static class Elastic
        implements JCElastic, Serializable
    {

        protected int horizontal;
        protected int vertical;

        public int getHorizontalElasticity()
        {
            return horizontal;
        }

        public int getVerticalElasticity()
        {
            return vertical;
        }

        public Elastic(int horizontal_elasticity, int vertical_elasticity)
        {
            horizontal = horizontal_elasticity;
            vertical = vertical_elasticity;
        }
    }


    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;
    public static final JCElastic HORIZONTALLY_ELASTIC_CONSTRAINT = new Elastic(1, 0);
    public static final JCElastic VERTICALLY_ELASTIC_CONSTRAINT = new Elastic(0, 1);
    public static final JCElastic COMPLETELY_ELASTIC_CONSTRAINT = new Elastic(1, 1);
    public static final String HORIZONTALLY_ELASTIC_TAG = "HorizontallyElastic";
    public static final String VERTICALLY_ELASTIC_TAG = "VerticallyElastic";
    public static final String COMPLETELY_ELASTIC_TAG = "CompletelyElastic";
    public static final int CENTER = 0;
    public static final int TOP = 1;
    public static final int LEFT = 2;
    public static final int BOTTOM = 3;
    public static final int RIGHT = 4;
    protected static final int NOT_ELASTIC = 0;
    protected static final int VERTICALLY_ELASTIC = 1;
    protected static final int HORIZONTALLY_ELASTIC = 2;
    protected int alignment;
    protected int orientation;
    protected Hashtable constraintMap;
    protected static Hashtable constraintCache = null;

    public static JCElastic createElasticConstraint(int horizontal_elasticity, int vertical_elasticity)
    {
        if(constraintCache == null)
            constraintCache = new Hashtable();
        Elastic e = new Elastic(horizontal_elasticity, vertical_elasticity);
        if(constraintCache.containsKey(((Object) (e))))
        {
            return (JCElastic)constraintCache.get(((Object) (e)));
        } else
        {
            constraintCache.put(((Object) (e)), ((Object) (e)));
            return ((JCElastic) (e));
        }
    }

    public ElasticLayout(int orientation)
    {
        this(orientation, orientation != 1 ? 1 : 2);
    }

    public ElasticLayout(int orientation, int alignment)
    {
        constraintMap = new Hashtable();
        this.orientation = orientation;
        if(alignment != 2 && alignment != 1 && alignment != 0 && alignment != 3 && alignment != 4)
        {
            throw new IllegalArgumentException("Incorrect value; use one of: ElasticLayout.LEFT, ElasticLayout.CENTER, ElasticLayout.RIGHT");
        } else
        {
            this.alignment = alignment;
            return;
        }
    }

    public void addLayoutComponent(String name, Component comp)
    {
        if(name.equals("HorizontallyElastic"))
            addLayoutComponent(comp, ((Object) (HORIZONTALLY_ELASTIC_CONSTRAINT)));
        else
        if(name.equals("VerticallyElastic"))
            addLayoutComponent(comp, ((Object) (VERTICALLY_ELASTIC_CONSTRAINT)));
        else
        if(name.equals("CompletelyElastic"))
            addLayoutComponent(comp, ((Object) (COMPLETELY_ELASTIC_CONSTRAINT)));
    }

    public void removeLayoutComponent(Component comp)
    {
        if(constraintMap.containsKey(((Object) (comp))))
            constraintMap.remove(((Object) (comp)));
    }

    public Dimension preferredLayoutSize(Container parent)
    {
        Dimension size = new Dimension();
        getPreferredLayoutSize(parent, size);
        return size;
    }

    protected int getPreferredLayoutSize(Container parent, Dimension preferred_size)
    {
        int n = parent.getComponentCount();
        int width = 0;
        int height = 0;
        int elasticity = 0;
        int elasticity_total = 0;
        if(orientation == 1)
        {
            for(int i = 0; i < n; i++)
            {
                Component c = parent.getComponent(i);
                if(c.isVisible())
                {
                    Dimension size = c.getPreferredSize();
                    width = Math.max(width, size.width);
                    height += size.height;
                    elasticity = getElasticity(c);
                    if(elasticity != 0)
                        elasticity_total += elasticity;
                }
            }

        } else
        {
            for(int i = 0; i < n; i++)
            {
                Component c = parent.getComponent(i);
                if(c.isVisible())
                {
                    Dimension size = c.getPreferredSize();
                    height = Math.max(height, size.height);
                    width += size.width;
                    elasticity = getElasticity(c);
                    if(elasticity != 0)
                        elasticity_total += elasticity;
                }
            }

        }
        Insets insets = parent.getInsets();
        width = width + insets.right + insets.left;
        height = height + insets.top + insets.bottom;
        if(preferred_size != null)
        {
            preferred_size.width = width;
            preferred_size.height = height;
        }
        return elasticity_total;
    }

    public Dimension minimumLayoutSize(Container parent)
    {
        int n = parent.getComponentCount();
        int width = 0;
        int height = 0;
        if(orientation == 1)
        {
            for(int i = 0; i < n; i++)
            {
                Component c = parent.getComponent(i);
                if(c.isVisible())
                {
                    Dimension size = c.getMinimumSize();
                    width = Math.max(width, size.width);
                    height += size.height;
                }
            }

        } else
        {
            for(int i = 0; i < n; i++)
            {
                Component c = parent.getComponent(i);
                if(c.isVisible())
                {
                    Dimension size = c.getMinimumSize();
                    height = Math.max(height, size.height);
                    width += size.width;
                }
            }

        }
        return new Dimension(width, height);
    }

    public void layoutContainer(Container parent)
    {
        Dimension preferred_size = new Dimension();
        int elasticity_total = getPreferredLayoutSize(parent, preferred_size);
        Dimension parent_size = ((Component) (parent)).getSize();
        Insets insets = parent.getInsets();
        parent_size.height -= insets.bottom;
        parent_size.width -= insets.right;
        preferred_size.height -= insets.bottom;
        preferred_size.width -= insets.right;
        int coord1_inset;
        int coord2_inset;
        int extra_space;
        if(orientation == 1)
        {
            extra_space = parent_size.height - preferred_size.height;
            coord1_inset = insets.left;
            coord2_inset = insets.top;
        } else
        {
            extra_space = parent_size.width - preferred_size.width;
            coord1_inset = insets.top;
            coord2_inset = insets.left;
        }
        int slice = 0;
        int fatter_slice_count = 0;
        if(elasticity_total > 0)
        {
            slice = extra_space / elasticity_total;
            fatter_slice_count = extra_space % elasticity_total;
        }
        int n = parent.getComponentCount();
        int coord2 = coord2_inset;
        int current_slice = 0;
        for(int i = 0; i < n; i++)
        {
            Component c = parent.getComponent(i);
            if(c.isVisible())
            {
                int dim1 = 0;
                int size_dim2;
                int max_dim1;
                int parent_dim1;
                if(orientation == 1)
                {
                    parent_dim1 = parent_size.width - coord1_inset;
                    int parent_dim2 = parent_size.height - coord2_inset;
                    max_dim1 = c.getMaximumSize().width;
                    if(max_dim1 < 0)
                        max_dim1 = c.getPreferredSize().width;
                    size_dim2 = c.getPreferredSize().height;
                } else
                {
                    parent_dim1 = parent_size.height - coord1_inset;
                    int parent_dim2 = parent_size.width - coord2_inset;
                    max_dim1 = c.getMaximumSize().height;
                    if(max_dim1 < 0)
                        max_dim1 = c.getPreferredSize().height;
                    size_dim2 = c.getPreferredSize().width;
                }
                dim1 = Math.min(parent_dim1, max_dim1);
                int coord1 = coord1_inset;
                if(dim1 != parent_dim1)
                    if(isDim1Elastic(c))
                    {
                        coord1 = 0;
                        dim1 = parent_dim1;
                    } else
                    if(alignment == 2 || alignment == 1)
                        coord1 = 0;
                    else
                    if(alignment == 0)
                        coord1 = (parent_dim1 - dim1) / 2;
                    else
                    if(alignment == 4 || alignment == 3)
                        coord1 = parent_dim1 - dim1;
                int elasticity = getElasticity(c);
                if(elasticity > 0)
                {
                    int stretch = calcStretch(slice, elasticity, current_slice, fatter_slice_count);
                    if(orientation == 1)
                        c.setBounds(coord1, coord2, dim1, size_dim2 + stretch);
                    else
                        c.setBounds(coord2, coord1, size_dim2 + stretch, dim1);
                    current_slice += elasticity;
                    coord2 += size_dim2 + stretch;
                } else
                {
                    if(orientation == 1)
                        c.setBounds(coord1, coord2, dim1, size_dim2);
                    else
                        c.setBounds(coord2, coord1, size_dim2, dim1);
                    coord2 += size_dim2;
                }
            }
        }

    }

    protected int calcStretch(int slice, int elasticity, int current_slice, int fatter_slice_count)
    {
        int stretch = 0;
        for(int i = current_slice; i < current_slice + elasticity; i++)
            stretch += slice + (i >= fatter_slice_count ? 0 : 1);

        return stretch;
    }

    protected boolean isDim1Elastic(Component c)
    {
        if(orientation == 1)
            return isHorizontallyElastic(c);
        else
            return isVerticallyElastic(c);
    }

    protected JCElastic getElastic(Component c)
    {
        JCElastic e = null;
        if(c instanceof JCElastic)
        {
            e = (JCElastic)c;
        } else
        {
            Object constraints = constraintMap.get(((Object) (c)));
            if((constraints instanceof NullConstraint) && (constraints instanceof JCElastic))
                e = (JCElastic)constraints;
        }
        return e;
    }

    protected boolean isVerticallyElastic(Component c)
    {
        JCElastic e = getElastic(c);
        return e != null && e.getVerticalElasticity() != 0;
    }

    protected boolean isHorizontallyElastic(Component c)
    {
        JCElastic e = getElastic(c);
        return e != null && e.getHorizontalElasticity() != 0;
    }

    protected int getElasticity(Component c)
    {
        int elasticity = 0;
        JCElastic e = getElastic(c);
        if(e != null)
            if(orientation == 0)
                elasticity = e.getHorizontalElasticity();
            else
            if(orientation == 1)
                elasticity = e.getVerticalElasticity();
        return elasticity;
    }

    public void addLayoutComponent(Component comp, Object constraints)
    {
        if(constraints != null && !(constraints instanceof JCElastic))
            throw new IllegalArgumentException("constraint must be null or an instance of JCElastic");
        if(constraints == null)
            constraints = ((Object) (new NullConstraint()));
        constraintMap.put(((Object) (comp)), constraints);
    }

    public Dimension maximumLayoutSize(Container parent)
    {
        int n = parent.getComponentCount();
        int width = 0;
        int height = 0;
        for(int i = 0; i < n; i++)
        {
            Component c = parent.getComponent(i);
            Dimension size = c.getMaximumSize();
            if(orientation == 1)
            {
                width = Math.max(width, size.width);
                height += size.height;
            } else
            {
                height = Math.max(height, size.height);
                width += size.width;
            }
        }

        return new Dimension(width, height);
    }

    public float getLayoutAlignmentX(Container target)
    {
        return 0.0F;
    }

    public float getLayoutAlignmentY(Container target)
    {
        return 0.0F;
    }

    public int getAlignment()
    {
        return alignment;
    }

    public void setAlignment(int alignment)
    {
        this.alignment = alignment;
    }

    public void invalidateLayout(Container container)
    {
    }

}
