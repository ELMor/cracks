// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BundleAdminRole.java

package com.sitraka.deploy.common.roles;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.common.roles:
//            Role

public class BundleAdminRole extends Role
{

    public static final String ANY_BUNDLE = "any";
    private List bundles;

    public BundleAdminRole()
    {
        bundles = ((List) (new Vector()));
    }

    public BundleAdminRole(String user, String bundle)
    {
        super(user);
        init(bundle);
    }

    public BundleAdminRole(String user, List bundles)
    {
        super(user);
        init(bundles);
    }

    public BundleAdminRole(String user, String bundleNames[])
    {
        super(user);
        ArrayList b = new ArrayList(bundleNames.length);
        for(int i = 0; i < bundleNames.length; i++)
            b.add(((Object) (bundleNames[i])));

        init(((List) (b)));
    }

    public BundleAdminRole(String bundle)
    {
        init(bundle);
    }

    private void init(List bundles)
    {
        this.bundles = ((List) (new Vector()));
        addBundles(bundles);
    }

    private void init(String bundle)
    {
        ArrayList b = new ArrayList(1);
        b.add(((Object) (bundle)));
        init(((List) (b)));
    }

    public String getDisplayName()
    {
        return "Bundle Admin";
    }

    public void addBundles(List bundles)
    {
        for(int i = 0; i < bundles.size(); i++)
            addBundle((String)bundles.get(i));

    }

    public void addBundle(String bundle)
    {
        if(bundle == null)
            return;
        bundle = bundle.toLowerCase();
        if(!bundles.contains(((Object) (bundle))))
            bundles.add(((Object) (bundle)));
    }

    public void removeBundle(String bundle)
    {
        bundle = bundle.toLowerCase();
        bundles.remove(((Object) (bundle)));
    }

    public List getBundles()
    {
        return bundles;
    }

    public void setAttributes(List attributes)
    {
        bundles = ((List) (new Vector()));
        for(Iterator atts = attributes.iterator(); atts.hasNext(); addBundle((String)atts.next()));
    }

    public List getAttributes()
    {
        return getBundles();
    }

    public boolean implies(Role other)
    {
        if(!(other instanceof BundleAdminRole))
            return false;
        if(!super.implies(other))
            return false;
        List otherBundles = ((BundleAdminRole)other).getBundles();
        if(bundles.contains("any") || otherBundles.contains("any"))
            return true;
        else
            return bundles.containsAll(((java.util.Collection) (otherBundles)));
    }

    public boolean isValid()
    {
        if(!super.isValid())
            return false;
        else
            return !bundles.isEmpty();
    }

    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append(super.toString());
        int count = bundles.size();
        for(int i = 0; i < count; i++)
        {
            sb.append((String)bundles.get(i));
            if(i < count - 1)
                sb.append(",");
        }

        return sb.toString();
    }

    public Object clone()
    {
        Object clone = super.clone();
        ((BundleAdminRole)clone).bundles = ((List) (new Vector()));
        return clone;
    }
}
