// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Role.java

package com.sitraka.deploy.common.roles;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Role
    implements Cloneable
{

    public static final String FIELD_DELIM = ":";
    public static final String ATTS_DELIM = ",";
    private List users;

    public Role()
    {
        users = ((List) (new Vector()));
    }

    public Role(String user)
    {
        ArrayList u = new ArrayList(1);
        u.add(((Object) (user)));
        init(((List) (u)));
    }

    public Role(List users)
    {
        init(users);
    }

    private void init(List users)
    {
        this.users = ((List) (new Vector()));
        addMembers(users);
    }

    public void setUsers(List users)
    {
        init(users);
    }

    public void addMember(String user)
    {
        if(!users.contains(((Object) (user))))
            users.add(((Object) (user)));
    }

    public void addMembers(List users)
    {
        for(int i = 0; i < users.size(); i++)
            addMember((String)users.get(i));

    }

    public void removeMember(String user)
    {
        users.remove(((Object) (user)));
    }

    public boolean hasMember(String user)
    {
        return users.contains(((Object) (user)));
    }

    public boolean hasMembers(List users)
    {
        return this.users.containsAll(((java.util.Collection) (users)));
    }

    public List getMembers()
    {
        return users;
    }

    public void setAttributes(List list)
    {
    }

    public List getAttributes()
    {
        return ((List) (new ArrayList(0)));
    }

    public boolean implies(Role other)
    {
        if(!isValid() || !other.isValid())
            return false;
        List otherUsers = other.getMembers();
        for(int i = 0; i < otherUsers.size(); i++)
            if(users.contains(otherUsers.get(i)))
                return true;

        return false;
    }

    public boolean isValid()
    {
        return !users.isEmpty();
    }

    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append(((Object)this).getClass().getName());
        sb.append(":");
        sb.append(getMembersAsString());
        sb.append(":");
        return sb.toString();
    }

    private String getMembersAsString()
    {
        StringBuffer sb = new StringBuffer();
        int count = users.size();
        for(int i = 0; i < count; i++)
        {
            sb.append((String)users.get(i));
            if(i < count - 1)
                sb.append(",");
        }

        return sb.toString();
    }

    public int hashCode()
    {
        return 7 * ((Object)this).getClass().getName().hashCode() + 13 * getMembersAsString().hashCode();
    }

    public Object clone()
    {
        Object clone = null;
        try
        {
            clone = super.clone();
        }
        catch(CloneNotSupportedException cnse) { }
        ((Role)clone).users = ((List) (new Vector()));
        return clone;
    }
}
