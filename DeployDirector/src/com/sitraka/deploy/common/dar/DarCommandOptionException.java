// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DarCommandOptionException.java

package com.sitraka.deploy.common.dar;

import java.io.PrintStream;

// Referenced classes of package com.sitraka.deploy.common.dar:
//            DarException

public class DarCommandOptionException extends DarException
{

    protected String contextInfo;

    public DarCommandOptionException(String message)
    {
        super(message);
        contextInfo = null;
    }

    public DarCommandOptionException(String message, String context)
    {
        super(message);
        contextInfo = null;
        contextInfo = context;
    }

    public void printDetails(PrintStream out)
    {
        out.println(((Throwable)this).getMessage());
        if(contextInfo != null)
        {
            out.println("\n");
            out.println(contextInfo);
        }
    }
}
