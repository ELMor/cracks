// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Information.java

package com.sitraka.deploy.common.dar;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.TreeWalker;

public class Information
{

    protected String title;
    protected String vendor;
    protected String description;
    protected boolean offlineAllowed;

    public Information()
    {
        title = "";
        vendor = "";
        description = "";
        offlineAllowed = false;
    }

    public Information(Node install_node)
        throws XmlException
    {
        title = "";
        vendor = "";
        description = "";
        offlineAllowed = false;
        TreeWalker treeWalker = XmlSupport.createTreeWalker(install_node);
        Node node = null;
        Node child_node = null;
        Node currentNode = treeWalker.getRoot();
        node = XmlSupport.getNextElementNamed(treeWalker, "title");
        if(node != null)
        {
            child_node = node.getFirstChild();
            if(child_node != null)
                title = child_node.getNodeValue();
        }
        node = XmlSupport.getNextElementNamed(treeWalker, "vendor");
        if(node != null)
        {
            child_node = node.getFirstChild();
            if(child_node != null)
                vendor = child_node.getNodeValue();
        }
        node = XmlSupport.getNextElementNamed(treeWalker, "description");
        if(node != null)
        {
            child_node = node.getFirstChild();
            if(child_node != null)
                description = child_node.getNodeValue();
        }
        node = XmlSupport.getNextElementNamed(treeWalker, "offline-allowed");
        offlineAllowed = node != null;
    }

    public String getTitle()
    {
        return title;
    }

    public String getVendor()
    {
        return vendor;
    }

    public String getDescription()
    {
        return description;
    }

    public boolean isOfflineAllowed()
    {
        return offlineAllowed;
    }
}
