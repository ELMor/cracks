// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ApplicationDesc.java

package com.sitraka.deploy.common.dar;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import org.w3c.dom.Node;

public class ApplicationDesc
{

    protected String name;
    protected String className;
    protected String arguments;

    public ApplicationDesc()
    {
        name = null;
        className = null;
        arguments = null;
    }

    public ApplicationDesc(Node base)
        throws XmlException
    {
        name = null;
        className = null;
        arguments = null;
        org.w3c.dom.traversal.TreeWalker treeWalker = XmlSupport.createTreeWalker(base);
        Node node = null;
        className = XmlSupport.getAttribute(base, "main-class");
        while((node = XmlSupport.getNextElementNamed(treeWalker, "argument")) != null) 
            if(arguments == null)
                arguments = XmlSupport.getTagValue(node);
            else
                arguments += " " + XmlSupport.getTagValue(node);
    }

    public String getClassName()
    {
        return className;
    }

    public String getName()
    {
        if(name == null && className != null)
        {
            name = className;
            if(name.indexOf(".") >= 0)
                name = name.substring(name.lastIndexOf(".") + 1);
        }
        return name;
    }

    public String getArguments()
    {
        return arguments;
    }
}
