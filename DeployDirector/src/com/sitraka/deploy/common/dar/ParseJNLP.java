// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ParseJNLP.java

package com.sitraka.deploy.common.dar;

import com.klg.jclass.util.JCLocaleManager;
import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.common.app.Admin;
import com.sitraka.deploy.common.app.AppFile;
import com.sitraka.deploy.common.app.ClassPath;
import com.sitraka.deploy.common.app.ConnectionPolicy;
import com.sitraka.deploy.common.app.EntryPoint;
import com.sitraka.deploy.common.app.InstallData;
import com.sitraka.deploy.common.app.JRE;
import com.sitraka.deploy.common.app.Platform;
import com.sitraka.deploy.common.app.Vendor;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.common.app.VersionDTD;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.Hashcode;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.MessageFormat;
import java.util.Vector;
import java.util.zip.ZipOutputStream;
import org.apache.xerces.parsers.AbstractDOMParser;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

// Referenced classes of package com.sitraka.deploy.common.dar:
//            DarFileAccessException, SuffixFileFilter, DarXMLException, Information, 
//            Resources, ApplicationDesc

public class ParseJNLP
{

    protected String dar;
    protected String war;
    protected String bundle;
    protected String version;
    protected String template;
    protected static JCLocaleManager li = JCLocaleManager.getDefault();
    protected Platform platformAll;
    protected Platform platformWin;
    protected Platform platformWini386;
    protected Platform platformUnix;
    protected Platform platformUnixi386;
    protected Platform platformUnixSparc;
    protected boolean includeWin;
    protected boolean includeWini386;
    protected boolean includeUnix;
    protected boolean includeUnixi386;
    protected boolean includeUnixSparc;
    protected Version versionXML;

    public ParseJNLP()
    {
        dar = null;
        war = null;
        bundle = null;
        version = null;
        template = null;
        JCLocaleManager.getDefault().add("com.sitraka.deploy.common.dar.resources.LocaleInfo");
        String debug = System.getProperty("deploy.debug_on");
        if(debug != null)
            if(!debug.equalsIgnoreCase("true"));
        platformAll = null;
        platformWin = null;
        platformWini386 = null;
        platformUnix = null;
        platformUnixi386 = null;
        platformUnixSparc = null;
        includeWin = false;
        includeWini386 = false;
        includeUnix = false;
        includeUnixi386 = false;
        includeUnixSparc = false;
        versionXML = null;
    }

    public void createDar(String darFile, String warFile, String bundleName, String versionName, String templateFile)
        throws DarFileAccessException, DarXMLException
    {
        dar = darFile;
        war = warFile;
        bundle = bundleName;
        version = versionName;
        template = templateFile;
        File war_source = null;
        if(war.equals("-"))
            war_source = saveStdin();
        else
            war_source = new File(war);
        File unpackDir = createTempDir();
        unzipWar(unpackDir, war_source);
        File meta_dir = new File(unpackDir, "META-INF");
        if(!meta_dir.exists() || !meta_dir.isDirectory())
        {
            if(meta_dir.exists())
                meta_dir.delete();
            meta_dir.mkdir();
        }
        File bundle_txt = new File(meta_dir, "bundlename.txt");
        if(bundle_txt.exists())
            bundle_txt.delete();
        try
        {
            Writer wr = ((Writer) (new OutputStreamWriter(((java.io.OutputStream) (new FileOutputStream(bundle_txt))))));
            wr.write(bundle);
            wr.flush();
            wr.close();
        }
        catch(IOException ioe)
        {
            String msg = MessageFormat.format(li.getString("dar message cant create bundlename file"), new Object[] {
                bundle_txt.getAbsolutePath()
            });
            throw new DarFileAccessException(msg, ((Exception) (ioe)));
        }
        SuffixFileFilter jnlpSuffix = new SuffixFileFilter("jnlp");
        File jnlpFiles[] = unpackDir.listFiles(((java.io.FileFilter) (jnlpSuffix)));
        if(jnlpFiles != null && jnlpFiles.length > 0)
            parseJNLP(unpackDir, jnlpFiles[0]);
        File webinf = new File(unpackDir, "WEB-INF");
        if(webinf.exists())
            FileUtils.deleteDirectory(webinf);
        File manifest = new File(meta_dir, "Manifest.mf");
        if(manifest.exists())
            manifest.delete();
        String zipName;
        if(dar == null || dar.length() == 0)
            zipName = bundle + ".dar";
        else
            zipName = dar;
        java.io.OutputStream out_stream = null;
        if(dar.equals("-"))
        {
            out_stream = ((java.io.OutputStream) (System.out));
        } else
        {
            File zipFile = new File(zipName);
            if(zipFile.getParent() != null)
            {
                File dirMaker = new File(zipFile.getParent());
                dirMaker.mkdirs();
            }
            try
            {
                out_stream = ((java.io.OutputStream) (new FileOutputStream(zipFile)));
            }
            catch(IOException ioe)
            {
                String msg = MessageFormat.format(li.getString("dar error creating output"), new Object[] {
                    zipFile.getAbsolutePath()
                });
                throw new DarFileAccessException(msg, ((Exception) (ioe)));
            }
        }
        ZipOutputStream zipper = new ZipOutputStream(out_stream);
        zipper.setLevel(0);
        findFiles(zipper, unpackDir, "");
        try
        {
            ((FilterOutputStream) (zipper)).flush();
            zipper.close();
        }
        catch(IOException ioe)
        {
            String msg = MessageFormat.format(li.getString("dar error creating output"), new Object[] {
                zipName
            });
            throw new DarFileAccessException(msg, ((Exception) (ioe)));
        }
        FileUtils.deleteDirectory(unpackDir);
    }

    protected File createTempDir()
        throws DarFileAccessException
    {
        File tempDir = FileUtils.createTempFile((String)null, "ddconv", ".tmp");
        if(tempDir == null)
        {
            tempDir = FileUtils.createTempFile(".", "ddconv", ".tmp");
            if(tempDir == null)
            {
                String msg = li.getString("dar message unable to create temp file");
                throw new DarFileAccessException(msg);
            }
        }
        return tempDir;
    }

    protected void findFiles(ZipOutputStream zos, File directory, String path)
    {
        File fileList[] = directory.listFiles();
        for(int i = 0; i < fileList.length; i++)
            if(fileList[i].isDirectory())
                findFiles(zos, fileList[i], path + fileList[i].getName() + File.separator);
            else
                FileUtils.addToZipStream(zos, fileList[i], path + fileList[i].getName());

    }

    protected File saveStdin()
        throws DarFileAccessException
    {
        File temp_save = FileUtils.createTempFile((String)null, "dd", ".tmp");
        if(temp_save == null)
            throw new DarFileAccessException(li.getString("dar message unable to create temp file"));
        if(!FileUtils.saveStreamToFile(System.in, temp_save))
        {
            temp_save.delete();
            throw new DarFileAccessException(li.getString("dar message error saving input stream"));
        } else
        {
            return temp_save;
        }
    }

    protected void unzipWar(File unpackDir, File unzipFile)
        throws DarFileAccessException
    {
        if(!unzipFile.exists() || !unzipFile.isFile() || !unzipFile.canRead())
        {
            String msg = li.getString("dar message war file not found");
            throw new DarFileAccessException(msg);
        }
        if(!FileUtils.unzipFile(unzipFile, unpackDir))
        {
            String msg = MessageFormat.format(li.getString("dar message unable to extract war file"), new Object[] {
                unzipFile.getAbsolutePath()
            });
            throw new DarFileAccessException(msg);
        } else
        {
            return;
        }
    }

    protected void parseJNLP(File unpackDir, File jnlp_file)
        throws DarFileAccessException, DarXMLException
    {
        InputSource jnlp_input;
        try
        {
            jnlp_input = XmlSupport.createInputSource(jnlp_file);
        }
        catch(IOException ioe)
        {
            String msg = MessageFormat.format(li.getString("dar message error loading jnlp"), new Object[] {
                jnlp_file.getAbsolutePath()
            });
            throw new DarFileAccessException(msg, ((Exception) (ioe)));
        }
        try
        {
            parseJNLP(unpackDir, jnlp_input);
        }
        finally
        {
            jnlp_file.delete();
        }
    }

    protected void parseJNLP(File unpackDir, InputSource xmlSource)
        throws DarFileAccessException, DarXMLException
    {
        if(template == null || template.length() == 0)
        {
            versionXML = new Version();
        } else
        {
            File xml_file = new File(template);
            try
            {
                versionXML = new Version(xml_file);
            }
            catch(IOException ioe)
            {
                String msg = MessageFormat.format(li.getString("dar message error reading xml file"), new Object[] {
                    xml_file.getAbsolutePath(), ((Throwable) (ioe)).getMessage()
                });
                throw new DarFileAccessException(msg, ((Exception) (ioe)));
            }
            catch(XmlException xmle)
            {
                String msg = MessageFormat.format(li.getString("dar message error parsing xml file"), new Object[] {
                    xml_file.getAbsolutePath(), ((Throwable) (xmle)).getMessage()
                });
                throw new DarXMLException(msg);
            }
        }
        InstallData installdata = versionXML.getInstallData();
        Vendor vendor;
        if((vendor = installdata.getVendor()) == null)
            vendor = new Vendor();
        if(vendor.getName() == null || vendor.getName().length() == 0)
            vendor.setName("Sitraka Inc.");
        if(vendor.getDirectory() == null || vendor.getDirectory().length() == 0)
            vendor.setDirectory("sitraka");
        installdata.setVendor(vendor);
        ConnectionPolicy connectionpolicy = versionXML.getConnectionPolicy();
        connectionpolicy.setScheduleInterval("0 0:0");
        org.w3c.dom.Document xml = null;
        DOMParser p = null;
        try
        {
            DOMParser parser = new DOMParser();
            parser.setFeature("http://xml.org/sax/features/validation", false);
            parser.setFeature("http://apache.org/xml/features/dom/include-ignorable-whitespace", false);
            parser.setErrorHandler(((org.xml.sax.ErrorHandler) (new com.sitraka.deploy.common.XmlSupport.ErrorPrinter())));
            parser.setEntityResolver(((org.xml.sax.EntityResolver) (new com.sitraka.deploy.common.XmlSupport.DTDStringResolver(VersionDTD.getDTD(), VersionDTD.getDTDName()))));
            parser.parse(xmlSource);
            xml = ((AbstractDOMParser) (parser)).getDocument();
        }
        catch(SAXException saxe)
        {
            String msg = MessageFormat.format(li.getString("dar message error parsing xml file"), new Object[] {
                "JNLP XML", saxe.getMessage()
            });
            throw new DarXMLException(msg);
        }
        catch(IOException ioe)
        {
            String msg = MessageFormat.format(li.getString("dar message error reading xml file"), new Object[] {
                "JNLP XML", ((Throwable) (ioe)).getMessage()
            });
            throw new DarFileAccessException(msg, ((Exception) (ioe)));
        }
        if(xml == null)
        {
            String msg = li.getString("dar message war xml create error");
            throw new DarXMLException(msg);
        }
        org.w3c.dom.traversal.TreeWalker treeWalker = XmlSupport.createTreeWalker(((Node) (xml)));
        Node node = null;
        node = XmlSupport.getNextElementNamed(treeWalker, "information");
        if(node == null)
        {
            String msg = MessageFormat.format(li.getString("dar message war missing xml tag"), new Object[] {
                bundle, "information"
            });
            throw new DarXMLException(msg);
        }
        Information info = null;
        try
        {
            info = new Information(node);
        }
        catch(XmlException xe)
        {
            throw new DarXMLException(((Throwable) (xe)).getMessage());
        }
        if(template != null && template.length() > 0)
            platformAll = versionXML.getPlatform();
        else
            platformAll = new Platform();
        platformAll.setName("all");
        platformWin = new Platform();
        platformWin.setName("windows");
        platformWini386 = new Platform();
        platformWini386.setName("i386");
        platformUnix = new Platform();
        platformUnix.setName("unix");
        platformUnixi386 = new Platform();
        platformUnixi386.setName("i386");
        platformUnixSparc = new Platform();
        platformUnixSparc.setName("sparc");
        treeWalker = XmlSupport.createTreeWalker(((Node) (xml)));
        node = XmlSupport.getNextElementNamed(treeWalker, "resources");
        if(node == null)
        {
            String msg = MessageFormat.format(li.getString("dar message war missing xml tag"), new Object[] {
                bundle, "resources"
            });
            throw new DarXMLException(msg);
        }
        Node resource_parent = node.getParentNode();
        NodeList siblings = resource_parent.getChildNodes();
        try
        {
            for(int i = 0; i < siblings.getLength(); i++)
            {
                Node child = siblings.item(i);
                if(child.getNodeName().equals("resources"))
                {
                    Resources resources = new Resources(node);
                    String osName = resources.getOS();
                    String archName = resources.getArch();
                    if(osName == null && archName == null)
                    {
                        Vector props = resources.getProperties();
                        Vector sys_props = installdata.getSystemProps();
                        for(int j = 0; j < props.size(); j++)
                            sys_props.addElement(props.elementAt(i));

                    }
                    generatePlatform(unpackDir, resources, osName, archName);
                }
            }

        }
        catch(XmlException xe)
        {
            throw new DarXMLException(((Throwable) (xe)).getMessage());
        }
        if(includeWini386)
            platformWin.addPlatform(platformWini386);
        if(includeUnixi386)
            platformUnix.addPlatform(platformUnixi386);
        if(includeUnixSparc)
            platformUnix.addPlatform(platformUnixSparc);
        if(includeWin)
            platformAll.addPlatform(platformWin);
        if(includeUnix)
            platformAll.addPlatform(platformUnix);
        treeWalker = XmlSupport.createTreeWalker(((Node) (xml)));
        node = XmlSupport.getNextElementNamed(treeWalker, "application-desc");
        if(node == null)
        {
            String msg = MessageFormat.format(li.getString("dar message war missing xml tag"), new Object[] {
                bundle, "application-desc"
            });
            throw new DarXMLException(msg);
        }
        ApplicationDesc ad = null;
        try
        {
            ad = new ApplicationDesc(node);
        }
        catch(XmlException xe)
        {
            throw new DarXMLException(((Throwable) (xe)).getMessage());
        }
        EntryPoint entry = new EntryPoint();
        entry.setName(ad.getName());
        entry.setClassName(ad.getClassName());
        entry.setArguments(ad.getArguments());
        platformAll.getJRE().addEntryPoint(entry);
        versionXML.setPlatform(platformAll);
        if(info.getVendor().length() != 0)
        {
            InstallData installdata2 = versionXML.getInstallData();
            Vendor vendor2 = installdata2.getVendor();
            vendor2.setName(info.getVendor());
        }
        if(info.isOfflineAllowed())
        {
            ConnectionPolicy cp = versionXML.getConnectionPolicy();
            cp.setAccessPolicy(2);
        }
        versionXML.setDescription(info.getDescription());
        if(version == null || version.length() == 0)
        {
            if(versionXML.getName() == null || versionXML.getName().equals(""))
                versionXML.setName("1.0.0");
        } else
        {
            versionXML.setName(version);
        }
        versionXML.setID("1");
        File meta_dir = new File(unpackDir, "META-INF");
        File versionFile = new File(meta_dir, "version.xml");
        try
        {
            versionXML.saveXMLToFile(versionFile);
        }
        catch(IOException ioe)
        {
            throw new DarFileAccessException(((Throwable) (ioe)).getMessage(), ((Exception) (ioe)));
        }
    }

    protected void generatePlatform(File unpackDir, Resources resource, String osName, String archName)
    {
        if(osName != null)
            osName = osName.toLowerCase();
        if(archName != null)
            archName = archName.toLowerCase();
        Admin admin = new Admin();
        admin.setUserName("");
        admin.setPassword("");
        Hashcode hashcode = new Hashcode();
        Vector fileHolder = new Vector();
        Vector hashcodeHolder = new Vector();
        AppFile appfile = null;
        fileHolder = resource.getFiles();
        if(osName == null && archName == null)
        {
            JRE jre;
            if(platformAll.getJRE() != null)
                jre = platformAll.getJRE();
            else
                jre = new JRE();
            Vector classpaths = new Vector();
            classpaths = jre.getClassPaths();
            for(int i = 0; i < fileHolder.size(); i++)
            {
                ClassPath cp = new ClassPath();
                cp.setFileName((String)fileHolder.elementAt(i));
                classpaths.add(((Object) (cp)));
                appfile = new AppFile(new File(unpackDir, (String)fileHolder.elementAt(i)), ((String) (null)), false, false);
                platformAll.addFile(appfile);
            }

            jre.setClassPaths(classpaths);
            if(template == null || template.length() == 0 || jre.getVersion() == null || jre.getVersion().equals(""))
            {
                String min_ver = resource.getMinJREVersion();
                if(min_ver != null)
                {
                    if(min_ver.length() == 3)
                        min_ver = min_ver + ".0";
                    min_ver = min_ver + "*";
                } else
                {
                    min_ver = "1.1.8*";
                }
                jre.setVersion(min_ver);
            }
            if(platformAll.getAdmin() == null)
                platformAll.setAdmin(admin);
            platformAll.setJRE(jre);
        } else
        if(osName.startsWith("windows"))
        {
            includeWin = true;
            Platform subPlatform = new Platform();
            JRE jre = new JRE();
            Vector classpaths = jre.getClassPaths();
            for(int i = 0; i < fileHolder.size(); i++)
            {
                ClassPath cp = new ClassPath();
                cp.setFileName((String)fileHolder.elementAt(i));
                classpaths.add(((Object) (cp)));
                hashcodeHolder.add(((Object) (Hashcode.computeHash((String)fileHolder.elementAt(i)))));
                appfile = new AppFile((String)fileHolder.elementAt(i));
                appfile.setHash((String)hashcodeHolder.elementAt(i));
                subPlatform.addFile(appfile);
            }

            jre.setClassPaths(classpaths);
            if(template == null || template.length() == 0)
            {
                jre.setVersion("1.1.8*");
                subPlatform.setAdmin(admin);
            }
            subPlatform.setJRE(jre);
            if(osName.endsWith("windows"))
            {
                JRE jre2 = platformWin.getJRE();
                Vector classpaths2 = jre2.getClassPaths();
                for(int i = 0; i < fileHolder.size(); i++)
                {
                    ClassPath cp2 = new ClassPath();
                    cp2.setFileName((String)fileHolder.elementAt(i));
                    classpaths2.add(((Object) (cp2)));
                    hashcodeHolder.add(((Object) (Hashcode.computeHash((String)fileHolder.elementAt(i)))));
                    appfile = new AppFile((String)fileHolder.elementAt(i));
                    appfile.setHash((String)hashcodeHolder.elementAt(i));
                    platformWin.addFile(appfile);
                }

                jre2.setClassPaths(classpaths2);
                if(template == null || template.length() == 0)
                {
                    jre.setVersion("1.1.8*");
                    subPlatform.setAdmin(admin);
                }
                platformWin.setJRE(jre2);
            } else
            if(osName.endsWith("95"))
            {
                includeWini386 = true;
                subPlatform.setName("95");
                platformWini386.addPlatform(subPlatform);
            } else
            if(osName.endsWith("98"))
            {
                includeWini386 = true;
                subPlatform.setName("98");
                platformWini386.addPlatform(subPlatform);
            } else
            if(osName.endsWith("me"))
            {
                includeWini386 = true;
                subPlatform.setName("me");
                platformWini386.addPlatform(subPlatform);
            } else
            if(osName.endsWith("nt"))
            {
                includeWini386 = true;
                subPlatform.setName("nt");
                platformWini386.addPlatform(subPlatform);
            }
        } else
        {
            includeUnix = true;
            if(archName == null)
            {
                JRE jre2 = platformUnix.getJRE();
                Vector classpaths2 = jre2.getClassPaths();
                for(int i = 0; i < fileHolder.size(); i++)
                {
                    ClassPath cp2 = new ClassPath();
                    cp2.setFileName((String)fileHolder.elementAt(i));
                    classpaths2.add(((Object) (cp2)));
                    hashcodeHolder.add(((Object) (Hashcode.computeHash((String)fileHolder.elementAt(i)))));
                    appfile = new AppFile((String)fileHolder.elementAt(i));
                    appfile.setHash((String)hashcodeHolder.elementAt(i));
                    platformUnix.addFile(appfile);
                }

                jre2.setClassPaths(classpaths2);
                if(template == null || template.length() == 0)
                {
                    jre2.setVersion("1.1.8*");
                    platformUnix.setAdmin(admin);
                }
                platformUnix.setJRE(jre2);
            } else
            if(osName.equalsIgnoreCase("solaris"))
            {
                includeUnixSparc = true;
                Platform subPlatform = new Platform();
                JRE jre2 = new JRE();
                Vector classpaths2 = jre2.getClassPaths();
                for(int i = 0; i < fileHolder.size(); i++)
                {
                    ClassPath cp2 = new ClassPath();
                    cp2.setFileName((String)fileHolder.elementAt(i));
                    classpaths2.add(((Object) (cp2)));
                    hashcodeHolder.add(((Object) (Hashcode.computeHash((String)fileHolder.elementAt(i)))));
                    appfile = new AppFile((String)fileHolder.elementAt(i));
                    appfile.setHash((String)hashcodeHolder.elementAt(i));
                    subPlatform.addFile(appfile);
                }

                jre2.setClassPaths(classpaths2);
                if(template == null || template.length() == 0)
                {
                    jre2.setVersion("1.1.8*");
                    subPlatform.setAdmin(admin);
                }
                subPlatform.setJRE(jre2);
                subPlatform.setName("solaris");
                platformUnixSparc.addPlatform(subPlatform);
            } else
            if(osName.equalsIgnoreCase("hp-ux"))
            {
                includeUnixSparc = true;
                Platform subPlatform = new Platform();
                JRE jre2 = new JRE();
                Vector classpaths2 = jre2.getClassPaths();
                for(int i = 0; i < fileHolder.size(); i++)
                {
                    ClassPath cp2 = new ClassPath();
                    cp2.setFileName((String)fileHolder.elementAt(i));
                    classpaths2.add(((Object) (cp2)));
                    hashcodeHolder.add(((Object) (Hashcode.computeHash((String)fileHolder.elementAt(i)))));
                    appfile = new AppFile((String)fileHolder.elementAt(i));
                    appfile.setHash((String)hashcodeHolder.elementAt(i));
                    subPlatform.addFile(appfile);
                }

                jre2.setClassPaths(classpaths2);
                if(template == null || template.length() == 0)
                {
                    jre2.setVersion("1.1.8*");
                    subPlatform.setAdmin(admin);
                }
                subPlatform.setJRE(jre2);
                subPlatform.setName("hp-ux");
                platformUnixSparc.addPlatform(subPlatform);
            } else
            {
                includeUnixi386 = true;
                Platform subPlatform = new Platform();
                JRE jre2 = new JRE();
                Vector classpaths2 = jre2.getClassPaths();
                for(int i = 0; i < fileHolder.size(); i++)
                {
                    ClassPath cp2 = new ClassPath();
                    cp2.setFileName((String)fileHolder.elementAt(i));
                    classpaths2.add(((Object) (cp2)));
                    hashcodeHolder.add(((Object) (Hashcode.computeHash((String)fileHolder.elementAt(i)))));
                    appfile = new AppFile((String)fileHolder.elementAt(i));
                    appfile.setHash((String)hashcodeHolder.elementAt(i));
                    subPlatform.addFile(appfile);
                }

                jre2.setClassPaths(classpaths2);
                if(template == null || template.length() == 0)
                {
                    jre2.setVersion("1.1.8*");
                    subPlatform.setAdmin(admin);
                }
                subPlatform.setJRE(jre2);
                subPlatform.setName(osName);
                platformUnixi386.addPlatform(subPlatform);
            }
        }
    }

}
