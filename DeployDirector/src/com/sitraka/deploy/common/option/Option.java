// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Option.java

package com.sitraka.deploy.common.option;


public class Option
{

    protected boolean isPersistent;
    public static final int REQUIRES_ARGUMENT = 1;
    public static final int MAY_HAVE_ARGUMENT = 0;
    public static final int NO_ARGUMENT = -1;
    protected int takesArguments;
    protected char shortName;
    protected String longName;

    public Option(boolean isPersistent, int takesArguments, char shortName, String longName)
    {
        this.isPersistent = false;
        this.isPersistent = isPersistent;
        this.takesArguments = takesArguments;
        this.shortName = shortName;
        this.longName = longName;
    }

    public String getLongName()
    {
        return longName;
    }

    public char getShortName()
    {
        return shortName;
    }

    public int takesArgument()
    {
        return takesArguments;
    }
}
