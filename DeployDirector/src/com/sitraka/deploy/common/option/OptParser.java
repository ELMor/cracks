// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   OptParser.java

package com.sitraka.deploy.common.option;

import java.util.HashMap;
import java.util.Map;

// Referenced classes of package com.sitraka.deploy.common.option:
//            Option

public class OptParser
{

    protected HashMap storedState;
    protected String storedArgs[];
    protected Option options[];
    protected int curr_pos;
    protected int opt_pos;
    protected int arg_pos;
    protected int lastOption;
    protected boolean inShortOpts;
    protected int compactOptIndex;
    public static final int MORE_ARGUMENTS = 1;
    public static final int FINISHED_PARSING = -1;

    public OptParser()
    {
        storedState = new HashMap();
        storedArgs = null;
        options = null;
        curr_pos = 0;
        opt_pos = 0;
        arg_pos = -1;
        lastOption = 0;
        inShortOpts = false;
        compactOptIndex = 0;
    }

    public OptParser(Option opts[])
    {
        storedState = new HashMap();
        storedArgs = null;
        options = null;
        curr_pos = 0;
        opt_pos = 0;
        arg_pos = -1;
        lastOption = 0;
        inShortOpts = false;
        compactOptIndex = 0;
        setOptions(opts);
    }

    public OptParser(Option opts[], String args[])
    {
        storedState = new HashMap();
        storedArgs = null;
        options = null;
        curr_pos = 0;
        opt_pos = 0;
        arg_pos = -1;
        lastOption = 0;
        inShortOpts = false;
        compactOptIndex = 0;
        setOptions(opts);
        setArgs(args);
    }

    public Option[] getOptions()
    {
        if(options == null)
        {
            return null;
        } else
        {
            Option returnOptions[] = new Option[options.length];
            System.arraycopy(((Object) (options)), 0, ((Object) (returnOptions)), 0, options.length);
            return returnOptions;
        }
    }

    public void setOptions(Option opts[])
    {
        if(opts == null)
        {
            options = null;
            return;
        } else
        {
            options = new Option[opts.length];
            System.arraycopy(((Object) (opts)), 0, ((Object) (options)), 0, opts.length);
            return;
        }
    }

    public void setArgs(String args[])
    {
        if(args == null || args.length < 1)
        {
            throw new IllegalArgumentException("Zero length or null argument array has been passed");
        } else
        {
            curr_pos = 0;
            storedArgs = new String[args.length];
            System.arraycopy(((Object) (args)), 0, ((Object) (storedArgs)), 0, args.length);
            return;
        }
    }

    public int getOption()
    {
        if(curr_pos >= storedArgs.length)
            return -1;
        Option opt = null;
        opt_pos = curr_pos;
        try
        {
            if(storedArgs[curr_pos].equals("--"))
            {
                opt_pos = -1;
                curr_pos++;
                return -1;
            }
            if(storedArgs[curr_pos].startsWith("--"))
            {
                String argname = storedArgs[curr_pos].substring(2);
                opt = findLongOpt(argname);
            } else
            if(storedArgs[curr_pos].startsWith("-"))
            {
                if(!inShortOpts && storedArgs[curr_pos].length() > 2)
                {
                    inShortOpts = true;
                    compactOptIndex = 1;
                }
                char argchar = '?';
                if(inShortOpts)
                {
                    argchar = storedArgs[curr_pos].charAt(compactOptIndex++);
                    if(compactOptIndex >= storedArgs[curr_pos].length())
                        compactOptIndex = 0;
                } else
                {
                    argchar = storedArgs[curr_pos].charAt(1);
                }
                opt = findShortOpt(argchar);
            } else
            {
                arg_pos = curr_pos++;
                return 1;
            }
            checkArg(opt);
            if(opt.isPersistent)
                addState(opt);
            if(inShortOpts && compactOptIndex == 0)
                inShortOpts = false;
            if(!inShortOpts)
                curr_pos++;
            lastOption = ((int) (opt.getShortName()));
            return ((int) (opt.getShortName()));
        }
        catch(IllegalArgumentException iae)
        {
            arg_pos = opt_pos;
            opt_pos = -1;
            curr_pos++;
            throw iae;
        }
    }

    protected void checkArg(Option opt)
    {
        arg_pos = -1;
        if(opt.takesArgument() == 1)
        {
            if(inShortOpts)
            {
                String msg = "Option \"-" + opt.getShortName() + "\" requires an " + "argument, so can't be combined with other options.";
                throw new IllegalArgumentException(msg);
            }
            if(curr_pos + 1 >= storedArgs.length || storedArgs[curr_pos + 1].startsWith("-") && !storedArgs[curr_pos + 1].equals("-"))
            {
                String msg = "Option \"-" + opt.getShortName() + " | --" + opt.getLongName() + "\" requires an argument.";
                throw new IllegalArgumentException(msg);
            }
            arg_pos = ++curr_pos;
        } else
        if(opt.takesArgument() == 0 && !inShortOpts && curr_pos + 1 < storedArgs.length && (!storedArgs[curr_pos + 1].startsWith("-") || storedArgs[curr_pos + 1].equals("-")))
            arg_pos = ++curr_pos;
    }

    protected void addState(Option opt)
    {
        if(opt.takesArgument() == 1)
            storedState.put(((Object) (opt.getLongName())), ((Object) (getOptionArg())));
        else
        if(opt.takesArgument() == 0)
            storedState.put(((Object) (opt.getLongName())), ((Object) (getOptionArg())));
        else
            storedState.put(((Object) (opt.getLongName())), ((Object) (null)));
    }

    public String getOptionArg()
    {
        if(arg_pos == -1)
            return null;
        else
            return storedArgs[arg_pos];
    }

    public int getOptionIndex()
    {
        return opt_pos;
    }

    public int getPreviousOpt()
    {
        return lastOption;
    }

    public Map getState()
    {
        return ((Map) (storedState));
    }

    public String getState(String name)
    {
        if(!storedState.containsKey(((Object) (name))))
            return null;
        else
            return (String)storedState.get(((Object) (name)));
    }

    public boolean hasState(String longName)
    {
        return storedState.containsKey(((Object) (longName)));
    }

    protected Option findLongOpt(String name)
    {
        for(int i = 0; i < options.length; i++)
            if(options[i].getLongName().equalsIgnoreCase(name))
                return options[i];

        boolean matched = false;
        int optionToReturn = -1;
        for(int i = 0; i < options.length; i++)
            if(options[i].getLongName().startsWith(name))
                if(!matched)
                {
                    matched = true;
                    optionToReturn = i;
                } else
                {
                    String msg = "Possibly abbreviated option name \"" + name + "\" is ambiguous.\nPossible matches are: \"--" + options[optionToReturn].getLongName() + "\", \"--" + options[i].getLongName() + "\"";
                    for(int j = i; j < options.length; j++)
                        if(options[j].getLongName().startsWith(name))
                            msg = msg + ", \"--" + options[j].getLongName() + "\"";

                    msg = msg + ".";
                    throw new IllegalArgumentException(msg);
                }

        if(matched)
            return options[optionToReturn];
        else
            throw new IllegalArgumentException("Option name \"--" + name + "\" is not recognized.");
    }

    protected Option findShortOpt(char argChar)
    {
        for(int i = 0; i < options.length; i++)
            if(options[i].getShortName() == argChar)
                return options[i];

        throw new IllegalArgumentException("Short option \"-" + argChar + "\" is not recognized.");
    }
}
