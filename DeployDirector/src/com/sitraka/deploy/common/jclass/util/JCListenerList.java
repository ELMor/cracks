// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JCListenerList.java

package com.sitraka.deploy.common.jclass.util;

import java.io.Serializable;
import java.util.Enumeration;

// Referenced classes of package com.sitraka.deploy.common.jclass.util:
//            JCListenerListEnumeration

public class JCListenerList
    implements Serializable
{

    JCListenerList next;
    Object listener;

    private JCListenerList(JCListenerList list, Object l)
    {
        next = null;
        listener = null;
        next = list;
        listener = l;
    }

    public static JCListenerList add(JCListenerList list, Object newListener)
    {
        return new JCListenerList(list, newListener);
    }

    public static JCListenerList remove(JCListenerList list, Object oldListener)
    {
        JCListenerList newList = null;
        if(list == null)
            return null;
        for(; list != null; list = list.next)
            if(list.listener != oldListener)
                newList = add(newList, list.listener);

        return newList;
    }

    public static Enumeration elements(JCListenerList list)
    {
        return ((Enumeration) (new JCListenerListEnumeration(list)));
    }
}
