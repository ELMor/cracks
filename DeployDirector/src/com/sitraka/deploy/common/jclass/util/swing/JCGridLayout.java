// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JCGridLayout.java

package com.sitraka.deploy.common.jclass.util.swing;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;

public class JCGridLayout extends GridLayout
{

    protected int hgap;
    protected int vgap;
    protected int rows;
    protected int cols;
    protected int row_heights[];
    protected int col_widths[];
    public static final int VARIABLE = 0;

    public JCGridLayout(int rows, int cols)
    {
        this(rows, cols, 0, 0);
    }

    public JCGridLayout(int rows, int cols, int hgap, int vgap)
    {
        super(rows, cols, hgap, vgap);
        row_heights = new int[0];
        col_widths = new int[0];
        this.rows = rows;
        this.cols = cols;
        this.hgap = hgap;
        this.vgap = vgap;
    }

    protected void getGridSizes(Container parent, boolean min)
    {
        int ncomponents = parent.getComponentCount();
        if(ncomponents == 0)
            return;
        int nrows = rows;
        int ncols = cols;
        if(nrows > 0)
            ncols = ((ncomponents + nrows) - 1) / nrows;
        else
            nrows = ((ncomponents + ncols) - 1) / ncols;
        row_heights = new int[nrows];
        col_widths = new int[ncols];
        for(int i = 0; i < ncomponents; i++)
        {
            Component comp = parent.getComponent(i);
            Dimension d = min ? comp.getMinimumSize() : comp.getPreferredSize();
            int row = i / ncols;
            if(d.height > row_heights[row])
                row_heights[row] = d.height;
            int col = i % ncols;
            if(d.width > col_widths[col])
                col_widths[col] = d.width;
        }

    }

    final int sum(int array[])
    {
        if(array == null)
            return 0;
        int s = 0;
        for(int i = 0; i < array.length; i++)
            s += array[i];

        return s;
    }

    public Dimension preferredLayoutSize(Container parent)
    {
        Insets insets = parent.getInsets();
        getGridSizes(parent, false);
        return new Dimension(insets.left + insets.right + sum(col_widths) + (col_widths.length + 1) * hgap, insets.top + insets.bottom + sum(row_heights) + (row_heights.length + 1) * vgap);
    }

    public Dimension minimumLayoutSize(Container parent)
    {
        Insets insets = parent.getInsets();
        getGridSizes(parent, true);
        return new Dimension(insets.left + insets.right + sum(col_widths) + (col_widths.length + 1) * hgap, insets.top + insets.bottom + sum(row_heights) + (row_heights.length + 1) * vgap);
    }

    protected void setBounds(int pos, int row, int col, Component comp, int x, int y, int w, 
            int h)
    {
        comp.setBounds(x, y, w, h);
    }

    public void layoutContainer(Container parent)
    {
        int ncomponents = parent.getComponentCount();
        if(ncomponents == 0)
            return;
        Insets insets = parent.getInsets();
        getGridSizes(parent, false);
        int nrows = rows;
        int ncols = cols;
        if(nrows > 0)
            ncols = ((ncomponents + nrows) - 1) / nrows;
        else
            nrows = ((ncomponents + ncols) - 1) / ncols;
        Dimension psize = ((Component) (parent)).getSize();
        int col = 0;
        int x = insets.left + hgap;
        for(; col < ncols; col++)
        {
            int row = 0;
            int y = insets.top + vgap;
            for(; row < nrows; row++)
            {
                int i = row * ncols + col;
                if(i < ncomponents)
                {
                    int w = Math.max(0, Math.min(col_widths[col], psize.width - insets.right - x));
                    int h = Math.max(0, Math.min(row_heights[row], psize.height - insets.bottom - y));
                    setBounds(i, row, col, parent.getComponent(i), x, y, w, h);
                }
                y += row_heights[row] + vgap;
            }

            x += col_widths[col] + hgap;
        }

    }
}
