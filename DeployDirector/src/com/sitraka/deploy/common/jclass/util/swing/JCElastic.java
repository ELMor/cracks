// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JCElastic.java

package com.sitraka.deploy.common.jclass.util.swing;


public interface JCElastic
{

    public abstract int getHorizontalElasticity();

    public abstract int getVerticalElasticity();
}
