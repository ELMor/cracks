// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JCTypeConverter.java

package com.sitraka.deploy.common.jclass.util;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.common.jclass.util:
//            JCStringTokenizer

public class JCTypeConverter
{

    public static boolean reportErrors = true;

    public JCTypeConverter()
    {
    }

    public static void error(String s, String token)
    {
        error("Error parsing '" + token + "' in " + s);
    }

    public static void error(String msg)
    {
        System.err.println(msg);
    }

    public static void parseError(String token)
    {
        if(reportErrors)
            error("Error parsing '" + token + "'");
    }

    public static String trim(Object obj)
    {
        String s;
        if(obj == null || (s = obj.toString()) == null)
            return null;
        s = s.trim();
        int k = s.indexOf('\0');
        if(k != -1)
            return s.substring(0, k);
        else
            return s;
    }

    public static int toInt(String s, int def)
    {
        if(s == null)
            return def;
        if(s.equalsIgnoreCase("maxint"))
            return 0x7fffffff;
        if(s.equalsIgnoreCase("novalue"))
            return -999;
        if(s.equalsIgnoreCase("variable"))
            return -998;
        if(s.equalsIgnoreCase("default"))
            return -999;
        try
        {
            return Integer.parseInt(s);
        }
        catch(Exception e)
        {
            parseError(s);
        }
        return def;
    }

    public static double toDouble(String s, double def)
    {
        if(s == null)
            return def;
        try
        {
            return Double.valueOf(s).doubleValue();
        }
        catch(Exception e)
        {
            parseError(s);
        }
        return def;
    }

    public static boolean toBoolean(String s, boolean def)
    {
        if(s == null)
            return def;
        try
        {
            return Boolean.valueOf(s).booleanValue();
        }
        catch(Exception e)
        {
            parseError(s);
        }
        return def;
    }

    public static String[] toStringList(String s)
    {
        return toStringList(s, ',');
    }

    public static String[] toStringList(String s, char delim)
    {
        return toStringList(s, delim, true);
    }

    public static String[] toStringList(String s, char delim, boolean trim)
    {
        if(s == null)
            return null;
        JCStringTokenizer string = new JCStringTokenizer(s);
        String list[] = new String[string.countTokens(delim)];
        for(int i = 0; string.hasMoreTokens(); i++)
        {
            String next = string.nextToken(delim);
            if(next == null)
                next = "";
            String trimmed = next.trim();
            if(trimmed == null)
                trimmed = "";
            list[i] = trim ? trimmed : next;
        }

        return list;
    }

    public static int[] toIntList(String s, char delim)
    {
        if(s == null)
            return null;
        JCStringTokenizer string = new JCStringTokenizer(s);
        int list[] = new int[string.countTokens(delim)];
        for(int i = 0; string.hasMoreTokens(); i++)
        {
            String token = string.nextToken(delim).trim();
            list[i] = toInt(token, 0);
        }

        return list;
    }

    public static Integer[] toIntegerList(String s, char delim)
    {
        if(s == null)
            return null;
        JCStringTokenizer string = new JCStringTokenizer(s);
        Integer list[] = new Integer[string.countTokens(delim)];
        for(int i = 0; string.hasMoreTokens(); i++)
        {
            String token = string.nextToken(delim).trim();
            list[i] = new Integer(toInt(token, 0));
        }

        return list;
    }

    public static Double[] toDoubleList(String s, char delim)
    {
        if(s == null)
            return null;
        JCStringTokenizer string = new JCStringTokenizer(s);
        Double list[] = new Double[string.countTokens(delim)];
        for(int i = 0; string.hasMoreTokens(); i++)
        {
            String token = string.nextToken(delim).trim();
            list[i] = new Double(toDouble(token, 0.0D));
        }

        return list;
    }

    public static int[] toIntList(String s, char delim, int def[])
    {
        int list[] = toIntList(s, delim);
        return list == null ? def : list;
    }

    public static Integer[] toIntegerList(String s, char delim, Integer def[])
    {
        Integer list[] = toIntegerList(s, delim);
        return list == null ? def : list;
    }

    public static Double[] toDoubleList(String s, char delim, Double def[])
    {
        Double list[] = toDoubleList(s, delim);
        return list == null ? def : list;
    }

    public static String toNewLine(String string)
    {
        if(string == null || string.indexOf("\\n") == -1)
            return string;
        char s[] = new char[string.length()];
        int k = 0;
        for(int i = 0; i < s.length;)
        {
            if(string.charAt(i) == '\\' && i < s.length - 1 && string.charAt(i + 1) == 'n')
            {
                s[k] = '\n';
                i++;
            } else
            {
                s[k] = string.charAt(i);
            }
            i++;
            k++;
        }

        return new String(s, 0, k);
    }

    public static String fromNewLine(String s)
    {
        if(s == null || s.indexOf('\n') == -1)
            return s;
        int total = 0;
        int i;
        for(i = 0; i < s.length(); i++)
            if(s.charAt(i) == '\n')
                total++;

        char c[] = new char[s.length() + total];
        i = 0;
        int k;
        for(k = 0; i < s.length(); k++)
        {
            if(s.charAt(i) == '\n')
            {
                c[k++] = '\\';
                c[k] = 'n';
            } else
            {
                c[k] = s.charAt(i);
            }
            i++;
        }

        return new String(c, 0, k);
    }

    public static Vector toVector(String s, char delim)
    {
        if(s == null)
            return null;
        JCStringTokenizer string = new JCStringTokenizer(s);
        Vector list = new Vector(string.countTokens(delim));
        string.strip_esc = false;
        for(int i = 0; string.hasMoreTokens(); i++)
        {
            String token = string.nextToken(delim);
            list.setElementAt(((Object) (token)), i);
        }

        return list;
    }

    public static Vector toVector(String s, char delim, Vector def)
    {
        Vector v = toVector(s, delim);
        return v == null ? def : v;
    }

    public static int toEnum(String string, String type, String param, String strings[][], int values[][], int def)
    {
        if(param == null)
            param = type;
        int index;
        for(index = 0; index < strings.length; index++)
            if(strings[index][0].equalsIgnoreCase(type))
                break;

        if(index == strings.length)
        {
            error("Error converting '" + string + "' to " + param);
            return def;
        }
        string = string.trim();
        for(int i = 1; i < strings[index].length; i++)
            if(string.equalsIgnoreCase(strings[index][i]))
                return values[index][i - 1];

        try
        {
            return Integer.parseInt(string);
        }
        catch(Exception e)
        {
            error("Error converting '" + string + "' to " + param);
        }
        return def;
    }

    public static int toEnum(String s, String type, String strings[], int values[], int def)
    {
        if(s == null)
            return def;
        s = s.trim();
        for(int i = 0; i < strings.length; i++)
            if(s.equalsIgnoreCase(strings[i]))
                return values[i];

        try
        {
            return Integer.parseInt(s);
        }
        catch(Exception e)
        {
            error("Error converting '" + s + "' to " + type);
        }
        return def;
    }

    public static long toEnum(String s, String type, String strings[], long values[], long def)
    {
        if(s == null)
            return def;
        s = s.trim();
        for(int i = 0; i < strings.length; i++)
            if(s.equalsIgnoreCase(strings[i]))
                return values[i];

        try
        {
            return Long.parseLong(s);
        }
        catch(Exception e)
        {
            error("Error converting '" + s + "' to " + type);
        }
        return def;
    }

    public static int toEnum(String s, String strings[], int values[], int def)
    {
        if(s == null)
            return def;
        s = s.trim();
        for(int i = 0; i < strings.length; i++)
            if(s.equalsIgnoreCase(strings[i]))
                return values[i];

        return def;
    }

    public static int[] toEnumList(String s, String type, String strings[], int values[], int def[])
    {
        if(s == null)
            return def;
        JCStringTokenizer string = new JCStringTokenizer(s);
        int list[] = new int[string.countTokens(',')];
        for(int i = 0; string.hasMoreTokens(); i++)
        {
            String token = string.nextToken(',').trim();
            int def_value = def == null || i >= def.length ? 0 : def[i];
            list[i] = toEnum(token, type, strings, values, def_value);
        }

        return list;
    }

    public static String fromEnum(int value, String strings[], int values[])
    {
        if(values == null || strings == null)
            return null;
        for(int i = 0; i < values.length; i++)
            if(value == values[i] && i < strings.length)
                return strings[i];

        return null;
    }

    public static String fromEnum(long value, String strings[], long values[])
    {
        if(values == null || strings == null)
            return null;
        for(int i = 0; i < values.length; i++)
            if(value == values[i] && i < strings.length)
                return strings[i];

        return null;
    }

    public static String fromEnum(int value, String param, String type, String strings[][], int values[][], String def)
    {
        int index;
        for(index = 0; index < strings.length; index++)
            if(strings[index][0].equalsIgnoreCase(type))
                break;

        if(index == strings.length)
            return null;
        for(int i = 0; i < values[index].length; i++)
            if(values[index][i] == value)
                return strings[index][i + 1];

        try
        {
            return String.valueOf(value);
        }
        catch(Exception e)
        {
            error("Error converting '" + value + "' to " + param);
        }
        return def;
    }

    public static void checkEnum(int value, String param, int values[])
    {
        for(int i = 0; i < values.length; i++)
            if(values[i] == value)
                return;

        throw new IllegalArgumentException("invalid " + param + ": " + value);
    }

    public static String toString(Object o)
    {
        if(!(o instanceof Vector))
            return fromNewLine(o == null ? "" : o.toString());
        Vector v = (Vector)o;
        StringBuffer s = new StringBuffer(v.size());
        int k = 0;
        for(int size = v.size(); k < size; k++)
        {
            s.append(toString(v.elementAt(k)));
            if(k < size - 1)
                s.append(',');
        }

        return new String(s);
    }

    public static Date toDate(String s, Date def)
    {
        if(s == null)
            return def;
        try
        {
            SimpleDateFormat formatter = (SimpleDateFormat)DateFormat.getDateTimeInstance(1, 1);
            ((DateFormat) (formatter)).setCalendar(Calendar.getInstance());
            ((DateFormat) (formatter)).setTimeZone(TimeZone.getDefault());
            return ((DateFormat) (formatter)).parse(s);
        }
        catch(ParseException pe)
        {
            return def;
        }
    }

    public static String fromDate(Date d, String def)
    {
        if(d == null)
            return def;
        try
        {
            SimpleDateFormat formatter = (SimpleDateFormat)DateFormat.getDateTimeInstance(1, 1);
            ((DateFormat) (formatter)).setCalendar(Calendar.getInstance());
            ((DateFormat) (formatter)).setTimeZone(TimeZone.getDefault());
            return ((DateFormat) (formatter)).format(d);
        }
        catch(Exception e)
        {
            return def;
        }
    }

    public static String removeEscape(String s)
    {
        if(s == null || s.indexOf('\\') == -1)
            return s;
        int len = s.length();
        char token[] = new char[len];
        int i = 0;
        int k;
        for(k = 0; i < len; k++)
        {
            char c = s.charAt(i);
            if(i + 1 < len && c == '\\')
            {
                i++;
                if(s.charAt(i) == 'n')
                    token[k] = '\n';
                else
                    token[k] = s.charAt(i);
            } else
            {
                token[k] = c;
            }
            i++;
        }

        return k <= 0 ? null : new String(token, 0, k);
    }

}
