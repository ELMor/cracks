// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JCListenerListEnumeration.java

package com.sitraka.deploy.common.jclass.util;

import java.util.Enumeration;

// Referenced classes of package com.sitraka.deploy.common.jclass.util:
//            JCListenerList

class JCListenerListEnumeration
    implements Enumeration
{

    JCListenerList current;

    JCListenerListEnumeration(JCListenerList list)
    {
        current = list;
    }

    public boolean hasMoreElements()
    {
        return current != null;
    }

    public Object nextElement()
    {
        Object o = current.listener;
        current = current.next;
        return o;
    }
}
