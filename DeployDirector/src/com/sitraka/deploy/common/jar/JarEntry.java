// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JarEntry.java

package com.sitraka.deploy.common.jar;

import com.sitraka.deploy.util.Hashcode;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;

public class JarEntry
    implements Serializable
{

    static final long serialVersionUID = 0x2a7c7e41c6db66b0L;
    protected String name;
    protected File file;
    protected int method;
    protected boolean fromJar;
    protected boolean isDirectory;
    protected String hashcode;
    protected long size;

    public JarEntry(File _file, String _name)
    {
        name = null;
        file = null;
        method = -1;
        fromJar = false;
        isDirectory = false;
        hashcode = null;
        size = -1L;
        if(_file == null || _name == null)
            return;
        file = _file;
        name = _name;
        fromJar = false;
        method = 8;
        isDirectory = _file.isDirectory();
        if(isDirectory && !name.endsWith("/"))
            name = name + "/";
    }

    public JarEntry(ZipEntry _entry, File _file)
    {
        name = null;
        file = null;
        method = -1;
        fromJar = false;
        isDirectory = false;
        hashcode = null;
        size = -1L;
        if(_entry == null || _file == null)
            return;
        fromJar = true;
        file = _file;
        name = _entry.getName();
        method = _entry.getMethod();
        isDirectory = _entry.isDirectory();
        if(method == -1)
            method = 8;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String _name)
    {
        if(_name == null)
        {
            return;
        } else
        {
            name = _name;
            return;
        }
    }

    public int getMethod()
    {
        return method;
    }

    public void setMethod(int _method)
    {
        method = _method;
    }

    public boolean isFromJar()
    {
        return fromJar;
    }

    public boolean isDirectory()
    {
        return isDirectory;
    }

    public InputStream getInputStream()
    {
        if(isDirectory())
            return null;
        FileInputStream fis = null;
        try
        {
            fis = new FileInputStream(file);
        }
        catch(FileNotFoundException e)
        {
            return null;
        }
        if(!fromJar)
            return ((InputStream) (fis));
        ZipInputStream zis = new ZipInputStream(((InputStream) (fis)));
        try
        {
            for(ZipEntry e = null; (e = zis.getNextEntry()) != null;)
                if(e.getName().equals(((Object) (name))))
                    return ((InputStream) (zis));

            zis.close();
            fis.close();
            zis = null;
            fis = null;
        }
        catch(ZipException e) { }
        catch(IOException e) { }
        return null;
    }

    public String getHashcode()
    {
        if(hashcode == null)
            computeHashcode();
        return hashcode;
    }

    public long getSize()
    {
        if(size == -1L)
            computeHashcode();
        return size;
    }

    public void computeHashcode()
    {
        if(hashcode != null || size != -1L)
            return;
        InputStream is = getInputStream();
        if(is == null)
            return;
        computeHashcode(is);
        try
        {
            is.close();
            is = null;
        }
        catch(IOException e)
        {
            return;
        }
    }

    public void computeHashcode(InputStream is)
    {
        hashcode = null;
        size = 0L;
        Hashcode h = new Hashcode();
        try
        {
            byte buffer[] = new byte[8192];
            for(int read = -1; (read = is.read(buffer, 0, 8192)) != -1;)
            {
                size += read;
                h.update(buffer, read);
            }

        }
        catch(IOException e)
        {
            hashcode = null;
            size = -1L;
            return;
        }
        hashcode = h.returnHash();
    }
}
