// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FileCheckpoint.java

package com.sitraka.deploy.common.checkpoint;


// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            BaseCheckpoint, FileUploadObject

public class FileCheckpoint extends BaseCheckpoint
{

    protected static String SerializeFileName = "File.chk";

    public FileCheckpoint()
    {
        this(((Object) (null)));
    }

    public FileCheckpoint(Object o)
    {
        super(o);
    }

    public FileUploadObject getFileObject()
    {
        return (FileUploadObject)((BaseCheckpoint)this).getUserData();
    }

    public String getDefaultExtension()
    {
        return SerializeFileName;
    }

}
