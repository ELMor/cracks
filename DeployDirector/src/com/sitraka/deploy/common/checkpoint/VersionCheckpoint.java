// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   VersionCheckpoint.java

package com.sitraka.deploy.common.checkpoint;


// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            BaseCheckpoint, VersionObject

public class VersionCheckpoint extends BaseCheckpoint
{

    protected static String SerializeFileName = "Version.chk";

    public VersionCheckpoint()
    {
        this(((Object) (null)));
    }

    public VersionCheckpoint(Object o)
    {
        super(o);
    }

    public VersionObject getVersionObject()
    {
        return (VersionObject)((BaseCheckpoint)this).getUserData();
    }

    public String getDefaultExtension()
    {
        return SerializeFileName;
    }

}
