// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ApplicationObject.java

package com.sitraka.deploy.common.checkpoint;

import java.io.Serializable;

// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            MetaInfo

public class ApplicationObject
    implements Serializable, MetaInfo
{

    protected String remoteServer;
    protected String application;

    public ApplicationObject(String remoteServer, String application)
    {
        this.remoteServer = null;
        this.application = null;
        this.remoteServer = remoteServer;
        this.application = application;
    }

    public String getServer()
    {
        return remoteServer;
    }

    public String getApplication()
    {
        return application;
    }
}
