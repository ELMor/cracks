// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Checkpoint.java

package com.sitraka.deploy.common.checkpoint;

import java.io.File;
import java.io.Serializable;

public interface Checkpoint
    extends Serializable
{

    public static final long serialVersionUID = 0xa93c77586ec0c818L;
    public static final int INVALID = 0;
    public static final int NOT_STARTED = 100;
    public static final int COMPLETED = 0xf423f;

    public abstract void removeCheckpointFile();

    public abstract void flush();

    public abstract File getTempFile();

    public abstract void setTempFile(File file);

    public abstract File getTempDir();

    public abstract void setTempDir(File file);

    public abstract File getDestinationDir();

    public abstract void setDestinationDir(File file);

    public abstract File getSourceDir();

    public abstract void setSourceDir(File file);

    public abstract int getState();

    public abstract void setState(int i);

    public abstract void setUserData(Object obj);

    public abstract Object getUserData();

    public abstract int validateFiles();

    public abstract String listInvalidFiles();

    public abstract boolean moveFiles();

    public abstract boolean extractFileDataFromZip(File file);

    public abstract boolean isIncomplete(int i);
}
