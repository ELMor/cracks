// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BaseCheckpoint.java

package com.sitraka.deploy.common.checkpoint;

import com.sitraka.deploy.common.awt.ProgressBar;
import com.sitraka.deploy.common.jar.JarEntry;
import com.sitraka.deploy.common.jar.JarFile;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.Hashcode;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            Checkpoint

public class BaseCheckpoint
    implements Checkpoint
{
    protected class FileObject
        implements Serializable
    {

        protected int state;
        protected String name;
        protected String parent;
        protected String hashcode;
        protected long size;
        protected long current_size;

        public void setHashcode(String hash)
        {
            hashcode = hash;
        }

        public void setSize(long size)
        {
            this.size = size;
        }

        public void setState(int new_state)
        {
            state = new_state;
        }

        public int getState()
        {
            return state;
        }

        public String getName()
        {
            return name;
        }

        public String getParent()
        {
            return parent;
        }

        public long getCurrentSize()
        {
            return current_size;
        }

        public long getSize()
        {
            return size;
        }

        public boolean moveFile(File from_dir, File to_dir, JarFile jarFile)
        {
            if(state == 4 || state == 5)
                return true;
            if(getParent() != null)
            {
                JarEntry j = new JarEntry(new File(from_dir, getParent() + File.separator + getName()), getName());
                jarFile.addEntry(j);
                return true;
            }
            from_dir = new File(FileUtils.unifyFileSeparator(from_dir.getAbsolutePath()));
            to_dir = new File(FileUtils.unifyFileSeparator(to_dir.getAbsolutePath()));
            File full_path_to_dest = new File(to_dir, name);
            if(!full_path_to_dest.exists())
                full_path_to_dest.mkdirs();
            if(!to_dir.canRead() || !to_dir.isDirectory())
                return false;
            File source = new File(from_dir, name);
            if(!source.canRead())
                return false;
            File new_file = new File(to_dir, name);
            if(new_file.exists() && new_file.isDirectory() && source.isDirectory())
            {
                state = 4;
                return true;
            }
            if(new_file.exists() && !new_file.delete())
                return false;
            if(!source.renameTo(new_file))
            {
                return false;
            } else
            {
                state = 4;
                return true;
            }
        }

        public boolean verifyFile(File basedir)
        {
            if(state == 3)
                return true;
            File file = null;
            if(parent != null && parent.length() != 0)
                file = new File(basedir, parent + "/" + name);
            else
                file = new File(basedir, name);
            if(!file.canRead())
            {
                state = 0;
                return false;
            }
            if(file.isDirectory())
            {
                state = 3;
                return true;
            }
            current_size = file.length();
            if(size != -1L && current_size != size)
            {
                if(current_size > size)
                {
                    file.delete();
                    current_size = 0L;
                }
                state = 0;
                return false;
            }
            String new_hash = Hashcode.computeHash(file);
            if(!new_hash.equals(((Object) (hashcode))))
            {
                state = 0;
                return false;
            } else
            {
                state = 3;
                return true;
            }
        }

        public boolean equals(Object obj)
        {
            if(obj == null)
                return false;
            if(obj instanceof String)
                if(parent == null)
                {
                    return getName().equals(((Object) (((FileObject)obj).getName())));
                } else
                {
                    String comp_name = getName() + "%%" + getParent();
                    return comp_name.equals(((Object) ((String)obj)));
                }
            if(!(obj instanceof FileObject))
                return false;
            FileObject f = (FileObject)obj;
            if(!getName().equals(((Object) (f.getName()))))
                return false;
            if((getParent() == null || f.getParent() == null) && getParent() != f.getParent())
                return false;
            return getParent() == f.getParent() || getParent().equals(((Object) (f.getParent())));
        }

        public FileObject(String name, String parent, String hashcode, long size)
        {
            state = 0;
            this.name = null;
            this.parent = null;
            this.hashcode = null;
            this.size = -1L;
            current_size = -1L;
            if(name == null)
            {
                throw new IllegalArgumentException("File name cannot be null");
            } else
            {
                this.name = name;
                this.parent = parent;
                this.size = size;
                this.hashcode = hashcode;
                state = 1;
                current_size = -1L;
                return;
            }
        }

        public FileObject(String name)
        {
            this(name, ((String) (null)), ((String) (null)), -1L);
        }
    }


    public static final int INITIAL_CHECK_COMPLETE = 200;
    public static final int HEADER_COMPLETE = 300;
    public static final int DOWNLOAD_COMPLETE = 400;
    public static final int MOVE_COMPLETE = 500;
    public static final int RECEIVE_COMPLETE = 600;
    public static final int FILE_NOT_STARTED = 1;
    public static final int FILE_STARTED = 2;
    public static final int FILE_VALIDATED = 3;
    public static final int FILE_MOVED = 4;
    public static final int FILE_COMPLETED = 5;
    protected int state;
    protected String app_name;
    protected File tempDir;
    protected File tempFile;
    protected File sourceDir;
    protected File destinationDir;
    protected Vector files;
    protected Hashtable jarFiles;
    protected int compressionMethod;
    protected int compressionLevel;
    protected Object userData;
    protected File outputFile;
    protected InputStream is;
    protected boolean autoFlush;

    public BaseCheckpoint()
    {
        state = 0;
        app_name = null;
        tempDir = null;
        tempFile = null;
        sourceDir = null;
        destinationDir = null;
        files = null;
        jarFiles = null;
        compressionMethod = 8;
        compressionLevel = -1;
        userData = null;
        outputFile = null;
        is = null;
        autoFlush = true;
    }

    public BaseCheckpoint(Object o)
    {
        this(((File) (null)), o);
    }

    public BaseCheckpoint(File file, Object o)
    {
        state = 0;
        app_name = null;
        tempDir = null;
        tempFile = null;
        sourceDir = null;
        destinationDir = null;
        files = null;
        jarFiles = null;
        compressionMethod = 8;
        compressionLevel = -1;
        userData = null;
        outputFile = null;
        is = null;
        autoFlush = true;
        files = new Vector();
        jarFiles = new Hashtable();
        setFile(file);
        setUserData(o);
    }

    public void setDefaultFileName(File tempDir)
    {
        setFile(FileUtils.createTempFile(FileUtils.unifyFileSeparator(tempDir.getAbsolutePath()), "ss", getDefaultExtension()));
    }

    public void setFile(File file)
    {
        if(file == null)
            outputFile = file;
        else
            outputFile = new File(FileUtils.unifyFileSeparator(file.getAbsolutePath()));
    }

    public String getDefaultExtension()
    {
        return "";
    }

    public boolean isIncomplete(int action)
    {
        boolean status = getState() < action;
        return status;
    }

    public void removeCheckpointFile()
    {
        if(outputFile != null && outputFile.exists())
            outputFile.delete();
    }

    public void flush()
    {
        if(outputFile == null)
            return;
        removeCheckpointFile();
        try
        {
            FileOutputStream fos = new FileOutputStream(outputFile);
            ObjectOutputStream out_stream = new ObjectOutputStream(((java.io.OutputStream) (fos)));
            out_stream.writeObject(((Object) (this)));
            out_stream.flush();
            out_stream.close();
            out_stream = null;
            System.gc();
        }
        catch(IOException ioe)
        {
            return;
        }
    }

    public static Checkpoint reload(File in_file)
    {
        if(!in_file.canRead())
            return null;
        Checkpoint check = null;
        try
        {
            FileInputStream fis = new FileInputStream(in_file);
            ObjectInputStream in_stream = new ObjectInputStream(((InputStream) (fis)));
            check = (Checkpoint)in_stream.readObject();
            in_stream.close();
            in_stream = null;
            System.gc();
        }
        catch(IOException ioe)
        {
            return null;
        }
        catch(ClassNotFoundException cnfe)
        {
            return null;
        }
        return check;
    }

    public File getTempFile()
    {
        return tempFile;
    }

    public void setTempFile(File tempFile)
    {
        this.tempFile = tempFile;
    }

    public File getTempDir()
    {
        return tempDir;
    }

    public void setTempDir(File dir)
    {
        tempDir = new File(FileUtils.unifyFileSeparator(dir.getAbsolutePath()));
    }

    public File getDestinationDir()
    {
        return destinationDir;
    }

    public void setDestinationDir(File dir)
    {
        destinationDir = new File(FileUtils.unifyFileSeparator(dir.getAbsolutePath()));
    }

    public File getSourceDir()
    {
        return sourceDir;
    }

    public void setSourceDir(File dir)
    {
        sourceDir = new File(FileUtils.unifyFileSeparator(dir.getAbsolutePath()));
    }

    public int getState()
    {
        return state;
    }

    public void setState(int new_state)
    {
        state = new_state;
        if(getAutoFlush())
            if(state == 0xf423f)
                removeCheckpointFile();
            else
                flush();
    }

    public void setUserData(Object o)
    {
        userData = o;
    }

    public Object getUserData()
    {
        return userData;
    }

    public void setInputStream(InputStream is)
    {
        this.is = is;
    }

    public InputStream getInputStream()
    {
        return is;
    }

    public void setAutoFlush(boolean flush)
    {
        autoFlush = flush;
    }

    public boolean getAutoFlush()
    {
        return autoFlush;
    }

    public int validateFiles()
    {
        return validateFiles(((ProgressBar) (null)));
    }

    public int validateFiles(ProgressBar meter)
    {
        int size = files != null ? files.size() : 0;
        if(size == 0)
            return -1;
        if(meter != null)
        {
            meter.setMinimum(0);
            meter.setMaximum(size);
            meter.setValue(0);
        }
        int count = 0;
        for(int i = 0; i < size; i++)
        {
            FileObject f = (FileObject)files.elementAt(i);
            if(f.verifyFile(getSourceDir()))
            {
                f.setState(3);
                count++;
            } else
            {
                f.setState(0);
            }
            if(meter != null)
                meter.setValue(i);
        }

        return count;
    }

    public String listInvalidFiles()
    {
        if(getSourceDir() != null)
            validateFiles();
        int size = files != null ? files.size() : 0;
        StringBuffer filelist = new StringBuffer();
        boolean bad_file_found = false;
        for(int i = 0; i < size; i++)
        {
            FileObject f = (FileObject)files.elementAt(i);
            if(f.getState() == 0)
            {
                String filename = null;
                if(f.getParent() != null)
                    filename = f.getParent() + "%%" + f.getName();
                else
                    filename = f.getName();
                filelist.append(filename);
                if(f.getSize() != -1L && f.getCurrentSize() < f.getSize())
                    filelist.append("|" + (f.getSize() - f.getCurrentSize()));
                filelist.append(",");
                bad_file_found = true;
            }
        }

        if(bad_file_found)
            return filelist.toString();
        else
            return null;
    }

    public boolean moveFiles()
    {
        return moveFiles(((ProgressBar) (null)));
    }

    public boolean moveFiles(ProgressBar meter)
    {
        int size = files != null ? files.size() : 0;
        if(meter != null)
        {
            meter.setMinimum(0);
            meter.setMaximum(size);
        }
        for(int i = 0; i < size; i++)
        {
            FileObject f = (FileObject)files.elementAt(i);
            JarFile j = null;
            String parent = f.getParent();
            if(parent != null)
            {
                parent = FileUtils.unifyFileSeparator(parent);
                j = (JarFile)jarFiles.get(((Object) (parent)));
            }
            if(!f.moveFile(getSourceDir(), getDestinationDir(), j))
                return false;
            if(meter != null)
                meter.setValue(i);
            if(j == null)
            {
                f.setState(5);
                files.removeElement(((Object) (f)));
                i--;
                size--;
            }
        }

        return true;
    }

    public void addJarFile(String jar_name)
    {
        String name = FileUtils.unifyFileSeparator(jar_name);
        JarFile j = (JarFile)jarFiles.get(((Object) (name)));
        if(j == null)
        {
            j = new JarFile(new File(getDestinationDir(), name), true, getCompressionMethod(), getCompressionLevel());
            j.setWorkingDir(getTempDir());
            j.setDestination(name);
            jarFiles.put(((Object) (name)), ((Object) (j)));
        }
    }

    public boolean extractFileDataFromZip(File zipfile)
    {
        Properties props = new Properties();
        long expected_size = -1L;
        ByteArrayOutputStream out_buffer = new ByteArrayOutputStream();
        ZipInputStream zis = null;
        ZipEntry entry = null;
        try
        {
            zis = new ZipInputStream(((InputStream) (new FileInputStream(zipfile))));
            entry = zis.getNextEntry();
            if(entry == null)
                try
                {
                    zis.close();
                    FileInputStream fis = new FileInputStream(zipfile);
                    byte inbuffer[] = new byte[80];
                    int bytes = fis.read(inbuffer);
                    if(bytes < 0)
                    {
                        boolean flag2 = false;
                        return flag2;
                    }
                    String first = new String(inbuffer);
                    if(first.indexOf("HTTP/1") != -1)
                    {
                        int offset = 0;
                        while(bytes >= 0) 
                        {
                            int new_bytes = fis.read(inbuffer);
                            String next = new String(inbuffer);
                            String total = first + next;
                            int pos = total.indexOf("\r\n\r\n");
                            if(pos != -1)
                            {
                                offset += pos + 4;
                                break;
                            }
                            offset += bytes;
                            bytes = new_bytes;
                            first = next;
                        }
                        fis.close();
                        fis = new FileInputStream(zipfile);
                        fis.skip(offset);
                        zis = new ZipInputStream(((InputStream) (fis)));
                        entry = zis.getNextEntry();
                    }
                }
                catch(IOException ioe) { }
            for(; entry != null && !entry.getName().equals("deploy.properties"); entry = zis.getNextEntry());
            if(entry == null)
            {
                boolean flag = false;
                return flag;
            }
            expected_size = entry.getSize();
            int n = -1;
            byte buffer[] = new byte[8096];
            while((n = zis.read(buffer, 0, 8096)) != -1) 
                out_buffer.write(buffer, 0, n);
        }
        catch(IOException e)
        {
            if(expected_size == -1L || expected_size != (long)out_buffer.size())
            {
                boolean flag1 = false;
                return flag1;
            }
        }
        finally
        {
            try
            {
                zis.close();
                zis = null;
                entry = null;
                System.gc();
            }
            catch(Exception e) { }
        }
        try
        {
            ByteArrayInputStream bis = new ByteArrayInputStream(out_buffer.toByteArray());
            props.load(((InputStream) (bis)));
            bis.close();
            bis = null;
            out_buffer = null;
        }
        catch(IOException e)
        {
            return false;
        }
        int base = 0;
        do
        {
            String name = (String)((Hashtable) (props)).get(((Object) ("file" + base + ".name")));
            if(name != null)
            {
                name = FileUtils.unifyFileSeparator(name);
                String hash = (String)((Hashtable) (props)).get(((Object) ("file" + base + ".hash")));
                String size_string = (String)((Hashtable) (props)).get(((Object) ("file" + base + ".size")));
                long file_length = Long.parseLong(size_string);
                String parent_name = (String)((Hashtable) (props)).get(((Object) ("file" + base + ".parent")));
                if(parent_name != null)
                {
                    parent_name = FileUtils.unifyFileSeparator(parent_name);
                    if((new File(getDestinationDir(), parent_name)).length() > 50000L)
                    {
                        addJarFile(parent_name);
                    } else
                    {
                        name = parent_name;
                        parent_name = null;
                        file_length = -1L;
                    }
                }
                FileObject f = new FileObject(name, parent_name, hash, file_length);
                if(!files.contains(((Object) (f))))
                    files.addElement(((Object) (f)));
                base++;
            } else
            {
                flush();
                return true;
            }
        } while(true);
    }

    public boolean deleteFile(File basedir, String filename)
    {
        if(filename.indexOf("%%") == -1)
        {
            File f = new File(basedir, filename);
            return f.delete();
        }
        int index = filename.indexOf("%%");
        String base = filename.substring(0, index);
        String sub = filename.substring(index + "%%".length());
        base = FileUtils.unifyFileSeparator(base);
        JarFile j = (JarFile)jarFiles.get(((Object) (base)));
        if(j == null)
            return false;
        else
            return j.removeEntry(sub);
    }

    public boolean rewriteJarFiles(File basedir)
    {
        for(Enumeration e = jarFiles.keys(); e.hasMoreElements();)
        {
            String name = (String)e.nextElement();
            name = FileUtils.unifyFileSeparator(name);
            JarFile j = (JarFile)jarFiles.get(((Object) (name)));
            File parent_dest = new File(j.getDestination().getParent());
            if(!parent_dest.exists() && (parent_dest.mkdirs()))
                return false;
            if(!j.saveToFile())
                return false;
            File f = j.getDestination();
            File dest = new File(basedir, name);
            if(dest.exists() && !dest.delete())
            {
                j.getDestination().delete();
                return false;
            }
            if(!f.renameTo(dest))
            {
                j.getDestination().delete();
                return false;
            }
        }

        int size = files != null ? files.size() : 0;
        for(int i = 0; i < size; i++)
        {
            FileObject f = (FileObject)files.elementAt(i);
            File file = new File(getTempDir(), f.getName());
            file.delete();
            files.removeElement(((Object) (f)));
            i--;
            size--;
        }

        return true;
    }

    public void setCompressionLevel(int level)
    {
        compressionLevel = level;
    }

    public int getCompressionLevel()
    {
        return compressionLevel;
    }

    public void setCompressionMethod(int method)
    {
        compressionMethod = method;
    }

    public int getCompressionMethod()
    {
        return compressionMethod;
    }
}
