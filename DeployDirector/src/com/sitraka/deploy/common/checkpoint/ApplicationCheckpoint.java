// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ApplicationCheckpoint.java

package com.sitraka.deploy.common.checkpoint;


// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            BaseCheckpoint, ApplicationObject

public class ApplicationCheckpoint extends BaseCheckpoint
{

    protected static String SerializeFileName = "App.chk";

    public ApplicationCheckpoint()
    {
        this(((Object) (null)));
    }

    public ApplicationCheckpoint(Object o)
    {
        super(o);
    }

    public ApplicationObject getApplicationObject()
    {
        return (ApplicationObject)((BaseCheckpoint)this).getUserData();
    }

    public String getDefaultExtension()
    {
        return SerializeFileName;
    }

}
