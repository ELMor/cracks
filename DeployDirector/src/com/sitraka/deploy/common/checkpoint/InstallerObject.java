// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   InstallerObject.java

package com.sitraka.deploy.common.checkpoint;

import java.io.Serializable;

// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            MetaInfo

public class InstallerObject
    implements Serializable, MetaInfo
{

    protected String remoteServer;
    protected String filename;

    public InstallerObject(String _remoteServer, String _filename)
    {
        remoteServer = null;
        filename = null;
        remoteServer = _remoteServer;
        filename = _filename;
    }

    public String getServer()
    {
        return remoteServer;
    }

    public String getFilename()
    {
        return filename;
    }
}
