// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LogCheckpoint.java

package com.sitraka.deploy.common.checkpoint;


// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            BaseCheckpoint, LogObject

public class LogCheckpoint extends BaseCheckpoint
{

    protected static String SerializeFileName = "Log.chk";

    public LogCheckpoint()
    {
        this(((Object) (null)));
    }

    public LogCheckpoint(Object o)
    {
        super(o);
    }

    public LogObject getLogObject()
    {
        return (LogObject)((BaseCheckpoint)this).getUserData();
    }

    public String getDefaultExtension()
    {
        return SerializeFileName;
    }

}
