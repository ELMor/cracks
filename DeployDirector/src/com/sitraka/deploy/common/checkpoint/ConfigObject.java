// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ConfigObject.java

package com.sitraka.deploy.common.checkpoint;

import java.io.Serializable;

// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            MetaInfo

public class ConfigObject
    implements Serializable, MetaInfo
{

    protected String remoteServer;

    public ConfigObject(String remoteServer)
    {
        this.remoteServer = null;
        this.remoteServer = remoteServer;
    }

    public String getServer()
    {
        return remoteServer;
    }
}
