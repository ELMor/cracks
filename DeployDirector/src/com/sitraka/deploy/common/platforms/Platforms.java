// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Platforms.java

package com.sitraka.deploy.common.platforms;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.apache.xerces.parsers.AbstractDOMParser;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

// Referenced classes of package com.sitraka.deploy.common.platforms:
//            PlatformPlatform, PlatformsDTD

public class Platforms
{

    protected PlatformPlatform rootPlatform;

    public Platforms()
    {
        rootPlatform = null;
        rootPlatform = new PlatformPlatform();
        rootPlatform.setPlatformList(new Vector());
    }

    public Platforms(File xmlfile)
        throws SAXException, IOException
    {
        this();
        parseXML(XmlSupport.createInputSource(xmlfile));
    }

    public Platforms(InputSource xml_source)
        throws SAXException, IOException
    {
        this();
        parseXML(xml_source);
    }

    public Platforms(InputStream stream)
        throws IOException
    {
        this();
        try
        {
            InputSource source = XmlSupport.createInputSource(stream);
            source.setSystemId("file:/" + PlatformsDTD.getDTDName());
            parseXML(source);
        }
        catch(SAXException e) { }
    }

    protected void parseXML(InputSource xml_source)
        throws SAXException, IOException
    {
        org.w3c.dom.Document xml = null;
        try
        {
            DOMParser parser = new DOMParser();
            parser.setFeature("http://xml.org/sax/features/validation", true);
            parser.setFeature("http://apache.org/xml/features/dom/include-ignorable-whitespace", false);
            parser.setErrorHandler(((org.xml.sax.ErrorHandler) (new com.sitraka.deploy.common.XmlSupport.ErrorPrinter())));
            parser.setEntityResolver(((org.xml.sax.EntityResolver) (new com.sitraka.deploy.common.XmlSupport.DTDStringResolver(PlatformsDTD.getDTD(), PlatformsDTD.getDTDName()))));
            parser.parse(xml_source);
            xml = ((AbstractDOMParser) (parser)).getDocument();
        }
        catch(SAXParseException e)
        {
            throw e;
        }
        catch(SAXException e)
        {
            throw e;
        }
        catch(IOException e)
        {
            throw e;
        }
        if(xml == null)
            throw new SAXException("Unable to create XmlDocument object");
        org.w3c.dom.traversal.TreeWalker treeWalker = XmlSupport.createTreeWalker(((Node) (xml)));
        Node platforms_node = XmlSupport.getNextElementNamed(treeWalker, "PLATFORMS");
        for(Node platform_node = XmlSupport.getNextElementNamed(treeWalker, "PLATFORM"); platform_node != null; platform_node = XmlSupport.getNextElementNamed(treeWalker, "PLATFORM"))
            if(platform_node.getParentNode() == platforms_node)
                try
                {
                    PlatformPlatform platform = new PlatformPlatform(platform_node, ((PlatformPlatform) (null)));
                    getPlatforms().addElement(((Object) (platform)));
                }
                catch(XmlException e)
                {
                    throw new SAXException(((Throwable) (e)).getMessage());
                }

    }

    public PlatformPlatform getRootPlatform()
    {
        return rootPlatform;
    }

    public boolean removePlatform(String platform, String vendor, String version)
    {
        Vector platforms = getPlatforms();
        int size = platforms != null ? platforms.size() : 0;
        for(int i = 0; i < size; i++)
        {
            PlatformPlatform plat = (PlatformPlatform)platforms.elementAt(i);
            boolean check = plat.removePlatform(platform, vendor, version);
            if(check)
            {
                if(plat.canRemove() && platforms.elementAt(i).equals(((Object) (plat))))
                    platforms.removeElementAt(i);
                return true;
            }
        }

        return false;
    }

    public PlatformPlatform findMatchingPlatform(String platform_name)
    {
        Vector platforms = getPlatforms();
        int size = platforms != null ? platforms.size() : 0;
        for(int i = 0; i < size; i++)
        {
            PlatformPlatform plat = (PlatformPlatform)platforms.elementAt(i);
            PlatformPlatform check = plat.findMatchingPlatform(platform_name);
            if(check != null)
                return check;
        }

        return null;
    }

    public PlatformPlatform findExactPlatform(String platformName)
    {
        Vector platforms = getPlatforms();
        int size = platforms != null ? platforms.size() : 0;
        for(int i = 0; i < size; i++)
        {
            PlatformPlatform plat = (PlatformPlatform)platforms.elementAt(i);
            if(plat.getName().equals(((Object) (platformName))))
                return plat;
        }

        return null;
    }

    public Vector getPlatforms()
    {
        return rootPlatform.getPlatformList();
    }

    public void setPlatforms(Vector platforms)
    {
        rootPlatform.setPlatformList(platforms);
    }

    public PlatformPlatform addPlatformSubtree(String platformName)
    {
        Vector platformList = PropertyUtils.readVector(platformName, '.');
        if(platformList == null)
            return null;
        String currentPlatform = null;
        PlatformPlatform root = rootPlatform;
        PlatformPlatform nextExistingPlatform = null;
        for(Enumeration e = platformList.elements(); e.hasMoreElements();)
        {
            String singleName = (String)e.nextElement();
            if(currentPlatform == null)
                currentPlatform = singleName;
            else
                currentPlatform = currentPlatform + "." + singleName;
            nextExistingPlatform = root.findExactPlatform(currentPlatform);
            if(nextExistingPlatform == null)
            {
                PlatformPlatform parent = rootPlatform.findMatchingPlatform(platformName);
                if(parent == null)
                    parent = rootPlatform;
                nextExistingPlatform = new PlatformPlatform();
                nextExistingPlatform.setParent(parent);
                nextExistingPlatform.setShortName(singleName);
                parent.addPlatform(nextExistingPlatform);
            }
            root = nextExistingPlatform;
        }

        return nextExistingPlatform;
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent;
        sb.append("<?xml version=\"1.0\"?>\n");
        sb.append("<!DOCTYPE PLATFORMS SYSTEM \"Platforms.dtd\">\n");
        sb.append("<PLATFORMS>\n");
        Vector platforms = getPlatforms();
        for(int i = 0; i < platforms.size(); i++)
        {
            PlatformPlatform platform = (PlatformPlatform)platforms.elementAt(i);
            sb.append(platform.getXML(indent_string));
        }

        sb.append("</PLATFORMS>\n");
        return sb.toString();
    }

    public void saveXMLToFile(File xmlFile)
        throws IOException
    {
        Writer writer = ((Writer) (new OutputStreamWriter(((java.io.OutputStream) (new FileOutputStream(xmlFile))), "UTF8")));
        writer.write(getXML("\t"));
        writer.flush();
        writer.close();
    }
}
