// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PlatformClassPath.java

package com.sitraka.deploy.common.platforms;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.util.FileUtils;
import org.w3c.dom.Node;

public class PlatformClassPath
{

    protected String file_name;

    public PlatformClassPath()
    {
        file_name = null;
    }

    public PlatformClassPath(Node class_node)
        throws XmlException
    {
        file_name = null;
        String _name = XmlSupport.getAttribute(class_node, "FILE");
        if(_name == null)
        {
            throw new XmlException("XML Error: FILE tag missing from CLASSPATH object");
        } else
        {
            setFileName(_name);
            return;
        }
    }

    public void setFileName(String filename)
    {
        file_name = FileUtils.makeInternalPath(filename);
    }

    public String getFileName()
    {
        return file_name;
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<CLASSPATH");
        sb.append(" FILE=\"" + XmlSupport.toCDATA(file_name) + "\"");
        sb.append("/>\n");
        return sb.toString();
    }
}
