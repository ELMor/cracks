// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CommonEnums.java

package com.sitraka.deploy.common;

import java.io.PrintStream;

public class CommonEnums
{

    public static final String DD_VERSION = "2.5.0";
    public static final String DD_URL = "http://www.sitraka.com/software/deploydirector";
    public static final String DD_LICENSE_VERSION = "2.5";
    public static final String LICENSE_SIGNATURE = "sitraka.license.signature";
    public static final String LICENSE_VERSION = "sitraka.license.version";
    public static final String LICENSE_EXPIRY = "expiry";
    public static final String LICENSE_APPLICATIONS = "applications";
    public static final String LICENSE_HOSTS = "hosts";
    public static final String LICENSE_SERIAL_NUMBER = "serial_number";
    public static final String LICENSE_BUNDLES = "bundles";
    public static final String LICENSE_CLIENTS = "clients";
    public static final String LICENSE_TYPE = "type";
    public static final String LICENSE_LICENSE_VERSION = "license_version";
    public static final String LICENSE_PRODUCT = "product";
    public static final String LICENSE_TYPE_ENTERPRISE = "enterprise";
    public static final String LICENSE_TYPE_DEPARTMENT = "department";
    public static final String LICENSE_TYPE_DEVELOPER = "developer";
    public static final String LICENSE_TYPE_EVALUATION = "evaluation";
    public static final long MIN_JAR_SIZE = 50000L;
    public static final int UNKNOWN_CLIENT = -1;
    public static final int DEPLOY_CLIENT = 0;
    public static final int DEPLOY_SERVER = 1;
    public static final int BROWSER_NETSCAPE = 2;
    public static final int BROWSER_MSIE = 3;
    public static final int BROWSER_LYNX = 4;
    public static final int BROWSER_AOL = 5;
    public static final int BROWSER_OPERA = 6;
    public static final int BROWSER_MOSAIC = 7;
    public static final int BROWSER_ON_UNKNOWN = 0;
    public static final int BROWSER_ON_WINDOWS = 1;
    public static final int BROWSER_ON_MAC = 2;
    public static final int BROWSER_ON_X11 = 3;
    public static final String AUTHORIZATION_REALM = "DeployDirector";
    public static final String ADMINNAME = "adminname";
    public static final String ADMINPASSWORD = "adminpassword";
    public static final String USERNAME = "username";
    public static final String USERPASSWORD = "userpassword";
    public static final String USEROBJECT = "userobject";
    public static final String HTTP_HEADER_ID = "DeploySam-ID";
    public static final String HTTP_STATUS_TEXT = "DeploySam-StatusText";
    public static final String HTTP_STATUS_CODE = "DeploySam-StatusCode";
    public static final String HTTP_SEQUENCE = "DD-Sequence";
    public static final String TEXT_UNLIMITED = "(unlimited)";
    public static final int INT_UNLIMITED = -1;
    public static final String TEXT_NEVER = "(never)";
    public static final String TEXT_UNKNOWN = "(unknown)";
    public static final String JRES_DIR = "jres";
    public static final String CAM_DIR = "lib";
    public static final String UPDATE_DIR = "dd";
    public static final String CAM_UPDATE_DIR = "CAMUpdate";
    public static final String APP_UPDATE_DIR = "AppUpdate";
    public static final String CHARTS_DIR = "charts";
    public static final String APPLICATION_PAGE = "application.html";
    public static final String APP_LIST = "bundles.lst";
    public static final String AUTH_OBJECT = "bundle.info";
    public static final String BOOT_JAR = "ddboot.jar";
    public static final String BUNDLE_PROPERTIES = "bundle.properties";
    public static final String CAB_INSTALLER_NAME = "install.cab";
    public static final String CAB_LAUNCHER_NAME = "launch.cab";
    public static final String CACHE_PROPERTIES = "cache.properties";
    public static final String CAM_CONFIG = "ddcam.config";
    public static final String CAM_JAR = "ddcam.jar";
    public static final String CLUSTER_PROPERTIES = "cluster.properties";
    public static final String DEPLOY_LOGO = "deploydirector.gif";
    public static final String DEPLOY_LOGO_SS = "deploydirector_ss.gif";
    public static final String DOWNLOAD_PROPERTIES_NAME = "deploy.properties";
    public static final String INSTALL_CHECKPOINT_NAME = "deploy.chk";
    public static final String JAR_INSTALLER_NAME = "install.jar";
    public static final String JAR_LAUNCHER_NAME = "launch.jar";
    public static final String JRE_PROPERTIES = "jre.properties";
    public static final String JRE_ZIP = "jre.zip";
    public static final String PLATFORM_XML = "platform.xml";
    public static final String PREBUILT_JRE = "prebuilt.zip";
    public static final String SERVER_PROPERTIES = "server.properties";
    public static final String VAULT_NAME = "vault";
    public static final String AUTHDIR_NAME = "auth";
    public static final String GROUP_DATA = "group.data";
    public static final String VERSION_LIST = "versions.lst";
    public static final String VERSION_XML = "version.xml";
    public static final String WIN32_DLL = "deployinstaller.dll";
    public static final String WIN32_JRE_FINDER = "deployjrefinder.exe";
    public static final String WIN32_LAUNCHER_EXE = "launcher.exe";
    public static final String UNIX_JRE_FINDER = "deployjrefinder";
    public static final String DEFAULT_LOG_DIR = "logs";
    public static final String DDADMIN_HELP_JAR = "ddonlinehelp.jar";
    public static final String DDADMIN_HELP_HS = "ddonlinehelp.hs";
    public static final String APP_DONTASKME = "dontaskme.lst";
    public static final String BUNDLE_NAME = "bundlename.txt";
    public static final String TEMP_CAM_JAR = "tempcam.jar";
    public static final String LAUNCHER_DAT = "launcher.dat";
    public static final String LAUNCHER_EXE = "launcher.exe";
    public static final String UPDATE_EXE = "update.exe";
    public static final String META_DIR = "META-INF";
    public static final String WAR_MANIFEST = "Manifest.mf";
    public static final String WEB_DIR = "WEB-INF";
    public static final String UPDATING_DAT = "updating.dat";
    public static final String ENTRYPOINTS_DAT = "entrys.dat";
    public static final String COOKIE_FILE = "cookies.dat";
    public static final String CAM_SPLASH_SCREEN = "splash.gif";
    public static final String CLIENT_DATABASE = "DDClients";
    public static final String CLIENT_LOG_FILE = "DDClientLog";
    public static final String LOAD_LOG_FILE = "DDLoadLog";
    public static final String SERVER_LOG_FILE = "DDServerLog";
    public static final String CAM_NAME = "DeployCam";
    public static final String CAM_APP_NAME = "DDCAM";
    public static final String CAM_VERSION = "2.5.0";
    public static final String SAM_NAME = "DeploySam";
    public static final String SAM_VERSION = "2.5.0";
    public static final String SDK_NAME = "DDSDK";
    public static final String SDK_VERSION = "2.5.0";
    public static final String ADMIN_CONFIG = "admin.config";
    public static final String ADMIN_BUILD_DIR = "admin.buildDirectory";
    public static final String ADMIN_SOURCE_DIR = "admin.sourceDirectory";
    public static final String ADMIN_LAST_SERVER = "admin.lastServer";
    public static final String ADMIN_SERVER_LIST = "admin.server";
    public static final String ADMIN_SERVER_LIST_NAME = "admin.serverName";
    public static final String ADMIN_SERVER_GROUP = "admin.group";
    public static final String ADMIN_NAME = "DeployAdmin";
    public static final String ADMIN_APP_NAME = "DDAdmin";
    public static final String ADMIN_VERSION = "2.5.0";
    public static final String INSTALL_DIR = "$(INSTALLDIR)";
    public static final String JAVA_HOME = "$(JAVAHOME)";
    public static final String INSTALLATION_FAILED = "INSTALLATION FAILED";
    public static final String INSTALLATION_SUCCEEDED = "INSTALLATION SUCCEEDED";
    public static final int CAM_EXIT_RESTART = 223;
    public static final int CAM_EXIT_OK = 222;
    public static final int CAM_EXIT_NAUGHTY = 221;
    public static final String CAM_OPTION_RUN = "DeployCamRun";
    public static final String CAM_OPTION_INSTALL = "DeployCamInstall";
    public static final String CAM_OPTION_UNINSTALL = "DeployCamUnInstall";
    public static final String CAM_OPTION_COUNTAPPS = "DeployCamCountApps";
    public static final String CAM_OPTION_VERSION = "DeployCamVersion";
    public static final String CAM_OPTION_UPDATE = "DeployCamUpdate";
    public static final String CAM_OPTION_FORCEUPDATE = "DeployCamForceUpdate";
    public static final String CAM_OPTION_HELP = "DeployCamHelp";
    public static final String CAM_OPTION_GUI = "DeployCamGui";
    public static final String CAM_OPTION_BATCH = "DeployCamBatch";
    public static final String CAM_OPTION_CAM_UPDATE_PENDING = "DeployCamCamUpdatePending";
    public static final String CAM_OPTION_BUNDLE_UPDATE_PENDING = "DeployCamBundleUpdatePending";
    public static final String DEPLOY_CAM_BUSYWAIT = "deploy.cam.busywait";
    public static final int CONNECT_ONSTARTUP = 1;
    public static final int CONNECT_SCHEDULED = 2;
    public static final int CONNECT_USERINITIATED = 3;
    public static final int CONNECTION_POLICY_VALUES[] = {
        1, 2, 3
    };
    public static final String CONNECTION_POLICY_NAMES[] = {
        "onstartup", "scheduled", "userinitiated"
    };
    public static final String CONNECTION_POLICY_DISPLAY_NAMES[] = {
        "On Startup", "Scheduled", "User Initiated"
    };
    public static final int ACCESS_MUSTCONNECT = 1;
    public static final int ACCESS_PREFERREDCONNECT = 2;
    public static final int ACCESS_POLICY_VALUES[] = {
        1, 2
    };
    public static final String ACCESS_POLICY_NAMES[] = {
        "mustconnect", "preferredconnect"
    };
    public static final String ACCESS_POLICY_DISPLAY_NAMES[] = {
        "Required", "Preferred"
    };
    public static final int MANDATORY = 1;
    public static final int SCHEDULEDMANDATORY = 2;
    public static final int OPTIONAL = 3;
    public static final int UPDATE_POLICY_VALUES[] = {
        1, 2, 3
    };
    public static final String UPDATE_POLICY_NAMES[] = {
        "mandatory", "scheduledmandatory", "optional"
    };
    public static final String UPDATE_POLICY_DISPLAY_NAMES[] = {
        "Mandatory", "Scheduled Mandatory", "Optional"
    };

    public CommonEnums()
    {
    }

    public static void main(String args[])
    {
        System.out.println("deploy.version=2.5.0");
        System.out.println("deploy.license.version=2.5");
        System.out.println("deploy.sdk.version=2.5.0");
        System.out.println("deploy.sdk.name=DDSDK");
        System.out.println("deploy.sam.version=2.5.0");
        System.out.println("deploy.sam.name=DeploySam");
        System.out.println("deploy.cam.version=2.5.0");
        System.out.println("deploy.cam.name=DeployCam");
        System.out.println("deploy.cam.app_name=DDCAM");
        System.out.println("deploy.admin.version=2.5.0");
        System.out.println("deploy.admin.name=DeployAdmin");
        System.out.println("deploy.admin.app_name=DDAdmin");
    }

}
