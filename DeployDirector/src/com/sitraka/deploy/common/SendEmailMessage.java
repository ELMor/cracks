// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SendEmailMessage.java

package com.sitraka.deploy.common;

import java.util.Hashtable;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEmailMessage
{

    public SendEmailMessage()
    {
    }

    public static void sendMessage(String mail_host_name, String subject, String from_user, String from_password, String from_address, String to_addresses[], String message)
        throws MessagingException
    {
        sendMessage(((String) (null)), mail_host_name, subject, from_user, from_password, from_address, to_addresses, message);
    }

    public static void sendMessage(String mail_host_type, String mail_host_name, String subject, String from_user, String from_password, String from_address, String to_addresses[], String message)
        throws MessagingException
    {
        if(mail_host_type == null)
            mail_host_type = "mail.smtp.host";
        Properties props = new Properties();
        ((Hashtable) (props)).put(((Object) (mail_host_type)), ((Object) (mail_host_name)));
        Session session = Session.getDefaultInstance(props, ((javax.mail.Authenticator) (null)));
        Message msg = ((Message) (new MimeMessage(session)));
        msg.setFrom(((javax.mail.Address) (new InternetAddress(from_address))));
        InternetAddress addresses[] = new InternetAddress[to_addresses.length];
        for(int i = 0; i < addresses.length; i++)
            addresses[i] = new InternetAddress(to_addresses[i]);

        msg.setRecipients(javax.mail.Message.RecipientType.TO, ((javax.mail.Address []) (addresses)));
        msg.setSubject(subject);
        if(from_user == null);
        if(from_password == null);
        msg.setContent(((Object) (message)), "text/plain");
        Transport.send(msg);
    }
}
