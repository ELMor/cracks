// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   XmlSupport.java

package com.sitraka.deploy.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringReader;
import org.apache.xerces.dom.TreeWalkerImpl;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.TreeWalker;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class XmlSupport
{
    public static class ErrorPrinter
        implements ErrorHandler
    {

        private void message(String level, SAXParseException e)
            throws SAXException
        {
            throw new SAXException("XML Error: An invalid XML attribute was detected '" + ((SAXException) (e)).getMessage() + "'");
        }

        public void error(SAXParseException e)
            throws SAXException
        {
            message("Error (recoverable)", e);
        }

        public void warning(SAXParseException e)
            throws SAXException
        {
            message("Warning", e);
        }

        public void fatalError(SAXParseException e)
            throws SAXException
        {
            message("FATAL ERROR", e);
        }

        public ErrorPrinter()
        {
        }
    }

    public static class DTDStringResolver
        implements EntityResolver
    {

        String dtdString;
        String dtdName;

        public InputSource resolveEntity(String publicId, String systemId)
            throws SAXException, IOException
        {
            if(systemId.endsWith(dtdName))
            {
                java.io.Reader reader = null;
                reader = ((java.io.Reader) (new StringReader(dtdString)));
                return new InputSource(reader);
            } else
            {
                return null;
            }
        }

        public DTDStringResolver(String dtdString, String dtdName)
        {
            this.dtdString = dtdString;
            this.dtdName = dtdName;
        }
    }


    public XmlSupport()
    {
    }

    public static String getAttribute(Node node, String tag)
    {
        NamedNodeMap attributes = node.getAttributes();
        Node attr_node = attributes.getNamedItem(tag);
        if(attr_node == null)
            return null;
        else
            return attr_node.getNodeValue();
    }

    public static String getTagValue(Node node)
    {
        TreeWalker tw = createTreeWalker(node);
        StringBuffer sb = new StringBuffer();
        while((node = tw.nextNode()) != null) 
            sb.append(node.getNodeValue());
        return sb.toString();
    }

    public static InputSource createInputSource(File xmlFile)
        throws IOException
    {
        InputSource inputSource = new InputSource();
        inputSource.setCharacterStream(((java.io.Reader) (new InputStreamReader(((InputStream) (new FileInputStream(xmlFile))), "UTF8"))));
        String path = xmlFile.getAbsolutePath();
        if(File.separatorChar != '/')
            path = path.replace(File.separatorChar, '/');
        if(!path.startsWith("/"))
            path = "/" + path;
        if(!path.endsWith("/") && xmlFile.isDirectory())
            path = path + "/";
        inputSource.setSystemId("file:" + path);
        inputSource.setEncoding("UTF-8");
        return inputSource;
    }

    public static InputSource createInputSource(InputStream stream)
        throws IOException
    {
        InputSource inputSource = new InputSource();
        inputSource.setCharacterStream(((java.io.Reader) (new InputStreamReader(stream, "UTF8"))));
        inputSource.setEncoding("UTF-8");
        return inputSource;
    }

    public static TreeWalker createTreeWalker(Node root)
    {
        return ((TreeWalker) (new TreeWalkerImpl(root, -1, ((org.w3c.dom.traversal.NodeFilter) (null)), false)));
    }

    public static Node getNextElementNamed(TreeWalker treeWalker, String elementName)
    {
        Node node;
        for(node = treeWalker.nextNode(); node != null; node = treeWalker.nextNode())
            if(node.getNodeType() == 1 && node.getNodeName().equals(((Object) (elementName))))
                break;

        return node;
    }

    public static String toCDATA(String string)
    {
        if(string == null)
            return "";
        int size = string.length();
        StringBuffer buffer = new StringBuffer(size * 2);
        for(int count = 0; count < size; count++)
        {
            char c = string.charAt(count);
            if(c == '\t' || c == '\n' || c == '\r' || c >= ' ' && c <= '\uD7FF' || c >= '\uE000' && c <= '\uFFFD' || c >= '\0' && c <= '\0')
            {
                switch(c)
                {
                case 60: // '<'
                    buffer.append("&lt;");
                    break;

                case 62: // '>'
                    buffer.append("&gt;");
                    break;

                case 38: // '&'
                    buffer.append("&amp;");
                    break;

                case 39: // '\''
                    buffer.append("&apos;");
                    break;

                case 34: // '"'
                    buffer.append("&quot;");
                    break;

                default:
                    buffer.append(c);
                    break;
                }
            } else
            {
                buffer.append("&#x");
                buffer.append(Integer.toHexString(((int) (c))));
                buffer.append(";");
            }
        }

        return buffer.toString();
    }

    public static String fromCDATA(String string)
    {
        String charTypes[] = {
            "lt;", "gt;", "amp;", "apos;", "quot;"
        };
        String typesChar[] = {
            "<", ">", "&", "'", "\""
        };
        StringBuffer outString = new StringBuffer(100);
        int index = string.indexOf("&");
        int lastIndex = 0;
        for(; index != -1; index = string.indexOf("&", index))
        {
            index++;
            for(int i = 0; i < charTypes.length; i++)
            {
                if(!string.startsWith(charTypes[i], index))
                    continue;
                outString.append(string.substring(lastIndex, index - 1));
                outString.append(typesChar[i]);
                index += charTypes[i].length();
                lastIndex = index;
                break;
            }

        }

        outString.append(string.substring(lastIndex, string.length()));
        return outString.toString();
    }

    public static void main(String args[])
    {
        String test = "& &cow; &amp; &lt; &gt; &apos; &quot; &amp &lt &gt &apos &quot &";
        String out = fromCDATA(test);
        System.out.println(out);
    }
}
