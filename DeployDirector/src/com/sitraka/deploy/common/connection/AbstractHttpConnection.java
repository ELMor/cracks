// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AbstractHttpConnection.java

package com.sitraka.deploy.common.connection;

import com.sitraka.deploy.ClientAuthentication;
import com.sitraka.deploy.common.Requests;
import com.sitraka.deploy.common.awt.BusyWait;
import com.sitraka.deploy.common.awt.MessageBox;
import com.sitraka.deploy.common.awt.resources.LocaleInfo;
import com.sitraka.deploy.common.jclass.util.JCListenerList;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.common.connection:
//            HttpConnectionListener, ConnectionInterface

public abstract class AbstractHttpConnection
    implements Requests, ConnectionInterface
{

    protected String server;
    protected Vector server_list;
    protected String last_server;
    protected String data_message;
    protected int content_length;
    protected JCListenerList listeners;
    protected Object user_id;
    protected Object password;
    protected Object user_object;
    protected static String proxyUsername = null;
    protected static String proxyPassword = null;
    protected int proxyRetries;
    protected String requestTicketID;
    protected String requestTicketText;
    protected int requestTicketCode;
    protected Hashtable header_table;
    protected boolean is_admin;
    protected int response_code;
    protected Integer RESPONSE_CODE_LOCK;
    protected static final int RETRY_COUNT = 3;
    protected int retryTimeout;
    protected ClientAuthentication clientAuthentication;
    protected boolean useGUI;
    public static final String TIMEOUT_ERROR = "DDTimeoutError";
    public static final String TIMEOUT_MESSAGE = "DDTimeoutMessage";
    public static final int PROXY_AUTHENTICATION_REQUIRED = 407;
    protected static final String PROXY_USERNAME_PASSWORD_FILENAME = "secauth.dat";
    protected static boolean DEBUG_ON = false;
    protected static boolean VERBOSE_DEBUG_ON = false;
    protected static PrintStream logFile;

    protected static void initLogFile()
    {
        if(logFile == System.out)
        {
            String logFileName = System.getProperty("deploy.debug_httplog");
            if(logFileName == null)
                logFile = System.out;
            else
                try
                {
                    logFile = new PrintStream(((OutputStream) (new FileOutputStream(logFileName))));
                }
                catch(IOException ioe)
                {
                    logFile = System.out;
                }
        }
    }

    public AbstractHttpConnection(String server_url)
    {
        server = null;
        server_list = null;
        last_server = null;
        data_message = null;
        content_length = -1;
        listeners = null;
        user_id = null;
        password = null;
        user_object = null;
        proxyRetries = 0;
        requestTicketID = null;
        requestTicketText = null;
        requestTicketCode = -1;
        header_table = null;
        is_admin = false;
        response_code = -1;
        RESPONSE_CODE_LOCK = new Integer(-1);
        retryTimeout = 1500;
        clientAuthentication = null;
        useGUI = false;
        server = server_url;
        header_table = new Hashtable();
    }

    public AbstractHttpConnection(URL server_url)
    {
        this(server_url != null ? server_url.toExternalForm() : null);
    }

    public AbstractHttpConnection()
    {
        this((String)null);
    }

    public void resetTags()
    {
        header_table = new Hashtable();
        content_length = -1;
    }

    public String getLastServer()
    {
        return last_server;
    }

    public void setRetryTimeout(int timeout)
    {
        retryTimeout = timeout;
    }

    public void setIsAdmin(boolean _admin)
    {
        is_admin = _admin;
    }

    public void setClientAuthentication(ClientAuthentication clientAuthentication)
    {
        this.clientAuthentication = clientAuthentication;
    }

    public ClientAuthentication getClientAuthentication()
    {
        return clientAuthentication;
    }

    public void setApplication(String application)
    {
        if(application == null)
            header_table.remove("BUNDLE");
        else
            header_table.put("BUNDLE", ((Object) (application)));
    }

    public String getApplication()
    {
        return (String)header_table.get("BUNDLE");
    }

    public void setVersion(String version)
    {
        if(version == null)
            header_table.remove("VERSION");
        else
            header_table.put("VERSION", ((Object) (version)));
    }

    public String getVersion()
    {
        return (String)header_table.get("VERSION");
    }

    public void setPlatform(String platform_name)
    {
        if(platform_name == null)
            header_table.remove("PLATFORM");
        else
            header_table.put("PLATFORM", ((Object) (platform_name)));
    }

    public void setFilename(String files)
    {
        if(files == null)
            header_table.remove("FILE");
        else
            header_table.put("FILE", ((Object) (files)));
    }

    public void setFromVersion(String from)
    {
        if(from == null)
            header_table.remove("FROM");
        else
            header_table.put("FROM", ((Object) (from)));
    }

    public void setToVersion(String to)
    {
        if(to == null)
            header_table.remove("TO");
        else
            header_table.put("TO", ((Object) (to)));
    }

    public void setVendor(String vendor)
    {
        if(vendor == null)
            header_table.remove("VENDOR");
        else
            header_table.put("VENDOR", ((Object) (vendor)));
    }

    public void setName(String name)
    {
        if(name == null)
            header_table.remove("NAME");
        else
            header_table.put("NAME", ((Object) (name)));
    }

    public void setFormat(String format)
    {
        if(format == null)
            header_table.remove("FORMAT");
        else
            header_table.put("FORMAT", ((Object) (format)));
    }

    public void setClassPaths(String classpaths)
    {
        if(classpaths == null)
            header_table.remove("CLASSPATHS");
        else
            header_table.put("CLASSPATHS", ((Object) (classpaths)));
    }

    public void setCommand(String command)
    {
        if(command == null)
            header_table.remove("COMMAND");
        else
            header_table.put("COMMAND", ((Object) (command)));
    }

    public String getCommand()
    {
        return (String)header_table.get("COMMAND");
    }

    public String getRequestTicketText()
    {
        return requestTicketText;
    }

    public void setClientID(String id)
    {
        if(id == null)
            header_table.remove("CLIENTIDENT");
        else
            header_table.put("CLIENTIDENT", ((Object) (id)));
    }

    public void setMessage(String message)
    {
        if(message == null)
            header_table.remove("MESSAGE");
        else
            header_table.put("MESSAGE", ((Object) (message)));
    }

    public void setTextMessage(String text)
    {
        data_message = text;
    }

    public void setServer(String servername)
    {
        server = servername;
        last_server = null;
        server_list = null;
    }

    public void setServerParam(String id)
    {
        if(id == null)
            header_table.remove("SERVER");
        else
            header_table.put("SERVER", ((Object) (id)));
    }

    public void setServerList(Vector new_list)
    {
        server_list = new_list;
    }

    public void setServerList(String new_list)
    {
        setServerList(PropertyUtils.readVector(new_list, ','));
    }

    public void setContentLength(int length)
    {
        content_length = length;
    }

    public int getContentLength()
    {
        return content_length;
    }

    public void setUserID(Object id)
    {
        user_id = id;
    }

    public void setUserIDObject(Object id)
    {
        if(id == null)
        {
            user_object = null;
            return;
        }
        if((id instanceof String) && ((String)id).indexOf(":") != -1)
        {
            String auth = (String)id;
            int index = auth.indexOf(":");
            user_id = ((Object) (auth.substring(0, index)));
            password = ((Object) (auth.substring(index + 1)));
            user_object = null;
        } else
        {
            user_id = null;
            password = null;
            user_object = id;
        }
    }

    public void setUserPassword(Object password)
    {
        this.password = password;
    }

    public void setProxyUsername(String id)
    {
        if(id == null)
            proxyUsername = (String)null;
        proxyUsername = id;
    }

    public String getProxyUsername()
    {
        if(proxyUsername == null)
            return (String)null;
        else
            return proxyUsername;
    }

    public void setProxyPassword(String pwd)
    {
        if(pwd == null)
            proxyPassword = (String)null;
        proxyPassword = pwd;
    }

    public String getProxyPassword()
    {
        if(proxyPassword == null)
            return (String)null;
        else
            return proxyPassword;
    }

    public void setOffset(long offset)
    {
        if(offset == -1L)
            header_table.remove("OFFSET");
        else
            header_table.put("OFFSET", ((Object) ("" + offset)));
    }

    public InputStream executeCommand(String command)
    {
        setCommand(command);
        return executeCommand();
    }

    public InputStream executeCommand()
    {
        if(server_list == null && server == null)
            return null;
        InputStream is = null;
        if(server != null)
        {
            is = executeCommandInternalRetry(server);
            if(is != null)
            {
                last_server = server;
                return is;
            }
        }
        if(last_server != null)
        {
            is = executeCommandInternalRetry(last_server);
            if(is != null)
                return is;
        }
        int size = server_list != null ? server_list.size() : 0;
        for(int i = 0; i < size; i++)
        {
            String current_server = (String)server_list.elementAt(i);
            is = executeCommandInternalRetry(current_server);
            if(is != null)
            {
                last_server = current_server;
                return is;
            }
        }

        if(getUseGUI())
        {
            int respCode = getResponseCode();
            if(respCode == -1)
            {
                StringBuffer servers = new StringBuffer();
                for(int i = 0; i < size; i++)
                {
                    servers.append("\n\t");
                    servers.append((String)server_list.elementAt(i));
                }

                Object args[] = {
                    servers
                };
                String text = MessageFormat.format(LocaleInfo.li.getString("DDTimeoutMessage"), args);
                BusyWait.hide();
                MessageBox.showError(LocaleInfo.li.getString("DDCAError"), text, true);
                BusyWait.show();
            } else
            {
                BusyWait.hide();
                MessageBox.showError(LocaleInfo.li.getString("DDCAError"), getResponseExplanation(respCode), true);
                BusyWait.show();
            }
        }
        return null;
    }

    public void addConnectionListener(HttpConnectionListener l)
    {
        listeners = JCListenerList.add(listeners, ((Object) (l)));
    }

    public void removeConnectionListener(HttpConnectionListener l)
    {
        listeners = JCListenerList.remove(listeners, ((Object) (l)));
    }

    public InputStream executeCommandInternalRetry(String nextServer)
    {
        InputStream is = null;
        int connectAttemptCount = 0;
        int authenticateAttemptCount = 0;
        Object originalInfo = null;
        while(connectAttemptCount < 3 && authenticateAttemptCount < 3) 
        {
            is = executeCommandInternal(nextServer);
            if(!isConnected())
            {
                if(VERBOSE_DEBUG_ON)
                    logFile.println("Could not connect to " + nextServer + " (retry " + (connectAttemptCount + 1) + " of " + 3 + ")");
                connectAttemptCount++;
                continue;
            }
            while(getRequestTicketText() != null && isConnected()) 
            {
                if(VERBOSE_DEBUG_ON && (requestTicketCode == 4 || requestTicketCode == 3))
                    logFile.println("Request Ticket loop continuing even though ticket code specified " + (requestTicketCode != 4 ? "COMPLETED" : "ABORTED"));
                if(VERBOSE_DEBUG_ON)
                {
                    logFile.println("HttpConnection successful, but received ticket with text: " + requestTicketText);
                    logFile.println("\t waiting " + retryTimeout + " ms before trying again");
                }
                if(requestTicketCode == 4)
                {
                    if(VERBOSE_DEBUG_ON)
                        logFile.println("Ticket loop:  got ABORTED, exiting");
                    setResponseCode(404);
                    return null;
                }
                HttpConnectionListener listener;
                for(Enumeration e = JCListenerList.elements(listeners); e.hasMoreElements(); listener.connectionStatusUpdated(getRequestTicketText(), isConnected()))
                    listener = (HttpConnectionListener)e.nextElement();

                doTheWait();
                is = executeCommandInternal(nextServer);
            }
            if(VERBOSE_DEBUG_ON)
                if(!isConnected())
                {
                    logFile.println("Ticket loop terminated:  No connection");
                    logFile.println("Last message was: " + requestTicketText);
                } else
                {
                    if(requestTicketText != null)
                        logFile.println("Ticket loop terminated with text:  " + requestTicketText);
                    else
                        logFile.println("Ticket loop terminated:  null string");
                    if(requestTicketCode == 4)
                        logFile.println("Final return code:  ABORTED");
                    else
                    if(requestTicketCode == 3)
                        logFile.println("Final return code:  COMPLETED");
                    else
                        logFile.println("Unexpected final return code:  " + requestTicketCode);
                }
            requestTicketID = null;
            if(!isConnected())
                continue;
            if(VERBOSE_DEBUG_ON)
                logFile.println("Successfully connected to " + nextServer);
            if(is != null)
                return is;
            if(getResponseCode() == 401)
            {
                BusyWait.hide();
                Object newInfo = authenticateClient();
                BusyWait.show();
                if(newInfo == null)
                    break;
                if(newInfo.equals(originalInfo))
                {
                    authenticateAttemptCount++;
                } else
                {
                    authenticateAttemptCount = 1;
                    originalInfo = newInfo;
                }
                continue;
            }
            if(getResponseCode() != 407)
                break;
        }
        return null;
    }

    protected synchronized void doTheWait()
    {
        try
        {
            ((Object)this).wait(retryTimeout);
        }
        catch(InterruptedException e) { }
    }

    public String executeTextCommand(String command)
    {
        setCommand(command);
        return executeTextCommand();
    }

    public String executeTextCommand()
    {
        InputStream is = executeCommand();
        if(is == null)
            return null;
        StringBuffer result = new StringBuffer();
        try
        {
            byte data[] = new byte[1024];
            for(int n = -1; (n = is.read(data, 0, 1024)) != -1;)
                result.append(new String(data, 0, n));

            is.close();
        }
        catch(IOException e)
        {
            return null;
        }
        return result.toString();
    }

    public OutputStream executeUploadCommand(String command)
    {
        setCommand(command);
        return executeUploadCommand();
    }

    public OutputStream executeUploadCommand()
    {
        if(server_list == null && server == null)
            return null;
        OutputStream os = null;
        if(server != null)
        {
            os = executeUploadCommandInternalRetry(server);
            if(os != null)
            {
                last_server = server;
                return os;
            }
        }
        if(last_server != null)
        {
            os = executeUploadCommandInternalRetry(last_server);
            if(os != null)
                return os;
        }
        int size = server_list != null ? server_list.size() : 0;
        for(int i = 0; i < size; i++)
        {
            String current_server = (String)server_list.elementAt(i);
            os = executeUploadCommandInternalRetry(current_server);
            if(os != null)
            {
                last_server = current_server;
                return os;
            }
        }

        return null;
    }

    public OutputStream executeUploadCommandInternalRetry(String nextServer)
    {
        OutputStream os = null;
        int connectAttemptCount = 0;
        int authenticateAttemptCount = 0;
        Object originalInfo = null;
        while(connectAttemptCount < 3 && authenticateAttemptCount < 3) 
        {
            os = executeUploadCommandInternal(nextServer);
            if(os != null)
                return os;
            if(getResponseCode() == 401)
            {
                Object newInfo = authenticateClient();
                if(newInfo == null)
                    break;
                if(newInfo.equals(originalInfo))
                {
                    authenticateAttemptCount++;
                } else
                {
                    authenticateAttemptCount = 1;
                    originalInfo = newInfo;
                }
            } else
            {
                connectAttemptCount++;
            }
        }
        return null;
    }

    protected Object authenticateClient()
    {
        ClientAuthentication clientAuth = getClientAuthentication();
        if(clientAuth == null)
            return ((Object) (null));
        clientAuth.setAuthenticationInfoAvailable(false);
        Object clientAuthenticationInfo = clientAuth.getAuthenticationInfo();
        if(!clientAuth.isAuthenticationInfoAvailable())
        {
            return ((Object) (null));
        } else
        {
            setUserIDObject(clientAuthenticationInfo);
            return clientAuthenticationInfo;
        }
    }

    public String getResponseExplanation()
    {
        return getResponseExplanation(getResponseCode());
    }

    public String getResponseExplanation(int code)
    {
        StringBuffer buffer = new StringBuffer();
        if(code <= 199)
            buffer.append("Information: ");
        else
        if(code <= 299)
            buffer.append("Success: ");
        else
        if(code <= 399)
            buffer.append("Redirection: ");
        else
        if(code <= 499)
            buffer.append("Client Error: ");
        else
        if(code <= 599)
            buffer.append("Server Error: ");
        else
            buffer.append("Unknown (invalid) Error Code: ");
        switch(code)
        {
        case -1: 
            buffer.append("No server(s) could be contacted. This request has timed out. ");
            buffer.append("No detailed response code is available.");
            break;

        case 200: 
            buffer.append("Request was sucessfully completed");
            break;

        case 401: 
            buffer.append("Access was not authorized");
            break;

        case 404: 
            buffer.append("The requested resource is not available");
            break;

        case 407: 
            buffer.append("Client must first authenticate itself with the proxy");
            break;

        case 408: 
            buffer.append("Client timed out");
            break;

        case 411: 
            buffer.append("Badly formatted request sent to server.\n");
            buffer.append("(Content-length required by server to process request)");
            break;

        case 414: 
            buffer.append("server is refusing to service the request because the");
            buffer.append(" Request-URI is longer than the server is willing to interpret");
            break;

        case 500: 
            buffer.append("An error inside the server has prevented it from");
            buffer.append(" fulfilling the request");
            break;

        case 501: 
            buffer.append("Server does not support the functionality needed to");
            buffer.append(" fulfill the request");
            break;

        case 503: 
            buffer.append("Server is overloaded and temporarily unable to handle the request");
            break;

        case 505: 
            buffer.append("Server does not support or refuses to accept the HTTP ");
            buffer.append(" version that was used in the request message");
            break;

        default:
            buffer.append("Code " + code + ", no description available");
            break;
        }
        return buffer.toString();
    }

    public boolean isErrorCode()
    {
        boolean flag1;
        synchronized(RESPONSE_CODE_LOCK)
        {
            if(response_code >= 300)
            {
                boolean flag = true;
                return flag;
            }
            flag1 = false;
        }
        return flag1;
    }

    public boolean isConnected()
    {
        boolean flag;
        synchronized(RESPONSE_CODE_LOCK)
        {
            flag = response_code != -1;
        }
        return flag;
    }

    public boolean isSuccessful()
    {
        return isConnected() && !isErrorCode();
    }

    public int getResponseCode()
    {
        int code = -1;
        synchronized(RESPONSE_CODE_LOCK)
        {
            code = response_code;
        }
        return code;
    }

    protected void setResponseCode(int new_code)
    {
        synchronized(RESPONSE_CODE_LOCK)
        {
            response_code = new_code;
        }
    }

    public abstract int finishUploadCommand();

    protected abstract InputStream executeCommandInternal(String s);

    protected abstract OutputStream executeUploadCommandInternal(String s);

    protected String[][] getParameters()
    {
        if(header_table.get("COMMAND") == null)
            return null;
        String header[][] = new String[header_table.size() + 1][2];
        Enumeration keys = header_table.keys();
        int i;
        for(i = 0; keys.hasMoreElements(); i++)
        {
            String name = (String)keys.nextElement();
            header[i][0] = name;
            header[i][1] = FileUtils.makeInternalPath((String)header_table.get(((Object) (name))));
        }

        header[i][0] = "ADMINCOMMAND";
        if(is_admin)
            header[i][1] = "true";
        else
            header[i][1] = "false";
        return header;
    }

    protected String createUserIDString()
    {
        String userName = "";
        if(user_object == null)
        {
            if(user_id == null)
                user_id = ((Object) (userName));
            return (String)user_id;
        }
        if(user_object instanceof String)
            return (String)user_object;
        if(!(user_object instanceof Serializable))
        {
            if(user_id == null)
                user_id = ((Object) (userName));
            return (String)user_id;
        }
        try
        {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(((OutputStream) (bos)));
            os.writeObject(user_object);
            return bos.toString();
        }
        catch(IOException e)
        {
            return userName;
        }
    }

    protected String createPasswordString()
    {
        if(user_object != null)
            return new String("");
        if(password == null)
            password = "";
        return (String)password;
    }

    public boolean getUseGUI()
    {
        return useGUI;
    }

    public void setUseGUI(boolean show)
    {
        useGUI = show;
    }

    static 
    {
        logFile = System.out;
        try
        {
            String is_debug = System.getProperty("deploy.debug");
            if(is_debug != null && is_debug.indexOf("httpconnection") != -1)
            {
                DEBUG_ON = true;
                initLogFile();
            } else
            {
                DEBUG_ON = false;
            }
            if(is_debug != null && is_debug.indexOf("verbosehttpconnection") != -1)
            {
                VERBOSE_DEBUG_ON = true;
                initLogFile();
            } else
            {
                VERBOSE_DEBUG_ON = false;
            }
        }
        catch(SecurityException e) { }
    }
}
