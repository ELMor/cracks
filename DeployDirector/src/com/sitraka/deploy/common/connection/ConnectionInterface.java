// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ConnectionInterface.java

package com.sitraka.deploy.common.connection;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

public interface ConnectionInterface
{

    public abstract void resetTags();

    public abstract void setApplication(String s);

    public abstract void setVersion(String s);

    public abstract void setPlatform(String s);

    public abstract void setFilename(String s);

    public abstract void setFromVersion(String s);

    public abstract void setToVersion(String s);

    public abstract void setVendor(String s);

    public abstract void setCommand(String s);

    public abstract void setClientID(String s);

    public abstract void setMessage(String s);

    public abstract void setTextMessage(String s);

    public abstract void setServer(String s);

    public abstract void setServerList(Vector vector);

    public abstract void setUserID(Object obj);

    public abstract void setUserIDObject(Object obj);

    public abstract void setUserPassword(Object obj);

    public abstract void setProxyUsername(String s);

    public abstract String getProxyUsername();

    public abstract void setProxyPassword(String s);

    public abstract String getProxyPassword();

    public abstract int getResponseCode();

    public abstract void setOffset(long l);

    public abstract String getResponseExplanation(int i);

    public abstract InputStream executeCommand(String s);

    public abstract InputStream executeCommand();

    public abstract String executeTextCommand(String s);

    public abstract String executeTextCommand();

    public abstract OutputStream executeUploadCommand(String s);

    public abstract OutputStream executeUploadCommand();

    public abstract int finishUploadCommand();
}
