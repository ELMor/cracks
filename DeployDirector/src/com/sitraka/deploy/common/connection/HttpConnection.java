// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   HttpConnection.java

package com.sitraka.deploy.common.connection;

import HTTPClient.AuthorizationInfo;
import HTTPClient.Codecs;
import HTTPClient.HTTPConnection;
import HTTPClient.HTTPResponse;
import HTTPClient.HttpOutputStream;
import HTTPClient.ModuleException;
import HTTPClient.NVPair;
import HTTPClient.ProtocolNotSuppException;
import HTTPClient.URI;
import com.sitraka.deploy.authentication.ProxyUsernamePasswordEditor;
import com.sitraka.deploy.common.jclass.util.JCListenerList;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.URL;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

// Referenced classes of package com.sitraka.deploy.common.connection:
//            AbstractHttpConnection, HttpConnectionListener

public class HttpConnection extends AbstractHttpConnection
{

    protected HTTPResponse response;
    protected HttpOutputStream o_stream;
    protected static Hashtable connectionPool = new Hashtable();

    public HttpConnection()
    {
        response = null;
        o_stream = null;
    }

    public int finishUploadCommand()
    {
        int status = -1;
        if(response == null)
        {
            if(AbstractHttpConnection.DEBUG_ON)
                AbstractHttpConnection.logFile.println("Response is NULL!");
            return -1;
        }
        try
        {
            ((AbstractHttpConnection)this).setResponseCode(-1);
            super.requestTicketID = null;
            super.requestTicketText = null;
            super.requestTicketCode = -1;
            ((OutputStream) (o_stream)).flush();
            o_stream.close();
            status = response.getStatusCode();
            ((AbstractHttpConnection)this).setResponseCode(status);
            if(AbstractHttpConnection.VERBOSE_DEBUG_ON)
            {
                String head;
                String val;
                for(Enumeration e = response.listHeaders(); e.hasMoreElements(); AbstractHttpConnection.logFile.println("Returned Header [" + head + "] = [" + val + "]"))
                {
                    head = (String)e.nextElement();
                    val = response.getHeader(head);
                }

            }
            if(AbstractHttpConnection.DEBUG_ON)
            {
                long now = System.currentTimeMillis();
                AbstractHttpConnection.logFile.println("Connection (upload) completed: ");
                AbstractHttpConnection.logFile.println("\tat " + new Date(now) + "(" + now + ")");
                AbstractHttpConnection.logFile.println("\tresponse code " + status + " " + ((AbstractHttpConnection)this).getResponseExplanation(status));
            }
            if(AbstractHttpConnection.DEBUG_ON || AbstractHttpConnection.VERBOSE_DEBUG_ON)
                AbstractHttpConnection.logFile.println("-------------------------------------------------------");
            if(response.getHeader("DeploySam-ID") != null)
            {
                super.requestTicketID = response.getHeader("DeploySam-ID");
                super.requestTicketText = response.getHeader("DeploySam-StatusText");
                try
                {
                    super.requestTicketCode = response.getHeaderAsInt("DeploySam-StatusCode");
                }
                catch(NumberFormatException nfe)
                {
                    super.requestTicketCode = super.requestTicketID != null ? 2 : -1;
                }
                pollForStatus(super.requestTicketID, super.last_server);
                status = ((AbstractHttpConnection)this).getResponseCode();
            }
            HttpConnectionListener listener;
            for(Enumeration e = JCListenerList.elements(super.listeners); e.hasMoreElements(); listener.connectionSuccessful(super.last_server))
                listener = (HttpConnectionListener)e.nextElement();

        }
        catch(ModuleException e) { }
        catch(IOException e) { }
        o_stream = null;
        response = null;
        return status;
    }

    protected void pollForStatus(String id, String server)
    {
        String orig_command = ((AbstractHttpConnection)this).getCommand();
        ((AbstractHttpConnection)this).setCommand("UPLOADSTATUS");
        for(; ((AbstractHttpConnection)this).getRequestTicketText() != null && ((AbstractHttpConnection)this).isConnected(); executeCommandInternal(server))
        {
            if(AbstractHttpConnection.VERBOSE_DEBUG_ON && (super.requestTicketCode == 4 || super.requestTicketCode == 3))
                AbstractHttpConnection.logFile.println("Request Ticket loop continuing even though ticket code specified " + (super.requestTicketCode != 4 ? "COMPLETED" : "ABORTED"));
            if(AbstractHttpConnection.DEBUG_ON)
            {
                AbstractHttpConnection.logFile.println("HttpConnection successful, but received requestTicket with text: " + super.requestTicketText);
                AbstractHttpConnection.logFile.println("\t waiting " + super.retryTimeout + " ms before trying again");
            }
            if(super.requestTicketCode == 4)
            {
                ((AbstractHttpConnection)this).setResponseCode(404);
                break;
            }
            if(super.requestTicketCode == 3)
            {
                ((AbstractHttpConnection)this).setResponseCode(200);
                break;
            }
            HttpConnectionListener listener;
            for(Enumeration e = JCListenerList.elements(super.listeners); e.hasMoreElements(); listener.connectionStatusUpdated(((AbstractHttpConnection)this).getRequestTicketText(), ((AbstractHttpConnection)this).isConnected()))
                listener = (HttpConnectionListener)e.nextElement();

            ((AbstractHttpConnection)this).doTheWait();
        }

        if(AbstractHttpConnection.VERBOSE_DEBUG_ON)
            if(!((AbstractHttpConnection)this).isConnected())
            {
                AbstractHttpConnection.logFile.println("Request Ticket loop terminated:  No connection");
                AbstractHttpConnection.logFile.println("Last message was: " + super.requestTicketText);
            } else
            {
                if(super.requestTicketText != null)
                    AbstractHttpConnection.logFile.println("Request Ticket loop terminated with text:  " + super.requestTicketText);
                else
                    AbstractHttpConnection.logFile.println("Request Ticket loop terminated:  null string");
                if(super.requestTicketCode == 4)
                    AbstractHttpConnection.logFile.println("Final return code:  ABORTED");
                else
                if(super.requestTicketCode == 3)
                    AbstractHttpConnection.logFile.println("Final return code:  COMPLETED");
                else
                    AbstractHttpConnection.logFile.println("Unexpected final return code:  " + super.requestTicketCode);
            }
        super.requestTicketID = null;
        ((AbstractHttpConnection)this).setCommand(orig_command);
    }

    protected NVPair[] buildParameters()
    {
        String params[][] = ((AbstractHttpConnection)this).getParameters();
        if(params == null)
            return null;
        int length = params.length;
        NVPair headers[] = new NVPair[length];
        for(int i = 0; i < params.length; i++)
            headers[i] = new NVPair(params[i][0], params[i][1]);

        return headers;
    }

    protected OutputStream executeUploadCommandInternal(String server_name)
    {
        OutputStream os = null;
        if(server_name == null)
            return null;
        NVPair headers[] = buildParameters();
        HTTPConnection conn = null;
        URL server_url = null;
        try
        {
            ((AbstractHttpConnection)this).setResponseCode(-1);
            server_url = new URL(server_name);
            conn = getConnection(server_url);
            if(AbstractHttpConnection.DEBUG_ON)
            {
                AbstractHttpConnection.logFile.println("(executeUploadCommandInternal) URL object: \"" + server_url.toString() + "\"");
                AbstractHttpConnection.logFile.println("(executeUploadCommandInternal) Connection object: \"" + conn.getHost() + "\":\"" + conn.getPort() + "\"");
            }
            HTTPResponse resp = null;
            addAuthorization(server_url, conn);
            super.requestTicketID = null;
            super.requestTicketText = null;
            super.requestTicketCode = -1;
            if(super.content_length != -1)
                o_stream = new HttpOutputStream(super.content_length);
            else
                o_stream = new HttpOutputStream();
            String location = server_url.getFile();
            int size = headers != null ? headers.length : 0;
            for(int i = 0; i < headers.length; i++)
            {
                location = location + (i != 0 ? "&" : "?");
                location = location + headers[i].getName() + "=" + headers[i].getValue();
            }

            if(AbstractHttpConnection.DEBUG_ON)
            {
                AbstractHttpConnection.logFile.println("-------------------------------------------------------");
                AbstractHttpConnection.logFile.println("Initiating connection (upload) to: " + conn);
                long now = System.currentTimeMillis();
                AbstractHttpConnection.logFile.println("\tat " + new Date(now) + " (" + now + ")");
                AbstractHttpConnection.logFile.println("\tusing timeout " + conn.getTimeout());
            }
            if(AbstractHttpConnection.VERBOSE_DEBUG_ON)
            {
                NVPair heads[] = conn.getDefaultHeaders();
                for(int i = 0; i < heads.length; i++)
                    AbstractHttpConnection.logFile.println("Default Header: [" + heads[i].getName() + "] = [" + heads[i].getValue() + "]");

                for(int i = 0; i < headers.length; i++)
                    AbstractHttpConnection.logFile.println("        Header: [" + headers[i].getName() + "] = [" + headers[i].getValue() + "]");

            }
            if(AbstractHttpConnection.VERBOSE_DEBUG_ON)
                AbstractHttpConnection.logFile.println("\tFull (PUT) URL: " + location);
            response = conn.Post(location, o_stream);
            return ((OutputStream) (o_stream));
        }
        catch(ConnectException e)
        {
            return null;
        }
        catch(ModuleException e) { }
        catch(InterruptedIOException e)
        {
            String conn_id = getConnectionLookup(server_url);
            connectionPool.remove(((Object) (conn_id)));
        }
        catch(IOException e) { }
        return null;
    }

    protected InputStream executeCommandInternal(String server_name)
    {
        InputStream is = null;
        if(server_name == null)
            return null;
        NVPair headers[] = buildParameters();
        HTTPConnection conn = null;
        URL server_url = null;
        try
        {
            ((AbstractHttpConnection)this).setResponseCode(-1);
            server_url = new URL(server_name);
            conn = getConnection(server_url);
            if(AbstractHttpConnection.DEBUG_ON)
            {
                AbstractHttpConnection.logFile.println("(executeCommandInternal) URL object: \"" + server_url.toString() + "\"");
                AbstractHttpConnection.logFile.println("(executeCommandInternal) Connection object: \"" + conn.getHost() + "\":\"" + conn.getPort() + "\"");
            }
            HTTPResponse resp = null;
            addAuthorization(server_url, conn);
            if(AbstractHttpConnection.proxyUsername != null && AbstractHttpConnection.proxyPassword != null || readProxyUsernamePassword())
            {
                java.util.Properties properties = System.getProperties();
                String host = PropertyUtils.readDefaultString(properties, "http.proxyHost", ((String) (null)));
                int port = PropertyUtils.readInt(properties, "http.proxyPort");
                if(host != null && port != 0x80000000)
                {
                    String realm = "Proxy-Authenticate Realm";
                    addAuthorization(host, port, conn, realm, AbstractHttpConnection.proxyUsername, AbstractHttpConnection.proxyPassword);
                }
            }
            if(super.requestTicketID != null)
            {
                NVPair new_headers[] = new NVPair[headers.length + 1];
                System.arraycopy(((Object) (headers)), 0, ((Object) (new_headers)), 0, headers.length);
                new_headers[headers.length] = new NVPair("DeploySam-ID", super.requestTicketID);
                headers = new_headers;
            }
            super.requestTicketID = null;
            super.requestTicketText = null;
            super.requestTicketCode = -1;
            long startTime = System.currentTimeMillis();
            if(AbstractHttpConnection.DEBUG_ON)
            {
                AbstractHttpConnection.logFile.println("-------------------------------------------------------");
                AbstractHttpConnection.logFile.println("Initiating connection (download) to: " + server_url);
                AbstractHttpConnection.logFile.println("\tat " + new Date(startTime) + " (" + startTime + ")");
                AbstractHttpConnection.logFile.println("\tusing timeout " + conn.getTimeout());
            }
            if(AbstractHttpConnection.VERBOSE_DEBUG_ON)
            {
                NVPair heads[] = conn.getDefaultHeaders();
                for(int i = 0; i < heads.length; i++)
                    AbstractHttpConnection.logFile.println("Default Header: [" + heads[i].getName() + "] = [" + heads[i].getValue() + "]");

                for(int i = 0; i < headers.length; i++)
                    AbstractHttpConnection.logFile.println("        Header: [" + headers[i].getName() + "] = [" + headers[i].getValue() + "]");

            }
            String cc = (String)super.header_table.get("COMMAND");
            boolean is_log = cc.equals("ERROR") || cc.equals("MESSAGE");
            boolean has_message = super.data_message != null;
            boolean has_files = super.header_table.get("FILE") != null;
            if(is_log && has_message)
            {
                HttpOutputStream out = new HttpOutputStream(super.data_message.getBytes().length + "NOTES=".length());
                String append = Codecs.nv2query(headers);
                if(AbstractHttpConnection.VERBOSE_DEBUG_ON)
                    AbstractHttpConnection.logFile.println("\tFull (POST) URL: " + server_url + "?" + append);
                resp = conn.Post(server_url.getFile() + "?" + append, out);
                ((OutputStream) (out)).write("NOTES=".getBytes());
                ((OutputStream) (out)).write(super.data_message.getBytes());
                ((OutputStream) (out)).flush();
                out.close();
            } else
            if(has_files)
            {
                Enumeration return_headers = null;
                int retries = 0;
                String files = (String)super.header_table.get("FILE");
                int curr = 0;
                NVPair new_headers[] = new NVPair[headers.length - 1];
                for(int i = 0; i < headers.length; i++)
                    if(!headers[i].getName().equals("FILE"))
                        new_headers[curr++] = headers[i];

                headers = new_headers;
                do
                {
                    String append = Codecs.nv2query(headers);
                    HttpOutputStream out = new HttpOutputStream(files.getBytes().length);
                    if(AbstractHttpConnection.VERBOSE_DEBUG_ON)
                        AbstractHttpConnection.logFile.println("\tFull (PUT) URL: " + server_url + "?" + append);
                    resp = conn.Post(server_url.getFile() + "?" + append, out);
                    ((OutputStream) (out)).write(files.getBytes());
                    ((OutputStream) (out)).flush();
                    out.close();
                    retries++;
                    return_headers = resp.listHeaders();
                    if(!return_headers.hasMoreElements())
                        try
                        {
                            Thread.sleep(1000L);
                        }
                        catch(InterruptedException e) { }
                } while((return_headers == null || !return_headers.hasMoreElements()) && retries < 100);
                if(AbstractHttpConnection.VERBOSE_DEBUG_ON)
                    AbstractHttpConnection.logFile.println("# of Attempts for PUT Request: " + retries);
            } else
            {
                if(AbstractHttpConnection.VERBOSE_DEBUG_ON)
                    AbstractHttpConnection.logFile.println("\tFull (GET) URL: " + server_url + "?" + Codecs.nv2query(headers));
                resp = conn.Get(server_url.getFile(), headers);
            }
            int code = resp.getStatusCode();
            ((AbstractHttpConnection)this).setResponseCode(code);
            if(code == 407 && super.proxyRetries < 3)
            {
                super.proxyRetries++;
                if(AbstractHttpConnection.proxyUsername == null || AbstractHttpConnection.proxyPassword == null)
                {
                    readProxyUsernamePassword();
                    if(AbstractHttpConnection.proxyUsername == null || AbstractHttpConnection.proxyPassword == null)
                        requestProxyUsernamePassword();
                    else
                        super.proxyRetries = 0;
                } else
                {
                    requestProxyUsernamePassword();
                }
                if(AbstractHttpConnection.proxyUsername == null || AbstractHttpConnection.proxyPassword == null)
                {
                    super.proxyRetries = 3;
                } else
                {
                    if(super.proxyRetries > 0)
                        writeProxyUsernamePassword();
                    java.util.Properties properties = System.getProperties();
                    String host = PropertyUtils.readDefaultString(properties, "http.proxyHost", ((String) (null)));
                    int port = PropertyUtils.readInt(properties, "http.proxyPort");
                    if(host != null && port != 0x80000000)
                    {
                        String realm = resp.getHeader("Proxy-Authenticate");
                        addAuthorization(host, port, conn, realm, AbstractHttpConnection.proxyUsername, AbstractHttpConnection.proxyPassword);
                    }
                }
            }
            try
            {
                int length = resp.getHeaderAsInt("Content-length");
                if(length != -1)
                    ((AbstractHttpConnection)this).setContentLength(length);
            }
            catch(NumberFormatException nfe)
            {
                ((AbstractHttpConnection)this).setContentLength(-1);
            }
            if(AbstractHttpConnection.DEBUG_ON)
            {
                long now = System.currentTimeMillis();
                AbstractHttpConnection.logFile.println("Connection (download) established: ");
                AbstractHttpConnection.logFile.println("\tat " + new Date(now) + "(" + now + ")");
                AbstractHttpConnection.logFile.println("\tduration (ms): " + (now - startTime));
                AbstractHttpConnection.logFile.println("\tresponse code " + code + " " + ((AbstractHttpConnection)this).getResponseExplanation(code));
            }
            if(AbstractHttpConnection.VERBOSE_DEBUG_ON)
            {
                String head;
                String val;
                for(Enumeration e = resp.listHeaders(); e.hasMoreElements(); AbstractHttpConnection.logFile.println("Returned Header [" + head + "] = [" + val + "]"))
                {
                    head = (String)e.nextElement();
                    val = resp.getHeader(head);
                }

            }
            if(AbstractHttpConnection.DEBUG_ON || AbstractHttpConnection.VERBOSE_DEBUG_ON)
                AbstractHttpConnection.logFile.println("-------------------------------------------------------");
            if(code >= 300)
                return null;
            super.last_server = server_name;
            if(resp.getHeader("DeploySam-ID") != null)
            {
                super.requestTicketID = resp.getHeader("DeploySam-ID");
                super.requestTicketText = resp.getHeader("DeploySam-StatusText");
                try
                {
                    super.requestTicketCode = resp.getHeaderAsInt("DeploySam-StatusCode");
                }
                catch(NumberFormatException nfe)
                {
                    super.requestTicketCode = super.requestTicketID != null ? 2 : -1;
                }
                return null;
            }
            HttpConnectionListener listener;
            for(Enumeration e = JCListenerList.elements(super.listeners); e.hasMoreElements(); listener.connectionSuccessful(server_name))
                listener = (HttpConnectionListener)e.nextElement();

            is = resp.getInputStream();
            return is;
        }
        catch(ConnectException e)
        {
            return null;
        }
        catch(ModuleException e) { }
        catch(InterruptedIOException e)
        {
            String conn_id = getConnectionLookup(server_url);
            connectionPool.remove(((Object) (conn_id)));
        }
        catch(IOException e) { }
        return null;
    }

    protected HTTPConnection getConnection(URL url)
    {
        if(url == null)
            return null;
        try
        {
            int defaultTimeout = Integer.parseInt(System.getProperty("deploy.http.timeout.default"));
            HTTPConnection.setDefaultTimeout(defaultTimeout);
            if(AbstractHttpConnection.DEBUG_ON)
            {
                AbstractHttpConnection.logFile.println("-------------------------------------------------------");
                AbstractHttpConnection.logFile.println("Setting default connection timeout to: " + defaultTimeout);
                AbstractHttpConnection.logFile.println("-------------------------------------------------------");
            }
        }
        catch(NumberFormatException e) { }
        HTTPConnection.setDefaultAllowUserInteraction(false);
        String index = getConnectionLookup(url);
        HTTPConnection conn = (HTTPConnection)connectionPool.get(((Object) (index)));
        if(conn == null)
            try
            {
                conn = new HTTPConnection(url);
                int timeout;
                try
                {
                    timeout = Integer.parseInt(System.getProperty("deploy.http.timeout", "10000"));
                }
                catch(NumberFormatException e)
                {
                    timeout = 10000;
                }
                if(AbstractHttpConnection.DEBUG_ON)
                {
                    AbstractHttpConnection.logFile.println("-------------------------------------------------------");
                    AbstractHttpConnection.logFile.println("New Connection:  Setting timeout to: " + timeout);
                    AbstractHttpConnection.logFile.println("-------------------------------------------------------");
                }
                conn.setTimeout(timeout);
                addAuthorization(url, conn);
                try
                {
                    String agent = System.getProperty("http.agent");
                    if(agent != null)
                    {
                        NVPair user_agent[] = new NVPair[1];
                        user_agent[0] = new NVPair("User-Agent", agent);
                        conn.setDefaultHeaders(user_agent);
                    }
                }
                catch(SecurityException e) { }
                connectionPool.put(((Object) (index)), ((Object) (conn)));
            }
            catch(ProtocolNotSuppException e)
            {
                return null;
            }
        return conn;
    }

    private String getConnectionLookup(URL url)
    {
        StringBuffer buffer = new StringBuffer();
        buffer.append(url.getProtocol() + "://" + url.getHost());
        int port = url.getPort();
        if(port == -1)
            port = URI.defaultPort(url.getProtocol());
        buffer.append(":" + port + url.getFile());
        return buffer.toString();
    }

    protected void addAuthorization(URL url, HTTPConnection conn)
    {
        addAuthorization(url, conn, "DeployDirector");
    }

    protected void addAuthorization(URL url, HTTPConnection conn, String realm)
    {
        String host = url.getHost();
        int port = url.getPort();
        if(port == -1)
        {
            String protocol = url.getProtocol();
            port = URI.defaultPort(protocol);
        }
        String username = ((AbstractHttpConnection)this).createUserIDString();
        String password = ((AbstractHttpConnection)this).createPasswordString();
        addAuthorization(host, port, conn, realm, username, password);
    }

    protected void addAuthorization(String host, int port, HTTPConnection conn, String realm, String username, String password)
    {
        if(realm == null)
            realm = "DeployDirector";
        AuthorizationInfo.addBasicAuthorization(host, port, realm, username, password);
        AuthorizationInfo.addDigestAuthorization(host, port, realm, username, password);
        boolean authorizing_proxy = false;
        java.util.Properties properties = System.getProperties();
        String proxy_host = PropertyUtils.readDefaultString(properties, "http.proxyHost", ((String) (null)));
        if(proxy_host != null && host.equals(((Object) (proxy_host))))
            authorizing_proxy = true;
        NVPair old_headers[] = conn.getDefaultHeaders();
        if(old_headers == null || old_headers.length == 0)
        {
            NVPair auth_headers[] = new NVPair[1];
            auth_headers[0] = new NVPair("Authorization", "Basic " + com.sitraka.deploy.util.Codecs.base64Encode(username + ":" + password));
            conn.setDefaultHeaders(auth_headers);
            old_headers = auth_headers;
        } else
        if(!authorizing_proxy)
        {
            boolean match = false;
            for(int i = 0; i < old_headers.length; i++)
            {
                String name = old_headers[i].getName();
                if(!name.equals("Authorization"))
                    continue;
                match = true;
                old_headers[i] = new NVPair("Authorization", "Basic " + com.sitraka.deploy.util.Codecs.base64Encode(username + ":" + password));
                conn.setDefaultHeaders(old_headers);
                break;
            }

            if(!match)
            {
                NVPair new_headers[] = new NVPair[old_headers.length + 1];
                System.arraycopy(((Object) (old_headers)), 0, ((Object) (new_headers)), 0, old_headers.length);
                new_headers[old_headers.length] = new NVPair("Authorization", "Basic " + com.sitraka.deploy.util.Codecs.base64Encode(username + ":" + password));
                conn.setDefaultHeaders(new_headers);
                old_headers = new_headers;
            }
        }
        if(authorizing_proxy)
        {
            boolean match = false;
            for(int i = 0; i < old_headers.length; i++)
            {
                String name = old_headers[i].getName();
                if(!name.equals("Proxy-Authorization"))
                    continue;
                match = true;
                old_headers[i] = new NVPair("Proxy-Authorization", "Basic " + com.sitraka.deploy.util.Codecs.base64Encode(username + ":" + password));
                conn.setDefaultHeaders(old_headers);
                break;
            }

            if(!match)
            {
                NVPair new_headers[] = new NVPair[old_headers.length + 1];
                System.arraycopy(((Object) (old_headers)), 0, ((Object) (new_headers)), 0, old_headers.length);
                new_headers[old_headers.length] = new NVPair("Proxy-Authorization", "Basic " + com.sitraka.deploy.util.Codecs.base64Encode(username + ":" + password));
                conn.setDefaultHeaders(new_headers);
                old_headers = new_headers;
            }
        }
    }

    protected void requestProxyUsernamePassword()
    {
        ProxyUsernamePasswordEditor ed = new ProxyUsernamePasswordEditor(((AbstractHttpConnection) (this)));
        ed.popup();
    }

    protected boolean writeProxyUsernamePassword()
    {
        String cam_home = System.getProperty("cam.home");
        if(cam_home == null || cam_home.length() <= 0)
            return false;
        String file_path = new String(FileUtils.unifyFileSeparator(cam_home));
        if(file_path.endsWith(File.separator))
            file_path = file_path + "lib" + File.separator + "secauth.dat";
        else
            file_path = file_path + File.separator + "lib" + File.separator + "secauth.dat";
        try
        {
            PrintWriter out = new PrintWriter(((OutputStream) (new FileOutputStream(file_path))));
            out.println(com.sitraka.deploy.util.Codecs.base64Encode(AbstractHttpConnection.proxyUsername));
            out.println(com.sitraka.deploy.util.Codecs.base64Encode(AbstractHttpConnection.proxyPassword));
            out.close();
            return true;
        }
        catch(IOException ioe)
        {
            return false;
        }
    }

    protected boolean readProxyUsernamePassword()
    {
        String cam_home = System.getProperty("cam.home");
        if(cam_home == null || cam_home.length() <= 0)
            return false;
        String file_path = new String(FileUtils.unifyFileSeparator(cam_home));
        if(file_path.endsWith(File.separator))
            file_path = file_path + "lib" + File.separator + "secauth.dat";
        else
            file_path = file_path + File.separator + "lib" + File.separator + "secauth.dat";
        try
        {
            BufferedReader in = new BufferedReader(((java.io.Reader) (new InputStreamReader(((InputStream) (new FileInputStream(file_path)))))));
            AbstractHttpConnection.proxyUsername = com.sitraka.deploy.util.Codecs.base64Decode(in.readLine());
            AbstractHttpConnection.proxyPassword = com.sitraka.deploy.util.Codecs.base64Decode(in.readLine());
            in.close();
            return true;
        }
        catch(IOException ioe)
        {
            return false;
        }
    }

    static 
    {
        String old_handlers = System.getProperty("java.protocol.handler.pkgs");
        if(old_handlers == null || old_handlers.indexOf("HTTPClient") == -1)
        {
            if(old_handlers != null && old_handlers != "")
                old_handlers = old_handlers + "|";
            else
                old_handlers = "";
            old_handlers = old_handlers + "HTTPClient";
            ((Hashtable) (System.getProperties())).put("java.protocol.handler.pkgs", ((Object) (old_handlers)));
        }
    }
}
