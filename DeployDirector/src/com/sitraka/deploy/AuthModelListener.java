// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AuthModelListener.java

package com.sitraka.deploy;

import java.util.EventListener;

// Referenced classes of package com.sitraka.deploy:
//            AuthModelEvent

public interface AuthModelListener
    extends EventListener
{

    public abstract void modelChanged(AuthModelEvent authmodelevent);

    public abstract void modelReset(AuthModelEvent authmodelevent);

    public abstract void modelSaved(AuthModelEvent authmodelevent);
}
