// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AuthorizationModel.java

package com.sitraka.deploy;

import java.util.List;

// Referenced classes of package com.sitraka.deploy:
//            AuthModel

public interface AuthorizationModel
    extends AuthModel
{

    public abstract List getContexts();

    public abstract List getUsers(Object obj);

    public abstract List getAccess(Object obj, Object obj1);

    public abstract void setAccess(Object obj, Object obj1, List list);
}
