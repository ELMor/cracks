// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   GeneratedTextWorker.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.common.app.AppFile;
import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.sam.AbstractTrackerEntry;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.RequestParser;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.request.RequestList;
import com.sitraka.deploy.sam.servlet.AuthInfo;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.sam.servlet.ServletNotFoundException;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            AbstractWorker

public class GeneratedTextWorker extends AbstractWorker
{

    public GeneratedTextWorker()
    {
        super.type = 4;
    }

    public void startWorking()
        throws ServletProcessException, ServletNotFoundException
    {
        synchronized(super.STATE_LOCK)
        {
            switch(super.parser.getRequest())
            {
            case 1000: 
                sendBundleList();
                break;

            case 1010: 
                sendServers();
                break;

            case 3102: 
                sendVersionXML();
                break;

            case 3201: 
                sendPlatformXML();
                break;

            case 1002: 
                sendBundleVersions();
                break;

            case 2005: 
                sendFileList();
                break;

            case 1001: 
                sendBundleProperties();
                break;

            case 1005: 
                sendLicenseFile();
                break;

            case 1003: 
                sendClientCompressionLevel();
                break;

            case 1006: 
                sendServerCompressionLevel();
                break;

            case 3001: 
                sendUploadStatus();
                break;

            case 2016: 
                sendIsLatestVersion();
                break;

            case 1004: 
                sendGetLatestVersion();
                break;

            case 2012: 
                sendIsAuthorized();
                break;
            }
        }
    }

    protected void sendIsLatestVersion()
        throws ServletNotFoundException
    {
        String content = null;
        String message = null;
        String app_name = super.parser.getApplication();
        String client_version = super.parser.getVersion();
        Application app = super.vault.getApplication(app_name);
        if(app_name == null || app == null || client_version == null)
        {
            String msg = "Unable to check for latest version; ";
            if(app_name == null)
                msg = msg + "application name not given.";
            else
            if(app == null)
                msg = msg + "application named " + app_name + " not found.";
            else
                msg = msg + "version name not given.";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        }
        Object user = AuthInfo.getUserAuthorization(super.request);
        String decoded = AuthInfo.decodeUser(user);
        String latest_version = super.vault.getLatestAuthorizedVersion(app_name, decoded);
        if(client_version.equalsIgnoreCase(latest_version))
        {
            content = "true";
            message = "Client has latest version";
        } else
        {
            content = "false";
            message = "Client does not have latest version";
        }
        if(!app_name.equals("DDCAM"))
            super.server.logMessage(super.request, "ClientCheckedStatus", super.parser.getApplication(), client_version, message);
        ((AbstractWorker)this).sendText(content);
    }

    protected void sendGetLatestVersion()
        throws ServletNotFoundException
    {
        String content = null;
        String app_name = super.parser.getApplication();
        Application app = super.vault.getApplication(app_name);
        if(app_name == null || app == null)
        {
            String msg = "Unable to get latest version; ";
            if(app_name == null)
                msg = msg + "application name not given.";
            else
                msg = msg + "application named " + app_name + " not found.";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        }
        Object user = AuthInfo.getUserAuthorization(super.request);
        String decoded = AuthInfo.decodeUser(user);
        content = super.vault.getLatestAuthorizedVersion(app_name, decoded);
        if(!app_name.equals("DDCAM"))
            super.server.logMessage(super.request, "ClientCheckedStatus", app_name, content, "Client requested latest version of " + app_name);
        ((AbstractWorker)this).sendText(content);
    }

    protected void sendLicenseFile()
        throws ServletNotFoundException, ServletProcessException
    {
        Application app = super.vault.getApplication(super.parser.getApplication());
        Version ver = ((AbstractWorker)this).getVersion();
        if(app == null || ver == null)
        {
            String msg = "Unable to retrieve license file; ";
            if(app == null)
                msg = msg + "application named " + super.parser.getApplication() + " not found.";
            else
                msg = msg + "version named " + super.parser.getVersion() + " of " + super.parser.getApplication() + " not found.";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        }
        File file = super.vault.getLicenseFile(app.getName(), ver.getName());
        if(file == null || !file.exists() || !file.canRead())
        {
            String msg = "Cannot access license file for " + app.getName() + ", version " + ver.getName();
            throw new ServletProcessException(msg, super.request, super.response);
        } else
        {
            ((AbstractWorker)this).sendTextFile(file);
            return;
        }
    }

    protected void sendBundleProperties()
        throws ServletNotFoundException
    {
        Application app = super.vault.getApplication(super.parser.getApplication());
        Version ver = ((AbstractWorker)this).getVersion();
        if(app == null || ver == null)
        {
            String msg = "Unable to retrieve bundle properties; ";
            if(app == null)
                msg = msg + "application named " + super.parser.getApplication() + " not found.";
            else
                msg = msg + "version named " + super.parser.getVersion() + " of " + super.parser.getApplication() + " not found.";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        }
        String platform = super.parser.getPlatform();
        if(platform == null)
            platform = "ALL";
        StringBuffer content = new StringBuffer();
        content.append("#\n# Bundle properties file for: " + app.getName() + ", version: " + ver.getName() + "\n#\n\n");
        content.append("# Name of the bundle:\n");
        content.append("bundlename=" + app.getName() + "\n\n");
        content.append("# Platform:\n");
        content.append("platform=" + platform + "\n\n");
        content.append(ver.getAppProperties(platform));
        String timeout = Servlet.properties.getProperty("deploy.http.client.timeout");
        if(timeout != null && timeout.trim().length() != 0)
        {
            content.append("# HTTP settings:\n");
            content.append("deploy.http.timeout=");
            content.append(timeout);
            content.append("\n\n");
        }
        String defaultTimeout = Servlet.properties.getProperty("deploy.http.client.timeout.default");
        if(defaultTimeout != null && defaultTimeout.trim().length() > 0)
        {
            content.append("deploy.http.timeout.default=");
            content.append(defaultTimeout);
            content.append("\n\n");
        }
        String hashCodeAlgorithm = Servlet.properties.getProperty("deploy.hashcode.algorithm");
        if(hashCodeAlgorithm != null && hashCodeAlgorithm.trim().length() > 0)
        {
            content.append("# Hash Code Algorithm: \n");
            content.append("deploy.hashcode.algorithm=");
            content.append(hashCodeAlgorithm);
            content.append("\n\n");
        }
        ((AbstractWorker)this).sendText(content.toString());
    }

    protected void sendPlatformXML()
        throws ServletNotFoundException, ServletProcessException
    {
        File xml_file = super.vault.getPlatformXML();
        if(xml_file == null)
        {
            String msg = "Unable to retrieve platform XML file.";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        } else
        {
            ((AbstractWorker)this).sendTextFile(xml_file);
            return;
        }
    }

    protected void sendVersionXML()
        throws ServletNotFoundException, ServletProcessException
    {
        String app_name = super.parser.getApplication();
        String ver_name = super.parser.getVersion();
        Application app = null;
        Version ver = null;
        if((app = super.vault.getApplication(app_name)) == null)
        {
            String msg = "Unable to retrieve version.xml file; application named " + app_name + " was not found in the vault.";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        }
        if(app.getVersion(ver_name) == null)
        {
            String msg = "Unable to retrieve version.xml file; version named " + ver_name + " of " + app_name + " was not found in the vault.";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        }
        File xml_file = super.vault.getVersionXML(app_name, ver_name);
        if(xml_file == null)
        {
            String msg = "Unable to retrieve version.xml file; xml not found for " + app_name + " version " + ver_name + ".";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        } else
        {
            ((AbstractWorker)this).sendTextFile(xml_file);
            return;
        }
    }

    protected void sendBundleVersions()
        throws ServletNotFoundException
    {
        String appname = super.parser.getApplication();
        if(appname == null)
        {
            String msg = "Unable to retrieve version list; no app name was given.";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        }
        String content = "";
        Properties props = new Properties();
        Application app = super.vault.getApplication(appname);
        if(app == null)
        {
            String msg = "Unable to retrieve version list; application named " + appname + " not found.";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        }
        Vector versions = app.listVersions();
        Object userData = AuthInfo.getUserAuthorization(super.request);
        String user = AuthInfo.decodeUser(userData);
        String authVersion = super.vault.getLatestAuthorizedVersion(appname, user);
        boolean versionsAuthorized = false;
        if(super.parser.isAdministrator())
            versionsAuthorized = true;
        int size = versions != null ? versions.size() : 0;
        int verCount = 0;
        for(int i = 0; i < size; i++)
        {
            Version ver = (Version)versions.elementAt(i);
            String verName = ver.getName();
            if(verName.equalsIgnoreCase(authVersion))
                versionsAuthorized = true;
            if(versionsAuthorized)
            {
                if(verCount == 0)
                    content = content.concat(ver.getName());
                else
                    content = content.concat("\n" + ver.getName());
                ((Hashtable) (props)).put(((Object) ("version" + verCount + ".name")), ((Object) (ver.getName())));
                String desc = ver.getDescription();
                ((Hashtable) (props)).put(((Object) ("version" + verCount + ".description")), ((Object) (desc != null ? ((Object) (desc)) : ((Object) (ver.getName())))));
                verCount++;
            }
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try
        {
            props.store(((java.io.OutputStream) (out)), "DeploySam: VERSION INFORMATION");
        }
        catch(IOException ioe) { }
        ((AbstractWorker)this).sendText(out.toString());
    }

    protected void sendFileList()
        throws ServletNotFoundException
    {
        String content = "";
        String platform = super.parser.getPlatform();
        if(platform == null)
            platform = "all";
        Version ver = ((AbstractWorker)this).getVersion();
        if(ver == null || platform == null)
        {
            String msg = "Unable to retrieve file list; ";
            if(ver == null)
                msg = msg + "version named " + super.parser.getVersion() + " of " + super.parser.getApplication() + " not found.";
            else
                msg = msg + "platform named " + platform + " not found.";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        }
        Vector files = ver.getFiles(platform);
        int size = files != null ? files.size() : 0;
        for(int i = 0; i < size; i++)
        {
            AppFile file = (AppFile)files.elementAt(i);
            content = content + file.getPropertiesFile(i, ((String) (null)));
        }

        ((AbstractWorker)this).sendText(content);
    }

    protected void sendServers()
        throws ServletProcessException
    {
        String servers = super.vault.listServers();
        if(servers == null)
        {
            throw new ServletProcessException("List of servers not available", super.request, super.response);
        } else
        {
            ((AbstractWorker)this).sendText(servers);
            return;
        }
    }

    protected void sendClientCompressionLevel()
    {
        sendCompressionLevel(((AbstractWorker)this).getClientCompressionLevel());
    }

    protected void sendServerCompressionLevel()
    {
        sendCompressionLevel(((AbstractWorker)this).getServerCompressionLevel());
    }

    protected void sendCompressionLevel(int compressionLevel)
    {
        String content = "";
        content = content + compressionLevel;
        ((AbstractWorker)this).sendText(content);
    }

    protected void sendBundleList()
        throws ServletProcessException
    {
        String content = "";
        if(!super.vault.isLocationSet())
            throw new ServletProcessException("Cannot access available applications list", super.request, super.response);
        String applications[] = super.vault.listApplications();
        for(int i = 0; i < applications.length; i++)
        {
            String app = applications[i].toLowerCase();
            if((!app.startsWith("DDAdmin".toLowerCase()) || super.parser.isAdministrator()) && (!app.startsWith("DDCAM".toLowerCase()) || super.parser.isAdministrator()))
                content = content + applications[i] + "\n";
        }

        ((AbstractWorker)this).sendText(content);
    }

    protected void sendUploadStatus()
        throws ServletNotFoundException
    {
        String idText = super.parser.getRequestID();
        if(idText == null)
        {
            String text = "Upload status ID was not provided.";
            super.server.logMessage(super.request, "ServerInternalError", super.parser.getApplication(), super.parser.getVersion(), text);
            throw new ServletNotFoundException(text, super.request, super.response);
        }
        long id = -1L;
        try
        {
            id = Long.parseLong(idText);
        }
        catch(NumberFormatException nfe)
        {
            String text = "Upload status ID is not a number \"" + idText + "\".";
            super.server.logMessage(super.request, "ServerInternalError", super.parser.getApplication(), super.parser.getVersion(), text);
            throw new ServletNotFoundException(text, super.request, super.response);
        }
        AbstractTrackerEntry obj = RequestList.getRequestByID(id);
        if(obj == null)
        {
            String text = "No upload status matching id \"" + idText + "\".";
            throw new ServletNotFoundException(text, super.request, super.response, true);
        }
        if(obj.getStatus() == 3)
            RequestList.removeReference(obj);
        super.server.setContentType(super.response, "text/plain");
        super.server.setContentLength(super.response, ((Object) (obj)).toString().length());
        super.server.setHeader(super.response, "Accept-Ranges", "none");
        super.server.setHeader(super.response, "DeploySam-ID", "" + obj.getID());
        super.server.setHeader(super.response, "DeploySam-StatusText", obj.getStatusText());
        super.server.setHeader(super.response, "DeploySam-StatusCode", "" + obj.getStatus());
        super.server.setHeader(super.response, "Cache-Control", "no-cache");
        super.server.setHeader(super.response, "Expires", (new Date(System.currentTimeMillis() - 0xf4240L)).toString());
        super.server.setHeader(super.response, "Last-Modified", (new Date()).toString());
        PrintWriter out = super.server.getTextOutputStream(super.response);
        out.print(((Object) (obj)).toString());
        out.flush();
        out.close();
    }

    protected void sendIsAuthorized()
        throws ServletNotFoundException
    {
        String app_name = super.parser.getApplication();
        String ver_name = super.parser.getVersion();
        if(app_name == null || ver_name == null)
        {
            String msg = "Unable to verify authorization; ";
            if(app_name == null)
                msg = msg + "application name was not given.";
            else
                msg = msg + "version name was not given.";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        } else
        {
            int authorized = super.server.isUserAuthenticated(super.request, super.response, super.parser);
            String isAuthorized = authorized != 1 ? "false" : "true";
            ((AbstractWorker)this).sendText(isAuthorized);
            return;
        }
    }
}
