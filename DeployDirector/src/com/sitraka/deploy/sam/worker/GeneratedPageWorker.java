// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   GeneratedPageWorker.java

package com.sitraka.deploy.sam.worker;

import HTTPClient.ParseException;
import HTTPClient.URI;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.ClientIdentifier;
import com.sitraka.deploy.sam.RequestParser;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.servlet.AuthInfo;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.sam.servlet.ServletNotFoundException;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import com.sitraka.deploy.util.PropertyUtils;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            AbstractWorker, AdminPageWorker

public class GeneratedPageWorker extends AbstractWorker
{

    public GeneratedPageWorker()
    {
        super.type = 5;
    }

    public void startWorking()
        throws ServletProcessException, ServletNotFoundException
    {
        synchronized(super.STATE_LOCK)
        {
            switch(super.parser.getRequest())
            {
            case 2008: 
                sendAppletPage(false);
                break;

            case 2010: 
                sendAppletPage(true);
                break;

            case 4002: 
                AdminPageWorker page = new AdminPageWorker();
                ((AbstractWorker) (page)).resetState(super.server, super.parser, super.request, super.response);
                page.sendAdminPage();
                page = null;
                break;

            case 4003: 
                AdminPageWorker info = new AdminPageWorker();
                ((AbstractWorker) (info)).resetState(super.server, super.parser, super.request, super.response);
                info.sendAdminInfoPage();
                info = null;
                break;

            case 3300: 
                sendServerLoadPage();
                break;

            case 2013: 
                sendAppletPage(false, true);
                break;

            case 2014: 
                sendAppletPage(true, true);
                break;
            }
        }
    }

    protected void sendAppletPage(boolean launch)
        throws ServletProcessException
    {
        sendAppletPage(launch, false);
    }

    protected void sendAppletPage(boolean launch, boolean usePlugin)
        throws ServletProcessException
    {
        if(!super.vault.isLocationSet())
        {
            String error_text = "Cannot install. The application vault is currently unavailable";
            throw new ServletProcessException(error_text, super.request, super.response, true);
        }
        String app = super.parser.getApplication();
        if(super.vault.getApplication(app) == null)
        {
            String err_text = app + " does not exist in the vault, cannot install";
            throw new ServletProcessException(err_text, super.request, super.response, true);
        }
        if("DDCAM".equalsIgnoreCase(app))
        {
            String error_text = "The requested application is not available for installation: DDCAM";
            throw new ServletProcessException(error_text, super.request, super.response, true);
        }
        Object user = AuthInfo.getUserAuthorization(super.request);
        String decoded = AuthInfo.decodeUser(user);
        String ver = super.vault.getLatestAuthorizedVersion(app, decoded);
        if(ver == null)
        {
            String err_text = "Cannot determine version of " + app + " to be installed";
            throw new ServletProcessException(err_text, super.request, super.response, true);
        }
        int browser_extension = ClientIdentifier.identifyClientType(super.request);
        String server_name = computeServerName(super.request);
        if(server_name.toLowerCase().endsWith("/install"))
        {
            int end = server_name.length() - "/install".length();
            server_name = server_name.substring(0, end);
        } else
        if(server_name.toLowerCase().endsWith("/launch"))
        {
            int end = server_name.length() - "/launch".length();
            server_name = server_name.substring(0, end);
        } else
        if(server_name.toLowerCase().endsWith("/install-plugin"))
        {
            int end = server_name.length() - "/install-plugin".length();
            server_name = server_name.substring(0, end);
        } else
        if(server_name.toLowerCase().endsWith("/launch-plugin"))
        {
            int end = server_name.length() - "/launch-plugin".length();
            server_name = server_name.substring(0, end);
        }
        String archive = server_name + "/";
        archive = archive + (launch ? "launch.jar" : "install.jar");
        String cab_file = server_name + "/";
        cab_file = cab_file + (launch ? "launch.cab" : "install.cab");
        String applet_tag = "";
        String parameters = super.parser.getParameters();
        Vector cookies = new Vector();
        String cookie_data = super.parser.getCookieData();
        if(cookie_data != null && cookie_data.trim().length() > 0)
            cookies.addElement(((Object) (cookie_data)));
        int cookie_num = 1;
        for(String cookie_name = "COOKIE" + cookie_num; (cookie_data = super.parser.getParam(cookie_name)) != null; cookie_name = "COOKIE" + cookie_num)
        {
            if(cookie_data.trim().length() > 0)
                cookies.addElement(((Object) (cookie_data)));
            cookie_num++;
        }

        if(usePlugin)
        {
            applet_tag = applet_tag + "<!-- HTML CONVERTED -->\n";
            applet_tag = applet_tag + "<OBJECT classid=\"" + PropertyUtils.readDefaultString(Servlet.properties, "deploy.applet.javaplugin.ie.classid", "") + "\"\n";
            if(ClientIdentifier.identifyClientOS(super.request) == 3)
                applet_tag = applet_tag + "  WIDTH=900 HEIGHT=700\n";
            else
                applet_tag = applet_tag + "  WIDTH=500 HEIGHT=300\n";
            applet_tag = applet_tag + "  NAME=\"" + app + ", version " + ver + "\n";
            applet_tag = applet_tag + "  \"ALT = \"You would need a Java-enabled browser to see this.\"\n";
            applet_tag = applet_tag + "  codebase=\"" + PropertyUtils.readDefaultString(Servlet.properties, "deploy.applet.javaplugin.ie.codebase", "") + "\">\n";
            applet_tag = applet_tag + "  <PARAM NAME = \"CODE\" VALUE = \"com/sitraka/deploy/install/Main" + (launch ? "Launcher" : "Installer") + ".class\" >\n";
            applet_tag = applet_tag + "  <PARAM NAME = \"CODEBASE\" VALUE = \".\" >\n";
            applet_tag = applet_tag + "  <PARAM NAME = \"ARCHIVE\" VALUE = \"" + archive + "\">\n";
            applet_tag = applet_tag + "  <PARAM NAME = \"NAME\" VALUE = \"" + app + ", " + ver + "\">\n";
            applet_tag = applet_tag + "  <PARAM NAME = \"MAYSCRIPT\" VALUE = true >\n";
            applet_tag = applet_tag + "  <PARAM NAME = \"type\" VALUE=\"" + PropertyUtils.readDefaultString(Servlet.properties, "deploy.applet.javaplugin.type", "") + "\">\n";
            applet_tag = applet_tag + "  <PARAM NAME = \"scriptable\" VALUE=\"false\">\n";
            applet_tag = applet_tag + "  <PARAM NAME = \"CABBASE\" VALUE =\"" + cab_file + "\">\n";
            applet_tag = applet_tag + "  <PARAM NAME = \"BUNDLE\" VALUE =\"" + app + "\">\n";
            applet_tag = applet_tag + "  <PARAM NAME = \"VERSION\" VALUE =\"" + ver + "\">\n";
            applet_tag = applet_tag + "  <PARAM NAME = \"SERVER\" VALUE =\"" + server_name + "\">\n";
            if(launch)
            {
                applet_tag = applet_tag + "  <PARAM NAME= \"LAUNCH\" VALUE=\"true\">\n";
                if(parameters != null)
                {
                    parameters = parameters.replace('&', ' ');
                    applet_tag = applet_tag + "  <PARAM NAME=\"PARAMETERS\" VALUE=\"" + parameters + "\">\n";
                }
            } else
            if(parameters != null && parameters.endsWith("/launch-plugin"))
            {
                applet_tag = applet_tag + "  <PARAM NAME= \"LAUNCH\" VALUE=\"true\">\n";
                parameters = parameters.replace('&', ' ');
                applet_tag = applet_tag + "  <PARAM NAME= \"PARAMETERS\" VALUE=\"" + parameters.substring(0, parameters.lastIndexOf("/launch-plugin")) + "\">\n";
            } else
            {
                applet_tag = applet_tag + "  <PARAM NAME= \"LAUNCH\" VALUE=\"false\">\n";
            }
            applet_tag = applet_tag + "  <PARAM NAME = \"PLUGIN\" VALUE =\"TRUE\">\n";
            if(cookies.size() > 0)
                if(cookies.size() == 1)
                {
                    cookie_data = (String)cookies.elementAt(0);
                    applet_tag = applet_tag + "  <PARAM NAME=\"COOKIE\" VALUE=\"" + cookie_data + "\">\n";
                } else
                {
                    for(int i = 0; i < cookies.size(); i++)
                    {
                        cookie_data = (String)cookies.elementAt(i);
                        applet_tag = applet_tag + "  <PARAM NAME=\"COOKIE" + (i + 1) + "\" VALUE=\"" + cookie_data + "\">\n";
                    }

                }
            Object obj = super.server.getUserAuthorization(super.request);
            if(obj == null)
                obj = ((Object) (new String("")));
            applet_tag = applet_tag + "  <PARAM NAME= \"AUTH_OBJECT\" VALUE=\"" + obj.toString() + "\">\n";
            applet_tag = applet_tag + "  <PARAM NAME= \"NEXT_PAGE\" VALUE=\"\">\n";
            applet_tag = applet_tag + "  <COMMENT>\n";
            applet_tag = applet_tag + "  <EMBED type=\"" + PropertyUtils.readDefaultString(Servlet.properties, "deploy.applet.javaplugin.type", "") + "\"\n";
            if(launch)
                applet_tag = applet_tag + "  CODE = \"com/sitraka/deploy/install/MainLauncher.class\"\n";
            else
                applet_tag = applet_tag + "  CODE = \"com/sitraka/deploy/install/MainInstaller.class\"\n";
            applet_tag = applet_tag + "  CODEBASE = \".\"\n";
            applet_tag = applet_tag + "  ARCHIVE =\"" + archive + "\"\n";
            applet_tag = applet_tag + "  ALT = \"You would need a Java-enabled browser to see this.\"\n";
            applet_tag = applet_tag + "  NAME = \"" + app + ", version " + ver + "\"\n";
            if(ClientIdentifier.identifyClientOS(super.request) == 3)
                applet_tag = applet_tag + "  WIDTH=900 HEIGHT=700\n";
            else
                applet_tag = applet_tag + "  WIDTH=500 HEIGHT=300\n";
            applet_tag = applet_tag + "  MAYSCRIPT = true \n";
            applet_tag = applet_tag + "  CABBASE = \"" + cab_file + "\"\n";
            applet_tag = applet_tag + "  BUNDLE = \"" + app + "\"\n";
            applet_tag = applet_tag + "  VERSION = \"" + ver + "\"\n";
            applet_tag = applet_tag + "  SERVER = \"" + server_name + "\"\n";
            if(launch)
            {
                applet_tag = applet_tag + "  LAUNCH = \"true\"\n";
                if(parameters != null)
                {
                    parameters = parameters.replace('&', ' ');
                    applet_tag = applet_tag + "  PARAMETERS = \"" + parameters + "\"\n";
                }
            } else
            if(parameters != null && parameters.endsWith("/launch-plugin"))
            {
                applet_tag = applet_tag + "  LAUNCH = \"true\"\n";
                parameters = parameters.replace('&', ' ');
                applet_tag = applet_tag + "  PARAMETERS = \"" + parameters.substring(0, parameters.lastIndexOf("/launch-plugin")) + "\"\n";
            } else
            {
                applet_tag = applet_tag + "  LAUNCH =\"false\"\n";
            }
            applet_tag = applet_tag + "  PLUGIN  = \"TRUE\"\n";
            applet_tag = applet_tag + "  AUTH_OBJECT = \"" + obj.toString() + "\"\n";
            applet_tag = applet_tag + "  NEXT_PAGE = \"\"\n";
            applet_tag = applet_tag + "  scriptable=false\n";
            applet_tag = applet_tag + "  pluginspage=\"" + PropertyUtils.readDefaultString(Servlet.properties, "deploy.applet.javaplugin.ns.pluginspage", "") + "\"><NOEMBED>\n";
            applet_tag = applet_tag + "  </NOEMBED>\n";
            applet_tag = applet_tag + "  </EMBED>\n";
            applet_tag = applet_tag + "  </COMMENT>\n";
            applet_tag = applet_tag + "  </OBJECT>\n";
        } else
        {
            applet_tag = applet_tag + "<applet code=\"com/sitraka/deploy/install/Main" + (launch ? "Launcher" : "Installer") + ".class\"\n";
            applet_tag = applet_tag + "  codebase=\".\"\n";
            applet_tag = applet_tag + "  archive=\"" + archive + "\"\n";
            applet_tag = applet_tag + "  name=\"" + app + ", version " + ver + " Installer\"\n";
            if(ClientIdentifier.identifyClientOS(super.request) == 3)
                applet_tag = applet_tag + "  width=900 height=700\n";
            else
                applet_tag = applet_tag + "  width=500 height=300\n";
            applet_tag = applet_tag + "  alt=\"You would need a Java-enabled browser to see this.\" MAYSCRIPT>\n";
            applet_tag = applet_tag + "<B>Your browser is not Java-enabled.</B>";
            applet_tag = applet_tag + "<LI>To view this page you need to be using a Java-enabled browser";
            applet_tag = applet_tag + "<LI>Please check to make sure Java and Javascript are enabled in your browser options.";
            applet_tag = applet_tag + "<LI>To proceed using Java Plug-in, ";
            if(launch)
            {
                applet_tag = applet_tag + "click <A HREF=\"" + server_name + "/launch-plugin";
                if(parameters != null)
                {
                    applet_tag = applet_tag + "?";
                    applet_tag = applet_tag + parameters.replace(' ', '&');
                }
                applet_tag = applet_tag + "\">here </A>\n";
            } else
            {
                applet_tag = applet_tag + "click <A HREF=\"" + server_name + "/install-plugin\">" + "here </A>\n";
            }
            applet_tag = applet_tag + "  <param NAME=\"CABBASE\" VALUE=\"" + cab_file + "\">\n";
            applet_tag = applet_tag + "  <param NAME=\"BUNDLE\" VALUE=\"" + app + "\">\n";
            applet_tag = applet_tag + "  <param NAME=\"VERSION\" VALUE=\"" + ver + "\">\n";
            applet_tag = applet_tag + "  <param NAME=\"SERVER\" VALUE=\"" + server_name + "\">\n";
            if(launch)
            {
                applet_tag = applet_tag + "  <param NAME=\"LAUNCH\" VALUE=\"true\">\n";
                if(parameters != null)
                {
                    parameters = parameters.replace('&', ' ');
                    applet_tag = applet_tag + "  <param NAME=\"PARAMETERS\" VALUE=\"" + parameters + "\">\n";
                }
            } else
            if(parameters != null && parameters.endsWith("/launch"))
            {
                applet_tag = applet_tag + "  <param NAME=\"LAUNCH\" VALUE=\"true\">\n";
                parameters = parameters.replace('&', ' ');
                applet_tag = applet_tag + "  <param NAME=\"PARAMETERS\" VALUE=\"" + parameters.substring(0, parameters.lastIndexOf("/launch")) + "\">\n";
            } else
            {
                applet_tag = applet_tag + "  <param NAME=\"LAUNCH\" VALUE=\"false\">\n";
            }
            if(cookies.size() > 0)
                if(cookies.size() == 1)
                {
                    cookie_data = (String)cookies.elementAt(0);
                    applet_tag = applet_tag + "  <PARAM NAME=\"COOKIE\" VALUE=\"" + cookie_data + "\">\n";
                } else
                {
                    for(int i = 0; i < cookies.size(); i++)
                    {
                        cookie_data = (String)cookies.elementAt(i);
                        applet_tag = applet_tag + "  <PARAM NAME=\"COOKIE" + (i + 1) + "\" VALUE=\"" + cookie_data + "\">\n";
                    }

                }
            Object obj = super.server.getUserAuthorization(super.request);
            if(obj == null)
                obj = ((Object) (new String("")));
            applet_tag = applet_tag + "  <param NAME=\"AUTH_OBJECT\" VALUE=\"" + obj.toString() + "\">\n";
            applet_tag = applet_tag + "  <param NAME=\"NEXT_PAGE\" VALUE=\"\">\n";
            applet_tag = applet_tag + "</applet>\n";
        }
        super.server.showInstallationPage(applet_tag, app, ver, super.request, super.response, launch);
    }

    protected String computeServerName(Object request)
    {
        Vector hosts = PropertyUtils.readVector(Servlet.properties, "deploy.server.processedhosts", ',');
        if(hosts == null || hosts.size() == 0)
            return super.server.getRequestUrl(request);
        InetAddress localhost = null;
        InetAddress requesthost = null;
        try
        {
            localhost = InetAddress.getLocalHost();
            URI requestURI = new URI(super.server.getRequestUrl(request));
            if(requestURI.getHost().equalsIgnoreCase("localhost") || requestURI.getHost().equalsIgnoreCase("127.0.0.1"))
                requesthost = localhost;
            else
                requesthost = InetAddress.getByName(requestURI.getHost());
        }
        catch(UnknownHostException e)
        {
            return super.server.getRequestUrl(request);
        }
        catch(ParseException e)
        {
            return super.server.getRequestUrl(request);
        }
        int size = hosts.size();
        for(int i = 0; i < size; i++)
            try
            {
                String currentHost = (String)hosts.elementAt(i);
                URI uri = new URI(currentHost);
                InetAddress testmachine = InetAddress.getByName(uri.getHost());
                if(requesthost.equals(((Object) (testmachine))))
                    return super.server.getRequestUrl(request);
                if(localhost.equals(((Object) (testmachine))))
                    return currentHost;
            }
            catch(ParseException e) { }
            catch(UnknownHostException e) { }

        return (String)hosts.elementAt(0);
    }

    protected void sendServerLoadPage()
    {
        double load[] = super.server.getServerLoad();
        long uptime = super.server.getUptime();
        long rawtime = uptime;
        long days = uptime / 0x5265c00L;
        uptime %= 0x5265c00L;
        long hours = uptime / 0x36ee80L;
        uptime %= 0x36ee80L;
        long minutes = uptime / 60000L;
        uptime %= 60000L;
        long seconds = uptime / 1000L;
        uptime %= 1000L;
        DecimalFormat df = (DecimalFormat)NumberFormat.getInstance();
        df.setMaximumFractionDigits(3);
        StringBuffer content = new StringBuffer();
        content.append("uptime=" + days + "." + hours + "." + minutes + "." + seconds + "." + uptime + "\n");
        content.append("uptime.text=days.hours.minutes.seconds.milliseconds\n");
        content.append("rawtime=" + rawtime + "\n");
        content.append("1.minute.average=" + ((NumberFormat) (df)).format(load[0]) + "\n");
        content.append("5.minute.average=" + ((NumberFormat) (df)).format(load[1]) + "\n");
        content.append("15.minute.average=" + ((NumberFormat) (df)).format(load[2]) + "\n");
        content.append("text=Up " + days + " day(s), " + hours + " hour(s), " + minutes + " minutes(s), ");
        content.append("load average: " + ((NumberFormat) (df)).format(load[0]) + ", ");
        content.append(((NumberFormat) (df)).format(load[1]) + ", ");
        content.append(((NumberFormat) (df)).format(load[2]));
        content.append("\n\nbuildid=DeploySam 2.5.0, Build: DD250-20021210-1039");
        ((AbstractWorker)this).sendText(content.toString(), "text/plain");
    }
}
