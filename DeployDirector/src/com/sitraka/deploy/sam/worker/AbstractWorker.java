// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AbstractWorker.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.common.MimeType;
import com.sitraka.deploy.common.Requests;
import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.app.AppFile;
import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.sam.AbstractTrackerEntry;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.ClientIdentifier;
import com.sitraka.deploy.sam.RequestParser;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.cache.CacheList;
import com.sitraka.deploy.sam.request.RequestList;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.sam.servlet.ServletNotFoundException;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.Hashcode;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            ReplicationThread, PolicyWorker, DiffWorker, FileWorker, 
//            GeneratedTextWorker, GeneratedPageWorker, LogWorker, UploadWorker, 
//            ReplicationWorker, DeleteWorker, FileNameParser

public abstract class AbstractWorker
    implements Requests
{
    protected class FileObject
    {

        protected File file;
        protected String filename;
        protected String hash;
        protected long offset;
        protected long size;
        protected boolean inJar;
        protected String parent;

        public String getHashcode()
        {
            return hash;
        }

        public String getName()
        {
            return filename;
        }

        public File getFile()
        {
            return file;
        }

        public long getOffset()
        {
            return offset;
        }

        public long getSize()
        {
            return size;
        }

        public boolean getInJar()
        {
            return inJar;
        }

        public String getParentsName()
        {
            return parent;
        }

        public void setHashcode(String h)
        {
            hash = h;
        }

        public void setName(String name)
        {
            filename = name;
        }

        public void setFile(File f)
        {
            file = f;
        }

        public void setOffset(long _offset)
        {
            offset = _offset;
        }

        public void setSize(long _size)
        {
            size = _size;
        }

        public void setinJar(boolean in)
        {
            inJar = in;
        }

        public void setParentsName(String _name)
        {
            parent = _name;
        }

        FileObject()
        {
            file = null;
            filename = null;
            hash = null;
            offset = -1L;
            size = -1L;
            inJar = false;
            parent = null;
        }

        FileObject(String name, File f)
        {
            file = null;
            filename = null;
            hash = null;
            offset = -1L;
            size = -1L;
            inJar = false;
            parent = null;
            filename = name;
            file = f;
            hash = null;
            offset = -1L;
            size = -1L;
            inJar = false;
            parent = null;
        }
    }


    public static final int TYPE_POLICY = 1;
    public static final int TYPE_DIFF = 2;
    public static final int TYPE_FILE = 3;
    public static final int TYPE_GENERATED_TEXT = 4;
    public static final int TYPE_GENERATED_PAGE = 5;
    public static final int TYPE_LOG = 6;
    public static final int TYPE_UPLOAD = 7;
    public static final int TYPE_REPLICATION = 8;
    public static final int TYPE_DELETE = 9;
    protected Object STATE_LOCK;
    private static final String REPLICATION_THREAD_FILE = "repstate";
    protected AppVault vault;
    protected Object request;
    protected Object response;
    protected ServerActions server;
    protected RequestParser parser;
    protected String server_sig;
    protected static int BUF_SIZE = 8192;
    protected boolean deleteOnExit;
    protected int type;
    private Hashtable jarFiles;
    protected String none;
    protected String origServer;
    public static ReplicationThread replication_queue = null;
    protected static Vector policy_pool;
    protected static Vector diff_pool;
    protected static Vector file_pool;
    protected static Vector generated_text_pool;
    protected static Vector generated_page_pool;
    protected static Vector log_pool;
    protected static Vector upload_pool;
    protected static Vector replication_pool;
    protected static Vector delete_pool;
    protected static Hashtable pools;
    private static Object QUEUE_LOCK = new Object();

    protected AbstractWorker()
    {
        STATE_LOCK = new Object();
        vault = null;
        request = null;
        response = null;
        server = null;
        parser = null;
        server_sig = null;
        deleteOnExit = false;
        type = 0;
        jarFiles = null;
        none = "(none)";
        origServer = null;
        unsetState();
    }

    public static final ReplicationThread getReplicationQueue(ServerActions server)
    {
        ReplicationThread replicationthread;
        synchronized(QUEUE_LOCK)
        {
            if(replication_queue == null)
            {
                File rep_file = new File(server.getVault().getServerEtcDir(), "repstate");
                replication_queue = new ReplicationThread(server, rep_file);
                ((Thread) (replication_queue)).setPriority(3);
                ((Thread) (replication_queue)).setName("ReplicationThread");
                ((Thread) (replication_queue)).setDaemon(true);
                ((Thread) (replication_queue)).start();
            }
            replicationthread = replication_queue;
        }
        return replicationthread;
    }

    public static final AbstractWorker getWorker(ServerActions server, int worker_type)
    {
        getReplicationQueue(server);
        return getWorkerFromPool(worker_type);
    }

    public static final AbstractWorker getWorker(ServerActions server, RequestParser parser, Object request, Object response)
        throws ServletProcessException
    {
        getReplicationQueue(server);
        AbstractWorker worker = null;
        int required_type = 0;
        switch(parser.getRequest())
        {
        case 2000: 
        case 2001: 
        case 2002: 
        case 2011: 
            required_type = 1;
            break;

        case 2003: 
        case 2007: 
            required_type = 2;
            break;

        case 2004: 
        case 2006: 
        case 2009: 
        case 2015: 
        case 3002: 
            required_type = 3;
            break;

        case 1000: 
        case 1001: 
        case 1002: 
        case 1003: 
        case 1004: 
        case 1005: 
        case 1006: 
        case 1010: 
        case 2005: 
        case 2012: 
        case 2016: 
        case 3001: 
        case 3102: 
        case 3201: 
            required_type = 4;
            break;

        case 2008: 
        case 2010: 
        case 2013: 
        case 2014: 
        case 3300: 
        case 4002: 
        case 4003: 
            required_type = 5;
            break;

        case 1008: 
        case 1009: 
            required_type = 6;
            break;

        case 3100: 
        case 3106: 
        case 3108: 
        case 3202: 
        case 3302: 
        case 3304: 
        case 3307: 
        case 3308: 
        case 3309: 
        case 3310: 
        case 3311: 
        case 3312: 
        case 3313: 
            required_type = 8;
            break;

        case 3103: 
        case 3104: 
        case 3105: 
        case 3109: 
        case 3203: 
        case 3303: 
        case 3305: 
        case 3306: 
        case 4000: 
        case 4001: 
            required_type = 7;
            break;

        case 3101: 
        case 3107: 
        case 3200: 
            required_type = 9;
            break;

        default:
            throw new ServletProcessException("Unknown request: " + parser.getCommand(), request, response, true);

        case 3301: 
            break;
        }
        worker = getWorkerFromPool(required_type);
        if(worker != null)
            worker.resetState(server, parser, request, response);
        return worker;
    }

    public abstract void startWorking()
        throws ServletProcessException, ServletNotFoundException;

    protected void setOrigServer()
    {
        origServer = parser.getServer();
        if(origServer == null)
            origServer = "DeployAdmin";
        else
            origServer = server.getServerURI(origServer);
    }

    public synchronized String getDescription()
    {
        StringBuffer b = new StringBuffer();
        b.append("Worker (ID #" + ((Object)this).hashCode() + ", ");
        b.append(" type: ");
        switch(type)
        {
        case 1: // '\001'
            b.append("PolicyWorker");
            break;

        case 2: // '\002'
            b.append("DiffWorker");
            break;

        case 3: // '\003'
            b.append("FileWorker");
            break;

        case 4: // '\004'
            b.append("GeneratedTextWorker");
            break;

        case 5: // '\005'
            b.append("GeneratedPageWorker");
            break;

        case 6: // '\006'
            b.append("LogWorker");
            break;

        case 7: // '\007'
            b.append("UploadWorker");
            break;

        case 8: // '\b'
            b.append("ReplicationWorker");
            break;

        case 9: // '\t'
            b.append("DeleteWorker");
            break;

        default:
            b.append("**UNKNOWN**");
            break;
        }
        b.append(")");
        return b.toString();
    }

    private void closeAllJars()
    {
        for(Enumeration e = jarFiles.keys(); e.hasMoreElements();)
            try
            {
                ZipFile f = (ZipFile)jarFiles.remove(e.nextElement());
                if(f != null)
                {
                    f.close();
                    f = null;
                }
            }
            catch(Exception ex) { }

    }

    private ZipFile openJarFile(File file)
        throws IOException
    {
        ZipFile result = null;
        String name = file.getAbsolutePath();
        if(file == null)
            throw new IOException("Provided file is <null>");
        if(!file.exists())
            throw new IOException("Specified jar file (" + file + ") does not exist");
        if(!file.canRead())
            throw new IOException("Cannot read jar file: " + file);
        if((result = (ZipFile)jarFiles.get(((Object) (name)))) != null)
        {
            return result;
        } else
        {
            result = new ZipFile(name);
            jarFiles.put(((Object) (name)), ((Object) (result)));
            return result;
        }
    }

    private InputStream getJarInputStream(File jar, String className)
        throws ServletProcessException
    {
        byte data[] = null;
        try
        {
            ZipFile f = openJarFile(jar);
            ZipEntry entry = f.getEntry(className);
            InputStream is = null;
            if(entry != null)
            {
                is = f.getInputStream(entry);
                data = readDataFromStream(is, entry.getSize());
                is.close();
                is = null;
            }
            entry = null;
        }
        catch(IOException ioe)
        {
            String text = "Cannot access file: " + jar.getAbsolutePath();
            throw new ServletProcessException(text, ((Throwable) (ioe)), "ServerExceptionError", request, response, true);
        }
        return ((InputStream) (new ByteArrayInputStream(data)));
    }

    private byte[] readDataFromStream(InputStream in, long expected_size)
        throws ServletProcessException
    {
        ByteArrayOutputStream out_buffer = new ByteArrayOutputStream();
        try
        {
            BufferedInputStream buffered_in = new BufferedInputStream(in);
            int n = -1;
            byte buffer[] = new byte[8096];
            while((n = buffered_in.read(buffer, 0, 8096)) != -1) 
                out_buffer.write(buffer, 0, n);
            return out_buffer.toByteArray();
        }
        catch(IOException ioe)
        {
            if(expected_size != -1L && expected_size == (long)out_buffer.size())
            {
                return out_buffer.toByteArray();
            } else
            {
                String text = "Error reading from input stream. ";
                throw new ServletProcessException(text, ((Throwable) (ioe)), "ServerExceptionError", request, response, true);
            }
        }
    }

    public void finishedWorking()
    {
        closeAllJars();
        unsetState();
        if(deleteOnExit)
            return;
        Vector pool = null;
        synchronized(pools)
        {
            pool = (Vector)pools.get(((Object) (new Integer(type))));
        }
        if(pool != null)
            synchronized(pool)
            {
                pool.addElement(((Object) (this)));
            }
    }

    protected void markRequestWithError(String errorText)
    {
        AbstractTrackerEntry obj = RequestList.getRequest(parser);
        if(obj == null)
            return;
        obj.setStatus(4);
        if(errorText == null)
            errorText = "Process aborted, no details are available";
        obj.setStatusText(errorText);
    }

    protected void printRequestObject(AbstractTrackerEntry obj)
    {
        server.setContentType(response, "text/plain");
        server.setContentLength(response, ((Object) (obj)).toString().length());
        server.setHeader(response, "Accept-Ranges", "none");
        server.setHeader(response, "DeploySam-ID", "" + obj.getID());
        server.setHeader(response, "DeploySam-StatusText", obj.getStatusText());
        server.setHeader(response, "DeploySam-StatusCode", "" + obj.getStatus());
        server.setHeader(response, "Cache-Control", "no-cache");
        server.setHeader(response, "Expires", (new Date(System.currentTimeMillis() - 0xf4240L)).toString());
        server.setHeader(response, "Last-Modified", (new Date()).toString());
        PrintWriter out = server.getTextOutputStream(response);
        out.print(((Object) (obj)).toString());
        out.flush();
        out.close();
        response = null;
    }

    protected boolean isRequestInProgress()
        throws ServletProcessException, ServletNotFoundException
    {
        boolean inProgress = false;
        AbstractTrackerEntry obj = RequestList.getRequest(parser);
        if(obj == null)
        {
            obj = ((AbstractTrackerEntry) (RequestList.addRequest(parser, ((File) (null)))));
            obj.setStatusText("Has not yet been started");
            obj.setStatus(1);
        } else
        {
            inProgress = true;
            if(obj.getStatus() == 4)
            {
                obj.removeReference();
                String text = "Error, unable to complete request:\n" + obj.getStatusText();
                throw new ServletProcessException(text, request, response, true);
            }
            obj.updateAccess();
            if(obj.getStatus() == 3)
            {
                if(obj.getText() != null)
                    sendText(obj.getText().toString());
                else
                    sendSingleFile(obj.getFile());
                RequestList.removeReference(obj);
                return true;
            }
            if(parser.getRequestID() == null)
                RequestList.addReference(obj);
        }
        printRequestObject(obj);
        return inProgress;
    }

    protected void setDeleteOnExit(boolean new_value)
    {
        deleteOnExit = new_value;
    }

    public static final AbstractWorker getWorkerFromPool(int worker_type)
    {
        AbstractWorker worker = null;
        Vector pool = null;
        synchronized(pools)
        {
            pool = (Vector)pools.get(((Object) (new Integer(worker_type))));
        }
        if(pool != null)
            synchronized(pool)
            {
                if(pool.size() == 0)
                {
                    worker = createWorker(worker_type);
                    worker.setDeleteOnExit(true);
                } else
                {
                    worker = (AbstractWorker)pool.elementAt(0);
                    pool.removeElementAt(0);
                }
            }
        return worker;
    }

    protected static final AbstractWorker createWorker(int worker_type)
    {
        AbstractWorker worker = null;
        switch(worker_type)
        {
        case 1: // '\001'
            worker = ((AbstractWorker) (new PolicyWorker()));
            break;

        case 2: // '\002'
            worker = ((AbstractWorker) (new DiffWorker()));
            break;

        case 3: // '\003'
            worker = ((AbstractWorker) (new FileWorker()));
            break;

        case 4: // '\004'
            worker = ((AbstractWorker) (new GeneratedTextWorker()));
            break;

        case 5: // '\005'
            worker = ((AbstractWorker) (new GeneratedPageWorker()));
            break;

        case 6: // '\006'
            worker = ((AbstractWorker) (new LogWorker()));
            break;

        case 7: // '\007'
            worker = ((AbstractWorker) (new UploadWorker()));
            break;

        case 8: // '\b'
            worker = ((AbstractWorker) (new ReplicationWorker()));
            break;

        case 9: // '\t'
            worker = ((AbstractWorker) (new DeleteWorker()));
            break;
        }
        return worker;
    }

    protected void unsetState()
    {
        synchronized(STATE_LOCK)
        {
            request = null;
            response = null;
            parser = null;
            server = null;
            vault = null;
            server_sig = null;
            jarFiles = null;
        }
    }

    protected void resetState(ServerActions server, RequestParser parser, Object request, Object response)
    {
        synchronized(STATE_LOCK)
        {
            this.request = request;
            this.response = response;
            this.parser = parser;
            this.server = server;
            vault = server.getVault();
            server_sig = Hashcode.computeMD5Hash(vault.listServers());
            jarFiles = new Hashtable();
        }
    }

    public synchronized Version getVersion()
    {
        String appname = parser.getApplication();
        String vername = parser.getVersion();
        return getVersion(appname, vername);
    }

    protected synchronized Version getVersion(String appname, String vername)
    {
        if(appname == null || vername == null)
            return null;
        Application app = vault.getApplication(appname);
        if(app == null)
        {
            return null;
        } else
        {
            Version ver = app.getVersion(vername);
            return ver;
        }
    }

    protected synchronized AppVault getVault()
    {
        return server.getVault();
    }

    protected String ensureFilesInVault(String filenames)
    {
        if(filenames == null || filenames.indexOf("..") == -1)
            return filenames;
        StringBuffer result = new StringBuffer();
        Vector files = PropertyUtils.readVector(filenames, ',');
        for(int i = 0; i < files.size(); i++)
        {
            String file = (String)files.elementAt(i);
            if(file.indexOf("..") == -1)
            {
                if(result.length() > 0)
                    result.append(",");
                result.append(file);
            }
        }

        return result.toString();
    }

    protected String getFilenames()
    {
        String filename = null;
        int length = server.getContentLength(request);
        if(length > 0)
        {
            InputStream fis = server.getBinaryInputStream(request);
            StringBuffer b = new StringBuffer();
            try
            {
                int n = -1;
                int total = 0;
                byte buff[] = new byte[BUF_SIZE];
                while((n = fis.read(buff, 0, BUF_SIZE)) != -1) 
                {
                    b.append(new String(buff, 0, n));
                    total += n;
                    if(total >= length)
                        break;
                }
            }
            catch(IOException ioe) { }
            filename = FileUtils.makeInternalPath(b.toString());
        } else
        {
            filename = FileUtils.makeInternalPath(parser.getFile());
        }
        if(filename == null)
        {
            return null;
        } else
        {
            filename = ensureFilesInVault(filename);
            parser.setFile(filename);
            return filename;
        }
    }

    protected void printHeaders(String type, long length)
    {
        server.setContentType(response, type);
        server.setHeader(response, "Host", server.getServerURI(request));
        long now = System.currentTimeMillis();
        Date last_mod = new Date(now - 1L);
        Date expires = new Date(now);
        server.setHeader(response, "Pragma", "no-cache");
        server.setContentLength(response, (new Long(length)).intValue());
        server.setHeader(response, "DeployCAM-Servers", server_sig);
        server.setHeader(response, "Accept-Ranges", "none");
        server.setHeader(response, "Last-Modified", last_mod.toString());
        server.setHeader(response, "Expires", expires.toString());
    }

    protected void printHeaders(File file)
    {
        String type = MimeType.getType(file.getAbsolutePath());
        printHeaders(type, file.length());
    }

    protected synchronized void sendText(String content)
    {
        sendText(content, "text/plain");
    }

    protected synchronized void sendText(String content, String type)
    {
        printHeaders(type, content.length());
        String hash = Hashcode.computeMD5Hash(content);
        server.setHeader(response, "Content-MD5", hash);
        PrintWriter out = server.getTextOutputStream(response);
        out.print(content);
        out.flush();
        out.close();
    }

    protected synchronized void sendTextFile(File file)
        throws ServletProcessException, ServletNotFoundException
    {
        sendSingleFile(file, -1L);
    }

    protected synchronized void sendSingleFile(File file)
        throws ServletProcessException, ServletNotFoundException
    {
        sendSingleFile(file, -1L);
    }

    protected synchronized void sendSingleFile(File file, long offset)
        throws ServletProcessException, ServletNotFoundException
    {
        if(!file.exists() || !file.canRead())
        {
            String text = "Cannot access file: " + file.getName();
            throw new ServletNotFoundException(text, request, response, true);
        }
        FileInputStream is = null;
        try
        {
            is = new FileInputStream(file);
            if(offset != -1L && offset < file.length())
            {
                long skipped = is.skip(offset);
                if(skipped != offset)
                {
                    is.close();
                    is = new FileInputStream(file);
                    server.logMessage(request, "ClientRequestFailed", parser.getVersion(), parser.getApplication(), "Unable to seek " + offset + " bytes into file " + file.getAbsolutePath() + " sending entire file.");
                }
            }
        }
        catch(FileNotFoundException fnfe)
        {
            String text = "Cannot access file: " + file.getName();
            throw new ServletNotFoundException(text, ((Throwable) (fnfe)), "ServerExceptionError", request, response, true);
        }
        catch(IOException ioe)
        {
            String text = "Cannot access file: " + file.getName();
            throw new ServletProcessException(text, ((Throwable) (ioe)), "ServerExceptionError", request, response, true);
        }
        if(offset == -1L)
        {
            printHeaders(file);
        } else
        {
            long length = file.length() - offset;
            printHeaders(MimeType.getType(file.getAbsolutePath()), length);
        }
        server.setHeader(response, "Content-Disposition", "inline; filename=" + file.getName());
        OutputStream os = server.getBinaryOutputStream(response);
        sendData(os, ((InputStream) (is)));
    }

    protected synchronized void sendSingleFileAsZip(File file, String name)
        throws ServletProcessException, ServletNotFoundException
    {
        if(!file.exists() || !file.canRead())
        {
            String text = "Cannot access file: " + file.getName();
            throw new ServletProcessException(text, request, response, true);
        } else
        {
            Hashtable table = new Hashtable();
            FileObject fo = new FileObject(name, file);
            fo.setHashcode(Hashcode.computeHash(file));
            fo.setSize(file.length());
            table.put(((Object) (name)), ((Object) (fo)));
            sendMultipleFiles(table);
            return;
        }
    }

    protected synchronized void sendData(OutputStream out, InputStream is)
        throws ServletProcessException
    {
        byte buf[] = new byte[BUF_SIZE];
        try
        {
            int i;
            while((i = is.read(buf)) > 0) 
                out.write(buf, 0, i);
            is.close();
        }
        catch(IOException ioe)
        {
            boolean log = true;
            if(((Throwable) (ioe)).getMessage().indexOf("Connection reset by peer") != -1)
                log = false;
            ServletProcessException spe = new ServletProcessException(getDescription(), ((Throwable) (ioe)), "ServerExceptionError", request, response, log);
            spe.setShowErrorPage(false);
            throw spe;
        }
        finally
        {
            is = null;
            buf = null;
        }
    }

    protected void sendDirectory(File base)
        throws ServletProcessException, ServletNotFoundException
    {
        Hashtable fileTable = new Hashtable();
        scanDirectory(base, base, fileTable);
        sendMultipleFiles(fileTable);
    }

    protected void sendDirectoryAsDar(File base)
        throws ServletProcessException, ServletNotFoundException
    {
        Hashtable fileTable = new Hashtable();
        scanDirectory(base, base, fileTable, false);
        sendDARFile(fileTable);
    }

    protected void scanDirectory(File base, File current_dir, Hashtable fileTable)
    {
        scanDirectory(base, current_dir, fileTable, true);
    }

    protected void scanDirectory(File base, File current_dir, Hashtable fileTable, boolean sendBase)
    {
        if(!base.canRead() || !current_dir.canRead())
            return;
        if(!base.isDirectory() || !current_dir.isDirectory())
            return;
        String files[] = current_dir.list();
        if(files.length == 0)
            return;
        String base_path = base.getAbsolutePath();
        String current_path = current_dir.getAbsolutePath();
        String prefix = null;
        if(current_path.equals(((Object) (base_path))))
        {
            prefix = !sendBase ? "" : base.getName();
        } else
        {
            prefix = !sendBase ? "" : base.getName() + File.separator;
            prefix = prefix + current_path.substring(base_path.length() + 1);
        }
        for(int i = 0; i < files.length; i++)
        {
            String current_file = files[i];
            File f = new File(current_dir, current_file);
            String filename = null;
            if(prefix.equals(""))
                filename = current_file;
            else
                filename = prefix + File.separator + current_file;
            FileObject fo = new FileObject(filename, f);
            if(f.isDirectory())
                fo.setHashcode("");
            else
                fo.setHashcode(Hashcode.computeHash(f));
            fo.setSize(f.length());
            fileTable.put(((Object) (filename)), ((Object) (fo)));
            if(f.isDirectory())
                scanDirectory(base, f, fileTable, sendBase);
        }

    }

    protected String createPropertiesFile(FileObject files[])
    {
        long uncompressed_size = 0L;
        Properties props = new Properties();
        for(int i = 0; i < files.length; i++)
        {
            String key = "file" + i + ".";
            String name = files[i].getName();
            name = FileUtils.makeInternalPath(name);
            ((Hashtable) (props)).put(((Object) (key + "name")), ((Object) (name)));
            ((Hashtable) (props)).put(((Object) (key + "size")), ((Object) ("" + files[i].getSize())));
            uncompressed_size += files[i].getSize();
            ((Hashtable) (props)).put(((Object) (key + "hash")), ((Object) (files[i].getHashcode())));
            if(files[i].getInJar())
            {
                String parent = files[i].getParentsName();
                parent = FileUtils.makeInternalPath(parent);
                ((Hashtable) (props)).put(((Object) (key + "parent")), ((Object) (parent)));
            }
        }

        ((Hashtable) (props)).put("deploy.uncompressedsize", ((Object) ("" + uncompressed_size)));
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try
        {
            props.store(((OutputStream) (out)), "DeploySam: FILE PROPERTIES");
        }
        catch(IOException ioe) { }
        return out.toString();
    }

    public void addFileObjectToZipStream(ZipOutputStream zipper, FileObject fo)
        throws ServletProcessException
    {
        InputStream is = null;
        String parent = "";
        try
        {
            if(fo.getInJar())
            {
                parent = fo.getParentsName() + "/";
                ZipFile jar = openJarFile(fo.getFile());
                if(jar == null)
                {
                    String text = "Cannot create JarFile from: " + fo.getFile();
                    markRequestWithError(text);
                    throw new ServletProcessException(text, request, response);
                }
                ZipEntry e = jar.getEntry(fo.getName());
                if(e == null)
                {
                    String text = "Cannot obtain obtain jar entry for" + fo.getName() + " from " + fo.getFile();
                    markRequestWithError(text);
                    ServletProcessException spe = new ServletProcessException(text, "ClientRequestFailed", request, response, true);
                    spe.setApplication("(unknown)");
                    spe.setVersion("(unknown)");
                    throw spe;
                }
                if(e.isDirectory())
                {
                    FileUtils.addToZipStream(zipper, (InputStream)null, parent + fo.getName());
                    return;
                }
                is = getJarInputStream(fo.getFile(), fo.getName());
                if(is == null)
                {
                    String text = "Cannot obtain input stream from jar entry " + e.getName() + " in " + fo.getFile();
                    markRequestWithError(text);
                    ServletProcessException spe = new ServletProcessException(text, "ClientRequestFailed", request, response, true);
                    spe.setApplication("(unknown)");
                    spe.setVersion("(unknown)");
                    throw spe;
                }
            } else
            if(!fo.getFile().isDirectory())
            {
                String name = fo.getFile().getAbsolutePath().toLowerCase();
                if(name.endsWith(".jar") || name.endsWith(".zip") || name.endsWith(".gif"))
                    zipper.setLevel(0);
                is = ((InputStream) (new FileInputStream(fo.getFile())));
            } else
            {
                is = null;
            }
        }
        catch(IOException ioe)
        {
            String text = "Error processing requested file \"" + fo.getFile() + "\" download aborted.";
            markRequestWithError(text);
            throw new ServletProcessException(text, ((Throwable) (ioe)), "ServerExceptionError", request, response, true);
        }
        try
        {
            if(is != null && fo.getOffset() != -1L && fo.getOffset() < fo.getSize())
            {
                long skipped = is.skip(fo.getOffset());
                if(skipped != fo.getOffset())
                {
                    String text = "Unable to seek " + fo.getOffset() + " bytes into" + " file " + fo.getFile().getAbsolutePath() + ". Download aborted.";
                    markRequestWithError(text);
                    is.close();
                    throw new ServletProcessException(text, request, response, true);
                }
            }
            FileUtils.addToZipStream(zipper, is, parent + fo.getName());
            if(is != null)
                is.close();
            ((FilterOutputStream) (zipper)).flush();
        }
        catch(IOException ioe)
        {
            String text = "Aborting file transfer:  " + ((Throwable) (ioe)).getMessage();
            markRequestWithError(text);
            throw new ServletProcessException(text, ((Throwable) (ioe)), "ServerExceptionError", request, response, true);
        }
    }

    protected String stripURL(String url, String ending)
    {
        int start = url.indexOf("://");
        if(start != -1)
        {
            int tmp_start = url.indexOf("/", start + 3);
            if(tmp_start == -1)
                start += 3;
            else
                start = tmp_start;
        } else
        {
            start = 0;
        }
        if(ending == null)
            ending = "";
        int end = url.toLowerCase().lastIndexOf(ending);
        if(ending.equals("") || end == -1)
            url = url.substring(start);
        else
            url = url.substring(start, end);
        return url;
    }

    protected File createWriteableTempFile()
    {
        File tmpfile = null;
        FileOutputStream fos = null;
        String locations[] = {
            vault.getServerTempDir().getAbsolutePath(), vault.getVaultBaseDir().getAbsolutePath() + File.separator + "dd", vault.getVaultBaseDir().getAbsolutePath(), null
        };
        for(int i = 0; i < locations.length;)
            try
            {
                if(locations[i] == null)
                    tmpfile = FileUtils.createTempFile();
                else
                    tmpfile = FileUtils.createTempFile(locations[i]);
                fos = new FileOutputStream(tmpfile);
                fos.close();
                fos = null;
                if(tmpfile.exists())
                    tmpfile.delete();
                return tmpfile;
            }
            catch(Exception e)
            {
                i++;
            }

        return null;
    }

    protected int getClientCompressionLevel()
    {
        return getCompressionLevel("deploy.compress.client", 9);
    }

    protected int getServerCompressionLevel()
    {
        return getCompressionLevel("deploy.compress.server", 0);
    }

    protected int getCompressionLevel(String compressionProperty, int defaultCompressionLevel)
    {
        int compressionLevel = PropertyUtils.readInt(Servlet.properties, compressionProperty, defaultCompressionLevel);
        if(compressionLevel < 0 || compressionLevel > 9)
            compressionLevel = defaultCompressionLevel;
        return compressionLevel;
    }

    protected int getCompressionLevel()
    {
        int compressionLevel = 0;
        if(parser.isAdministrator() || ClientIdentifier.identifyClientType(request) == 1)
            compressionLevel = getServerCompressionLevel();
        else
            compressionLevel = getClientCompressionLevel();
        return compressionLevel;
    }

    protected void sendMultipleFiles(FileObject files[])
        throws ServletProcessException, ServletNotFoundException
    {
        if(files == null)
            throw new ServletNotFoundException("No files passed to send multiple", request, response);
        File tmpfile = null;
        FileOutputStream fos = null;
        try
        {
            tmpfile = createWriteableTempFile();
            if(tmpfile == null)
            {
                String text = "Could not create writeable temp file in " + vault.getServerTempDir();
                markRequestWithError(text);
                throw new ServletProcessException(text, "ClientRequestFailed", request, response, true);
            }
            fos = new FileOutputStream(tmpfile);
        }
        catch(IOException ioe)
        {
            markRequestWithError(getDescription());
            tmpfile.delete();
            throw new ServletProcessException(getDescription(), ((Throwable) (ioe)), "ServerExceptionError", request, response, true);
        }
        String propvalues = createPropertiesFile(files);
        if(propvalues == null)
        {
            String msg = "Unable to create property file for requested files";
            markRequestWithError(msg);
            throw new ServletProcessException(msg, request, response, true);
        }
        ZipOutputStream zipper = new ZipOutputStream(((OutputStream) (fos)));
        zipper.setLevel(getCompressionLevel());
        FileUtils.addToZipStream(zipper, propvalues, "deploy.properties");
        try
        {
            for(int i = 0; i < files.length; i++)
            {
                zipper.setLevel(getCompressionLevel());
                addFileObjectToZipStream(zipper, files[i]);
                files[i] = null;
            }

        }
        catch(ServletProcessException spe)
        {
            try
            {
                zipper.close();
                fos.close();
                fos = null;
            }
            catch(IOException ioe) { }
            tmpfile.delete();
            throw spe;
        }
        try
        {
            ((FilterOutputStream) (zipper)).flush();
            zipper.finish();
            zipper.close();
            fos.close();
            fos = null;
        }
        catch(IOException ioe)
        {
            tmpfile.delete();
            throw new ServletProcessException(getDescription(), ((Throwable) (ioe)), "ServerExceptionError", request, response, true);
        }
        AbstractTrackerEntry obj = RequestList.getRequest(parser);
        if(obj != null)
        {
            response = null;
            obj.setFile(tmpfile);
            obj.setStatusText("Completed at: " + new Date(System.currentTimeMillis()));
            obj.setStatus(3);
            obj.updateAccess();
            if(!parser.isAdministrator() && CacheList.addRequest(obj))
            {
                RequestList.removeRequest(obj);
                if(tmpfile.canRead())
                    tmpfile.delete();
            }
            return;
        }
        if(response == null)
            return;
        server.setHeader(response, "Host", server.getServerURI(request));
        server.setContentType(response, "application/zip");
        server.setContentLength(response, (new Long(tmpfile.length())).intValue());
        server.setHeader(response, "Content-Disposition", "inline; filename=file.zip");
        server.setHeader(response, "Accept-Ranges", "none");
        try
        {
            FileInputStream fis = new FileInputStream(tmpfile);
            sendData(server.getBinaryOutputStream(response), ((InputStream) (fis)));
            fis.close();
            fis = null;
        }
        catch(FileNotFoundException fnfe)
        {
            String msg = "Could not find the temp file we were using to send files to the client";
            ServletProcessException spe = new ServletProcessException(msg, ((Throwable) (fnfe)), "ServerExceptionError", request, response, true);
            spe.setShowErrorPage(false);
            throw spe;
        }
        catch(IOException ioe)
        {
            String msg = "Error copying the cached file to the response.";
            ServletProcessException spe = new ServletProcessException(msg, ((Throwable) (ioe)), request, response, true);
            spe.setShowErrorPage(false);
            throw spe;
        }
        tmpfile.delete();
        if(parser.getRequest() == 2006 || parser.getRequest() == 2015 || parser.getRequest() == 2004)
            throw new IllegalArgumentException("Illegal code path, DIST or FILE request has not been added to RequestList");
        else
            return;
    }

    protected void sendDARFile(FileObject files[])
        throws ServletProcessException, ServletNotFoundException
    {
        if(files == null)
            throw new ServletNotFoundException("No files passed to sendDARFile", request, response);
        File tmpfile = null;
        FileOutputStream fos = null;
        try
        {
            tmpfile = createWriteableTempFile();
            if(tmpfile == null)
            {
                String text = "Could not write temp file to " + vault.getServerTempDir();
                markRequestWithError(text);
                throw new ServletProcessException(text, "ClientRequestFailed", request, response, true);
            }
            fos = new FileOutputStream(tmpfile);
        }
        catch(IOException ioe)
        {
            markRequestWithError(getDescription());
            tmpfile.delete();
            throw new ServletProcessException(getDescription(), ((Throwable) (ioe)), "ServerExceptionError", request, response, true);
        }
        ZipOutputStream zipper = new ZipOutputStream(((OutputStream) (fos)));
        zipper.setLevel(getCompressionLevel());
        FileUtils.addToZipStream(zipper, parser.getApplication(), "META-INF" + File.separator + "bundlename.txt");
        try
        {
            for(int i = 0; i < files.length; i++)
            {
                zipper.setLevel(getCompressionLevel());
                addFileObjectToZipStream(zipper, files[i]);
                files[i] = null;
            }

        }
        catch(ServletProcessException spe)
        {
            try
            {
                zipper.close();
                fos.close();
                fos = null;
            }
            catch(IOException ioe) { }
            tmpfile.delete();
            throw spe;
        }
        try
        {
            ((FilterOutputStream) (zipper)).flush();
            zipper.finish();
            zipper.close();
            fos.close();
            fos = null;
        }
        catch(IOException ioe)
        {
            tmpfile.delete();
            throw new ServletProcessException(getDescription(), ((Throwable) (ioe)), "ServerExceptionError", request, response, true);
        }
        AbstractTrackerEntry obj = RequestList.getRequest(parser);
        if(obj != null)
        {
            response = null;
            obj.setFile(tmpfile);
            obj.setStatusText("Completed at: " + new Date(System.currentTimeMillis()));
            obj.setStatus(3);
            obj.updateAccess();
            if(!parser.isAdministrator() && CacheList.addRequest(obj))
            {
                RequestList.removeRequest(obj);
                if(tmpfile.canRead())
                    tmpfile.delete();
            }
            return;
        }
        if(response == null)
            return;
        server.setHeader(response, "Host", server.getServerURI(request));
        server.setContentType(response, "application/zip");
        server.setContentLength(response, (new Long(tmpfile.length())).intValue());
        server.setHeader(response, "Content-Disposition", "inline; filename=file.zip");
        server.setHeader(response, "Accept-Ranges", "none");
        try
        {
            FileInputStream fis = new FileInputStream(tmpfile);
            sendData(server.getBinaryOutputStream(response), ((InputStream) (fis)));
            fis.close();
            fis = null;
        }
        catch(FileNotFoundException fnfe)
        {
            String text = "Could not find the temp file we were using to send files to the client.";
            ServletProcessException spe = new ServletProcessException(text, ((Throwable) (fnfe)), "ServerExceptionError", request, response, true);
            spe.setShowErrorPage(false);
            throw spe;
        }
        catch(IOException ioe)
        {
            String msg = "Error copying the cached file to the response.";
            ServletProcessException spe = new ServletProcessException(msg, ((Throwable) (ioe)), request, response, true);
            spe.setShowErrorPage(false);
            throw spe;
        }
        finally
        {
            tmpfile.delete();
        }
    }

    protected void sendMultipleFiles(Hashtable fileTable)
        throws ServletProcessException, ServletNotFoundException
    {
        if(fileTable == null)
            throw new ServletNotFoundException("No files passed to sendMultiple", request, response);
        FileObject files[] = new FileObject[fileTable.size()];
        Enumeration keys = fileTable.keys();
        int i = 0;
        while(keys.hasMoreElements()) 
        {
            String filename = (String)keys.nextElement();
            FileObject f = (FileObject)fileTable.get(((Object) (filename)));
            files[i++] = f;
        }
        sendMultipleFiles(files);
    }

    protected void sendDARFile(Hashtable fileTable)
        throws ServletProcessException, ServletNotFoundException
    {
        if(fileTable == null)
            throw new ServletNotFoundException("No files passed to sendDARFile", request, response);
        File tmpVersionXml = createWriteableTempFile();
        FileObject files[] = new FileObject[fileTable.size()];
        Enumeration keys = fileTable.keys();
        int i = 0;
        boolean haveVersionXML = false;
        while(keys.hasMoreElements()) 
        {
            String filename = (String)keys.nextElement();
            FileObject f = (FileObject)fileTable.get(((Object) (filename)));
            if(!haveVersionXML && filename.endsWith("version.xml"))
            {
                try
                {
                    Version version = new Version(f.getFile());
                    version.saveXMLToFile(tmpVersionXml, false);
                }
                catch(XmlException xe)
                {
                    String msg = "Unable to parse version.xml: " + f.getFile();
                    throw new ServletNotFoundException(msg, request, response);
                }
                catch(IOException ioe)
                {
                    String msg = "Unable to save new version.xml: " + tmpVersionXml;
                    throw new ServletNotFoundException(msg, request, response);
                }
                f.setFile(tmpVersionXml);
                f.setName("META-INF" + File.separator + "version.xml");
                haveVersionXML = true;
            }
            files[i++] = f;
        }
        sendDARFile(files);
        tmpVersionXml.delete();
    }

    protected synchronized Hashtable buildFileTable(String appName, String verName)
        throws ServletProcessException, ServletNotFoundException
    {
        if(appName == null || verName == null)
        {
            String msg = "Unable to generate file table:  ";
            if(appName == null)
                msg = msg + "Application name is null.";
            else
                msg = msg + "Version name is null.";
            throw new ServletNotFoundException(msg, request, response, true);
        }
        Version ver = getVersion(appName, verName);
        if(ver == null)
        {
            String msg = "Unable to generate file table:  No version found for " + appName + " " + verName;
            throw new ServletNotFoundException(msg, request, response, true);
        }
        String platform = parser.getPlatform();
        if(platform == null)
            platform = "all";
        AbstractTrackerEntry obj = RequestList.getRequest(parser);
        if(obj != null)
        {
            obj.setStatus(2);
            obj.setStatusText("Request is in progress");
        }
        Vector files = ver.getFiles(platform);
        String list = "";
        for(int i = 0; i < files.size(); i++)
        {
            AppFile file = (AppFile)files.elementAt(i);
            String name = FileUtils.makeInternalPath(file.getName());
            list = list + name + ",";
        }

        return buildFileTable(appName, verName, list);
    }

    protected synchronized Hashtable buildFileTable(String appName, String verName, String filelist)
        throws ServletProcessException
    {
        Version ver = getVersion(appName, verName);
        String platform = parser.getPlatform();
        if(platform == null)
            platform = "all";
        if(ver == null && !"DeploySam".equals(((Object) (platform))))
        {
            String text = "Version " + verName + " of " + appName + " does not exist";
            markRequestWithError(text);
            ServletProcessException spe = new ServletProcessException(text, "ClientRequestFailed", request, response, true);
            spe.setApplication(appName);
            spe.setVersion(verName);
            throw spe;
        }
        StringTokenizer toker = new StringTokenizer(filelist, ",");
        Hashtable fileTable = new Hashtable();
        Vector appFiles = null;
        AbstractTrackerEntry obj = RequestList.getRequest(parser);
        if(obj != null)
        {
            obj.setStatus(2);
            obj.setStatusText("Request is in progress");
        }
        if(!"DeploySam".equals(((Object) (platform))))
            appFiles = ver.getFiles(platform);
        while(toker.hasMoreTokens()) 
        {
            String filename = toker.nextToken();
            if(filename == null)
                break;
            String parent_name = FileNameParser.extractParentFile(filename);
            String sub_name = FileNameParser.extractChildFile(filename);
            File file = null;
            if(!"DeploySam".equals(((Object) (platform))))
                file = vault.getApplicationFile(appName, verName, parent_name, platform);
            else
                file = vault.getAbsoluteFile(parent_name);
            if(file == null || !file.canRead())
            {
                String text = "File: " + parent_name + " does not exist in " + appName + ", version " + verName;
                markRequestWithError(text);
                ServletProcessException spe = new ServletProcessException(text, "ClientRequestFailed", request, response, true);
                spe.setApplication(appName);
                spe.setVersion(verName);
                throw spe;
            }
            if(!"DeploySam".equals(((Object) (platform))))
            {
                AppFile af = ver.findAppFile(platform, parent_name, sub_name);
                if(af == null)
                {
                    String text = "File: " + parent_name + " does not exist in " + appName + ", version " + verName;
                    markRequestWithError(text);
                    ServletProcessException spe = new ServletProcessException(text, "ClientRequestFailed", request, response, true);
                    spe.setApplication(appName);
                    spe.setVersion(verName);
                    throw spe;
                }
                long offset = FileNameParser.extractOffset(filename);
                FileObject fo = new FileObject(af.getName(), file);
                fo.setHashcode(af.getHash());
                fo.setOffset(offset);
                fo.setSize(af.getSize().longValue());
                String unique_name = null;
                if(sub_name == null)
                {
                    unique_name = af.getName();
                    fo.setinJar(false);
                } else
                {
                    unique_name = parent_name + "%%" + sub_name;
                    fo.setinJar(true);
                    fo.setParentsName(parent_name);
                }
                fileTable.put(((Object) (unique_name)), ((Object) (fo)));
            } else
            {
                FileObject fo = new FileObject(filename, file);
                fo.setHashcode(Hashcode.computeHash(file));
                fo.setSize(file.length());
                fo.setinJar(false);
                fileTable.put(((Object) (filename)), ((Object) (fo)));
            }
        }
        return fileTable;
    }

    static 
    {
        policy_pool = null;
        diff_pool = null;
        file_pool = null;
        generated_text_pool = null;
        generated_page_pool = null;
        log_pool = null;
        upload_pool = null;
        replication_pool = null;
        delete_pool = null;
        pools = null;
        pools = new Hashtable();
        policy_pool = new Vector(5);
        for(int i = 0; i < 5; i++)
            policy_pool.addElement(((Object) (createWorker(1))));

        diff_pool = new Vector(5);
        for(int i = 0; i < 5; i++)
            diff_pool.addElement(((Object) (createWorker(2))));

        file_pool = new Vector(10);
        for(int i = 0; i < 10; i++)
            file_pool.addElement(((Object) (createWorker(3))));

        generated_text_pool = new Vector(10);
        for(int i = 0; i < 10; i++)
            generated_text_pool.addElement(((Object) (createWorker(4))));

        generated_page_pool = new Vector(5);
        for(int i = 0; i < 5; i++)
            generated_page_pool.addElement(((Object) (createWorker(5))));

        log_pool = new Vector(5);
        for(int i = 0; i < 5; i++)
            log_pool.addElement(((Object) (createWorker(6))));

        upload_pool = new Vector(5);
        for(int i = 0; i < 5; i++)
            upload_pool.addElement(((Object) (createWorker(7))));

        replication_pool = new Vector(5);
        for(int i = 0; i < 5; i++)
            replication_pool.addElement(((Object) (createWorker(8))));

        delete_pool = new Vector(5);
        for(int i = 0; i < 5; i++)
            delete_pool.addElement(((Object) (createWorker(9))));

        pools.put(((Object) (new Integer(1))), ((Object) (policy_pool)));
        pools.put(((Object) (new Integer(2))), ((Object) (diff_pool)));
        pools.put(((Object) (new Integer(3))), ((Object) (file_pool)));
        pools.put(((Object) (new Integer(4))), ((Object) (generated_text_pool)));
        pools.put(((Object) (new Integer(5))), ((Object) (generated_page_pool)));
        pools.put(((Object) (new Integer(6))), ((Object) (log_pool)));
        pools.put(((Object) (new Integer(7))), ((Object) (upload_pool)));
        pools.put(((Object) (new Integer(8))), ((Object) (replication_pool)));
        pools.put(((Object) (new Integer(9))), ((Object) (delete_pool)));
    }
}
