// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   InstallerReplicationMessage.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.common.checkpoint.InstallerCheckpoint;
import com.sitraka.deploy.common.checkpoint.InstallerObject;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            ReplicationMessage

public class InstallerReplicationMessage extends ReplicationMessage
{

    public InstallerReplicationMessage(String remote_server, int type, InstallerObject installerObject, InstallerCheckpoint checkpoint, Object _auth, String otherServers)
    {
        super(remote_server, type, ((Object) (installerObject)), ((com.sitraka.deploy.common.checkpoint.Checkpoint) (checkpoint)), _auth, otherServers);
    }

    public InstallerObject getInstallerObject()
    {
        return (InstallerObject)((ReplicationMessage)this).getUserData();
    }

    public InstallerCheckpoint getInstallerCheckpoint()
    {
        return (InstallerCheckpoint)((ReplicationMessage)this).getCheckpoint();
    }

    public String getFilename()
    {
        return getInstallerObject().getFilename();
    }
}
