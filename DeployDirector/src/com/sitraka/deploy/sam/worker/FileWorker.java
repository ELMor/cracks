// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FileWorker.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.sam.AbstractTrackerEntry;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.LogQueue;
import com.sitraka.deploy.sam.RequestParser;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.cache.CacheList;
import com.sitraka.deploy.sam.servlet.ServletNotFoundException;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            AbstractWorker, FileNameParser

public class FileWorker extends AbstractWorker
{

    public FileWorker()
    {
        super.type = 3;
    }

    public void startWorking()
        throws ServletProcessException, ServletNotFoundException
    {
        synchronized(super.STATE_LOCK)
        {
            switch(super.parser.getRequest())
            {
            case 2006: 
                sendFullDistribution();
                break;

            case 2004: 
                sendFile();
                break;

            case 2009: 
                sendJRE();
                break;

            case 3002: 
                sendLog();
                break;
            }
        }
    }

    protected void sendFile()
        throws ServletProcessException, ServletNotFoundException
    {
        File file = null;
        String ver = super.parser.getVersion();
        String app = super.parser.getApplication();
        String platform = super.parser.getPlatform();
        if(platform == null)
            platform = "all";
        java.io.InputStream is = null;
        String filename = ((AbstractWorker)this).getFilenames();
        if(app == null && ver == null && filename != null && filename.indexOf("LocaleInfo_") >= 0)
        {
            String text = "Requested locale specific file \"" + filename + "\" is not available.";
            throw new ServletProcessException(text, super.request, super.response, false);
        }
        if(filename == null)
            throw new ServletNotFoundException("No file name given for send file", super.request, super.response, true);
        if(filename.toLowerCase().endsWith("install.cab") || filename.toLowerCase().endsWith("install.jar"))
            file = super.vault.getInstaller(filename);
        else
        if(filename.toLowerCase().endsWith("launch.cab") || filename.toLowerCase().endsWith("launch.jar"))
            file = super.vault.getLauncher(filename);
        else
        if(filename.toLowerCase().endsWith("deploydirector.gif"))
            file = new File(super.vault.getServerEtcDir(), "deploydirector.gif");
        else
        if(filename.toLowerCase().endsWith("deploydirector_ss.gif"))
        {
            file = new File(super.vault.getServerEtcDir(), "deploydirector_ss.gif");
        } else
        {
            if(!super.parser.isAdministrator())
            {
                AbstractTrackerEntry a = CacheList.getRequest(super.parser);
                if(a != null && a.getFile() != null)
                {
                    ((AbstractWorker)this).sendSingleFile(a.getFile());
                    return;
                }
            }
            if(app == null && ver == null && !platform.equalsIgnoreCase("DeploySam"))
            {
                String msg = "No application or version is given and the platform is not DeploySam.";
                throw new ServletNotFoundException(msg, super.request, super.response, true);
            }
            if(((AbstractWorker)this).isRequestInProgress())
            {
                return;
            } else
            {
                java.util.Hashtable fileTable = ((AbstractWorker)this).buildFileTable(app, ver, filename);
                ((AbstractWorker)this).sendMultipleFiles(fileTable);
                return;
            }
        }
        if(file == null || !file.canRead())
            throw new ServletNotFoundException("File does not exist or can't be read.", super.request, super.response, true);
        try
        {
            is = ((java.io.InputStream) (new FileInputStream(file.getAbsolutePath())));
        }
        catch(FileNotFoundException fnfe) { }
        ((AbstractWorker)this).printHeaders(file);
        java.io.OutputStream out = super.server.getBinaryOutputStream(super.response);
        if(out == null)
        {
            file = null;
            throw new ServletProcessException("Unable to access binary output stream", super.request, super.response, true);
        } else
        {
            ((AbstractWorker)this).sendData(out, is);
            return;
        }
    }

    protected void sendLog()
        throws ServletProcessException, ServletNotFoundException
    {
        LogQueue logQueue = super.server.getLogQueue();
        String name = ((AbstractWorker)this).getFilenames();
        File logFile = logQueue.getLogCopy(name);
        if(logFile == null || !logFile.canRead())
        {
            String msg = null;
            if(logFile == null)
                msg = "Log file name not specified.";
            else
                msg = "Log file " + logFile.getAbsolutePath() + " can't be read.";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        } else
        {
            ((AbstractWorker)this).sendSingleFileAsZip(logFile, name);
            logFile.delete();
            return;
        }
    }

    protected void sendJRE()
        throws ServletProcessException, ServletNotFoundException
    {
        String app = super.parser.getApplication();
        String version = super.parser.getVersion();
        String platform = super.parser.getPlatform();
        if(app == null)
            throw new ServletNotFoundException("Unable to get JRE:  application is null.", super.request, super.response, true);
        if(version == null)
            throw new ServletNotFoundException("Unable to get JRE:  version is null.", super.request, super.response, true);
        if(platform == null)
            throw new ServletNotFoundException("Unable to get JRE:  platform is null.", super.request, super.response, true);
        File file = super.vault.getJREZip(platform, app, version);
        if(file == null || !file.canRead())
        {
            String msg = "Unable to access required JRE for: " + app + " version " + version + ", platform " + platform;
            throw new ServletProcessException(msg, "ClientRequestFailed", super.request, super.response, true);
        }
        String offset_string = super.parser.getParam("OFFSET");
        long offset = -1L;
        if(offset_string != null)
            offset = FileNameParser.extractOffset("|" + offset_string);
        ((AbstractWorker)this).sendSingleFile(file, offset);
    }

    protected void sendFullDistribution()
        throws ServletProcessException, ServletNotFoundException
    {
        String appName = super.parser.getApplication();
        String verName = super.parser.getVersion();
        if(appName == null || verName == null)
        {
            String msg = "Unable to get distribution:  ";
            if(appName == null)
            {
                if(verName == null)
                    msg = msg + "application and version names are null.";
                else
                    msg = msg + "application name is null.";
            } else
            {
                msg = msg + "version name is null.";
            }
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        }
        if(((AbstractWorker)this).getVersion(appName, verName) == null)
        {
            String msg = "Unable to get distribution:  version for " + appName + ", version " + verName + " is not found.";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        }
        if(!super.parser.isAdministrator())
        {
            AbstractTrackerEntry a = CacheList.getRequest(super.parser);
            if(a != null && a.getFile() != null)
            {
                long id = a.getID();
                if(!Long.toString(id).equals(((Object) (super.parser.getRequestID()))))
                    super.server.logMessage(super.request, "ClientBundleUpdateStarted", appName, verName);
                ((AbstractWorker)this).sendSingleFile(a.getFile());
                return;
            }
        }
        if(((AbstractWorker)this).isRequestInProgress())
            return;
        java.util.Hashtable fileTable = ((AbstractWorker)this).buildFileTable(appName, verName);
        if(fileTable == null)
        {
            String msg = "Unable to retrieve file table for: " + appName + " " + verName + ".";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        } else
        {
            super.server.logMessage(super.request, "ClientBundleUpdateStarted", appName, verName);
            ((AbstractWorker)this).sendMultipleFiles(fileTable);
            return;
        }
    }
}
