// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ConfigReplicationMessage.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.common.checkpoint.ConfigCheckpoint;
import com.sitraka.deploy.common.checkpoint.ConfigObject;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            ReplicationMessage

public class ConfigReplicationMessage extends ReplicationMessage
{

    public ConfigReplicationMessage(String remote_server, int type, ConfigObject configObject, ConfigCheckpoint checkpoint, Object _auth, String otherServers)
    {
        super(remote_server, type, ((Object) (configObject)), ((com.sitraka.deploy.common.checkpoint.Checkpoint) (checkpoint)), _auth, otherServers);
    }

    public ConfigObject getConfigObject()
    {
        return (ConfigObject)((ReplicationMessage)this).getUserData();
    }

    public ConfigCheckpoint getConfigCheckpoint()
    {
        return (ConfigCheckpoint)((ReplicationMessage)this).getCheckpoint();
    }
}
