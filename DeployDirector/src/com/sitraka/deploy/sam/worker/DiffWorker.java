// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DiffWorker.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.common.app.AppFile;
import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.sam.AbstractTrackerEntry;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.RequestParser;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.request.RequestList;
import com.sitraka.deploy.sam.servlet.ServletNotFoundException;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import java.util.Date;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            AbstractWorker

public class DiffWorker extends AbstractWorker
{

    public DiffWorker()
    {
        super.type = 2;
    }

    public void startWorking()
        throws ServletProcessException, ServletNotFoundException
    {
        synchronized(super.STATE_LOCK)
        {
            switch(super.parser.getRequest())
            {
            case 2003: 
                sendFileDifferences();
                break;

            case 2007: 
                sendInstallDifferences();
                break;
            }
        }
    }

    protected void sendFileDifferences()
        throws ServletProcessException, ServletNotFoundException
    {
        StringBuffer content = new StringBuffer();
        String platform = super.parser.getPlatform();
        String appName = super.parser.getApplication();
        String from = super.parser.getFrom();
        String to = super.parser.getTo();
        Version version_from = ((AbstractWorker)this).getVersion(appName, from);
        Version version_to = ((AbstractWorker)this).getVersion(appName, to);
        if(platform == null)
            platform = "all";
        if(to == null || from == null || version_to == null)
        {
            String msg = "Error computing install differences: ";
            if(to == null)
                msg = msg + "target version not specified";
            else
            if(version_to == null)
                msg = msg + "target version " + to + " not found";
            if(from == null)
            {
                if(to == null || version_to == null)
                    msg = msg + "; ";
                msg = msg + "source version not specified";
            }
            msg = msg + ".";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        }
        if(((AbstractWorker)this).isRequestInProgress())
            return;
        super.server.logMessage(super.request, "ClientBundleUpdateStarted", appName, from, "From version " + from + " to version " + to);
        AbstractTrackerEntry obj = RequestList.getRequest(super.parser);
        obj.setStatus(2);
        obj.setStatusText("Computing file differences for " + appName);
        if(version_from == null)
        {
            Vector files = version_to.getFiles(platform);
            int size = files != null ? files.size() : 0;
            for(int i = 0; i < size; i++)
            {
                AppFile file = (AppFile)files.elementAt(i);
                content.append("# ADDED\n");
                content.append(file.getPropertiesFile(i, ((String) (null))));
                content.append("\n");
            }

        } else
        {
            content.append(version_from.computeDifferenceFrom(version_to, platform));
        }
        if(content.length() == 0)
        {
            content.append("# No differences or unable to compute file differences\n");
            content.append("# between " + super.parser.getFrom() + " and " + super.parser.getTo() + "\n");
            content.append("# on platform " + (platform != null ? platform : "all") + "\n");
            content.append("# However, no error is being logged as of this point.\n");
        }
        obj.setText(content);
        obj.setStatusText("Completed at: " + new Date(System.currentTimeMillis()));
        obj.setStatus(3);
        obj.updateAccess();
    }

    protected void sendInstallDifferences()
        throws ServletProcessException, ServletNotFoundException
    {
        StringBuffer content = new StringBuffer();
        String platform = super.parser.getPlatform();
        String appName = super.parser.getApplication();
        String from = super.parser.getFrom();
        String to = super.parser.getTo();
        Version version_from = ((AbstractWorker)this).getVersion(appName, from);
        Version version_to = ((AbstractWorker)this).getVersion(appName, to);
        if(platform == null)
            platform = "all";
        if(to == null || version_to == null || from == null)
        {
            String msg = "Error computing install differences: ";
            if(to == null)
                msg = msg + "target version not specified";
            else
            if(version_to == null)
                msg = msg + "target version " + to + " not found";
            if(from == null)
            {
                if(to == null || version_to == null)
                    msg = msg + "; ";
                msg = msg + "source version not specified";
            }
            msg = msg + ".";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        }
        if(((AbstractWorker)this).isRequestInProgress())
            return;
        AbstractTrackerEntry obj = RequestList.getRequest(super.parser);
        if(version_from == null)
        {
            Application app = super.vault.getApplication(super.parser.getApplication());
            content.append("#\n# Install properties file for: " + app.getName() + ", version: " + version_to.getName() + "\n#\n\n");
            content.append("# Application name\n");
            content.append("application.name=" + app.getName() + "\n\n");
            content.append("# Application platform\n");
            content.append("application.platform=" + platform + "\n\n");
            content.append(version_to.getInstallProperties(platform));
        } else
        {
            content.append(version_from.computeInstallPropDifferenceFrom(version_to, platform));
        }
        if(content.length() == 0)
        {
            content.append("# No differences or unable to compute install differences\n");
            content.append("# between " + super.parser.getFrom() + " and " + super.parser.getTo() + "\n");
            content.append("# of " + super.parser.getApplication() + " on platform " + super.parser.getPlatform() + "\n");
            content.append("# However, no error is being logged as of this point.\n");
        }
        obj.setText(content);
        obj.setStatusText("Completed at: " + new Date(System.currentTimeMillis()));
        obj.setStatus(3);
        obj.updateAccess();
    }
}
