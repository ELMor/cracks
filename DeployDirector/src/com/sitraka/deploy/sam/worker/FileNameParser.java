// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FileNameParser.java

package com.sitraka.deploy.sam.worker;


public class FileNameParser
{

    public FileNameParser()
    {
    }

    public static String extractChildFile(String name)
    {
        int separator = name.indexOf("%%");
        if(separator == -1)
            return null;
        String extracted = name;
        separator = name.indexOf("|");
        if(separator != -1)
            extracted = extracted.substring(0, separator);
        separator = name.indexOf("%%");
        extracted = extracted.substring(separator + "%%".length());
        return extracted;
    }

    public static String extractParentFile(String name)
    {
        String extracted = name;
        int separator = extracted.indexOf("%%");
        if(separator == -1)
        {
            separator = name.indexOf("|");
            if(separator != -1)
                extracted = extracted.substring(0, separator);
            return extracted;
        }
        separator = extracted.indexOf("%%");
        if(separator != -1)
            extracted = extracted.substring(0, separator);
        return extracted;
    }

    public static long extractOffset(String name)
    {
        long offset = -1L;
        int separator = name.indexOf("|");
        if(separator != -1)
        {
            String offset_string = name.substring(separator + 1);
            try
            {
                offset = Long.parseLong(offset_string);
                if(offset < 0L)
                    offset = -1L;
            }
            catch(NumberFormatException e)
            {
                offset = -1L;
            }
        }
        return offset;
    }
}
