/* ReplicationWorker - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.sitraka.deploy.sam.worker;
import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;

import com.sitraka.deploy.common.checkpoint.ApplicationCheckpoint;
import com.sitraka.deploy.common.checkpoint.ApplicationObject;
import com.sitraka.deploy.common.checkpoint.Checkpoint;
import com.sitraka.deploy.common.checkpoint.ConfigCheckpoint;
import com.sitraka.deploy.common.checkpoint.ConfigObject;
import com.sitraka.deploy.common.checkpoint.FileCheckpoint;
import com.sitraka.deploy.common.checkpoint.FileUploadObject;
import com.sitraka.deploy.common.checkpoint.InstallerCheckpoint;
import com.sitraka.deploy.common.checkpoint.InstallerObject;
import com.sitraka.deploy.common.checkpoint.JRECheckpoint;
import com.sitraka.deploy.common.checkpoint.JREObject;
import com.sitraka.deploy.common.checkpoint.LogCheckpoint;
import com.sitraka.deploy.common.checkpoint.LogObject;
import com.sitraka.deploy.common.checkpoint.VersionCheckpoint;
import com.sitraka.deploy.common.checkpoint.VersionObject;
import com.sitraka.deploy.sam.LogQueue;
import com.sitraka.deploy.sam.log.ClientDataLogger;
import com.sitraka.deploy.sam.log.ClientLogger;
import com.sitraka.deploy.sam.log.LoadLogger;
import com.sitraka.deploy.sam.log.ServerLogger;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.sam.servlet.ServletNotFoundException;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import com.sitraka.deploy.util.Codecs;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.Hashcode;

public class ReplicationWorker extends AbstractWorker
{
    protected String auth = null;

    public ReplicationWorker() {
        type = 8;
    }

    public void startWorking()
        throws ServletProcessException, ServletNotFoundException {
        this.setOrigServer();
        Object userAuth = server.getUserAuthorization(request);
        if (userAuth == null) {
            if (parser.getRequest() != 3304 && parser.getRequest() != 3312) {
                String msg = ("No Authorization found in request "
                              + parser.getCommand() + " from: " + origServer);
                ServletProcessException spe
                    = new ServletProcessException(msg, "ServerRequestFailed",
                                                  request, response, true);
                spe.setApplication(none);
                spe.setVersion(none);
                throw spe;
            }
            userAuth = "dGVzdDp0ZXN0";
        }
        auth = Codecs.base64Decode(userAuth.toString());
        synchronized (STATE_LOCK) {
            switch (parser.getRequest()) {
            case 3202:
                replicateJRE();
                break;
            case 3108:
                replicateBundle();
                break;
            case 3100:
                replicateVersion();
                break;
            case 3302:
                replicateConfig();
                break;
            case 3304:
                replicateLogs();
                break;
            case 3311:
                replicateHTML();
                break;
            case 3310:
                replicateInstaller();
                break;
            case 3312:
                sendLogs();
                break;
            case 3309:
                sendJRE();
                break;
            case 3313:
                sendCompleteVersion();
                break;
            case 3307:
                sendCompleteBundle();
                break;
            case 3308:
                sendConfig();
                break;
            case 3106:
                sendDAR();
                break;
            }
        }
    }

    protected void sendLogs()
        throws ServletProcessException, ServletNotFoundException {
        String name = parser.getName();
        String from = parser.getFrom();
        String to = parser.getTo();
        if (from == null || to == null || name == null) {
            String msg = ("Log send request missing data. FROM=" + from
                          + " TO=" + to + " NAME=" + name);
            ServletProcessException spe
                = new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
            spe.setApplication(none);
            spe.setVersion(none);
            throw spe;
        }
        File base = Servlet.getLogFileDir(vault.getVaultBaseDir());
        if (base == null) {
            String msg
                = ("Cannot determine log file base directory; request from: "
                   + origServer);
            ServletProcessException spe
                = new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
            spe.setApplication(none);
            spe.setVersion(none);
            throw spe;
        }
        LogQueue logQueue = server.getLogQueue();
        String[] logFiles;
        String lastTimestamp;
        if (name.equals(ClientDataLogger.getFileName())) {
            logFiles = new String[] { ClientDataLogger.getFileName() };
            lastTimestamp
                = LogQueue.readTimestampFile
                      (LogQueue.makeClientDataTimestampFilename(base))
                      .toString();
        } else {
            logFiles = new String[] { ClientLogger.getFileName(),
                                      LoadLogger.getFileName(),
                                      ServerLogger.getFileName() };
            lastTimestamp
                = LogQueue.readTimestampFile
                      (LogQueue.makeTimestampFilename(base)).toString();
        }
        Hashtable table = new Hashtable();
        boolean sameTimestamp = lastTimestamp.equals(to);
        File log_file = null;
        for (int i = 0; i < logFiles.length; i++) {
            if (sameTimestamp)
                log_file = LogQueue.makeIncrementalFile(base, logFiles[i]);
            else {
                log_file = FileUtils.createTempFile();
                logQueue.createIncrementalLog(logQueue.getLogger(logFiles[i]),
                                              log_file, from, to);
            }
            if (log_file.canRead()) {
                String filename = logFiles[i];
                AbstractWorker.FileObject fo
                    = new AbstractWorker.FileObject(filename, log_file);
                fo.setHashcode(Hashcode.computeHash(log_file));
                fo.setSize(log_file.length());
                table.put(filename, fo);
            }
        }
        this.sendMultipleFiles(table);
        if (!sameTimestamp) {
            Enumeration e = table.elements();
            while (e.hasMoreElements()) {
                AbstractWorker.FileObject fo
                    = (AbstractWorker.FileObject) e.nextElement();
                fo.getFile().delete();
            }
        }
    }

    protected void sendJRE()
        throws ServletProcessException, ServletNotFoundException {
        String platform = parser.getPlatform();
        String vendor = parser.getVendor();
        String version = parser.getVersion();
        String offset = parser.getParam("OFFSET");
        if (vendor == null || version == null || platform == null) {
            String msg = ("Unable to perform send JRE requested by "
                          + origServer + "request was missing ");
            boolean prev = false;
            if (vendor == null) {
                msg += "the vendor name";
                prev = true;
            }
            if (version == null) {
                if (prev)
                    msg = msg + (platform == null ? ", " : " and ");
                msg += "the version name";
                prev = true;
            }
            if (platform == null) {
                if (prev)
                    msg += " and ";
                msg += "the platform name";
            }
            msg += ".";
            ServletProcessException spe
                = new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
            spe.setApplication(vendor + " JRE");
            throw spe;
        }
        File base
            = vault.getJREBaseDirectory(platform, vendor, version, false);
        if (base == null || !base.canRead()) {
            String msg
                = ("JRE base directory for platform \"" + platform
                   + "\" cannot be accessed, request from: " + origServer);
            ServletProcessException spe
                = new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
            spe.setApplication(vendor + " JRE");
            throw spe;
        }
        File jre_file = new File(base, "jre.zip");
        if (jre_file == null || !jre_file.canRead())
            jre_file = new File(base, "prebuilt.zip");
        if (jre_file == null || !jre_file.canRead()) {
            String msg = ("jre.zip file for platform \"" + platform
                          + "\" cannot be located or accessed; request from: "
                          + origServer);
            ServletProcessException spe
                = new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
            spe.setApplication(vendor + " JRE");
            throw spe;
        }
        String filename = jre_file.getName();
        if (offset != null)
            filename += "|" + (String) offset;
        this.sendSingleFileAsZip(jre_file, filename);
    }

    protected void sendConfig()
        throws ServletProcessException, ServletNotFoundException {
        File base = vault.getVaultBaseDir();
        String offset = parser.getParam("OFFSET");
        if (base == null || !base.canRead()) {
            String msg = ("sendConfig request from " + origServer
                          + ":  Cannot determine vault base directory.");
            ServletProcessException spe
                = new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
            spe.setApplication(none);
            spe.setVersion(none);
            throw spe;
        }
        File config_file = new File(base, "cluster.properties");
        if (!config_file.canRead()) {
            String msg
                = ("sendConfig request from " + origServer
                   + ":  Cluster properties file cannot be located or accessed");
            ServletProcessException spe
                = new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
            spe.setApplication(none);
            spe.setVersion(none);
            throw spe;
        }
        String filename = "cluster.properties";
        if (offset != null)
            filename += "|" + (String) offset;
        this.sendSingleFileAsZip(config_file, "cluster.properties");
    }

    protected void sendCompleteVersion()
        throws ServletProcessException, ServletNotFoundException {
        String bundle = parser.getApplication();
        String version = parser.getVersion();
        if (bundle == null || version == null) {
            String msg = ("sendCompleteVersion request from " + origServer
                          + ":  Version send request missing data.  No ");
            if (bundle == null) {
                if (version == null)
                    msg += "bundle or version ";
                else
                    msg += "bundle ";
            } else
                msg += "version ";
            msg += "name was specified.";
            throw new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
        }
        File base = vault.getVersionBaseDir(bundle, version);
        if (base == null || !base.canRead()) {
            String msg = ("sendCompleteVersion request from " + origServer
                          + ":  Cannot access version base directory");
            throw new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
        }
        if (!this.isRequestInProgress())
            this.sendDirectory(base);
    }

    protected void sendCompleteBundle()
        throws ServletProcessException, ServletNotFoundException {
        String bundle = parser.getApplication();
        if (bundle == null) {
            String msg
                = ("sendCompleteBundle request from " + origServer
                   + ":  Bundle send request missing data. BUNDLE was null.");
            ServletProcessException spe
                = new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
            spe.setVersion(none);
            throw spe;
        }
        File base = vault.getApplicationBaseDir(bundle);
        if (base == null || !base.canRead()) {
            String msg = ("sendCompleteBundle request from " + origServer
                          + ":  Cannot access bundle base directory.");
            ServletProcessException spe
                = new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
            spe.setVersion(none);
            throw spe;
        }
        if (!this.isRequestInProgress())
            this.sendDirectory(base);
    }

    protected void sendDAR()
        throws ServletProcessException, ServletNotFoundException {
        String bundle = parser.getApplication();
        String version = parser.getVersion();
        if (bundle == null || version == null) {
            String msg = ("sendDAR request from " + origServer
                          + ":  DAR send request missing data.  No ");
            if (bundle == null) {
                if (version == null)
                    msg += "bundle or version ";
                else
                    msg += "bundle ";
            } else
                msg += "version ";
            msg += "name was specified.";
            throw new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
        }
        File base = vault.getVersionBaseDir(bundle, version);
        if (base == null || !base.canRead()) {
            String msg = ("sendDAR request from " + origServer
                          + ":  Cannot access version base directory.");
            throw new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
        }
        if (!this.isRequestInProgress())
            this.sendDirectoryAsDar(base);
    }

    protected void sendACK() {
        this.sendText("Notification Received");
    }

    public void replicate(Checkpoint checkpoint) {
        if (checkpoint != null) {
            if (checkpoint instanceof JRECheckpoint)
                replicateJRE((JRECheckpoint) checkpoint);
            else if (checkpoint instanceof ApplicationCheckpoint)
                replicateBundle((ApplicationCheckpoint) checkpoint);
            else if (checkpoint instanceof VersionCheckpoint)
                replicateVersion((VersionCheckpoint) checkpoint);
            else if (checkpoint instanceof ConfigCheckpoint)
                replicateConfig((ConfigCheckpoint) checkpoint);
            else if (checkpoint instanceof LogCheckpoint)
                replicateLogs((LogCheckpoint) checkpoint);
            else if (checkpoint instanceof FileCheckpoint)
                replicateFile((FileCheckpoint) checkpoint);
            else if (checkpoint instanceof InstallerCheckpoint)
                replicateInstaller((InstallerCheckpoint) checkpoint);
        }
    }

    protected boolean allowedToReplicate(String otherServer)
        throws ServletProcessException {
        if (!Servlet.isLicensed())
            return false;
        String clusterServers
            = Servlet.properties.getProperty("deploy.otherhosts");
        if (clusterServers != null
            && clusterServers.indexOf(otherServer) != -1)
            return true;
        String msg = ("Received replication notification from " + otherServer
                      + " but it is not listed in our list of other servers.  "
                      + "Request rejected.");
        ServletProcessException spe
            = new ServletProcessException(msg, "ServerRequestFailed", request,
                                          response, true);
        spe.setApplication(none);
        spe.setVersion(none);
        throw spe;
    }

    public void replicateJRE() throws ServletProcessException {
        String remote_server = parser.getServer();
        String vendor = parser.getVendor();
        String version = parser.getVersion();
        String platform = parser.getPlatform();
        String name = parser.getName();
        String format = parser.getFormat();
        String classpaths = parser.getClasspaths();
        if (remote_server == null || vendor == null || version == null
            || platform == null || name == null || format == null
            || classpaths == null) {
            String msg = ("replicateJRE request from " + origServer
                          + ":  Unable to replicate, request was missing"
                          + (remote_server == null ? " SERVER" : "")
                          + (vendor == null ? " VENDOR" : "")
                          + (version == null ? " VERSION" : "")
                          + (platform == null ? " PLATFORM" : "")
                          + (name == null ? " NAME" : "")
                          + (format == null ? " FORMAT" : "")
                          + (classpaths == null ? " CLASSPATHS" : "") + ".");
            ServletProcessException spe
                = new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
            spe.setApplication(vendor + " JRE");
            throw spe;
        }
        if (allowedToReplicate(remote_server)) {
            server.logMessage(request, "ServerJREUpdateStarted", none, none,
                              "From: " + origServer);
            JRECheckpoint checkpoint
                = new JRECheckpoint(new JREObject(remote_server, vendor,
                                                  version, platform, name,
                                                  format, classpaths));
            checkpoint.setDefaultFileName(vault.getServerCheckpointDir());
            checkpoint.setTempDir(vault.getServerTempDir());
            checkpoint.setState(100);
            replicateJRE(checkpoint);
            sendACK();
        }
    }

    public void replicateJRE(JRECheckpoint checkpoint) {
        AbstractWorker.getReplicationQueue(server)
            .receiveJREReplication(checkpoint, auth);
    }

    public void replicateBundle() throws ServletProcessException {
        String remote_server = parser.getServer();
        String application = parser.getApplication();
        if (remote_server == null || application == null) {
            String msg
                = ("replicateBundle request from " + origServer
                   + ":  Unable to replicate bundle, request was missing ");
            if (remote_server == null) {
                if (application == null)
                    msg += "the server and bundle names.";
                else
                    msg += "the server name.";
            } else
                msg += "the bundle name.";
            ServletProcessException spe
                = new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
            spe.setVersion(none);
            throw spe;
        }
        if (allowedToReplicate(remote_server)) {
            server.logMessage(request, "ServerBundleUpdateStarted",
                              application, "(all)", "From: " + origServer);
            ApplicationCheckpoint checkpoint
                = (new ApplicationCheckpoint
                   (new ApplicationObject(remote_server, application)));
            checkpoint.setDefaultFileName(vault.getServerCheckpointDir());
            checkpoint.setTempDir(vault.getServerTempDir());
            checkpoint.setState(100);
            replicateBundle(checkpoint);
            sendACK();
        }
    }

    public void replicateBundle(ApplicationCheckpoint checkpoint) {
        AbstractWorker.getReplicationQueue(server)
            .receiveApplicationReplication(checkpoint, auth);
    }

    public void replicateVersion() throws ServletProcessException {
        String remote_server = parser.getServer();
        String application = parser.getApplication();
        String version = parser.getVersion();
        if (remote_server == null || application == null || version == null) {
            String msg = ("replicateVersion request from " + origServer
                          + ":  Unable to replicate, request was missing"
                          + (remote_server == null ? " SERVER" : "")
                          + (application == null ? " BUNDLE" : "")
                          + (version == null ? " VERSION." : "."));
            throw new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
        }
        if (allowedToReplicate(remote_server)) {
            server.logMessage(request, "ServerVersionUpdateStarted",
                              application, version, "From: " + origServer);
            VersionCheckpoint checkpoint
                = new VersionCheckpoint(new VersionObject(remote_server,
                                                          application,
                                                          version));
            checkpoint.setDefaultFileName(vault.getServerCheckpointDir());
            checkpoint.setTempDir(vault.getServerTempDir());
            checkpoint.setState(100);
            replicateVersion(checkpoint);
            sendACK();
        }
    }

    public void replicateVersion(VersionCheckpoint checkpoint) {
        AbstractWorker.getReplicationQueue(server)
            .receiveVersionReplication(checkpoint, auth);
    }

    public void replicateConfig() throws ServletProcessException {
        String remote_server = parser.getServer();
        if (remote_server == null) {
            String msg
                = ("replicateConfig request from " + origServer
                   + ":  Unable to replicate config, missing SERVER name.");
            ServletProcessException spe
                = new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
            spe.setApplication(none);
            spe.setVersion(none);
            throw spe;
        }
        if (allowedToReplicate(remote_server)) {
            server.logMessage(request, "ServerConfigUpdateStarted", none, none,
                              "From: " + origServer);
            ConfigCheckpoint checkpoint
                = new ConfigCheckpoint(new ConfigObject(remote_server));
            checkpoint.setDefaultFileName(vault.getServerCheckpointDir());
            checkpoint.setTempDir(vault.getServerTempDir());
            checkpoint.setState(100);
            replicateConfig(checkpoint);
            sendACK();
        }
    }

    public void replicateConfig(ConfigCheckpoint checkpoint) {
        AbstractWorker.getReplicationQueue(server)
            .receiveConfigReplication(checkpoint, auth);
    }

    public void replicateHTML() throws ServletProcessException {
        String remote_server = parser.getServer();
        String filename = this.getFilenames();
        if (remote_server == null) {
            String msg
                = ("replicateHTML request from " + origServer
                   + "Unable to replicate, request was missing SERVER.");
            ServletProcessException spe
                = new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
            spe.setApplication(none);
            spe.setVersion(none);
            throw spe;
        }
        if (allowedToReplicate(remote_server)) {
            if (filename == null) {
                String msg
                    = ("replicateHTML request from " + origServer
                       + "Unable to replicate, request was missing FILENAME.");
                ServletProcessException spe
                    = new ServletProcessException(msg, "ServerRequestFailed",
                                                  request, response, true);
                spe.setApplication(none);
                spe.setVersion(none);
                throw spe;
            }
            server.logMessage(request, "ServerHTMLUpdateStarted", none, none,
                              "From: " + origServer);
            FileCheckpoint checkpoint
                = new FileCheckpoint(new FileUploadObject(remote_server,
                                                          filename));
            checkpoint.setDefaultFileName(vault.getServerCheckpointDir());
            checkpoint.setTempDir(vault.getServerTempDir());
            checkpoint.setState(100);
            replicateFile(checkpoint);
            sendACK();
        }
    }

    public void replicateFile(FileCheckpoint checkpoint) {
        AbstractWorker.getReplicationQueue(server)
            .receiveFileReplication(checkpoint, auth);
    }

    public void replicateInstaller() throws ServletProcessException {
        String remote_server = parser.getServer();
        String filename = this.getFilenames();
        if (remote_server == null) {
            String msg
                = ("replicateInstaller request from " + origServer
                   + "Unable to replicate, request was missing SERVER.");
            ServletProcessException spe
                = new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
            spe.setApplication(none);
            spe.setVersion(none);
            throw spe;
        }
        if (allowedToReplicate(remote_server)) {
            if (filename == null) {
                String msg
                    = ("replicateInstaller request from " + origServer
                       + "Unable to replicate, request was missing FILENAME.");
                ServletProcessException spe
                    = new ServletProcessException(msg, "ServerRequestFailed",
                                                  request, response, true);
                spe.setApplication(none);
                spe.setVersion(none);
                throw spe;
            }
            server.logMessage(request, "ServerInstallerUpdateStarted", none,
                              none, "From: " + origServer);
            InstallerCheckpoint checkpoint
                = new InstallerCheckpoint(new InstallerObject(remote_server,
                                                              filename));
            checkpoint.setDefaultFileName(vault.getServerCheckpointDir());
            checkpoint.setTempDir(vault.getServerTempDir());
            checkpoint.setState(100);
            replicateInstaller(checkpoint);
            sendACK();
        }
    }

    public void replicateInstaller(InstallerCheckpoint checkpoint) {
        AbstractWorker.getReplicationQueue(server)
            .receiveInstallerReplication(checkpoint, auth);
    }

    public void replicateLogs() throws ServletProcessException {
        String remote_server = parser.getServer();
        String name = parser.getName();
        String from = parser.getFrom();
        String to = parser.getTo();
        if (remote_server == null) {
            String msg
                = ("replicateLogs request from " + origServer
                   + ":  Unable to replicate, request was missing SERVER.");
            ServletProcessException spe
                = new ServletProcessException(msg, "ServerRequestFailed",
                                              request, response, true);
            spe.setApplication(none);
            spe.setVersion(none);
            throw spe;
        }
        if (allowedToReplicate(remote_server)) {
            LogCheckpoint checkpoint
                = new LogCheckpoint(new LogObject(remote_server, name, from,
                                                  to));
            checkpoint.setDefaultFileName(vault.getServerCheckpointDir());
            checkpoint.setTempDir(vault.getServerTempDir());
            checkpoint.setState(100);
            replicateLogs(checkpoint);
            sendACK();
        }
    }

    public void replicateLogs(LogCheckpoint checkpoint) {
        AbstractWorker.getReplicationQueue(server)
            .receiveLogReplication(checkpoint, auth);
    }
}
