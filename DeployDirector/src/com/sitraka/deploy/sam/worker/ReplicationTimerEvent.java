// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ReplicationTimerEvent.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.common.timer.TimerEvent;
import com.sitraka.deploy.common.timer.TimerEventListener;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            ReplicationMessage

public class ReplicationTimerEvent extends TimerEvent
{

    protected ReplicationMessage message;

    public ReplicationTimerEvent(ReplicationMessage message, long delay, TimerEventListener tel)
    {
        super(System.currentTimeMillis() + delay, 0L, tel);
        this.message = null;
        this.message = message;
    }

    public ReplicationTimerEvent(ReplicationMessage message, TimerEventListener tel)
    {
        this(message, 0L, tel);
    }

    public ReplicationMessage getMessage()
    {
        return message;
    }
}
