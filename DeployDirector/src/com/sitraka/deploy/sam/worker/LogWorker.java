// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LogWorker.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.sam.RequestParser;
import com.sitraka.deploy.sam.ServerActions;
import java.io.IOException;
import java.io.InputStream;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            AbstractWorker

public class LogWorker extends AbstractWorker
{

    public LogWorker()
    {
        super.type = 6;
    }

    public void startWorking()
    {
        synchronized(super.STATE_LOCK)
        {
            switch(super.parser.getRequest())
            {
            case 1008: 
            case 1009: 
                logMessage();
                break;
            }
        }
    }

    protected void logMessage()
    {
        String appName = super.parser.getApplication();
        String version = super.parser.getVersion();
        String message = super.parser.getMessage();
        String notes = super.parser.getParam("NOTES");
        int length = super.server.getContentLength(super.request);
        if(notes != null)
        {
            length -= notes.length();
            if(notes.toUpperCase().startsWith("NOTES="))
            {
                length -= "NOTES=".length();
                notes = notes.substring("NOTES=".length());
            } else
            if(length == 6)
                length = 0;
        }
        if(length > 0)
        {
            InputStream is = super.server.getBinaryInputStream(super.request);
            StringBuffer b = new StringBuffer();
            try
            {
                int n = -1;
                int total = 0;
                byte buff[] = new byte[AbstractWorker.BUF_SIZE];
                while((n = is.read(buff, 0, AbstractWorker.BUF_SIZE)) != -1) 
                {
                    b.append(new String(buff, 0, n));
                    total += n;
                    if(total >= length)
                        break;
                }
            }
            catch(IOException ioe) { }
            notes = b.toString();
        } else
        {
            notes = super.parser.getParam("NOTES");
        }
        if(notes != null && notes.toUpperCase().startsWith("NOTES="))
            notes = notes.substring("NOTES=".length());
        if(message != null)
            message = message.trim();
        if(notes != null)
            notes = notes.trim();
        super.server.logMessage(super.request, message, appName, version, notes);
        ((AbstractWorker)this).sendText("LOGGED");
    }
}
