// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AdminPageWorker.java

package com.sitraka.deploy.sam.worker;

import HTTPClient.ParseException;
import HTTPClient.URI;
import com.sitraka.deploy.common.PlatformIdentifier;
import com.sitraka.deploy.common.TemplateText;
import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.LogQueue;
import com.sitraka.deploy.sam.RequestParser;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.cache.CacheList;
import com.sitraka.deploy.sam.log.ClientDataLogger;
import com.sitraka.deploy.sam.log.LogException;
import com.sitraka.deploy.sam.request.RequestList;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.util.Codecs;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            GeneratedPageWorker, AbstractWorker

public class AdminPageWorker extends GeneratedPageWorker
{

    public AdminPageWorker()
    {
    }

    protected void setDefaultPageValues(TemplateText text, String baseURL)
    {
        Properties properties = Servlet.properties;
        double load[] = ((AbstractWorker)this).server.getServerLoad();
        long uptime = ((AbstractWorker)this).server.getUptime();
        long rawtime = uptime;
        long startup_time = System.currentTimeMillis() - uptime;
        long days = uptime / 0x5265c00L;
        uptime %= 0x5265c00L;
        long hours = uptime / 0x36ee80L;
        uptime %= 0x36ee80L;
        long minutes = uptime / 60000L;
        uptime %= 60000L;
        long seconds = uptime / 1000L;
        uptime %= 1000L;
        NumberFormat commaNumber = NumberFormat.getNumberInstance();
        commaNumber.setGroupingUsed(true);
        DecimalFormat df = (DecimalFormat)NumberFormat.getInstance();
        df.setMaximumFractionDigits(3);
        df.setMinimumFractionDigits(3);
        DateFormat sdf = DateFormat.getDateTimeInstance(1, 1);
        String requestCount = commaNumber.format(RequestList.getNumFiles());
        text.setPlaceHolder("REQUESTCOUNT", ((Object) (requestCount)));
        text.setPlaceHolder("CACHECOUNT", ((Object) (commaNumber.format(CacheList.getNumFiles()))));
        text.setPlaceHolder("CACHESIZE", ((Object) ("" + commaNumber.format(CacheList.getCacheSize()))));
        long cache_max = PropertyUtils.readLong(properties, "deploy.server.cache.size", -1L);
        if(cache_max < 0L)
            cache_max = 0L;
        text.setPlaceHolder("CACHEMAX", ((Object) ("" + commaNumber.format(cache_max))));
        text.setPlaceHolder("CACHEEXPIRE", ((Object) (PropertyUtils.readDefaultString(properties, "deploy.server.cache.maxage", "(unknown)"))));
        text.setPlaceHolder("VMVENDOR", ((Object) (System.getProperty("java.vendor"))));
        text.setPlaceHolder("VMVERSION", ((Object) (System.getProperty("java.version"))));
        text.setPlaceHolder("APPSERVER", ((Object) (PropertyUtils.readDefaultString(properties, "deploy.appserver", "(unknown)"))));
        text.setPlaceHolder("OSNAME", ((Object) (System.getProperty("os.name"))));
        text.setPlaceHolder("OSVERSION", ((Object) (System.getProperty("os.version"))));
        text.setPlaceHolder("OSDATA", ((Object) (PlatformIdentifier.getPlatformString())));
        long freeMem = Runtime.getRuntime().freeMemory();
        long totalMem = Runtime.getRuntime().totalMemory();
        long allocedMem = totalMem - freeMem;
        text.setPlaceHolder("FREEMEMORY", ((Object) ("" + commaNumber.format(freeMem))));
        text.setPlaceHolder("TOTALMEMORY", ((Object) ("" + commaNumber.format(totalMem))));
        text.setPlaceHolder("ALLOCATEDMEMORY", ((Object) ("" + commaNumber.format(allocedMem))));
        double alloced_meg = (double)allocedMem / 1048576D;
        double total_meg = (double)totalMem / 1048576D;
        double free_meg = (double)freeMem / 1048576D;
        text.setPlaceHolder("FREEMEGS", ((Object) ("" + ((NumberFormat) (df)).format(free_meg))));
        text.setPlaceHolder("TOTALMEGS", ((Object) ("" + ((NumberFormat) (df)).format(total_meg))));
        text.setPlaceHolder("ALLOCATEDMEGS", ((Object) ("" + ((NumberFormat) (df)).format(alloced_meg))));
        text.setPlaceHolder("ADMINNAME", ((Object) (properties.getProperty("deploy.admin.username"))));
        text.setPlaceHolder("BUILDNUMBER", "DeploySam 2.5.0, Build: DD250-20021210-1039");
        text.setPlaceHolder("SAMVERSION", "2.5.0");
        text.setPlaceHolder("SERVERNAME", ((Object) (((AbstractWorker)this).server.getServletInfo())));
        text.setPlaceHolder("LOGO", ((Object) (baseURL + "/" + "deploydirector.gif")));
        text.setPlaceHolder("LOGO_SS", ((Object) (baseURL + "/" + "deploydirector_ss.gif")));
        text.setPlaceHolder("URL", "http://www.sitraka.com/software/deploydirector");
        text.setPlaceHolder("INSTALLURL", ((Object) (baseURL + "/DDAdmin/install")));
        text.setPlaceHolder("BASEURL", ((Object) (baseURL)));
        text.setPlaceHolder("1MINAVG", ((Object) (((NumberFormat) (df)).format(load[0]))));
        text.setPlaceHolder("5MINAVG", ((Object) (((NumberFormat) (df)).format(load[1]))));
        text.setPlaceHolder("15MINAVG", ((Object) (((NumberFormat) (df)).format(load[2]))));
        text.setPlaceHolder("CONCURRENTREQUESTS", ((Object) ("" + ((AbstractWorker)this).server.getConcurrentRequestCount())));
        text.setPlaceHolder("MAXCONCURRENTREQUESTS", ((Object) ("" + ((AbstractWorker)this).server.getMaxConcurrentRequestsRecorded())));
        text.setPlaceHolder("STARTTIME", ((Object) (sdf.format(new Date(startup_time)))));
        text.setPlaceHolder("UPTIMETEXT", ((Object) (days + " days, " + hours + " hours, " + minutes + " minutes, " + seconds + " seconds")));
        int value = PropertyUtils.readInt(properties, "clients", -1);
        text.setPlaceHolder("MAXCLIENTS", ((Object) ("" + value)));
        text.setPlaceHolder("MAXCLIENTSTEXT", ((Object) (value != -1 ? ((Object) ("" + value)) : "(unlimited)")));
        value = PropertyUtils.readInt(properties, "bundles", -1);
        text.setPlaceHolder("MAXBUNDLES", ((Object) ("" + value)));
        text.setPlaceHolder("MAXBUNDLESTEXT", ((Object) (value != -1 ? ((Object) ("" + value)) : "(unlimited)")));
        String bundles = PropertyUtils.readDefaultString(properties, "applications", ((String) (null)));
        text.setPlaceHolder("LICENSEDAPPLICATIONS", ((Object) (bundles != null ? ((Object) (bundles)) : "(unlimited)")));
        int clientCount = -1;
        try
        {
            clientCount = ((AbstractWorker)this).server.getLogQueue().getClientDataLogger().getClients(0x1cf7c5800L);
        }
        catch(LogException le) { }
        text.setPlaceHolder("NUMCLIENTS", ((Object) (clientCount != -1 ? ((Object) ("" + clientCount)) : "(unknown)")));
        text.setPlaceHolder("ISLICENSED", ((Object) (Servlet.licensed ? "has" : "<B>does not have</B>")));
        text.setPlaceHolder("LICENSEVALID", ((Object) (Servlet.licensed ? "<font color=\"#009900\">Valid</font>" : "<font color=\"#ff0000\">Not Valid</font>")));
        text.setPlaceHolder("SERIALNUMBER", ((Object) (PropertyUtils.readDefaultString(properties, "serial_number", "(unknown)"))));
        text.setPlaceHolder("LICENSEDPASSWORD", ((Object) (PropertyUtils.readDefaultString(properties, "sitraka.license.signature", "(unknown)"))));
        long date = PropertyUtils.readShortDate(properties, "expiry");
        if(date > 0L)
        {
            text.setPlaceHolder("EXPIRY", ((Object) (sdf.format(new Date(date)))));
            text.setPlaceHolder("EXPIRYTEXT", ((Object) (sdf.format(new Date(date)))));
        } else
        {
            text.setPlaceHolder("EXPIRY", "");
            text.setPlaceHolder("EXPIRYTEXT", "(never)");
        }
        Vector allHosts = PropertyUtils.readVector(properties, "deploy.cluster.processedhosts", ',');
        text.setPlaceHolder("NUMSERVERS", ((Object) ("" + allHosts.size())));
        StringBuffer serverBR = new StringBuffer();
        StringBuffer serverLI = new StringBuffer();
        for(int i = 0; i < allHosts.size(); i++)
        {
            String host = (String)allHosts.elementAt(i);
            URI uri = null;
            try
            {
                uri = new URI(host);
            }
            catch(ParseException e)
            {
                uri = null;
            }
            serverLI.append("<tr>\n");
            serverLI.append("<td><FONT FACE=\"Verdana, Arial, Helvetica\" SIZE=\"-1\">");
            serverLI.append(uri != null ? uri.getHost() : "(unknown)");
            serverLI.append("</FONT></td>\n");
            serverLI.append("<td><FONT FACE=\"Verdana, Arial, Helvetica\" SIZE=\"-1\">");
            serverLI.append("<A HREF=\"");
            serverLI.append(host);
            serverLI.append("/admin-old\">");
            serverLI.append(host);
            serverLI.append("/admin-old</A>");
            serverLI.append("</FONT></td>\n");
            serverLI.append("</tr>\n");
            serverBR.append(uri != null ? uri.getHost() : "(unknown)");
            if(i < allHosts.size() - 1)
                serverBR.append(",");
        }

        text.setPlaceHolder("LICENSEDHOSTS", ((Object) (PropertyUtils.readDefaultString(properties, "hosts", "(unknown)"))));
        text.setPlaceHolder("SERVERLIST", ((Object) (serverLI.toString())));
        String apps[] = ((AbstractWorker)this).vault.listApplications();
        text.setPlaceHolder("NUMBUNDLES", ((Object) ("" + ((AbstractWorker)this).vault.countApplications())));
        StringBuffer rows = new StringBuffer();
        for(int i = 0; i < apps.length; i++)
        {
            Vector appDetails = PropertyUtils.readVector(apps[i], ',');
            String name = (String)appDetails.elementAt(0);
            Vector versions = ((AbstractWorker)this).vault.getApplication(name).listVersions();
            Version v = (Version)versions.elementAt(0);
            rows.append("<tr>\n");
            rows.append("<td><FONT FACE=\"Verdana, Arial, Helvetica\" SIZE=\"-1\">");
            rows.append(name);
            rows.append("</FONT></td>\n");
            rows.append("<td><FONT FACE=\"Verdana, Arial, Helvetica\" SIZE=\"-1\"><B>");
            rows.append(v.getName());
            rows.append("</B>");
            if(versions.size() > 1)
            {
                rows.append(" (");
                for(int j = 1; j < versions.size(); j++)
                {
                    rows.append(((Version)versions.elementAt(j)).getName());
                    if(j < versions.size() - 1)
                        rows.append(", ");
                }

                rows.append(")");
            }
            rows.append("</FONT></td>\n");
            rows.append("<td><FONT FACE=\"Verdana, Arial, Helvetica\" SIZE=\"-1\">");
            rows.append(sdf.format(v.getCreationDate()));
            rows.append("</FONT></td>\n");
            rows.append("<td><FONT FACE=\"Verdana, Arial, Helvetica\" SIZE=\"-1\">");
            if(name.equalsIgnoreCase("DDCAM"))
            {
                rows.append("&nbsp;");
            } else
            {
                rows.append("(<a href=\"");
                rows.append(baseURL);
                rows.append("/");
                rows.append(Codecs.URLEncode(name));
                rows.append("/install\">install</a>)");
            }
            rows.append("</FONT></td>\n");
            rows.append("</tr>\n");
        }

        text.setPlaceHolder("BUNDLEROWS", ((Object) (rows.toString())));
    }

    protected void sendAdminPage()
    {
        String baseURL = ((AbstractWorker)this).stripURL(((AbstractWorker)this).server.getRequestUrl(((AbstractWorker)this).request), "/admin");
        java.io.InputStream s = null;
        try
        {
            s = ((java.io.InputStream) (new FileInputStream(Servlet.properties.getProperty("deploy.server.basedir") + File.separator + "etc" + File.separator + "AdminPage.html")));
        }
        catch(FileNotFoundException e)
        {
            s = ((Object)this).getClass().getResourceAsStream("/com/sitraka/deploy/sam/servlet/AdminPage.html");
        }
        TemplateText text = null;
        if(s != null)
            try
            {
                text = new TemplateText(s);
            }
            catch(IOException e)
            {
                ((AbstractWorker)this).server.logException(((AbstractWorker)this).request, ((Throwable) (e)), ((AbstractWorker)this).parser.getApplication(), ((AbstractWorker)this).parser.getVersion(), "Could not read from AdminPage input stream");
                s = null;
            }
        if(s == null)
        {
            ((AbstractWorker)this).sendText(createDefaultAdminPage(baseURL), "text/html");
            return;
        } else
        {
            setDefaultPageValues(text, baseURL);
            ((AbstractWorker)this).sendText(text.toString(), "text/html");
            return;
        }
    }

    protected String createDefaultAdminPage(String baseURL)
    {
        StringBuffer content = new StringBuffer();
        content.append("<html><head><meta name=\"Author\" content=\"");
        content.append("DeploySam");
        content.append(" ");
        content.append("2.5.0");
        content.append("\">\n");
        content.append("<title>DeployDirector Administrator's Info Page</title></head>\n");
        content.append("<body>\n");
        content.append("<FONT FACE=\"Verdana, Arial, Helvetica\" SIZE=\"-1\">");
        content.append("<UL>");
        content.append("<BR>");
        content.append("<FONT SIZE=\"+1\">Error: Default admin page could not be\n");
        content.append("located in SAM jar file or on the file system");
        content.append("</FONT><BR><BR>");
        content.append("<BR>To install the DeployDirector administrator's tool, click\n");
        content.append("<A HREF=\"");
        content.append(baseURL);
        content.append("/DDAdmin/install\">here</A>\n");
        content.append("<BR><BR>");
        content.append("</UL>");
        content.append("\n\n<hr WIDTH=\"100%\">\n");
        content.append("<FONT SIZE=\"-1\"><B><I>Powered By Sitraka's</I>\n");
        content.append("<A HREF=\"");
        content.append("http://www.sitraka.com/software/deploydirector");
        content.append("\">\n");
        content.append("<BR><img SRC=\"");
        content.append(baseURL);
        content.append("/");
        content.append("deploydirector_ss.gif");
        content.append("\" BORDER=0>\n<BR>");
        content.append("http://www.sitraka.com/software/deploydirector");
        content.append("</A></B></FONT>\n");
        content.append("</FONT></body>\n");
        content.append("</html>\n");
        return content.toString();
    }

    protected void sendAdminInfoPage()
    {
        String baseURL = ((AbstractWorker)this).stripURL(((AbstractWorker)this).server.getRequestUrl(((AbstractWorker)this).request), "/ddinfo");
        java.io.InputStream s = null;
        try
        {
            s = ((java.io.InputStream) (new FileInputStream(Servlet.properties.getProperty("deploy.server.basedir") + File.separator + "etc" + File.separator + "AdminInfoPage.html")));
        }
        catch(FileNotFoundException e)
        {
            s = ((Object)this).getClass().getResourceAsStream("/com/sitraka/deploy/sam/servlet/AdminInfoPage.html");
        }
        TemplateText text = null;
        if(s != null)
            try
            {
                text = new TemplateText(s);
            }
            catch(IOException e)
            {
                ((AbstractWorker)this).server.logException(((AbstractWorker)this).request, ((Throwable) (e)), ((AbstractWorker)this).parser.getApplication(), ((AbstractWorker)this).parser.getVersion(), "Could not read from AdminInfoPage input stream");
                s = null;
            }
        if(s == null)
        {
            ((AbstractWorker)this).sendText(createDefaultAdminInfoPage(baseURL), "text/html");
            return;
        }
        setDefaultPageValues(text, baseURL);
        StringBuffer content = new StringBuffer();
        Vector hosts = getValidHostNames();
        int size = hosts.size();
        if(size == 0)
        {
            content.append("<LI><FONT COLOR=\"RED\">No valid host names found. This is a critical error");
            content.append(" and will prevent DeployDirector from running on this system.</FONT></LI>\n");
        } else
        {
            for(int i = 0; i < size; i++)
            {
                content.append("<LI>");
                content.append((String)hosts.elementAt(i));
                content.append("</LI>\n");
            }

        }
        text.setPlaceHolder("VALIDHOSTNAMES", ((Object) (content.toString())));
        content = new StringBuffer();
        for(Enumeration e = System.getProperties().propertyNames(); e.hasMoreElements(); content.append("</TR>\n"))
        {
            String key = (String)e.nextElement();
            String value = System.getProperty(key);
            if("line.separator".equals(((Object) (key))))
            {
                StringBuffer result = new StringBuffer();
                for(int i = 0; i < value.length(); i++)
                {
                    char character = value.charAt(i);
                    if(character == '\n')
                        result.append("\\n");
                    else
                    if(character == '\r')
                        result.append("\\r");
                    else
                        result.append(character);
                }

                value = result.toString();
            }
            content.append("<TR>\n");
            content.append("<TD BGCOLOR=\"#D0D0D0\"><FONT FACE=\"Verdana, Arial, Helvetica\" SIZE=\"-1\">");
            content.append(key);
            content.append("</FONT></TT></TD>\n");
            content.append("<TD><FONT FACE=\"Verdana, Arial, Helvetica\" SIZE=\"-1\">");
            content.append(value);
            content.append("</FONT></TD>\n");
        }

        content.append("<TR>\n");
        content.append("<TD BGCOLOR=\"#D0D0D0\"><FONT FACE=\"Verdana, Arial, Helvetica\" SIZE=\"-1\">Classloader</FONT></TD>\n");
        ClassLoader l = ((Object)this).getClass().getClassLoader();
        content.append("<TD><FONT FACE=\"Verdana, Arial, Helvetica\" SIZE=\"-1\">");
        content.append(l != null ? ((Object) (l)).toString() : "<I>system classloader</I>");
        content.append("</FONT></TD>\n");
        content.append("</TR>\n");
        text.setPlaceHolder("JAVAVMPROPS", ((Object) (content.toString())));
        ((AbstractWorker)this).sendText(text.toString(), "text/html");
    }

    protected String createDefaultAdminInfoPage(String baseURL)
    {
        StringBuffer content = new StringBuffer();
        content.append("<html><head><meta name=\"Author\" content=\"");
        content.append("DeploySam");
        content.append(" ");
        content.append("2.5.0");
        content.append("\">\n");
        content.append("<title>DeployDirector Server Info Page</title></head>\n");
        content.append("<body>\n");
        content.append("<FONT FACE=\"Verdana, Arial, Helvetica\"><H2>Error:</H2></FONT>\n");
        content.append("<FONT FACE=\"Verdana, Arial, Helvetica\">Default admin info page could not be\n");
        content.append("located in SAM jar file or on the file system. This will limit the ");
        content.append("amount of information that can be displayed here.\n Please make sure that ");
        content.append("the <tt>basedir</tt> parameter to the DeployDirector servlet is pointing ");
        content.append("to the director that the DeployDirector server was installed in.\n");
        content.append("</FONT><BR><HR>\n");
        content.append("<FONT FACE=\"Verdana, Arial, Helvetica\">To view this server's");
        content.append(" administration page, click <a href=\"");
        content.append(baseURL);
        content.append("/admin-old\">here</a>, or visit <TT>");
        content.append(baseURL);
        content.append("/admin-old</TT>.</FONT><BR><BR>\n");
        content.append("<FONT FACE=\"Verdana, Arial, Helvetica\">Version Information: ");
        content.append(getSAMVersion());
        content.append("</FONT><BR><BR>\n");
        content.append("<FONT FACE=\"Verdana, Arial, Helvetica\">");
        content.append("The following is the list of host names (and addresses) DeployDirector ");
        content.append("considers valid for this system. This is not an exclusive list.<BR>\n");
        content.append("<OL>\n");
        Vector hosts = getValidHostNames();
        int size = hosts.size();
        if(size == 0)
        {
            content.append("<LI><FONT COLOR=\"RED\">No valid host names found. This is a critical error");
            content.append(" and will prevent DeployDirector from running on this system.</FONT></LI>\n");
        } else
        {
            for(int i = 0; i < size; i++)
            {
                content.append("<LI>");
                content.append((String)hosts.elementAt(i));
                content.append("</LI>\n");
            }

        }
        content.append("</OL>\n");
        content.append("\n\n<hr WIDTH=\"100%\">\n");
        content.append("<FONT SIZE=\"-1\"><B><I>Powered By Sitraka's</I>\n");
        content.append("<A HREF=\"");
        content.append("http://www.sitraka.com/software/deploydirector");
        content.append("\">\n");
        content.append("<BR><img SRC=\"");
        content.append(baseURL);
        content.append("/");
        content.append("deploydirector_ss.gif");
        content.append("\" BORDER=0>\n<BR>");
        content.append("http://www.sitraka.com/software/deploydirector");
        content.append("</A></B></FONT>\n");
        content.append("</FONT></body>\n");
        content.append("</html>\n");
        return content.toString();
    }

    public Vector getValidHostNames()
    {
        Vector result = new Vector();
        result.addElement("localhost");
        try
        {
            InetAddress localhost = InetAddress.getLocalHost();
            String name = localhost.getHostName();
            String address = localhost.getHostAddress();
            if(name != null)
            {
                result.addElement(((Object) (name)));
                InetAddress allnames[] = InetAddress.getAllByName(name);
                int size = allnames != null ? allnames.length : 0;
                for(int i = 0; i < size; i++)
                {
                    String localname = allnames[i].getHostName();
                    String localaddress = allnames[i].getHostAddress();
                    if(localname != null && !localname.equals(((Object) (name))))
                        result.addElement(((Object) (localname)));
                    if(localaddress != null && !localaddress.equals(((Object) (address))))
                        result.addElement(((Object) (localaddress)));
                }

            }
            result.addElement(((Object) (localhost.getHostAddress())));
        }
        catch(UnknownHostException e) { }
        return result;
    }

    public String getSAMVersion()
    {
        return "DeploySam 2.5.0, Build: DD250-20021210-1039";
    }

    public String getPlatform()
    {
        StringBuffer platform = new StringBuffer();
        platform.append(System.getProperty("os.name"));
        platform.append(" ");
        platform.append(System.getProperty("os.version"));
        platform.append(" (");
        platform.append(PlatformIdentifier.getPlatformString());
        platform.append(")");
        return platform.toString();
    }

    public String getVMString()
    {
        StringBuffer vm = new StringBuffer();
        vm.append("Vendor: ");
        vm.append(System.getProperty("java.vendor"));
        vm.append(", version ");
        vm.append(System.getProperty("java.version"));
        return vm.toString();
    }
}
