// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LogReplicationMessage.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.common.checkpoint.LogCheckpoint;
import com.sitraka.deploy.common.checkpoint.LogObject;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            ReplicationMessage

public class LogReplicationMessage extends ReplicationMessage
{

    public LogReplicationMessage(String remote_server, int type, LogObject logObject, LogCheckpoint checkpoint, Object _auth, String otherServers)
    {
        super(remote_server, type, ((Object) (logObject)), ((com.sitraka.deploy.common.checkpoint.Checkpoint) (checkpoint)), _auth, otherServers);
    }

    public LogObject getLogObject()
    {
        return (LogObject)((ReplicationMessage)this).getUserData();
    }

    public LogCheckpoint getLogCheckpoint()
    {
        return (LogCheckpoint)((ReplicationMessage)this).getCheckpoint();
    }
}
