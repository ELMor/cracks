// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   VersionReplicationMessage.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.common.checkpoint.VersionCheckpoint;
import com.sitraka.deploy.common.checkpoint.VersionObject;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            ReplicationMessage

public class VersionReplicationMessage extends ReplicationMessage
{

    public VersionReplicationMessage(String remote_server, int type, VersionObject versionObject, VersionCheckpoint checkpoint, Object _auth, String otherServers)
    {
        super(remote_server, type, ((Object) (versionObject)), ((com.sitraka.deploy.common.checkpoint.Checkpoint) (checkpoint)), _auth, otherServers);
    }

    public VersionObject getVersionObject()
    {
        return (VersionObject)((ReplicationMessage)this).getUserData();
    }

    public VersionCheckpoint getVersionCheckpoint()
    {
        return (VersionCheckpoint)((ReplicationMessage)this).getCheckpoint();
    }

    public String getApplication()
    {
        return getVersionObject().getApplication();
    }

    public String getVersion()
    {
        return getVersionObject().getVersion();
    }
}
