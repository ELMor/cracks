// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   UploadWorker.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.checkpoint.ApplicationCheckpoint;
import com.sitraka.deploy.common.checkpoint.ApplicationObject;
import com.sitraka.deploy.common.checkpoint.BaseCheckpoint;
import com.sitraka.deploy.common.checkpoint.ConfigCheckpoint;
import com.sitraka.deploy.common.checkpoint.ConfigObject;
import com.sitraka.deploy.common.checkpoint.DARCheckpoint;
import com.sitraka.deploy.common.checkpoint.DARObject;
import com.sitraka.deploy.common.checkpoint.FileCheckpoint;
import com.sitraka.deploy.common.checkpoint.FileUploadObject;
import com.sitraka.deploy.common.checkpoint.InstallerCheckpoint;
import com.sitraka.deploy.common.checkpoint.InstallerObject;
import com.sitraka.deploy.common.checkpoint.JRECheckpoint;
import com.sitraka.deploy.common.checkpoint.JREObject;
import com.sitraka.deploy.common.checkpoint.LogCheckpoint;
import com.sitraka.deploy.common.checkpoint.LogObject;
import com.sitraka.deploy.common.checkpoint.VersionCheckpoint;
import com.sitraka.deploy.common.checkpoint.VersionObject;
import com.sitraka.deploy.sam.AbstractTrackerEntry;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.LogQueue;
import com.sitraka.deploy.sam.RequestParser;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.log.ClientDataLogger;
import com.sitraka.deploy.sam.log.LogException;
import com.sitraka.deploy.sam.receive.AbstractReceiver;
import com.sitraka.deploy.sam.receive.AppReceiver;
import com.sitraka.deploy.sam.receive.ConfigReceiver;
import com.sitraka.deploy.sam.receive.DARReceiver;
import com.sitraka.deploy.sam.receive.FileReceiver;
import com.sitraka.deploy.sam.receive.InstallerReceiver;
import com.sitraka.deploy.sam.receive.JREReceiver;
import com.sitraka.deploy.sam.receive.LogReceiver;
import com.sitraka.deploy.sam.receive.ReceiverEvent;
import com.sitraka.deploy.sam.receive.ReceiverListener;
import com.sitraka.deploy.sam.receive.VersionReceiver;
import com.sitraka.deploy.sam.request.RequestList;
import com.sitraka.deploy.sam.request.RequestObject;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import com.sitraka.deploy.util.Codecs;
import com.sitraka.deploy.util.Crypt;
import com.sitraka.deploy.util.PropertyUtils;
import com.sitraka.licensing.LicenseProperties;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            AbstractWorker, ReplicationThread

public class UploadWorker extends AbstractWorker
    implements ReceiverListener
{

    public String localhost;
    long startTime;

    public UploadWorker()
    {
        localhost = null;
        startTime = 0L;
        super.type = 7;
    }

    public void startWorking()
        throws ServletProcessException
    {
        localhost = Servlet.properties.getProperty("deploy.localhost");
        startTime = System.currentTimeMillis();
        InputStream is = super.server.getBinaryInputStream(super.request);
        if(is == null && (super.parser.getRequest() != 4001 || super.parser.getRequest() != 4000))
        {
            String msg = "Unable to access input stream from upload request";
            throw new ServletProcessException(msg, super.request, super.response, true);
        }
        ((AbstractWorker)this).setOrigServer();
        synchronized(super.STATE_LOCK)
        {
            switch(super.parser.getRequest())
            {
            case 3203: 
                receiveJRE(is);
                break;

            case 3104: 
                receiveVersion(is);
                break;

            case 3109: 
                receiveBundle(is);
                break;

            case 3303: 
                receiveConfig(is);
                break;

            case 3105: 
                receiveDAR(is);
                break;

            case 3103: 
                reorderVersions(is);
                break;

            case 3305: 
                receiveFile(is);
                break;

            case 3306: 
                receiveInstaller(is);
                break;

            case 4001: 
                setNewPassword();
                break;

            case 4000: 
                setNewLicense();
                break;
            }
        }
        try
        {
            is.close();
        }
        catch(IOException ioe) { }
    }

    protected void sendACK()
    {
        sendACK("Upload Received");
    }

    protected void sendACK(String message)
    {
        if(super.response == null)
        {
            return;
        } else
        {
            ((AbstractWorker)this).sendText(message);
            return;
        }
    }

    protected void receiveFile(InputStream is)
        throws ServletProcessException
    {
        if(super.parser.getFile() == null)
        {
            String msg = "HTML upload request missing name of installer file";
            ServletProcessException spe = new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
            spe.setApplication("DDAdmin");
            spe.setVersion(super.vault.getLatestVersion("DDAdmin"));
            throw spe;
        }
        FileUploadObject fileUpload = new FileUploadObject(localhost, super.parser.getFile());
        FileCheckpoint checkpoint = new FileCheckpoint(((Object) (fileUpload)));
        ((BaseCheckpoint) (checkpoint)).setAutoFlush(false);
        ((BaseCheckpoint) (checkpoint)).setState(0);
        ((BaseCheckpoint) (checkpoint)).setInputStream(is);
        ((BaseCheckpoint) (checkpoint)).setTempDir(super.vault.getServerTempDir());
        FileReceiver fr = new FileReceiver(super.server, super.vault);
        super.server.logMessage(super.request, "ServerHTMLUpdateStarted", super.none, super.none, "From: " + super.origServer);
        try
        {
            fr.processMessage(checkpoint);
        }
        catch(ServletProcessException spe)
        {
            spe.setRequest(super.request);
            spe.setResponse(super.response);
            throw spe;
        }
        sendACK();
        if(super.vault.listOtherServers() != null)
        {
            String auth = Codecs.base64Decode(super.server.getUserAuthorization(super.request).toString());
            AbstractWorker.getReplicationQueue(super.server).sendFileReplication(localhost, fileUpload, ((Object) (auth)));
        }
    }

    protected void receiveInstaller(InputStream is)
        throws ServletProcessException
    {
        if(super.parser.getFile() == null)
        {
            String msg = "HTML upload request missing name of installer file";
            ServletProcessException spe = new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
            spe.setApplication("DDAdmin");
            spe.setVersion(super.vault.getLatestVersion("DDAdmin"));
            throw spe;
        }
        InstallerObject installerObject = new InstallerObject(localhost, super.parser.getFile());
        InstallerCheckpoint checkpoint = new InstallerCheckpoint(((Object) (installerObject)));
        ((BaseCheckpoint) (checkpoint)).setAutoFlush(false);
        ((BaseCheckpoint) (checkpoint)).setState(0);
        ((BaseCheckpoint) (checkpoint)).setInputStream(is);
        ((BaseCheckpoint) (checkpoint)).setTempDir(super.vault.getServerTempDir());
        InstallerReceiver ir = new InstallerReceiver(super.server, super.vault);
        ((AbstractReceiver) (ir)).addListener(((ReceiverListener) (this)));
        super.server.logMessage(super.request, "ServerInstallerUpdateStarted", super.none, super.none, "From: " + super.origServer);
        RequestObject obj = createRequest();
        try
        {
            ir.processMessage(checkpoint);
        }
        catch(ServletProcessException spe)
        {
            requestAborted(obj, ((String) (null)));
            spe.setRequest(super.request);
            spe.setResponse(super.response);
            throw spe;
        }
        sendACK();
        requestCompleted(obj, "Request completed sucessfully");
        if(super.vault.listOtherServers() != null)
        {
            String auth = Codecs.base64Decode(super.server.getUserAuthorization(super.request).toString());
            AbstractWorker.getReplicationQueue(super.server).sendInstallerReplication(localhost, installerObject, ((Object) (auth)));
        }
    }

    protected void receiveLog(InputStream is)
        throws ServletProcessException
    {
        String name = super.parser.getName();
        String from = super.parser.getFrom();
        String to = super.parser.getTo();
        LogObject logObject = new LogObject(localhost, name, from, to);
        LogCheckpoint checkpoint = new LogCheckpoint(((Object) (logObject)));
        ((BaseCheckpoint) (checkpoint)).setAutoFlush(false);
        ((BaseCheckpoint) (checkpoint)).setState(0);
        ((BaseCheckpoint) (checkpoint)).setInputStream(is);
        ((BaseCheckpoint) (checkpoint)).setTempDir(super.vault.getServerTempDir());
        LogReceiver lr = new LogReceiver(super.server, super.vault);
        ((AbstractReceiver) (lr)).addListener(((ReceiverListener) (this)));
        RequestObject obj = createRequest();
        try
        {
            lr.processMessage(checkpoint);
        }
        catch(ServletProcessException spe)
        {
            requestAborted(obj, ((String) (null)));
            spe.setRequest(super.request);
            spe.setResponse(super.response);
            throw spe;
        }
        sendACK();
        requestCompleted(obj, "Request completed sucessfully");
        if(super.vault.listOtherServers() != null)
        {
            String auth = Codecs.base64Decode(super.server.getUserAuthorization(super.request).toString());
            AbstractWorker.getReplicationQueue(super.server).sendLogReplication(localhost, logObject, ((Object) (auth)));
        }
    }

    protected void receiveJRE(InputStream is)
        throws ServletProcessException
    {
        String vendor = super.parser.getVendor();
        String version = super.parser.getVersion();
        String platform = super.parser.getPlatform();
        String name = super.parser.getName();
        String format = super.parser.getFormat();
        String classpaths = super.parser.getClasspaths();
        if(vendor == null || version == null || platform == null || name == null || format == null || classpaths == null)
        {
            String msg = "JRE upload request is missing the following arguments: " + (vendor != null ? "" : " VENDOR") + (version != null ? "" : " VERSION") + (platform != null ? "" : " PLATFORM") + (name != null ? "" : " NAME") + (format != null ? "" : " FORMAT") + (classpaths != null ? "" : " CLASSPATHS") + ".";
            ServletProcessException spe = new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
            spe.setApplication("(vendor: " + vendor + ")");
            throw spe;
        }
        JREObject jreObject = new JREObject(localhost, vendor, version, platform, name, format, classpaths);
        JRECheckpoint checkpoint = new JRECheckpoint(((Object) (jreObject)));
        ((BaseCheckpoint) (checkpoint)).setAutoFlush(false);
        ((BaseCheckpoint) (checkpoint)).setState(0);
        ((BaseCheckpoint) (checkpoint)).setInputStream(is);
        ((BaseCheckpoint) (checkpoint)).setTempDir(super.vault.getServerTempDir());
        super.server.logMessage(super.request, "ServerJREUpdateStarted", "(vendor: " + vendor + ")", version, "(Platform: " + platform + ") From: " + super.origServer);
        JREReceiver jr = new JREReceiver(super.server, super.vault);
        ((AbstractReceiver) (jr)).addListener(((ReceiverListener) (this)));
        RequestObject obj = createRequest();
        try
        {
            jr.processMessage(checkpoint);
        }
        catch(ServletProcessException spe)
        {
            requestAborted(obj, ((String) (null)));
            spe.setRequest(super.request);
            spe.setResponse(super.response);
            throw spe;
        }
        super.server.logMessage(super.request, "ServerJREUpdateComplete", "(vendor: " + vendor + ")", version, "(Platform: " + platform + ") From: " + super.origServer);
        sendACK();
        requestCompleted(obj, "Request completed sucessfully");
        if(super.vault.listOtherServers() != null)
        {
            String auth = Codecs.base64Decode(super.server.getUserAuthorization(super.request).toString());
            AbstractWorker.getReplicationQueue(super.server).sendJREReplication(localhost, jreObject, ((Object) (auth)));
        }
    }

    protected void receiveVersion(InputStream is)
        throws ServletProcessException
    {
        String application = super.parser.getApplication();
        String version = super.parser.getVersion();
        if(application == null || version == null)
        {
            String msg = "Version upload request is missing data:  APPLICATION=" + application + " VERSION=" + version;
            throw new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
        }
        VersionObject versionObject = new VersionObject(localhost, application, version);
        VersionCheckpoint checkpoint = new VersionCheckpoint(((Object) (versionObject)));
        ((BaseCheckpoint) (checkpoint)).setAutoFlush(false);
        ((BaseCheckpoint) (checkpoint)).setState(0);
        ((BaseCheckpoint) (checkpoint)).setInputStream(is);
        ((BaseCheckpoint) (checkpoint)).setTempDir(super.vault.getServerTempDir());
        VersionReceiver vr = new VersionReceiver(super.server, super.vault);
        ((AbstractReceiver) (vr)).addListener(((ReceiverListener) (this)));
        super.server.logMessage(super.request, "ServerVersionUpdateStarted", application, version, "(" + application + " " + version + ") " + "From: " + super.origServer);
        RequestObject obj = createRequest();
        try
        {
            vr.processMessage(checkpoint);
        }
        catch(ServletProcessException spe)
        {
            requestAborted(obj, ((String) (null)));
            spe.setRequest(super.request);
            spe.setResponse(super.response);
            throw spe;
        }
        super.server.logMessage(super.request, "ServerVersionUpdateComplete", application, version, "(" + application + " " + version + ") " + "From: " + super.origServer);
        sendACK();
        requestCompleted(obj, "Request completed sucessfully");
        if(super.vault.listOtherServers() != null)
        {
            String auth = Codecs.base64Decode(super.server.getUserAuthorization(super.request).toString());
            AbstractWorker.getReplicationQueue(super.server).sendVersionReplication(localhost, versionObject, ((Object) (auth)));
        }
    }

    protected void receiveDAR(InputStream is)
        throws ServletProcessException
    {
        String darFile = super.parser.getFile();
        String application = super.parser.getApplication();
        String version = super.parser.getVersion();
        if(application == null || version == null || darFile == null)
        {
            String msg = "DAR upload missing data:  DAR = \"" + darFile + "\", Application = \"" + application + "\", Version = \"" + version + "\"";
            throw new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
        }
        boolean newApp = super.vault.getApplication(application) == null;
        DARObject darObject = new DARObject(localhost, darFile, application, version);
        DARCheckpoint checkpoint = new DARCheckpoint(((Object) (darObject)));
        ((BaseCheckpoint) (checkpoint)).setAutoFlush(false);
        ((BaseCheckpoint) (checkpoint)).setState(0);
        ((BaseCheckpoint) (checkpoint)).setInputStream(is);
        ((BaseCheckpoint) (checkpoint)).setTempDir(super.vault.getServerTempDir());
        DARReceiver dr = new DARReceiver(super.server, super.vault);
        ((AbstractReceiver) (dr)).addListener(((ReceiverListener) (this)));
        super.server.logMessage(super.request, "ServerDARUploadStarted", application, version, "DAR \"" + darFile + "\":  (" + application + ", " + version + ") " + "From: " + super.origServer);
        RequestObject obj = createRequest();
        try
        {
            dr.processMessage(checkpoint);
        }
        catch(ServletProcessException spe)
        {
            requestAborted(obj, ((String) (null)));
            spe.setRequest(super.request);
            spe.setResponse(super.response);
            throw spe;
        }
        super.server.logMessage(super.request, "ServerDARUploadComplete", application, version, "DAR \"" + darFile + "\":  (" + application + ", " + version + ") " + "From: " + super.origServer);
        sendACK();
        requestCompleted(obj, "Request completed sucessfully");
        if(super.vault.listOtherServers() != null)
        {
            String auth = Codecs.base64Decode(super.server.getUserAuthorization(super.request).toString());
            if(newApp)
            {
                ApplicationObject app_obj = new ApplicationObject(localhost, application);
                AbstractWorker.getReplicationQueue(super.server).sendApplicationReplication(localhost, app_obj, ((Object) (auth)));
            } else
            {
                VersionObject vo = new VersionObject(localhost, application, version);
                AbstractWorker.getReplicationQueue(super.server).sendVersionReplication(localhost, vo, ((Object) (auth)));
            }
        }
    }

    protected void receiveBundle(InputStream is)
        throws ServletProcessException
    {
        String application = super.parser.getApplication();
        if(application == null)
        {
            String msg = "Application upload request from " + super.origServer + " is missing data.  APPLICATION was null.";
            ServletProcessException spe = new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
            spe.setVersion(super.none);
            throw spe;
        }
        ApplicationObject applicationObject = new ApplicationObject(localhost, application);
        ApplicationCheckpoint checkpoint = new ApplicationCheckpoint(((Object) (applicationObject)));
        ((BaseCheckpoint) (checkpoint)).setAutoFlush(false);
        ((BaseCheckpoint) (checkpoint)).setState(0);
        ((BaseCheckpoint) (checkpoint)).setInputStream(is);
        ((BaseCheckpoint) (checkpoint)).setTempDir(super.vault.getServerTempDir());
        AppReceiver ar = new AppReceiver(super.server, super.vault);
        ((AbstractReceiver) (ar)).addListener(((ReceiverListener) (this)));
        super.server.logMessage(super.request, "ServerBundleUpdateStarted", application, "(none - new bundle)", "(" + application + ") " + "From: " + super.origServer);
        RequestObject obj = createRequest();
        try
        {
            ar.processMessage(checkpoint);
        }
        catch(ServletProcessException spe)
        {
            requestAborted(obj, ((String) (null)));
            spe.setRequest(super.request);
            spe.setResponse(super.response);
            throw spe;
        }
        super.server.logMessage(super.request, "ServerBundleUpdateComplete", application, "(none - new bundle)", "(" + application + ") " + "From: " + super.origServer);
        sendACK();
        requestCompleted(obj, "Request completed sucessfully");
        if(super.vault.listOtherServers() != null)
        {
            String auth = Codecs.base64Decode(super.server.getUserAuthorization(super.request).toString());
            AbstractWorker.getReplicationQueue(super.server).sendApplicationReplication(localhost, applicationObject, ((Object) (auth)));
        }
    }

    protected void setNewPassword()
        throws ServletProcessException
    {
        StringBuffer content = new StringBuffer();
        String pass1 = super.parser.getParam("PASSWORD1");
        String pass2 = super.parser.getParam("PASSWORD2");
        String name = super.parser.getParam("NAME");
        if(name == null || pass1 == null || pass2 == null)
        {
            String msg = "Unable to set password; the following parameters are missing: " + (name != null ? "" : " NAME") + (pass1 != null ? "" : " PASSWORD1") + (pass2 != null ? "" : " PASSWORD2") + ".";
            ServletProcessException spe = new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, false);
            spe.setApplication("DeploySam");
            spe.setVersion("2.5.0");
            throw spe;
        }
        if(!pass1.equals(((Object) (pass2))))
        {
            String msg = "Unable to set password, provided passwords do not match.";
            ServletProcessException spe = new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, false);
            spe.setApplication("DeploySam");
            spe.setVersion("2.5.0");
            throw spe;
        }
        File propFile = new File(super.vault.getVaultBaseDir(), "cluster.properties");
        if(!propFile.canRead())
        {
            String msg = "Unable to set password, cannot read 'cluster.properties'.";
            ServletProcessException spe = new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, false);
            spe.setApplication("DeploySam");
            spe.setVersion("2.5.0");
            throw spe;
        }
        String oldName = Servlet.properties.getProperty("deploy.admin.username");
        String oldPass = Servlet.properties.getProperty("deploy.admin.password");
        try
        {
            FileInputStream fis = new FileInputStream(propFile);
            Properties props = new Properties();
            props.load(((InputStream) (fis)));
            fis.close();
            fis = null;
            ((Hashtable) (props)).put("deploy.admin.username", ((Object) (name)));
            String encryptedPass = Crypt.crypt(oldPass, pass1);
            ((Hashtable) (props)).put("deploy.admin.password", ((Object) (encryptedPass)));
            FileOutputStream fos = new FileOutputStream(propFile);
            try
            {
                props.store(((OutputStream) (fos)), "DeploySam: cluster.properties");
            }
            catch(IOException ioe)
            {
                String msg = "Unable to set password, cannot re-write 'cluster.properties'.";
                ServletProcessException spe = new ServletProcessException(msg, ((Throwable) (ioe)), "ServerRequestFailed", super.request, super.response, false);
                spe.setApplication("DeploySam");
                spe.setVersion("2.5.0");
                throw spe;
            }
            ((OutputStream) (fos)).flush();
            fos.close();
            props = null;
        }
        catch(IOException ioe)
        {
            String msg = "Unable to set password, cannot load/save 'cluster.properties'.";
            ServletProcessException spe = new ServletProcessException(msg, ((Throwable) (ioe)), "ServerRequestFailed", super.request, super.response, true);
            spe.setApplication("DeploySam");
            spe.setVersion("2.5.0");
            throw spe;
        }
        content.append("<html>\n");
        content.append("<head>\n");
        content.append("<meta name=\"Author\" content=\"");
        content.append("DeploySam");
        content.append(" ");
        content.append("2.5.0");
        content.append("\">\n");
        content.append("<title>DeployDirector Administraton</title>\n");
        content.append("</head>\n");
        content.append("<BODY>\n");
        content.append("<center>\n");
        content.append("<h2>");
        content.append(Servlet.properties.getProperty("deploy.server.name"));
        content.append("</h2>\n\n");
        content.append("The new administrator username and password has been saved. This change is <B>not</b>\n");
        content.append("replicated to any other servers in the cluster.\n");
        content.append("<BR><BR>To return to the administrator's page, click ");
        content.append("<A HREF=\"");
        content.append(Servlet.properties.getProperty("deploy.localhost"));
        content.append("/admin\">here</A>\n");
        content.append("</center>\n");
        content.append("</BODY>\n");
        content.append("</HTML>\n");
        ((AbstractWorker)this).sendText(content.toString(), "text/html");
        super.server.restartServer();
    }

    protected void parseLicenseText()
        throws ServletProcessException
    {
        String licenseText = super.parser.getParam("LICENSETEXT");
        if(licenseText == null)
            throw new ServletProcessException("No license data", super.request, super.response, false);
        Properties properties = new Properties();
        ByteArrayInputStream stream = new ByteArrayInputStream(licenseText.getBytes());
        try
        {
            properties.load(((InputStream) (stream)));
        }
        catch(IOException ioe)
        {
            throw new ServletProcessException("Error reading license text.", ((Throwable) (ioe)), super.request, super.response, false);
        }
        validateAndInstallLicense(properties);
    }

    private void validateAndInstallLicense(Properties props)
        throws ServletProcessException
    {
        int numApplications = super.vault.countApplications();
        LicenseProperties licenseProperties = new LicenseProperties();
        String serial_number = PropertyUtils.readDefaultString(props, "serial_number", ((String) (null)));
        if(serial_number == null)
        {
            String msg = "No serial number listed.";
            throw new ServletProcessException(msg, super.request, super.response, false);
        }
        ((Properties) (licenseProperties)).setProperty("serial_number", serial_number);
        String hosts = PropertyUtils.readDefaultString(props, "hosts", ((String) (null)));
        if(hosts == null)
        {
            String msg = "No host names specified.";
            throw new ServletProcessException(msg, super.request, super.response, false);
        }
        ((Properties) (licenseProperties)).setProperty("hosts", hosts);
        int hostResult = licenseProperties.validateHost("hosts");
        if(hostResult != 1)
        {
            if(hostResult == 12)
            {
                String msg = "Hosts property not found.";
                throw new ServletProcessException(msg, super.request, super.response, false);
            }
            if(hostResult == 3)
            {
                String msg = "Number of CPUs exceeded.";
                throw new ServletProcessException(msg, super.request, super.response, false);
            }
            if(hostResult == 2)
            {
                String msg = "Hostname is invalid.";
                throw new ServletProcessException(msg, super.request, super.response, false);
            } else
            {
                String msg = "Unknown hostname error.";
                throw new ServletProcessException(msg, super.request, super.response, false);
            }
        }
        int num_clients = -1;
        String clients = props.getProperty("clients", ((String) (null)));
        if(clients != null)
        {
            try
            {
                num_clients = Integer.parseInt(clients);
            }
            catch(NumberFormatException e)
            {
                if("(unlimited)".equals(((Object) (clients))))
                {
                    num_clients = -1;
                } else
                {
                    String msg = "Maximum number of clients incorrectly specified or missing: " + clients;
                    throw new ServletProcessException(msg, super.request, super.response, false);
                }
            }
            ((Properties) (licenseProperties)).setProperty("clients", clients);
        }
        int client_count = 0;
        try
        {
            client_count = super.server.getLogQueue().getClientDataLogger().getClients(0xffffffffcf7c5800L);
        }
        catch(LogException le) { }
        catch(NullPointerException npe) { }
        if(num_clients > 0 && client_count > num_clients)
        {
            String msg = "number of current clients (" + client_count + ") exceeds maximum allowed by license (" + num_clients + ")";
            throw new ServletProcessException(msg, super.request, super.response, false);
        }
        String signature = PropertyUtils.readDefaultString(props, "sitraka.license.signature", ((String) (null)));
        if(signature == null)
        {
            String msg = "License signature is missing";
            throw new ServletProcessException(msg, super.request, super.response, false);
        }
        ((Properties) (licenseProperties)).setProperty("sitraka.license.signature", signature);
        boolean checkNumberOfBundles = true;
        String app = props.getProperty("applications", ((String) (null)));
        Vector applications;
        if(app == null)
            applications = new Vector();
        else
        if(app.trim().length() == 0 || app.indexOf("(unlimited)") >= 0)
        {
            ((Properties) (licenseProperties)).setProperty("applications", app);
            applications = new Vector();
        } else
        {
            ((Properties) (licenseProperties)).setProperty("applications", app);
            applications = PropertyUtils.readVector(props, "applications", ',');
            if(applications == null)
                applications = new Vector();
            else
                checkNumberOfBundles = false;
        }
        int num_apps = -1;
        String bundles = props.getProperty("bundles", ((String) (null)));
        if(bundles != null)
        {
            try
            {
                num_apps = Integer.parseInt(bundles);
            }
            catch(NumberFormatException e)
            {
                if("(unlimited)".equals(((Object) (bundles))))
                {
                    num_apps = -1;
                } else
                {
                    String msg = "Maximum number of bundles incorrectly specified or missing: " + bundles;
                    throw new ServletProcessException(msg, super.request, super.response, false);
                }
            }
            ((Properties) (licenseProperties)).setProperty("bundles", bundles);
        }
        if(checkNumberOfBundles && num_apps > 0 && numApplications > num_apps)
        {
            String msg = "Number of user bundles (" + numApplications + ") exceeds maximum allowed by license (" + num_apps + ")";
            throw new ServletProcessException(msg, super.request, super.response, false);
        }
        String date_string = props.getProperty("expiry", ((String) (null)));
        if(date_string != null && !"(never)".equals(((Object) (date_string))))
        {
            ((Properties) (licenseProperties)).setProperty("expiry", date_string);
            if(licenseProperties.isDateExpired("expiry"))
            {
                String msg = "License has expired: " + date_string;
                throw new ServletProcessException(msg, super.request, super.response, false);
            }
        }
        String ddLicenseVersion = PropertyUtils.readDefaultString(props, "license_version", ((String) (null)));
        if(!"2.5".equals(((Object) (ddLicenseVersion))))
        {
            String msg = "License Version is incompatible: " + ddLicenseVersion + " vs " + "2.5";
            throw new ServletProcessException(msg, super.request, super.response, false);
        }
        ((Properties) (licenseProperties)).setProperty("license_version", ddLicenseVersion);
        String licenseVersion = PropertyUtils.readDefaultString(props, "sitraka.license.version", ((String) (null)));
        if(!"2.5".equals(((Object) (licenseVersion))))
        {
            String msg = "License Version is missing.";
            throw new ServletProcessException(msg, super.request, super.response, false);
        }
        ((Properties) (licenseProperties)).setProperty("sitraka.license.version", licenseVersion);
        String licenseType = PropertyUtils.readDefaultString(props, "type", ((String) (null)));
        if(!"department".equals(((Object) (licenseType))) && !"enterprise".equals(((Object) (licenseType))) && !"developer".equals(((Object) (licenseType))) && !"evaluation".equals(((Object) (licenseType))))
        {
            String msg = "License Type is invalid: " + licenseType;
            throw new ServletProcessException(msg, super.request, super.response, false);
        }
        ((Properties) (licenseProperties)).setProperty("type", licenseType);
        Class centinal;
        if("department".equals(((Object) (licenseType))) || "developer".equals(((Object) (licenseType))) || "evaluation".equals(((Object) (licenseType))))
            try
            {
                centinal = Class.forName("org.apache.tomcat.core.Security");
            }
            catch(ClassNotFoundException cnfe)
            {
                String msg = "When using a " + licenseType + " license, " + "DeployDirector must be run with the standalone " + "Tomcat Server shipped by Sitraka.";
                throw new ServletProcessException(msg, super.request, super.response, false);
            }
        String licenseProduct = PropertyUtils.readDefaultString(props, "product", ((String) (null)));
        if(licenseProduct == null)
        {
            String msg = "License Product is invalid '" + licenseProduct + "'";
            ServletProcessException spe = new ServletProcessException(msg, "ServerLicenseError", super.request, super.response, false);
            spe.setApplication("DeploySam");
            spe.setVersion("2.5.0");
            throw spe;
        }
        ((Properties) (licenseProperties)).setProperty("product", licenseProduct);
        licenseProperties.validate();
        boolean result = licenseProperties.isValid();
        if(!result)
        {
            String msg = "Data does not match signature";
            throw new ServletProcessException(msg, super.request, super.response, false);
        }
        File propFile = new File(super.vault.getVaultBaseDir(), "cluster.properties");
        if(!propFile.canRead())
        {
            String msg = "Cannot access 'cluster.properties' to change licensing info.";
            throw new ServletProcessException(msg, super.request, super.response, false);
        }
        try
        {
            FileInputStream fis = new FileInputStream(propFile);
            Properties clusterProps = new Properties();
            clusterProps.load(((InputStream) (fis)));
            fis.close();
            fis = null;
            ((Hashtable) (clusterProps)).remove("applications");
            ((Hashtable) (clusterProps)).remove("bundles");
            ((Hashtable) (clusterProps)).remove("clients");
            ((Hashtable) (clusterProps)).remove("expiry");
            ((Hashtable) (clusterProps)).remove("hosts");
            ((Hashtable) (clusterProps)).remove("license_version");
            ((Hashtable) (clusterProps)).remove("serial_number");
            ((Hashtable) (clusterProps)).remove("sitraka.license.signature");
            ((Hashtable) (clusterProps)).remove("sitraka.license.version");
            ((Hashtable) (clusterProps)).remove("type");
            String key;
            for(Enumeration e = ((Hashtable) (licenseProperties)).keys(); e.hasMoreElements(); ((Hashtable) (clusterProps)).put(((Object) (key)), ((Object) (((Properties) (licenseProperties)).getProperty(key)))))
                key = (String)e.nextElement();

            FileOutputStream fos = new FileOutputStream(propFile);
            try
            {
                clusterProps.store(((OutputStream) (fos)), "DeploySam: cluster.properties");
            }
            catch(IOException ioe) { }
            ((OutputStream) (fos)).flush();
            fos.close();
            clusterProps = null;
        }
        catch(IOException ioe)
        {
            String msg = "Unable to load cluster properties file for new licensing info.";
            ServletProcessException spe = new ServletProcessException(msg, ((Throwable) (ioe)), super.request, super.response, true);
            spe.setApplication("DeploySam");
            spe.setVersion("2.5.0");
            throw spe;
        }
    }

    protected void setNewLicense()
        throws ServletProcessException
    {
        boolean result = false;
        if(super.parser.getParam("LICENSETEXT") == null)
        {
            String msg = "No license text to parse.";
            throw new ServletProcessException(msg, super.request, super.response, false);
        } else
        {
            parseLicenseText();
            String baseURL = ((AbstractWorker)this).stripURL(super.server.getRequestUrl(super.request), "") + "/admin";
            StringBuffer content = new StringBuffer();
            content.append("<html>\n");
            content.append("<head>\n");
            content.append("<meta name=\"Author\" content=\"");
            content.append("DeploySam");
            content.append(" ");
            content.append("2.5.0");
            content.append("\">\n");
            content.append("<META HTTP-EQUIV=\"Refresh\" content=\"10; URL=");
            content.append(baseURL);
            content.append("\">\n");
            content.append("<title>DeployDirector Administraton</title>\n");
            content.append("</head>\n");
            content.append("<BODY>\n");
            content.append("<FONT FACE=\"Verdana, Arial, Helvetica\" SIZE=\"-1\">\n");
            content.append("<div ALIGN=right>\n");
            content.append("<b><font size=+2>Administrator's Page, version 2.5.0</FONT></b><BR>\n");
            content.append("<i><font size=-1>This page is intended for the administrator\n");
            content.append("of this DeployDirector server.</font></i><BR>\n");
            content.append("</div>\n");
            content.append("<hr WIDTH=\"100%\">");
            content.append("<BR><font size=+1>The new license information has been accepted and saved.</FONT><BR>\n");
            content.append("<BLOCKQUOTE>\n");
            content.append("To return to the administrator's page, click ");
            content.append("<A HREF=\"");
            content.append(baseURL);
            content.append("\">here</A>\n");
            content.append("<BR>You will automatically be taken back there in 10 seconds.<BR><BR>\n");
            content.append("</BLOCKQUOTE>\n");
            content.append("<hr WIDTH=\"100%\"><b><i><font size=-1>Powered By Sitraka Software's</font></i></b>\n");
            content.append("<br><b><font size=-1><a href=\"http://www.sitraka.com/software/deploydirector\">\n");
            content.append("http://www.sitraka.com/software/deploydirector</a></font></b>\n");
            content.append("</FONT>\n");
            content.append("</BODY>\n");
            content.append("</HTML>\n");
            ((AbstractWorker)this).sendText(content.toString(), "text/html");
            super.server.restartServer();
            return;
        }
    }

    protected void receiveConfig(InputStream is)
        throws ServletProcessException
    {
        ConfigObject configObject = new ConfigObject(localhost);
        ConfigCheckpoint checkpoint = new ConfigCheckpoint(((Object) (configObject)));
        ((BaseCheckpoint) (checkpoint)).setAutoFlush(false);
        ((BaseCheckpoint) (checkpoint)).setState(0);
        ((BaseCheckpoint) (checkpoint)).setInputStream(is);
        ((BaseCheckpoint) (checkpoint)).setTempDir(super.vault.getServerTempDir());
        ConfigReceiver cr = new ConfigReceiver(super.server, super.vault);
        super.server.logMessage(super.request, "ServerConfigUpdateStarted", super.none, super.none, "From: " + super.origServer);
        String name = super.parser.getName();
        boolean wasLicensed = Servlet.isLicensed();
        try
        {
            cr.processMessage(checkpoint);
        }
        catch(ServletProcessException spe)
        {
            spe.setRequest(super.request);
            spe.setResponse(super.response);
            throw spe;
        }
        sendACK();
        if(name.equals("cluster.properties"))
            if(wasLicensed)
            {
                if(((AbstractWorker)this).getVault().listOtherServers() != null)
                {
                    String auth = Codecs.base64Decode(super.server.getUserAuthorization(super.request).toString());
                    AbstractWorker.getReplicationQueue(super.server).sendConfigReplication(localhost, configObject, ((Object) (auth)));
                }
            } else
            if(!Servlet.isLicensed())
                throw new ServletProcessException("foo", super.request, super.response, false);
    }

    protected void reorderVersions(InputStream is)
        throws ServletProcessException
    {
        String appname = super.parser.getApplication();
        if(appname == null || is == null)
        {
            String msg = "Error processing VERSIONREORDER command, some data was missing.  Application specified was '" + appname + "', input stream was " + (is != null ? "present" : "missing");
            throw new ServletProcessException(msg, super.request, super.response, true);
        }
        int content_length = super.server.getContentLength(super.request);
        if(content_length <= 0)
        {
            String msg = "Unknown content length in upload. Will not process the request";
            throw new ServletProcessException(msg, super.request, super.response, true);
        }
        StringBuffer content = new StringBuffer();
        try
        {
            ZipInputStream zis = new ZipInputStream(is);
            ZipEntry entry = zis.getNextEntry();
            if(entry == null || !entry.getName().equals("versions.lst"))
            {
                String msg = "Error, first entry in the uploaded zip was not: versions.lst";
                throw new ServletProcessException(msg, super.request, super.response, true);
            }
            byte buffer[] = new byte[2048];
            for(int n = -1; (n = zis.read(buffer, 0, 2048)) != -1;)
                content.append(new String(buffer, 0, n));

        }
        catch(IOException e)
        {
            String msg = "Error occured during upload of new versions list: " + ((Throwable) (e)).getMessage();
            throw new ServletProcessException(msg, super.request, super.response, true);
        }
        if(content.length() == 0)
        {
            String msg = "Error, no data found in uploaded versions list";
            throw new ServletProcessException(msg, super.request, super.response, true);
        }
        Application app = super.vault.getApplication(appname);
        if(app == null)
        {
            String msg = "Error, (application: " + appname + ") does not exist on this server";
            throw new ServletProcessException(msg, super.request, super.response, true);
        }
        if(!app.areVersionsOutOfOrder(content.toString()))
            return;
        if(!app.resyncVersionOrder(content.toString()))
        {
            String msg = "Error, (application: " + appname + ") unable to resync to new versions list";
            throw new ServletProcessException(msg, super.request, super.response, true);
        }
        if(!super.vault.createApplicationVersionsFile(app, ((File) (null)), false))
        {
            String msg = "Error, unable to save new version order to disk";
            throw new ServletProcessException(msg, super.request, super.response, true);
        }
        sendACK();
        if(super.vault.listOtherServers() != null)
        {
            ApplicationObject applicationObject = new ApplicationObject(localhost, appname);
            String auth = Codecs.base64Decode(super.server.getUserAuthorization(super.request).toString());
            AbstractWorker.getReplicationQueue(super.server).sendVersionReorder(localhost, applicationObject, ((Object) (auth)));
        }
    }

    protected RequestObject createRequest()
    {
        RequestObject obj = RequestList.addRequest(super.parser, ((File) (null)));
        ((AbstractTrackerEntry) (obj)).setStatus(1);
        ((AbstractTrackerEntry) (obj)).setStatusText("Receiving data");
        return obj;
    }

    protected void requestAborted(RequestObject obj, String message)
    {
        ((AbstractTrackerEntry) (obj)).setStatus(4);
        ((AbstractTrackerEntry) (obj)).setStatusText("Uploaded data could not be processed. Check server log for details");
    }

    protected void requestCompleted(RequestObject obj, String message)
    {
        ((AbstractTrackerEntry) (obj)).setStatus(3);
        ((AbstractTrackerEntry) (obj)).setStatusText("Request completed sucessfully");
    }

    public void receiveZipComplete(ReceiverEvent re)
    {
        RequestObject obj = (RequestObject)RequestList.getRequest(super.parser);
        if(obj == null)
        {
            return;
        } else
        {
            ((AbstractTrackerEntry) (obj)).setStatus(2);
            ((AbstractTrackerEntry) (obj)).setStatusText("Upload being processed");
            super.server.setContentType(super.response, "text/plain");
            super.server.setContentLength(super.response, obj.toString().length());
            super.server.setHeader(super.response, "Accept-Ranges", "none");
            super.server.setHeader(super.response, "DeploySam-ID", "" + ((AbstractTrackerEntry) (obj)).getID());
            super.server.setHeader(super.response, "DeploySam-StatusText", ((AbstractTrackerEntry) (obj)).getStatusText());
            super.server.setHeader(super.response, "DeploySam-StatusCode", "" + ((AbstractTrackerEntry) (obj)).getStatus());
            super.server.setHeader(super.response, "Cache-Control", "no-cache");
            super.server.setHeader(super.response, "Expires", (new Date(System.currentTimeMillis() - 0xf4240L)).toString());
            super.server.setHeader(super.response, "Last-Modified", (new Date()).toString());
            PrintWriter out = super.server.getTextOutputStream(super.response);
            out.print(obj.toString());
            out.flush();
            out.close();
            super.response = null;
            return;
        }
    }

    public void receiveFileComplete(ReceiverEvent receiverevent)
    {
    }
}
