// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FileReplicationMessage.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.common.checkpoint.FileCheckpoint;
import com.sitraka.deploy.common.checkpoint.FileUploadObject;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            ReplicationMessage

public class FileReplicationMessage extends ReplicationMessage
{

    public FileReplicationMessage(String remoteServer, int type, FileUploadObject fileObject, FileCheckpoint checkpoint, Object auth, String otherServers)
    {
        super(remoteServer, type, ((Object) (fileObject)), ((com.sitraka.deploy.common.checkpoint.Checkpoint) (checkpoint)), auth, otherServers);
    }

    public FileUploadObject getFileUploadObject()
    {
        return (FileUploadObject)((ReplicationMessage)this).getUserData();
    }

    public FileCheckpoint getFileCheckpoint()
    {
        return (FileCheckpoint)((ReplicationMessage)this).getCheckpoint();
    }

    public String getFilename()
    {
        return getFileUploadObject().getFilename();
    }
}
