// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ReplicationThread.java

package com.sitraka.deploy.sam.worker;

import com.klg.jclass.util.JCStringTokenizer;
import com.sitraka.deploy.common.Requests;
import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.common.checkpoint.ApplicationCheckpoint;
import com.sitraka.deploy.common.checkpoint.ApplicationObject;
import com.sitraka.deploy.common.checkpoint.ConfigCheckpoint;
import com.sitraka.deploy.common.checkpoint.ConfigObject;
import com.sitraka.deploy.common.checkpoint.FileCheckpoint;
import com.sitraka.deploy.common.checkpoint.FileUploadObject;
import com.sitraka.deploy.common.checkpoint.InstallerCheckpoint;
import com.sitraka.deploy.common.checkpoint.InstallerObject;
import com.sitraka.deploy.common.checkpoint.JRECheckpoint;
import com.sitraka.deploy.common.checkpoint.JREObject;
import com.sitraka.deploy.common.checkpoint.LogCheckpoint;
import com.sitraka.deploy.common.checkpoint.LogObject;
import com.sitraka.deploy.common.checkpoint.VersionCheckpoint;
import com.sitraka.deploy.common.checkpoint.VersionObject;
import com.sitraka.deploy.common.connection.AbstractHttpConnection;
import com.sitraka.deploy.common.connection.HttpConnection;
import com.sitraka.deploy.common.timer.Timer;
import com.sitraka.deploy.common.timer.TimerEvent;
import com.sitraka.deploy.common.timer.TimerEventListener;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.receive.AbstractReceiver;
import com.sitraka.deploy.sam.receive.AppReceiver;
import com.sitraka.deploy.sam.receive.ConfigReceiver;
import com.sitraka.deploy.sam.receive.FileReceiver;
import com.sitraka.deploy.sam.receive.InstallerReceiver;
import com.sitraka.deploy.sam.receive.JREReceiver;
import com.sitraka.deploy.sam.receive.LogReceiver;
import com.sitraka.deploy.sam.receive.VersionReceiver;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Enumeration;
import java.util.Vector;
import java.util.zip.ZipOutputStream;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            ReplicationTimerEvent, ReplicationMessage, JREReplicationMessage, FileReplicationMessage, 
//            InstallerReplicationMessage, LogReplicationMessage, ConfigReplicationMessage, VersionReplicationMessage, 
//            BundleReplicationMessage

public class ReplicationThread extends Timer
    implements TimerEventListener, Requests
{

    protected ServerActions server;
    protected File serializeFile;
    protected static final String none = "(none)";
    protected static final int TYPE_RECEIVE = 1;
    protected static final int TYPE_SEND = 2;
    protected static final int TYPE_DELETE = 3;
    protected static final int TYPE_REORDER = 4;
    private static final long ONE_HOUR = 0x36ee80L;
    private static final long RETRY_SEQUENCE[] = {
        1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L, 55L, 
        89L
    };
    private long oneHour;

    public ReplicationThread(ServerActions server, File file)
    {
        this.server = null;
        serializeFile = null;
        oneHour = 0x36ee80L;
        this.server = server;
        serializeFile = file;
        long oneHourEquivalent = PropertyUtils.readInterval(Servlet.properties, "deploy.debug.one_hour", -1L);
        if(oneHourEquivalent > 0L)
            oneHour = oneHourEquivalent;
        else
            oneHour = 0x36ee80L;
        loadQueue();
    }

    public void loadQueue()
    {
        Vector v = null;
        File file = getSerializeFile();
        if(!file.exists() || !file.canRead())
            return;
        try
        {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream in_stream = new ObjectInputStream(((java.io.InputStream) (new BufferedInputStream(((java.io.InputStream) (fis))))));
            v = (Vector)in_stream.readObject();
            in_stream.close();
            in_stream = null;
        }
        catch(IOException ioe) { }
        catch(ClassNotFoundException cnfe) { }
        if(v != null)
        {
            TimerEvent event;
            for(Enumeration e = v.elements(); e.hasMoreElements(); event.addTimerEventListener(((TimerEventListener) (this))))
                event = (TimerEvent)e.nextElement();

            ((Timer)this).setQueue(v);
        }
    }

    public void saveQueue()
    {
        try
        {
            FileOutputStream fos = new FileOutputStream(getSerializeFile());
            ObjectOutputStream out_stream = new ObjectOutputStream(((java.io.OutputStream) (new BufferedOutputStream(((java.io.OutputStream) (fos))))));
            out_stream.writeObject(((Object) (((Timer)this).getQueue())));
            out_stream.flush();
            out_stream.close();
            out_stream = null;
        }
        catch(IOException ioe) { }
    }

    public void setSerializeFile(File file)
    {
        serializeFile = file;
    }

    public File getSerializeFile()
    {
        return serializeFile;
    }

    public void eventOccured(TimerEvent e)
    {
        ReplicationTimerEvent rte = (ReplicationTimerEvent)e;
        ReplicationMessage message = rte.getMessage();
        try
        {
            processMessage(((Object) (message)));
        }
        catch(ServletProcessException spe)
        {
            String app = spe.getApplication();
            String ver = spe.getVersion();
            String cat = spe.getCategory();
            String msg = ((Throwable) (spe)).getMessage();
            String name = spe.getRemoteName();
            server.logMessage(name, cat, app, ver, msg);
        }
        saveQueue();
    }

    public void flushOccured(TimerEvent timerevent)
    {
    }

    protected void processMessage(Object message)
        throws ServletProcessException
    {
        if(((ReplicationMessage)message).getOtherServers() == null)
        {
            String msg = "No other servers in cluster, cannot replicate. Replication message type: " + (message != null ? message.getClass().getName() : "(null) message");
            throw new ServletProcessException(msg, "ServerInternalError", "(none)", "(none)", true);
        }
        if(message instanceof JREReplicationMessage)
        {
            JREReplicationMessage jre_msg = (JREReplicationMessage)message;
            switch(((ReplicationMessage) (jre_msg)).getType())
            {
            case 1: // '\001'
                JREReceiver jr = new JREReceiver(server, server.getVault());
                ((AbstractReceiver) (jr)).setAdminIDObject(((ReplicationMessage) (jre_msg)).getAuthorization());
                jr.processMessage((JRECheckpoint)((ReplicationMessage) (jre_msg)).getCheckpoint());
                break;

            case 2: // '\002'
            case 3: // '\003'
                HttpConnection conn = new HttpConnection();
                ((AbstractHttpConnection) (conn)).setIsAdmin(true);
                ((AbstractHttpConnection) (conn)).setVendor(jre_msg.getVendor());
                ((AbstractHttpConnection) (conn)).setVersion(jre_msg.getVersion());
                ((AbstractHttpConnection) (conn)).setPlatform(jre_msg.getPlatform());
                ((AbstractHttpConnection) (conn)).setUserIDObject(((ReplicationMessage) (jre_msg)).getAuthorization());
                ((AbstractHttpConnection) (conn)).setClassPaths(jre_msg.getClasspaths());
                ((AbstractHttpConnection) (conn)).setFormat(jre_msg.getFormat());
                ((AbstractHttpConnection) (conn)).setName(jre_msg.getName());
                ((AbstractHttpConnection) (conn)).setServerParam(((ReplicationMessage) (jre_msg)).getServer());
                if(((ReplicationMessage) (jre_msg)).getType() == 2)
                    ((AbstractHttpConnection) (conn)).setCommand("SERVERNEWJRE");
                else
                    ((AbstractHttpConnection) (conn)).setCommand("DELETEJRE");
                sendNotification(conn, ((ReplicationMessage) (jre_msg)));
                break;

            default:
                String msg = "Unsupported replication type requested for JRE: " + ((ReplicationMessage) (jre_msg)).getType();
                throw new ServletProcessException(msg, "ServerInternalError", "(none)", "(none)", true);
            }
        } else
        if(message instanceof FileReplicationMessage)
        {
            FileReplicationMessage file_msg = (FileReplicationMessage)message;
            switch(((ReplicationMessage) (file_msg)).getType())
            {
            case 1: // '\001'
                FileReceiver fr = new FileReceiver(server, server.getVault());
                ((AbstractReceiver) (fr)).setAdminIDObject(((ReplicationMessage) (file_msg)).getAuthorization());
                fr.processMessage((FileCheckpoint)((ReplicationMessage) (file_msg)).getCheckpoint());
                break;

            case 2: // '\002'
                HttpConnection http = new HttpConnection();
                ((AbstractHttpConnection) (http)).setIsAdmin(true);
                ((AbstractHttpConnection) (http)).setUserIDObject(((ReplicationMessage) (file_msg)).getAuthorization());
                ((AbstractHttpConnection) (http)).setServerParam(((ReplicationMessage) (file_msg)).getServer());
                ((AbstractHttpConnection) (http)).setFilename(FileUtils.makeInternalPath(file_msg.getFilename()));
                ((AbstractHttpConnection) (http)).setCommand("SERVERNEWIHTML");
                sendNotification(http, ((ReplicationMessage) (file_msg)));
                break;

            default:
                String msg = "Unsupported replication type requested for File: " + ((ReplicationMessage) (file_msg)).getType();
                throw new ServletProcessException(msg, "ServerInternalError", "(none)", "(none)", true);
            }
        } else
        if(message instanceof InstallerReplicationMessage)
        {
            InstallerReplicationMessage inst_msg = (InstallerReplicationMessage)message;
            switch(((ReplicationMessage) (inst_msg)).getType())
            {
            case 1: // '\001'
                InstallerReceiver ir = new InstallerReceiver(server, server.getVault());
                ((AbstractReceiver) (ir)).setAdminIDObject(((ReplicationMessage) (inst_msg)).getAuthorization());
                ir.processMessage((InstallerCheckpoint)((ReplicationMessage) (inst_msg)).getCheckpoint());
                break;

            case 2: // '\002'
                HttpConnection http = new HttpConnection();
                ((AbstractHttpConnection) (http)).setIsAdmin(true);
                ((AbstractHttpConnection) (http)).setUserIDObject(((ReplicationMessage) (inst_msg)).getAuthorization());
                ((AbstractHttpConnection) (http)).setServerParam(((ReplicationMessage) (inst_msg)).getServer());
                ((AbstractHttpConnection) (http)).setFilename(FileUtils.makeInternalPath(inst_msg.getFilename()));
                ((AbstractHttpConnection) (http)).setCommand("SERVERNEWINSTALLER");
                sendNotification(http, ((ReplicationMessage) (inst_msg)));
                break;

            default:
                String msg = "Unsupported replication type requested for Installer: " + ((ReplicationMessage) (inst_msg)).getType();
                throw new ServletProcessException(msg, "ServerInternalError", "(none)", "(none)", true);
            }
        } else
        if(message instanceof LogReplicationMessage)
        {
            LogReplicationMessage log_msg = (LogReplicationMessage)message;
            switch(((ReplicationMessage) (log_msg)).getType())
            {
            case 1: // '\001'
                LogReceiver lr = new LogReceiver(server, server.getVault());
                ((AbstractReceiver) (lr)).setAdminIDObject(((ReplicationMessage) (log_msg)).getAuthorization());
                lr.processMessage((LogCheckpoint)((ReplicationMessage) (log_msg)).getCheckpoint());
                break;

            case 2: // '\002'
                HttpConnection http = new HttpConnection();
                ((AbstractHttpConnection) (http)).setIsAdmin(true);
                ((AbstractHttpConnection) (http)).setUserIDObject(((ReplicationMessage) (log_msg)).getAuthorization());
                ((AbstractHttpConnection) (http)).setServerParam(((ReplicationMessage) (log_msg)).getServer());
                ((AbstractHttpConnection) (http)).setName(log_msg.getLogObject().getName());
                ((AbstractHttpConnection) (http)).setFromVersion(log_msg.getLogObject().getFrom());
                ((AbstractHttpConnection) (http)).setToVersion(log_msg.getLogObject().getTo());
                ((AbstractHttpConnection) (http)).setCommand("SERVERNEWLOG");
                sendNotification(http, ((ReplicationMessage) (log_msg)));
                break;

            default:
                String msg = "Unsupported replication type requested for LOG: " + ((ReplicationMessage) (log_msg)).getType();
                throw new ServletProcessException(msg, "ServerInternalError", "(none)", "(none)", true);
            }
        } else
        if(message instanceof ConfigReplicationMessage)
        {
            ConfigReplicationMessage cfg_msg = (ConfigReplicationMessage)message;
            switch(((ReplicationMessage) (cfg_msg)).getType())
            {
            case 1: // '\001'
                ConfigReceiver cr = new ConfigReceiver(server, server.getVault());
                ((AbstractReceiver) (cr)).setAdminIDObject(((ReplicationMessage) (cfg_msg)).getAuthorization());
                cr.processMessage((ConfigCheckpoint)((ReplicationMessage) (cfg_msg)).getCheckpoint());
                break;

            case 2: // '\002'
                HttpConnection http = new HttpConnection();
                ((AbstractHttpConnection) (http)).setIsAdmin(true);
                ((AbstractHttpConnection) (http)).setUserIDObject(((ReplicationMessage) (cfg_msg)).getAuthorization());
                ((AbstractHttpConnection) (http)).setServerParam(((ReplicationMessage) (cfg_msg)).getServer());
                ((AbstractHttpConnection) (http)).setCommand("SERVERNEWCONFIG");
                sendNotification(http, ((ReplicationMessage) (cfg_msg)));
                break;

            default:
                String msg = "Unsupported replication type requested for CONFIG: " + ((ReplicationMessage) (cfg_msg)).getType();
                throw new ServletProcessException(msg, "ServerInternalError", "(none)", "(none)", true);
            }
        } else
        if(message instanceof VersionReplicationMessage)
        {
            VersionReplicationMessage ver_msg = (VersionReplicationMessage)message;
            switch(((ReplicationMessage) (ver_msg)).getType())
            {
            case 1: // '\001'
                VersionReceiver vr = new VersionReceiver(server, server.getVault());
                ((AbstractReceiver) (vr)).setAdminIDObject(((ReplicationMessage) (ver_msg)).getAuthorization());
                vr.processMessage((VersionCheckpoint)((ReplicationMessage) (ver_msg)).getCheckpoint());
                break;

            case 2: // '\002'
            case 3: // '\003'
                HttpConnection http = new HttpConnection();
                ((AbstractHttpConnection) (http)).setIsAdmin(true);
                ((AbstractHttpConnection) (http)).setApplication(ver_msg.getApplication());
                ((AbstractHttpConnection) (http)).setVersion(ver_msg.getVersion());
                ((AbstractHttpConnection) (http)).setUserIDObject(((ReplicationMessage) (ver_msg)).getAuthorization());
                ((AbstractHttpConnection) (http)).setServerParam(((ReplicationMessage) (ver_msg)).getServer());
                if(((ReplicationMessage) (ver_msg)).getType() == 2)
                    ((AbstractHttpConnection) (http)).setCommand("SERVERNEWVERSION");
                else
                    ((AbstractHttpConnection) (http)).setCommand("DELETEVERSION");
                sendNotification(http, ((ReplicationMessage) (ver_msg)));
                break;

            default:
                String msg = "Unsupported replication type requested for VERSION: " + ((ReplicationMessage) (ver_msg)).getType();
                throw new ServletProcessException(msg, "ServerInternalError", "(none)", "(none)", true);
            }
        } else
        if(message instanceof BundleReplicationMessage)
        {
            BundleReplicationMessage bnd_msg = (BundleReplicationMessage)message;
            switch(((ReplicationMessage) (bnd_msg)).getType())
            {
            case 1: // '\001'
                AppReceiver ar = new AppReceiver(server, server.getVault());
                ((AbstractReceiver) (ar)).setAdminIDObject(((ReplicationMessage) (bnd_msg)).getAuthorization());
                ar.processMessage((ApplicationCheckpoint)((ReplicationMessage) (bnd_msg)).getCheckpoint());
                break;

            case 2: // '\002'
            case 3: // '\003'
                HttpConnection conn = new HttpConnection();
                ((AbstractHttpConnection) (conn)).setIsAdmin(true);
                ((AbstractHttpConnection) (conn)).setApplication(bnd_msg.getApplication());
                ((AbstractHttpConnection) (conn)).setUserIDObject(((ReplicationMessage) (bnd_msg)).getAuthorization());
                ((AbstractHttpConnection) (conn)).setServerParam(((ReplicationMessage) (bnd_msg)).getServer());
                if(((ReplicationMessage) (bnd_msg)).getType() == 2)
                    ((AbstractHttpConnection) (conn)).setCommand("SERVERNEWBUNDLE");
                else
                    ((AbstractHttpConnection) (conn)).setCommand("DELETEBUNDLE");
                sendNotification(conn, ((ReplicationMessage) (bnd_msg)));
                break;

            case 4: // '\004'
                HttpConnection http = new HttpConnection();
                ((AbstractHttpConnection) (http)).setIsAdmin(true);
                ((AbstractHttpConnection) (http)).setApplication(bnd_msg.getApplication());
                ((AbstractHttpConnection) (http)).setUserIDObject(((ReplicationMessage) (bnd_msg)).getAuthorization());
                ((AbstractHttpConnection) (http)).setServerParam(((ReplicationMessage) (bnd_msg)).getServer());
                ((AbstractHttpConnection) (http)).setCommand("VERSIONREORDER");
                Vector v = server.getVault().getApplication(bnd_msg.getApplication()).listVersions();
                StringBuffer new_versions = new StringBuffer();
                int size = v != null ? v.size() : 0;
                for(int i = 0; i < size; i++)
                {
                    Version ver = (Version)v.elementAt(i);
                    new_versions.append(ver.getName() + "\n");
                }

                sendNotification(http, ((ReplicationMessage) (bnd_msg)), new_versions.toString());
                break;

            default:
                String msg = "Unsupported replication type requested for APPLICATION: " + ((ReplicationMessage) (bnd_msg)).getType();
                throw new ServletProcessException(msg, "ServerInternalError", "(none)", "(none)", true);
            }
        } else
        {
            String msg = "Unknown replication message type: " + (message != null ? message.getClass().getName() : "(null) message");
            throw new ServletProcessException(msg, "ServerInternalError", "(none)", "(none)", true);
        }
    }

    public void receiveFileReplication(FileCheckpoint checkpoint, Object auth)
    {
        FileUploadObject fileObject = checkpoint.getFileObject();
        FileReplicationMessage file_msg = new FileReplicationMessage(fileObject.getServer(), 1, fileObject, checkpoint, auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (file_msg)));
    }

    public void receiveInstallerReplication(InstallerCheckpoint checkpoint, Object auth)
    {
        InstallerObject installerObject = checkpoint.getInstallerObject();
        InstallerReplicationMessage inst_msg = new InstallerReplicationMessage(installerObject.getServer(), 1, installerObject, checkpoint, auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (inst_msg)));
    }

    public void receiveLogReplication(LogCheckpoint checkpoint, Object auth)
    {
        LogObject logObject = checkpoint.getLogObject();
        LogReplicationMessage log_msg = new LogReplicationMessage(logObject.getServer(), 1, logObject, checkpoint, auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (log_msg)));
    }

    public void receiveConfigReplication(ConfigCheckpoint checkpoint, Object auth)
    {
        ConfigObject configObject = checkpoint.getConfigObject();
        ConfigReplicationMessage cfg_msg = new ConfigReplicationMessage(configObject.getServer(), 1, configObject, checkpoint, auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (cfg_msg)));
    }

    public void receiveJREReplication(JRECheckpoint checkpoint, Object auth)
    {
        JREObject jreObject = checkpoint.getJREObject();
        JREReplicationMessage jre_msg = new JREReplicationMessage(jreObject.getServer(), 1, jreObject, checkpoint, auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (jre_msg)));
    }

    public void receiveApplicationReplication(ApplicationCheckpoint checkpoint, Object auth)
    {
        ApplicationObject applicationObject = checkpoint.getApplicationObject();
        BundleReplicationMessage app_msg = new BundleReplicationMessage(applicationObject.getServer(), 1, applicationObject, checkpoint, auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (app_msg)));
    }

    public void receiveVersionReplication(VersionCheckpoint checkpoint, Object auth)
    {
        VersionObject versionObject = checkpoint.getVersionObject();
        VersionReplicationMessage ver_msg = new VersionReplicationMessage(versionObject.getServer(), 1, versionObject, checkpoint, auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (ver_msg)));
    }

    public void sendFileReplication(String remote_server, FileUploadObject fileObject, Object auth)
    {
        FileReplicationMessage file_msg = new FileReplicationMessage(remote_server, 2, fileObject, ((FileCheckpoint) (null)), auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (file_msg)));
    }

    public void sendInstallerReplication(String remote_server, InstallerObject installerObject, Object auth)
    {
        InstallerReplicationMessage inst_msg = new InstallerReplicationMessage(remote_server, 2, installerObject, ((InstallerCheckpoint) (null)), auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (inst_msg)));
    }

    public void sendLogReplication(String remote_server, LogObject logObject, Object auth)
    {
        LogReplicationMessage log_msg = new LogReplicationMessage(remote_server, 2, logObject, ((LogCheckpoint) (null)), auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (log_msg)));
    }

    public void sendConfigReplication(String remote_server, ConfigObject configObject, Object auth)
    {
        ConfigReplicationMessage cfg_msg = new ConfigReplicationMessage(remote_server, 2, configObject, ((ConfigCheckpoint) (null)), auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (cfg_msg)));
    }

    public void sendJREReplication(String remote_server, JREObject jreObject, Object auth)
    {
        JREReplicationMessage jre_msg = new JREReplicationMessage(remote_server, 2, jreObject, ((JRECheckpoint) (null)), auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (jre_msg)));
    }

    public void sendJREDeletion(String remote_server, JREObject jreObject, Object auth)
    {
        JREReplicationMessage jre_msg = new JREReplicationMessage(remote_server, 3, jreObject, ((JRECheckpoint) (null)), auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (jre_msg)));
    }

    public void sendApplicationReplication(String remote_server, ApplicationObject applicationObject, Object auth)
    {
        BundleReplicationMessage bnd_msg = new BundleReplicationMessage(remote_server, 2, applicationObject, ((ApplicationCheckpoint) (null)), auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (bnd_msg)));
    }

    public void sendApplicationDeletion(String remote_server, ApplicationObject applicationObject, Object auth)
    {
        BundleReplicationMessage bnd_msg = new BundleReplicationMessage(remote_server, 3, applicationObject, ((ApplicationCheckpoint) (null)), auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (bnd_msg)));
    }

    public void sendVersionReorder(String remote_server, ApplicationObject applicationObject, Object auth)
    {
        BundleReplicationMessage bnd_msg = new BundleReplicationMessage(remote_server, 4, applicationObject, ((ApplicationCheckpoint) (null)), auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (bnd_msg)));
    }

    public void sendVersionReplication(String remote_server, VersionObject versionObject, Object auth)
    {
        VersionReplicationMessage ver_msg = new VersionReplicationMessage(remote_server, 2, versionObject, ((VersionCheckpoint) (null)), auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (ver_msg)));
    }

    public void sendVersionDeletion(String remote_server, VersionObject versionObject, Object auth)
    {
        VersionReplicationMessage ver_msg = new VersionReplicationMessage(remote_server, 3, versionObject, ((VersionCheckpoint) (null)), auth, server.getVault().listOtherServers());
        addMessage(((ReplicationMessage) (ver_msg)));
    }

    protected void sendNotification(HttpConnection command, ReplicationMessage message)
    {
        sendNotification(command, message, ((String) (null)));
    }

    protected void sendNotification(HttpConnection command, ReplicationMessage message, String data)
    {
        int retryCount = message.getRetryCount();
        String this_server = message.getServer();
        String other_servers = message.getOtherServers();
        JCStringTokenizer toker = new JCStringTokenizer(other_servers);
        String failed_servers = null;
        while(toker.hasMoreTokens()) 
        {
            String next_server = toker.nextToken(',');
            ((AbstractHttpConnection) (command)).setServer(next_server);
            if(data == null)
            {
                ((AbstractHttpConnection) (command)).executeCommand();
            } else
            {
                java.io.OutputStream out = ((AbstractHttpConnection) (command)).executeUploadCommand();
                if(out != null)
                {
                    try
                    {
                        ZipOutputStream zos = new ZipOutputStream(out);
                        FileUtils.addToZipStream(zos, data, "versions.lst");
                        zos.finish();
                        ((FilterOutputStream) (zos)).flush();
                    }
                    catch(IOException ioe) { }
                    command.finishUploadCommand();
                }
            }
            if(!((AbstractHttpConnection) (command)).isSuccessful())
            {
                if(retryCount < RETRY_SEQUENCE.length)
                {
                    String app = ((AbstractHttpConnection) (command)).getApplication();
                    if(app == null)
                        app = "(none)";
                    String version = ((AbstractHttpConnection) (command)).getVersion();
                    if(version == null)
                        version = "(none)";
                    String msg = ((AbstractHttpConnection) (command)).getCommand() + " " + ((AbstractHttpConnection) (command)).getResponseExplanation(((AbstractHttpConnection) (command)).getResponseCode()) + " From: " + server.getServerURI(this_server) + " to " + server.getServerURI(next_server);
                    server.logMessage(server.getServerURI(next_server), "ServerReplicationDelayed", app, version, msg);
                }
                if(failed_servers == null)
                    failed_servers = next_server;
                else
                    failed_servers = failed_servers + "," + next_server;
            }
        }
        if(failed_servers != null)
            if(retryCount < RETRY_SEQUENCE.length)
            {
                retryCount++;
                message.setOtherServers(failed_servers);
                message.setRetryCount(retryCount);
                addMessageToResend(message);
            } else
            {
                String app = ((AbstractHttpConnection) (command)).getApplication();
                if(app == null)
                    app = "(none)";
                String version = ((AbstractHttpConnection) (command)).getVersion();
                if(version == null)
                    version = "(none)";
                String msg = ((AbstractHttpConnection) (command)).getCommand() + " From: " + server.getServerURI(this_server) + " to " + failed_servers;
                server.logMessage(failed_servers, "ServerReplicationRejected", app, version, msg);
            }
    }

    public synchronized void addMessage(ReplicationMessage message, long delay)
    {
        ((Timer)this).addTimerEvent(((TimerEvent) (new ReplicationTimerEvent(message, delay, ((TimerEventListener) (this))))));
        saveQueue();
    }

    public synchronized void addMessage(ReplicationMessage message)
    {
        addMessage(message, 0L);
    }

    public synchronized void addMessageToResend(ReplicationMessage message)
    {
        addMessage(message, oneHour * RETRY_SEQUENCE[message.getRetryCount() - 1]);
    }

}
