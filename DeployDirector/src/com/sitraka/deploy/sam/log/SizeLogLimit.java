// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SizeLogLimit.java

package com.sitraka.deploy.sam.log;


// Referenced classes of package com.sitraka.deploy.sam.log:
//            LogLimit, LogException, Database

public class SizeLogLimit
    implements LogLimit
{

    protected int minSize;
    protected int maxSize;
    protected int timestampIndex;

    public SizeLogLimit(int minSize, int maxSize, int timestampIndex)
    {
        this.minSize = 0;
        this.maxSize = -1;
        this.timestampIndex = 0;
        this.minSize = minSize;
        this.maxSize = maxSize;
        this.timestampIndex = timestampIndex;
    }

    public void compactLog(Database db)
        throws LogException
    {
        if(maxSize < 0)
            return;
        if(db.getLogCount() > maxSize)
            db.compactLog(minSize, maxSize, timestampIndex);
    }

    public void compactDatabase(Database db)
        throws LogException
    {
        if(maxSize < 0)
            return;
        if(db.getRecordCount() > maxSize)
            db.compactDatabase(minSize, maxSize, timestampIndex);
    }
}
