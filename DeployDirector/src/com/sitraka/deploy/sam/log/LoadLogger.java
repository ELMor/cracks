// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LoadLogger.java

package com.sitraka.deploy.sam.log;

import java.sql.Timestamp;

// Referenced classes of package com.sitraka.deploy.sam.log:
//            BaseLogger, Field, LogException, Logger, 
//            Database, LogLimit

public class LoadLogger extends BaseLogger
{

    public static final String TABLE = "DDLoadLog";
    public Field FIELDS[];
    public static final int KEY_COLUMNS[] = {
        0, 1
    };
    public static final int IGNORE_ON_UPDATE_COLUMNS[] = new int[0];
    protected Object values[];

    public LoadLogger(String server_id, Database database, LogLimit logLimit)
        throws LogException
    {
        super(server_id);
        FIELDS = (new Field[] {
            new Field("Timestamp", "TS", java.sql.Timestamp.class, "DATETIME", 23), new Field("Server ID", "ServerID", java.lang.String.class, "VARCHAR(64)", 10), new Field("Uptime", "Uptime", java.lang.Long.class, "INT", 10), new Field("Avg1Min", "Avg1Min", java.lang.Float.class, "FLOAT", 10), new Field("Avg5Min", "Avg5Min", java.lang.Float.class, "FLOAT", 10), new Field("Avg30Min", "Avg30Min", java.lang.Float.class, "FLOAT", 10)
        });
        values = new Object[FIELDS.length];
        ((Logger)this).init(FIELDS, database);
        database.init(FIELDS, "DDLoadLog", KEY_COLUMNS, IGNORE_ON_UPDATE_COLUMNS, logLimit);
    }

    public String getName()
    {
        return "DDLoadLog";
    }

    public static String getFileName()
    {
        return "DDLoadLog";
    }

    public synchronized void log(long uptime, double avg_1_min, double avg_5_min, double avg_30_min)
        throws LogException
    {
        values[0] = ((Object) (new Timestamp(System.currentTimeMillis())));
        values[1] = ((Object) (((Logger)this).getServerID()));
        values[2] = ((Object) (new Long(uptime)));
        values[3] = ((Object) (new Float((float)avg_1_min)));
        values[4] = ((Object) (new Float((float)avg_5_min)));
        values[5] = ((Object) (new Float((float)avg_30_min)));
        ((Logger)this).log(values);
    }

}
