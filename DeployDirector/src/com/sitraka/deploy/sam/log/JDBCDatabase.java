// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JDBCDatabase.java

package com.sitraka.deploy.sam.log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.log:
//            Database, LogException, Field, LogLimit

public class JDBCDatabase extends Database
{

    protected String url;
    protected String login;
    protected String password;
    protected boolean cacheConnections;
    protected String table;
    protected String logStatementString;
    protected String updateStatementString;
    protected PreparedStatement logStatement;
    protected int logStatementCount;
    protected PreparedStatement updateStatement;
    protected int updateStatementCount;
    protected Connection connection;
    protected int connectionCount;
    protected Statement statement;
    protected int statementCount;
    protected Field fields[];
    protected int keyIndices[];
    protected boolean updateIndices[];
    protected LogLimit logLimit;

    public JDBCDatabase(String driver, String url, String login, String password, boolean cache_connections)
    {
        this.url = null;
        this.login = null;
        this.password = null;
        table = null;
        logStatementString = null;
        updateStatementString = null;
        logStatement = null;
        logStatementCount = 0;
        updateStatement = null;
        updateStatementCount = 0;
        connection = null;
        connectionCount = 0;
        statement = null;
        statementCount = 0;
        fields = null;
        keyIndices = null;
        updateIndices = null;
        logLimit = null;
        this.url = url;
        this.login = login;
        this.password = password;
        cacheConnections = cache_connections;
        if(driver != null)
            try
            {
                Class.forName(driver).newInstance();
            }
            catch(ClassNotFoundException cnfe)
            {
                throw new IllegalArgumentException("Cannot locate specified driver class = \"" + driver + "\"");
            }
            catch(InstantiationException ie) { }
            catch(IllegalAccessException iae) { }
    }

    public void init(Field fields[], String table, int key_indices[], int ignore_on_update_indices[], LogLimit logLimit)
        throws LogException
    {
        this.fields = fields;
        keyIndices = key_indices;
        updateIndices = ((Database)this).buildUpdateIndices(fields.length, ignore_on_update_indices);
        this.table = table;
        logStatementString = buildInsertStatement(table, fields);
        updateStatementString = buildUpdateStatement(table, fields, key_indices, ignore_on_update_indices);
        this.logLimit = logLimit;
        getStatement();
        boolean table_exists = true;
        try
        {
            statement.executeQuery("select * from " + table);
        }
        catch(SQLException sqle)
        {
            table_exists = false;
        }
        if(!table_exists)
            try
            {
                statement.executeUpdate(buildCreateTableStatement(table, fields, key_indices));
            }
            catch(SQLException sqle)
            {
                throw new LogException("Could not create table", ((Exception) (sqle)));
            }
        returnStatement();
    }

    public int[] getKeyColumnIndices()
    {
        return keyIndices;
    }

    public boolean[] getUpdateColumnIndices()
    {
        return updateIndices;
    }

    public Field[] getFields()
    {
        return fields;
    }

    public void compactDatabase(int minRecords, int maxRecords, int timestampIndex)
        throws LogException
    {
        throw new LogException("Method not implemented", ((Exception) (null)));
    }

    public void compactDatabase(Timestamp timestamp, int timestampIndex)
        throws LogException
    {
        compactLog(timestamp, timestampIndex);
    }

    public void compactLog(int minRecords, int maxRecords, int timestampIndex)
        throws LogException
    {
        String compact_statement = "delete from " + table + " where " + fields[0].id + " in (select b." + fields[0].id + " from " + table + " a, " + table + " b where a." + fields[timestampIndex].id + " >= b." + fields[timestampIndex].id + " group by b." + fields[timestampIndex].id + ", b." + fields[0].id + " having count(*) > " + minRecords + ")";
        getStatement();
        try
        {
            statement.executeUpdate(compact_statement);
        }
        catch(SQLException sqle)
        {
            returnStatement();
            throw new LogException("Compact Log failed", ((Exception) (sqle)));
        }
        returnStatement();
    }

    public void compactLog(Timestamp timestamp, int timestampIndex)
        throws LogException
    {
        String compact_statement = "delete from " + table + " where " + fields[timestampIndex].id + " < ?";
        getConnection();
        PreparedStatement statement = null;
        try
        {
            statement = connection.prepareStatement(compact_statement);
        }
        catch(SQLException sqle)
        {
            throw new LogException("Cannot prepare statement", ((Exception) (sqle)));
        }
        try
        {
            statement.setTimestamp(1, timestamp);
            statement.executeQuery();
        }
        catch(SQLException sqle)
        {
            returnConnection();
            throw new LogException("Compact Log failed", ((Exception) (sqle)));
        }
        returnConnection();
    }

    public int getLogCount()
        throws LogException
    {
        return getRecordCount();
    }

    public void log(Object values[])
        throws LogException
    {
        getLogStatement();
        try
        {
            for(int i = 0; i < values.length; i++)
                addFieldToStatement(logStatement, i, i + 1, values[i]);

        }
        catch(SQLException sqle)
        {
            throw new LogException("error setting field in prepared statement", ((Exception) (sqle)));
        }
        try
        {
            logStatement.executeUpdate();
        }
        catch(SQLException sqle2)
        {
            throw new LogException("error executing update", ((Exception) (sqle2)));
        }
        returnLogStatement();
        logLimit.compactLog(((Database) (this)));
    }

    public void dispose()
    {
        if(connection != null)
            try
            {
                connection.close();
            }
            catch(SQLException sqle) { }
    }

    public void write(Object values[])
        throws LogException
    {
        boolean record_exists = true;
        getStatement();
        try
        {
            String exists_query = "select * from " + table + " where ";
            for(int i = 0; i < keyIndices.length; i++)
            {
                if(i != 0)
                    exists_query = exists_query + " and ";
                exists_query = exists_query + fields[keyIndices[i]].id + " = '" + values[keyIndices[i]] + "'";
            }

            statement.executeQuery(exists_query);
            ResultSet rs = statement.getResultSet();
            if(!rs.next())
                record_exists = false;
        }
        catch(SQLException sqle)
        {
            throw new LogException("error checking for existing record", ((Exception) (sqle)));
        }
        if(record_exists)
        {
            getUpdateStatement();
            try
            {
                int param = 1;
                for(int i = 0; i < updateIndices.length; i++)
                    if(updateIndices[i] && !inKeyIndices(i))
                        addFieldToStatement(updateStatement, i, param++, values[i]);

                for(int i = 0; i < keyIndices.length; i++)
                    addFieldToStatement(updateStatement, keyIndices[i], param++, values[keyIndices[i]]);

            }
            catch(SQLException sqle)
            {
                throw new LogException("error setting field in prepared statement", ((Exception) (sqle)));
            }
            try
            {
                updateStatement.executeUpdate();
            }
            catch(SQLException sqle2)
            {
                throw new LogException("error executing update", ((Exception) (sqle2)));
            }
            returnUpdateStatement();
        } else
        {
            log(values);
        }
        returnStatement();
    }

    public Object[] getRecord(int comparators[], int compare_fields[], Object compare_values[])
        throws LogException
    {
        Object record[] = null;
        try
        {
            getConnection();
            ResultSet rs = select(comparators, compare_fields, compare_values);
            rs.next();
            record = new Object[fields.length];
            for(int i = 0; i < record.length; i++)
                record[i] = rs.getObject(fields[i].id);

            returnConnection();
        }
        catch(SQLException sqle)
        {
            returnConnection();
            throw new LogException("Error obtaining record", ((Exception) (sqle)));
        }
        return record;
    }

    public Vector getRecords(int comparators[], int compare_fields[], Object compare_values[])
        throws LogException
    {
        getConnection();
        Vector records;
        try
        {
            ResultSet rs = select(comparators, compare_fields, compare_values);
            records = new Vector();
            for(boolean record_exists = rs.next(); record_exists; record_exists = rs.next())
            {
                Object values[] = new Object[fields.length];
                for(int i = 0; i < values.length; i++)
                    values[i] = rs.getObject(fields[i].id);

                records.addElement(((Object) (values)));
            }

        }
        catch(SQLException sqle)
        {
            returnConnection();
            throw new LogException("Error obtaining record", ((Exception) (sqle)));
        }
        returnConnection();
        return records;
    }

    public int getRecordCount(int comparators[], int compare_fields[], Object compare_values[])
        throws LogException
    {
        try
        {
            ResultSet rs = select(comparators, compare_fields, compare_values);
            int record_count = 0;
            for(boolean record_exists = rs.next(); record_exists; record_exists = rs.next())
                record_count++;

            returnConnection();
            return record_count;
        }
        catch(SQLException sqle)
        {
            returnConnection();
            throw new LogException("Cannot obtain record count", ((Exception) (sqle)));
        }
    }

    public int getUniqueRecordCount(int index, Timestamp timestamp, int timestampIndex)
        throws LogException
    {
        String query = "select count(distinct " + fields[index].id + ") from " + table + " where " + fields[timestampIndex].id + " > ?";
        getConnection();
        PreparedStatement statement = null;
        try
        {
            statement = connection.prepareStatement(query);
        }
        catch(SQLException sqle)
        {
            throw new LogException("Cannot prepare statement", ((Exception) (sqle)));
        }
        ResultSet set = null;
        int count = 0;
        try
        {
            statement.setTimestamp(1, timestamp);
            set = statement.executeQuery();
            if(set != null)
            {
                set.next();
                count = set.getInt(1);
            }
        }
        catch(SQLException sqle)
        {
            returnConnection();
            throw new LogException("getUniqueRecordCount failed", ((Exception) (sqle)));
        }
        returnConnection();
        return count;
    }

    public Vector getRecords()
        throws LogException
    {
        return getRecords(((int []) (null)), ((int []) (null)), ((Object []) (null)));
    }

    public int getRecordCount()
        throws LogException
    {
        return getRecordCount(((int []) (null)), ((int []) (null)), ((Object []) (null)));
    }

    protected ResultSet select(int comparators[], int compare_fields[], Object compare_values[])
        throws SQLException
    {
        String query = "select * from " + table;
        if(comparators != null)
        {
            if(comparators.length != compare_fields.length && compare_fields.length != compare_values.length)
                throw new IllegalArgumentException("all arrays must be of equal length");
            query = query + " where ";
            for(int i = 0; i < comparators.length; i++)
            {
                if(i != 0)
                    query = query + " and ";
                query = query + fields[compare_fields[i]].id + " ";
                String comparator = null;
                switch(comparators[i])
                {
                case 1: // '\001'
                    comparator = " < ? ";
                    break;

                case 2: // '\002'
                    comparator = " <= ? ";
                    break;

                case 3: // '\003'
                    comparator = " = ? ";
                    break;

                case 4: // '\004'
                    comparator = " > ? ";
                    break;

                case 5: // '\005'
                    comparator = " >= ? ";
                    break;

                case 6: // '\006'
                    comparator = " != ? ";
                    break;

                default:
                    throw new IllegalArgumentException("invalid comparator value");
                }
                query = query + comparator;
            }

        }
        PreparedStatement statement = null;
        try
        {
            statement = connection.prepareStatement(query);
        }
        catch(SQLException sqle)
        {
            throw sqle;
        }
        ResultSet set = null;
        try
        {
            if(comparators != null)
            {
                for(int i = 0; i < comparators.length; i++)
                    addFieldToStatement(statement, i, i + 1, compare_values[i]);

            }
            set = statement.executeQuery();
        }
        catch(SQLException sqle)
        {
            throw sqle;
        }
        return set;
    }

    protected static String buildCreateTableStatement(String table, Field fields[], int key_columns[])
    {
        String create_statement = "create table " + table + " (";
        for(int i = 0; i < fields.length; i++)
        {
            create_statement = create_statement + fields[i].id + " " + fields[i].jdbcType;
            if(inKeyIndices(key_columns, i))
                create_statement = create_statement + " not null";
            else
                create_statement = create_statement + " null";
            if(i != fields.length - 1)
                create_statement = create_statement + ", ";
        }

        if(key_columns == null);
        create_statement = create_statement + ")";
        return create_statement;
    }

    protected String buildUpdateStatement(String table, Field fields[], int key_indices[], int ignore_on_update_indices[])
    {
        String update_statement = "update " + table;
        update_statement = update_statement + " set ";
        boolean first_arg = true;
        for(int j = 0; j < fields.length; j++)
        {
            boolean skip = false;
            skip = inKeyIndices(j);
            for(int l = 0; l < ignore_on_update_indices.length; l++)
            {
                if(j != ignore_on_update_indices[l])
                    continue;
                skip = true;
                break;
            }

            if(!skip)
            {
                if(first_arg)
                    first_arg = false;
                else
                    update_statement = update_statement + ", ";
                update_statement = update_statement + " " + fields[j].id + " = ?";
            }
        }

        update_statement = update_statement + " where ";
        for(int i = 0; i < key_indices.length; i++)
        {
            if(i != 0)
                update_statement = update_statement + "and ";
            update_statement = update_statement + fields[key_indices[i]].id + " = ? ";
        }

        return update_statement;
    }

    protected static String buildInsertStatement(String table, Field fields[])
    {
        String insert_statement = "insert into " + table + " VALUES (";
        for(int i = 0; i < fields.length; i++)
        {
            insert_statement = insert_statement + " ?";
            if(i != fields.length - 1)
                insert_statement = insert_statement + ",";
        }

        insert_statement = insert_statement + ")";
        return insert_statement;
    }

    protected void getConnection()
        throws LogException
    {
        connectionCount++;
        if(connection == null)
            try
            {
                connection = DriverManager.getConnection(url, login, password);
            }
            catch(SQLException sqle)
            {
                throw new LogException("Cannot establish Connection", ((Exception) (sqle)));
            }
    }

    protected void returnConnection()
        throws LogException
    {
        connectionCount--;
        if(!cacheConnections && connectionCount == 0)
        {
            try
            {
                connection.close();
            }
            catch(SQLException sqle)
            {
                throw new LogException("Cannot close Connection", ((Exception) (sqle)));
            }
            connection = null;
        }
    }

    protected void getStatement()
        throws LogException
    {
        statementCount++;
        if(statement == null)
        {
            getConnection();
            try
            {
                statement = connection.createStatement();
            }
            catch(SQLException sqle)
            {
                throw new LogException("Cannot create statement", ((Exception) (sqle)));
            }
        }
    }

    protected void returnStatement()
        throws LogException
    {
        statementCount--;
        if(!cacheConnections && statementCount == 0)
        {
            statement = null;
            returnConnection();
        }
    }

    protected void getLogStatement()
        throws LogException
    {
        logStatementCount++;
        if(logStatement == null)
        {
            getConnection();
            try
            {
                logStatement = connection.prepareStatement(logStatementString);
            }
            catch(SQLException sqle)
            {
                throw new LogException("Cannot prepare statement", ((Exception) (sqle)));
            }
        }
    }

    protected void returnLogStatement()
        throws LogException
    {
        logStatementCount--;
        if(!cacheConnections && logStatementCount == 0)
        {
            logStatement = null;
            returnConnection();
        }
    }

    protected void getUpdateStatement()
        throws LogException
    {
        updateStatementCount++;
        if(updateStatement == null)
        {
            getConnection();
            try
            {
                updateStatement = connection.prepareStatement(updateStatementString);
            }
            catch(SQLException sqle)
            {
                throw new LogException("Cannot prepare statement", ((Exception) (sqle)));
            }
        }
    }

    protected void returnUpdateStatement()
        throws LogException
    {
        updateStatementCount--;
        if(!cacheConnections && updateStatementCount == 0)
        {
            updateStatement = null;
            returnConnection();
        }
    }

    protected boolean inKeyIndices(int field_index)
    {
        return inKeyIndices(keyIndices, field_index);
    }

    protected static boolean inKeyIndices(int key_indices[], int field_index)
    {
        for(int k = 0; k < key_indices.length; k++)
            if(field_index == key_indices[k])
                return true;

        return false;
    }

    protected void addFieldToStatement(PreparedStatement statement, int field, int param, Object field_value)
        throws SQLException
    {
        if(fields[field].javaType == (java.lang.String.class))
            statement.setString(param, (String)field_value);
        else
        if(fields[field].javaType == (java.sql.Timestamp.class))
            statement.setTimestamp(param, (Timestamp)field_value);
        else
        if(fields[field].javaType == (java.lang.Float.class))
        {
            if(field_value == null)
                statement.setFloat(param, 0.0F);
            else
                statement.setFloat(param, ((Number)field_value).floatValue());
        } else
        if(fields[field].javaType == (java.lang.Long.class))
        {
            if(field_value == null)
                statement.setLong(param, 0L);
            else
                statement.setLong(param, ((Number)field_value).longValue());
        } else
        {
            throw new IllegalArgumentException(field_value + " is not of a supported data type");
        }
    }

    protected boolean saveDatabaseToFile(File file)
        throws LogException
    {
        if(file == null)
            return false;
        Vector allRecords = getRecords();
        PrintWriter writer;
        try
        {
            writer = new PrintWriter(((java.io.Writer) (new BufferedWriter(((java.io.Writer) (new FileWriter(file.getAbsolutePath())))))), true);
        }
        catch(IOException ioe)
        {
            throw new LogException("cannot open file", ((Exception) (ioe)));
        }
        Object record[];
        for(Enumeration e = allRecords.elements(); e.hasMoreElements(); ((Database)this).writePortableEntry(writer, record))
            record = (Object[])e.nextElement();

        return true;
    }
}
