// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LogLimit.java

package com.sitraka.deploy.sam.log;


// Referenced classes of package com.sitraka.deploy.sam.log:
//            LogException, Database

public interface LogLimit
{

    public abstract void compactLog(Database database)
        throws LogException;

    public abstract void compactDatabase(Database database)
        throws LogException;
}
