// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DateLogLimit.java

package com.sitraka.deploy.sam.log;

import java.sql.Timestamp;

// Referenced classes of package com.sitraka.deploy.sam.log:
//            LogLimit, LogException, Database

public class DateLogLimit
    implements LogLimit
{

    protected long interval;
    protected int timestampIndex;

    public DateLogLimit(long interval, int timestampIndex)
    {
        this.interval = 0L;
        this.timestampIndex = 0;
        this.interval = interval;
        this.timestampIndex = timestampIndex;
    }

    public void compactLog(Database db)
        throws LogException
    {
        if(interval < 0L)
        {
            return;
        } else
        {
            Timestamp oldestRecordTimestamp = new Timestamp(System.currentTimeMillis() - interval);
            db.compactLog(oldestRecordTimestamp, timestampIndex);
            return;
        }
    }

    public void compactDatabase(Database db)
        throws LogException
    {
        if(interval < 0L)
        {
            return;
        } else
        {
            Timestamp oldestRecordTimestamp = new Timestamp(System.currentTimeMillis() - interval);
            db.compactDatabase(oldestRecordTimestamp, timestampIndex);
            return;
        }
    }
}
