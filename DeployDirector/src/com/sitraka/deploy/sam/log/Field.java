// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Field.java

package com.sitraka.deploy.sam.log;


public class Field
{

    public String name;
    public String id;
    public Class javaType;
    public String jdbcType;
    public int displayWidth;

    public Field(String name, String id, Class java_type, String jdbc_type, int display_width)
    {
        this.name = name;
        this.id = id;
        javaType = java_type;
        jdbcType = jdbc_type;
        displayWidth = display_width;
    }
}
