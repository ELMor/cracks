// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerLogger.java

package com.sitraka.deploy.sam.log;

import java.sql.Timestamp;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.log:
//            BaseLogger, Field, LogStats, LogException, 
//            Logger, Database, LogLimit

public class ServerLogger extends BaseLogger
{

    public static final String TABLE = "DDServerLog";
    public static final int TIMESTAMP_ID = 0;
    public static final int SERVER_ID = 1;
    public static final int EVENT = 2;
    public static final int REMOTE_ID = 3;
    public static final int NOTES = 4;
    public Field FIELDS[];
    public static final int KEY_COLUMNS[] = {
        0, 1
    };
    public static final int IGNORE_ON_UPDATE_COLUMNS[] = new int[0];
    public static final String LOG_STATEMENT = "insert into DDServerLog VALUES (?, ?, ?, ?, ?)";
    protected Object values[];

    public ServerLogger(String server_id, Database database, LogLimit logLimit)
        throws LogException
    {
        super(server_id);
        FIELDS = (new Field[] {
            new Field("Timestamp", "TS", java.sql.Timestamp.class, "DATETIME", 23), new Field("Server ID", "ServerID", java.lang.String.class, "VARCHAR(64)", 23), new Field("Event", "Event", java.lang.String.class, "VARCHAR(64)", 20), new Field("Remote ID", "RemoteID", java.lang.String.class, "VARCHAR(64)", 10), new Field("Notes", "Notes", java.lang.String.class, "VARCHAR(255)", 30)
        });
        values = new Object[FIELDS.length];
        ((Logger)this).init(FIELDS, database);
        database.init(FIELDS, "DDServerLog", KEY_COLUMNS, IGNORE_ON_UPDATE_COLUMNS, logLimit);
    }

    public String getName()
    {
        return "DDServerLog";
    }

    public static String getFileName()
    {
        return "DDServerLog";
    }

    public void log(String event, String remote_id, String notes)
        throws LogException
    {
        log(new Timestamp(System.currentTimeMillis()), event, remote_id, notes);
    }

    public synchronized void log(Timestamp ts, String event, String remote_id, String notes)
        throws LogException
    {
        values[0] = ((Object) (ts));
        values[1] = ((Object) (((Logger)this).getServerID()));
        values[2] = ((Object) (event));
        values[3] = ((Object) (remote_id));
        values[4] = ((Object) (notes));
        ((Logger)this).log(values);
    }

    public LogStats queryLogStats(Timestamp startTime, Timestamp endTime)
        throws LogException
    {
        Vector records = null;
        Vector eventNames = new Vector();
        Vector eventCount = new Vector();
        records = ((Logger)this).database.getRecords(new int[] {
            1, 4
        }, new int[] {
            0, 0
        }, new Object[] {
            startTime, endTime
        });
        if(records != null)
        {
            for(int i = 0; i < records.size(); i++)
            {
                Object record[] = (Object[])records.elementAt(i);
                String event = (String)record[2];
                int index = eventNames.indexOf(((Object) (event)));
                if(index == -1)
                {
                    eventNames.addElement(((Object) (event)));
                    eventCount.addElement(((Object) (new Integer(1))));
                } else
                {
                    Integer num = (Integer)eventCount.elementAt(index);
                    num = new Integer(num.intValue() + 1);
                    eventCount.setElementAt(((Object) (num)), index);
                }
            }

        }
        return new LogStats(eventNames, eventCount);
    }

}
