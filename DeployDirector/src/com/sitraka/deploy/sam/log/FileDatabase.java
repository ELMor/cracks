// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FileDatabase.java

package com.sitraka.deploy.sam.log;

import com.sitraka.deploy.util.CollectionsSort;
import com.sitraka.deploy.util.FileUtils;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.log:
//            Database, LogException, Logger, LogLimit, 
//            Field

public class FileDatabase extends Database
{
    class TimestampIndexComparator
        implements Comparator
    {

        protected int index;

        public int compareTo(Timestamp thisDate, Timestamp anotherDate)
        {
            long thisTime = ((Date) (thisDate)).getTime();
            long anotherTime = ((Date) (anotherDate)).getTime();
            return thisTime >= anotherTime ? ((int) (thisTime != anotherTime ? 1 : 0)) : -1;
        }

        public int compare(Object o1, Object o2)
        {
            Object a1[] = (Object[])o1;
            Object a2[] = (Object[])o2;
            if(a1[index] == null && a2[index] == null)
                return 0;
            if(a1[index] == null)
                return -1;
            if(a2[index] == null)
                return 1;
            else
                return compareTo((Timestamp)a1[index], (Timestamp)a2[index]);
        }

        public boolean equals(Object o)
        {
            return equals(o);
        }

        public TimestampIndexComparator(int index)
        {
            this.index = 0;
            this.index = index;
        }
    }

    class StringIndexComparator
        implements Comparator
    {

        protected int index;

        public int compare(Object o1, Object o2)
        {
            Object a1[] = (Object[])o1;
            Object a2[] = (Object[])o2;
            if(a1[index] == null && a2[index] == null)
                return 0;
            if(a1[index] == null)
                return -1;
            if(a2[index] == null)
                return 1;
            else
                return ((String)a1[index]).compareTo((String)a2[index]);
        }

        public boolean equals(Object o)
        {
            return equals(o);
        }

        public StringIndexComparator(int index)
        {
            this.index = 0;
            this.index = index;
        }
    }


    protected OutputStream outputStream;
    protected PrintWriter printWriter;
    protected Object STREAM_LOCK;
    protected File directory;
    protected File file;
    protected boolean cacheStream;
    protected Field fields[];
    protected int keyIndices[];
    protected boolean updateIndices[];
    protected String table;
    protected int logCount;
    protected LogLimit logLimit;
    protected Vector database;

    public FileDatabase(File directory, boolean cache_stream)
    {
        outputStream = null;
        printWriter = null;
        STREAM_LOCK = new Object();
        this.directory = null;
        file = null;
        fields = null;
        keyIndices = null;
        updateIndices = null;
        table = null;
        logCount = 0;
        logLimit = null;
        database = null;
        this.directory = directory;
        cacheStream = cache_stream;
    }

    public void init(Field fields[], String table, int key_indices[], int ignore_on_update_indices[], LogLimit logLimit)
        throws LogException
    {
        this.fields = fields;
        keyIndices = key_indices;
        updateIndices = ((Database)this).buildUpdateIndices(fields.length, ignore_on_update_indices);
        this.table = table;
        this.logLimit = logLimit;
        file = new File(directory, table);
        try
        {
            if(!file.canRead())
                FileUtils.createNewFile(file);
        }
        catch(IOException e) { }
        countLogRecords();
    }

    public void readLog(Vector v)
        throws LogException
    {
        try
        {
            BufferedReader reader = new BufferedReader(((java.io.Reader) (new FileReader(file.getAbsolutePath()))));
            for(String line = null; (line = reader.readLine()) != null;)
                v.addElement(((Object) (line)));

        }
        catch(IOException ioe)
        {
            throw new LogException("read error", ((Exception) (ioe)));
        }
    }

    public void writeLog(Vector v, int startIndex)
        throws LogException
    {
        try
        {
            BufferedWriter writer = new BufferedWriter(((Writer) (new FileWriter(file.getAbsolutePath()))));
            for(int i = startIndex; i < v.size(); i++)
            {
                ((Writer) (writer)).write((String)v.elementAt(i));
                writer.newLine();
            }

            writer.close();
        }
        catch(IOException ioe)
        {
            throw new LogException("write error", ((Exception) (ioe)));
        }
    }

    public void compactLog(int minRecords, int maxRecords, int timestampIndex)
        throws LogException
    {
        Vector logRecords = new Vector();
        readLog(logRecords);
        if(logRecords.size() > maxRecords)
        {
            int startIndex = logRecords.size() - minRecords;
            startIndex = Math.max(0, startIndex);
            writeLog(logRecords, startIndex);
        }
    }

    public void compactLog(Timestamp timestamp, int timestampIndex)
        throws LogException
    {
        Vector logRecords = new Vector();
        readLog(logRecords);
        Vector newRecords = new Vector();
        for(int i = 0; i < logRecords.size(); i++)
        {
            Object values[] = Logger.textToRecord(fields, (String)logRecords.elementAt(i));
            if(((Timestamp)values[timestampIndex]).after(timestamp))
                newRecords.addElement(logRecords.elementAt(i));
        }

        if(logRecords.size() > newRecords.size())
            writeLog(newRecords, 0);
    }

    public void compactDatabase(Timestamp timestamp, int timestampIndex)
        throws LogException
    {
        ensureDatabaseIsLoaded();
        for(int i = 0; i < database.size(); i++)
        {
            Object values[] = (Object[])database.elementAt(i);
            if(((Timestamp)values[timestampIndex]).before(timestamp))
                database.removeElementAt(i);
        }

    }

    public void compactDatabase(int minRecords, int maxRecords, int timestampIndex)
        throws LogException
    {
        ensureDatabaseIsLoaded();
        if(database.size() > maxRecords)
        {
            CollectionsSort.sortVector(database, ((Comparator) (new TimestampIndexComparator(timestampIndex))));
            int extraRecords = database.size() - minRecords;
            for(int i = 0; i < extraRecords; i++)
                database.removeElementAt(0);

        }
    }

    public int getLogCount()
        throws LogException
    {
        return logCount;
    }

    public int getUniqueRecordCount(int index, Timestamp timestamp, int timestampIndex)
        throws LogException
    {
        ensureDatabaseIsLoaded();
        Vector v = search(new int[] {
            1
        }, new int[] {
            timestampIndex
        }, new Object[] {
            timestamp
        });
        if(v == null)
            return 0;
        CollectionsSort.sortVector(v, ((Comparator) (new StringIndexComparator(index))));
        int uniqueCount = 0;
        String current = "";
        for(int i = 0; i < v.size(); i++)
        {
            Object values[] = (Object[])v.elementAt(i);
            String next = (String)values[index];
            if(next == null)
                current = null;
            else
            if(!next.equals(((Object) (current))))
            {
                uniqueCount++;
                current = next;
            }
        }

        return uniqueCount;
    }

    public int countLogRecords()
    {
        if(database == null)
            try
            {
                BufferedReader reader = new BufferedReader(((java.io.Reader) (new FileReader(file.getAbsolutePath()))));
                String line = null;
                for(logCount = 0; (line = reader.readLine()) != null; logCount++);
            }
            catch(IOException ioe) { }
        return logCount;
    }

    public int[] getKeyColumnIndices()
    {
        return keyIndices;
    }

    public boolean[] getUpdateColumnIndices()
    {
        return updateIndices;
    }

    public Field[] getFields()
    {
        return fields;
    }

    public void log(Object values[])
        throws LogException
    {
        synchronized(STREAM_LOCK)
        {
            PrintWriter out = getWriter(true);
            ((Database)this).writePortableEntry(out, values);
            returnWriter();
            logCount++;
            if(database != null)
                write(values, true);
            else
                logLimit.compactLog(((Database) (this)));
        }
    }

    public void dispose()
    {
        try
        {
            if(outputStream != null)
            {
                outputStream.close();
                outputStream = null;
            }
        }
        catch(IOException ioe) { }
    }

    public void write(Object values[])
        throws LogException
    {
        write(values, false);
    }

    public void write(Object values[], boolean forceAdd)
        throws LogException
    {
        ensureDatabaseIsLoaded();
        Object record[] = ((Database)this).getRecord(values);
        if(forceAdd || record == null)
        {
            Object copyOfValues[] = new Object[values.length];
            for(int i = 0; i < values.length; i++)
                copyOfValues[i] = values[i];

            database.addElement(((Object) (copyOfValues)));
        } else
        {
            for(int j = 0; j < values.length; j++)
                if(updateIndices[j])
                    record[j] = values[j];

        }
        logLimit.compactDatabase(((Database) (this)));
        saveDatabaseToFile();
    }

    public Object[] getRecord(int comparators[], int compare_fields[], Object compare_values[])
        throws LogException
    {
        ensureDatabaseIsLoaded();
        Vector results = search(comparators, compare_fields, compare_values);
        if(results == null)
            return null;
        else
            return (Object[])results.elementAt(0);
    }

    public Vector getRecords(int comparators[], int compare_fields[], Object compare_values[])
        throws LogException
    {
        ensureDatabaseIsLoaded();
        return search(comparators, compare_fields, compare_values);
    }

    public int getRecordCount(int comparators[], int compare_fields[], Object compare_values[])
        throws LogException
    {
        ensureDatabaseIsLoaded();
        Vector results = search(comparators, compare_fields, compare_values);
        if(results == null)
            return 0;
        else
            return results.size();
    }

    public Vector getRecords()
        throws LogException
    {
        ensureDatabaseIsLoaded();
        return database;
    }

    public int getRecordCount()
        throws LogException
    {
        ensureDatabaseIsLoaded();
        return database.size();
    }

    protected void ensureDatabaseIsLoaded()
        throws LogException
    {
        if(database != null)
            return;
        database = new Vector();
        try
        {
            BufferedReader reader = new BufferedReader(((java.io.Reader) (new FileReader(file.getAbsolutePath()))));
            for(String line = null; (line = reader.readLine()) != null;)
                if(line.trim().length() != 0)
                {
                    Object values[] = Logger.textToRecord(fields, line);
                    database.addElement(((Object) (values)));
                }

        }
        catch(IOException ioe)
        {
            throw new LogException("read error", ((Exception) (ioe)));
        }
    }

    protected void saveDatabaseToFile()
        throws LogException
    {
        synchronized(STREAM_LOCK)
        {
            ensureDatabaseIsLoaded();
            cacheStream = false;
            PrintWriter out = getWriter(false);
            Object record[];
            for(Enumeration e = database.elements(); e.hasMoreElements(); ((Database)this).writePortableEntry(out, record))
                record = (Object[])e.nextElement();

            returnWriter();
        }
    }

    protected Vector search(int comparators[], int compare_fields[], Object compare_values[])
    {
        Vector results = null;
        for(Enumeration e = database.elements(); e.hasMoreElements();)
        {
            Object record[] = (Object[])e.nextElement();
            boolean match = true;
            for(int j = 0; j < comparators.length; j++)
            {
                if(compare(comparators[j], compare_values[j], record[compare_fields[j]]))
                    continue;
                match = false;
                break;
            }

            if(match)
            {
                if(results == null)
                    results = new Vector();
                results.addElement(((Object) (record)));
            }
        }

        return results;
    }

    protected boolean compare(int comparator, Object compare_value, Object database_value)
    {
        if(compare_value == null && database_value == null)
            return true;
        if(compare_value == null || database_value == null)
            return false;
        if(compare_value instanceof Timestamp)
        {
            long compare_time = ((Date) ((Timestamp)compare_value)).getTime();
            long database_time = ((Date) ((Timestamp)database_value)).getTime();
            long diff = compare_time - database_time;
            switch(comparator)
            {
            case 1: // '\001'
                return diff < 0L;

            case 2: // '\002'
                return diff <= 0L;

            case 3: // '\003'
                return diff == 0L;

            case 4: // '\004'
                return diff > 0L;

            case 5: // '\005'
                return diff >= 0L;

            case 6: // '\006'
                return diff != 0L;
            }
            return false;
        }
        if(comparator == 3)
        {
            if(!compare_value.equals(database_value))
                return false;
            if(compare_value == null)
                return false;
            return compare_value.equals(database_value);
        } else
        {
            throw new IllegalArgumentException("Comparision not supported");
        }
    }

    protected PrintWriter getWriter(boolean append)
        throws LogException
    {
        if(cacheStream && outputStream != null)
            return printWriter;
        try
        {
            outputStream = ((OutputStream) (new BufferedOutputStream(((OutputStream) (new FileOutputStream(file.getAbsolutePath(), append))))));
        }
        catch(IOException ioe)
        {
            throw new LogException("cannot open file", ((Exception) (ioe)));
        }
        printWriter = new PrintWriter(outputStream, true);
        return printWriter;
    }

    protected void returnWriter()
        throws LogException
    {
        if(!cacheStream)
            try
            {
                outputStream.close();
                outputStream = null;
            }
            catch(IOException ioe)
            {
                throw new LogException("cannot close file", ((Exception) (ioe)));
            }
    }
}
