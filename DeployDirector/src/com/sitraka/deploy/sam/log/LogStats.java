// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LogStats.java

package com.sitraka.deploy.sam.log;

import java.util.Vector;

public class LogStats
{

    protected Vector events;
    protected Vector count;

    public LogStats()
    {
        events = null;
        count = null;
        events = new Vector();
        count = new Vector();
    }

    public LogStats(Vector events, Vector count)
    {
        this.events = null;
        this.count = null;
        this.events = events;
        this.count = count;
    }

    public void setEvents(Vector events)
    {
        this.events = events;
    }

    public Vector getEvents()
    {
        return events;
    }

    public void setCount(Vector count)
    {
        this.count = count;
    }

    public Vector getCount()
    {
        return count;
    }
}
