// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServletNotFoundException.java

package com.sitraka.deploy.sam.servlet;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

public class ServletNotFoundException extends ServletException
{

    protected boolean logMessage;
    protected Object request;
    protected Object response;
    protected String category;
    protected String application;
    protected String version;
    protected boolean showErrorPage;

    public ServletNotFoundException(String message)
    {
        super(message);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        showErrorPage = true;
    }

    public ServletNotFoundException(String message, Object req, Object resp)
    {
        super(message);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        showErrorPage = true;
        request = req;
        response = resp;
    }

    public ServletNotFoundException(String message, boolean log)
    {
        super(message);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        showErrorPage = true;
        logMessage = log;
    }

    public ServletNotFoundException(String message, Object req, Object resp, boolean log)
    {
        super(message);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        showErrorPage = true;
        request = req;
        response = resp;
        logMessage = log;
    }

    public ServletNotFoundException(String message, Throwable root)
    {
        super(message, root);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        showErrorPage = true;
    }

    public ServletNotFoundException(String message, Throwable root, Object req, Object resp)
    {
        super(message, root);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        showErrorPage = true;
        request = req;
        response = resp;
    }

    public ServletNotFoundException(String message, Throwable root, boolean log)
    {
        super(message, root);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        showErrorPage = true;
        logMessage = log;
    }

    public ServletNotFoundException(String message, Throwable root, Object req, Object resp, boolean log)
    {
        super(message, root);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        showErrorPage = true;
        request = req;
        response = resp;
        logMessage = log;
    }

    public ServletNotFoundException(String message, Throwable root, String cat, Object req, Object resp, boolean log)
    {
        super(message, root);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        showErrorPage = true;
        category = cat;
        if(cat == null)
            category = "ServerExceptionError";
        request = req;
        response = resp;
        logMessage = log;
        application = ((ServletRequest) ((HttpServletRequest)req)).getParameter("APPLICATION");
        version = ((ServletRequest) ((HttpServletRequest)req)).getParameter("VERSION");
    }

    public ServletNotFoundException(String message, String cat, Object req, Object resp, boolean log)
    {
        super(message);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        showErrorPage = true;
        category = cat;
        request = req;
        response = resp;
        logMessage = log;
        application = ((ServletRequest) ((HttpServletRequest)req)).getParameter("APPLICATION");
        version = ((ServletRequest) ((HttpServletRequest)req)).getParameter("VERSION");
    }

    public boolean mustLog()
    {
        return logMessage;
    }

    public String getApplication()
    {
        return application;
    }

    public void setApplication(String app)
    {
        application = app;
    }

    public String getCategory()
    {
        return category;
    }

    public Object getRequest()
    {
        return request;
    }

    public Object getResponse()
    {
        return response;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String ver)
    {
        version = ver;
    }

    public boolean isShowErrorPage()
    {
        return showErrorPage;
    }

    public void setShowErrorPage(boolean show)
    {
        showErrorPage = show;
    }
}
