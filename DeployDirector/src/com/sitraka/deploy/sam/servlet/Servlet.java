// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Servlet.java

package com.sitraka.deploy.sam.servlet;

import HTTPClient.ParseException;
import HTTPClient.URI;
import com.sitraka.deploy.Authentication;
import com.sitraka.deploy.Authorization;
import com.sitraka.deploy.common.Requests;
import com.sitraka.deploy.common.TemplateText;
import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.common.checkpoint.BaseCheckpoint;
import com.sitraka.deploy.common.connection.HttpConnection;
import com.sitraka.deploy.common.roles.RoleAuthorization;
import com.sitraka.deploy.common.timer.Timer;
import com.sitraka.deploy.sam.AbstractMessageQueue;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.Configuration;
import com.sitraka.deploy.sam.LoadAverage;
import com.sitraka.deploy.sam.LogQueue;
import com.sitraka.deploy.sam.Redir;
import com.sitraka.deploy.sam.RequestParser;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.cache.CacheList;
import com.sitraka.deploy.sam.log.ClientDataLogger;
import com.sitraka.deploy.sam.log.LogException;
import com.sitraka.deploy.sam.request.RequestList;
import com.sitraka.deploy.sam.worker.AbstractWorker;
import com.sitraka.deploy.sam.worker.ReplicationWorker;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.Hashcode;
import com.sitraka.deploy.util.PropertyUtils;
import com.sitraka.licensing.LicenseProperties;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;
import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpUtils;

// Referenced classes of package com.sitraka.deploy.sam.servlet:
//            ServletProcessException, ServletNotFoundException, LimitedInputStream, AuthInfo

public class Servlet extends HttpServlet
    implements Requests, ServerActions
{

    protected String vaultType;
    protected AppVault vault;
    protected static boolean vaultAvailable = true;
    protected static String vaultUnavailableText = "";
    protected static Object VAULT_LOCK = new Object();
    protected String servletName;
    public static Properties properties = null;
    protected static Timer manager = null;
    protected static LogQueue logger = null;
    protected ServletConfig init_config;
    protected LoadAverage load_average;
    protected static final long startup_time = System.currentTimeMillis();
    public static boolean licensed = false;
    protected int maxClients;
    private static final long OneDay = 0x5265c00L;
    public static final long LicenseExceededConsecutiveSlidingWindow = 0x1cf7c5800L;
    protected static int concurrentRequests = 0;
    protected static int maxConcurrentRequestsRecorded = 0;
    protected static Object REQUEST_COUNT_LOCK = new Object();
    protected Vector localHostErrors;
    protected static int maxBundles = 0;
    protected Vector applications;
    protected static Servlet servletInstance = null;

    public Servlet()
    {
        servletName = "DeploySam";
        init_config = null;
        load_average = null;
        maxClients = 0;
        localHostErrors = new Vector();
    }

    public void init(ServletConfig config)
        throws ServletException
    {
        long start_time = System.currentTimeMillis();
        ((GenericServlet)this).init(config);
        init_config = config;
        String handlers = System.getProperty("java.protocol.handler.pkgs");
        if(handlers == null || handlers.indexOf("HTTPClient") == -1)
        {
            if(handlers != null && handlers != "")
                handlers = handlers + "|";
            else
                handlers = "";
            handlers = handlers + "HTTPClient";
            ((Hashtable) (System.getProperties())).put("java.protocol.handler.pkgs", ((Object) (handlers)));
        }
        try
        {
            doLocalInit(config);
            if(vault != null && vault.isLocationSet())
            {
                FileUtils.deleteDirectory(vault.getServerTempDir());
                vault.getServerTempDir().mkdirs();
            }
        }
        catch(Exception e)
        {
            String text = logException(((Object) (null)), ((Throwable) (e)), "DeploySam", "2.5.0", "Exception generated during initialization. ");
            setVaultUnavailable(text);
        }
        float duration = getElapsedTime(start_time);
        servletInstance = this;
        logMessage((String)null, "ServerStartupComplete", getServletInfo(), "2.5.0", "Startup completed in " + duration + " seconds");
    }

    public static Servlet getUniqueInstance()
    {
        return servletInstance;
    }

    float getElapsedTime(long startTime)
    {
        float result = (float)((System.currentTimeMillis() - startTime) / 100L) / 10F;
        return result;
    }

    protected void doLocalInit(ServletConfig config)
        throws ServletException
    {
        String basedir = ((GenericServlet)this).getInitParameter("basedir");
        if(basedir == null)
        {
            setVaultUnavailable("Not provided with the the vault base directory, make sure 'basedir' is set.");
            return;
        }
        ((Hashtable) (System.getProperties())).put("deploy.appserver", ((Object) (((GenericServlet)this).getServletConfig().getServletContext().getServerInfo())));
        try
        {
            properties = Configuration.loadConfiguration(basedir);
        }
        catch(Exception e)
        {
            String text = logException(((Object) (null)), ((Throwable) (e)), "DeploySam", "2.5.0", "Exception loading configuation: ");
            setVaultUnavailable("Configuration error: " + ((Throwable) (e)).getMessage() + "\n<HR><PRE>" + text + "\n</PRE>");
            properties = null;
            return;
        }
        ((Hashtable) (properties)).put("deploy.server.basedir", ((Object) (basedir)));
        ((Hashtable) (properties)).put("deploy.appserver", ((Object) (((GenericServlet)this).getServletConfig().getServletContext().getServerInfo())));
        File app_dir = new File(basedir);
        if(!app_dir.exists() || !app_dir.isDirectory())
        {
            setVaultUnavailable("Provided base directory: " + basedir + " does not exist or is not a directory");
            return;
        }
        String algorithm = properties.getProperty("deploy.hashcode.algorithm");
        if(algorithm != null && algorithm.trim().length() > 0)
            Hashcode.setHashcodeAlgorithm(algorithm);
        vaultType = properties.getProperty("deploy.vaulttype", "file");
        synchronized(servletName)
        {
            servletName = (String)((Hashtable) (properties)).get("deploy.server.name");
            if(servletName == null)
                servletName = "DeploySam";
        }
        vault = AppVault.getInstance(vaultType);
        try
        {
            vault.setLocation(basedir);
            vault.setServers(properties);
            vault.setClusterServers(properties);
        }
        catch(Exception e)
        {
            String text = logException(((Object) (null)), ((Throwable) (e)), "DeploySam", "2.5.0", "Exception during startup: ");
            if(e instanceof FileNotFoundException)
                setVaultUnavailable(((Throwable) (e)).getMessage());
            else
                setVaultUnavailable("<PRE>Error creating vault:\n" + text + "</PRE>");
            return;
        }
        manager = Timer.getTimer();
        synchronized(manager)
        {
            InetAddress sample = findLocalHost();
            String servername = servletName.replace(',', '_') + "/" + sample.getHostName();
            vault.setOtherServers(properties);
            logger = new LogQueue(servername, manager, properties, ((ServerActions) (this)));
            ((Thread) (logger)).setPriority(2);
            ((Thread) (logger)).setName("LoggingThread");
            ((Thread) (logger)).setDaemon(true);
            ((Thread) (logger)).start();
            long interval = PropertyUtils.readInterval(properties, "deploy.log.load.interval", -1L);
            load_average = new LoadAverage(manager, logger, interval, startup_time);
        }
        String configError = PropertyUtils.readDefaultString(properties, "deploy.config.errormessage", ((String) (null)));
        if(configError != null)
        {
            logMessage((String)null, "ServerConfigurationError", "DeploySam", "2.5.0", configError);
            ((Hashtable) (properties)).remove("deploy.config.errormessage");
        }
        int size = localHostErrors.size();
        for(int i = 0; i < size; i++)
            logMessage((String)null, "ServerConfigurationError", "DeploySam", "2.5.0", (String)localHostErrors.elementAt(i));

        localHostErrors = new Vector();
        reloadCheckpoints();
        licensed = Redir.isLicensed(((ServerActions) (this)), properties, vault.countApplications());
        if(licensed);
        maxClients = PropertyUtils.readInt(properties, "clients", -1);
        maxBundles = PropertyUtils.readInt(properties, "bundles", -1);
        String app = PropertyUtils.readDefaultString(properties, "applications", ((String) (null)));
        if(app == null || app.indexOf("(unlimited)") >= 0)
        {
            applications = new Vector();
        } else
        {
            applications = PropertyUtils.readVector(properties, "applications", ',');
            if(applications == null)
                applications = new Vector();
        }
        setVaultAvailable();
        CacheList.initializeCache(basedir);
        RequestList.initialize();
        synchronized(REQUEST_COUNT_LOCK)
        {
            concurrentRequests = 0;
        }
        ((Hashtable) (System.getProperties())).put("deploy.tmpdir", ((Object) (vault.getServerTempDir().toString())));
        System.gc();
    }

    protected InetAddress findLocalHost()
        throws ServletException
    {
        InetAddress localHosts[] = null;
        try
        {
            InetAddress server_id = InetAddress.getLocalHost();
            localHosts = InetAddress.getAllByName(server_id.getHostName());
        }
        catch(UnknownHostException e)
        {
            throw new ServletException("Unable to determine local host name from class InetAddress");
        }
        Vector cluster_hosts = PropertyUtils.readVector(properties, "deploy.cluster.processedhosts", ',');
        int cluster_size = cluster_hosts != null ? cluster_hosts.size() : 0;
        int local_size = localHosts != null ? localHosts.length : 0;
        HttpConnection conn = new HttpConnection();
        conn = null;
        InetAddress result = localHosts[0];
        StringBuffer otherHosts = new StringBuffer();
        for(int j = 0; j < local_size; j++)
        {
            for(int i = 0; i < cluster_size; i++)
            {
                String current = (String)cluster_hosts.elementAt(i);
                URI currentURI = null;
                try
                {
                    currentURI = new URI(current);
                }
                catch(ParseException e)
                {
                    localHostErrors.addElement(((Object) ("URL read from properties files ('" + current + "') is not valid, skipping it.")));
                    cluster_hosts.removeElementAt(i);
                    cluster_size--;
                    continue;
                }
                String thisMachine = localHosts[j].getHostName().toLowerCase();
                String currentMachine = currentURI.getHost().toLowerCase();
                try
                {
                    InetAddress test = InetAddress.getByName(currentMachine);
                    if(test.equals(((Object) (localHosts[j]))))
                    {
                        ((Hashtable) (properties)).put("deploy.localhost", ((Object) (current)));
                        result = localHosts[j];
                        continue;
                    }
                }
                catch(UnknownHostException e)
                {
                    localHostErrors.addElement(((Object) ("Could not determine IP address for host: " + currentMachine)));
                }
                if(j == 0)
                {
                    if(otherHosts.length() != 0)
                        otherHosts.append(",");
                    otherHosts.append(current);
                }
            }

        }

        String computed = PropertyUtils.readDefaultString(properties, "deploy.localhost", ((String) (null)));
        if(computed == null)
            localHostErrors.addElement("No entries in deploy.[server|cluster].host.* matches this machine");
        ((Hashtable) (properties)).put("deploy.otherhosts", ((Object) (otherHosts.toString())));
        return result;
    }

    protected void reloadCheckpoints()
    {
        File checkpointDir = vault.getServerCheckpointDir();
        File checkpointList[] = checkpointDir.listFiles();
        if(checkpointList == null || checkpointList.length == 0)
            return;
        ReplicationWorker w = (ReplicationWorker)AbstractWorker.getWorker(((ServerActions) (this)), 8);
        for(int i = 0; i < checkpointList.length; i++)
        {
            com.sitraka.deploy.common.checkpoint.Checkpoint checkpoint = BaseCheckpoint.reload(checkpointList[i]);
            w.replicate(checkpoint);
        }

    }

    protected void setVaultUnavailable(String reason)
    {
        synchronized(VAULT_LOCK)
        {
            vaultAvailable = false;
            vaultUnavailableText = reason;
        }
    }

    protected void setVaultAvailable()
    {
        synchronized(VAULT_LOCK)
        {
            vaultAvailable = true;
            vaultUnavailableText = "";
        }
    }

    public void restartServer()
    {
        restartServer(true);
    }

    protected void restartServer(boolean log)
    {
        long start_time = System.currentTimeMillis();
        if(log)
            logMessage((String)null, "ServerReloadStart", getServletInfo(), "2.5.0");
        if(logger != null)
            logger.stopLoggingThread();
        if(manager != null)
        {
            manager.flushQueue();
            manager.stopTimer();
        }
        setVaultUnavailable("Server is in the process of reloading.<br>Please reload this page in 10 seconds.");
        CacheList.saveAllData();
        logger = null;
        vaultType = null;
        manager = null;
        load_average = null;
        vault = null;
        properties = null;
        System.gc();
        try
        {
            doLocalInit(init_config);
        }
        catch(ServletException e)
        {
            logException(((Object) (null)), ((Throwable) (e)), "DeploySam", "2.5.0", "Exception during restart");
            return;
        }
        if(log)
        {
            float duration = getElapsedTime(start_time);
            logMessage((String)null, "ServerReloadComplete", getServletInfo(), "2.5.0", "Reload performed in " + duration + " seconds");
        }
    }

    public LogQueue getLogQueue()
    {
        return logger;
    }

    public void destroy()
    {
        logMessage((String)null, "ServerShutdownComplete", getServletInfo(), "2.5.0");
        if(logger != null)
            ((AbstractMessageQueue) (logger)).flushQueue();
        if(manager != null)
            manager.flushQueue();
        if(logger != null)
            logger.stopLoggingThread();
        CacheList.saveAllData();
    }

    protected void service(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException
    {
        registerRequestStart();
        try
        {
            super.service(req, resp);
        }
        finally
        {
            registerRequestEnd();
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        processRequest(request, response, 2);
    }

    public void doPut(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        processRequest(request, response, 1);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        processRequest(request, response, 0);
    }

    protected int isAuthorized(HttpServletRequest request, HttpServletResponse response, RequestParser parser)
    {
        if(parser.isSanctionedCommand() || parser.isUnlicensedCommand())
            return 1;
        if((parser.isAdminCommand() || parser.isAdministrator()) && isAdminAuthenticated(((Object) (request)), ((Object) (response)), parser) == 1)
            return 1;
        if(parser.isAuthorizableCommand())
        {
            int command = parser.getRequest();
            if(command == 2016)
            {
                Object user = AuthInfo.getUserAuthorization(((Object) (request)));
                String decoded = AuthInfo.decodeUser(user);
                String reqBundle = parser.getApplication();
                String reqVersion = parser.getVersion();
                String currentVersion = getVault().getLatestAuthorizedVersion(reqBundle, decoded);
                if(currentVersion == null)
                    return 2;
                if(reqVersion == null || !reqVersion.equalsIgnoreCase(currentVersion))
                    return 1;
            }
            return isUserAuthenticated(((Object) (request)), ((Object) (response)), parser);
        } else
        {
            return 2;
        }
    }

    protected void sendChallenge(HttpServletResponse response)
    {
        response.setHeader("WWW-Authenticate", "Basic realm=\"DeployDirector\"");
        response.setStatus(401);
    }

    protected RequestParser getParser(HttpServletRequest request, int type)
    {
        String post_params = "";
        for(Enumeration enum = ((ServletRequest) (request)).getParameterNames(); enum.hasMoreElements();)
        {
            String name = (String)enum.nextElement();
            String value = ((ServletRequest) (request)).getParameter(name);
            if(!post_params.equals(""))
                post_params = post_params + "&";
            post_params = post_params + name + "=" + value;
        }

        if(post_params.length() == 0 && (type == 1 || type == 2))
        {
            String query = request.getQueryString();
            if(query != null)
                post_params = query;
        }
        String request_text = normalizeRequest(request.getRequestURI(), request.getServletPath());
        RequestParser parser = new RequestParser(request_text, type, post_params);
        parser.restrictPlatform(vault);
        String requestID = request.getHeader("DeploySam-ID");
        if(request_text.toLowerCase().endsWith("/launch") || request_text.toLowerCase().endsWith("/install") || request_text.toLowerCase().endsWith("/install-plugin") || request_text.toLowerCase().endsWith("/launch-plugin"))
        {
            String param = request.getQueryString();
            if(param != null)
            {
                param = param.replace('&', ' ');
                parser.setParameters(param);
            }
        }
        if(requestID != null)
            parser.setRequestID(requestID);
        return parser;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response, int type)
        throws IOException
    {
        String uri = request.getRequestURI();
        if(uri.endsWith("/admin"))
        {
            uri = uri.substring(0, uri.lastIndexOf("/"));
            uri = uri.substring(0, uri.lastIndexOf("/"));
            uri = uri + "/admin/index.jsp";
            response.sendRedirect(uri);
            return;
        }
        RequestParser parser = getParser(request, type);
        try
        {
            boolean exceededMaximumBundles = false;
            boolean exceededExpiryDate = false;
            boolean exceededClientNumber = false;
            if(!isVaultAvailable())
            {
                if(!handleRestart(response, parser))
                    showVaultUnavailable(request, response);
                return;
            }
            exceededExpiryDate = exceededExpiryDate();
            if(!haveApplicationNames())
                exceededMaximumBundles = exceededMaximumBundles();
            exceededClientNumber = !allowClientAccess(request, parser);
            licensed &= !exceededMaximumBundles && !exceededExpiryDate && !exceededClientNumber;
            if(!licensed && !parser.isUnlicensedCommand() && !parser.isAdminUnlicensedCommand())
            {
                if(exceededMaximumBundles)
                {
                    showErrorPage("Exceeded the number of licensed Bundles. Request denied.", ((Object) (request)), ((Object) (response)));
                    return;
                }
                if(exceededExpiryDate)
                {
                    showErrorPage("Server license has expired. Request denied.", ((Object) (request)), ((Object) (response)));
                    return;
                }
                if(exceededClientNumber)
                {
                    showErrorPage("Maximum number of clients exceeded. Request by new client denied.", ((Object) (request)), ((Object) (response)));
                    return;
                } else
                {
                    showErrorPage("Server not licensed. Request denied.", ((Object) (request)), ((Object) (response)));
                    return;
                }
            }
            boolean authorized = isAuthorized(request, response, parser) == 1;
            if(!authorized)
            {
                sendChallenge(response);
                return;
            }
            if(!isApplicationLicensed(parser))
            {
                showErrorPage("The application, " + parser.getApplication() + ", is not licensed for deployment.", ((Object) (request)), ((Object) (response)));
                return;
            }
            logClientDataMessage(request, parser);
            if(handleRestart(response, parser))
                return;
            executeCommand(request, response, parser);
        }
        catch(ServletProcessException spe)
        {
            Object req = spe.getRequest();
            if(spe.mustLog())
            {
                String msg = ((Throwable) (spe)).getMessage() + "\n    " + rebuildURL(req);
                if(spe.getCategory() == "ServerExceptionError")
                {
                    logException(req, ((ServletException) (spe)).getRootCause(), spe.getApplication(), spe.getVersion(), msg);
                } else
                {
                    String remoteName = spe.getRemoteName();
                    if(remoteName != null)
                        logMessage(remoteName, spe.getCategory(), spe.getApplication(), spe.getVersion(), msg);
                    else
                        logMessage(req, spe.getCategory(), spe.getApplication(), spe.getVersion(), msg);
                }
            }
            if(spe.isShowErrorPage())
                showErrorPage(((Throwable) (spe)).getMessage(), req, spe.getResponse());
        }
        catch(ServletNotFoundException snfe)
        {
            Object req = snfe.getRequest();
            if(snfe.mustLog())
            {
                String msg = ((Throwable) (snfe)).getMessage() + "\n    " + rebuildURL(req);
                logMessage(req, snfe.getCategory(), snfe.getApplication(), snfe.getVersion(), msg);
            }
            if(snfe.isShowErrorPage())
                show404Page(req, snfe.getResponse());
        }
        catch(Exception e)
        {
            String appname = parser != null ? parser.getApplication() : "(unknown)";
            String version = parser != null ? parser.getVersion() : "(unknown)";
            String text = logException(((Object) (request)), ((Throwable) (e)), appname, version, ((String) (null)));
            showErrorPage("<PRE>" + text + "</PRE>", ((Object) (request)), ((Object) (response)));
        }
        catch(Error e)
        {
            String appname = parser != null ? parser.getApplication() : "(unknown)";
            String version = parser != null ? parser.getVersion() : "(unknown)";
            String text = logException(((Object) (request)), ((Throwable) (e)), appname, version, ((String) (null)));
            showErrorPage("<PRE>" + text + "</PRE>", ((Object) (request)), ((Object) (response)));
        }
    }

    protected boolean handleRestart(HttpServletResponse response, RequestParser parser)
    {
        if(parser.getRequest() == 3301)
        {
            restartServer();
            PrintWriter writer = getTextOutputStream(((Object) (response)));
            if(isVaultAvailable())
                writer.write("ServerReloadComplete");
            else
                writer.write(vaultUnavailableText);
            writer.flush();
            writer.close();
            return true;
        } else
        {
            return false;
        }
    }

    protected void executeCommand(HttpServletRequest request, HttpServletResponse response, RequestParser parser)
        throws ServletProcessException, ServletNotFoundException
    {
        AbstractWorker w = AbstractWorker.getWorker(((ServerActions) (this)), parser, ((Object) (request)), ((Object) (response)));
        if(w != null)
            try
            {
                w.startWorking();
            }
            finally
            {
                w.finishedWorking();
            }
        w = null;
    }

    protected void registerRequestStart()
    {
        if(load_average != null)
            load_average.registerRequest();
        synchronized(REQUEST_COUNT_LOCK)
        {
            concurrentRequests++;
            if(concurrentRequests > maxConcurrentRequestsRecorded)
                maxConcurrentRequestsRecorded = concurrentRequests;
        }
    }

    protected void registerRequestEnd()
    {
        synchronized(REQUEST_COUNT_LOCK)
        {
            if(concurrentRequests-- < 0)
                concurrentRequests = 0;
        }
    }

    public int getConcurrentRequestCount()
    {
        int count = 0;
        synchronized(REQUEST_COUNT_LOCK)
        {
            count = concurrentRequests;
        }
        return count;
    }

    public int getMaxConcurrentRequestsRecorded()
    {
        int count = 0;
        synchronized(REQUEST_COUNT_LOCK)
        {
            count = maxConcurrentRequestsRecorded;
        }
        return count;
    }

    private final boolean allowClientAccess(HttpServletRequest request, RequestParser parser)
    {
        boolean allow = true;
        if(exceededMaximumClients())
            if(isEvalLicense())
            {
                allow = false;
            } else
            {
                String userID = request.getRemoteUser();
                if(userID == null)
                    userID = getUserName(((Object) (request)), "username");
                if(userID == null || userID.equals(""))
                {
                    allow = true;
                } else
                {
                    boolean existingClient = false;
                    try
                    {
                        existingClient = getLogQueue().getClientDataLogger().hasClientConnected(userID, 0x1cf7c5800L);
                    }
                    catch(LogException le) { }
                    if(!existingClient)
                        allow = false;
                }
            }
        return allow;
    }

    private final boolean exceededMaximumClients()
    {
        boolean exceeded = false;
        int clientCount = 0;
        try
        {
            clientCount = getLogQueue().getClientDataLogger().getClients(0x1cf7c5800L);
        }
        catch(LogException le) { }
        if(maxClients != -1 && clientCount > maxClients)
            exceeded = true;
        return exceeded;
    }

    public static boolean isLicensed()
    {
        return licensed;
    }

    private final boolean exceededMaximumBundles()
    {
        boolean exceeded = false;
        if(maxBundles == -1)
            exceeded = false;
        else
        if(vault.countApplications() > maxBundles)
            exceeded = true;
        return exceeded;
    }

    private final boolean isApplicationLicensed(RequestParser parser)
    {
        String app = parser.getApplication();
        boolean appLicensed = true;
        int cmd = parser.getRequest();
        if(!parser.isAdministrator() && app != null && haveApplicationNames() && (cmd == 2004 || cmd == 2006 || cmd == 2008 || cmd == 2010 || cmd == 2013 || cmd == 2014) && !app.equalsIgnoreCase("DDAdmin") && !app.equalsIgnoreCase("DDCAM") && !app.equalsIgnoreCase("DDSDK"))
        {
            appLicensed = false;
            for(int i = 0; i < applications.size(); i++)
            {
                String str = (String)applications.elementAt(i);
                if(!str.equalsIgnoreCase(app))
                    continue;
                appLicensed = true;
                break;
            }

        }
        return appLicensed;
    }

    private final boolean haveApplicationNames()
    {
        boolean haveApps = false;
        if(applications != null && applications.size() > 0)
            haveApps = true;
        return haveApps;
    }

    private final boolean exceededExpiryDate()
    {
        Properties props = properties;
        LicenseProperties licenseProperties = new LicenseProperties();
        String date_string = props.getProperty("expiry", ((String) (null)));
        if(date_string != null && !"(never)".equals(((Object) (date_string))))
        {
            ((Properties) (licenseProperties)).setProperty("expiry", date_string);
            if(licenseProperties.isDateExpired("expiry"))
                return true;
        }
        return false;
    }

    private final boolean isEvalLicense()
    {
        boolean isEval = false;
        long date = PropertyUtils.readShortDate(properties, "expiry");
        if(date > 0L)
            isEval = true;
        return isEval;
    }

    protected boolean isVaultAvailable()
    {
        boolean flag;
        synchronized(VAULT_LOCK)
        {
            flag = vaultAvailable;
        }
        return flag;
    }

    public String getServletInfo()
    {
        return servletName;
    }

    public static File getLogFileDir(File servlet_dir)
    {
        String log_location = PropertyUtils.readDefaultString(properties, "deploy.log.file.location", servlet_dir.toString());
        File log_dir = new File(log_location);
        if(!log_dir.exists() && !log_dir.mkdirs())
        {
            log_dir = new File(servlet_dir, "logs");
            if(!log_dir.exists() && !log_dir.mkdirs())
                return null;
        }
        return log_dir;
    }

    public synchronized void logClientDataMessage(HttpServletRequest req, RequestParser parser)
    {
        if(logger == null)
            return;
        if(parser.getRequest() != 2016)
            return;
        String remote_id = null;
        String user_id = null;
        String last_client_ip = null;
        if(req != null)
        {
            remote_id = req.getHeader("CLIENTIDENT");
            user_id = req.getRemoteUser();
            last_client_ip = ((ServletRequest) (req)).getRemoteAddr();
            if(user_id == null)
                user_id = getUserName(((Object) (req)), "username");
            if(remote_id == null)
            {
                String values[] = ((ServletRequest) (req)).getParameterValues("CLIENTIDENT");
                remote_id = values != null ? values[0] : null;
            }
            if(remote_id == null)
                remote_id = ((ServletRequest) (req)).getRemoteHost();
            if(remote_id == null)
                remote_id = ((ServletRequest) (req)).getRemoteAddr();
        }
        if(user_id == null)
            user_id = remote_id;
        logger.logClientDataMessage(remote_id, user_id, parser.getApplication(), parser.getVersion(), last_client_ip);
    }

    public synchronized void logMessage(Object request, String message, String application, String version, String notes)
    {
        if(logger == null)
            return;
        HttpServletRequest req = (HttpServletRequest)request;
        String remote_id = req != null ? req.getHeader("CLIENTIDENT") : null;
        String user_id = req != null ? req.getRemoteUser() : null;
        if(user_id == null && req != null)
            user_id = getUserName(request, "username");
        if(remote_id == null && req != null)
        {
            String values[] = ((ServletRequest) (req)).getParameterValues("CLIENTIDENT");
            remote_id = values != null ? values[0] : null;
        }
        if(remote_id == null && req != null)
            remote_id = ((ServletRequest) (req)).getRemoteHost();
        if(remote_id == null && req != null)
            remote_id = ((ServletRequest) (req)).getRemoteAddr();
        if(user_id == null)
            user_id = remote_id;
        logger.logMessage(message, remote_id, user_id, application, version, notes);
    }

    public synchronized void logMessage(String remote_id, String message, String application, String version, String notes)
    {
        if(logger == null)
        {
            return;
        } else
        {
            remote_id = getServerURI(remote_id);
            logger.logMessage(message, remote_id, remote_id, application, version, notes);
            return;
        }
    }

    public void logMessage(Object request, String message, String application, String version)
    {
        logMessage(request, message, application, version, "");
    }

    public void logMessage(String remote_id, String message, String application, String version)
    {
        logMessage(remote_id, message, application, version, "");
    }

    public synchronized String logException(Object request, Throwable e, String application, String version, String extra_info)
    {
        StringBuffer notes = new StringBuffer();
        if(extra_info != null)
        {
            notes.append(extra_info);
            notes.append("\n");
        }
        StringWriter writer = new StringWriter();
        PrintWriter w = new PrintWriter(((java.io.Writer) (writer)));
        e.printStackTrace(w);
        notes.append(writer.toString());
        logMessage(request, "ServerExceptionError", application, version, notes.toString());
        return notes.toString();
    }

    public double[] getServerLoad()
    {
        return load_average.getCurrentLoad();
    }

    public long getUptime()
    {
        return System.currentTimeMillis() - startup_time;
    }

    public void show404Page(Object request, Object response)
    {
        StringBuffer content = new StringBuffer();
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse resp = (HttpServletResponse)response;
        content.append("<html><head><meta name=\"Author\" content=\"");
        content.append("DeploySam");
        content.append(" ");
        content.append("2.5.0");
        content.append("\">\n");
        content.append("<title>Error 404 - File Not Found</title></head><body>\n");
        content.append("<h3>Error 404 - File Not Found:</h3>\n");
        content.append("<tt>" + getRequestUrl(request) + "\n");
        Enumeration enum = ((ServletRequest) (req)).getParameterNames();
        content.append("<ul>\n");
        String name;
        String value;
        for(; enum.hasMoreElements(); content.append("NAME='" + name + "' VALUE='" + value + "'<BR>\n"))
        {
            name = (String)enum.nextElement();
            value = ((ServletRequest) (req)).getParameter(name);
        }

        content.append("</ul>\n");
        content.append("</tt>\n");
        content.append("</body></html>\n");
        if(resp != null)
        {
            resp.setStatus(404);
            resp.setHeader("Host", getServerURI(request));
            Date last_mod = new Date();
            ((ServletResponse) (resp)).setContentType("text/html");
            ((ServletResponse) (resp)).setContentLength(content.length());
            try
            {
                PrintWriter out = ((ServletResponse) (resp)).getWriter();
                out.print(content.toString());
                out.flush();
                out.close();
            }
            catch(IOException e)
            {
                logException(request, ((Throwable) (e)), ((ServletRequest) (req)).getParameter("APPLICATION"), ((ServletRequest) (req)).getParameter("VERSION"), ((String) (null)));
            }
        }
        logMessage(request, "ClientRequestFailed", ((ServletRequest) (req)).getParameter("APPLICATION"), ((ServletRequest) (req)).getParameter("VERSION"), "Error 404 - File not Found\n    Requested File: " + rebuildURL(request));
    }

    public void showVaultUnavailable(HttpServletRequest request, HttpServletResponse response)
    {
        String error_text = null;
        StringBuffer content = new StringBuffer();
        synchronized(VAULT_LOCK)
        {
            logMessage(((Object) (request)), "ClientRequestFailed", getServletInfo(), "2.5.0", vaultUnavailableText);
            error_text = vaultUnavailableText;
        }
        content.append("<HTML><HEAD><TITLE>Error</TITLE>\n");
        content.append("<meta name=\"Author\" content=\"");
        content.append("DeploySam");
        content.append(" ");
        content.append("2.5.0");
        content.append("\">\n");
        content.append("</HEAD>\n<BODY>\n");
        content.append("<H1>");
        content.append(servletName);
        content.append("</H1>\n");
        content.append("<H2><FONT color=\"red\">Vault is not currently available</FONT></H2>\n");
        content.append("<b><i>Reason:</b></i><br><ul>\n");
        content.append(error_text);
        content.append("</ul>\n");
        content.append("<HR>\n");
        content.append("<FONT SIZE=\"-1\"><B><I>Application management by</I></FONT><BR>\n");
        content.append("<STRONG><FONT SIZE=+2 FACE=\"Helvetica\" COLOR=\"#F7AD00\">DeployDirector");
        content.append("<sup>tm</sup></FONT></STRONG><BR>\n");
        content.append("<A HREF=\"");
        content.append("http://www.sitraka.com/software/deploydirector");
        content.append("\">");
        content.append("http://www.sitraka.com/software/deploydirector");
        content.append("</A>\n</BODY></HTML>");
        PrintWriter out;
        try
        {
            out = ((ServletResponse) (response)).getWriter();
        }
        catch(IOException e)
        {
            return;
        }
        response.setStatus(404);
        response.setHeader("Host", getServerURI(((Object) (request))));
        Date last_mod = new Date();
        response.setHeader("Expires", last_mod.toString());
        response.setHeader("Last-Modified", last_mod.toString());
        response.setHeader("Cache-Control", "no-cache");
        ((ServletResponse) (response)).setContentType("text/html");
        ((ServletResponse) (response)).setContentLength(content.length());
        out.print(content.toString());
        out.flush();
        out.close();
    }

    public void showInstallationPage(String applet_tag, String app, String version, Object request, Object response, boolean launch)
    {
        if(applet_tag == null)
            showErrorPage("No applet tag provided, cannot install or launch", request, response);
        String content = null;
        String default_content = "<HTML>\n<HEAD><TITLE>Installation of " + app + " " + version + "</TITLE></HEAD>\n" + "<BODY><FONT FACE=\"Verdana, Arial, Helvetica\" " + "SIZE=\"-1\">\n" + applet_tag + "\n</FONT></BODY>\n</HTML>";
        File basedir = vault.getVaultBaseDir();
        String filename = basedir.getAbsolutePath() + File.separator + "vault" + File.separator + app.toLowerCase() + File.separator + version.toLowerCase() + File.separator + "dd" + File.separator + "application.html";
        filename = FileUtils.unifyFileSeparator(filename);
        File htmlFile = new File(filename);
        if(!htmlFile.canRead())
            htmlFile = new File(vault.getServerEtcDir(), "application.html");
        if(!htmlFile.canRead())
        {
            logMessage(request, "ClientRequestFailed", app, version, "Cannot access fallback html page: " + filename + " using default text");
            content = default_content;
        } else
        {
            try
            {
                Application vault_app = vault.getApplication(app);
                Version vault_ver = vault_app.getVersion(version);
                TemplateText prep = new TemplateText(htmlFile);
                prep.setPlaceHolder("SERVLET NAME", ((Object) (servletName)));
                prep.setPlaceHolder("APPLET", ((Object) (applet_tag)));
                prep.setPlaceHolder("LOGO", ((Object) (getPathToServlet(request) + "/" + "deploydirector.gif")));
                prep.setPlaceHolder("LOGO_SS", ((Object) (getPathToServlet(request) + "/" + "deploydirector_ss.gif")));
                prep.setPlaceHolder("URL", "http://www.sitraka.com/software/deploydirector");
                prep.setPlaceHolder("VERSION", ((Object) (vault_ver.getName())));
                prep.setPlaceHolder("APPLICATION", ((Object) (vault_app.getName())));
                prep.setPlaceHolder("LAUNCHORINSTALL", ((Object) (launch ? "Launch" : "Install")));
                content = prep.toString();
            }
            catch(IOException e)
            {
                logException(request, ((Throwable) (e)), app, version, "Can't access: '" + htmlFile + "' using default text");
                content = default_content;
            }
        }
        HttpServletResponse resp = (HttpServletResponse)response;
        resp.setHeader("Host", getServerURI(request));
        ((ServletResponse) (resp)).setContentType("text/html");
        ((ServletResponse) (resp)).setContentLength(content.length());
        try
        {
            PrintWriter out = ((ServletResponse) (resp)).getWriter();
            out.print(content);
            out.flush();
            out.close();
        }
        catch(IOException e)
        {
            return;
        }
    }

    public void showErrorPage(String error_text, Object request, Object response, boolean log)
    {
        if(log)
            logMessage(request, "ClientRequestFailed", getServletInfo(), "2.5.0", error_text + "\n    " + rebuildURL(request));
        showErrorPage(error_text, request, response);
    }

    public void showErrorPage(String error_text, Object request, Object response)
    {
        if(response == null)
            return;
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse resp = (HttpServletResponse)response;
        String content = null;
        String default_text = "<HTML><HEAD><TITLE>Error</TITLE></HEAD>\n<BODY>\n<H2>" + servletName + " - Error</H2>\n" + "<FONT color=\"red\">" + error_text + "</font>\n" + "</BODY></HTML>";
        String error_name = properties.getProperty("deploy.error.page");
        if(error_name != null)
        {
            File header = new File(error_name);
            if(!header.canRead())
                content = default_text;
            else
                try
                {
                    TemplateText prep = new TemplateText(error_name);
                    prep.setPlaceHolder("SERVLET NAME", ((Object) (servletName)));
                    prep.setPlaceHolder("ERROR TEXT", ((Object) (error_text)));
                    prep.setPlaceHolder("LOGO", ((Object) (getPathToServlet(request) + "/" + "deploydirector.gif")));
                    prep.setPlaceHolder("LOGO_SS", ((Object) (getPathToServlet(request) + "/" + "deploydirector_ss.gif")));
                    prep.setPlaceHolder("URL", "http://www.sitraka.com/software/deploydirector");
                    StringBuffer req_content = new StringBuffer();
                    req_content.append("<TT>\n");
                    req_content.append(getRequestUrl(request));
                    String name;
                    String value;
                    for(Enumeration enum = ((ServletRequest) (req)).getParameterNames(); enum.hasMoreElements(); req_content.append("<BR>NAME='" + name + "' VALUE='" + value + "'\n"))
                    {
                        name = (String)enum.nextElement();
                        value = ((ServletRequest) (req)).getParameter(name);
                    }

                    req_content.append("</tt>\n");
                    prep.setPlaceHolder("REQUEST", ((Object) (req_content.toString())));
                    content = prep.toString();
                }
                catch(IOException e)
                {
                    content = default_text;
                }
        } else
        {
            content = default_text;
        }
        try
        {
            resp.setStatus(404);
            resp.setHeader("Host", getServerURI(request));
            ((ServletResponse) (resp)).setContentType("text/html");
            ((ServletResponse) (resp)).setContentLength(content.length());
        }
        catch(IllegalStateException e) { }
        try
        {
            PrintWriter out = ((ServletResponse) (resp)).getWriter();
            out.print(content);
            out.flush();
            out.close();
        }
        catch(IOException e)
        {
            return;
        }
    }

    public int isUserAuthenticated(Object request, Object response, RequestParser parser)
    {
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse resp = (HttpServletResponse)response;
        if(!isVaultAvailable())
        {
            showVaultUnavailable(req, resp);
            return 2;
        }
        String app = parser.getApplication();
        String version = parser.getVersion();
        Object user = AuthInfo.getUserAuthorization(request);
        String decoded = AuthInfo.decodeUser(user);
        if(parser.getRequest() == 2008 || parser.getRequest() == 2010 || parser.getRequest() == 2013 || parser.getRequest() == 2014)
        {
            if(app == null)
            {
                String message = "Request to install with no application specified";
                showErrorPage(message, request, response, true);
                return 2;
            }
            if(version == null)
                version = getVault().getLatestAuthorizedVersion(app, decoded);
        }
        if(app == null || version == null)
            if(parser.getRequest() == 2004)
            {
                if(parser.isSpecialFile())
                    return 1;
                File testFile = new File(getVault().getVaultBaseDir(), parser.getRequestText());
                return !testFile.exists() ? 1 : 2;
            } else
            {
                return 2;
            }
        Application app_obj = vault.getApplication(app);
        if(app_obj == null)
        {
            String message = "Application: " + app + " does not exist";
            showErrorPage(message, request, response, true);
            return 2;
        }
        int command = parser.getRequest();
        if(app_obj.getVersion(version) == null)
            if(command == 2016)
            {
                version = vault.getLatestVersion(app);
            } else
            {
                String message = "Application " + app + " does not have version " + version;
                showErrorPage(message, request, response, true);
                return 2;
            }
        if(app.toLowerCase().trim().equalsIgnoreCase("DDCAM"))
            return 1;
        if(validateRequest(req, resp, parser, decoded, app, version, false) == 1)
            return 1;
        return isAdminAuthenticated(((Object) (req)), ((Object) (resp)), parser) != 1 ? 2 : 1;
    }

    public int isAdminAuthenticated(Object req, Object resp, RequestParser parser)
    {
        HttpServletRequest request = (HttpServletRequest)req;
        HttpServletResponse response = (HttpServletResponse)resp;
        String user = AuthInfo.getUserName(((Object) (request)), "adminname");
        if(user == null || user.trim().length() == 0)
        {
            return 2;
        } else
        {
            String password = AuthInfo.getUserPassword(((Object) (request)), "adminpassword");
            int authorized = validateRequest(request, response, parser, user + ":" + password, "DDAdmin", ((String) (null)), true);
            boolean status = authorized == 1;
            return authorized;
        }
    }

    protected int validateRequest(HttpServletRequest request, HttpServletResponse response, RequestParser parser, String userData, String app, String version, boolean adminAuthorize)
    {
        Authentication authentication = null;
        authentication = getAuthenticationObject(request, app, version);
        if(authentication == null)
            return 2;
        boolean authenticated = authentication.isAuthentic(((Object) (userData)));
        if(!authenticated)
            return 2;
        Authorization authorization = getAuthorizationObject(request, app, version);
        String userID = authentication.getUserID(((Object) (userData)));
        int authorized = 2;
        if(authorization instanceof RoleAuthorization)
            authorized = ((RoleAuthorization)authorization).isRoleAuthorized(userID, parser.getRoles());
        else
            authorized = authorization.isAuthorized(userID, app, version);
        return authorized;
    }

    protected boolean sanityCheck(HttpServletRequest request, String app, String version)
    {
        Application app_obj = vault.getApplication(app);
        if(app_obj == null)
        {
            logMessage(((Object) (request)), "ServerInternalError", app, version, "Application: " + app + " does not exist");
            return false;
        }
        if(app_obj.getVersion(version) == null)
        {
            logMessage(((Object) (request)), "ServerInternalError", app, version, "Application " + app + " does not have version " + version);
            return false;
        } else
        {
            return true;
        }
    }

    protected Authentication getAuthenticationObject(HttpServletRequest request, String app, String version)
    {
        Authentication authentication = getVault().getAuthenticationObject(app, version);
        if(authentication == null)
        {
            String message = "Unable to access authentication object for " + app + ", " + version;
            if("DDAdmin".equals(((Object) (app))))
                logMessage(((Object) (request)), "ServerLoginFailed", app, version, message);
            else
                logMessage(((Object) (request)), "ClientLoginFailed", app, version, message);
            return null;
        } else
        {
            return authentication;
        }
    }

    protected Authorization getAuthorizationObject(HttpServletRequest request, String app, String version)
    {
        Authorization authorization = getVault().getAuthorizationObject(app, version);
        if(authorization == null)
        {
            String message = "Unable to obtain authorization object for " + app + ", " + version;
            if("DDAdmin".equals(((Object) (app))))
                logMessage(((Object) (request)), "ServerLoginFailed", app, version, message);
            else
                logMessage(((Object) (request)), "ClientLoginFailed", app, version, message);
            return null;
        } else
        {
            return authorization;
        }
    }

    public String getAdminID()
    {
        if(properties == null)
            return null;
        else
            return PropertyUtils.readDefaultString(properties, "deploy.admin.username", "");
    }

    public String getAdminPassword()
    {
        if(properties == null)
            return null;
        else
            return PropertyUtils.readDefaultString(properties, "deploy.admin.password", "");
    }

    public void setContentLength(Object response, int length)
    {
        HttpServletResponse resp = (HttpServletResponse)response;
        ((ServletResponse) (resp)).setContentLength(length);
    }

    public void setContentType(Object response, String type)
    {
        HttpServletResponse resp = (HttpServletResponse)response;
        ((ServletResponse) (resp)).setContentType(type);
    }

    public void setHeader(Object response, String header, String value)
    {
        HttpServletResponse resp = (HttpServletResponse)response;
        if(header.equalsIgnoreCase("Content-Length"))
            ((ServletResponse) (resp)).setContentLength(Integer.parseInt(value));
        else
            resp.setHeader(header, value);
    }

    public int getContentLength(Object request)
    {
        if(!(request instanceof HttpServletRequest))
            return -1;
        HttpServletRequest req = (HttpServletRequest)request;
        String clength = String.valueOf(((ServletRequest) (req)).getContentLength());
        if(clength == null)
            clength = ((ServletRequest) (req)).getParameter("Content-Length");
        if(clength == null)
            return -1;
        try
        {
            int length = Integer.parseInt(clength);
            return length;
        }
        catch(NumberFormatException e)
        {
            return -1;
        }
    }

    public PrintWriter getTextOutputStream(Object response)
    {
        try
        {
            return ((ServletResponse) ((HttpServletResponse)response)).getWriter();
        }
        catch(IOException e)
        {
            return null;
        }
    }

    public OutputStream getBinaryOutputStream(Object response)
    {
        try
        {
            return ((OutputStream) (((ServletResponse) ((HttpServletResponse)response)).getOutputStream()));
        }
        catch(IOException e)
        {
            return null;
        }
    }

    public InputStream getBinaryInputStream(Object request)
    {
        try
        {
            int length = getContentLength(request);
            InputStream is = ((InputStream) (((ServletRequest) ((HttpServletRequest)request)).getInputStream()));
            if(length <= 0)
                return is;
            else
                return ((InputStream) (new LimitedInputStream(is, length)));
        }
        catch(IOException e)
        {
            return null;
        }
    }

    public String rebuildURL(Object request)
    {
        HttpServletRequest req = (HttpServletRequest)request;
        String url = getRequestUrl(((Object) (req)));
        String params = req.getQueryString();
        if(params != null)
            url = url + "?" + params;
        return url;
    }

    public String getRequestUrl(Object request)
    {
        return HttpUtils.getRequestURL((HttpServletRequest)request).toString();
    }

    public String getPathToServlet(Object request)
    {
        HttpServletRequest req = (HttpServletRequest)request;
        String path = req.getContextPath() + req.getServletPath();
        if(path == null || path.length() == 0)
            path = "/";
        return path;
    }

    public String getServerURI(Object request)
    {
        String url = getRequestUrl(request);
        return getServerURI(url);
    }

    public String getServerURI(String url)
    {
        if(url != null)
        {
            int index = url.indexOf("//");
            if(index >= 0)
            {
                url = url.substring(index + 2);
                index = url.indexOf('/');
                if(index >= 0)
                    url = url.substring(0, index);
            }
        }
        return url;
    }

    /**
     * @deprecated Method getServerPath is deprecated
     */

    public String getServerPath(Object request)
    {
        HttpServletRequest req = (HttpServletRequest)request;
        String url = req.getServletPath();
        return url;
    }

    public AppVault getVault()
    {
        return vault;
    }

    protected String normalizeRequest(String request_text, String path)
    {
        if(request_text.equals(((Object) (path))))
            return "";
        if(request_text.endsWith(path))
            return "";
        int index = request_text.indexOf(path);
        index += path.length() + 1;
        if(index >= request_text.length())
            return "";
        else
            return request_text.substring(index);
    }

    public Object getUserAuthorization(Object request)
    {
        return AuthInfo.getUserAuthorization(request);
    }

    public String getUserName(Object request, String field)
    {
        return AuthInfo.getUserName(request, field);
    }

    public String getUserPassword(Object request, String field)
    {
        return AuthInfo.getUserPassword(request, field);
    }

}
