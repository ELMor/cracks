// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LimitedInputStream.java

package com.sitraka.deploy.sam.servlet;

import java.io.IOException;
import java.io.InputStream;

public class LimitedInputStream extends InputStream
{

    protected int total;
    protected int content_length;
    protected InputStream wrapped_stream;

    public LimitedInputStream(InputStream _source, int _length)
    {
        total = 0;
        content_length = -1;
        wrapped_stream = null;
        wrapped_stream = _source;
        content_length = _length;
    }

    public int read()
        throws IOException
    {
        if(total >= content_length)
            return -1;
        int value = wrapped_stream.read();
        if(value == -1)
            total = content_length;
        else
            total += value;
        return value;
    }

    public InputStream getWrappedStream()
    {
        return wrapped_stream;
    }

    public long getContentLength()
    {
        return (long)content_length;
    }

    public int read(byte b[], int off, int len)
        throws IOException
    {
        if(total >= content_length)
            return -1;
        int adjustedLength = len;
        if(total + len > content_length)
            adjustedLength = content_length - total;
        int value = wrapped_stream.read(b, off, adjustedLength);
        total += value;
        return value;
    }

    public int read(byte b[])
        throws IOException
    {
        if(b == null)
            throw new NullPointerException("Input Buffer cannot be null");
        else
            return wrapped_stream.read(b, 0, b.length);
    }

    public synchronized void mark(int readlimit)
    {
        wrapped_stream.mark(readlimit);
    }

    public long skip(long n)
        throws IOException
    {
        return wrapped_stream.skip(n);
    }

    public int available()
        throws IOException
    {
        return content_length - total;
    }

    public void close()
        throws IOException
    {
        wrapped_stream.close();
    }

    public synchronized void reset()
        throws IOException
    {
        wrapped_stream.reset();
    }

    public boolean markSupported()
    {
        return wrapped_stream.markSupported();
    }
}
