// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DisplayStatus.java

package com.sitraka.deploy.sam;

import HTTPClient.ParseException;
import HTTPClient.URI;
import com.sitraka.deploy.sam.worker.AdminPageWorker;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam:
//            Configuration, Redir, ServerActions, AppVault, 
//            LogQueue, RequestParser

public class DisplayStatus
{
    class FakeServer
        implements ServerActions
    {

        public AppVault getVault()
        {
            return null;
        }

        public LogQueue getLogQueue()
        {
            return null;
        }

        public Object getUserAuthorization(Object request)
        {
            return ((Object) (null));
        }

        public String getAdminID()
        {
            return null;
        }

        public String getAdminPassword()
        {
            return null;
        }

        public String getRequestUrl(Object request)
        {
            return null;
        }

        public String getServerPath(Object request)
        {
            return null;
        }

        public String getServerURI(Object request)
        {
            return null;
        }

        public String getServerURI(String url)
        {
            return null;
        }

        public String getServletInfo()
        {
            return "Server Standin Class";
        }

        public String getUserName(Object request, String field)
        {
            return null;
        }

        public String logException(Object request, Throwable e, String application, String s, String s1)
        {
            return null;
        }

        public String rebuildURL(Object request)
        {
            return null;
        }

        public double[] getServerLoad()
        {
            return null;
        }

        public int getConcurrentRequestCount()
        {
            return 0;
        }

        public int getContentLength(Object request)
        {
            return 0;
        }

        public int getMaxConcurrentRequestsRecorded()
        {
            return 0;
        }

        public int isUserAuthenticated(Object request, Object response, RequestParser parser)
        {
            return 0;
        }

        public InputStream getBinaryInputStream(Object request)
        {
            return null;
        }

        public OutputStream getBinaryOutputStream(Object response)
        {
            return null;
        }

        public PrintWriter getTextOutputStream(Object response)
        {
            return null;
        }

        public long getUptime()
        {
            return 0L;
        }

        public void logMessage(Object request, String message, String application, String version)
        {
            reallyLog(message, application, version, ((String) (null)));
        }

        public void logMessage(Object request, String message, String application, String version, String notes)
        {
            reallyLog(message, application, version, notes);
        }

        public void logMessage(String remote_id, String message, String application, String version, String notes)
        {
            reallyLog(message, application, version, notes);
        }

        public void logMessage(String remote_id, String message, String application, String version)
        {
            reallyLog(message, application, version, ((String) (null)));
        }

        public void restartServer()
        {
        }

        public void setContentLength(Object obj, int i)
        {
        }

        public void setContentType(Object obj, String s)
        {
        }

        public void setHeader(Object obj, String s, String s1)
        {
        }

        public void show404Page(Object obj, Object obj1)
        {
        }

        public void showErrorPage(String s, Object obj, Object obj1)
        {
        }

        public void showErrorPage(String s, Object obj, Object obj1, boolean flag)
        {
        }

        public void showInstallationPage(String s, String s1, String s2, Object obj, Object obj1, boolean flag)
        {
        }

        protected void reallyLog(String message, String app, String ver, String notes)
        {
            System.out.println("--------------------------------------------------");
            System.err.println("Error: " + message);
            System.err.println("\tApplication: " + app);
            System.err.println("\tVersion: " + ver);
            System.err.println("\tNotes: " + (notes == null ? "none" : notes));
            System.out.println("--------------------------------------------------");
        }

        FakeServer()
        {
        }
    }


    protected AdminPageWorker worker;
    protected ServerActions fake;
    protected Properties props;

    public DisplayStatus(String commandline[])
    {
        worker = null;
        fake = null;
        props = null;
        worker = new AdminPageWorker();
        if(commandline != null && commandline.length > 0)
        {
            fake = ((ServerActions) (new FakeServer()));
            try
            {
                props = Configuration.loadConfiguration(commandline[0]);
            }
            catch(FileNotFoundException e)
            {
                System.err.println("Error: file could not be accessed from base directory: " + commandline[0]);
                ((Throwable) (e)).printStackTrace();
                props = null;
                fake = null;
                return;
            }
            findLocalHost();
        }
    }

    protected void findLocalHost()
    {
        InetAddress localHosts[] = null;
        try
        {
            InetAddress server_id = InetAddress.getLocalHost();
            localHosts = InetAddress.getAllByName(server_id.getHostName());
        }
        catch(UnknownHostException e)
        {
            ((Throwable) (e)).printStackTrace();
            return;
        }
        Vector cluster_hosts = PropertyUtils.readVector(props, "deploy.cluster.processedhosts", ',');
        int cluster_size = cluster_hosts != null ? cluster_hosts.size() : 0;
        int local_size = localHosts != null ? localHosts.length : 0;
        InetAddress result = localHosts[0];
        StringBuffer otherHosts = new StringBuffer();
        for(int j = 0; j < local_size; j++)
        {
            for(int i = 0; i < cluster_size; i++)
            {
                String current = (String)cluster_hosts.elementAt(i);
                URI currentURI = null;
                try
                {
                    currentURI = new URI(current);
                }
                catch(ParseException e)
                {
                    System.out.println("URL read from properties files ('" + current + "') is not valid, skipping it.");
                    cluster_hosts.removeElementAt(i);
                    cluster_size--;
                    continue;
                }
                String thisMachine = localHosts[j].getHostName().toLowerCase();
                String currentMachine = currentURI.getHost().toLowerCase();
                try
                {
                    InetAddress test = InetAddress.getByName(currentMachine);
                    if(test.equals(((Object) (localHosts[j]))))
                    {
                        ((Hashtable) (props)).put("deploy.localhost", ((Object) (current)));
                        result = localHosts[j];
                        continue;
                    }
                }
                catch(UnknownHostException e)
                {
                    System.out.println("Could not determine IP address for host: " + currentMachine);
                }
                if(j == 0)
                {
                    if(otherHosts.length() != 0)
                        otherHosts.append(",");
                    otherHosts.append(current);
                }
            }

        }

        String computed = PropertyUtils.readDefaultString(props, "deploy.localhost", ((String) (null)));
        if(computed == null)
            System.out.println("No entries in deploy.[server|cluster].host.* matches this machine");
        ((Hashtable) (props)).put("deploy.otherhosts", ((Object) (otherHosts.toString())));
    }

    protected void displayHeader()
    {
        String header = "DeployDirector Server Information";
        System.out.println("");
        System.out.println(header);
        for(int i = 0; i < header.length(); i++)
            System.out.print("-");

        System.out.println("");
    }

    protected void displayHosts()
    {
        Vector hosts = worker.getValidHostNames();
        System.out.println("  The following is the list of host names (and addresses) DeployDirector");
        System.out.println("  considers valid for this system. This is not an exclusive list.");
        int size = hosts.size();
        if(size == 0)
        {
            System.out.println("    No valid host names found. This is a critical error");
            System.out.println("    and will prevent DeployDirector from running on this system.");
        } else
        {
            for(int i = 0; i < size; i++)
                System.out.println("    " + (i + 1) + ". " + (String)hosts.elementAt(i));

        }
        System.out.println("");
    }

    protected void displaySAMVersion()
    {
        System.out.print("  Version information: ");
        System.out.println(worker.getSAMVersion());
        System.out.println("");
    }

    protected void displayPlatform()
    {
        System.out.print("  Platform: ");
        System.out.println(worker.getPlatform());
        System.out.println("");
    }

    protected void displayVM()
    {
        System.out.print("  Java VM: ");
        System.out.println(worker.getVMString());
        System.out.println("");
    }

    protected void displayLicense()
    {
        if(props == null)
            return;
        boolean licensed = Redir.isLicensed(fake, props, 0);
        System.out.print("  Current license status: ");
        if(licensed)
            System.out.println("Licensed");
        else
            System.out.println("Not licensed");
        System.out.println("");
    }

    public static void main(String args[])
    {
        DisplayStatus display = new DisplayStatus(args);
        display.displayHeader();
        display.displaySAMVersion();
        display.displayPlatform();
        display.displayVM();
        display.displayHosts();
        display.displayLicense();
    }
}
