/* RequestList - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.sitraka.deploy.sam.request;
import java.io.File;

import com.sitraka.deploy.common.timer.Timer;
import com.sitraka.deploy.common.timer.TimerEvent;
import com.sitraka.deploy.common.timer.TimerEventListener;
import com.sitraka.deploy.sam.AbstractTrackerEntry;
import com.sitraka.deploy.sam.AbstractTrackerList;
import com.sitraka.deploy.sam.RequestParser;

public class RequestList extends AbstractTrackerList
    implements TimerEventListener
{
    private static final long DEFAULT_CACHE_AGE = 300000L;
    protected static RequestList instance = null;
    protected static Timer manager = null;

    public static void initialize() {
        if (instance == null)
            instance = new RequestList();
        instance.max_cache_age = 300000L;
        instance.max_cache_size = 9223372036854775807L;
        instance.cache_available = true;
        TimerEvent t = new TimerEvent(System.currentTimeMillis(),
                                      instance.max_cache_age, instance);
        Timer.getTimer().addTimerEvent(t);
    }

    protected void saveData() {
        /* empty */
    }

    public static RequestObject addRequest(RequestParser parser,
                                           File tempFile) {
        if (instance != null && !instance.isAvailable())
            return null;
        if (tempFile != null && !tempFile.canRead())
            return null;
        String request = instance.convertParserToString(parser);
        RequestObject c = new RequestObject(tempFile, request);
        if (!c.isValid())
            return null;
        if (!instance.addEntry(c))
            return null;
        instance.saveCachedData();
        return c;
    }

    public boolean isAvailable() {
        if (instance == null)
            initialize();
        return instance.cache_available;
    }

    public static int getNumFiles() {
        if (instance == null || !instance.isAvailable())
            return -1;
        return instance.numFiles();
    }

    public static long getCacheSize() {
        if (instance == null || !instance.isAvailable())
            return -1L;
        return instance.getSizeofCache();
    }

    public static AbstractTrackerEntry getRequest(RequestParser _request) {
        if (instance == null)
            initialize();
        return instance.getCachedRequest(_request);
    }

    public static AbstractTrackerEntry getRequestByID(long id) {
        if (instance == null)
            initialize();
        return instance.findByID(id);
    }

    public static void saveAllData() {
        if (instance != null && instance.isAvailable())
            instance.saveCachedData();
    }

    public static void removeRequest(AbstractTrackerEntry obj) {
        if (instance == null)
            initialize();
        instance.removeEntry(obj);
    }

    public static void removeBundleVersionReference(String bundle_name,
                                                    String version) {
        if (instance != null && instance.isAvailable())
            instance.removeBundleVersion(bundle_name, version);
    }

    public static void removeBundleReference(String bundle_name) {
        if (instance != null && instance.isAvailable())
            instance.removeBundle(bundle_name);
    }

    public void eventOccured(TimerEvent e) {
        this.removeExpiredEntries();
    }

    public void flushOccured(TimerEvent e) {
        /* empty */
    }

    public static void addReference(AbstractTrackerEntry obj) {
        if (obj != null)
            obj.addReference();
    }

    public static void removeReference(AbstractTrackerEntry obj) {
        if (obj != null) {
            obj.removeReference();
            if (obj.getReferenceCount() <= 0)
                instance.deleteEntry(obj);
        }
    }

    public static long getMaxCacheAge() {
        if (instance == null)
            return 0L;
        return instance.maxAge();
    }

    public static RequestObject[] getAllRequests() {
        if (instance == null)
            return new RequestObject[0];
        return instance.getAllRequestsInternal();
    }

    public RequestObject[] getAllRequestsInternal() {
        if (!instance.isAvailable())
            return new RequestObject[0];
        RequestObject[] result = null;
        synchronized (CACHE_LOCK) {
            int size = cache == null ? 0 : cache.size();
            result = new RequestObject[size];
            for (int i = 0; i < size; i++)
                result[i] = (RequestObject) cache.elementAt(i);
        }
        return result;
    }
}
