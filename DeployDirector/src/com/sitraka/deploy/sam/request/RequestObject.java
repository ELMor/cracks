// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RequestObject.java

package com.sitraka.deploy.sam.request;

import com.sitraka.deploy.sam.AbstractTrackerEntry;
import com.sitraka.deploy.sam.cache.CacheList;
import java.io.File;
import java.util.Date;

// Referenced classes of package com.sitraka.deploy.sam.request:
//            RequestList

public class RequestObject extends AbstractTrackerEntry
{

    public RequestObject(File _newFile, String _request)
    {
        super(_newFile, _request);
    }

    public String toString()
    {
        StringBuffer buffer = new StringBuffer();
        buffer.append(((Object)this).getClass().getName() + " [" + ((Object)this).hashCode() + "]\n");
        if(super.file != null)
            buffer.append("\tFile: " + super.file + "\n");
        else
        if(super.text != null)
            buffer.append("\tText: " + super.text.length() + " characters\n");
        else
            buffer.append("\tData: no data being held right now (request in progress?)\n");
        buffer.append("\tSize: " + super.size + "\n");
        buffer.append("\tCreation date: " + new Date(super.createDate) + "\n");
        buffer.append("\tLast access: " + new Date(super.accessDate) + "\n");
        long timeout = 0L;
        if(RequestList.getRequestByID(super.id) != null)
            timeout = super.accessDate + RequestList.getMaxCacheAge();
        else
            timeout = super.accessDate + CacheList.getMaxCacheAge();
        timeout -= System.currentTimeMillis();
        if(timeout <= 0L)
        {
            buffer.append("\tRequest has already expired, waiting for cleanup thread to remove it.\n");
        } else
        {
            timeout /= 1000L;
            buffer.append("\tExpires in: " + timeout + " seconds (~ " + (float)timeout / 60F + " minutes)\n");
        }
        buffer.append("\tReference count: " + super.refCount + "\n");
        buffer.append("\tRequest Text: " + super.request + "\n");
        buffer.append("\tUnique id: " + super.id + "\n");
        buffer.append("\tStatus Text: " + super.statusText + "\n");
        buffer.append("\tStatus: " + super.status + "\n");
        buffer.append("\tIs Valid?: " + super.valid);
        return buffer.toString();
    }
}
