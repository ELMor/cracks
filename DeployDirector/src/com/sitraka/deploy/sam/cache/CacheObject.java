// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CacheObject.java

package com.sitraka.deploy.sam.cache;

import com.sitraka.deploy.sam.AbstractTrackerEntry;
import com.sitraka.deploy.sam.request.RequestList;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.File;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.cache:
//            CacheList

public class CacheObject extends AbstractTrackerEntry
{

    public CacheObject(File _cacheDir, File _newFile, String _request)
    {
        super(_newFile, _request);
        super.valid = ((AbstractTrackerEntry)this).moveFile(_cacheDir);
        super.valid = true;
        ((AbstractTrackerEntry)this).setStatus(3);
    }

    public CacheObject(File _cacheDir, Properties props, int base)
    {
        String filename = PropertyUtils.readDefaultString(props, "file" + base + ".file", ((String) (null)));
        if(filename == null)
        {
            super.valid = false;
            return;
        }
        ((Hashtable) (props)).remove(((Object) ("file" + base + ".file")));
        super.file = new File(_cacheDir, filename);
        if(!super.file.canRead())
        {
            super.valid = false;
            return;
        }
        super.size = super.file.length();
        super.request = PropertyUtils.readDefaultString(props, "file" + base + ".request", ((String) (null)));
        ((Hashtable) (props)).remove(((Object) ("file" + base + ".request")));
        if(super.request == null)
        {
            super.valid = false;
            return;
        }
        long rightNow = System.currentTimeMillis();
        super.createDate = PropertyUtils.readLong(props, "file" + base + ".creationdate", rightNow);
        ((Hashtable) (props)).remove(((Object) ("file" + base + ".creationdate")));
        if(super.createDate > rightNow)
            super.createDate = rightNow;
        super.accessDate = PropertyUtils.readLong(props, "file" + base + ".accessdate", rightNow);
        ((Hashtable) (props)).remove(((Object) ("file" + base + ".accessdate")));
        if(super.accessDate > rightNow)
            super.accessDate = rightNow;
        super.id = PropertyUtils.readLong(props, "file" + base + ".id", super.id);
        super.refCount = PropertyUtils.readInt(props, "file" + base + ".refcount", super.refCount);
        super.valid = true;
        ((AbstractTrackerEntry)this).setStatus(3);
    }

    public String toString()
    {
        StringBuffer buffer = new StringBuffer();
        buffer.append(((Object)this).getClass().getName() + " [" + ((Object)this).hashCode() + "]\n");
        buffer.append("\tFile: " + super.file + "\n");
        buffer.append("\tSize: " + super.size + "\n");
        buffer.append("\tCreation date: " + new Date(super.createDate) + "\n");
        buffer.append("\tLast access: " + new Date(super.accessDate) + "\n");
        long timeout = 0L;
        if(CacheList.getRequestByID(super.id) != null)
            timeout = super.accessDate + CacheList.getMaxCacheAge();
        else
            timeout = super.accessDate + RequestList.getMaxCacheAge();
        timeout -= System.currentTimeMillis();
        timeout /= 1000L;
        buffer.append("\tExpires in: " + timeout + " seconds (~ " + timeout / 60L + " minutes)\n");
        buffer.append("\tReference count: " + super.refCount + "\n");
        buffer.append("\tRequest Text: " + super.request + "\n");
        buffer.append("\tUnique id: " + super.id + "\n");
        buffer.append("\tStatus Text: " + super.statusText + "\n");
        buffer.append("\tStatus: " + super.status + "\n");
        buffer.append("\tIs Valid?: " + super.valid);
        return buffer.toString();
    }
}
