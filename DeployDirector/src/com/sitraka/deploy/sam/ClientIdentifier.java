// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ClientIdentifier.java

package com.sitraka.deploy.sam;

import javax.servlet.http.HttpServletRequest;

public class ClientIdentifier
{

    public ClientIdentifier()
    {
    }

    public static String clientTypeToName(int type)
    {
        String name = "";
        switch(type)
        {
        case 0: // '\0'
            name = "DeployCam";
            break;

        case 1: // '\001'
            name = "DeploySam";
            break;

        case 2: // '\002'
            name = "Netscape Communicator/Navigator";
            break;

        case 3: // '\003'
            name = "Microsoft Internet Explorer";
            break;

        case 4: // '\004'
            name = "Lynx Text Browser";
            break;

        case 5: // '\005'
            name = "AOL Custom Browser";
            break;

        case 6: // '\006'
            name = "Opera";
            break;

        case 7: // '\007'
            name = "Mosaic";
            break;

        case -1: 
        default:
            name = "Unknown";
            break;
        }
        return name;
    }

    public static int identifyClientType(Object request)
    {
        HttpServletRequest req = (HttpServletRequest)request;
        String client_name = req.getHeader("User-Agent");
        return identifyClientType(client_name);
    }

    public static int identifyClientType(String client_name)
    {
        int client = -1;
        if(client_name.indexOf("MSIE") != -1)
            client = 3;
        else
        if(client_name.indexOf("Internet Explorer") != -1)
            client = 3;
        else
        if(client_name.indexOf("Lynx") != -1)
            client = 4;
        else
        if(client_name.indexOf("AOL") != -1)
            client = 5;
        else
        if(client_name.indexOf("Opera") != -1)
            client = 6;
        else
        if(client_name.indexOf("Mosaic") != -1)
            client = 7;
        else
        if(client_name.indexOf("DeployCam") != -1)
            client = 0;
        else
        if(client_name.indexOf("DeploySam") != -1)
            client = 1;
        else
        if(client_name.indexOf("Mozilla") != -1)
            client = 2;
        return client;
    }

    public static String identifyClientVersion(Object request)
    {
        HttpServletRequest req = (HttpServletRequest)request;
        String agent = req.getHeader("User-Agent");
        int client_type = identifyClientType(agent);
        return identifyClientVersion(agent, client_type);
    }

    public static String identifyClientVersion(String agent, int client_type)
    {
        String version = null;
        int index = 0;
        switch(client_type)
        {
        case 0: // '\0'
        case 1: // '\001'
        case 2: // '\002'
            index = agent.indexOf("/");
            version = agent.substring(index + 1);
            index = version.indexOf(" ");
            version = version.substring(0, index);
            break;

        case 3: // '\003'
            index = agent.indexOf("MSIE");
            version = agent.substring(index + 5);
            index = version.indexOf(";");
            version = version.substring(0, index);
            break;

        case 6: // '\006'
            index = agent.indexOf("Opera");
            version = agent.substring(index + 5);
            index = version.indexOf(" ");
            version = version.substring(0, index);
            break;

        case 5: // '\005'
            index = agent.indexOf("AOL");
            version = agent.substring(index + 3);
            index = version.indexOf(" ");
            version = version.substring(0, index);
            break;

        case 4: // '\004'
        default:
            version = "";
            break;
        }
        return version;
    }

    public static int identifyClientOS(Object request)
    {
        HttpServletRequest req = (HttpServletRequest)request;
        String agent = req.getHeader("User-Agent");
        return identifyClientOS(agent);
    }

    public static int identifyClientOS(String agent)
    {
        int os = 0;
        agent = agent.toLowerCase().trim();
        if(agent.indexOf("x11") != -1)
            os = 3;
        else
        if(agent.indexOf("mac") != -1)
            os = 2;
        else
        if(agent.indexOf("windows") != -1)
            os = 1;
        else
        if(agent.indexOf("win") != -1)
            os = 1;
        else
            os = 0;
        return os;
    }
}
