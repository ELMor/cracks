// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerEmailConfigBase.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.util.PropertyUtils;
import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport, JSPSupport

public abstract class ServerEmailConfigBase extends JSPPropertiesSupport
{

    protected String username;
    protected String origPassword;
    protected String password;
    protected String server;

    public ServerEmailConfigBase()
    {
        username = null;
        origPassword = null;
        password = null;
        server = null;
    }

    public void refresh()
    {
        super.refresh();
        username = null;
        password = null;
        server = null;
    }

    public boolean validate()
    {
        ((JSPPropertiesSupport)this).initializeProperties(false);
        ((JSPSupport)this).errMessage = "";
        boolean valid = true;
        if(server == null || server.trim().length() == 0)
        {
            ((JSPSupport)this).errMessage = "You must supply an originating server.<br>\n";
            valid = false;
        }
        if(username == null || username.trim().length() == 0)
        {
            ((JSPSupport)this).errMessage += "You must supply a source username.<br>\n";
            valid = false;
        }
        if(password == null || password.trim().length() == 0)
            super.properties.setProperty("deploy.error.email.password", "");
        else
        if(!password.equals(((Object) (origPassword))))
            super.properties.setProperty("deploy.error.email.password", PropertyUtils.writePassword(password));
        super.properties.setProperty("deploy.error.email.server", server);
        super.properties.setProperty("deploy.error.email.user", username);
        return ((JSPPropertiesSupport)this).storeProperties();
    }

    protected void getExistingValues()
    {
        if(username == null)
            username = super.properties.getProperty("deploy.error.email.user", "");
        if(password == null)
        {
            password = super.properties.getProperty("deploy.error.email.password", "");
            origPassword = password;
        }
        if(server == null)
            server = super.properties.getProperty("deploy.error.email.server", "");
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getUsername()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return username;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return password;
    }

    public void setServer(String server)
    {
        this.server = server;
    }

    public String getServer()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return server;
    }
}
