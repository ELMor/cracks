// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LicenseEnter.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.util.FileUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPSupport, LicenseWorker

public class LicenseEnter extends JSPSupport
{

    protected String newLicense;

    public LicenseEnter()
    {
    }

    public void refresh()
    {
        super.refresh();
        newLicense = null;
    }

    public boolean validate()
    {
        boolean valid = true;
        File tempFile = FileUtils.createTempFile();
        PrintWriter printwriter = null;
        try
        {
            printwriter = new PrintWriter(((java.io.OutputStream) (new FileOutputStream(tempFile))));
        }
        catch(IOException ioe)
        {
            super.errMessage = "Could not create output stream to write file.";
            return false;
        }
        System.err.println("Validating new license:\n" + newLicense);
        printwriter.print(newLicense);
        printwriter.flush();
        boolean failed = printwriter.checkError();
        printwriter.close();
        if(failed)
        {
            super.errMessage = "Could not write to temporary file \"" + tempFile.getAbsolutePath() + "\"<br>";
            FileUtils.deleteDirectory(tempFile);
            return false;
        }
        LicenseWorker lw = new LicenseWorker();
        lw.refresh();
        lw.setFile(tempFile.getAbsolutePath());
        if(!lw.validate())
        {
            super.errMessage = ((JSPSupport) (lw)).getErrMessage();
            valid = false;
        }
        FileUtils.deleteDirectory(tempFile);
        return valid;
    }

    public void setNewLicense(String license_data)
    {
        newLicense = license_data;
    }

    public String getNewLicense()
    {
        return newLicense;
    }
}
