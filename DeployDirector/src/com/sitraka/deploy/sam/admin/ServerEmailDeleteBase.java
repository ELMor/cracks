// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerEmailDeleteBase.java

package com.sitraka.deploy.sam.admin;

import java.util.Hashtable;
import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport, JSPSupport

public abstract class ServerEmailDeleteBase extends JSPPropertiesSupport
{

    protected String removedAddress;

    public ServerEmailDeleteBase()
    {
        removedAddress = null;
    }

    public void refresh()
    {
        super.refresh();
        removedAddress = null;
    }

    public boolean validate()
    {
        return true;
    }

    protected void getExistingValues()
    {
    }

    public void removeAddress(String address)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        int index = -1;
        String prop_base = "deploy.error.email.";
        String prop_value = null;
        do
        {
            index++;
            String prop_name = prop_base + index + ".address";
            prop_value = super.properties.getProperty(prop_name);
        } while(prop_value != null && !prop_value.equals(((Object) (address))));
        if(prop_value == null)
        {
            ((JSPSupport)this).errMessage = "Unable to find recipient \"" + address + "\"";
            return;
        }
        for(String prop_name = prop_base + (index + 1) + ".address"; super.properties.getProperty(prop_name) != null; prop_name = prop_base + (index + 1) + ".address")
        {
            super.properties.setProperty(prop_base + index + ".address", super.properties.getProperty(prop_base + (index + 1) + ".address"));
            super.properties.setProperty(prop_base + index + ".notification", super.properties.getProperty(prop_base + (index + 1) + ".notification"));
            index++;
        }

        ((Hashtable) (super.properties)).remove(((Object) (prop_base + index + ".address")));
        ((Hashtable) (super.properties)).remove(((Object) (prop_base + index + ".notification")));
        removedAddress = address;
        ((JSPPropertiesSupport)this).storeProperties();
    }

    public String getRemovedAddress()
    {
        return removedAddress;
    }
}
