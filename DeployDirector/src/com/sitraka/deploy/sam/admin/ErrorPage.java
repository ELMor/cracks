// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ErrorPage.java

package com.sitraka.deploy.sam.admin;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.Part;
import com.sitraka.deploy.common.TemplateText;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Properties;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport, JSPSupport

public class ErrorPage extends JSPPropertiesSupport
{

    protected File errorFile;

    public ErrorPage()
    {
    }

    public void refresh()
    {
        super.refresh();
        errorFile = null;
    }

    public boolean validate()
    {
        return true;
    }

    public boolean loadErrorPage(HttpServletRequest request)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        MultipartParser parser = null;
        try
        {
            parser = new MultipartParser(request, 0x400000);
        }
        catch(IOException ioe)
        {
            ((JSPSupport)this).errMessage = "Error creating parser:\n" + ((Throwable) (ioe)).getMessage();
            System.err.println(((JSPSupport)this).errMessage);
            return false;
        }
        File temp_dir = FileUtils.createTempFile();
        temp_dir.mkdir();
        Part form_part = null;
        boolean found_file = false;
        try
        {
            while((form_part = parser.readNextPart()) != null) 
            {
                String name = form_part.getName();
                if(form_part.isParam())
                {
                    System.err.println("Unrecognised parameter: name = " + name);
                    continue;
                }
                if(!form_part.isFile())
                    continue;
                FilePart part = (FilePart)form_part;
                String file_name = part.getFileName();
                if(file_name == null || file_name.length() == 0)
                    continue;
                try
                {
                    part.writeTo(temp_dir);
                }
                catch(IOException ioe)
                {
                    ((JSPSupport)this).errMessage = "Error reading inputStream: \n" + ((Throwable) (ioe)).getMessage();
                    System.err.println(((JSPSupport)this).errMessage);
                    continue;
                }
                errorFile = new File(temp_dir, file_name);
                found_file = true;
            }
        }
        catch(IOException ioe)
        {
            ((JSPSupport)this).errMessage = "Error reading multipart data: \n" + ((Throwable) (ioe)).getMessage();
            return false;
        }
        if(!found_file)
        {
            ((JSPSupport)this).errMessage = "No error page found in upload.";
            return false;
        } else
        {
            boolean valid = true;
            AppVault vault = ((JSPSupport)this).getServlet().getVault();
            File custom_file = new File(vault.getServerEtcDir(), "custom.html");
            FileUtils.copyFiles(errorFile, custom_file, true);
            super.properties.setProperty("deploy.error.page", "$(VAULTDIR)/etc/custom.html");
            boolean status = ((JSPPropertiesSupport)this).storeProperties();
            errorFile.delete();
            temp_dir.delete();
            return status;
        }
    }

    protected void getExistingValues()
    {
        String error_name = super.properties.getProperty("deploy.error.page", "");
        if(error_name.trim().length() == 0)
            errorFile = null;
        else
            errorFile = new File(error_name);
    }

    protected String getPropFile()
    {
        return "server.properties";
    }

    public void setImportFile(String infile)
    {
        errorFile = new File(infile);
    }

    public String getImportFile()
    {
        if(errorFile == null)
            return "unknown";
        else
            return errorFile.getName();
    }

    public String getSamplePage(HttpServletRequest request)
    {
        String servlet = request.getContextPath() + request.getServletPath();
        String base = servlet.substring(0, servlet.lastIndexOf("admin")) + "deploy";
        String servletURL = ((ServletRequest) (request)).getScheme() + "://" + ((ServletRequest) (request)).getServerName() + ":" + ((ServletRequest) (request)).getServerPort() + base;
        String content = null;
        String default_text = "<FONT color=\"red\" size=+1><b>Error loading error page</b></font>\n<hr>\n<H2>DeploySam - Error</H2>\n<FONT color=\"red\">Error description will appear here</font>";
        ((JSPPropertiesSupport)this).initializeProperties();
        String error_name = super.properties.getProperty("deploy.error.page", "");
        String VAULTDIR = "$(VAULTDIR)";
        int index = error_name.indexOf(VAULTDIR);
        if(index >= 0)
        {
            File vault_dir = ((JSPSupport)this).getServlet().getVault().getVaultBaseDir();
            String dir_name = vault_dir.getAbsolutePath();
            error_name = error_name.substring(0, index) + dir_name + error_name.substring(index + VAULTDIR.length());
        }
        if(error_name.length() > 0)
        {
            File error_file = new File(error_name);
            if(!error_file.canRead())
                content = default_text;
            else
                try
                {
                    TemplateText prep = new TemplateText(error_name);
                    prep.setPlaceHolder("SERVLET NAME", "DeploySam");
                    prep.setPlaceHolder("ERROR TEXT", "Error description will appear here.");
                    prep.setPlaceHolder("LOGO", ((Object) (servletURL + "/" + "deploydirector.gif")));
                    prep.setPlaceHolder("LOGO_SS", ((Object) (servletURL + "/" + "deploydirector_ss.gif")));
                    prep.setPlaceHolder("URL", "http://www.sitraka.com/software/deploydirector");
                    String req = "<tt>\nproto://server.name:port/path/to/servlet/deploy<BR>NAME='PARAM1' VALUE='PARAM_VAL1'\n<BR>NAME='PARAM2' VALUE='PARAM_VAL2'\n<BR>...\n</tt>\n";
                    prep.setPlaceHolder("REQUEST", ((Object) (req)));
                    content = prep.toString();
                    index = content.toLowerCase().indexOf("<body");
                    index = content.indexOf(">", index);
                    content = content.substring(index + 1);
                    index = content.toLowerCase().lastIndexOf("</body>");
                    content = content.substring(0, index);
                }
                catch(IOException e)
                {
                    content = default_text;
                }
        } else
        {
            content = default_text;
        }
        return content;
    }

    public boolean restoreDefault()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        AppVault vault = ((JSPSupport)this).getServlet().getVault();
        File error_file = new File(vault.getServerEtcDir(), "error.html");
        if(!error_file.exists() || !error_file.canRead())
        {
            return false;
        } else
        {
            super.properties.setProperty("deploy.error.page", "$(VAULTDIR)/etc/error.html");
            return ((JSPPropertiesSupport)this).storeProperties();
        }
    }
}
