// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BundleExport2.java

package com.sitraka.deploy.sam.admin;


// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPSupport

public class BundleExport2 extends JSPSupport
{

    protected String version;
    protected String exportFile;

    public BundleExport2()
    {
    }

    public void refresh()
    {
        super.refresh();
        version = null;
        exportFile = null;
    }

    public boolean validate()
    {
        boolean valid = true;
        if(version == null)
        {
            super.errMessage = "You must select a version to continue<br>";
            valid = false;
        }
        if(exportFile == null)
        {
            if(super.errMessage != null)
                super.errMessage += "You must also specify a destination DAR file<br>";
            else
                super.errMessage = "You must specify a destination DAR file<br>";
            valid = false;
        }
        return valid;
    }

    public void setVersion(String name)
    {
        version = name;
    }

    public String getVersion()
    {
        return version;
    }

    public void setExportFile(String filename)
    {
        exportFile = filename;
    }

    public String getExportFile()
    {
        return exportFile;
    }
}
