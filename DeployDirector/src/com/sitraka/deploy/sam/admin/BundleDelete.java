// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BundleDelete.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.common.checkpoint.ApplicationObject;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.sam.worker.AbstractWorker;
import com.sitraka.deploy.sam.worker.ReplicationThread;
import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPSupport

public class BundleDelete extends JSPSupport
{

    protected String bundle;

    public BundleDelete()
    {
        bundle = null;
    }

    public void refresh()
    {
        super.refresh();
        bundle = null;
    }

    public boolean validate()
    {
        if(bundle == null)
        {
            super.errMessage = "Bundle name not set, cannot continue<br>";
            return false;
        }
        if(((JSPSupport)this).getServlet() == null)
            return false;
        AppVault vault = ((JSPSupport)this).getServlet().getVault();
        if(!vault.deleteApplication(bundle))
        {
            super.errMessage = "Vault cannot remove the bundle<br>Check the server log for more details<br>";
            return false;
        }
        if(vault.listOtherServers() != null)
        {
            String localhost = Servlet.properties.getProperty("deploy.localhost");
            ReplicationThread rt = AbstractWorker.getReplicationQueue(((com.sitraka.deploy.sam.ServerActions) (((JSPSupport)this).getServlet())));
            ApplicationObject app_obj = new ApplicationObject(localhost, bundle);
            rt.sendApplicationDeletion(localhost, app_obj, ((Object) (super.authData)));
        }
        return true;
    }

    public void setBundle(String name)
    {
        bundle = name;
    }

    public String getBundle()
    {
        return bundle;
    }
}
