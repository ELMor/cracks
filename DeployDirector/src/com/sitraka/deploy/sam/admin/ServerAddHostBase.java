// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerAddHostBase.java

package com.sitraka.deploy.sam.admin;

import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport, JSPSupport

public abstract class ServerAddHostBase extends JSPPropertiesSupport
{

    protected int hostToAdd;
    protected String host;
    protected String accessProtocol;
    protected String accessPort;
    protected String rootPage;

    public ServerAddHostBase()
    {
        hostToAdd = -1;
        host = null;
        accessProtocol = null;
        accessPort = null;
        rootPage = null;
    }

    public void refresh()
    {
        super.refresh();
        host = null;
        accessProtocol = null;
        accessPort = null;
        rootPage = null;
        hostToAdd = -1;
    }

    public boolean validate()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        if(hostToAdd == -1)
            getExistingValues();
        if(isExistingHost(host))
        {
            ((JSPSupport)this).errMessage = "Host \"" + host + "\" already exists.";
            return false;
        } else
        {
            String base_name = "deploy." + getGroupName() + ".host." + hostToAdd;
            super.properties.setProperty(base_name + ".machine", host);
            super.properties.setProperty(base_name + ".page", rootPage);
            super.properties.setProperty(base_name + ".port", accessPort);
            super.properties.setProperty(base_name + ".protocol", accessProtocol);
            return ((JSPPropertiesSupport)this).storeProperties();
        }
    }

    protected void getExistingValues()
    {
        String prop_base = "deploy." + getGroupName() + ".host.";
        hostToAdd = 0;
        String prop_name = prop_base + hostToAdd + ".machine";
        for(String prop_value = super.properties.getProperty(prop_name); prop_value != null && prop_value.length() != 0; prop_value = super.properties.getProperty(prop_name))
        {
            hostToAdd++;
            prop_name = prop_base + hostToAdd + ".machine";
        }

    }

    protected String getPropFile()
    {
        return "cluster.properties";
    }

    protected boolean isExistingHost(String newHost)
    {
        String host_part = newHost;
        if(newHost.indexOf(".") != -1)
            host_part = newHost.substring(0, newHost.indexOf("."));
        int index = 0;
        String prop_base = "deploy." + getGroupName() + ".host.";
        String host_prop = prop_base + index + ".machine";
        for(String old_host = super.properties.getProperty(host_prop); old_host != null && old_host.length() != 0; old_host = super.properties.getProperty(host_prop))
        {
            if(old_host.startsWith(host_part))
            {
                if(old_host.indexOf(".") == -1 || newHost.indexOf(".") == -1)
                    return true;
                if(old_host.equals(((Object) (newHost))))
                    return true;
            }
            index++;
            host_prop = prop_base + index + ".machine";
        }

        return false;
    }

    protected abstract String getGroupName();

    public void setHost(String name)
    {
        host = name;
    }

    public String getHost()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return host;
    }

    public void setAccessProtocol(String protocol)
    {
        accessProtocol = protocol;
    }

    public String getAccessProtocol()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return accessProtocol;
    }

    public void setAccessPort(String port)
    {
        accessPort = port;
    }

    public String getAccessPort()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return accessPort;
    }

    public void setRootPage(String page)
    {
        rootPage = page;
    }

    public String getRootPage()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return rootPage;
    }
}
