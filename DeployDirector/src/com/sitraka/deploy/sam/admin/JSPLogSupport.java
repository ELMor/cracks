// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JSPLogSupport.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.servlet.Servlet;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class JSPLogSupport
{

    protected String errMessage;
    protected String logName;

    public JSPLogSupport()
    {
        errMessage = null;
        logName = null;
    }

    public void refresh()
    {
        errMessage = null;
    }

    public boolean validate()
    {
        return true;
    }

    public String getErrMessage()
    {
        return errMessage;
    }

    public String getServerLog()
    {
        if(logName == null)
        {
            errMessage = "The name of the log file must be specified.";
            return errMessage;
        }
        Servlet servlet = Servlet.getUniqueInstance();
        if(servlet == null)
        {
            errMessage = "Unable to find or contact the servlet.";
            return errMessage;
        }
        File vaultBase = servlet.getVault().getVaultBaseDir();
        File logDir = Servlet.getLogFileDir(vaultBase);
        File serverLog = new File(logDir, logName);
        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(((java.io.Reader) (new FileReader(serverLog))));
        }
        catch(FileNotFoundException fnfe)
        {
            errMessage = "Unable to find " + logName + " file \"" + serverLog.getAbsolutePath() + "\"<br>\n" + ((Throwable) (fnfe)).getMessage() + "<br>";
            return errMessage;
        }
        String out_data = "";
        String s;
        try
        {
            while((s = reader.readLine()) != null) 
                out_data = out_data + s + "\n";
        }
        catch(IOException ioe)
        {
            errMessage = "Error reading from " + logName + " file \"" + serverLog.getAbsolutePath() + "\"<br>\n" + ((Throwable) (ioe)).getMessage() + "<br>";
            if(out_data.length() > 0)
                return out_data + "<br>\n" + errMessage;
            else
                return errMessage;
        }
        return out_data;
    }
}
