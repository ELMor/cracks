// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerEmailAddEditBase.java

package com.sitraka.deploy.sam.admin;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport, EmailEntry, BundleInfo

public abstract class ServerEmailAddEditBase extends JSPPropertiesSupport
{

    protected String address;
    protected Hashtable errorLevels;
    protected String levelList;
    protected String otherList;
    protected static int LEVEL_COUNT = 5;
    protected static String LEVEL_NAMES[][] = {
        {
            "client.local", "Local client (CAM) events"
        }, {
            "client.connection", "Client connections to server"
        }, {
            "server.local", "Server events"
        }, {
            "server.connection", "Inter-server connections"
        }, {
            "bundle.all", "All client bundle events"
        }
    };

    public ServerEmailAddEditBase()
    {
        address = null;
        errorLevels = new Hashtable();
        levelList = null;
        otherList = null;
    }

    public void refresh()
    {
        super.refresh();
        address = null;
        levelList = null;
        otherList = null;
        errorLevels.clear();
        Vector levels = getFieldList();
        for(int i = 0; i < levels.size(); i++)
        {
            EmailEntry level_entry = (EmailEntry)levels.elementAt(i);
            errorLevels.put(((Object) (level_entry.getName())), ((Object) (Boolean.FALSE)));
        }

    }

    public void processParameters(HttpServletRequest request)
    {
        for(Enumeration params = ((ServletRequest) (request)).getParameterNames(); params.hasMoreElements();)
        {
            String name = (String)params.nextElement();
            if(!name.startsWith("formButton"))
                if(name.equals("address"))
                    setAddress(((ServletRequest) (request)).getParameter("address"));
                else
                if(name.equals("otherlevels"))
                    setOtherLevels(((ServletRequest) (request)).getParameter("otherlevels"));
                else
                    errorLevels.put(((Object) (name)), ((Object) (Boolean.TRUE)));
        }

    }

    public void setAddress(String recipient)
    {
        address = recipient;
    }

    public String getAddress()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return address;
    }

    public void setNotificationLevel(String level, boolean report)
    {
        errorLevels.put(((Object) (level)), ((Object) (report ? ((Object) (Boolean.TRUE)) : ((Object) (Boolean.FALSE)))));
    }

    public boolean isNotificationLevelSet(String level)
    {
        return errorLevels.get(((Object) (level))) == Boolean.TRUE;
    }

    public void setOtherLevels(String otherLevels)
    {
        otherList = otherLevels;
        Hashtable temp_levels = getNotificationLevels(otherLevels);
        Object entry;
        for(Enumeration level_enum = temp_levels.keys(); level_enum.hasMoreElements(); errorLevels.put(entry, temp_levels.get(entry)))
            entry = level_enum.nextElement();

    }

    public String getOtherLevels()
    {
        Vector base_levels = getFieldNames();
        StringBuffer level_list = new StringBuffer();
        for(Enumeration added_set = errorLevels.keys(); added_set.hasMoreElements();)
        {
            String added_level = (String)added_set.nextElement();
            if(!base_levels.contains(((Object) (added_level))))
            {
                if(level_list.length() != 0)
                    level_list.append(",");
                level_list.append(added_level);
            }
        }

        return level_list.toString();
    }

    public static String getNotificationString(Hashtable levels)
    {
        String notification = null;
        for(Enumeration level_list = levels.keys(); level_list.hasMoreElements();)
        {
            String level = (String)level_list.nextElement();
            boolean is_set = (Boolean)levels.get(((Object) (level))) == Boolean.TRUE;
            if(is_set)
                if(notification == null)
                    notification = level;
                else
                    notification = notification + "," + level;
        }

        return notification;
    }

    public static Hashtable getNotificationLevels(String levelNames)
    {
        if(levelNames == null)
            levelNames = "";
        Hashtable notificationFlags = new Hashtable();
        String level;
        for(StringTokenizer st = new StringTokenizer(levelNames, " ,\t\n\r"); st.hasMoreTokens(); notificationFlags.put(((Object) (level)), ((Object) (Boolean.TRUE))))
            level = st.nextToken();

        return notificationFlags;
    }

    public static Vector getFieldList()
    {
        Vector categories = new Vector();
        for(int i = 0; i < LEVEL_COUNT; i++)
            categories.addElement(((Object) (new EmailEntry(LEVEL_NAMES[i][0], LEVEL_NAMES[i][1]))));

        Vector bundles = (new BundleInfo()).getBundleListVector();
        for(int i = 0; i < bundles.size(); i++)
        {
            String bundle_name = (String)bundles.elementAt(i);
            String bundle_label = "Bundle " + bundle_name;
            bundle_name = "bundle." + bundle_name;
            categories.addElement(((Object) (new EmailEntry(bundle_name, bundle_label))));
        }

        return categories;
    }

    public static Vector getFieldNames()
    {
        Vector names = new Vector();
        for(int i = 0; i < LEVEL_COUNT; i++)
            names.addElement(((Object) (LEVEL_NAMES[i][0])));

        Vector bundles = (new BundleInfo()).getBundleListVector();
        for(int i = 0; i < bundles.size(); i++)
        {
            String bundle_name = "bundle." + (String)bundles.elementAt(i);
            names.addElement(((Object) (bundle_name)));
        }

        return names;
    }

}
