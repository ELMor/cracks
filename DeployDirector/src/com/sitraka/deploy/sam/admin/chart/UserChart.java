// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   UserChart.java

package com.sitraka.deploy.sam.admin.chart;

import com.klg.jclass.chart.ChartDataView;
import com.klg.jclass.chart.JCChart;
import com.klg.jclass.chart.data.JCDefaultDataSource;
import com.klg.jclass.schart.JCServerChart;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.LogQueue;
import com.sitraka.deploy.sam.log.ClientDataLogger;
import com.sitraka.deploy.sam.log.LogException;
import com.sitraka.deploy.sam.servlet.Servlet;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// Referenced classes of package com.sitraka.deploy.sam.admin.chart:
//            AbstractChart, ChartException

public class UserChart extends AbstractChart
{

    public UserChart(HttpServletRequest request, HttpServletResponse response, String extension)
        throws ChartException
    {
        super(request, response, extension, "user.xml");
        ((AbstractChart)this).extractBundleList();
    }

    public void generateChart()
        throws ChartException
    {
        String bundles[] = ((AbstractChart)this).getBundleList();
        if(bundles == null)
        {
            AppVault vault = Servlet.getUniqueInstance().getVault();
            bundles = vault.listBundles();
        }
        double users[][] = new double[1][bundles.length];
        JCDefaultDataSource ds = null;
        try
        {
            ClientDataLogger logger = Servlet.getUniqueInstance().getLogQueue().getClientDataLogger();
            for(int i = 0; i < bundles.length; i++)
                users[0][i] = logger.queryUsersForBundle(bundles[i], super.startTimestamp, super.endTimestamp);

            ds = new JCDefaultDataSource(((double [][]) (null)), users, bundles, bundles, "Bundle Users Source");
        }
        catch(LogException le)
        {
            throw new ChartException("Chart Error: Reading Logs: " + ((Throwable) (le)).getMessage(), 500);
        }
        ((JCChart) (super.chart)).getDataView(0).setDataSource(((com.klg.jclass.chart.ChartDataModel) (ds)));
        try
        {
            if(super.extension == null)
            {
                ((ServletResponse) (super.response)).setContentType("image/png");
                super.chart.encodeAsPNG(((OutputStream) (((ServletResponse) (super.response)).getOutputStream())));
            } else
            if("imagemap".equals(((Object) (super.extension))))
                throw new ChartException("Chart Error: Not Implemented", 501);
            ((OutputStream) (((ServletResponse) (super.response)).getOutputStream())).flush();
        }
        catch(IOException ioe)
        {
            throw new ChartException("Chart Error: IOException generating chart.", 500);
        }
    }
}
