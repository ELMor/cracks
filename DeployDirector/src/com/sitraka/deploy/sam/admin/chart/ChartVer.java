// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ChartVer.java

package com.sitraka.deploy.sam.admin.chart;

import com.klg.jclass.util.internal.OEMVerify;

public class ChartVer
    implements OEMVerify
{

    public ChartVer()
    {
    }

    public int isOKToRun(long tval)
    {
        long root;
        for(root = 1L; root * root <= tval; root += 10L);
        for(; root * root > tval; root--);
        long sum = 0L;
        int i = 2;
        for(int count = 1; count < 12 && (long)i <= root; i++)
            if(tval % (long)i == 0L)
            {
                sum += i;
                count++;
            }

        int retval = sum != 0L ? (int)sum : (int)(tval % 100L + 19L);
        return retval;
    }
}
