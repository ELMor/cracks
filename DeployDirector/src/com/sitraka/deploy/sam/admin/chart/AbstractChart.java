// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AbstractChart.java

package com.sitraka.deploy.sam.admin.chart;

import com.klg.jclass.schart.JCServerChart;
import com.klg.jclass.schart.JCServerChartFactory;
import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.log.LogStats;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.util.PropertyUtils;
import java.awt.Component;
import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Vector;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// Referenced classes of package com.sitraka.deploy.sam.admin.chart:
//            ChartException, Chart

public abstract class AbstractChart
{

    public static final int XSIZE_DEFAULT = 800;
    public static final int YSIZE_DEFAULT = 600;
    protected int xSize;
    protected int ySize;
    protected boolean xSizeDefined;
    protected boolean ySizeDefined;
    public static final long STARTDATE_DEFAULT = 0L;
    protected long startDate;
    protected long endDate;
    protected Timestamp startTimestamp;
    protected Timestamp endTimestamp;
    protected boolean licensed;
    protected String bundleList[];
    protected String versionList[];
    protected String eventList[];
    protected Vector versionListList;
    protected String toVersionList[];
    protected String fromVersionList[];
    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected String extension;
    protected JCServerChart chart;

    public AbstractChart(HttpServletRequest request, HttpServletResponse response, String extension, String xmlFilename)
        throws ChartException
    {
        xSize = 800;
        ySize = 600;
        xSizeDefined = false;
        ySizeDefined = false;
        startDate = 0L;
        endDate = System.currentTimeMillis();
        licensed = false;
        bundleList = null;
        versionList = null;
        eventList = null;
        versionListList = null;
        toVersionList = null;
        fromVersionList = null;
        this.request = null;
        this.response = null;
        this.extension = null;
        chart = null;
        this.request = request;
        this.response = response;
        this.extension = extension;
        initializeChart(xmlFilename);
        extractXSize();
        extractYSize();
        extractStartDate();
        extractEndDate();
        extractLicensed();
        setupChartSize();
        setupTimestamp();
    }

    protected void initializeChart(String xmlFilename)
        throws ChartException
    {
        File chartFile = new File(Chart.getChartDir(), xmlFilename);
        try
        {
            FileInputStream chartInput = new FileInputStream(chartFile);
            chart = JCServerChartFactory.makeServerChartFromStream(((java.io.InputStream) (chartInput)), ((javax.servlet.ServletContext) (null)), "", 1);
        }
        catch(IOException ioe)
        {
            throw new ChartException("Chart Error: File error with: " + chartFile.getAbsoluteFile() + " Message: " + ((Throwable) (ioe)).getMessage(), 500);
        }
        if(chart == null)
            throw new ChartException("Chart Error: Unable to create chart.", 500);
        else
            return;
    }

    public abstract void generateChart()
        throws ChartException;

    protected void extractXSize()
    {
        String size = ((ServletRequest) (request)).getParameter("XSIZE");
        try
        {
            xSize = Integer.parseInt(size);
            xSizeDefined = true;
        }
        catch(NumberFormatException e)
        {
            xSize = 800;
            xSizeDefined = false;
        }
    }

    protected void extractYSize()
    {
        String size = ((ServletRequest) (request)).getParameter("YSIZE");
        try
        {
            ySize = Integer.parseInt(size);
            ySizeDefined = true;
        }
        catch(NumberFormatException e)
        {
            ySize = 800;
            ySizeDefined = false;
        }
    }

    protected void extractStartDate()
    {
        String date = ((ServletRequest) (request)).getParameter("STARTDATE");
        if(date == null)
            startDate = 0L;
        else
            startDate = PropertyUtils.parseLongDate(date);
    }

    protected void extractEndDate()
    {
        String date = ((ServletRequest) (request)).getParameter("ENDDATE");
        if(date == null)
            endDate = System.currentTimeMillis();
        else
            endDate = PropertyUtils.parseLongDate(date);
    }

    protected void extractLicensed()
    {
        String lic = ((ServletRequest) (request)).getParameter("LICENSED");
        if(lic == null)
            licensed = false;
        else
        if(lic.equalsIgnoreCase("true"))
            licensed = true;
        else
            licensed = false;
    }

    protected void extractBundle()
    {
        String bundle = ((ServletRequest) (request)).getParameter("BUNDLE");
        if(bundle == null)
            bundleList = null;
        else
            bundleList = (new String[] {
                bundle
            });
    }

    protected void extractBundleList()
    {
        bundleList = ((ServletRequest) (request)).getParameterValues("BUNDLE");
        if(bundleList == null)
        {
            AppVault vault = Servlet.getUniqueInstance().getVault();
            bundleList = vault.listBundles();
        }
    }

    protected void extractVersionList()
    {
        versionList = ((ServletRequest) (request)).getParameterValues("VERSION");
        if(bundleList != null && versionList == null)
        {
            AppVault vault = Servlet.getUniqueInstance().getVault();
            Vector list = vault.getApplication(bundleList[0]).listVersions();
            versionList = new String[list.size()];
            for(int i = 0; i < versionList.length; i++)
                versionList[i] = ((Version)list.elementAt(i)).getName();

        }
    }

    protected void extractEventList()
    {
        eventList = ((ServletRequest) (request)).getParameterValues("EVENT");
    }

    protected void extractToVersion()
    {
        String ver = ((ServletRequest) (request)).getParameter("TOVERSION");
        if(ver == null)
            toVersionList = null;
        else
            toVersionList = (new String[] {
                ver
            });
    }

    protected void extractFromVersion()
    {
        String ver = ((ServletRequest) (request)).getParameter("FROMVERSION");
        if(ver == null)
            fromVersionList = null;
        else
            fromVersionList = (new String[] {
                ver
            });
    }

    public void setupChartSize()
    {
        int x = 800;
        int y = 600;
        if(xSizeDefined)
            x = xSize;
        else
        if(((Component) (chart)).getSize().width > 0)
            x = ((Component) (chart)).getSize().width;
        if(ySizeDefined)
            y = ySize;
        else
        if(((Component) (chart)).getSize().height > 0)
            y = ((Component) (chart)).getSize().height;
        ((Component) (chart)).setSize(x, y);
    }

    public void setupTimestamp()
    {
        if(licensed)
        {
            startTimestamp = new Timestamp(System.currentTimeMillis() - 0x1cf7c5800L);
            endTimestamp = new Timestamp(System.currentTimeMillis());
        } else
        {
            startTimestamp = new Timestamp(startDate);
            endTimestamp = new Timestamp(endDate);
        }
    }

    protected void fillEventCountArray(LogStats logStats, String namesArray[], double countArray[][])
    {
        Vector eventCounts = logStats.getCount();
        Vector eventNames = logStats.getEvents();
        for(int i = 0; i < countArray[0].length; i++)
        {
            countArray[0][i] = ((Integer)eventCounts.elementAt(i)).intValue();
            namesArray[i] = (String)eventNames.elementAt(i);
        }

    }

    protected void fillLimitedEventCountArray(LogStats logStats, String eventList[], double countArray[][])
    {
        Vector eventCounts = logStats.getCount();
        Vector eventNames = logStats.getEvents();
        for(int i = 0; i < countArray[0].length; i++)
        {
            int index = eventNames.indexOf(((Object) (eventList[i])));
            if(index == -1)
                countArray[0][i] = 0.0D;
            else
                countArray[0][i] = ((Integer)eventCounts.elementAt(index)).intValue();
        }

    }

    public int getXSize()
    {
        return xSize;
    }

    public void setXSize(int xSize)
    {
        this.xSize = xSize;
    }

    public int getYSize()
    {
        return ySize;
    }

    public void setYSize(int ySize)
    {
        this.ySize = ySize;
    }

    public long getStartDate()
    {
        return startDate;
    }

    public void setStartDate(long startDate)
    {
        this.startDate = startDate;
    }

    public long getEndDate()
    {
        return endDate;
    }

    public void setEndDate(long endDate)
    {
        this.endDate = endDate;
    }

    public boolean isLicensed()
    {
        return licensed;
    }

    public void setLicensed(boolean licensed)
    {
        this.licensed = licensed;
    }

    public String getBundle()
    {
        if(bundleList == null)
            return null;
        if(bundleList.length == 0)
            return null;
        else
            return bundleList[0];
    }

    public String[] getBundleList()
    {
        return bundleList;
    }

    public void setBundleList(String bundleList[])
    {
        this.bundleList = bundleList;
    }

    public String[] getVersionList()
    {
        return versionList;
    }

    public void setVersionList(String versionList[])
    {
        this.versionList = versionList;
    }

    public String[] getEventList()
    {
        return eventList;
    }

    public void setEventList(String eventList[])
    {
        this.eventList = eventList;
    }

    public Vector getVersionListList()
    {
        return versionListList;
    }

    public void setVersionListList(Vector versionListList)
    {
        this.versionListList = versionListList;
    }

    public String[] getToVersionList()
    {
        return toVersionList;
    }

    public void setToVersionList(String toVersionList[])
    {
        this.toVersionList = toVersionList;
    }

    public String[] getFromVersionList()
    {
        return fromVersionList;
    }

    public void setFromVersionList(String fromVersionList[])
    {
        this.fromVersionList = fromVersionList;
    }

    public HttpServletRequest getRequest()
    {
        return request;
    }

    public HttpServletResponse getResponce()
    {
        return response;
    }

    public String getExtension()
    {
        return extension;
    }

    public JCServerChart getChart()
    {
        return chart;
    }
}
