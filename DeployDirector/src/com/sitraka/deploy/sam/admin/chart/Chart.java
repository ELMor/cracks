// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Chart.java

package com.sitraka.deploy.sam.admin.chart;

import com.klg.jclass.schart.property.JCServletStringAccessor;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// Referenced classes of package com.sitraka.deploy.sam.admin.chart:
//            ChartVer, UserChart, UserVersionChart, ClientEventsChart, 
//            ServerEventsChart, ChartException

public class Chart extends HttpServlet
{

    public static final String TYPE = "TYPE";
    public static final String TICKET = "TICKET";
    public static final String XSIZE = "XSIZE";
    public static final String YSIZE = "YSIZE";
    public static final String STARTDATE = "STARTDATE";
    public static final String ENDDATE = "ENDDATE";
    public static final String LICENSED = "LICENSED";
    public static final String BUNDLE = "BUNDLE";
    public static final String VERSION = "VERSION";
    public static final String EVENT = "EVENT";
    public static final String TOVERSION = "TOVERSION";
    public static final String FROMVERSION = "FROMVERSION";
    public static final String TYPE_USER = "user";
    public static final String TYPE_USERVERSION = "userVersion";
    public static final String TYPE_CLIENTSTATS = "clientStats";
    public static final String TYPE_SERVERSTATS = "serverStats";
    public static final String TYPE_DOWNLOAD = "download";
    public static final String TYPE_DOWNLOADBUNDLE = "downloadBundle";
    public static final String TYPE_SERVERUPTIME = "serverUptime";
    public static final String TYPE_SERVERLOAD = "serverLoad";
    public static final String TYPE_TICKET = "ticket";
    public static final String EXTENSION_IMAGEMAP = "imagemap";
    protected static File chartsDir;

    public Chart()
    {
    }

    public void init(ServletConfig config)
        throws ServletException
    {
        JCServletStringAccessor.setObject(((Object) (new ChartVer())));
        String chartsDirString = config.getInitParameter("chartsdir");
        if(chartsDirString == null)
        {
            System.out.println("Chart Error: 'chartdir' parameter not defined.");
            return;
        }
        chartsDir = new File(chartsDirString);
        if(!chartsDir.exists())
        {
            System.out.println("Chart Error: chartsdir: " + chartsDir.getAbsolutePath() + " does not exist");
            return;
        } else
        {
            return;
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
    {
        String type = null;
        String extension = null;
        String pathInfo = request.getPathInfo();
        if(pathInfo == null)
        {
            try
            {
                response.sendError(400, "Chart Error: Missing chart type");
            }
            catch(IOException ioe) { }
            return;
        }
        if(pathInfo.startsWith("/"))
            pathInfo = pathInfo.substring(1);
        if(pathInfo.endsWith("/"))
            pathInfo = pathInfo.substring(0, pathInfo.length() - 1);
        int index = pathInfo.indexOf('/');
        if(index == -1)
        {
            type = pathInfo;
        } else
        {
            type = pathInfo.substring(0, index);
            extension = pathInfo.substring(index + 1, pathInfo.length());
        }
        try
        {
            if(type.equals("user"))
            {
                UserChart chart = new UserChart(request, response, extension);
                chart.generateChart();
            } else
            if(type.equals("userVersion"))
            {
                UserVersionChart chart = new UserVersionChart(request, response, extension);
                chart.generateChart();
            } else
            if(type.equals("clientStats"))
            {
                ClientEventsChart chart = new ClientEventsChart(request, response, extension);
                chart.generateChart();
            } else
            if(type.equals("serverStats"))
            {
                ServerEventsChart chart = new ServerEventsChart(request, response, extension);
                chart.generateChart();
            } else
            if(!type.equals("download") && !type.equals("downloadBundle") && !type.equals("serverUptime") && !type.equals("serverLoad"))
                if(!type.equals("ticket"));
        }
        catch(ChartException ce)
        {
            try
            {
                response.sendError(ce.getStatusCode(), ((Throwable) (ce)).getMessage());
            }
            catch(IOException ioe) { }
        }
    }

    public static File getChartDir()
    {
        return chartsDir;
    }
}
