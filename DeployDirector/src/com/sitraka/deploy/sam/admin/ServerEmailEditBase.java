// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerEmailEditBase.java

package com.sitraka.deploy.sam.admin;

import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            ServerEmailAddEditBase, JSPPropertiesSupport, JSPSupport

public abstract class ServerEmailEditBase extends ServerEmailAddEditBase
{

    protected int addressToEdit;

    public ServerEmailEditBase()
    {
        addressToEdit = -1;
    }

    public void refresh()
    {
        super.refresh();
        addressToEdit = -1;
    }

    public boolean validate()
    {
        ((JSPPropertiesSupport)this).initializeProperties(false);
        if(addressToEdit == -1)
            findRecipientProperties();
        String prop_base = "deploy.error.email." + addressToEdit;
        String notification = ServerEmailAddEditBase.getNotificationString(super.errorLevels);
        ((JSPPropertiesSupport)this).properties.setProperty(prop_base + ".address", super.address);
        ((JSPPropertiesSupport)this).properties.setProperty(prop_base + ".notification", notification);
        return ((JSPPropertiesSupport)this).storeProperties();
    }

    protected void getExistingValues()
    {
        if(addressToEdit == -1)
            return;
        String prop_base = "deploy.error.email." + addressToEdit;
        if(super.levelList == null || super.levelList.trim().length() == 0)
            super.levelList = ((JSPPropertiesSupport)this).properties.getProperty(prop_base + ".notification", "");
        super.errorLevels = ServerEmailAddEditBase.getNotificationLevels(super.levelList);
    }

    protected void findRecipientProperties()
    {
        if(super.address == null)
        {
            ((JSPSupport)this).errMessage = "Can't retrieve properties for an unknown recipient";
            super.address = "";
        }
        addressToEdit = -1;
        String prop_base = "deploy.error.email.";
        String prop_value = null;
        do
        {
            addressToEdit++;
            String prop_name = prop_base + addressToEdit + ".address";
            prop_value = ((JSPPropertiesSupport)this).properties.getProperty(prop_name);
        } while(prop_value != null && !super.address.equals(((Object) (prop_value))));
        if(prop_value == null)
        {
            if(super.address.length() != 0)
                ((JSPSupport)this).errMessage = "No recipient matching address \"" + super.address + "\" was found.";
            return;
        } else
        {
            return;
        }
    }

    public void setRecipientToEdit(String recipient)
    {
        super.address = recipient;
        ((JSPPropertiesSupport)this).initializeProperties(false);
        findRecipientProperties();
        getExistingValues();
    }
}
