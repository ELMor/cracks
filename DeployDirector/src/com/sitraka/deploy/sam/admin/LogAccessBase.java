// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LogAccessBase.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.servlet.Servlet;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPSupport

public abstract class LogAccessBase extends JSPSupport
{

    public LogAccessBase()
    {
    }

    public boolean validate()
    {
        return true;
    }

    public abstract int getNotesField();

    public Vector getLogEntries(String fileName)
    {
        if(((JSPSupport)this).getServlet() == null)
            return vectorFormat(super.errMessage);
        File vaultBase = ((JSPSupport)this).getServlet().getVault().getVaultBaseDir();
        File logDir = Servlet.getLogFileDir(vaultBase);
        File serverLog = new File(logDir, fileName);
        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(((java.io.Reader) (new FileReader(serverLog))));
        }
        catch(FileNotFoundException fnfe)
        {
            super.errMessage = "Unable to find " + fileName + " file \"" + serverLog.getAbsolutePath() + "\"<br>\n" + ((Throwable) (fnfe)).getMessage() + "<br>";
            return vectorFormat(super.errMessage);
        }
        Vector entries = new Vector();
        int notes = getNotesField();
        String s;
        try
        {
            while((s = reader.readLine()) != null) 
                entries.addElement(((Object) (parseLine(s, notes))));
        }
        catch(IOException ioe)
        {
            super.errMessage = "Error reading from " + fileName + " file \"" + serverLog.getAbsolutePath() + "\"<br>\n" + ((Throwable) (ioe)).getMessage() + "<br>";
            return vectorFormat(super.errMessage);
        }
        int num_entries = entries.size();
        Vector reversed = new Vector(num_entries);
        while(num_entries > 0) 
        {
            num_entries--;
            reversed.addElement(entries.elementAt(num_entries));
        }
        return reversed;
    }

    protected Vector vectorFormat(String message)
    {
        String msg_text = "<pre>\n" + super.errMessage + "\n</pre>";
        Vector error_report = new Vector();
        error_report.addElement(((Object) (msg_text)));
        Vector wrapper = new Vector();
        wrapper.addElement(((Object) (error_report)));
        return wrapper;
    }

    protected Vector parseLine(String lineData, int notes)
    {
        Vector line_vector = new Vector();
        int start = 0;
        int i;
        for(int field = 0; field < notes && start < lineData.length() && (i = lineData.indexOf(',', start)) > -1; field++)
        {
            String data = lineData.substring(start, i);
            line_vector.addElement(((Object) (data)));
            start = i + 1;
        }

        if(start < lineData.length())
            line_vector.addElement(((Object) (lineData.substring(start))));
        return line_vector;
    }

    public String fixHTML(String input)
    {
        StringBuffer output = new StringBuffer("");
        String specials = "&<\" ";
        String spec_trans[] = {
            "&amp;", "&lt;", "&quot;", "&nbsp;"
        };
        String escapes = "nt";
        String esc_trans[] = {
            "<br>\n", "&nbsp;&nbsp;&nbsp;&nbsp;"
        };
        int in_size = input.length();
        for(int i = 0; i < in_size; i++)
        {
            char c = input.charAt(i);
            int index;
            if(c == '\\')
            {
                if(++i < in_size)
                {
                    c = input.charAt(i);
                    if((index = escapes.indexOf(((int) (c)))) != -1)
                        output.append(esc_trans[index]);
                    else
                        output.append(c);
                }
            } else
            if((index = specials.indexOf(((int) (c)))) != -1)
                output.append(spec_trans[index]);
            else
                output.append(c);
        }

        return output.toString();
    }
}
