// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LogQueue.java

package com.sitraka.deploy.sam;

import com.klg.jclass.util.JCListenerList;
import com.sitraka.deploy.SAMEvent;
import com.sitraka.deploy.SAMListener;
import com.sitraka.deploy.common.LoggingEnums;
import com.sitraka.deploy.common.checkpoint.LogObject;
import com.sitraka.deploy.common.timer.Timer;
import com.sitraka.deploy.common.timer.TimerEvent;
import com.sitraka.deploy.common.timer.TimerEventListener;
import com.sitraka.deploy.sam.log.BaseLogger;
import com.sitraka.deploy.sam.log.ClientDataLogger;
import com.sitraka.deploy.sam.log.ClientLogger;
import com.sitraka.deploy.sam.log.Database;
import com.sitraka.deploy.sam.log.DateLogLimit;
import com.sitraka.deploy.sam.log.FileDatabase;
import com.sitraka.deploy.sam.log.JDBCDatabase;
import com.sitraka.deploy.sam.log.LoadLogger;
import com.sitraka.deploy.sam.log.LogException;
import com.sitraka.deploy.sam.log.Logger;
import com.sitraka.deploy.sam.log.ServerLogger;
import com.sitraka.deploy.sam.log.SizeLogLimit;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.sam.worker.AbstractWorker;
import com.sitraka.deploy.sam.worker.ReplicationThread;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam:
//            AbstractMessageQueue, EmailNotifier, LogMessage, ServerActions, 
//            AppVault

public class LogQueue extends AbstractMessageQueue
{
    class Duration
    {

        protected Timestamp from;
        protected Timestamp to;

        public Timestamp getFrom()
        {
            return from;
        }

        public Timestamp getTo()
        {
            return to;
        }

        public Duration(Timestamp from, Timestamp to)
        {
            this.from = from;
            this.to = to;
        }
    }

    class UpdateListener
        implements TimerEventListener
    {

        public void saveDatabase()
        {
            try
            {
                ((Logger) (clientDataLogger)).saveDatabaseToFile();
            }
            catch(LogException le)
            {
                ((Throwable) (le)).printStackTrace();
            }
        }

        public void eventOccured(TimerEvent e)
        {
            saveDatabase();
        }

        public void flushOccured(TimerEvent e)
        {
            saveDatabase();
        }

        UpdateListener()
        {
        }
    }

    class ClientDataAggregateListener
        implements TimerEventListener
    {

        public void aggregateLogs()
        {
            Duration duration = createIncrementalClientDataLog();
            if(duration != null)
                AbstractWorker.getReplicationQueue(server).sendLogReplication(serverAddress, new LogObject(serverAddress, clientDataLogger.getName(), duration.getFrom().toString(), duration.getTo().toString()), ((Object) (server.getAdminID() + ":" + server.getAdminPassword())));
        }

        public void eventOccured(TimerEvent e)
        {
            aggregateLogs();
        }

        public void flushOccured(TimerEvent timerevent)
        {
        }

        ClientDataAggregateListener()
        {
        }
    }

    class AggregateListener
        implements TimerEventListener
    {

        public void aggregateLogs()
        {
            Duration duration = createIncrementalLogs();
            if(duration != null)
                AbstractWorker.getReplicationQueue(server).sendLogReplication(serverAddress, new LogObject(serverAddress, "logs", duration.getFrom().toString(), duration.getTo().toString()), ((Object) (server.getAdminID() + ":" + server.getAdminPassword())));
        }

        public void eventOccured(TimerEvent e)
        {
            aggregateLogs();
        }

        public void flushOccured(TimerEvent timerevent)
        {
        }

        AggregateListener()
        {
        }
    }


    public static final int LOG_TYPE_FILE = 1;
    public static final int LOG_TYPE_JDBC = 2;
    protected static final String LOG_TYPE_NAMES[] = {
        "file", "jdbc"
    };
    protected static final int LOG_TYPE_VALUES[] = {
        1, 2
    };
    protected static final String ClientDataAggregateStart = "ClientDataAggregateStart";
    protected static final String CLIENT_DATA_AGGREGATE_START = "now";
    protected static final String ClientDataAggregateInterval = "ClientDataAggregateInterval";
    protected static final String CLIENT_DATA_AGGREGATE_INTERVAL = "1 day";
    protected ServerLogger serverLogger;
    protected ClientLogger clientLogger;
    protected LoadLogger loadLogger;
    protected ClientDataLogger clientDataLogger;
    protected Timer manager;
    protected boolean isAvailable;
    protected int logType;
    protected boolean central_log;
    protected File logDir;
    protected long updateStart;
    protected long updateInterval;
    protected long aggregateStart;
    protected long aggregateInterval;
    protected String serverID;
    protected String serverAddress;
    protected String otherServers;
    protected EmailNotifier notifier;
    protected String unknown;
    protected Database server_db;
    protected Database client_db;
    protected Database load_db;
    protected Database client_data_db;
    JCListenerList listeners;
    protected ServerActions server;

    public LogQueue(String serverID, Timer man, Properties properties, ServerActions server)
    {
        serverLogger = null;
        clientLogger = null;
        loadLogger = null;
        clientDataLogger = null;
        manager = null;
        isAvailable = false;
        logType = 0;
        central_log = false;
        logDir = null;
        updateStart = -1L;
        updateInterval = -1L;
        aggregateStart = -1L;
        aggregateInterval = -1L;
        this.serverID = null;
        serverAddress = null;
        otherServers = null;
        notifier = null;
        unknown = "(unknown)";
        server_db = null;
        client_db = null;
        load_db = null;
        client_data_db = null;
        listeners = null;
        this.server = null;
        this.serverID = serverID;
        this.server = server;
        manager = man;
        configInternalVariables(properties);
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            if(serverLogger != null)
                ((Logger) (serverLogger)).dispose();
            if(clientLogger != null)
                ((Logger) (clientLogger)).dispose();
            if(loadLogger != null)
                ((Logger) (loadLogger)).dispose();
            if(clientDataLogger != null)
                ((Logger) (clientDataLogger)).dispose();
        }
        catch(LogException e) { }
        listeners = JCListenerList.remove(listeners, ((Object) (null)));
        if(listeners != null)
            listeners = null;
    }

    public File getLogCopy(String name)
    {
        Logger logger = getLogger(name);
        if(logger == null)
            return null;
        File tempFile = FileUtils.createTempFile();
        if(logType == 1)
        {
            if(logDir == null)
                return null;
            boolean status = FileUtils.copyFiles(new File(logDir, name), tempFile, true);
            if(status)
                return tempFile;
        } else
        if(logType == 2)
            try
            {
                boolean status = logger.saveDatabaseToFile(tempFile);
                if(status)
                    return tempFile;
            }
            catch(LogException le) { }
        tempFile.delete();
        return null;
    }

    protected void configInternalVariables(Properties properties)
    {
        notifier = new EmailNotifier(this, properties);
        ((Thread) (notifier)).setName("EmailNotificationThread");
        ((Thread) (notifier)).setPriority(2);
        ((Thread) (notifier)).setDaemon(true);
        ((Thread) (notifier)).start();
        serverAddress = PropertyUtils.readDefaultString(properties, "deploy.localhost", ((String) (null)));
        otherServers = server.getVault().listOtherServers();
        logType = PropertyUtils.readEnum(properties, "deploy.log.type", LOG_TYPE_NAMES, LOG_TYPE_VALUES);
        if(logType == -1)
        {
            String message = "Unknown log type specified: '" + PropertyUtils.readDefaultString(properties, "deploy.log.type", ((String) (null))) + "'";
            disableLogging(message);
            throw new IllegalArgumentException(message + " logging disabled");
        }
        server_db = null;
        client_db = null;
        load_db = null;
        client_data_db = null;
        logDir = Servlet.getLogFileDir(server.getVault().getVaultBaseDir());
        if(logType == 1)
        {
            boolean status = createFileDatabases();
            if(!status)
                return;
        } else
        if(logType == 2)
        {
            boolean status = createJDBCDatabases(properties);
            if(!status)
            {
                logType = 1;
                status = createFileDatabases();
                if(!status)
                    return;
            }
        }
        int min = PropertyUtils.readInt(properties, "deploy.log.limit.min", 100);
        int max = PropertyUtils.readInt(properties, "deploy.log.limit.max", -1);
        long interval = PropertyUtils.readInterval(properties, "deploy.log.limit.interval", -1L);
        SizeLogLimit sizeLogLimit = new SizeLogLimit(min, max, BaseLogger.getTimestampIndex());
        DateLogLimit dateLogLimit = new DateLogLimit(interval, ClientDataLogger.getTimestampIndex());
        try
        {
            serverLogger = new ServerLogger(serverID, server_db, ((com.sitraka.deploy.sam.log.LogLimit) (sizeLogLimit)));
            clientLogger = new ClientLogger(serverID, client_db, ((com.sitraka.deploy.sam.log.LogLimit) (sizeLogLimit)));
            loadLogger = new LoadLogger(serverID, load_db, ((com.sitraka.deploy.sam.log.LogLimit) (sizeLogLimit)));
            clientDataLogger = new ClientDataLogger(serverID, client_data_db, ((com.sitraka.deploy.sam.log.LogLimit) (dateLogLimit)));
        }
        catch(LogException le)
        {
            disableLogging("Unable to load logging classes: " + ((Throwable) (le)).getMessage());
            return;
        }
        if(logType == 1)
            addLoggingListener(properties, "deploy.log.update.start", "deploy.log.update.interval", ((TimerEventListener) (new UpdateListener())));
        central_log = PropertyUtils.readBoolean(properties, "deploy.log.cluster");
        if(!central_log && otherServers != null)
        {
            addLoggingListener(properties, "deploy.log.aggregate.start", "deploy.log.aggregate.interval", ((TimerEventListener) (new AggregateListener())));
            ((Hashtable) (properties)).put("ClientDataAggregateStart", "now");
            ((Hashtable) (properties)).put("ClientDataAggregateInterval", "1 day");
            addLoggingListener(properties, "ClientDataAggregateStart", "ClientDataAggregateInterval", ((TimerEventListener) (new ClientDataAggregateListener())));
        }
        isAvailable = true;
        addUserListener(properties);
    }

    protected boolean createJDBCDatabases(Properties properties)
    {
        String value = null;
        value = properties.getProperty("deploy.log.jdbc.driver");
        String driver = value != null ? value.trim() : null;
        value = properties.getProperty("deploy.log.jdbc.url");
        String db_url = value != null ? value.trim() : null;
        value = properties.getProperty("deploy.log.jdbc.user");
        String user = value != null ? value.trim() : null;
        value = properties.getProperty("deploy.log.jdbc.password");
        String password = value != null ? value.trim() : "";
        try
        {
            server_db = ((Database) (new JDBCDatabase(driver, db_url, user, password, true)));
            client_db = ((Database) (new JDBCDatabase(driver, db_url, user, password, true)));
            load_db = ((Database) (new JDBCDatabase(driver, db_url, user, password, true)));
            client_data_db = ((Database) (new JDBCDatabase(driver, db_url, user, password, true)));
        }
        catch(IllegalArgumentException iae)
        {
            sendErrorByEmail("ServerConfigurationError", "Unable to load database logging classes: " + ((Throwable) (iae)).getMessage());
            return false;
        }
        return true;
    }

    protected boolean createFileDatabases()
    {
        if(logDir == null)
        {
            disableLogging("Logging directory cannot be accessed");
            return false;
        } else
        {
            server_db = ((Database) (new FileDatabase(logDir, false)));
            client_db = ((Database) (new FileDatabase(logDir, false)));
            load_db = ((Database) (new FileDatabase(logDir, false)));
            client_data_db = ((Database) (new FileDatabase(logDir, false)));
            return true;
        }
    }

    protected void addUserListener(Properties properties)
    {
        String userClassName = PropertyUtils.readDefaultString(properties, "deploy.log.listener", ((String) (null)));
        if(userClassName == null)
            return;
        try
        {
            Class userClass = Class.forName(userClassName);
            SAMListener l = (SAMListener)userClass.newInstance();
            listeners = JCListenerList.add(listeners, ((Object) (l)));
        }
        catch(Exception e)
        {
            logMessage("ServerInternalError", ((String) (null)), ((String) (null)), "DeploySam", "2.5.0", "deploy.log.listener class '" + userClassName + "' could not be loaded. Exception: " + ((Object) (e)).getClass().getName());
        }
    }

    protected void addLoggingListener(Properties properties, String startProp, String intervalProp, TimerEventListener l)
    {
        long start = -1L;
        start = PropertyUtils.readDate(properties, startProp);
        long interval = PropertyUtils.readInterval(properties, intervalProp, -1L);
        if(interval < 0L)
            interval = 0L;
        manager.addTimerEvent(new TimerEvent(start, interval, l));
    }

    protected void disableLogging(String reason)
    {
        isAvailable = false;
        sendErrorByEmail("ServerConfigurationError", reason);
    }

    public synchronized boolean isAvailable()
    {
        return isAvailable;
    }

    public void logMessage(String event, String remote_id, String user_id, String application, String version)
    {
        logMessage(event, remote_id, user_id, application, version, ((String) (null)));
    }

    public void logMessage(String event, String remote_id, String user_id, String application, String version, String notes)
    {
        if(event == null)
            event = "ServerInternalError";
        LogMessage m = new LogMessage(event, new Timestamp(System.currentTimeMillis()), serverID);
        m.setRemoteId(remote_id != null ? remote_id : unknown);
        m.setUserId(user_id != null ? user_id : unknown);
        m.setApplication(application != null ? application : unknown);
        m.setVersion(version != null ? version : unknown);
        m.setNotes(notes != null ? notes : "");
        logMessage(m);
    }

    public void logLoadMessage(double loads[], long uptime)
    {
        LogMessage m = new LogMessage("ServerLoadRecord", new Timestamp(System.currentTimeMillis()), serverID);
        m.setLoadAverage(loads);
        m.setUptime(uptime);
        logMessage(m);
    }

    public void logClientDataMessage(String remote_id, String user_id, String bundle_name, String bundle_version, String last_client_IP)
    {
        if(user_id == null || remote_id == null || bundle_name == null)
        {
            return;
        } else
        {
            LogMessage m = new LogMessage("ClientDataRecord", new Timestamp(System.currentTimeMillis()), serverID);
            m.setRemoteId(remote_id != null ? remote_id : unknown);
            m.setUserId(user_id != null ? user_id : unknown);
            m.setApplication(bundle_name != null ? bundle_name : unknown);
            m.setVersion(bundle_version != null ? bundle_version : unknown);
            m.setLastConnection(m.getTimestamp());
            m.setLastClientIP(last_client_IP != null ? last_client_IP : unknown);
            m.setInitialVersion(m.getVersion());
            m.setInitialUserId(m.getUserId());
            m.setInstallDate(m.getTimestamp());
            logMessage(m);
            return;
        }
    }

    public void logMessage(LogMessage m)
    {
        if(isAvailable())
            ((AbstractMessageQueue)this).addMessage(((Object) (m)));
        if(!m.getEvent().equals("ServerEmailFailed"))
            ((AbstractMessageQueue) (notifier)).addMessage(((Object) (m)));
    }

    public synchronized void stopLoggingThread()
    {
        ((AbstractMessageQueue)this).stopQueue(true);
        ((AbstractMessageQueue) (notifier)).stopQueue(true);
    }

    protected void sendErrorByEmail(String error, String notes)
    {
        LogMessage logMessage = new LogMessage(error, new Timestamp(System.currentTimeMillis()), serverID);
        logMessage.setRemoteId(unknown);
        logMessage.setUserId(unknown);
        logMessage.setApplication("DeploySam");
        logMessage.setVersion("2.5.0");
        logMessage.setNotes("Error in logging process: " + notes);
        Enumeration e = JCListenerList.elements(listeners);
        SAMEvent event = new SAMEvent(error, serverID, logMessage.getRemoteId(), logMessage.getTimestamp(), logMessage.getNotes());
        while(e.hasMoreElements()) 
        {
            SAMListener listener = (SAMListener)e.nextElement();
            if(LoggingEnums.isErrorEvent(error))
                listener.serverError(event);
            else
                listener.serverEvent(event);
        }
        ((AbstractMessageQueue) (notifier)).addMessage(((Object) (logMessage)));
    }

    protected void processMessage(Object message)
    {
        LogMessage m = (LogMessage)message;
        try
        {
            switch(m.getMessageType())
            {
            case 1: // '\001'
                clientLogger.log(m.getTimestamp(), m.getEvent(), m.getRemoteId(), m.getUserId(), m.getApplication(), m.getVersion(), m.getNotes());
                fireClientEvent(m);
                break;

            case 2: // '\002'
                serverLogger.log(m.getTimestamp(), m.getEvent(), m.getRemoteId(), m.getNotes());
                fireServerEvent(m);
                break;

            case 3: // '\003'
                loadLogger.log(m.getUptime(), m.getShortTermLoad(), m.getMidTermLoad(), m.getLongTermLoad());
                fireLoadEvent(m);
                break;

            case 4: // '\004'
                clientDataLogger.write(m.getRemoteId(), m.getUserId(), m.getApplication(), m.getVersion(), m.getLastConnection(), m.getLastClientIP(), m.getInitialVersion(), m.getInitialUserId(), m.getInstallDate());
                break;

            default:
                serverLogger.log(m.getTimestamp(), "ServerInternalError", m.getRemoteId(), "Unknown log request received:\n" + m.toString());
                break;
            }
        }
        catch(LogException le)
        {
            sendErrorByEmail("ServerLogFailure", ((Throwable) (le)).getMessage());
            return;
        }
    }

    public Logger getLogger(String name)
    {
        if(serverLogger.getName().equals(((Object) (name))))
            return ((Logger) (serverLogger));
        if(loadLogger.getName().equals(((Object) (name))))
            return ((Logger) (loadLogger));
        if(clientLogger.getName().equals(((Object) (name))))
            return ((Logger) (clientLogger));
        if(clientDataLogger.getName().equals(((Object) (name))))
            return ((Logger) (clientDataLogger));
        else
            return null;
    }

    public ClientDataLogger getClientDataLogger()
    {
        return clientDataLogger;
    }

    public ClientLogger getClientLogger()
    {
        return clientLogger;
    }

    public ServerLogger getServerLogger()
    {
        return serverLogger;
    }

    public LoadLogger getLoadLogger()
    {
        return loadLogger;
    }

    public static File makeIncrementalFile(File basedir, String fileName)
    {
        return new File(basedir, fileName + ".inc");
    }

    public static File makeTimestampFilename(File basedir)
    {
        return new File(basedir, "logstamp");
    }

    public static File makeClientDataTimestampFilename(File basedir)
    {
        return new File(basedir, "datstamp");
    }

    public boolean createIncrementalLog(Logger logger, Timestamp from, Timestamp to)
    {
        String name = logger.getName();
        File incrementalFile = makeIncrementalFile(logDir, name);
        return createIncrementalLog(logger, incrementalFile, from, to);
    }

    public boolean createIncrementalLog(Logger logger, File incrementalFile, String from, String to)
    {
        return createIncrementalLog(logger, incrementalFile, Timestamp.valueOf(from), Timestamp.valueOf(to));
    }

    public boolean createIncrementalLog(Logger logger, File incrementalFile, Timestamp from, Timestamp to)
    {
        String name = logger.getName();
        FileOutputStream incrementalOutputStream = null;
        try
        {
            incrementalOutputStream = new FileOutputStream(incrementalFile);
        }
        catch(Exception e)
        {
            sendErrorByEmail("ServerLogFailure", "Cannot create incremental log file " + incrementalFile.getName() + "\n" + ((Throwable) (e)).getMessage());
            return false;
        }
        synchronized(logger)
        {
            try
            {
                logger.sendLog(((OutputStream) (incrementalOutputStream)), from, to);
            }
            catch(LogException le)
            {
                sendErrorByEmail("ServerLogFailure", "Cannot retrieve log records for " + name + "\n" + ((Throwable) (le)).getMessage());
            }
            try
            {
                ((OutputStream) (incrementalOutputStream)).flush();
                incrementalOutputStream.close();
            }
            catch(IOException ioe)
            {
                sendErrorByEmail("ServerLogFailure", "Cannot save log file " + incrementalFile.getName() + "\n" + ((Throwable) (ioe)).getMessage());
            }
        }
        return incrementalFile.length() > 0L;
    }

    public static Timestamp readTimestampFile(File timestampFile)
    {
        Timestamp lastTime = null;
        try
        {
            ObjectInputStream ois = new ObjectInputStream(((java.io.InputStream) (new FileInputStream(timestampFile))));
            if(ois != null)
            {
                lastTime = (Timestamp)ois.readObject();
                ois.close();
            }
        }
        catch(Exception e)
        {
            lastTime = new Timestamp(0L);
        }
        return lastTime;
    }

    protected boolean writeTimestampFile(File timestampFile, Timestamp timestamp)
    {
        boolean status = false;
        try
        {
            ObjectOutputStream oos = new ObjectOutputStream(((OutputStream) (new FileOutputStream(timestampFile))));
            if(oos != null)
            {
                oos.writeObject(((Object) (timestamp)));
                oos.flush();
                oos.close();
                status = true;
            }
        }
        catch(IOException ioe)
        {
            status = false;
            sendErrorByEmail("ServerLogFailure", "Cannot write to timestamp file " + timestampFile.getName() + "\n" + ((Throwable) (ioe)).getMessage());
        }
        return status;
    }

    protected Duration createIncrementalLogs()
    {
        File timestampFile = makeTimestampFilename(logDir);
        Timestamp lastTime = readTimestampFile(timestampFile);
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        writeTimestampFile(timestampFile, currentTime);
        boolean newLog = false;
        boolean status = createIncrementalLog(((Logger) (serverLogger)), lastTime, currentTime);
        newLog = newLog || status;
        status = createIncrementalLog(((Logger) (clientLogger)), lastTime, currentTime);
        newLog = newLog || status;
        status = createIncrementalLog(((Logger) (loadLogger)), lastTime, currentTime);
        newLog = newLog || status;
        if(newLog)
            return new Duration(lastTime, currentTime);
        else
            return null;
    }

    protected Duration createIncrementalClientDataLog()
    {
        File timestampFile = makeClientDataTimestampFilename(logDir);
        Timestamp lastTime = readTimestampFile(timestampFile);
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        writeTimestampFile(timestampFile, currentTime);
        boolean newLog = createIncrementalLog(((Logger) (clientDataLogger)), lastTime, currentTime);
        if(newLog)
            return new Duration(lastTime, currentTime);
        else
            return null;
    }

    protected void fireLoadEvent(LogMessage m)
    {
        Enumeration e = JCListenerList.elements(listeners);
        SAMEvent event = new SAMEvent(m.getEvent(), serverID, new Timestamp(System.currentTimeMillis()), m.getUptime(), m.getShortTermLoad(), m.getMidTermLoad(), m.getLongTermLoad());
        SAMListener listener;
        for(; e.hasMoreElements(); listener.serverLoad(event))
            listener = (SAMListener)e.nextElement();

    }

    protected void fireClientEvent(LogMessage m)
    {
        Enumeration e = JCListenerList.elements(listeners);
        SAMEvent event = new SAMEvent(m.getEvent(), serverID, m.getRemoteId(), m.getUserId(), m.getApplication(), m.getVersion(), m.getTimestamp(), m.getNotes());
        while(e.hasMoreElements()) 
        {
            SAMListener listener = (SAMListener)e.nextElement();
            if(LoggingEnums.isErrorEvent(m.getEvent()))
                listener.clientError(event);
            else
                listener.clientEvent(event);
        }
    }

    protected void fireServerEvent(LogMessage m)
    {
        Enumeration e = JCListenerList.elements(listeners);
        SAMEvent event = new SAMEvent(m.getEvent(), serverID, m.getRemoteId(), m.getTimestamp(), m.getNotes());
        while(e.hasMoreElements()) 
        {
            SAMListener listener = (SAMListener)e.nextElement();
            if(LoggingEnums.isErrorEvent(m.getEvent()))
                listener.serverError(event);
            else
                listener.serverEvent(event);
        }
    }

}
