// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AbstractTracker.java

package com.sitraka.deploy.sam;


public interface AbstractTracker
{

    public static final int NOT_STARTED = 1;
    public static final int IN_PROGRESS = 2;
    public static final int COMPLETED = 3;
    public static final int ABORTED = 4;
}
