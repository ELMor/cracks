// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LogReceiver.java

package com.sitraka.deploy.sam.receive;

import com.sitraka.deploy.common.checkpoint.BaseCheckpoint;
import com.sitraka.deploy.common.checkpoint.LogCheckpoint;
import com.sitraka.deploy.common.checkpoint.LogObject;
import com.sitraka.deploy.common.connection.AbstractHttpConnection;
import com.sitraka.deploy.common.connection.HttpConnection;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.LogQueue;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.log.LogException;
import com.sitraka.deploy.sam.log.Logger;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import com.sitraka.deploy.util.FileUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

// Referenced classes of package com.sitraka.deploy.sam.receive:
//            AbstractReceiver

public class LogReceiver extends AbstractReceiver
{

    public LogReceiver(ServerActions server, AppVault vault)
    {
        super(server, vault);
    }

    public synchronized void processMessage(LogCheckpoint checkpoint)
        throws ServletProcessException
    {
        LogObject lo = checkpoint.getLogObject();
        String remoteServerName = super.server.getServerURI(lo.getServer());
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(200))
        {
            ((BaseCheckpoint) (checkpoint)).setSourceDir(FileUtils.createTempFile(((BaseCheckpoint) (checkpoint)).getTempDir()));
            ((BaseCheckpoint) (checkpoint)).setDestinationDir(((BaseCheckpoint) (checkpoint)).getSourceDir());
            ((BaseCheckpoint) (checkpoint)).setState(200);
        }
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(500))
        {
            HttpConnection http = null;
            java.io.InputStream is = ((BaseCheckpoint) (checkpoint)).getInputStream();
            if(is == null)
            {
                http = new HttpConnection();
                ((AbstractHttpConnection) (http)).setIsAdmin(true);
                ((AbstractHttpConnection) (http)).setCommand("SERVERUPDATELOG");
                ((AbstractHttpConnection) (http)).setFromVersion(lo.getFrom());
                ((AbstractHttpConnection) (http)).setToVersion(lo.getTo());
                ((AbstractHttpConnection) (http)).setName(lo.getName());
                ((AbstractHttpConnection) (http)).setUserIDObject(super.adminIDObject);
                java.util.Vector others = ((AbstractReceiver)this).getOtherServers();
                if(others == null)
                {
                    String msg = "Log replication: Could not create HTTP connection to other servers.  No other servers listed in the cluster. Request came from: " + remoteServerName;
                    throw new ServletProcessException(msg, "ServerRequestFailed", super.none, super.none, remoteServerName, true);
                }
                ((AbstractHttpConnection) (http)).setServerList(others);
            }
            super.server.logMessage(remoteServerName, "ServerLogAggregateStart", super.none, super.none, "From: " + remoteServerName);
            try
            {
                ((AbstractReceiver)this).receiveZip(((com.sitraka.deploy.common.checkpoint.Checkpoint) (checkpoint)), http, is, true);
            }
            catch(ServletProcessException spe)
            {
                String msg = "receiveZip failed; upload rejected from: " + remoteServerName;
                FileUtils.deleteDirectory(((BaseCheckpoint) (checkpoint)).getDestinationDir());
                spe.setApplication(super.none);
                spe.setVersion(super.none);
                spe.setRemoteName(remoteServerName);
                throw spe;
            }
            ((BaseCheckpoint) (checkpoint)).setState(500);
        }
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(600))
        {
            aggregateLogFiles(((BaseCheckpoint) (checkpoint)).getDestinationDir(), lo);
            super.server.logMessage(remoteServerName, "ServerLogAggregateComplete", super.none, super.none, "From: " + remoteServerName);
            FileUtils.deleteDirectory(((BaseCheckpoint) (checkpoint)).getDestinationDir());
            ((BaseCheckpoint) (checkpoint)).setState(600);
        }
        ((BaseCheckpoint) (checkpoint)).setState(0xf423f);
    }

    protected void aggregateLogFiles(File destinationDir, LogObject lo)
    {
        File logList[] = destinationDir.listFiles();
        if(logList == null || logList.length == 0)
            return;
        String remoteServerName = super.server.getServerURI(lo.getServer());
        for(int i = 0; i < logList.length; i++)
        {
            Logger logger = super.server.getLogQueue().getLogger(logList[i].getName());
            if(logger == null)
            {
                String msg = "Log request from " + remoteServerName + "failed:  " + "Cannot find logger for incremental file " + logList[i];
                super.server.logMessage(remoteServerName, "ServerInternalError", super.none, super.none, msg);
                continue;
            }
            FileInputStream fis = null;
            try
            {
                fis = new FileInputStream(logList[i]);
            }
            catch(IOException ioe)
            {
                String msg = "Log request from " + remoteServerName + "failed:  " + "IOException opening file input stream for " + logList[i] + "\n\t" + ((Throwable) (ioe)).getMessage();
                super.server.logMessage(remoteServerName, "ServerLogFailure", super.none, super.none, msg);
                continue;
            }
            try
            {
                logger.receiveLog(((java.io.InputStream) (fis)));
            }
            catch(LogException le)
            {
                String msg = "Log request from " + remoteServerName + "failed:  " + "LogException writing to log file " + logList[i] + "\n\t" + ((Throwable) (le)).getMessage();
                super.server.logMessage(remoteServerName, "ServerLogFailure", super.none, super.none, msg);
            }
            try
            {
                fis.close();
            }
            catch(IOException ioe)
            {
                String msg = "Log request from " + remoteServerName + "failed:  " + "IOException closing file input stream for " + logList[i] + "\n\t" + ((Throwable) (ioe)).getMessage();
                super.server.logMessage(remoteServerName, "ServerLogFailure", super.none, super.none, msg);
            }
        }

    }
}
