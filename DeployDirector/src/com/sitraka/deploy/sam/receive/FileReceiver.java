// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FileReceiver.java

package com.sitraka.deploy.sam.receive;

import com.klg.jclass.util.JCListenerList;
import com.sitraka.deploy.common.checkpoint.BaseCheckpoint;
import com.sitraka.deploy.common.checkpoint.FileCheckpoint;
import com.sitraka.deploy.common.checkpoint.FileUploadObject;
import com.sitraka.deploy.common.connection.AbstractHttpConnection;
import com.sitraka.deploy.common.connection.HttpConnection;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import com.sitraka.deploy.util.FileUtils;
import java.util.Enumeration;

// Referenced classes of package com.sitraka.deploy.sam.receive:
//            AbstractReceiver, ReceiverEvent, ReceiverListener

public class FileReceiver extends AbstractReceiver
{

    public FileReceiver(ServerActions server, AppVault vault)
    {
        super(server, vault);
    }

    public synchronized void processMessage(FileCheckpoint checkpoint)
        throws ServletProcessException
    {
        FileUploadObject fo = checkpoint.getFileObject();
        String remoteServerName = super.server.getServerURI(fo.getServer());
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(200))
        {
            ((BaseCheckpoint) (checkpoint)).setSourceDir(FileUtils.createTempFile(((BaseCheckpoint) (checkpoint)).getTempDir()));
            ((BaseCheckpoint) (checkpoint)).setDestinationDir(super.vault.getVaultBaseDir());
            ((BaseCheckpoint) (checkpoint)).setState(200);
        }
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(500))
        {
            HttpConnection conn = null;
            java.io.InputStream is = ((BaseCheckpoint) (checkpoint)).getInputStream();
            if(is == null)
            {
                conn = new HttpConnection();
                ((AbstractHttpConnection) (conn)).setIsAdmin(true);
                ((AbstractHttpConnection) (conn)).setUserIDObject(super.adminIDObject);
                ((AbstractHttpConnection) (conn)).setCommand("FILE");
                ((AbstractHttpConnection) (conn)).setFilename(FileUtils.makeInternalPath(fo.getFilename()));
                ((AbstractHttpConnection) (conn)).setPlatform("DeploySam");
                java.util.Vector others = ((AbstractReceiver)this).getOtherServers();
                if(others == null)
                {
                    String msg = "File replication: Could not create HTTP connection to other servers.  No other servers listed in the cluster.  Request came from: " + remoteServerName;
                    throw new ServletProcessException(msg, "ServerRequestFailed", super.none, super.none, remoteServerName, true);
                }
                ((AbstractHttpConnection) (conn)).setServerList(others);
            }
            try
            {
                ((AbstractReceiver)this).receiveZip(((com.sitraka.deploy.common.checkpoint.Checkpoint) (checkpoint)), conn, is, true);
            }
            catch(ServletProcessException spe)
            {
                String msg = "receiveZip failed:  upload rejected from \"" + remoteServerName + "\".";
                spe.setRemoteName(remoteServerName);
                throw spe;
            }
            super.server.logMessage(remoteServerName, "ServerHTMLReceived", super.none, super.none, "From: " + remoteServerName);
            ((BaseCheckpoint) (checkpoint)).setState(500);
        }
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(600))
            ((BaseCheckpoint) (checkpoint)).setState(600);
        ((BaseCheckpoint) (checkpoint)).setState(0xf423f);
        super.server.logMessage(remoteServerName, "ServerHTMLUpdateComplete", super.none, super.none, "From: " + remoteServerName);
        fireReceiveFileComplete(new ReceiverEvent(((Object) (this)), ((com.sitraka.deploy.common.checkpoint.MetaInfo) (fo))));
    }

    protected void fireReceiveFileComplete(ReceiverEvent event)
    {
        Enumeration e = JCListenerList.elements(super.listeners);
        for(int i = 0; i < 2; i++)
        {
            while(e.hasMoreElements()) 
            {
                ReceiverListener listener = (ReceiverListener)e.nextElement();
                listener.receiveFileComplete(event);
            }
            e = JCListenerList.elements(AbstractReceiver.globalListeners);
        }

    }
}
