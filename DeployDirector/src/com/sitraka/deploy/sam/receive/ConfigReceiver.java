// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ConfigReceiver.java

package com.sitraka.deploy.sam.receive;

import com.sitraka.deploy.common.checkpoint.BaseCheckpoint;
import com.sitraka.deploy.common.checkpoint.ConfigCheckpoint;
import com.sitraka.deploy.common.checkpoint.ConfigObject;
import com.sitraka.deploy.common.connection.AbstractHttpConnection;
import com.sitraka.deploy.common.connection.HttpConnection;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.servlet.ServletProcessException;

// Referenced classes of package com.sitraka.deploy.sam.receive:
//            AbstractReceiver

public class ConfigReceiver extends AbstractReceiver
{

    public ConfigReceiver(ServerActions server, AppVault vault)
    {
        super(server, vault);
    }

    public synchronized void processMessage(ConfigCheckpoint checkpoint)
        throws ServletProcessException
    {
        ConfigObject co = checkpoint.getConfigObject();
        String remoteServerName = super.server.getServerURI(co.getServer());
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(200))
        {
            ((BaseCheckpoint) (checkpoint)).setDestinationDir(super.vault.getVaultBaseDir());
            ((BaseCheckpoint) (checkpoint)).setState(200);
        }
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(500))
        {
            HttpConnection conn = null;
            java.io.InputStream is = ((BaseCheckpoint) (checkpoint)).getInputStream();
            if(is == null)
            {
                conn = new HttpConnection();
                ((AbstractHttpConnection) (conn)).setIsAdmin(true);
                ((AbstractHttpConnection) (conn)).setCommand("SERVERCONFIG");
                ((AbstractHttpConnection) (conn)).setUserIDObject(super.adminIDObject);
                ((AbstractHttpConnection) (conn)).setServerList(co.getServer());
            }
            try
            {
                ((AbstractReceiver)this).receiveZip(((com.sitraka.deploy.common.checkpoint.Checkpoint) (checkpoint)), conn, is, true);
            }
            catch(ServletProcessException spe)
            {
                String msg = "receiveZip failed: upload rejected from \"" + remoteServerName + "\".";
                spe.setRemoteName(remoteServerName);
                throw spe;
            }
            super.server.logMessage(remoteServerName, "ServerConfigReceived", super.none, super.none, "From: " + remoteServerName);
            ((BaseCheckpoint) (checkpoint)).setState(500);
        }
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(600))
        {
            ((BaseCheckpoint) (checkpoint)).setState(600);
            super.server.restartServer();
        }
        ((BaseCheckpoint) (checkpoint)).setState(0xf423f);
        super.server.logMessage(remoteServerName, "ServerConfigUpdateComplete", super.none, super.none, "From: " + remoteServerName);
    }
}
