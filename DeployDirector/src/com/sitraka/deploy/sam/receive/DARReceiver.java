// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DARReceiver.java

package com.sitraka.deploy.sam.receive;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.common.checkpoint.BaseCheckpoint;
import com.sitraka.deploy.common.checkpoint.DARCheckpoint;
import com.sitraka.deploy.common.checkpoint.DARObject;
import com.sitraka.deploy.common.connection.AbstractHttpConnection;
import com.sitraka.deploy.common.connection.HttpConnection;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import com.sitraka.deploy.util.FileUtils;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Vector;
import org.xml.sax.SAXException;

// Referenced classes of package com.sitraka.deploy.sam.receive:
//            AbstractReceiver

public class DARReceiver extends AbstractReceiver
{

    public DARReceiver(ServerActions server, AppVault vault)
    {
        super(server, vault);
    }

    public synchronized void processMessage(DARCheckpoint checkpoint)
        throws ServletProcessException
    {
        DARObject dar = checkpoint.getDARObject();
        String remoteServerName = super.server.getServerURI(dar.getServer());
        Application app = super.vault.getApplication(dar.getApplication());
        boolean newApp = app == null;
        boolean addAppDir = false;
        File versionDir = null;
        File appDir = new File(super.vault.getVaultBaseDir(), "vault" + File.separator + dar.getApplication().toLowerCase());
        if(newApp)
        {
            if(!appDir.exists())
            {
                addAppDir = true;
                appDir.mkdirs();
            }
            File versions = new File(appDir, "versions.lst");
            try
            {
                FileWriter fw = new FileWriter(versions);
                ((Writer) (fw)).write(dar.getVersion());
                ((OutputStreamWriter) (fw)).close();
            }
            catch(IOException ioe)
            {
                if(addAppDir)
                    FileUtils.deleteDirectory(appDir);
                String msg = "(" + dar.getApplication() + " " + dar.getVersion() + ") could not create new " + "application on server " + remoteServerName;
                throw new ServletProcessException(msg, "ServerRequestFailed", dar.getApplication(), dar.getVersion(), remoteServerName, true);
            }
        }
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(200))
        {
            if(!newApp && app.getVersion(dar.getVersion()) != null)
            {
                String msg = "(" + dar.getApplication() + " " + dar.getVersion() + ") Version entry already exists in vault; upload " + "rejected from: " + remoteServerName;
                throw new ServletProcessException(msg, "ServerRequestFailed", dar.getApplication(), dar.getVersion(), remoteServerName, true);
            }
            ((BaseCheckpoint) (checkpoint)).setSourceDir(((BaseCheckpoint) (checkpoint)).getTempDir());
            ((BaseCheckpoint) (checkpoint)).setDestinationDir(((BaseCheckpoint) (checkpoint)).getTempDir());
            ((BaseCheckpoint) (checkpoint)).setState(200);
        }
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(500))
        {
            HttpConnection http = null;
            java.io.InputStream is = ((BaseCheckpoint) (checkpoint)).getInputStream();
            if(is == null)
            {
                http = new HttpConnection();
                ((AbstractHttpConnection) (http)).setIsAdmin(true);
                ((AbstractHttpConnection) (http)).setCommand("SERVERVERSION");
                ((AbstractHttpConnection) (http)).setApplication(dar.getApplication());
                ((AbstractHttpConnection) (http)).setVersion(dar.getVersion());
                ((AbstractHttpConnection) (http)).setUserIDObject(super.adminIDObject);
                Vector others = ((AbstractReceiver)this).getOtherServers();
                if(others == null)
                {
                    if(addAppDir)
                        FileUtils.deleteDirectory(appDir);
                    String msg = "DAR replication: Could not create HTTP connection to other servers.  No other servers listed in the cluster. Request came from: " + remoteServerName;
                    throw new ServletProcessException(msg, "ServerRequestFailed", dar.getApplication(), dar.getVersion(), remoteServerName, true);
                }
                ((AbstractHttpConnection) (http)).setServerList(others);
            }
            try
            {
                ((AbstractReceiver)this).receiveZip(((com.sitraka.deploy.common.checkpoint.Checkpoint) (checkpoint)), http, is, false);
            }
            catch(ServletProcessException spe)
            {
                if(addAppDir)
                    FileUtils.deleteDirectory(appDir);
                String msg = "receiveZip failed; upload rejected from: " + remoteServerName;
                spe.setApplication(dar.getApplication());
                spe.setVersion(dar.getVersion());
                spe.setRemoteName(remoteServerName);
                throw spe;
            }
            File darFile = new File(((BaseCheckpoint) (checkpoint)).getTempDir(), dar.getDARName());
            versionDir = new File(appDir, dar.getVersion().toLowerCase());
            boolean status = FileUtils.unzipFile(darFile, versionDir);
            if(darFile.delete());
            if(!status)
            {
                if(addAppDir)
                    FileUtils.deleteDirectory(appDir);
                else
                    FileUtils.deleteDirectory(versionDir);
                String msg = "Upload from " + remoteServerName + " failed; error" + " unpacking DAR file " + darFile.getAbsolutePath();
                throw new ServletProcessException(msg, "ServerRequestFailed", dar.getApplication(), dar.getVersion(), remoteServerName, true);
            }
            ((BaseCheckpoint) (checkpoint)).setState(500);
        }
        super.server.logMessage(remoteServerName, "ServerVersionReceived", dar.getApplication(), dar.getVersion(), "(" + dar.getApplication() + " " + dar.getVersion() + ") From: " + remoteServerName);
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(600))
        {
            if(newApp)
                try
                {
                    app = new Application(dar.getApplication(), appDir);
                    super.vault.addApplication(app);
                }
                catch(SAXException saxe)
                {
                    if(addAppDir)
                        FileUtils.deleteDirectory(appDir);
                    else
                        FileUtils.deleteDirectory(versionDir);
                    String msg = "Upload from " + remoteServerName + " failed; " + "SAXException creating new application " + dar.getApplication();
                    throw new ServletProcessException(msg, ((Throwable) (saxe)), "ServerRequestFailed", dar.getApplication(), dar.getVersion(), remoteServerName, true);
                }
                catch(IOException ioe)
                {
                    if(addAppDir)
                        FileUtils.deleteDirectory(appDir);
                    else
                        FileUtils.deleteDirectory(versionDir);
                    String msg = "Upload from " + remoteServerName + " failed; " + "IOException creating new application " + dar.getApplication();
                    throw new ServletProcessException(msg, ((Throwable) (ioe)), "ServerRequestFailed", dar.getApplication(), dar.getVersion(), remoteServerName, true);
                }
            versionDir = new File(appDir, dar.getVersion().toLowerCase());
            File oldXML = new File(versionDir, "META-INF" + File.separator + "version.xml");
            File versionXML = super.vault.getVersionXML(dar.getApplication(), dar.getVersion());
            FileUtils.copyFiles(oldXML, versionXML);
            try
            {
                Version newVersion;
                if(!newApp)
                {
                    newVersion = new Version(versionXML);
                    app.addVersionToHead(newVersion);
                } else
                {
                    newVersion = (Version)app.listVersions().elementAt(0);
                }
                newVersion.computeFileHashAndSize(appDir);
                newVersion.setName(dar.getVersion());
                newVersion.saveXMLToFile(versionXML);
            }
            catch(IOException ioe)
            {
                if(addAppDir)
                    FileUtils.deleteDirectory(appDir);
                else
                    FileUtils.deleteDirectory(versionDir);
                String msg = "Upload from " + remoteServerName + " failed; " + "IOException creating new version " + dar.getVersion();
                throw new ServletProcessException(msg, ((Throwable) (ioe)), "ServerRequestFailed", dar.getApplication(), dar.getVersion(), remoteServerName, true);
            }
            catch(XmlException xmle)
            {
                if(addAppDir)
                    FileUtils.deleteDirectory(appDir);
                else
                    FileUtils.deleteDirectory(versionDir);
                String msg = "Upload from " + remoteServerName + " failed; " + "XMLException creating new version " + dar.getVersion();
                throw new ServletProcessException(msg, ((Throwable) (xmle)), "ServerRequestFailed", dar.getApplication(), dar.getVersion(), remoteServerName, true);
            }
            File meta_inf = new File(versionDir, "META-INF");
            FileUtils.deleteDirectory(meta_inf);
            if(!super.vault.createApplicationVersionsFile(app, ((File) (null)), true))
            {
                if(addAppDir)
                    FileUtils.deleteDirectory(appDir);
                else
                    FileUtils.deleteDirectory(versionDir);
                String msg = "Upload from " + remoteServerName + " failed; " + "error creating application versions file for " + dar.getApplication() + ", " + dar.getVersion();
                throw new ServletProcessException(msg, "ServerRequestFailed", dar.getApplication(), dar.getVersion(), remoteServerName, true);
            }
            if(!super.vault.createApplicationsFile(((File) (null)), true))
            {
                if(addAppDir)
                    FileUtils.deleteDirectory(appDir);
                else
                    FileUtils.deleteDirectory(versionDir);
                String msg = "Upload from " + remoteServerName + " failed; " + "error creating applications file for " + dar.getApplication();
                throw new ServletProcessException(msg, "ServerRequestFailed", dar.getApplication(), dar.getVersion(), remoteServerName, true);
            }
            ((BaseCheckpoint) (checkpoint)).setState(600);
        }
        ((BaseCheckpoint) (checkpoint)).setState(0xf423f);
    }
}
