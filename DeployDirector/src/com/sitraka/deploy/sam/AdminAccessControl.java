// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AdminAccessControl.java

package com.sitraka.deploy.sam;

import com.sitraka.deploy.common.app.AccessControl;

public class AdminAccessControl extends AccessControl
{

    public AdminAccessControl()
    {
        ((AccessControl)this).setClientAuthentication("com.sitraka.deploy.authentication.ClientUsernamePassword");
        ((AccessControl)this).setAuthentication("com.sitraka.deploy.admin.auth.AdminAuthentication");
        ((AccessControl)this).setAuthorization("com.sitraka.deploy.admin.auth.AdminAuthorization");
        ((AccessControl)this).setAuthGroups("com.sitraka.deploy.authentication.SimpleAuthGroups");
        ((AccessControl)this).setAllowCache(false);
    }
}
