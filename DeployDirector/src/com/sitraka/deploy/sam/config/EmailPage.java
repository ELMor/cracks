// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   EmailPage.java

package com.sitraka.deploy.sam.config;

import com.sitraka.deploy.sam.config.resources.LocaleInfo;
import com.sitraka.deploy.util.PropertyUtils;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.EventObject;
import java.util.Hashtable;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

// Referenced classes of package com.sitraka.deploy.sam.config:
//            ConfigPage, EmailAuthenticator, ProcessingPage, ConfigWizardGUI

public class EmailPage extends ConfigPage
    implements ActionListener
{

    protected static ResourceBundle li;
    public static final String TITLE = "DDConfWizEmailTitle";
    public static final String SOURCE_TITLE = "DDConfWizEmailSourceTitle";
    public static final String RECIPIENT_TITLE = "DDConfWizEmailRecipientTitle";
    public static final String MAIN_DOCS = "DDConfWizEmailMainDoc";
    public static final String TEST = "DDConfiWizEmailTest";
    public static final String DISABLE_EMAIL = "DDConfWizEmailDisableEmail";
    public static final String ADMIN_ACCOUNT = "DDConfWizEmailAdminAccount";
    public static final String PASSWORD = "DDConfWizEmailPassword";
    public static final String HOST = "DDConfWizEmailHost";
    public static final String USER_ACCOUNT = "DDConfWizEmailUserAccount";
    public static final String NOTIFICATION = "DDConfWizEmailNotification";
    public static final String CONTINUE = "DDConfWizEmailContinue";
    public static final String HOST_NOT_FOUND = "DDConfWizEmailHostNotFound";
    public static final String INVALID_ADMIN_ACCOUNT = "DDConfWizEmailInvalidAdminAccount";
    public static final String INVALID_USER_ACCOUNT = "DDConfWizEmailInvalidUserAccount";
    public static final String VALIDATION_ERROR = "DDConfWizEmailValidationError";
    public static final String NOTIFICATION_UNSELECTED = "DDConfWizEmailNotificationUnselected";
    public static final String ADMIN_ACCOUNT_MISSING = "DDConfWizEmailAdminAccountRequired";
    public static final String USER_ACCOUNT_MISSING = "DDConfWizEmailUserAccountRequired";
    public static final String MAIL_HOST_MISSING = "DDConfWizEmailMailHostRequired";
    public static final String NO_PASSWORD_PROVIDED = "DDConfWizEmailNoPassword";
    public static final String CONFIG_UNTESTED = "DDConfWizEmailUntestedConfig";
    public static final String TEST_FAILED = "DDConfWizEmailTestFailed";
    public static final String EMAIL_SUBJECT = "DDConfWizEmailEmailSubject";
    public static final String EMAIL_CONTENT = "DDConfWizEmailEmailContext";
    public static final String ERROR = "DDConfWizEmailError";
    public static final String EMAIL_DISABLED = "DDConfWizEmailEmailDisabled";
    protected JTextField adminAccount;
    protected JPasswordField password;
    protected JTextField host;
    protected JTextField userAccount;
    protected JList levels;
    protected JCheckBox disableEmail;
    protected JButton test;
    protected JPanel source;
    protected JPanel recipient;
    protected Vector levelsList;
    protected boolean configurationTested;
    protected boolean acceptedEmptyPassword;
    protected String oldAdminAccount;
    protected String oldUserAccount;
    protected String oldPassword;
    protected String oldHost;
    protected Object oldNotificationLevel[];
    protected Properties clusterProps;

    public EmailPage(ConfigWizardGUI gui, int buttons)
    {
        super(gui, buttons);
        adminAccount = new JTextField();
        password = new JPasswordField();
        host = new JTextField();
        userAccount = new JTextField();
        levels = new JList();
        disableEmail = new JCheckBox(li.getString("DDConfWizEmailDisableEmail"));
        test = new JButton(li.getString("DDConfiWizEmailTest"));
        source = new JPanel(((java.awt.LayoutManager) (new GridBagLayout())));
        recipient = new JPanel(((java.awt.LayoutManager) (new GridBagLayout())));
        configurationTested = false;
        acceptedEmptyPassword = false;
        oldAdminAccount = new String("");
        oldUserAccount = new String("");
        oldPassword = new String("");
        oldHost = new String("");
        oldNotificationLevel = null;
        clusterProps = ProcessingPage.getClusterProperties();
        JPanel page = new JPanel(((java.awt.LayoutManager) (new GridBagLayout())));
        ((AbstractButton) (disableEmail)).addActionListener(((ActionListener) (this)));
        ((AbstractButton) (test)).addActionListener(((ActionListener) (this)));
        ((JComponent) (source)).setBorder(((javax.swing.border.Border) (BorderFactory.createTitledBorder(li.getString("DDConfWizEmailSourceTitle")))));
        ((JComponent) (recipient)).setBorder(((javax.swing.border.Border) (BorderFactory.createTitledBorder(li.getString("DDConfWizEmailRecipientTitle")))));
        ((Container) (source)).add(((java.awt.Component) (new JLabel(li.getString("DDConfWizEmailAdminAccount")))), ((Object) (new GridBagConstraints(0, 0, 1, 1, 0.0D, 0.0D, 12, 0, new Insets(2, 10, 2, 2), 0, 0))));
        ((Container) (source)).add(((java.awt.Component) (adminAccount)), ((Object) (new GridBagConstraints(1, 0, 2, 1, 1.0D, 0.0D, 18, 2, new Insets(2, 2, 2, 10), 0, 0))));
        ((Container) (source)).add(((java.awt.Component) (new JLabel(li.getString("DDConfWizEmailPassword")))), ((Object) (new GridBagConstraints(0, 1, 1, 1, 0.0D, 0.0D, 12, 0, new Insets(2, 10, 2, 2), 0, 0))));
        ((Container) (source)).add(((java.awt.Component) (password)), ((Object) (new GridBagConstraints(1, 1, 2, 1, 1.0D, 0.0D, 18, 2, new Insets(2, 2, 2, 10), 0, 0))));
        ((Container) (source)).add(((java.awt.Component) (new JLabel(li.getString("DDConfWizEmailHost")))), ((Object) (new GridBagConstraints(0, 2, 1, 1, 0.0D, 0.0D, 12, 0, new Insets(2, 10, 2, 2), 0, 0))));
        ((Container) (source)).add(((java.awt.Component) (host)), ((Object) (new GridBagConstraints(1, 2, 2, 1, 1.0D, 0.0D, 18, 2, new Insets(2, 2, 2, 10), 0, 0))));
        ((Container) (recipient)).add(((java.awt.Component) (new JLabel(li.getString("DDConfWizEmailUserAccount")))), ((Object) (new GridBagConstraints(0, 0, 1, 1, 0.0D, 0.0D, 12, 0, new Insets(2, 10, 2, 2), 0, 0))));
        ((Container) (recipient)).add(((java.awt.Component) (userAccount)), ((Object) (new GridBagConstraints(1, 0, 1, 1, 1.0D, 0.0D, 18, 2, new Insets(2, 2, 2, 10), 0, 0))));
        ((Container) (recipient)).add(((java.awt.Component) (new JLabel(li.getString("DDConfWizEmailNotification")))), ((Object) (new GridBagConstraints(0, 1, 1, 1, 0.0D, 0.0D, 12, 0, new Insets(2, 10, 2, 2), 0, 0))));
        ((Container) (recipient)).add(((java.awt.Component) (new JScrollPane(((java.awt.Component) (levels))))), ((Object) (new GridBagConstraints(1, 1, 1, 1, 1.0D, 1.0D, 18, 1, new Insets(2, 2, 2, 10), 0, 0))));
        ((Container) (page)).add(((java.awt.Component) (source)), ((Object) (new GridBagConstraints(0, 0, 2, 1, 1.0D, 0.5D, 18, 1, new Insets(20, 20, 2, 20), 0, 0))));
        ((Container) (page)).add(((java.awt.Component) (recipient)), ((Object) (new GridBagConstraints(0, 1, 2, 1, 1.0D, 0.5D, 18, 1, new Insets(20, 20, 2, 20), 0, 0))));
        ((Container) (page)).add(((java.awt.Component) (disableEmail)), ((Object) (new GridBagConstraints(0, 2, 1, 1, 0.0D, 0.0D, 18, 0, new Insets(10, 20, 20, 2), 0, 0))));
        ((Container) (page)).add(((java.awt.Component) (test)), ((Object) (new GridBagConstraints(1, 2, 1, 1, 0.0D, 0.0D, 12, 0, new Insets(10, 2, 20, 20), 0, 0))));
        ((ConfigPage)this).setContentPanel(page);
        ((ConfigPage)this).setTitle(li.getString("DDConfWizEmailTitle"));
        enableDisable();
        initializeLevelsList();
    }

    protected void enableDisable()
    {
        boolean enable = ((AbstractButton) (disableEmail)).isSelected();
        ((JComponent) (adminAccount)).setEnabled(!enable);
        ((JComponent) (password)).setEnabled(!enable);
        ((JComponent) (host)).setEnabled(!enable);
        ((JComponent) (userAccount)).setEnabled(!enable);
        ((JComponent) (levels)).setEnabled(!enable);
        ((JComponent) (source)).setEnabled(!enable);
        ((JComponent) (recipient)).setEnabled(!enable);
        if(isDisabledEmail())
        {
            setAdminAccount("");
            setUserAccount("");
            setPassword("");
            setHost("");
            setSelectedLevels(((Object []) (null)));
            clearProperties();
        }
    }

    public void test()
    {
        Properties mailProps = new Properties();
        if(((AbstractButton) (disableEmail)).isSelected())
        {
            JOptionPane.showMessageDialog(((java.awt.Component) (this)), ((Object) (li.getString("DDConfWizEmailEmailDisabled"))), li.getString("DDConfWizEmailError"), 0);
            return;
        }
        if(!validatePage())
            return;
        configurationTested = true;
        ((Frame) (super.gui)).setCursor(3);
        try
        {
            ((Hashtable) (mailProps)).put("mail.smtp.host", ((Object) (getHost())));
            ((Hashtable) (mailProps)).put("mail.smtp.user", ((Object) (getAdminAccount())));
            Session mailSession = Session.getInstance(mailProps, ((javax.mail.Authenticator) (new EmailAuthenticator(getAdminAccount(), new String(getPassword())))));
            MimeMessage msg = new MimeMessage(mailSession);
            msg.setFrom(((javax.mail.Address) (new InternetAddress(getAdminAccount()))));
            ((Message) (msg)).setRecipient(javax.mail.Message.RecipientType.TO, ((javax.mail.Address) (new InternetAddress(getUserAccount()))));
            msg.setSubject(li.getString("DDConfWizEmailEmailSubject"));
            msg.setContent(((Object) (li.getString("DDConfWizEmailEmailContext"))), "text/plain");
            Transport.send(((Message) (msg)));
        }
        catch(MessagingException me)
        {
            JOptionPane.showMessageDialog(((java.awt.Component) (this)), ((Object) (li.getString("DDConfWizEmailTestFailed") + "\n" + ((Throwable) (me)).toString())), li.getString("DDConfWizEmailError"), 0);
            configurationTested = false;
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(((java.awt.Component) (this)), ((Object) (li.getString("DDConfWizEmailTestFailed") + "\n" + ((Throwable) (e)).toString())), li.getString("DDConfWizEmailError"), 0);
            configurationTested = false;
        }
        catch(Throwable t)
        {
            JOptionPane.showMessageDialog(((java.awt.Component) (this)), ((Object) (li.getString("DDConfWizEmailTestFailed") + "\n" + t.toString())), li.getString("DDConfWizEmailError"), 0);
            configurationTested = false;
        }
        try
        {
            if(configurationTested)
            {
                Thread.currentThread();
                Thread.sleep(10000L);
            }
        }
        catch(Exception e) { }
        ((Window) (super.gui)).setCursor(Cursor.getDefaultCursor());
        oldAdminAccount = getAdminAccount();
        oldUserAccount = getUserAccount();
        oldHost = getHost();
        oldPassword = getPassword();
        oldNotificationLevel = getSelectedLevels();
    }

    protected void setDisableEmail(boolean disable)
    {
        ((AbstractButton) (disableEmail)).setSelected(disable);
        enableDisable();
    }

    protected boolean isDisabledEmail()
    {
        return ((AbstractButton) (disableEmail)).isSelected();
    }

    protected void setAdminAccount(String adminAccount)
    {
        ((JTextComponent) (this.adminAccount)).setText(adminAccount);
    }

    protected String getAdminAccount()
    {
        return ((JTextComponent) (adminAccount)).getText();
    }

    protected void setPassword(String pswd)
    {
        ((JTextComponent) (password)).setText(pswd);
    }

    protected String getPassword()
    {
        return new String(password.getPassword());
    }

    protected void setHost(String host)
    {
        ((JTextComponent) (this.host)).setText(host);
    }

    protected String getHost()
    {
        return ((JTextComponent) (host)).getText();
    }

    protected void setUserAccount(String userAccount)
    {
        ((JTextComponent) (this.userAccount)).setText(userAccount);
    }

    protected String getUserAccount()
    {
        return ((JTextComponent) (userAccount)).getText();
    }

    protected void setLevels(Vector levelsList)
    {
        levels.setModel(((javax.swing.ListModel) (new DefaultComboBoxModel(levelsList))));
        this.levelsList = levelsList;
    }

    public void setSelectedLevels(Object selectedLevels[])
    {
        Vector indexList = new Vector();
        if(selectedLevels == null)
        {
            levels.clearSelection();
        } else
        {
            for(int i = 0; i < selectedLevels.length; i++)
            {
                int index = levelsList.indexOf(selectedLevels[i]);
                if(index != -1)
                    indexList.add(((Object) (new Integer(index))));
            }

            int selectedIndices[] = new int[indexList.size()];
            for(int i = 0; i < selectedIndices.length; i++)
                selectedIndices[i] = ((Integer)indexList.elementAt(i)).intValue();

            levels.setSelectedIndices(selectedIndices);
        }
    }

    public Object[] getSelectedLevels()
    {
        return levels.getSelectedValues();
    }

    protected boolean beforeNext()
    {
        compareSettings();
        if(validatePage())
        {
            if(isDefaultConfiguration())
            {
                ProcessingPage.setClusterProperties(clusterProps);
                return true;
            }
            if(!configurationTested)
            {
                int confirm = JOptionPane.showConfirmDialog(((java.awt.Component) (this)), ((Object) (li.getString("DDConfWizEmailUntestedConfig"))), li.getString("DDConfWizEmailContinue"), 0);
                if(confirm == 1)
                    return false;
            }
            ProcessingPage.setClusterProperties(clusterProps);
            return true;
        } else
        {
            return false;
        }
    }

    protected boolean beforePrevious()
    {
        return true;
    }

    protected void refreshPage()
    {
        Vector selected = new Vector();
        ((JComponent) (super.next)).requestFocus();
        super.genericPageUpdate();
        setAdminAccount(clusterProps.getProperty("deploy.error.email.user"));
        setUserAccount(clusterProps.getProperty("deploy.error.email.0.address"));
        setHost(clusterProps.getProperty("deploy.error.email.server"));
        String pswd = clusterProps.getProperty("deploy.error.email.password");
        if(getAdminAccount().equals("") || getAdminAccount() == null || pswd.equals("") || pswd == null)
            setPassword("");
        else
            setPassword(PropertyUtils.parsePassword(pswd));
        String levels = clusterProps.getProperty("deploy.error.email.0.notification");
        StringTokenizer st = new StringTokenizer(levels, ",");
        int counter = 0;
        for(; st.hasMoreElements(); selected.addElement(((Object) (((String)st.nextElement()).trim()))));
        Object list[] = new Object[selected.size()];
        for(int i = 0; i < list.length; i++)
            list[i] = selected.elementAt(i);

        setDisableEmail(isDefaultConfiguration());
        setLevels(levelsList);
        setSelectedLevels(list);
    }

    protected String getDocs()
    {
        return li.getString("DDConfWizEmailMainDoc");
    }

    protected void initializeLevelsList()
    {
        levelsList = new Vector(8);
        levelsList.addElement("bundle.DDCAM");
        levelsList.addElement("bundle.DDAdmin");
        levelsList.addElement("bundle.DDSDK");
        levelsList.addElement("bundle.all");
        levelsList.addElement("client.connection");
        levelsList.addElement("client.local");
        levelsList.addElement("server.connection");
        levelsList.addElement("server.local");
    }

    protected void compareSettings()
    {
        if(oldAdminAccount.equals(((Object) (getAdminAccount()))) && oldUserAccount.equals(((Object) (getUserAccount()))) && oldHost.equals(((Object) (getHost()))) && oldPassword.equals(((Object) (getPassword()))))
            configurationTested = true;
        else
            configurationTested = false;
    }

    protected boolean isDefaultConfiguration()
    {
        return (getAdminAccount().equals("") || getAdminAccount() == null) && (getUserAccount().equals("") || getUserAccount() == null) && (getHost().equals("") || getHost() == null) && (getPassword().equals("") || getPassword() == null) && levels.isSelectionEmpty();
    }

    protected boolean validatePage()
    {
        if(isDisabledEmail())
        {
            clearProperties();
            return true;
        }
        if(!allFieldsCompleted())
            return false;
        if(getAdminAccount().indexOf('@') == -1)
        {
            validationError(li.getString("DDConfWizEmailInvalidAdminAccount"));
            return false;
        }
        if(getUserAccount().indexOf('@') == -1)
        {
            validationError(li.getString("DDConfWizEmailInvalidUserAccount"));
            return false;
        }
        try
        {
            InetAddress smtpHost = InetAddress.getByName(getHost());
            if(smtpHost == null)
            {
                int confirm = JOptionPane.showConfirmDialog(((java.awt.Component) (this)), ((Object) (li.getString("DDConfWizEmailHostNotFound"))), li.getString("DDConfWizEmailContinue"), 0);
                if(confirm == 1)
                    return false;
            }
        }
        catch(UnknownHostException uhe)
        {
            int confirm = JOptionPane.showConfirmDialog(((java.awt.Component) (this)), ((Object) (li.getString("DDConfWizEmailHostNotFound"))), li.getString("DDConfWizEmailContinue"), 0);
            if(confirm == 1)
                return false;
        }
        updateProperties();
        return true;
    }

    protected boolean allFieldsCompleted()
    {
        String account = getAdminAccount();
        int index = account.indexOf(",");
        if(index != -1)
            setAdminAccount(account.substring(0, index));
        index = account.indexOf(" ");
        if(index != -1)
            setAdminAccount(account.substring(0, index));
        account = getUserAccount();
        index = account.indexOf(",");
        if(index != -1)
            setUserAccount(account.substring(0, index));
        index = account.indexOf(" ");
        if(index != -1)
            setUserAccount(account.substring(0, index));
        if(getAdminAccount() == null || getAdminAccount().equals(""))
        {
            validationError(li.getString("DDConfWizEmailAdminAccountRequired"));
            return false;
        }
        if(getUserAccount() == null || getUserAccount().equals(""))
        {
            validationError(li.getString("DDConfWizEmailUserAccountRequired"));
            return false;
        }
        if(getHost() == null || getHost().equals(""))
        {
            validationError(li.getString("DDConfWizEmailMailHostRequired"));
            return false;
        }
        if(levels.isSelectionEmpty())
        {
            int confirm = JOptionPane.showConfirmDialog(((java.awt.Component) (this)), ((Object) (li.getString("DDConfWizEmailNotificationUnselected"))), li.getString("DDConfWizEmailContinue"), 0);
            if(confirm == 1)
                return false;
        }
        if((getPassword() == null || getPassword().equals("")) && !acceptedEmptyPassword)
        {
            int confirm = JOptionPane.showConfirmDialog(((java.awt.Component) (this)), ((Object) (li.getString("DDConfWizEmailNoPassword"))), li.getString("DDConfWizEmailContinue"), 0);
            if(confirm == 1)
                return false;
            acceptedEmptyPassword = true;
        }
        return true;
    }

    protected void validationError(String msg)
    {
        JOptionPane.showMessageDialog(((java.awt.Component) (this)), ((Object) (msg)), li.getString("DDConfWizEmailValidationError"), 0);
    }

    protected void updateProperties()
    {
        String notification = new String("");
        Object selectedLevels[] = getSelectedLevels();
        for(int i = 0; i < selectedLevels.length; i++)
        {
            notification = notification + (String)selectedLevels[i];
            if(i != selectedLevels.length - 1)
                notification = notification + ",";
        }

        String encoded_pw;
        if(getAdminAccount().equals("") || getAdminAccount() == null || getPassword().equals("") || getPassword() == null)
            encoded_pw = new String("");
        else
            encoded_pw = PropertyUtils.writePassword(getPassword());
        ((Hashtable) (clusterProps)).put("deploy.error.email.0.notification", ((Object) (notification)));
        ((Hashtable) (clusterProps)).put("deploy.error.email.0.address", ((Object) (getUserAccount())));
        ((Hashtable) (clusterProps)).put("deploy.error.email.user", ((Object) (getAdminAccount())));
        ((Hashtable) (clusterProps)).put("deploy.error.email.password", ((Object) (encoded_pw)));
        ((Hashtable) (clusterProps)).put("deploy.error.email.server", ((Object) (((JTextComponent) (host)).getText())));
    }

    protected void clearProperties()
    {
        ((Hashtable) (clusterProps)).put("deploy.error.email.0.notification", "");
        ((Hashtable) (clusterProps)).put("deploy.error.email.0.address", "");
        ((Hashtable) (clusterProps)).put("deploy.error.email.user", "");
        ((Hashtable) (clusterProps)).put("deploy.error.email.password", "");
        ((Hashtable) (clusterProps)).put("deploy.error.email.server", "");
    }

    public void actionPerformed(ActionEvent e)
    {
        if(((EventObject) (e)).getSource() == disableEmail)
            enableDisable();
        else
        if(((EventObject) (e)).getSource() == test)
            test();
    }

    static 
    {
        li = LocaleInfo.li;
    }
}
