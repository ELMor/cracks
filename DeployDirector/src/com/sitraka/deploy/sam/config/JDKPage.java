// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JDKPage.java

package com.sitraka.deploy.sam.config;

import java.awt.Container;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.EventObject;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.JTextComponent;

// Referenced classes of package com.sitraka.deploy.sam.config:
//            ConfigPage, ProcessingPage, ConfigWizardGUI

public class JDKPage extends ConfigPage
    implements ActionListener, ListSelectionListener
{

    public static final String TITLE = "DDConfWizJDKTitle";
    public static final String UPDATE = "DDConfWizJDKUpdate";
    public static final String NOTE = "DDConfWizJDKNote";
    public static final String LOCATION = "DDConfWizJDKLocation";
    public static final String SEARCH = "DDConfWizJDKSearch";
    public static final String DOC = "DDConfWizJDKDoc";
    public static final String JDK_ERROR = "DDConfWizJDKError";
    public static final String JDK_RESELECT = "DDConfWizJDKReselect";
    protected JTextField jdkLocation;
    protected JList locationList;
    protected JButton update;
    protected JButton browse;

    public JDKPage(ConfigWizardGUI gui, int buttons)
    {
        super(gui, buttons);
        jdkLocation = new JTextField();
        locationList = new JList();
        update = new JButton(ConfigPage.li.getString("DDConfWizJDKUpdate"));
        browse = new JButton("Browse");
        JPanel fields = new JPanel();
        ((Container) (fields)).setLayout(((java.awt.LayoutManager) (new GridBagLayout())));
        ((AbstractButton) (update)).addActionListener(((ActionListener) (this)));
        ((AbstractButton) (browse)).addActionListener(((ActionListener) (this)));
        locationList.addListSelectionListener(((ListSelectionListener) (this)));
        ((Container) (fields)).add(((java.awt.Component) (new JLabel(ConfigPage.li.getString("DDConfWizJDKNote")))), ((Object) (new GridBagConstraints(0, 0, 3, 1, 0.0D, 0.0D, 18, 0, new Insets(20, 20, 2, 20), 0, 0))));
        ((Container) (fields)).add(((java.awt.Component) (new JLabel(ConfigPage.li.getString("DDConfWizJDKLocation")))), ((Object) (new GridBagConstraints(0, 1, 1, 1, 0.0D, 0.0D, 17, 0, new Insets(20, 20, 2, 2), 0, 0))));
        ((Container) (fields)).add(((java.awt.Component) (jdkLocation)), ((Object) (new GridBagConstraints(1, 1, 1, 1, 1.0D, 0.0D, 10, 2, new Insets(20, 2, 2, 2), 0, 0))));
        ((Container) (fields)).add(((java.awt.Component) (browse)), ((Object) (new GridBagConstraints(2, 1, 1, 1, 0.0D, 0.0D, 17, 0, new Insets(20, 2, 2, 20), 0, 0))));
        ((Container) (fields)).add(((java.awt.Component) (new JLabel(ConfigPage.li.getString("DDConfWizJDKSearch")))), ((Object) (new GridBagConstraints(0, 2, 3, 1, 0.0D, 0.0D, 18, 0, new Insets(20, 20, 2, 2), 0, 0))));
        ((Container) (fields)).add(((java.awt.Component) (new JScrollPane(((java.awt.Component) (locationList))))), ((Object) (new GridBagConstraints(0, 3, 3, 1, 1.0D, 1.0D, 18, 1, new Insets(2, 20, 2, 20), 0, 0))));
        ((Container) (fields)).add(((java.awt.Component) (update)), ((Object) (new GridBagConstraints(2, 4, 1, 1, 0.0D, 0.0D, 12, 0, new Insets(2, 2, 20, 20), 0, 0))));
        ((ConfigPage)this).setContentPanel(fields);
        ((ConfigPage)this).setTitle(ConfigPage.li.getString("DDConfWizJDKTitle"));
    }

    public String getJDKLocation()
    {
        return ((JTextComponent) (jdkLocation)).getText();
    }

    public void setJDKLocation(String location)
    {
        ((JTextComponent) (jdkLocation)).setText(location);
    }

    public void updateJDKList()
    {
        ((Frame) (super.gui)).setCursor(3);
        Vector jvms = findNativeJVMs();
        Vector unique = new Vector();
        for(int i = 0; i < jvms.size(); i++)
        {
            boolean add = true;
            for(int j = i + 1; j < jvms.size(); j++)
                if(jvms.elementAt(i).equals(jvms.elementAt(j)))
                    add = false;

            if(add)
                unique.addElement(jvms.elementAt(i));
        }

        locationList.setModel(((ListModel) (new DefaultComboBoxModel(unique))));
        ((Window) (super.gui)).setCursor(Cursor.getDefaultCursor());
    }

    public void selectJDKDirectory()
    {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(1);
        if(chooser.showOpenDialog(((java.awt.Component) (this))) == 0)
            setJDKLocation(chooser.getSelectedFile().toString());
    }

    protected boolean beforeNext()
    {
        String jdk_home = ((JTextComponent) (jdkLocation)).getText();
        if(confirmJDK(jdk_home))
        {
            saveConfig();
            return rewriteTomcatFiles(jdk_home);
        } else
        {
            JOptionPane.showMessageDialog(((java.awt.Component) (super.gui)), ((Object) (ConfigPage.li.getString("DDConfWizJDKReselect"))), ConfigPage.li.getString("DDConfWizJDKError"), 1);
            return false;
        }
    }

    protected boolean beforePrevious()
    {
        return true;
    }

    protected void refreshPage()
    {
        Vector jvms = new Vector();
        ((ConfigPage)this).genericPageUpdate();
        Properties wizConfig = ProcessingPage.getWizardProperties();
        String currentJVM;
        for(int i = 0; i >= 0; i++)
        {
            currentJVM = wizConfig.getProperty("deploy.jvm.archive." + (new Integer(i)).toString(), new String("done"));
            if(currentJVM.equals("done"))
                break;
            jvms.addElement(((Object) (currentJVM)));
        }

        currentJVM = wizConfig.getProperty("deploy.jvm.selected");
        locationList.setModel(((ListModel) (new DefaultComboBoxModel(jvms))));
        setJDKLocation(currentJVM);
    }

    protected String getDocs()
    {
        return ConfigPage.li.getString("DDConfWizJDKDoc");
    }

    protected void saveConfig()
    {
        Properties wizConfig = new Properties();
        String wizConfigFile = System.getProperty("DDConfig.home") + File.separator + "wizard.properties";
        ((Hashtable) (wizConfig)).put("deploy.jvm.selected", ((Object) (getJDKLocation())));
        ListModel data = locationList.getModel();
        if(data.getSize() != 0)
        {
            for(int i = 0; i < data.getSize(); i++)
                ((Hashtable) (wizConfig)).put(((Object) ("deploy.jvm.archive." + (new Integer(i)).toString())), data.getElementAt(i));

        }
        ProcessingPage.setWizardProperties(wizConfig);
    }

    protected String detectOS()
    {
        String os = System.getProperty("os.name");
        if(os.indexOf("Windows") != -1)
            return "windows";
        if(os.toLowerCase().indexOf("sun") != -1)
            return "unix";
        if(os.toLowerCase().indexOf("aix") != -1)
            return "unix";
        if(os.toLowerCase().indexOf("linux") != -1)
            return "unix";
        if(os.toLowerCase().indexOf("hp") != -1)
            return "unix";
        else
            return null;
    }

    protected boolean rewriteTomcatFiles(String jdk_home)
    {
        String tomcat = System.getProperty("DDConfig.home") + File.separator + "standalone" + File.separator;
        if(detectOS().equals("windows"))
            try
            {
                File startup = new File(tomcat + "startup.bat");
                if(startup.exists())
                    startup.delete();
                String newContent = new String("");
                newContent = newContent + "@echo off\r\n";
                newContent = newContent + "set \"JAVA_HOME=" + jdk_home + "\"\r\n";
                newContent = newContent + "call tomcat start %1\r\n";
                newContent = newContent + "pause";
                FileWriter output = new FileWriter(startup);
                ((Writer) (output)).write(newContent);
                ((OutputStreamWriter) (output)).flush();
                ((OutputStreamWriter) (output)).close();
                File shutdown = new File(tomcat + "shutdown.bat");
                if(shutdown.exists())
                    shutdown.delete();
                newContent = new String("");
                newContent = newContent + "@echo off\r\n";
                newContent = newContent + "set \"JAVA_HOME=" + jdk_home + "\"\r\n";
                newContent = newContent + "call tomcat stop\r\n";
                output = new FileWriter(shutdown);
                ((Writer) (output)).write(newContent);
                ((OutputStreamWriter) (output)).flush();
                ((OutputStreamWriter) (output)).close();
                return true;
            }
            catch(IOException ioe) { }
        else
        if(detectOS().equals("unix"))
            try
            {
                File startup = new File(tomcat + "startup.sh");
                if(startup.exists())
                    startup.delete();
                String newContent = new String("");
                newContent = newContent + "#! /bin/sh\n";
                newContent = newContent + "JAVA_HOME=\"" + jdk_home + "\" \n";
                newContent = newContent + "export JAVA_HOME\n";
                newContent = newContent + "BASEDIR=`dirname $0`\n";
                newContent = newContent + "$BASEDIR" + File.separator + "tomcat.sh start \"$@\"\n";
                FileWriter output = new FileWriter(startup);
                ((Writer) (output)).write(newContent);
                ((OutputStreamWriter) (output)).flush();
                ((OutputStreamWriter) (output)).close();
                File shutdown = new File(tomcat + "shutdown.sh");
                if(shutdown.exists())
                    shutdown.delete();
                newContent = new String("");
                newContent = newContent + "#! /bin/sh\n";
                newContent = newContent + "JAVA_HOME=\"" + jdk_home + "\" \n";
                newContent = newContent + "export JAVA_HOME\n";
                newContent = newContent + "BASEDIR=`dirname $0`\n";
                newContent = newContent + "$BASEDIR" + File.separator + "tomcat.sh stop \"$@\"\n";
                output = new FileWriter(shutdown);
                ((Writer) (output)).write(newContent);
                ((OutputStreamWriter) (output)).flush();
                ((OutputStreamWriter) (output)).close();
                return true;
            }
            catch(IOException ioe) { }
        return false;
    }

    protected Vector findNativeJVMs()
    {
        String ddcam_dir = System.getProperty("DDConfig.home") + File.separator + "vault" + File.separator + "ddcam" + File.separator + "2.5.0" + File.separator;
        Vector jvm_list = new Vector();
        String out_string = "";
        if(detectOS().equals("windows"))
            try
            {
                Runtime rt = Runtime.getRuntime();
                Process p = rt.exec(ddcam_dir + "windows" + File.separator + "i386" + File.separator + "deployjrefinder.exe");
                p.waitFor();
                InputStream out = p.getInputStream();
                byte buffer[] = new byte[100];
                int i;
                while((i = out.read(buffer, 0, 100)) != -1) 
                    out_string = out_string + new String(buffer, 0, i);
                jvm_list = parseJVMString(out_string);
            }
            catch(IOException e) { }
            catch(InterruptedException e) { }
        else
        if(detectOS().equals("unix"))
            try
            {
                Runtime rt = Runtime.getRuntime();
                Process p = rt.exec("sh " + ddcam_dir + "unix" + File.separator + "deployjrefinder");
                InputStream out = p.getInputStream();
                byte buffer[] = new byte[100];
                int j;
                while((j = out.read(buffer, 0, 100)) != -1) 
                {
                    out_string = out_string + new String(buffer, 0, j);
                    if(out_string.indexOf("***DONE***") > 0)
                        break;
                    Thread.currentThread();
                    Thread.sleep(500L);
                }
                p.destroy();
                jvm_list = parseJVMString(out_string);
            }
            catch(IOException e) { }
            catch(InterruptedException e) { }
        else
            jvm_list.addElement("None found.");
        return jvm_list;
    }

    protected Vector parseJVMString(String jvm_string)
    {
        Vector jvm_list = new Vector();
        StringTokenizer jres;
        if(detectOS().equals("unix"))
            jres = new StringTokenizer(jvm_string, "\n");
        else
            jres = new StringTokenizer(jvm_string, "\r\n");
        while(jres.hasMoreTokens()) 
        {
            String entry = jres.nextToken();
            boolean jre_found = false;
            if(!detectOS().equals("unix") || !entry.equals("***DONE***"))
            {
                StringTokenizer jre_info = new StringTokenizer(entry, ",");
                try
                {
                    String jre_vendor = jre_info.nextToken();
                    String version = jre_info.nextToken();
                    String microversion = jre_info.nextToken();
                    String java_home = jre_info.nextToken();
                    if(detectOS().equals("unix"))
                        java_home = java_home.substring(0, java_home.lastIndexOf(File.separator, java_home.lastIndexOf(File.separator) - 1));
                    jre_found = confirmJDK(java_home);
                    if(jre_found)
                        jvm_list.addElement(((Object) (java_home)));
                }
                catch(NoSuchElementException e) { }
            }
        }
        return jvm_list;
    }

    protected boolean confirmJDK(String jdk_home)
    {
        String javaStyles[] = {
            "jrew.exe", "javaw.exe", "jre", "java"
        };
        String separator = File.separator;
        String java_exec = jdk_home + separator + "bin" + separator;
        String java_tools = jdk_home + separator + "lib" + separator + "tools.jar";
        String jre_name = null;
        for(int i = 0; i < javaStyles.length; i++)
        {
            jre_name = java_exec + javaStyles[i];
            if((new File(jre_name)).exists() && (new File(java_tools)).exists())
                return true;
        }

        return false;
    }

    public void actionPerformed(ActionEvent e)
    {
        if(((EventObject) (e)).getSource() == update)
            updateJDKList();
        if(((EventObject) (e)).getSource() == browse)
            selectJDKDirectory();
    }

    public void valueChanged(ListSelectionEvent e)
    {
        if(((EventObject) (e)).getSource() == locationList && e.getFirstIndex() != -1)
            ((JTextComponent) (jdkLocation)).setText((String)locationList.getSelectedValue());
    }
}
