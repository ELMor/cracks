// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   LicensePage.java

package com.sitraka.deploy.sam.config;

import com.sitraka.deploy.sam.config.resources.LocaleInfo;
import com.sitraka.licensing.InvalidLicenseException;
import com.sitraka.licensing.LicenseProperties;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.EventObject;
import java.util.Hashtable;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

// Referenced classes of package com.sitraka.deploy.sam.config:
//            ConfigPage, ProcessingPage, ExternalBrowser, ConfigWizardGUI

public class LicensePage
    extends ConfigPage
    implements ActionListener {

  protected static ResourceBundle li;
  public static final String TITLE = "License";
  public static final String SERIAL_NUMBER = "DDConfWizLicenseSerialNumber";
  public static final String EXPIRY = "DDConfWizLicenseExpiry";
  public static final String BUNDLES = "DDConfWizLicenseBundles";
  public static final String CLIENTS = "DDConfWizLicenseClients";
  public static final String HOSTS = "DDConfWizLicenseHosts";
  public static final String APPLICATIONS = "DDConfWizLicenseApplications";
  public static final String SIGNATURE = "DDConfWizLicenseSignature";
  public static final String TYPE = "DDConfWizLicenseType";
  public static final String DD_VERSION = "DDConfWizLicenseDDVersion";
  public static final String VERSION = "DDConfWizLicenseVersion";
  public static final String ADD_LICENSE = "DDConfWizLicenseAddLicense";
  public static final String REPLACE_LICENSE = "DDConfWizLicenseReplaceLicense";
  public static final String BUY_LICENSE = "DDConfWizLicenseBuyLicense";
  public static final String OBTAIN_LICENSE = "DDConfWizLicenseObtainLicense";
  public static final String PRODUCT_SITE = "DDConfWizLicenseProductSite";
  public static final String NOT_APPLICABLE = "DDConfWizLicenseNotApplicable";
  public static final String NOT_AVAILABLE = "DDConfWizLicenseNotAvailable";
  public static final String ERROR = "DDConfWizLicneseError";
  public static final String ERROR_READING = "DDConfWizLicenseErrorReading";
  public static final String ERROR_FNFE = "DDConfWizLicenseErrorFNFE";
  public static final String ERROR_CORRUPT = "DDConfWizLicenseErrorCorrupt";
  public static final String LICENSE_INVALID = "DDConfWizLicenseInvalid";
  public static final String MAIN_DOCS = "DDConfWizLicensePageMainDocs";
  public static final String CONTINUE_WITH_INVALID =
      "DDConfWizLicenseContinueWithInvalid";
  protected JButton addLicense;
  protected JButton buyLicense;
  protected JButton getLicense;
  protected JButton productSite;
  protected JPanel licenseContainer;
  protected boolean licenseValid;
  protected Object licenseArray[][];
  protected static final int LIC_PUBLIC_FIELDS = 10;
  protected static final int LIC_NUM_FIELDS = 11;
  protected static final int LIC_SERIAL = 0;
  protected static final int LIC_HOSTS = 1;
  protected static final int LIC_PRODUCT = 2;
  protected static final int LIC_TYPE = 3;
  protected static final int LIC_BUNDLES = 4;
  protected static final int LIC_CLIENTS = 5;
  protected static final int LIC_APPS = 6;
  protected static final int LIC_EXPIRY = 7;
  protected static final int LIC_DD_VERSION = 8;
  protected static final int LIC_VERSION = 9;
  protected static final int LIC_SIGNATURE = 10;
  protected Properties clusterProps;
  protected static boolean isLicensed = false;
  protected JDialog pleaseWait;

  public LicensePage(ConfigWizardGUI gui, int buttons) {
    super(gui, buttons);
    JPanel page = new JPanel( ( (java.awt.LayoutManager) (new GridBagLayout())));
    licenseContainer = new JPanel( ( (java.awt.LayoutManager) (new
        GridBagLayout())));
    ( (JComponent) (licenseContainer)).setBackground(Color.white);
    ( (JComponent) (licenseContainer)).setBorder(LineBorder.
                                                 createBlackLineBorder());
    initializeLicenseArray();
    for (int i = 0; i < 10; i++) {
      ( (Container) (licenseContainer)).add( ( (java.awt.Component) ( (JLabel)
          licenseArray[i][0])),
          ( (Object) (new GridBagConstraints(0, i, 1, 1, 0.0D, 0.0D, 12, 0,
                                             new Insets(2, 2, 2, 2), 0, 0))));
      ( (Container) (licenseContainer)).add( ( (java.awt.Component) ( (JLabel)
          licenseArray[i][1])),
          ( (Object) (new GridBagConstraints(1, i, 1, 1, 0.0D, 0.0D, 18, 0,
                                             new Insets(2, 2, 2, 2), 0, 0))));
    }

    ( (Container) (page)).add( ( (java.awt.Component) (licenseContainer)),
                              ( (
                               Object) (new GridBagConstraints(0, 0, 1, 5, 0.5D,
        0.5D, 12, 2, new Insets(2, 2, 2, 2), 0, 0))));
    addLicense = new JButton(li.getString("DDConfWizLicenseAddLicense"));
    ( (AbstractButton) (addLicense)).addActionListener( ( (ActionListener) (this)));
    ( (Container) (page)).add( ( (java.awt.Component) (addLicense)),
                              ( (Object) (new
                                          GridBagConstraints(1, 0, 1, 1, 0.0D,
        0.0D, 18, 2, new Insets(2, 20, 2, 20), 0, 0))));
    if (isLicensed) {
      ( (AbstractButton) (addLicense)).setText(li.getString(
          "DDConfWizLicenseReplaceLicense"));
    }
    buyLicense = new JButton(li.getString("DDConfWizLicenseBuyLicense"));
    ( (AbstractButton) (buyLicense)).addActionListener( ( (ActionListener) (this)));
    ( (Container) (page)).add( ( (java.awt.Component) (buyLicense)),
                              ( (Object) (new
                                          GridBagConstraints(1, 2, 1, 1, 0.0D,
        0.0D, 18, 2, new Insets(10, 20, 2, 20), 0, 0))));
    getLicense = new JButton(li.getString("DDConfWizLicenseObtainLicense"));
    ( (AbstractButton) (getLicense)).addActionListener( ( (ActionListener) (this)));
    ( (Container) (page)).add( ( (java.awt.Component) (getLicense)),
                              ( (Object) (new
                                          GridBagConstraints(1, 3, 1, 1, 0.0D,
        0.0D, 18, 2, new Insets(2, 20, 2, 20), 0, 0))));
    productSite = new JButton(li.getString("DDConfWizLicenseProductSite"));
    ( (AbstractButton) (productSite)).addActionListener( ( (ActionListener) (this)));
    ( (Container) (page)).add( ( (java.awt.Component) (productSite)),
                              ( (Object) (new
                                          GridBagConstraints(1, 4, 1, 1, 0.0D,
        0.0D, 18, 2, new Insets(2, 20, 2, 20), 0, 0))));
    ( (ConfigPage)this).setContentPanel(page);
    ( (ConfigPage)this).setTitle(li.getString("License"));
  }

  protected boolean beforeNext() {
    if (!isLicensed) {
      int response = JOptionPane.showConfirmDialog( ( (java.awt.Component) (super.
          gui)), ( (Object) (li.getString("DDConfWizLicenseContinueWithInvalid"))),
          li.getString("DDConfWizLicenseInvalid"), 0);
      if (response == 0) {
        writeCurrentSettings();
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return true;
    }
  }

  protected boolean beforePrevious() {
    return true;
  }

  protected void refreshPage() {
  }

  protected String getDocs() {
    return li.getString("DDConfWizLicensePageMainDocs");
  }

  private void initializeLicenseArray() {
    initArray();
    readCurrentSettings();
    readClusterLicense();
  }

  protected void initArray() {
    licenseArray = new Object[11][3];
    licenseArray[0][0] = ( (Object) (new JLabel(li.getString(
        "DDConfWizLicenseSerialNumber") + ":")));
    licenseArray[1][0] = ( (Object) (new JLabel(li.getString(
        "DDConfWizLicenseHosts") + ":")));
    licenseArray[2][0] = ( (Object) (new JLabel("Product:")));
    licenseArray[3][0] = ( (Object) (new JLabel(li.getString(
        "DDConfWizLicenseType") + ":")));
    licenseArray[4][0] = ( (Object) (new JLabel(li.getString(
        "DDConfWizLicenseBundles") + ":")));
    licenseArray[5][0] = ( (Object) (new JLabel(li.getString(
        "DDConfWizLicenseClients") + ":")));
    licenseArray[6][0] = ( (Object) (new JLabel(li.getString(
        "DDConfWizLicenseApplications") + ":")));
    licenseArray[7][0] = ( (Object) (new JLabel(li.getString(
        "DDConfWizLicenseExpiry") + ":")));
    licenseArray[8][0] = ( (Object) (new JLabel(li.getString(
        "DDConfWizLicenseDDVersion") + ":")));
    licenseArray[9][0] = ( (Object) (new JLabel(li.getString(
        "DDConfWizLicenseVersion") + ":")));
    licenseArray[10][0] = null;
    licenseArray[0][1] = null;
    licenseArray[1][1] = null;
    licenseArray[2][1] = null;
    licenseArray[3][1] = null;
    licenseArray[4][1] = null;
    licenseArray[5][1] = null;
    licenseArray[6][1] = null;
    licenseArray[7][1] = null;
    licenseArray[8][1] = null;
    licenseArray[9][1] = null;
    licenseArray[10][1] = null;
    licenseArray[0][2] = "serial_number";
    licenseArray[1][2] = "hosts";
    licenseArray[2][2] = "product";
    licenseArray[3][2] = "type";
    licenseArray[4][2] = "bundles";
    licenseArray[5][2] = "clients";
    licenseArray[6][2] = "applications";
    licenseArray[7][2] = "expiry";
    licenseArray[8][2] = "license_version";
    licenseArray[9][2] = "sitraka.license.version";
    licenseArray[10][2] = "sitraka.license.signature";
  }

  protected void addNewLicense() {
    LicenseProperties licenseProp = new LicenseProperties();
    JFileChooser fileChooser = new JFileChooser();
    int retVal = fileChooser.showOpenDialog( ( (java.awt.Component) (this)));
    try {
      if (retVal == 0) {
        java.io.File file = fileChooser.getSelectedFile();
        licenseProp.loadFromFile(file);
        ( (Frame) (super.gui)).setCursor(3);
        String licenseVersion = ( (Properties) (licenseProp)).getProperty(
            "license_version");
        if (licenseProp.isValid() && licenseVersion != null &&
            licenseVersion.equals("2.5")) {
          for (int i = 0; i < licenseArray.length; i++) {
            String key = (String) licenseArray[i][2];
            String value = ( (Properties) (licenseProp)).getProperty(key, "");
            ( (Hashtable) (clusterProps)).put( ( (Object) (key)),
                                              ( (Object) (value)));
          }

          readClusterLicense();
        }
        else {
          JOptionPane.showMessageDialog( ( (java.awt.Component) (this)),
                                        "Invalid License",
                                        li.getString("DDConfWizLicneseError"),
                                        0);
        }
      }
    }
    catch (InvalidLicenseException ile) {
      JOptionPane.showMessageDialog( ( (java.awt.Component) (this)),
                                    ( (Object) (
                                     li.getString("DDConfWizLicenseErrorCorrupt"))),
                                    li.getString("DDConfWizLicneseError"), 0);
    }
    catch (FileNotFoundException fnfe) {
      JOptionPane.showMessageDialog( ( (java.awt.Component) (this)),
                                    ( (Object) (
                                     li.getString("DDConfWizLicenseErrorFNFE"))),
                                    li.getString("DDConfWizLicneseError"), 0);
    }
    catch (IOException ioe) {
      JOptionPane.showMessageDialog( ( (java.awt.Component) (this)),
                                    ( (Object) (
                                     li.getString("DDConfWizLicenseErrorReading"))),
                                    li.getString("DDConfWizLicneseError"), 0);
    }
    ( (Window) (super.gui)).setCursor(Cursor.getDefaultCursor());
    if (isLicensed) {
      ( (AbstractButton) (addLicense)).setText(li.getString(
          "DDConfWizLicenseReplaceLicense"));
    }
  }

  private void readClusterLicense() {
    String value = clusterProps.getProperty("sitraka.license.signature");
    if (value == null || value.equals("")) {
      isLicensed = false;
      for (int i = 0; i < 10; i++) {
        if (licenseArray[i][1] == null) {
          licenseArray[i][1] = ( (Object) (new JLabel(li.getString(
              "DDConfWizLicenseNotAvailable"))));
        }
        else {
          ( (JLabel) licenseArray[i][1]).setText(li.getString(
              "DDConfWizLicenseNotAvailable"));
        }
      }

    }
    else {
      isLicensed = true;
    }
    for (int i = 0; i < 10; i++) {
      String key = (String) licenseArray[i][2];
      value = clusterProps.getProperty(key);
      if (value == null || value.equals("")) {
        if (licenseArray[i][1] == null) {
          licenseArray[i][1] = ( (Object) (new JLabel(li.getString(
              "DDConfWizLicenseNotApplicable"))));
        }
        else {
          ( (JLabel) licenseArray[i][1]).setText(li.getString(
              "DDConfWizLicenseNotApplicable"));
        }
      }
      else
      if (licenseArray[i][1] == null) {
        licenseArray[i][1] = ( (Object) (new JLabel(value)));
      }
      else {
        ( (JLabel) licenseArray[i][1]).setText(value);
      }
      if (value != null && value.equals("")) {
        ( (Hashtable) (clusterProps)).remove( ( (Object) (key)));
      }
    }

  }

  protected void readCurrentSettings() {
    clusterProps = ProcessingPage.getClusterProperties();
  }

  protected void writeCurrentSettings() {
    ProcessingPage.setClusterProperties(clusterProps);
  }

  public void actionPerformed(ActionEvent ae) {
    if ( ( (EventObject) (ae)).getSource() == addLicense) {
      addNewLicense();
    }
    else
    if ( ( (EventObject) (ae)).getSource() == buyLicense) {
      ExternalBrowser.displayURL("http://www.sitraka.com/buy");
    }
    else
    if ( ( (EventObject) (ae)).getSource() == getLicense) {
      String machineName = ProcessingPage.getClusterProperties().getProperty(
          "deploy.cluster.host.0.machine", "");
      ExternalBrowser.displayURL(
          "http://www.sitraka.com/licenses/getlicenses/deploydirector?hosts=" +
          URLEncoder.encode(machineName));
    }
    else
    if ( ( (EventObject) (ae)).getSource() == productSite) {
      ExternalBrowser.displayURL(
          "http://www.sitraka.com/software/deploydirector");
    }
  }

  static {
    li = LocaleInfo.li;
  }
}