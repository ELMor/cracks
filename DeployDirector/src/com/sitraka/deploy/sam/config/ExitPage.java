// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ExitPage.java

package com.sitraka.deploy.sam.config;

import com.sitraka.deploy.sam.config.resources.LocaleInfo;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ResourceBundle;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

// Referenced classes of package com.sitraka.deploy.sam.config:
//            ConfigPage, ConfigWizardGUI

public class ExitPage extends ConfigPage
{

    protected static ResourceBundle li;
    public static final String TITLE = "DDConfWizExitTitle";
    public static final String FINISHED_CONFIGURING = "DDConfWizExitFinishedConf";
    public static final String PRESS_FINISH = "DDConfWizExitPressFinish";
    public static final String START_SERVER_QUESTION = "DDConfWizExitStartServerQuestion";
    public static final String START_SERVER = "DDConfWizExitStartServer";
    public static final String MAIN_DOCS = "DDConfWizExitPageMainDocs";
    public static final String FEW_MINUTES = "DDConfWizExitWaitAFewMinutes";
    public static final String PLEASE_WAIT = "DDConfWizExitPleaseWait";
    public static final String STARTING_SERVER = "DDConfWizExitStartingServer";

    public ExitPage(ConfigWizardGUI gui, int buttons)
    {
        super(gui, buttons);
        JPanel borderPanel = new JPanel();
        ((Container) (borderPanel)).setLayout(((java.awt.LayoutManager) (new GridBagLayout())));
        GridBagConstraints c = new GridBagConstraints();
        ((JComponent) (borderPanel)).setBorder(((javax.swing.border.Border) (new BevelBorder(1))));
        Font finishedFont = new Font("Enlarged Text", 0, 16);
        JLabel configFinished = new JLabel(li.getString("DDConfWizExitFinishedConf"));
        ((JComponent) (configFinished)).setFont(finishedFont);
        JLabel pressFinish = new JLabel(li.getString("DDConfWizExitPressFinish"));
        ((JComponent) (pressFinish)).setFont(finishedFont);
        c.gridy = 0;
        ((Container) (borderPanel)).add(((java.awt.Component) (configFinished)), ((Object) (c)));
        c.gridy++;
        ((Container) (borderPanel)).add(((java.awt.Component) (pressFinish)), ((Object) (c)));
        ((ConfigPage)this).setContentPanel(borderPanel);
        ((ConfigPage)this).setTitle(li.getString("DDConfWizExitTitle"));
    }

    protected boolean beforeNext()
    {
        return false;
    }

    protected boolean beforePrevious()
    {
        return true;
    }

    protected boolean beforeFinish()
    {
        return true;
    }

    protected void refreshPage()
    {
        ((JComponent) (super.finish)).requestFocus();
        super.genericPageUpdate();
    }

    protected String getDocs()
    {
        return li.getString("DDConfWizExitPageMainDocs");
    }

    static 
    {
        li = LocaleInfo.li;
    }
}
