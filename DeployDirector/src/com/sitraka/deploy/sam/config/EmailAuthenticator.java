// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   EmailAuthenticator.java

package com.sitraka.deploy.sam.config;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class EmailAuthenticator extends Authenticator
{

    protected PasswordAuthentication password;

    public EmailAuthenticator(String mailUser, String userPassword)
    {
        password = new PasswordAuthentication(mailUser, userPassword);
    }

    protected PasswordAuthentication getPasswordAuthentication()
    {
        return password;
    }
}
