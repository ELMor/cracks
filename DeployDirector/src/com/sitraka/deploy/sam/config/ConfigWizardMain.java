// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ConfigWizardMain.java

package com.sitraka.deploy.sam.config;

import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.config.resources.LocaleInfo;
import java.util.Properties;
import java.util.ResourceBundle;
import org.apache.xerces.parsers.DOMParser;
import org.apache.xml.serialize.Serializer;

// Referenced classes of package com.sitraka.deploy.sam.config:
//            ConfigWizardGUI

public class ConfigWizardMain
{

    protected static ResourceBundle li;
    public static final String NO_ADMIN_PAGE_SWITCH_TITLE = "DDConfWizNoAdminPageSwitchTitle";
    public static final String NO_ADMIN_PAGE_SWITCH_MESSAGE = "DDConfWizNoAdminPageSwitchMessage";
    protected static final String VALID = "valid==true";
    protected static ConfigWizardGUI gui;
    protected static Properties clusterProperties;
    protected static Properties serverProperties;
    protected static AppVault vault;
    protected static String vaultType;
    protected static DOMParser domParser = null;
    protected static Serializer xmlSerializer = null;
    protected static String initialServletLocation = "";
    public static String HOME = "";

    public ConfigWizardMain()
    {
    }

    public static boolean setupWizardMain(ConfigWizardGUI theGUI)
    {
        gui = theGUI;
        return true;
    }

    public static ConfigWizardGUI getGUI()
    {
        return gui;
    }

    static 
    {
        li = LocaleInfo.li;
    }
}
