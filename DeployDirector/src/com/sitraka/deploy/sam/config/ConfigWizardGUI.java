// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ConfigWizardGUI.java

package com.sitraka.deploy.sam.config;

import com.sitraka.deploy.sam.config.resources.LocaleInfo;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.MessageFormat;
import java.util.Hashtable;
import java.util.ResourceBundle;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

// Referenced classes of package com.sitraka.deploy.sam.config:
//            IntroPage, JDKPage, IDPage, LicensePage, 
//            EmailPage, ProcessingPage, ExitPage, ConfigPage, 
//            ConfigWizardMain

public class ConfigWizardGUI extends JFrame
{
    public class Exiter extends WindowAdapter
    {

        protected ConfigWizardGUI gui;

        public void windowClosing(WindowEvent e)
        {
            if(gui.allowClose())
            {
                setVisible(false);
                dispose();
                System.exit(0);
            }
        }

        public Exiter(ConfigWizardGUI wizGUI)
        {
            gui = wizGUI;
        }
    }


    protected static ResourceBundle li;
    protected CardLayout layout;
    protected Hashtable pages;
    protected Hashtable pageIDs;
    protected int nextCard;
    protected int currentCard;
    public static final String FILE_NOT_FOUND = "DDConfWizGUIFileNotFound";
    public static final String FILE_NOT_FOUND_HEADER = "DDConfWizGUIFileNotFoundHeader";
    public static final String VALIDATE_ERROR_HEADER = "DDConfWizGUIValidateErrorHeader";
    public static final String CONFIRM_CANCEL = "DDConfWizGUIConfirmCancel";
    public static final String CONFIRM_CANCEL_HEADER = "DDConfWizGUIConfirmCancelHeader";
    public static final String PROPERTY_ERROR = "DDConfWizGUIPropertyError";
    public static final String CONFIG_ERROR = "DDConfWizGUIConfigurationError";
    protected ConfigPage emailPage;

    public ConfigWizardGUI()
    {
        super("DeployDirector Configuration Wizard");
        ((Window)this).addWindowListener(((java.awt.event.WindowListener) (new Exiter(this))));
        ((JFrame)this).setDefaultCloseOperation(0);
        Container contentPane = ((JFrame)this).getContentPane();
        layout = new CardLayout();
        contentPane.setLayout(((java.awt.LayoutManager) (layout)));
        pages = new Hashtable();
        pageIDs = new Hashtable();
        nextCard = 0;
    }

    private void setupPages()
    {
        ProcessingPage.init();
        ConfigPage introPage = ((ConfigPage) (new IntroPage(this, 6)));
        add(((JPanel) (introPage)), "IntroPage");
        ConfigPage jdkPage = ((ConfigPage) (new JDKPage(this, 7)));
        add(((JPanel) (jdkPage)), "JDKPage");
        ConfigPage idPage = ((ConfigPage) (new IDPage(this, 7)));
        add(((JPanel) (idPage)), "IDPage");
        ConfigPage licensePage = ((ConfigPage) (new LicensePage(this, 7)));
        add(((JPanel) (licensePage)), "LicensePage");
        emailPage = ((ConfigPage) (new EmailPage(this, 7)));
        add(((JPanel) (emailPage)), "EmailPage");
        ConfigPage processingPage = ((ConfigPage) (new ProcessingPage(this, 7)));
        add(((JPanel) (processingPage)), "ProcessingPage");
        ConfigPage exitPage = ((ConfigPage) (new ExitPage(this, 9)));
        add(((JPanel) (exitPage)), "ExitPage");
        showPage("IntroPage");
        currentCard = 0;
        ((Window)this).pack();
        ((Component)this).setSize(((Component)this).getBounds().width + 50, ((Component)this).getBounds().height);
        center(((Component) (this)));
        ((Frame)this).setResizable(false);
        ((Window)this).show();
    }

    private void add(JPanel newPanel, String pageID)
    {
        ((JFrame)this).getContentPane().add(((Component) (newPanel)), ((Object) (pageID)));
        pages.put(((Object) (new Integer(nextCard))), ((Object) (pageID)));
        pageIDs.put(((Object) (pageID)), ((Object) (new Integer(nextCard))));
        nextCard++;
    }

    public static void center(Component frame)
    {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension windowSize = frame.getSize();
        frame.setLocation((screenSize.width - windowSize.width) / 2, (screenSize.height - windowSize.height) / 2);
    }

    protected void prevPage()
    {
        currentCard--;
        String keyID = (String)pages.get(((Object) (new Integer(currentCard))));
        showPage(keyID);
    }

    protected void nextPage()
    {
        currentCard++;
        String keyID = (String)pages.get(((Object) (new Integer(currentCard))));
        showPage(keyID);
    }

    protected void showPage(String pageID)
    {
        currentCard = ((Integer)pageIDs.get(((Object) (pageID)))).intValue();
        Component components[] = ((JFrame)this).getContentPane().getComponents();
        ((ConfigPage)components[currentCard]).refreshPage();
        layout.show(((JFrame)this).getContentPane(), pageID);
    }

    public int getCurrentCard()
    {
        return currentCard;
    }

    public void missingFileAlert(String filename)
    {
        Object messageArg[] = {
            filename
        };
        String j = li.getString("DDConfWizGUIFileNotFound");
        JOptionPane.showMessageDialog(((Component) (this)), ((Object) (MessageFormat.format(li.getString("DDConfWizGUIFileNotFound"), messageArg))), li.getString("DDConfWizGUIFileNotFoundHeader"), 0);
    }

    public static void validateError(Component parent, String message)
    {
        JOptionPane.showMessageDialog(parent, ((Object) (message)), li.getString("DDConfWizGUIValidateErrorHeader"), 1);
    }

    private boolean allowClose()
    {
        return cancel() == 0;
    }

    public int cancel()
    {
        return JOptionPane.showConfirmDialog(((Component) (this)), ((Object) (li.getString("DDConfWizGUIConfirmCancel"))), li.getString("DDConfWizGUIConfirmCancelHeader"), 0);
    }

    public ConfigPage getEmailPage()
    {
        return emailPage;
    }

    public static void main(String args[])
    {
        ConfigWizardGUI wiz = new ConfigWizardGUI();
        if(System.getProperty("DDConfig.home") == null || System.getProperty("DDConfig.home").equals(""))
        {
            JOptionPane.showMessageDialog(((Component) (wiz)), ((Object) (li.getString("DDConfWizGUIPropertyError"))), li.getString("DDConfWizGUIConfigurationError"), 0);
            System.exit(0);
        }
        if(ConfigWizardMain.setupWizardMain(wiz))
            wiz.setupPages();
        else
            System.exit(0);
    }

    static 
    {
        li = LocaleInfo.li;
    }

}
