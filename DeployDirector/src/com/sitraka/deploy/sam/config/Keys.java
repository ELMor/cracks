// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Keys.java

package com.sitraka.deploy.sam.config;


public class Keys
{

    public static final int CONTENT_PANEL_WIDTH = 520;
    public static final int CONTENT_PANEL_HEIGHT = 313;
    public static final int HELP_PANEL_WIDTH = 220;
    public static final int HELP_PANEL_HEIGHT = 313;
    public static final String BUY_URL = "http://www.sitraka.com/buy";
    public static final String SITE_URL = "http://www.sitraka.com/software/deploydirector";
    public static final String LICENSE_URL = "http://www.sitraka.com/licenses/getlicenses/deploydirector";
    public static final String MACHINE_NAME_KEY = "deploy.cluster.host.0.machine";
    public static final String PORT_KEY = "deploy.cluster.host.0.port";
    public static final String ADMIN_PAGE_KEY = "deploy.cluster.host.0.page";
    public static final String PROTOCOL_KEY = "deploy.cluster.host.0.protocol";
    public static final String BASE_CONTEXT = "/servlet";
    public static final String SERVLET_CONTEXT = "/deploy";
    public static final String ADMIN_CONTEXT = "/admin";
    public static final String DEFAULT_PORT = "8080";
    public static final String ROLE_PROPERTY = "deploy.admin.authentication";
    public static final String ROLE_CLASS = "com.sitraka.deploy.authentication.SimpleAuthentication";
    public static final String NONROLE_CLASS = "com.sitraka.deploy.admin.auth.AdminAuthentication";
    public static final String ADMIN_EMAIL_KEY = "deploy.error.email.0.address";
    public static final String NOTIFICATION_LEVEL_KEY = "deploy.error.email.0.notification";
    public static final String USER_NAME_KEY = "deploy.error.email.user";
    public static final String PASSWORD_KEY = "deploy.error.email.password";
    public static final String MAIL_HOST_KEY = "deploy.error.email.server";
    public static final String BUNDLES_KEY = "bundles";
    public static final String CLIENTS_KEY = "clients";
    public static final String HOSTS_KEY = "hosts";
    public static final String EXPIRY_KEY = "expiry";
    public static final String SERIAL_NUMBER_KEY = "serial_number";
    public static final String APPLICATIONS_KEY = "applications";
    public static final String SIGNATURE_KEY = "sitraka.license.signature";
    public static final String LICENSE_TYPE_KEY = "type";
    public static final String LICENSE_VERSION_KEY = "sitraka.license.version";
    public static final String LICENSE_DDVERSION_KEY = "license_version";
    public static final String SERVER_MACHINE_KEY = "deploy.local.host.machine";
    public static final String SERVER_PAGE_KEY = "deploy.local.host.page";
    public static final String SERVER_PORT_KEY = "deploy.local.host.port";
    public static final String SERVER_PROTOCOL_KEY = "deploy.local.host.protocol";
    public static final String CLUSTER_MACHINE_SERVER_KEY = "deploy.server.host.0.machine";
    public static final String CLUSTER_PAGE_SERVER_KEY = "deploy.cluster.host.0.page";
    public static final String CLUSTER_PORT_SERVER_KEY = "deploy.cluster.host.0.port";
    public static final String CLUSTER_PROTOCOL_SERVER_KEY = "deploy.cluster.host.0.protocol";
    public static final String HASH_ALGORITHM = "deploy.hashcode.algorithm";
    public static final String ADMIN_PAGE_KEYS[] = {
        "deploy.cluster.host.0.page", "deploy.local.host.page", "deploy.cluster.host.0.page"
    };
    public static final String LICENSE_SERIAL_NUMBER_KEY = "serial_number";
    public static final String LICENSE_BUNDLES_KEY = "bundles";
    public static final String LICENSE_CLIENTS_KEY = "clients";
    public static final String LICENSE_HOSTS_KEY = "hosts";
    public static final String LICENSE_APPLICATIONS_KEY = "applications";
    public static final String LICENSE_SIGNATURE_KEY = "sitraka.license.signature";
    public static final String LICENSE_EXPIRY_KEY = "expiry";
    public static final String WIZARD_PROPERTIES = "wizard.properties";
    public static final String WIZARD_ARCHIVED_JVM = "deploy.jvm.archive.";
    public static final String WIZARD_SELECTED_JVM = "deploy.jvm.selected";
    public static final String WIZARD_VAULT_CONVERT = "wizard.convert.vault";

    public Keys()
    {
    }

}
