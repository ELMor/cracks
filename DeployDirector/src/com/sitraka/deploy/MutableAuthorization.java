// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   MutableAuthorization.java

package com.sitraka.deploy;


// Referenced classes of package com.sitraka.deploy:
//            Authorization, AuthContextAware, AuthorizationModel

public interface MutableAuthorization
    extends Authorization, AuthContextAware
{

    public abstract AuthorizationModel getAuthorizationModel();
}
