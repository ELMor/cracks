// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AuthModel.java

package com.sitraka.deploy;


// Referenced classes of package com.sitraka.deploy:
//            AuthModelListener

public interface AuthModel
{

    public abstract void addAuthModelListener(AuthModelListener authmodellistener);

    public abstract void removeAuthModelListener(AuthModelListener authmodellistener);
}
