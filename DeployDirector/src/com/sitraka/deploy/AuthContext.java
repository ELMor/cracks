// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AuthContext.java

package com.sitraka.deploy;

import java.util.List;

// Referenced classes of package com.sitraka.deploy:
//            AuthGroups

public interface AuthContext
{

    public abstract List getBundles();

    public abstract List getBundlesForAuthorization(String s);

    public abstract List getBundlesForAuthentication(String s);

    public abstract List getVersions(String s);

    public abstract List getUsers(String s);

    public abstract AuthGroups getGroups(String s);

    public abstract String getAuthorizationClassName(String s);

    public abstract String getAuthenticationClassName(String s);

    public abstract String getLatestVersion(String s);
}
