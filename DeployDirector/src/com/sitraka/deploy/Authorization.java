// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Authorization.java

package com.sitraka.deploy;

import java.awt.Component;
import java.io.File;
import java.io.IOException;

public interface Authorization
{

    public static final int AUTHORIZED = 1;
    public static final int NOT_AUTHORIZED = 2;
    public static final String AUTHORIZATION_FILENAME = "authorization.dat";

    public abstract int isAuthorized(String s, String s1, String s2);

    public abstract boolean usesDataFile();

    public abstract File getDataFile();

    public abstract void setDataFile(File file)
        throws IOException;

    public abstract boolean hasEditor();

    public abstract Component getEditorComponent(String s);

    public abstract boolean commitChanges();

    public abstract boolean isModified();
}
