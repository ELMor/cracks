// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CollectionsSort.java

package com.sitraka.deploy.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class CollectionsSort
{

    public CollectionsSort()
    {
    }

    public static void sortVector(Vector v)
    {
        Collections.sort(((java.util.List) (v)));
    }

    public static void sortVector(Vector v, Comparator c)
    {
        Collections.sort(((java.util.List) (v)), c);
    }
}
