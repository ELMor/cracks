// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Hashcode.java

package com.sitraka.deploy.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

// Referenced classes of package com.sitraka.deploy.util:
//            Codecs

public class Hashcode
{

    protected static String hashcodeAlgorithm = "MD5";
    protected static final int BUFFER_SIZE = 16384;
    protected MessageDigest internal_md;

    public static String getHashcodeAlgorithm()
    {
        return hashcodeAlgorithm;
    }

    public static void setHashcodeAlgorithm(String hashcodeAlgorithm)
    {
        hashcodeAlgorithm = hashcodeAlgorithm;
    }

    public static String asHex(byte buf[])
    {
        StringBuffer str = new StringBuffer(buf.length * 3);
        for(int idx = 0; idx < buf.length; idx++)
        {
            str.append(Character.forDigit(buf[idx] >>> 4 & 0xf, 16));
            str.append(Character.forDigit(buf[idx] & 0xf, 16));
            str.append(':');
        }

        str.setLength(str.length() - 1);
        return str.toString();
    }

    public static String asHex(String buf)
    {
        return asHex(buf.getBytes());
    }

    public Hashcode()
    {
        internal_md = null;
        try
        {
            internal_md = MessageDigest.getInstance(hashcodeAlgorithm);
        }
        catch(NoSuchAlgorithmException e) { }
    }

    public void update(byte buffer[], int num_bytes)
    {
        if(internal_md == null)
        {
            return;
        } else
        {
            internal_md.update(buffer, 0, num_bytes);
            return;
        }
    }

    public String returnHash()
    {
        if(internal_md == null)
        {
            return null;
        } else
        {
            byte result[] = internal_md.digest();
            return new String(Codecs.base64Encode(result));
        }
    }

    public static String computeMD5Hash(String input)
    {
        return computeHash(input, "MD5");
    }

    public static String computeHash(String input)
    {
        return computeHash(input, hashcodeAlgorithm);
    }

    public static String computeHash(String input, String algorithm)
    {
        if(input == null)
            return null;
        try
        {
            MessageDigest md = MessageDigest.getInstance(algorithm);
            byte result[] = md.digest(input.getBytes());
            return new String(Codecs.base64Encode(result));
        }
        catch(Exception e)
        {
            return null;
        }
    }

    public static String computeMD5Hash(File input)
    {
        return computeHash(input, "MD5");
    }

    public static String computeHash(File input)
    {
        return computeHash(input, hashcodeAlgorithm);
    }

    public static String computeHash(File input, String algorithm)
    {
        if(!input.exists() || !input.canRead())
            return null;
        try
        {
            FileInputStream is = new FileInputStream(input);
            String code = computeHash(((InputStream) (is)), algorithm);
            try
            {
                is.close();
            }
            catch(IOException e) { }
            is = null;
            return code;
        }
        catch(FileNotFoundException e)
        {
            return null;
        }
    }

    public static String computeMD5Hash(InputStream in)
    {
        return computeHash(in, "MD5");
    }

    public static String computeHash(InputStream in)
    {
        return computeHash(in, hashcodeAlgorithm);
    }

    public static String computeHash(InputStream in, String algorithm)
    {
        if(in == null)
            return null;
        try
        {
            byte buffer[] = new byte[16384];
            int got = -1;
            MessageDigest md = MessageDigest.getInstance(algorithm);
            while((got = in.read(buffer, 0, 16384)) > 0) 
                md.update(buffer, 0, got);
            byte result[] = md.digest();
            return new String(Codecs.base64Encode(result));
        }
        catch(Exception e)
        {
            return null;
        }
    }

    public static void main(String args[])
    {
        if(args.length < 1)
        {
            System.err.println("Must pass exactly one arg, the filename to generate from!");
            System.exit(1);
        }
        File file = new File(args[0]);
        if(!file.exists() || !file.canRead())
        {
            System.err.println("Error: '" + file.getAbsolutePath() + "' does not exisy or cannot be read");
            System.exit(2);
        }
        if(args.length == 2 && args[1].equalsIgnoreCase("quiet"))
        {
            String hashcode = computeHash(file);
            System.out.println(hashcode);
            System.exit(0);
        }
        long start_time = System.currentTimeMillis();
        String hashcode = computeHash(file);
        long stop_time = System.currentTimeMillis();
        long duration = stop_time - start_time;
        System.out.println("File: " + file.getAbsolutePath());
        System.out.println("\t-> Hash: " + hashcode + " <- ");
        System.out.println("\tFile Size (kilobytes): " + (double)file.length() / 1024D);
        System.out.println("\tComputation Time (ms): " + duration);
        System.out.println("\tComputation Time (ms/kilobyte): " + (double)duration / ((double)file.length() / 1024D));
        if(hashcodeAlgorithm.equals("MD5"))
        {
            System.out.println("\tThis is a 144 bit hash code. The chances of collision are");
            System.out.println("\t\t2^-(144/2) or about 1 in 4722366482869645213696 (2.12e-24 %)");
        }
    }

}
