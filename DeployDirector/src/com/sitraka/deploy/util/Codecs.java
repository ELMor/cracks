// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Codecs.java

package com.sitraka.deploy.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.BitSet;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

// Referenced classes of package com.sitraka.deploy.util:
//            ParseException

public class Codecs
{

    private static BitSet BoundChar;
    private static BitSet EBCDICUnsafeChar;
    private static byte Base64EncMap[];
    private static byte Base64DecMap[];
    private static char UUEncMap[];
    private static byte UUDecMap[];

    private Codecs()
    {
    }

    public static final String base64Encode(String str)
    {
        if(str == null)
        {
            return null;
        } else
        {
            byte data[] = new byte[str.length()];
            data = str.getBytes();
            return new String(base64Encode(data));
        }
    }

    public static final byte[] base64Encode(byte data[])
    {
        if(data == null)
            return null;
        byte dest[] = new byte[((data.length + 2) / 3) * 4];
        int sidx = 0;
        int didx = 0;
        for(; sidx < data.length - 2; sidx += 3)
        {
            dest[didx++] = Base64EncMap[data[sidx] >>> 2 & 0x3f];
            dest[didx++] = Base64EncMap[data[sidx + 1] >>> 4 & 0xf | data[sidx] << 4 & 0x3f];
            dest[didx++] = Base64EncMap[data[sidx + 2] >>> 6 & 3 | data[sidx + 1] << 2 & 0x3f];
            dest[didx++] = Base64EncMap[data[sidx + 2] & 0x3f];
        }

        if(sidx < data.length)
        {
            dest[didx++] = Base64EncMap[data[sidx] >>> 2 & 0x3f];
            if(sidx < data.length - 1)
            {
                dest[didx++] = Base64EncMap[data[sidx + 1] >>> 4 & 0xf | data[sidx] << 4 & 0x3f];
                dest[didx++] = Base64EncMap[data[sidx + 1] << 2 & 0x3f];
            } else
            {
                dest[didx++] = Base64EncMap[data[sidx] << 4 & 0x3f];
            }
        }
        for(; didx < dest.length; didx++)
            dest[didx] = 61;

        return dest;
    }

    public static final String base64Decode(String str)
    {
        if(str == null)
        {
            return null;
        } else
        {
            byte data[] = new byte[str.length()];
            data = str.getBytes();
            return new String(base64Decode(data));
        }
    }

    public static final byte[] base64Decode(byte data[])
    {
        if(data == null)
            return null;
        int tail;
        for(tail = data.length; data[tail - 1] == 61; tail--);
        byte dest[] = new byte[tail - data.length / 4];
        for(int idx = 0; idx < data.length; idx++)
            data[idx] = Base64DecMap[data[idx]];

        int sidx = 0;
        int didx;
        for(didx = 0; didx < dest.length - 2; didx += 3)
        {
            dest[didx] = (byte)(data[sidx] << 2 & 0xff | data[sidx + 1] >>> 4 & 3);
            dest[didx + 1] = (byte)(data[sidx + 1] << 4 & 0xff | data[sidx + 2] >>> 2 & 0xf);
            dest[didx + 2] = (byte)(data[sidx + 2] << 6 & 0xff | data[sidx + 3] & 0x3f);
            sidx += 4;
        }

        if(didx < dest.length)
            dest[didx] = (byte)(data[sidx] << 2 & 0xff | data[sidx + 1] >>> 4 & 3);
        if(++didx < dest.length)
            dest[didx] = (byte)(data[sidx + 1] << 4 & 0xff | data[sidx + 2] >>> 2 & 0xf);
        return dest;
    }

    public static final char[] uuencode(byte data[])
    {
        if(data == null)
            return null;
        if(data.length == 0)
            return new char[0];
        int line_len = 45;
        char nl[] = System.getProperty("line.separator", "\n").toCharArray();
        char dest[] = new char[((data.length + 2) / 3) * 4 + (((data.length + line_len) - 1) / line_len) * (nl.length + 1)];
        int sidx = 0;
        int didx = 0;
        while(sidx + line_len < data.length) 
        {
            dest[didx++] = UUEncMap[line_len];
            for(int end = sidx + line_len; sidx < end; sidx += 3)
            {
                dest[didx++] = UUEncMap[data[sidx] >>> 2 & 0x3f];
                dest[didx++] = UUEncMap[data[sidx + 1] >>> 4 & 0xf | data[sidx] << 4 & 0x3f];
                dest[didx++] = UUEncMap[data[sidx + 2] >>> 6 & 3 | data[sidx + 1] << 2 & 0x3f];
                dest[didx++] = UUEncMap[data[sidx + 2] & 0x3f];
            }

            for(int idx = 0; idx < nl.length; idx++)
                dest[didx++] = nl[idx];

        }
        dest[didx++] = UUEncMap[data.length - sidx];
        for(; sidx + 2 < data.length; sidx += 3)
        {
            dest[didx++] = UUEncMap[data[sidx] >>> 2 & 0x3f];
            dest[didx++] = UUEncMap[data[sidx + 1] >>> 4 & 0xf | data[sidx] << 4 & 0x3f];
            dest[didx++] = UUEncMap[data[sidx + 2] >>> 6 & 3 | data[sidx + 1] << 2 & 0x3f];
            dest[didx++] = UUEncMap[data[sidx + 2] & 0x3f];
        }

        if(sidx < data.length - 1)
        {
            dest[didx++] = UUEncMap[data[sidx] >>> 2 & 0x3f];
            dest[didx++] = UUEncMap[data[sidx + 1] >>> 4 & 0xf | data[sidx] << 4 & 0x3f];
            dest[didx++] = UUEncMap[data[sidx + 1] << 2 & 0x3f];
            dest[didx++] = UUEncMap[0];
        } else
        if(sidx < data.length)
        {
            dest[didx++] = UUEncMap[data[sidx] >>> 2 & 0x3f];
            dest[didx++] = UUEncMap[data[sidx] << 4 & 0x3f];
            dest[didx++] = UUEncMap[0];
            dest[didx++] = UUEncMap[0];
        }
        for(int idx = 0; idx < nl.length; idx++)
            dest[didx++] = nl[idx];

        if(didx != dest.length)
            throw new Error("Calculated " + dest.length + " chars but wrote " + didx + " chars!");
        else
            return dest;
    }

    private static final byte[] uudecode(BufferedReader rdr)
        throws ParseException, IOException
    {
        String line;
        while((line = rdr.readLine()) != null && !line.startsWith("begin ")) ;
        if(line == null)
            throw new ParseException("'begin' line not found");
        StringTokenizer tok = new StringTokenizer(line);
        tok.nextToken();
        int file_mode;
        try
        {
            file_mode = Integer.parseInt(tok.nextToken(), 8);
        }
        catch(Exception e)
        {
            throw new ParseException("Invalid mode on line: " + line);
        }
        String file_name;
        try
        {
            file_name = tok.nextToken();
        }
        catch(NoSuchElementException e)
        {
            throw new ParseException("No file name found on line: " + line);
        }
        byte body[] = new byte[1000];
        int off;
        byte tmp[];
        for(off = 0; (line = rdr.readLine()) != null && !line.equals("end"); off += tmp.length)
        {
            tmp = uudecode(line.toCharArray());
            if(off + tmp.length > body.length)
                body = resizeArray(body, off + 1000);
            System.arraycopy(((Object) (tmp)), 0, ((Object) (body)), off, tmp.length);
        }

        if(line == null)
            throw new ParseException("'end' line not found");
        else
            return resizeArray(body, off);
    }

    public static final byte[] uudecode(char data[])
    {
        if(data == null)
            return null;
        byte dest[] = new byte[(data.length / 4) * 3];
        int sidx = 0;
        int didx = 0;
        while(sidx < data.length) 
        {
            int len = ((int) (UUDecMap[data[sidx++]]));
            int end;
            for(end = didx + len; didx < end - 2;)
            {
                byte A = UUDecMap[data[sidx]];
                byte B = UUDecMap[data[sidx + 1]];
                byte C = UUDecMap[data[sidx + 2]];
                byte D = UUDecMap[data[sidx + 3]];
                dest[didx++] = (byte)(A << 2 & 0xff | B >>> 4 & 3);
                dest[didx++] = (byte)(B << 4 & 0xff | C >>> 2 & 0xf);
                dest[didx++] = (byte)(C << 6 & 0xff | D & 0x3f);
                sidx += 4;
            }

            if(didx < end)
            {
                byte A = UUDecMap[data[sidx]];
                byte B = UUDecMap[data[sidx + 1]];
                dest[didx++] = (byte)(A << 2 & 0xff | B >>> 4 & 3);
            }
            if(didx < end)
            {
                byte B = UUDecMap[data[sidx + 1]];
                byte C = UUDecMap[data[sidx + 2]];
                dest[didx++] = (byte)(B << 4 & 0xff | C >>> 2 & 0xf);
            }
            for(; sidx < data.length && data[sidx] != '\n' && data[sidx] != '\r'; sidx++);
            for(; sidx < data.length && (data[sidx] == '\n' || data[sidx] == '\r'); sidx++);
        }
        return resizeArray(dest, didx);
    }

    public static final String quotedPrintableEncode(String str)
    {
        if(str == null)
            return null;
        char map[] = {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
            'A', 'B', 'C', 'D', 'E', 'F'
        };
        char nl[] = System.getProperty("line.separator", "\n").toCharArray();
        char res[] = new char[(int)((double)str.length() * 1.5D)];
        char src[] = str.toCharArray();
        int cnt = 0;
        int didx = 1;
        int last = 0;
        int slen = str.length();
        for(int sidx = 0; sidx < slen; sidx++)
        {
            char ch = src[sidx];
            if(ch == nl[0] && match(src, sidx, nl))
            {
                if(res[didx - 1] == ' ')
                {
                    res[didx - 1] = '=';
                    res[didx++] = '2';
                    res[didx++] = '0';
                } else
                if(res[didx - 1] == '\t')
                {
                    res[didx - 1] = '=';
                    res[didx++] = '0';
                    res[didx++] = '9';
                }
                res[didx++] = '\r';
                res[didx++] = '\n';
                sidx += nl.length - 1;
                cnt = didx;
            } else
            if(ch > '~' || ch < ' ' && ch != '\t' || ch == '=' || EBCDICUnsafeChar.get(((int) (ch))))
            {
                res[didx++] = '=';
                res[didx++] = map[(ch & 0xf0) >>> 4];
                res[didx++] = map[ch & 0xf];
            } else
            {
                res[didx++] = ch;
            }
            if(didx > cnt + 70)
            {
                res[didx++] = '=';
                res[didx++] = '\r';
                res[didx++] = '\n';
                cnt = didx;
            }
            if(didx > res.length - 5)
                res = resizeArray(res, res.length + 500);
        }

        return String.valueOf(res, 1, didx - 1);
    }

    private static final boolean match(char str[], int start, char arr[])
    {
        if(str.length < start + arr.length)
            return false;
        for(int idx = 1; idx < arr.length; idx++)
            if(str[start + idx] != arr[idx])
                return false;

        return true;
    }

    public static final String quotedPrintableDecode(String str)
        throws ParseException
    {
        if(str == null)
            return null;
        char res[] = new char[(int)((double)str.length() * 1.1000000000000001D)];
        char src[] = str.toCharArray();
        char nl[] = System.getProperty("line.separator", "\n").toCharArray();
        int last = 0;
        int didx = 0;
        int slen = str.length();
        for(int sidx = 0; sidx < slen;)
        {
            char ch = src[sidx++];
            if(ch == '=')
            {
                if(sidx >= slen - 1)
                    throw new ParseException("Premature end of input detected");
                if(src[sidx] == '\n' || src[sidx] == '\r')
                {
                    sidx++;
                    if(src[sidx - 1] == '\r' && src[sidx] == '\n')
                        sidx++;
                } else
                {
                    int hi = Character.digit(src[sidx], 16);
                    int lo = Character.digit(src[sidx + 1], 16);
                    if((hi | lo) < 0)
                        throw new ParseException(new String(src, sidx - 1, 3) + " is an invalid code");
                    char repl = (char)(hi << 4 | lo);
                    sidx += 2;
                    res[didx++] = repl;
                }
                last = didx;
            } else
            if(ch == '\n' || ch == '\r')
            {
                if(ch == '\r' && sidx < slen && src[sidx] == '\n')
                    sidx++;
                for(int idx = 0; idx < nl.length; idx++)
                    res[last++] = nl[idx];

                didx = last;
            } else
            {
                res[didx++] = ch;
                if(ch != ' ' && ch != '\t')
                    last = didx;
            }
            if(didx > res.length - nl.length - 2)
                res = resizeArray(res, res.length + 500);
        }

        return new String(res, 0, didx);
    }

    public static final String URLEncode(String str)
    {
        if(str == null)
            return null;
        else
            return URLEncoder.encode(str);
    }

    public static final String URLDecode(String str)
        throws ParseException
    {
        if(str == null)
            return null;
        char res[] = new char[str.length()];
        int didx = 0;
        for(int sidx = 0; sidx < str.length(); sidx++)
        {
            char ch = str.charAt(sidx);
            if(ch == '+')
                res[didx++] = ' ';
            else
            if(ch == '%')
                try
                {
                    res[didx++] = (char)Integer.parseInt(str.substring(sidx + 1, sidx + 3), 16);
                    sidx += 2;
                }
                catch(NumberFormatException e)
                {
                    throw new ParseException(str.substring(sidx, sidx + 3) + " is an invalid code");
                }
            else
                res[didx++] = ch;
        }

        return String.valueOf(res, 0, didx);
    }

    public static final Object[] resizeArray(Object src[], int new_size)
    {
        Object tmp[] = new Object[new_size];
        System.arraycopy(((Object) (src)), 0, ((Object) (tmp)), 0, src.length >= new_size ? new_size : src.length);
        return tmp;
    }

    public static final String[] resizeArray(String src[], int new_size)
    {
        String tmp[] = new String[new_size];
        System.arraycopy(((Object) (src)), 0, ((Object) (tmp)), 0, src.length >= new_size ? new_size : src.length);
        return tmp;
    }

    public static final boolean[] resizeArray(boolean src[], int new_size)
    {
        boolean tmp[] = new boolean[new_size];
        System.arraycopy(((Object) (src)), 0, ((Object) (tmp)), 0, src.length >= new_size ? new_size : src.length);
        return tmp;
    }

    public static final byte[] resizeArray(byte src[], int new_size)
    {
        byte tmp[] = new byte[new_size];
        System.arraycopy(((Object) (src)), 0, ((Object) (tmp)), 0, src.length >= new_size ? new_size : src.length);
        return tmp;
    }

    public static final char[] resizeArray(char src[], int new_size)
    {
        char tmp[] = new char[new_size];
        System.arraycopy(((Object) (src)), 0, ((Object) (tmp)), 0, src.length >= new_size ? new_size : src.length);
        return tmp;
    }

    public static final int[] resizeArray(int src[], int new_size)
    {
        int tmp[] = new int[new_size];
        System.arraycopy(((Object) (src)), 0, ((Object) (tmp)), 0, src.length >= new_size ? new_size : src.length);
        return tmp;
    }

    static 
    {
        BoundChar = new BitSet(256);
        for(int ch = 48; ch <= 57; ch++)
            BoundChar.set(ch);

        for(int ch = 65; ch <= 90; ch++)
            BoundChar.set(ch);

        for(int ch = 97; ch <= 122; ch++)
            BoundChar.set(ch);

        BoundChar.set(43);
        BoundChar.set(95);
        BoundChar.set(45);
        BoundChar.set(46);
        EBCDICUnsafeChar = new BitSet(256);
        EBCDICUnsafeChar.set(33);
        EBCDICUnsafeChar.set(34);
        EBCDICUnsafeChar.set(35);
        EBCDICUnsafeChar.set(36);
        EBCDICUnsafeChar.set(64);
        EBCDICUnsafeChar.set(91);
        EBCDICUnsafeChar.set(92);
        EBCDICUnsafeChar.set(93);
        EBCDICUnsafeChar.set(94);
        EBCDICUnsafeChar.set(96);
        EBCDICUnsafeChar.set(123);
        EBCDICUnsafeChar.set(124);
        EBCDICUnsafeChar.set(125);
        EBCDICUnsafeChar.set(126);
        byte map[] = {
            65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 
            75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 
            85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 
            101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 
            111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 
            121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 
            56, 57, 43, 47
        };
        Base64EncMap = map;
        Base64DecMap = new byte[128];
        for(int idx = 0; idx < Base64EncMap.length; idx++)
            Base64DecMap[Base64EncMap[idx]] = (byte)idx;

        UUEncMap = new char[64];
        for(int idx = 0; idx < UUEncMap.length; idx++)
            UUEncMap[idx] = (char)(idx + 32);

        UUDecMap = new byte[128];
        for(int idx = 0; idx < UUEncMap.length; idx++)
            UUDecMap[UUEncMap[idx]] = (byte)idx;

    }
}
