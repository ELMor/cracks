// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PropertyUtils.java

package com.sitraka.deploy.util;

import com.sitraka.deploy.common.jclass.util.JCStringTokenizer;
import com.sitraka.deploy.common.jclass.util.JCTypeConverter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.util:
//            Codecs, Crypt

public class PropertyUtils
{

    protected static final String MODIFIER_NAMES[] = {
        "second", "seconds", "minute", "minutes", "hour", "hours", "day", "days"
    };
    protected static final long MODIFIER_VALUES[] = {
        1000L, 1000L, 60000L, 60000L, 0x36ee80L, 0x36ee80L, 0x5265c00L, 0x5265c00L
    };
    protected static final String LONG_DATE_FORMATS[] = {
        "yyyy/MM/dd h:mm:ss a z", "yyyy/MM/dd hh:mm:ss z", "yyyy/MM/dd hh:mm:ss", "yyyy/MM/dd hh:mm z", "yyyy/MM/dd hh:mm", "yyyy/MM/dd h:mm:ss a", "yyyy/MM/dd h:mm a z", "yyyy/MM/dd h:mm a", "yyyy.MM.dd h:mm:ss a z", "yyyy.MM.dd hh:mm:ss z", 
        "yyyy.MM.dd hh:mm:ss", "yyyy.MM.dd hh:mm z", "yyyy.MM.dd hh:mm", "yyyy.MM.dd h:mm:ss a", "yyyy.MM.dd h:mm a z", "yyyy.MM.dd h:mm a"
    };
    protected static final String SHORT_DATE_FORMATS[] = {
        "MMM dd, yyyy", "yyyy/MMM/dd", "yyyy.MM.dd", "yyyy/MM/dd"
    };
    protected static final String DATE_NOW = "now";
    protected static final String DATE_TODAY = "today";
    protected static final String DATE_TOMORROW = "tomorrow";
    protected static final String SPECIAL_DATES[] = {
        "now", "today", "tomorrow"
    };
    private static byte encode_data[] = {
        16, 55, -3, -96, -126, -112, -120, -58, 49, -55, 
        14, -106, 120, 50, -68, -41, 39, 28, -54, -57, 
        73, 7, -85, -49, -15, 64, -37, 49, -79, -3, 
        -29, 34, -72, 23, 108, -63, 95, -124, 12, -39, 
        -2, 21, -78, 119, 53, -88, 10, -86, 74, -29, 
        -20, -74, 84, 122, -41, -35, -41, 119, 76, 40, 
        -56, 108, -109, -116, 99, 88, -66, 115, 41, 39, 
        -42, -3, -121, -96, 96, -56, -96, -3, 53, -91, 
        92, 66, 123, -47, 100, 80, 120, -82, 84, 90, 
        -106, 35, -38, 31, 82, -111, -20, 58, 86, 112, 
        71, 27, -120, 83, 126, 26, -114, 18, 65, 56, 
        10, -19, -24, 64, -119, 87, -84, -102, -18, -36, 
        100, 118, 59, -30, -44, 64, 114, -49, -74, 17, 
        -18, 87, 85, 101, -7, -84, -46, -5, 65, -56, 
        -33, -120, -108, 73, 126, -23, 3, -53, 18, -48, 
        -87, -41, 40, 120, 101, 120, -52, 59, 73, 50, 
        -126, 3, -42, -67, -14, 4, -100, -50, -59, -91, 
        -67, 21, -128, 59, -112, 79, 105, -51, 93, 57, 
        8, 5, 80, -87, 69, 99, -50, -18, 85, 18, 
        -115, -53, 91, -116, 106, -5, 81, -115, 54, 125, 
        58, -25, -15, 118, -3, 112, 14, 55, -11, 24, 
        115, -104, -95, -86, -117, 47, 3, 4, -88, 16, 
        40, 66, 101, 35, -91, 28, 41, -110, 114, 56, 
        112, -49, 8, 120, -108, 11, 26, 28, 49, -108, 
        -89, -50, -74, -91, 7, 35, -18, -30, 127, -60, 
        -70, -111, 24, -22, -55, -53
    };

    public PropertyUtils()
    {
    }

    public static int readInt(Properties props, String prop_name)
    {
        int result = 0x80000000;
        if(props == null || prop_name == null)
            return result;
        String value = props.getProperty(prop_name);
        if(value != null)
        {
            value = value.trim();
            try
            {
                result = Integer.parseInt(value);
            }
            catch(NumberFormatException e)
            {
                result = 0x80000000;
            }
            catch(Exception e)
            {
                result = 0x80000000;
            }
        }
        return result;
    }

    public static long readLong(Properties props, String prop_name)
    {
        long result = 0x8000000000000000L;
        if(props == null || prop_name == null)
            return result;
        String value = props.getProperty(prop_name);
        if(value != null)
        {
            value = value.trim();
            try
            {
                result = Long.parseLong(value);
            }
            catch(NumberFormatException e)
            {
                result = 0x8000000000000000L;
            }
            catch(Exception e)
            {
                result = 0x8000000000000000L;
            }
        }
        return result;
    }

    public static int readInt(Properties props, String prop_name, int default_value)
    {
        int result = readInt(props, prop_name);
        if(result == 0x80000000)
            result = default_value;
        return result;
    }

    public static long readLong(Properties props, String prop_name, long default_value)
    {
        long result = readLong(props, prop_name);
        if(result == 0x8000000000000000L)
            result = default_value;
        return result;
    }

    public static int readInt(Properties props, String prop_name, int default_value, int max_value, int min_value)
    {
        int result = readInt(props, prop_name, default_value);
        if(result < min_value)
            result = default_value;
        else
        if(result > max_value)
            result = default_value;
        return result;
    }

    protected static double readBaseInterval(String value)
    {
        int index = value.indexOf(' ');
        double result = Double.MIN_VALUE;
        String base = null;
        if(index != -1)
            base = value.substring(0, index);
        else
            base = value;
        try
        {
            result = Double.valueOf(base).doubleValue();
        }
        catch(NumberFormatException e)
        {
            result = Double.MIN_VALUE;
        }
        catch(Exception e)
        {
            result = Double.MIN_VALUE;
        }
        return result;
    }

    protected static long readModifier(String value)
    {
        long result = 1L;
        int index = value.lastIndexOf(' ');
        if(index != -1)
        {
            String modifier = value.substring(index + 1);
            for(int i = 0; i < MODIFIER_NAMES.length; i++)
            {
                if(!modifier.equalsIgnoreCase(MODIFIER_NAMES[i]))
                    continue;
                result = MODIFIER_VALUES[i];
                break;
            }

        }
        return result;
    }

    public static long readInterval(Properties props, String prop_name)
    {
        long result = 0x8000000000000000L;
        String value = props.getProperty(prop_name);
        if(value != null)
        {
            value = value.trim();
            double base = readBaseInterval(value);
            long modifier = readModifier(value);
            if(base == Double.MIN_VALUE)
            {
                result = 0x8000000000000000L;
            } else
            {
                Double temp = new Double(base * (double)modifier);
                result = temp.longValue();
            }
        }
        return result;
    }

    public static long readInterval(Properties props, String prop_name, long default_value)
    {
        long value = readInterval(props, prop_name);
        if(value == 0x8000000000000000L)
            value = default_value;
        return value;
    }

    protected static long checkForSpecialDate(String value)
    {
        long result = 0L;
        if(value == null)
            return 0L;
        if(value.equalsIgnoreCase("now"))
            result = System.currentTimeMillis();
        else
        if(value.equalsIgnoreCase("today"))
        {
            Calendar cal = Calendar.getInstance();
            cal.set(10, 0);
            cal.set(12, 0);
            cal.set(13, 0);
            result = cal.getTime().getTime();
        } else
        if(value.equalsIgnoreCase("tomorrow"))
        {
            Calendar cal = Calendar.getInstance();
            cal.roll(5, true);
            result = cal.getTime().getTime();
        }
        return result;
    }

    public static String[] getLongDateFormats()
    {
        return LONG_DATE_FORMATS;
    }

    public static String[] getShortDateFormats()
    {
        return SHORT_DATE_FORMATS;
    }

    public static long parseLongDate(String value)
    {
        return parseDateInternal(value, LONG_DATE_FORMATS);
    }

    public static String writeLongDate(Date date)
    {
        SimpleDateFormat formatter = new SimpleDateFormat(LONG_DATE_FORMATS[0]);
        return ((DateFormat) (formatter)).format(date);
    }

    public static String writeShortDate(Date date)
    {
        return writeShortDate(date, SHORT_DATE_FORMATS[0]);
    }

    public static String writeShortDate(Date date, String format)
    {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return ((DateFormat) (formatter)).format(date);
    }

    public static long parseShortDate(String value)
    {
        return parseDateInternal(value, SHORT_DATE_FORMATS);
    }

    protected static long parseDateInternal(String value, String formats[])
    {
        long result = 0L;
        if(value == null || formats == null)
            return result;
        for(int i = 0; i < formats.length; i++)
            try
            {
                SimpleDateFormat dateFormat = new SimpleDateFormat(formats[i]);
                Date when = ((DateFormat) (dateFormat)).parse(value);
                GregorianCalendar cal = new GregorianCalendar();
                ((Calendar) (cal)).setTime(when);
                result = ((Calendar) (cal)).getTime().getTime();
                break;
            }
            catch(ParseException pe) { }
            catch(Exception e) { }

        return result;
    }

    public static long readDate(Properties props, String prop_name)
    {
        long result = 0L;
        if(props == null || prop_name == null)
            return result;
        String value = props.getProperty(prop_name);
        if(value != null)
        {
            value = value.trim();
            if((result = checkForSpecialDate(value)) != 0L)
                return result;
            result = parseLongDate(value);
        }
        return result;
    }

    public static long readShortDate(Properties props, String prop_name)
    {
        long result = 0L;
        if(props == null || prop_name == null)
            return result;
        String value = props.getProperty(prop_name);
        if(value != null)
        {
            value = value.trim();
            if((result = checkForSpecialDate(value)) != 0L)
                return result;
            result = parseShortDate(value);
        }
        return result;
    }

    public static String readPassword(Properties props, String prop_name)
    {
        return readPassword(props, prop_name, ((String) (null)));
    }

    public static String readPassword(Properties props, String prop_name, String fallback)
    {
        if(props == null || prop_name == null)
            return fallback;
        String value = props.getProperty(prop_name);
        if(value == null || value.trim().length() == 0)
            return fallback;
        else
            return new String(decodePassword(value.getBytes()));
    }

    public static String parsePassword(String encoded)
    {
        return new String(decodePassword(encoded.getBytes()));
    }

    public static String writePassword(String plaintext)
    {
        return new String(encodePassword(plaintext.getBytes()));
    }

    private static byte[] decodePassword(byte encoded[])
    {
        encoded = Codecs.base64Decode(encoded);
        int pw_len = encoded[0] - 33;
        int offset = encoded[1] - 33;
        offset += (pw_len & 3) << 6;
        pw_len = (pw_len & 0xff) >> 2;
        int count = encoded.length - 2;
        byte mask[] = getDataMask(offset, count);
        byte base_data[] = new byte[count];
        System.arraycopy(((Object) (encoded)), 2, ((Object) (base_data)), 0, count);
        byte decoded[] = unrotate(base_data, mask);
        byte plaintext[] = new byte[pw_len];
        System.arraycopy(((Object) (decoded)), 0, ((Object) (plaintext)), 0, pw_len);
        return plaintext;
    }

    private static byte[] encodePassword(byte plaintext[])
    {
        int start_index = (int)(Math.random() * 256D);
        int pad = (int)(Math.random() * 4D);
        int count = plaintext.length + pad;
        byte mask[] = getDataMask(start_index, count);
        byte precipher[] = new byte[count];
        System.arraycopy(((Object) (plaintext)), 0, ((Object) (precipher)), 0, plaintext.length);
        for(int i = 0; i < pad; i++)
            precipher[plaintext.length + i] = encode_data[start_index + i];

        byte rotated[] = rotate(precipher, mask);
        byte encoded[] = new byte[count + 2];
        int len_char = (plaintext.length << 2) + (start_index >> 6) + 33;
        encoded[0] = (byte)len_char;
        int offset_char = (start_index & 0x3f) + 33;
        encoded[1] = (byte)offset_char;
        System.arraycopy(((Object) (rotated)), 0, ((Object) (encoded)), 2, count);
        byte encoded_text[] = Codecs.base64Encode(encoded);
        return encoded_text;
    }

    protected static byte[] getDataMask(int offset, int byte_count)
    {
        byte encoded[] = new byte[byte_count];
        byte salt_data[] = new byte[2];
        for(int i = 0; i < 2; i++)
        {
            if(offset + i == 256)
                offset -= 256;
            salt_data[i] = (byte)((encode_data[offset + i] & 0x3f) + 48);
        }

        String salt = new String(salt_data);
        offset += 2;
        byte key_data[] = new byte[byte_count];
        for(int i = 0; i < byte_count; i++)
        {
            if(offset + i == 256)
                offset -= 256;
            key_data[i] = encode_data[offset + i];
        }

        String key = new String(key_data);
        int insert_at = byte_count - 1;
        for(int i = 0; i < byte_count; i += 8)
        {
            int end_index = i + 8 > byte_count ? byte_count : i + 8;
            String chunk = Crypt.crypt(salt, key.substring(i, end_index));
            for(int j = 0; j < end_index - i; j++)
                encoded[insert_at--] = (byte)chunk.charAt(j);

        }

        return encoded;
    }

    protected static byte[] rotate(byte data[], byte mask[])
    {
        byte rotated[] = new byte[mask.length];
        for(int i = 0; i < mask.length; i++)
        {
            int value = data[i] + mask[i];
            if(value > 255)
                value -= 256;
            rotated[i] = (byte)value;
        }

        return rotated;
    }

    protected static byte[] unrotate(byte data[], byte mask[])
    {
        byte unrotated[] = new byte[mask.length];
        for(int i = 0; i < mask.length; i++)
        {
            int value = data[i] - mask[i];
            if(value < 0)
                value += 256;
            unrotated[i] = (byte)value;
        }

        return unrotated;
    }

    public static boolean readDefaultBoolean(Properties props, String prop_name, boolean defaultValue)
    {
        boolean result = defaultValue;
        if(props == null || prop_name == null)
            return result;
        String value = props.getProperty(prop_name);
        if(value != null)
        {
            value = value.trim();
            if(value.equalsIgnoreCase("true"))
                result = true;
            else
            if(value.equalsIgnoreCase("yes"))
                result = true;
            else
            if(value.equalsIgnoreCase("on"))
                result = true;
            else
            if(value.equalsIgnoreCase("1"))
                result = true;
            else
            if(value.equalsIgnoreCase("false"))
                result = false;
            else
            if(value.equalsIgnoreCase("no"))
                result = false;
            else
            if(value.equalsIgnoreCase("off"))
                result = false;
            else
            if(value.equalsIgnoreCase("0"))
                result = false;
        }
        return result;
    }

    public static boolean readBoolean(Properties props, String prop_name)
    {
        return readDefaultBoolean(props, prop_name, false);
    }

    public static String readDefaultString(Properties props, String prop_name, String default_value)
    {
        if(props == null || prop_name == null)
            return default_value;
        String result = props.getProperty(prop_name);
        if(result != null)
        {
            result = result.trim();
            if(result.length() == 0 || result.equals(""))
                result = default_value;
        } else
        {
            result = default_value;
        }
        return result;
    }

    public static int toEnum(String value, String enum_names[], int enum_values[])
    {
        return JCTypeConverter.toEnum(value, enum_names, enum_values, -1);
    }

    public static String fromEnum(int value, String enum_names[], int enum_values[])
    {
        return JCTypeConverter.fromEnum(value, enum_names, enum_values);
    }

    public static int readEnum(Properties props, String prop_name, String enum_names[], int enum_values[])
    {
        int result = -1;
        if(props == null || prop_name == null)
            return result;
        String value = props.getProperty(prop_name);
        if(value != null)
        {
            value = value.trim();
            result = toEnum(value, enum_names, enum_values);
        }
        return result;
    }

    public static Vector readVector(Properties props, String prop_name, char separator)
    {
        String value = props.getProperty(prop_name);
        return readVector(value, separator, '\\');
    }

    public static Vector readVector(String value, char separator)
    {
        return readVector(value, separator, '\\');
    }

    public static Vector readVector(String value, char separator, char escape_char)
    {
        Vector result = new Vector();
        if(value != null)
        {
            value = value.trim();
            JCStringTokenizer toker = new JCStringTokenizer(value);
            toker.setEscapeChar(escape_char);
            while(toker.hasMoreTokens()) 
            {
                String token = toker.nextToken(separator);
                if(token != null && token.trim().length() != 0)
                    result.addElement(((Object) (token.trim())));
            }
        }
        return result;
    }

    public static boolean containsProperty(Properties props, String prefix, String suffix)
    {
        if(props == null || prefix == null)
            return false;
        if(suffix == null)
            suffix = "";
        prefix = prefix.toLowerCase();
        suffix = suffix.toLowerCase();
        for(Enumeration keys = ((Hashtable) (props)).keys(); keys.hasMoreElements();)
        {
            String key_name = ((String)keys.nextElement()).toLowerCase();
            if(key_name.startsWith(prefix) && key_name.endsWith(suffix))
                return true;
        }

        return false;
    }

    public static boolean containsPropertyValue(Properties props, String prefix, String suffix)
    {
        if(props == null || prefix == null)
            return false;
        if(suffix == null)
            suffix = "";
        prefix = prefix.toLowerCase();
        suffix = suffix.toLowerCase();
        for(Enumeration keys = ((Hashtable) (props)).keys(); keys.hasMoreElements();)
        {
            String key_name = ((String)keys.nextElement()).toLowerCase();
            if(key_name.startsWith(prefix) && key_name.endsWith(suffix))
            {
                String value = props.getProperty(key_name);
                if(value != null && value.trim().length() != 0)
                    return true;
            }
        }

        return false;
    }

}
