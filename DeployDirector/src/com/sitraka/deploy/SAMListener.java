// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SAMListener.java

package com.sitraka.deploy;


// Referenced classes of package com.sitraka.deploy:
//            SAMEvent

public interface SAMListener
{

    public abstract void clientEvent(SAMEvent samevent);

    public abstract void clientError(SAMEvent samevent);

    public abstract void serverEvent(SAMEvent samevent);

    public abstract void serverError(SAMEvent samevent);

    public abstract void serverLoad(SAMEvent samevent);
}
