// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AuthContextAware.java

package com.sitraka.deploy;


// Referenced classes of package com.sitraka.deploy:
//            AuthContext

public interface AuthContextAware
{

    public abstract void setAuthContext(AuthContext authcontext);

    public abstract AuthContext getAuthContext();
}
