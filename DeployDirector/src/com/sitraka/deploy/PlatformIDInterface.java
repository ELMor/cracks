// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PlatformIDInterface.java

package com.sitraka.deploy;


public interface PlatformIDInterface
{

    public abstract String getOSType(String s);

    public abstract String getOSName(String s);

    public abstract String getOSVersion(String s);

    public abstract String getArchitecture(String s);
}
