// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   NonPublicAuthContext.java

package com.sitraka.deploy;

import java.util.Properties;

// Referenced classes of package com.sitraka.deploy:
//            AuthContext, Authorization, Authentication

public interface NonPublicAuthContext
    extends AuthContext
{

    public abstract String getProperty(String s);

    public abstract String getProperty(String s, String s1);

    public abstract Properties getProperties();

    public abstract Authorization getAuthorizationObject(String s);

    public abstract Authentication getAuthenticationObject(String s);

    public abstract Object getCurrentUser();
}
