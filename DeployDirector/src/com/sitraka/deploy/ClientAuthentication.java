// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ClientAuthentication.java

package com.sitraka.deploy;

import java.awt.Component;
import java.io.File;
import java.io.IOException;

public interface ClientAuthentication
{

    public abstract Object getAuthenticationInfo();

    public abstract void setAuthenticationInfo(Object obj);

    public abstract boolean writeAuthenticationInfo();

    public abstract boolean isAuthenticationInfoAvailable();

    public abstract void setAuthenticationInfoAvailable(boolean flag);

    public abstract boolean usesDataFile();

    public abstract void setDataFile(File file)
        throws IOException;

    public abstract boolean hasEditor();

    public abstract Component getEditorComponent();

    public abstract void initEditor();

    public abstract void commitEdits();
}
