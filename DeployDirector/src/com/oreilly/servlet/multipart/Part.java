// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Part.java

package com.oreilly.servlet.multipart;


public abstract class Part
{

    private String name;

    Part(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public boolean isFile()
    {
        return false;
    }

    public boolean isParam()
    {
        return false;
    }
}
