// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 

package com.m7.wide.eclipse.jstudio;

import com.m7.A.B;
import com.m7.A.Q;
import com.m7.A.S;
import com.m7.wide.B.A.A;
import com.m7.wide.LogMgr;
import com.m7.wide.WideEnv;
import com.m7.wide.dialog.MessageDialog;
import com.m7.wide.eclipse.EclipseWideEnv;
import com.m7.wide.eclipse.debugger.WideDebugger;
import com.m7.wide.eclipse.resources.ResourceKeys;
import com.m7.wide.storage.C;
import java.awt.AWTEvent;
import java.awt.ActiveEvent;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.MenuComponent;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.apache.commons.logging.Log;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IPluginDescriptor;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IPageListener;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.internal.WorkbenchPage;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import sun.awt.SunToolkit;

public class Plugin extends AbstractUIPlugin
    implements IPageListener, IPartListener, DragSourceListener
{
    private static class _A extends Thread
    {

        Display C;
        EventQueue A;
        private final Runnable B = new Runnable() {

            public void run()
            {
                A(A);
            }

                
                {
                    super();
                }
        };

        public void run()
        {
            do
                try
                {
                    synchronized(A)
                    {
                        SunToolkit.flushPendingEvents();
                        if(C == null || C.isDisposed())
                            C = A();
                        if(C != null && !C.isDisposed())
                            C.asyncExec(B);
                        ((Object) (A)).wait();
                    }
                }
                catch(Throwable throwable)
                {
                    LogMgr.getUnhandledExceptionLogger().error("", throwable);
                }
            while(true);
        }

        private Display A()
        {
            Plugin plugin = Plugin.getDefault();
            if(plugin != null)
            {
                IWorkbench iworkbench = plugin.getWorkbench();
                if(iworkbench != null)
                {
                    IWorkbenchWindow aiworkbenchwindow[] = iworkbench.getWorkbenchWindows();
                    if(aiworkbenchwindow != null && aiworkbenchwindow.length > 0)
                    {
                        Shell shell = aiworkbenchwindow[0].getShell();
                        if(shell != null)
                            return shell.getDisplay();
                    }
                }
            }
            return null;
        }

        boolean A(EventQueue eventqueue)
        {
            boolean flag;
            if(!EventQueue.isDispatchThread())
            {
                LogMgr.getUnhandledExceptionLogger().error("AWTWatchDog: Must be dispatch thread.");
                return false;
            }
            flag = false;
_L2:
            if(eventqueue.peekEvent() == null)
                break; /* Loop/switch isn't completed */
            flag = true;
            AWTEvent awtevent;
            Object obj;
            awtevent = eventqueue.getNextEvent();
            obj = awtevent.getSource();
            if(!(awtevent instanceof KeyEvent) || ((KeyEvent)awtevent).getKeyCode() != 18)
                try
                {
                    if(awtevent instanceof ActiveEvent)
                        ((ActiveEvent)awtevent).dispatch();
                    else
                    if(obj instanceof Component)
                        ((Component)obj).dispatchEvent(awtevent);
                    else
                    if(obj instanceof MenuComponent)
                    {
                        ((MenuComponent)obj).dispatchEvent(awtevent);
                    } else
                    {
                        LogMgr.getUnhandledExceptionLogger().error("Unable to dispatch event");
                        throw new Error("Unable to dispatch event: " + awtevent);
                    }
                }
                catch(InterruptedException interruptedexception)
                {
                    LogMgr.getUnhandledExceptionLogger().error(((Object) (interruptedexception)));
                }
            if(true) goto _L2; else goto _L1
_L1:
            return flag;
        }

        public _A(EventQueue eventqueue)
        {
            setName("Event-Bridge");
            A = eventqueue;
            if(EventQueue.isDispatchThread())
                C = Display.getCurrent();
        }
    }


    private static Plugin A;
    private ResourceBundle G;
    private File E;
    private static final String D = "jspMarker";
    public static final String EXTERNAL_OFFSET_ATTR = "nitroX.xoffset";
    public static final String EXTERNAL_LEN_ATTR = "nitroX.xlen";
    public static final String DECORATOR_ID = "com.m7.wide.eclipse.jstudio.WebDecorator";
    public static final String ICON_PATH = "icons/";
    public static final String J2EE_CP = "J2ee.runtime.m7";
    private final IPluginDescriptor C;
    private long B;
    static HashSet F = new HashSet();
    static final boolean $assertionsDisabled; /* synthetic field */

    public Plugin(IPluginDescriptor iplugindescriptor)
        throws IOException
    {
        super(iplugindescriptor);
        B = 0L;
        C = iplugindescriptor;
    }

    private void A(IPluginDescriptor iplugindescriptor)
        throws IOException
    {
        try
        {
            URL url = getDescriptor().getInstallURL();
            url = Platform.resolve(url);
            File file = new File(url.getPath());
            E = file;
            EclipseWideEnv eclipsewideenv = new EclipseWideEnv(file.toURL());
        }
        catch(Exception exception)
        {
            throw new IOException(exception.getMessage());
        }
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception exception1)
        {
            exception1.printStackTrace();
        }
        A = this;
        try
        {
            G = ResourceBundle.getBundle(ResourceKeys.baseName());
        }
        catch(MissingResourceException missingresourceexception)
        {
            G = null;
        }
        String s = System.getProperty("m7.launcher");
        if(!S.A(((Object) (s)), "true"))
        {
            String s1 = getResourceString("plugin.warning");
            String s2 = getResourceString("plugin.launcher.title");
            String s3 = getResourceString("plugin.launcher.message");
            IOException ioexception1 = new IOException(s3);
            Shell shell = getWorkbench().getActiveWorkbenchWindow().getShell();
            Status status = new Status(2, C.getUniqueIdentifier(), -1, ioexception1.getMessage(), ((Throwable) (ioexception1)));
            ErrorDialog errordialog = new ErrorDialog(shell, s1, s2, ((org.eclipse.core.runtime.IStatus) (status)), 2);
            errordialog.open();
            throw ioexception1;
        }
        EventQueue eventqueue = Toolkit.getDefaultToolkit().getSystemEventQueue();
        (new _A(eventqueue)).start();
        ArrayList arraylist = new ArrayList();
        try
        {
            com.m7.wide.B.A.A.A(((java.util.List) (arraylist)));
        }
        catch(IOException ioexception)
        {
            String s4 = getResourceString("plugin.license.title");
            String s5 = getResourceString("plugin.license.message");
            Shell shell1 = getWorkbench().getActiveWorkbenchWindow().getShell();
            Status status1 = new Status(4, C.getUniqueIdentifier(), -1, ioexception.getMessage(), ((Throwable) (ioexception)));
            ErrorDialog errordialog1 = new ErrorDialog(shell1, s4, s5, ((org.eclipse.core.runtime.IStatus) (status1)), 4);
            errordialog1.open();
            throw ioexception;
        }
        catch(RuntimeException runtimeexception)
        {
            if(runtimeexception != A.F)
                throw runtimeexception;
        }
        WideEnv.getEnv().startup();
        IWorkbenchWindow iworkbenchwindow = getWorkbench().getActiveWorkbenchWindow();
        if(iworkbenchwindow != null)
            iworkbenchwindow.addPageListener(((IPageListener) (this)));
        if(!com.m7.A.B.A())
        {
            final File initFile = new File(getPluginLocation(), "init.properties");
            if(initFile.exists())
                SwingUtilities.invokeLater(new Runnable() {

                    public void run()
                    {
                        try
                        {
                            initForFirstTime(initFile, false);
                        }
                        finally
                        {
                            File file1 = new File(getPluginLocation(), "tour.properties");
                            boolean flag;
                            if(file1.exists())
                                flag = file1.delete();
                            flag = initFile.renameTo(file1);
                        }
                    }

            
            {
                super();
            }
                });
            long l = Runtime.getRuntime().totalMemory() / 0x100000L;
            long l1 = Runtime.getRuntime().maxMemory() / 0x100000L;
            LogMgr.getInfoLogger().info(((Object) ("total mem: " + l + ", max: " + l1)));
        }
        if(((java.util.List) (arraylist)).size() > 0)
            B = System.currentTimeMillis();
    }

    public boolean skipLicenseStatusDialog()
    {
        if(B > 0L)
        {
            long l = System.currentTimeMillis();
            return l - B < 2000L;
        } else
        {
            return false;
        }
    }

    public void initForFirstTime(File file)
    {
        initForFirstTime(file, true);
    }

    public void initForFirstTime(File file, boolean flag)
    {
        try
        {
            getWorkbench().showPerspective("com.m7.wide.eclipse.WidePerspective", getWorkbench().getActiveWorkbenchWindow());
        }
        catch(WorkbenchException workbenchexception) { }
        Properties properties = new Properties();
        try
        {
            properties.load(((java.io.InputStream) (new FileInputStream(file))));
        }
        catch(Exception exception)
        {
            LogMgr.getUnhandledExceptionLogger().error("could not read initial props file", ((Throwable) (exception)));
            return;
        }
        String s = properties.getProperty("projectName");
        IWorkspace iworkspace = ResourcesPlugin.getWorkspace();
        final IProject project = iworkspace.getRoot().getProject(s);
        if(project.exists())
        {
            if(flag)
            {
                String s1 = getResourceString("welcome.project.exists");
                MessageFormat messageformat = new MessageFormat(s1);
                Object aobj[] = {
                    s
                };
                MessageDialog.showInfo(messageformat.format(((Object) (aobj))));
            }
            return;
        }
        final IProjectDescription description = iworkspace.newProjectDescription(s);
        String s2 = System.getProperty("user.dir");
        if(s2 == null)
        {
            LogMgr.getInfoLogger().error("user.dir property returned null");
            return;
        }
        File file1 = new File(s2);
        file1 = new File(file1, "samples");
        file1 = new File(file1, s);
        if(!file1.exists() || file1.isFile())
        {
            LogMgr.getInfoLogger().error(((Object) ("folder:" + file1 + " does not exist")));
            return;
        }
        description.setLocation(((org.eclipse.core.runtime.IPath) (new Path(file1.toString()))));
        WorkspaceModifyOperation workspacemodifyoperation = new WorkspaceModifyOperation() {

            protected void execute(IProgressMonitor iprogressmonitor)
                throws CoreException
            {
                iprogressmonitor.beginTask("", 3000);
                project.create(description, ((IProgressMonitor) (new SubProgressMonitor(iprogressmonitor, 1000))));
                if(iprogressmonitor.isCanceled())
                {
                    return;
                } else
                {
                    project.open(((IProgressMonitor) (new SubProgressMonitor(iprogressmonitor, 1000))));
                    project.build(6, ((IProgressMonitor) (new SubProgressMonitor(iprogressmonitor, 1000))));
                    return;
                }
            }

            
                throws CoreException
            {
                super();
            }
        };
        Shell shell = getWorkbench().getActiveWorkbenchWindow().getShell();
        ProgressMonitorDialog progressmonitordialog = new ProgressMonitorDialog(shell);
        try
        {
            progressmonitordialog.run(false, false, ((org.eclipse.jface.operation.IRunnableWithProgress) (workspacemodifyoperation)));
        }
        catch(Exception exception1)
        {
            LogMgr.getUnhandledExceptionLogger().error("could not import initial project", ((Throwable) (exception1)));
            return;
        }
    }

    public String getJspMarkerID()
    {
        return getDescriptor().getUniqueIdentifier() + "." + "jspMarker";
    }

    public void shutdown()
        throws CoreException
    {
        super.shutdown();
        WideDebugger.shutdown();
        WideEnv.getEnv().cleanUp();
        com.m7.wide.storage.C.D();
    }

    public void startup()
        throws CoreException
    {
        try
        {
            A(C);
        }
        catch(IOException ioexception)
        {
            throw new CoreException(((org.eclipse.core.runtime.IStatus) (new Status(4, C.getUniqueIdentifier(), -1, ioexception.getMessage(), ((Throwable) (ioexception))))));
        }
        super.startup();
        WideDebugger.startup();
        IWorkspaceDescription iworkspacedescription = ResourcesPlugin.getWorkspace().getDescription();
        boolean flag = iworkspacedescription.isAutoBuilding();
        if(!flag)
            SwingUtilities.invokeLater(new Runnable() {

                public void run()
                {
                    getWorkbench().getPreferenceStore().setValue("AUTO_BUILD", false);
                    getWorkbench().getPreferenceStore().setValue("AUTO_BUILD", true);
                }

            
            {
                super();
            }
            });
    }

    public static Plugin getDefault()
    {
        return A;
    }

    public File getPluginLocation()
    {
        return E;
    }

    public static IWorkspace getWorkspace()
    {
        return ResourcesPlugin.getWorkspace();
    }

    public static String getResourceString(String s)
    {
        ResourceBundle resourcebundle = getDefault().getResourceBundle();
        return resourcebundle.getString(s);
        MissingResourceException missingresourceexception;
        missingresourceexception;
        return s;
    }

    public ResourceBundle getResourceBundle()
    {
        return G;
    }

    protected void initializeDefaultPreferences(IPreferenceStore ipreferencestore)
    {
        WideDebugger.initializeDefaultPreferences(ipreferencestore);
    }

    public static ImageDescriptor getImageDescriptor(String s)
    {
        URL url1;
        URL url = getDefault().getDescriptor().getInstallURL();
        url1 = new URL(url, "icons/" + s);
        return ImageDescriptor.createFromURL(url1);
        MalformedURLException malformedurlexception;
        malformedurlexception;
        return ImageDescriptor.getMissingImageDescriptor();
    }

    public String getWideEditorId(String s)
    {
        if(s == null)
            return null;
        IExtension iextension = getDefault().getDescriptor().getExtension("com.m7.wide.eclipse.editors");
        IConfigurationElement aiconfigurationelement[] = iextension.getConfigurationElements();
        for(int i = 0; i < aiconfigurationelement.length; i++)
        {
            IConfigurationElement iconfigurationelement = aiconfigurationelement[i];
            if(!iconfigurationelement.getName().equals("editor"))
                continue;
            String s1 = iconfigurationelement.getAttribute("extensions");
            java.util.List list = Q.A(s1, ',', true);
            if(Q.A(s, list) >= 0)
                return iconfigurationelement.getAttribute("id");
        }

        return null;
    }

    public void pageActivated(IWorkbenchPage iworkbenchpage)
    {
        IViewReference aiviewreference[] = iworkbenchpage.getViewReferences();
        for(int i = 0; i < aiviewreference.length; i++)
        {
            IWorkbenchPart iworkbenchpart = aiviewreference[i].getPart(false);
            if(iworkbenchpart != null)
                A(iworkbenchpart);
        }

        iworkbenchpage.addPartListener(((IPartListener) (this)));
    }

    public void pageClosed(IWorkbenchPage iworkbenchpage)
    {
        iworkbenchpage.removePartListener(((IPartListener) (this)));
    }

    public void pageOpened(IWorkbenchPage iworkbenchpage)
    {
    }

    public void partActivated(IWorkbenchPart iworkbenchpart)
    {
    }

    public void partBroughtToTop(IWorkbenchPart iworkbenchpart)
    {
    }

    public void partClosed(IWorkbenchPart iworkbenchpart)
    {
    }

    public void partDeactivated(IWorkbenchPart iworkbenchpart)
    {
    }

    public void partOpened(IWorkbenchPart iworkbenchpart)
    {
        A(iworkbenchpart);
    }

    private void A(IWorkbenchPart iworkbenchpart)
    {
        if(iworkbenchpart.getSite() == null)
            return;
        if((iworkbenchpart.getSite().getId().equals("org.eclipse.ui.views.ResourceNavigator") || iworkbenchpart.getSite().getId().equals("org.eclipse.jdt.ui.PackageExplorer")) && iworkbenchpart.getSite().getPage() != null && (iworkbenchpart.getSite().getPage() instanceof WorkbenchPage))
        {
            Composite composite = ((WorkbenchPage)iworkbenchpart.getSite().getPage()).getClientComposite();
            A(((Control) (composite)));
        }
    }

    private void A(Control control)
    {
        if(control.getData("DragSource") != null)
        {
            DragSource dragsource = (DragSource)control.getData("DragSource");
            dragsource.addDragListener(((DragSourceListener) (this)));
        }
        if(control instanceof Composite)
        {
            Composite composite = (Composite)control;
            Control acontrol[] = composite.getChildren();
            if(acontrol != null)
            {
                for(int i = 0; i < acontrol.length; i++)
                    A(acontrol[i]);

            }
        }
    }

    public void dragStart(DragSourceEvent dragsourceevent)
    {
        A(false);
    }

    public void dragSetData(DragSourceEvent dragsourceevent)
    {
    }

    public void dragFinished(DragSourceEvent dragsourceevent)
    {
        A(true);
    }

    private void A(boolean flag)
    {
        Iterator iterator = F.iterator();
        do
        {
            if(!iterator.hasNext())
                break;
            Control control = (Control)iterator.next();
            if(!control.isDisposed())
                control.setEnabled(flag);
        } while(true);
    }

    public static void addComposite(Composite composite)
    {
        boolean flag = F.add(((Object) (composite)));
        if(!$assertionsDisabled && !flag)
            throw new AssertionError();
        else
            return;
    }

    public static void removeComposite(Composite composite)
    {
        boolean flag = F.remove(((Object) (composite)));
        if(!$assertionsDisabled && !flag)
            throw new AssertionError();
        else
            return;
    }

    static Class _mthclass$(String s)
    {
        return Class.forName(s);
        ClassNotFoundException classnotfoundexception;
        classnotfoundexception;
        throw (new NoClassDefFoundError()).initCause(((Throwable) (classnotfoundexception)));
    }

    static 
    {
        $assertionsDisabled = !(com.m7.wide.eclipse.jstudio.Plugin.class).desiredAssertionStatus();
    }
}
