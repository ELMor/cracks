
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Properties;

public class Main {
	byte seed[] = null;

	String keyStream = null;

	private void init(Properties p) {
		String u = "::";
		keyStream = p.getProperty("Serial") + u + p.getProperty("VersionMajor")
				+ u + p.getProperty("VersionMinor") + u
				+ p.getProperty("OrderId") + u + p.getProperty("chk03") + u
				+ p.getProperty("ExpirationDate") + u
				+ p.getProperty("EmissionDate") + u
				+ DecoderEncoder.calcCRC32(p.getProperty("Serial"));
		seed = (new BigInteger(p.getProperty("BigInteger"), 16)).toByteArray();
	}

	public Main(String fileName) {
		Properties prop=null;
		try {
			prop = new Properties();
			prop.load(new FileInputStream(fileName));
			init(prop);
			DecoderEncoder dec = new DecoderEncoder(seed);
			prop.put("Activation", dec.undecode(keyStream));
			prop.save(new FileOutputStream(fileName), "Generated by crazyRecord");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("Falta properties");
			return;
		}
		new Main(args[0]);
	}
}