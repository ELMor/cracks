// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   LicenseKeyCheck.java

package the.vault;

import java.util.*;

// Referenced classes of package com.xhive.util:
//            Base64

public class Check {

  private static boolean ENCODE = true;
  private static final transient String ITEM_SEPARATOR = "#";
  private static final transient String PROPERTY_SEPARATOR = "/";

  public Check() {
  }

  public static String checkLicense(String licenseKey) {
    if (licenseKey == null) {
      return "No license key was entered in xhive.properties";
    }
    String companyName = getLicenseCompany(licenseKey);
    if (companyName == null) {
      return "License key is incorrect";
    }
    Date licenseDate = getLicenseDate(licenseKey);
    if (licenseDate == null) {
      return "License date is incorrect";
    }
    if (licenseDate.getTime() < (new Date()).getTime()) {
      return "License expired at " + licenseDate.toString();
    }
    String version = getVersion(licenseKey);
    if (!"4.0".equals( ( (Object) (version)))) {
      return "This license does not work for this version";
    }
    else {
      return null;
    }
  }

  public static String getLicenseProperty(String licenseKey, String propName) {
    String propValue = null;
    String inputText = getLicenseInputText(licenseKey);
    if (inputText == null) {
      return null;
    }
    try {
      StringTokenizer elements = new StringTokenizer(inputText, "#");
      elements.nextToken();
      String properties = elements.nextToken();
      if (properties != null) {
        propValue = getProperty(properties, propName);
      }
    }
    catch (Throwable e) {
      propValue = null;
    }
    return propValue;
  }

  private static String getProperty(String propertiesString,
                                    String propertyName) {
    Hashtable properties = new Hashtable();
    String propName;
    String propValue;
    for (StringTokenizer propTokens = new StringTokenizer(propertiesString, "/");
         propTokens.hasMoreTokens();
         properties.put( ( (Object) (propName)), ( (Object) (propValue)))) {
      String propItem = propTokens.nextToken();
      propName = propItem.substring(0, 1);
      propValue = propItem.substring(1);
    }

    return (String) properties.get( ( (Object) (propertyName)));
  }

  public static Date getLicenseDate(String licenseKey) {
    Date date = null;
    String inputText = getLicenseInputText(licenseKey);
    if (inputText == null) {
      return null;
    }
    try {
      StringTokenizer elements = new StringTokenizer(inputText, "#");
      String dateText = elements.nextToken();
      date = new Date(Long.parseLong(dateText));
    }
    catch (Throwable e) {
      date = null;
    }
    return date;
  }

  public static String getLicenseCompany(String licenseKey) {
    String company = getLicenseProperty(licenseKey, "C");
    if (company != null && ENCODE) {
      String decComp = Base64.decode(company);
      if (decComp != null) {
        company = decComp;
      }
    }
    return company;
  }

  public static String getVersion(String licenseKey) {
    return getLicenseProperty(licenseKey, "V").replace(' ', '.');
  }

  private static String getLicenseInputText(String licenseKey) {
    String licenseInputTextHashed;
    String cryptText;
    String cryptTest;
    int NUMBER_LENGTH = 3;
    int length = Integer.parseInt(licenseKey.substring(0, NUMBER_LENGTH));
    licenseInputTextHashed = licenseKey.substring(NUMBER_LENGTH,
                                                  length + NUMBER_LENGTH);
    cryptText = licenseKey.substring(length + NUMBER_LENGTH, licenseKey.length());
    String salt = cryptText.substring(0, 2);
    String licenseInputTextHashedReversed = (new StringBuffer(
        licenseInputTextHashed)).reverse().toString();
    cryptTest = Crypt.crypt(licenseInputTextHashedReversed, salt);
    if (!cryptTest.equals( ( (Object) (cryptText)))) {
      return null;
    }
    String licenseInputText = unHashData(licenseInputTextHashed);
    return licenseInputText;
  }

  public static String hashData(String dateText, Hashtable properties) {
    String dottedText = dateText + "#";
    if (properties != null && properties.size() > 0) {
      Enumeration keys = properties.keys();
      for (boolean first = true; keys.hasMoreElements(); first = false) {
        String key = (String) keys.nextElement();
        String value = (String) properties.get( ( (Object) (key)));
        if ("C".equals( ( (Object) (key))) && ENCODE) {
          value = Base64.encode(value);
        }
        if (!first) {
          dottedText = dottedText + "/";
        }
        dottedText = dottedText + key + value;
      }

    }
    dottedText = dottedText.replace(' ', '.');
    StringBuffer text = new StringBuffer(dottedText);
    text.reverse();
    return translateChars(text.toString(), true);
  }

  private static String unHashData(String inputText) {
    inputText = translateChars(inputText, false);
    StringBuffer text = new StringBuffer(inputText);
    text.reverse();
    String spacedText = text.toString().replace('.', ' ');
    return spacedText;
  }

  public static String translateChars(String input, boolean encrypt) {
    String original_table =
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890./=*";
    String translated_table =
        "2345abcUVWdefgijklmnohpqrstzABC7890./DEFGHIJKLMNOuvwxyPQRSTXYZ016*=";
    String ori;
    String trans;
    if (encrypt) {
      ori = original_table;
      trans = translated_table;
    }
    else {
      ori = translated_table;
      trans = original_table;
    }
    int i = 0;
    StringBuffer result = new StringBuffer();
    for (; i < input.length(); i++) {
      char c = input.charAt(i);
      int index = ori.indexOf( ( (int) (c)));
      if (index == -1) {
        result.append(c);
      }
      else {
        result.append(trans.charAt(index));
      }
    }

    return result.toString();
  }

}