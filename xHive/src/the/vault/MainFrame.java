package the.vault;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class MainFrame
    extends JFrame {
  JPanel contentPane;
  JLabel jLabel1 = new JLabel();
  JLabel jLabel2 = new JLabel();
  JTextField jTextField1 = new JTextField();
  JTextField jLabelKey = new JTextField();
  GridBagLayout gridBagLayout1 = new GridBagLayout();

  //Construct the frame
  public MainFrame() {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //Component initialization
  private void jbInit() throws Exception {
    contentPane = (JPanel)this.getContentPane();
    jLabel1.setToolTipText("");
    jLabel1.setText("Company Name");
    contentPane.setLayout(gridBagLayout1);
    this.setSize(new Dimension(413, 96));
    this.setTitle("X-HIVE Registration KeyGen");
    jLabel2.setText("Registration Key");
    jTextField1.setText("The Vault");
    jTextField1.addKeyListener(new MainFrame_jTextField1_keyAdapter(this));
    jTextField1.addInputMethodListener(new
                                       MainFrame_jTextField1_inputMethodAdapter(this));
    jLabelKey.setFont(new java.awt.Font("Dialog", 0, 9));
    jLabelKey.setText(Crack.Crack(jTextField1.getText()));
    contentPane.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(7, 5, 0, 288), 45, 10));
    contentPane.add(jLabel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.NONE,
        new Insets(0, 5, 0, 286), 41, 10));
    contentPane.add(jLabelKey, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
        new Insets(0, 5, 10, 7), 132, 8));
    contentPane.add(jTextField1, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
        , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
        new Insets(7, 98, 0, 7), 256, 1));
  }

  //Overridden so we can exit when window is closed
  protected void processWindowEvent(WindowEvent e) {
    super.processWindowEvent(e);
    if (e.getID() == WindowEvent.WINDOW_CLOSING) {
      System.exit(0);
    }
  }

  void jTextField1_inputMethodTextChanged(InputMethodEvent e) {
    jLabelKey.setText(Crack.Crack(jTextField1.getText()));
  }

  void jTextField1_keyTyped(KeyEvent e) {
    jLabelKey.setText(Crack.Crack(jTextField1.getText()));

  }

}

class MainFrame_jTextField1_inputMethodAdapter
    implements java.awt.event.InputMethodListener {
  MainFrame adaptee;

  MainFrame_jTextField1_inputMethodAdapter(MainFrame adaptee) {
    this.adaptee = adaptee;
  }

  public void inputMethodTextChanged(InputMethodEvent e) {
    adaptee.jTextField1_inputMethodTextChanged(e);
  }

  public void caretPositionChanged(InputMethodEvent e) {
  }
}

class MainFrame_jTextField1_keyAdapter
    extends java.awt.event.KeyAdapter {
  MainFrame adaptee;

  MainFrame_jTextField1_keyAdapter(MainFrame adaptee) {
    this.adaptee = adaptee;
  }

  public void keyTyped(KeyEvent e) {
    adaptee.jTextField1_keyTyped(e);
  }
}