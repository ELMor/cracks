// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   DesignerPlugin.java

package com.swtdesigner;

import com.instantiations.common.core.LicenseInfo;
import com.instantiations.common.core.LicenseManager;
import com.instantiations.common.core.LicenseProduct;
import com.instantiations.common.core.PluginCompatibility;
import com.instantiations.common.core.PluginLogContext;
import com.instantiations.common.core.ProductLog;
import com.instantiations.common.ui.PluginCompatibilityUI;
import com.swtdesigner.editors.MultiPageEditor;
import com.swtdesigner.model.IComponentInfo;
import com.swtdesigner.model.JavaInfo;
import com.swtdesigner.model.util.StringUtilities;
import com.swtdesigner.preferences.IDesignerPrefConstants;
import com.swtdesigner.properties.PropertyManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.security.Permission;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

public class DesignerPlugin extends AbstractUIPlugin
    implements IDesignerPrefConstants
{

    private static DesignerPlugin plugin;
    private ResourceBundle resourceBundle;
    private static boolean m_CtrlPressed;
    private static boolean m_ShiftPressed;
    private static Event m_LastKeyDownEvent;
    public static final boolean IS_WINDOWS;
    public static final boolean IS_UNIX;
    public static final boolean IS_MAC;
    public static final boolean IS_OS2;
    public static final boolean IS_LINUX;
    public static final boolean IS_MOTIF = "motif".equals(SWT.getPlatform());
    private static IStatus LAST_STATUS = null;
    private static HashMap m_NameToIconMap = new HashMap();

    public DesignerPlugin()
    {
        plugin = this;
        try
        {
            resourceBundle = ResourceBundle.getBundle("com.swtdesigner.DesignerPluginResources");
        }
        catch(MissingResourceException x)
        {
            resourceBundle = null;
        }
    }

    public void start(BundleContext context)
        throws Exception
    {
        super.start(context);
        ProductLog.setLogContext(new PluginLogContext(this));
        LicenseManager.initialize();
        try
        {
            PluginCompatibility.check(LicenseInfo.getProductGenericName(), this);
            PluginCompatibilityUI.warnUserIfNecessary();
        }
        catch(Exception e)
        {
            log(e);
        }
        System.setProperty("javax.swing.adjustPopupLocationToFit", "false");
        Listener listener = new Listener() {

            public void handleEvent(Event event)
            {
                if(event.type == 1 || event.type == 31)
                    DesignerPlugin.m_LastKeyDownEvent = event;
                else
                if(event.type != 2)
                    DesignerPlugin.m_LastKeyDownEvent = null;
                if(event.keyCode == 0x40000)
                {
                    if(event.type == 1)
                        DesignerPlugin.m_CtrlPressed = true;
                    if(event.type == 2)
                        DesignerPlugin.m_CtrlPressed = false;
                    return;
                }
                if(event.keyCode == 0x20000)
                {
                    if(event.type == 1)
                        DesignerPlugin.m_ShiftPressed = true;
                    if(event.type == 2)
                        DesignerPlugin.m_ShiftPressed = false;
                    return;
                } else
                {
                    return;
                }
            }

        };
        Display.getCurrent().addFilter(3, listener);
        Display.getCurrent().addFilter(4, listener);
        Display.getCurrent().addFilter(1, listener);
        Display.getCurrent().addFilter(2, listener);
        Display.getCurrent().addFilter(31, listener);
        installSecurityManager();
        System.setProperty("java.content.handler.pkgs", "com.swtdesigner.content");
    }

    public void stop(BundleContext context)
        throws Exception
    {
        PropertyManager.storeTrackingData();
        super.stop(context);
    }

    private void installSecurityManager()
    {
        if(System.getSecurityManager() == null)
            System.setSecurityManager(new SecurityManager() {

                public void checkPermission(Permission permission)
                {
                }

                public void checkPermission(Permission permission, Object obj)
                {
                }

                /**
                 * @deprecated Method checkExit is deprecated
                 */

                public void checkExit(int status)
                {
                    Class classes[];
                    int i;
                    try
                    {
                        throw new Exception();
                    }
                    catch(Throwable e)
                    {
                        StringWriter sw = new StringWriter();
                        PrintWriter pw = new PrintWriter(sw);
                        e.printStackTrace(pw);
                        if(sw.getBuffer().toString().indexOf("javax.swing.JFrame.setDefaultCloseOperation") != -1)
                            return;
                        classes = getClassContext();
                        i = 0;
                    }
                    for(; i < classes.length; i++)
                    {
                        ClassLoader classLoader = classes[i].getClassLoader();
                        if(classLoader != null)
                        {
                            String className = classLoader.getClass().getName();
                            if(className.equals("com.swtdesigner.model.util.ast.WorkspaceClassLoader") || className.equals("com.instantiations.assist.eclipse.junit.execution.core.UserDefinedClassLoader"))
                                throw new SecurityException("Exit from within user-loaded code");
                        }
                    }

                }

            });
    }

    public static DesignerPlugin getDefault()
    {
        return plugin;
    }

    public static boolean isCtrlPressed()
    {
        return m_CtrlPressed;
    }

    public static boolean isShiftPressed()
    {
        return m_ShiftPressed;
    }

    public static Event getLastKeyDownEvent()
    {
        return m_LastKeyDownEvent;
    }

    public static boolean isJDK15(IJavaProject project)
    {
        try
        {
            String complianceString = project.getOption("org.eclipse.jdt.core.compiler.compliance", true);
            if(complianceString != null)
            {
                double compliance = Double.parseDouble(complianceString);
                if(compliance >= 1.5D)
                    return true;
            }
        }
        catch(Throwable throwable) { }
        return false;
    }

    public static IWorkspace getWorkspace()
    {
        return ResourcesPlugin.getWorkspace();
    }

    public static String getResourceString(String key)
    {
        ResourceBundle bundle = getDefault().getResourceBundle();
        try
        {
            return bundle.getString(key);
        }
        catch(Throwable e)
        {
            return key;
        }
    }

    public ResourceBundle getResourceBundle()
    {
        return resourceBundle;
    }

    public static String getPluginId()
    {
        return getDefault().getBundle().getSymbolicName();
    }

    public static IStatus getLastStatus()
    {
        return LAST_STATUS;
    }

    public static void log(IStatus status)
    {
        LAST_STATUS = status;
        getDefault().getLog().log(status);
    }

    public static void log(String message)
    {
        log(((IStatus) (new Status(1, getPluginId(), 1, message, null))));
    }

    public static void log(Throwable e)
    {
        String errorMessage = e.getMessage();
        if(errorMessage != null && errorMessage.length() > 0 && errorMessage.charAt(0) == '[')
            return;
        e.printStackTrace();
        String message = e.getMessage();
        if(message == null)
            message = e.getClass().getName();
        log("Designer internal error: " + message, e);
    }

    public static void log(String message, Throwable e)
    {
        log(((IStatus) (createStatus(message, e))));
    }

    public static Status createStatus(String message, Throwable e)
    {
        return new Status(4, getPluginId(), 4, message, e) {

            public boolean isMultiStatus()
            {
                return true;
            }

        };
    }

    public static Image getImage(String name)
    {
        Image image = (Image)m_NameToIconMap.get(name);
        if(image == null)
        {
            try
            {
                URL url = new URL(LicenseInfo.getInstallURL(), "icons/" + name);
                image = new Image(Display.getCurrent(), url.openStream());
            }
            catch(IOException e)
            {
                log(e);
            }
            m_NameToIconMap.put(name, image);
        }
        return image;
    }

    public static InputStream getFile(String name)
    {
        try
        {
            return (new URL(LicenseInfo.getInstallURL(), name)).openStream();
        }
        catch(IOException e)
        {
            log(e);
        }
        return null;
    }

    public static ImageDescriptor getImageDescriptor(String name)
    {
        try
        {
            URL url = new URL(LicenseInfo.getInstallURL(), "icons/" + name);
            return ImageDescriptor.createFromURL(url);
        }
        catch(IOException e)
        {
            log(e);
        }
        return null;
    }

    public static Display getStandardDisplay()
    {
        Display display = Display.getCurrent();
        if(display == null)
            display = Display.getDefault();
        return display;
    }

    public static IWorkbenchWindow getActiveWorkbenchWindow()
    {
        return getDefault().getWorkbench().getActiveWorkbenchWindow();
    }

    public static Shell getShell()
    {
        if(getActiveWorkbenchWindow() != null)
            return getActiveWorkbenchWindow().getShell();
        else
            return null;
    }

    public static boolean isShellVisible()
    {
        return getShell() != null && getShell().isVisible();
    }

    public static MultiPageEditor getActiveDesignerEditor()
    {
        IWorkbenchWindow window = getActiveWorkbenchWindow();
        if(window == null)
            return null;
        IWorkbenchPage page = window.getActivePage();
        if(page == null)
            return null;
        org.eclipse.ui.IEditorPart editor = page.getActiveEditor();
        if(editor instanceof MultiPageEditor)
            return (MultiPageEditor)editor;
        else
            return null;
    }

    public static boolean syncExec(Runnable runnable)
    {
        Shell shell = getShell();
        if(shell != null)
        {
            Display display = shell.getDisplay();
            if(display != null)
            {
                if(Thread.currentThread() == display.getThread())
                    runnable.run();
                else
                    display.syncExec(runnable);
                return true;
            }
        }
        return false;
    }

    /**
     * @deprecated Method initializeDefaultPreferences is deprecated
     */

    protected void initializeDefaultPreferences(IPreferenceStore store)
    {
        super.initializeDefaultPreferences(store);
        store.setDefault("DefaultFormSizeX", 500);
        store.setDefault("DefaultFormSizeY", 375);
        store.setDefault("PreviewOffsetX", 100);
        store.setDefault("PreviewOffsetY", 100);
        store.setDefault("AllowAbsoluteLayout", true);
        store.setDefault("ShowNoLayoutMessage", true);
        store.setDefault("P_HIGHLIGHT_BORDERLESS_CONTAINERS", true);
        store.setDefault("P_USE_MINUS_AS_CHARACTER_WHEN_FULL_SELECTION", false);
        store.setDefault("P_DROP_COMBO_EDITOR_ON_ACTIVATION", false);
        store.setDefault("GridStep", 5);
        store.setDefault("HideGrid", false);
        store.setDefault("P_USE_GRID_STEP_WHEN_SHIFT", "0");
        store.setDefault("P_DIRECT_EDIT_MODE", "V_DIRECT_EDIT_MODE_ALT_CLICK");
        store.setDefault("DraftMode", !IS_WINDOWS);
        store.setDefault("P_USE_SWING_PREVIEW", IS_WINDOWS);
        store.setDefault("ShowThumbnail", false);
        store.setDefault("P_GOTO_DEFINITION", true);
        store.setDefault("P_SHOW_IMPORTANT_PROPERTIES", false);
        store.setDefault("AllowDoubleClick", true);
        store.setDefault("P_KEEP_REAL_EVENT_HANDLERS", true);
        store.setDefault("P_AUTOMATIC_AUTOSIZE", false);
        store.setDefault("P_SHOW_TEXT_IN_CONTROL_TREE", true);
        store.setDefault("P_SHOW_TITLE_PREFIX_IN_CONTROL_TREE", true);
        store.setDefault("P_GEN_ICOLORPROVIDER", false);
        store.setDefault("P_SHOW_PLATFORM_WIDGETS", false);
        store.setDefault("P_SEPARATE_EVENT_TABLE", true);
        store.setDefault("P_TEST_FREE_MODE", false);
        store.setDefault("P_USE_PALETTE_SCROLLBAR", false);
        store.setDefault("P_SUPPRESS_VERSION_WARNINGS", false);
        store.setDefault("P_NLS_INCLUDE_DEFAULT_VALUES", false);
        store.setDefault("P_NLS_AUTO_EXTERNALIZE", false);
        store.setDefault("P_NLS_CODE_STYLE", "V_NLS_CODE_STYLE_ECLIPSE");
        store.setDefault("P_PREFERENCE_SHOW_TREE", false);
        store.setDefault("P_FIELD_EDITORS_INNER_CONTROLS", "0");
        store.setDefault("P_LAYOUT_INHERIT", true);
        store.setDefault("P_LAYOUT_TYPE", "0");
        store.setDefault("P_GRID_ALIGNMENT_FIGURES", "V_ALIGNMENT_FIGURES_SELECTED");
        store.setDefault("P_GRID_LAYOUT_EDIT_POLICY", "V_GRID_POLICY_FREE");
        store.setDefault("P_GRID_LAYOUT_GRAB_TEXT", true);
        store.setDefault("P_GRID_LAYOUT_GRAB_TABLE", false);
        store.setDefault("P_GRID_LAYOUT_RIGHT_LABEL", false);
        store.setDefault("P_GRID_OLD_CODE_STYLE", false);
        store.setDefault("P_AUTO_ADD_CUSTOM_SWT_BEANS", true);
        store.setDefault("P_AUTO_ADD_CUSTOM_SWING_BEANS", true);
        store.setDefault("P_CHECK_CUSTOM_BEANS_DEPENDENCES", true);
        store.setDefault("P_FORMS_PAINT_BORDERS", true);
        store.setDefault("P_FORMS_AUTO_ADAPT_CONTROLS", true);
        store.setDefault("P_FRAMES_RECENT", "");
        store.setDefault("P_ALIGMENT_WINDOW_X", 200);
        store.setDefault("P_ALIGMENT_WINDOW_Y", 100);
        store.setDefault("DESIGNER_EDITOR_CANVAS_POSITION", 0);
        store.setDefault("DESIGNER_EDITOR_LAYOUT", 0);
        store.setDefault("DESIGNER_EDITOR_SYNC_DELAY", 500);
        store.setDefault("P_MAXIMUM_BEAN_INSTANTIATION_TIME", 5000);
        store.setDefault("HIDE_BEGIN", "//$hide>>$");
        store.setDefault("HIDE_END", "//$hide<<$");
        store.setDefault("HIDE_SINGLE", "//$hide$");
        store.setDefault("P_UNPARSED_STRING", "");
        store.setDefault("P_EVALUATE_COMPLEX_EXPRESSIONS", true);
        store.setDefault("P_LISTEN_FOR_CUSTOM_COMPONENTS_CHANGE", true);
        setFormLayoutDefaults(store);
        setSpringLayoutDefaults(store);
        initializeSwingPreferences(store);
        initializeCodeGenerationPreferences(store);
        initializeDiagnosticPreferences(store);
        initializePropertyManagerPreferences(store);
        initializePalettePreferences(store);
        initializePropertyCompositeFlyoutPreferences(store);
    }

    public static void setFormLayoutDefaults(IPreferenceStore store)
    {
        store.setDefault("P_FORM_LAYOUT_ALIGN_KEEP_STYLE", false);
        store.setDefault("P_FORM_LAYOUT_ALIGNMENT_FIGURES", "V_ALIGNMENT_FIGURES_SELECTED");
        PreferenceConverter.setDefault(store, "P_FORM_LAYOUT_PERCENT_COLOR", new RGB(128, 128, 128));
        PreferenceConverter.setDefault(store, "P_FORM_LAYOUT_OFFSET_COLOR", new RGB(0, 0, 255));
        store.setDefault("P_FORM_LAYOUT_SNAP_SENSITIVITY", 5);
        store.setDefault("P_FORM_LAYOUT_H_MARGIN", 5);
        store.setDefault("P_FORM_LAYOUT_V_MARGIN", 5);
        store.setDefault("P_FORM_LAYOUT_PH_MARGIN", 5);
        store.setDefault("P_FORM_LAYOUT_PV_MARGIN", 5);
        store.setDefault("P_FORM_LAYOUT_WIDGET_H_STEP", 5);
        store.setDefault("P_FORM_LAYOUT_WIDGET_V_STEP", 5);
        store.setDefault("P_FORM_LAYOUT_MODE", 0);
        store.setDefault("P_FORM_LAYOUT_H_PERCENTS", "");
        store.setDefault("P_FORM_LAYOUT_V_PERCENTS", "");
    }

    public static void setSpringLayoutDefaults(IPreferenceStore store)
    {
        store.setDefault("P_SPRING_LAYOUT_ALIGN_KEEP_STYLE", false);
        store.setDefault("P_SPRING_LAYOUT_ALIGNMENT_FIGURES", "V_ALIGNMENT_FIGURES_SELECTED");
        PreferenceConverter.setDefault(store, "P_SPRING_LAYOUT_PERCENT_COLOR", new RGB(128, 128, 128));
        PreferenceConverter.setDefault(store, "P_SPRING_LAYOUT_OFFSET_COLOR", new RGB(0, 0, 255));
        store.setDefault("P_SPRING_LAYOUT_SNAP_SENSITIVITY", 5);
        store.setDefault("P_SPRING_LAYOUT_H_MARGIN", 5);
        store.setDefault("P_SPRING_LAYOUT_V_MARGIN", 5);
        store.setDefault("P_SPRING_LAYOUT_WIDGET_H_STEP", 5);
        store.setDefault("P_SPRING_LAYOUT_WIDGET_V_STEP", 5);
        store.setDefault("P_SPRING_LAYOUT_MODE", 0);
    }

    protected static void initializeSwingPreferences(IPreferenceStore preferences)
    {
        preferences.setDefault("defaultLookAndFeel", "<current platform>");
        preferences.setDefault("LOOK_AND_FEEL_LIST_PREFERENCE", "JGoodies Plastic 3D,${plugin}/looks-1.3.1.jar,com.jgoodies.looks.plastic.Plastic3DLookAndFeel;JGoodies Plastic XP,${plugin}/looks-1.3.1.jar,com.jgoodies.looks.plastic.PlasticXPLookAndFeel;JGoodies Plastic,${plugin}/looks-1.3.1.jar,com.jgoodies.looks.plastic.PlasticLookAndFeel;JGoodies Windows,${plugin}/looks-1.3.1.jar,com.jgoodies.looks.windows.WindowsLookAndFeel;Liquid,${plugin}/liquidlnf.jar,com.birosoft.liquid.LiquidLookAndFeel;Kunststoff,${plugin}/kunststoff.jar,com.incors.plaf.kunststoff.KunststoffLookAndFeel");
        preferences.setDefault("displayExpertSwingProperties", false);
        preferences.setDefault("P_SWING_BORDER_LAYOUT_CTRL_FOR_DIRECTION_INDEPENDENT", false);
        preferences.setDefault("P_SWING_JSPINNER_EMPTY_CONSTRUCTOR", true);
        preferences.setDefault("P_SWING_SET_PREFERRED_SIZE_ON_MOVE", false);
        preferences.setDefault("P_SWING_GBL_EMPTY_CONSTRUCTOR", true);
        preferences.setDefault("P_SWING_GBL_SHOW_SPAN_HANDLES", true);
        preferences.setDefault("P_SWING_GBL_SHOW_INSET_HANDLES", true);
        preferences.setDefault("P_SWING_GBL_SHOW_FILL_HANDLES", true);
        preferences.setDefault("P_SWING_GBL_SHOW_HANDLES_TOOLTIPS", false);
        preferences.setDefault("P_SHOW_SWING_BORDERS_IN_TREE", false);
        preferences.setDefault("P_SHOW_OBJECT_PROPERTIES", false);
        preferences.setDefault("P_JGFL_ALIGNMENT_FIGURES", "V_ALIGNMENT_FIGURES_SELECTED");
        preferences.setDefault("P_JGFL_DEFAULT_COLUMN_COUNT", 2);
        preferences.setDefault("P_JGFL_DEFAULT_ROW_COUNT", 2);
        preferences.setDefault("P_JGFL_SHOW_PROMPTER", true);
        preferences.setDefault("P_JGCF_PALETTE", "V_JGCF_PALETTE_JAR");
        preferences.setDefault("P_SWING_LAYOUT_INHERIT", false);
        preferences.setDefault("P_SWING_LAYOUT_DEFAULT", "0");
    }

    protected static void initializeCodeGenerationPreferences(IPreferenceStore preferences)
    {
        preferences.setDefault("useExistingBlockStyle", true);
        preferences.setDefault("createComponentsInBlocks", false);
        preferences.setDefault("includeComponentPrefix", true);
        preferences.setDefault("componentPrefix", "");
        preferences.setDefault("methodNameForNewStatements", "");
        preferences.setDefault("methodNameForNewStatementsWrapInTry", false);
        preferences.setDefault("P_EVENT_CODE_TYPE", 0);
        preferences.setDefault("P_EVENT_INNER_CLASS_POSITION", 3);
        preferences.setDefault("createStubEventHandlerMethods", false);
        preferences.setDefault("stubEventHandlerMethodNameTemplate", "do_${component_name}_${event_name}");
        preferences.setDefault("autoCreateResourceManager", true);
        String classNames[] = getDefaultClassesWithOptions();
        preferences.setDefault("classesWithOptions", StringUtilities.mergeStrings(classNames, ","));
        for(int i = 0; i < classNames.length; i++)
            preferences.setDefault("createAsFieldByDefault." + classNames[i], true);

        preferences.setDefault("createComponentsAsFields", false);
        preferences.setDefault("createNonVisualBeansAsFields", true);
        preferences.setDefault("declarationStyle", 0);
        preferences.setDefault("makeDeclarationsFinal", true);
        preferences.setDefault("shareVariablesIfPossible", false);
        preferences.setDefault("CAPITALIZE_FIRST_LETTER_OF_FIELD", true);
        preferences.setDefault("PREFIX_FIELD_WITH_THIS", false);
        preferences.setDefault("createAccessorsForFields", false);
        preferences.setDefault("fieldReferenceStyle", 1);
        preferences.setDefault("useLocalVariableForAccessedFields", false);
        preferences.setDefault("accessorMethodUseLimit", 1);
        preferences.setDefault("fieldInitializationLocation", 2);
        preferences.setDefault("explicitlyInitializeFields", false);
        preferences.setDefault("renameTextComponentVariableNames", 1);
        preferences.setDefault("textComponentVariableNamesTemplate", "${text}${class_name}");
        preferences.setDefault("textComponentVariableNamesWordsLimit", 3);
        preferences.setDefault("useExistingContentPane", true);
    }

    protected static void initializeDiagnosticPreferences(IPreferenceStore preferences)
    {
        preferences.setDefault("diagnosticsEnabled", false);
    }

    protected static void initializePropertyManagerPreferences(IPreferenceStore preferences)
    {
        preferences.setDefault("usePropertyManager", true);
        preferences.setDefault("moveImportantToTop", true);
        preferences.setDefault("shouldTrackProperties", true);
        preferences.setDefault("trackThreshold", 25);
        preferences.setDefault("paintImportantBold", 2);
    }

    protected static void initializePalettePreferences(IPreferenceStore preferences)
    {
        preferences.setDefault("P_PALETTE_DOCK_LOCATION", 8);
        preferences.setDefault("P_PALETTE_STATE", 4);
        preferences.setDefault("P_PALETTE_WIDTH", 125);
        preferences.setDefault("P_PALETTE_OPEN_NO_HOVER", false);
        preferences.setDefault("P_SHOW_SWING_CONTAINER_PALETTE", true);
        preferences.setDefault("P_SHOW_SWING_LAYOUTS_PALETTE", true);
        preferences.setDefault("P_SHOW_SWING_CONTROLS_PALETTE", true);
        preferences.setDefault("P_SHOW_JGOODIES_CONTROLS_PALETTE", true);
        preferences.setDefault("P_SHOW_SWING_MENUS_PALETTE", true);
        preferences.setDefault("P_SHOW_AWT_PALETTE", false);
        preferences.setDefault("P_SHOW_SWT_COMPOSITES_PALETTE", true);
        preferences.setDefault("P_SHOW_SWT_LAYOUTS_PALETTE", true);
        preferences.setDefault("P_SHOW_SWT_CONTROLS_PALETTE", true);
        preferences.setDefault("P_SHOW_JFACE_VIEWERS_PALETTE", true);
        preferences.setDefault("P_SHOW_SWT_MENUS_PALETTE", true);
        preferences.setDefault("P_SHOW_SWT_AWT_PALETTE", true);
    }

    protected static void initializePropertyCompositeFlyoutPreferences(IPreferenceStore preferences)
    {
        preferences.setDefault("P_PROPERTY_COMPOSITE_DOCK_LOCATION", 8);
        preferences.setDefault("P_PROPERTY_COMPOSITE_STATE", 4);
        preferences.setDefault("P_PROPERTY_COMPOSITE_WIDTH", 200);
        preferences.setDefault("P_PROPERTY_COMPOSITE_OPEN_NO_HOVER", false);
    }

    public static boolean isPaletteOpenNoHover()
    {
        return getStore().getBoolean("P_PALETTE_OPEN_NO_HOVER");
    }

    public static boolean isPropertyCompositeOpenNoHover()
    {
        return getStore().getBoolean("P_PROPERTY_COMPOSITE_OPEN_NO_HOVER");
    }

    public static void addPropertyChangeListener(IPropertyChangeListener listener)
    {
        getStore().addPropertyChangeListener(listener);
    }

    public static void removePropertyChangeListener(IPropertyChangeListener listener)
    {
        getStore().removePropertyChangeListener(listener);
    }

    private static IPreferenceStore getStore()
    {
        return getDefault().getPreferenceStore();
    }

    public static boolean isAbsoluteLayoutEnabled()
    {
        return getStore().getBoolean("AllowAbsoluteLayout");
    }

    public static int snapGrid(int value)
    {
        int step = getGridStep();
        return (value / step) * step;
    }

    public static int getGridStep()
    {
        return getStore().getInt("GridStep");
    }

    public static boolean getHideGrid()
    {
        return getStore().getBoolean("HideGrid");
    }

    public static boolean useGridWhenShiftIsDown()
    {
        return getStore().getInt("P_USE_GRID_STEP_WHEN_SHIFT") == 0;
    }

    public static boolean showNoLayoutMessage()
    {
        return getStore().getBoolean("ShowNoLayoutMessage");
    }

    public static boolean highlightBorderlessContainers()
    {
        return getStore().getBoolean("P_HIGHLIGHT_BORDERLESS_CONTAINERS");
    }

    public static boolean useMinusAsCharacterWhenFullSelection()
    {
        return getStore().getBoolean("P_USE_MINUS_AS_CHARACTER_WHEN_FULL_SELECTION");
    }

    public static boolean shouldDropDownComboOnActivation()
    {
        return getStore().getBoolean("P_DROP_COMBO_EDITOR_ON_ACTIVATION");
    }

    public static String getDirectEditMode()
    {
        return getStore().getString("P_DIRECT_EDIT_MODE");
    }

    public static boolean isAutoDirectModeEnabled()
    {
        return getStore().getBoolean("P_AUTO_DIRECT_EDIT");
    }

    public static boolean isDraftModeEnabled()
    {
        return getStore().getBoolean("DraftMode");
    }

    public static boolean isSwingPreviewEnabled()
    {
        return getStore().getBoolean("P_USE_SWING_PREVIEW");
    }

    public static boolean testFreeMode()
    {
        return getStore().getBoolean("P_TEST_FREE_MODE");
    }

    public static boolean showThumbnail()
    {
        return getStore().getBoolean("ShowThumbnail");
    }

    public static boolean showPaletteScrollBar()
    {
        return getStore().getBoolean("P_USE_PALETTE_SCROLLBAR");
    }

    public static boolean suppressVersionWarnings()
    {
        return getStore().getBoolean("P_SUPPRESS_VERSION_WARNINGS");
    }

    public static boolean genIColorProvider()
    {
        return getStore().getBoolean("P_GEN_ICOLORPROVIDER");
    }

    public static boolean isGotoDefinitionEnabled()
    {
        return getStore().getBoolean("P_GOTO_DEFINITION");
    }

    public static boolean isShowImportantPropertiesEnabled()
    {
        return getStore().getBoolean("P_SHOW_IMPORTANT_PROPERTIES");
    }

    public static boolean isSelectionEventOnDoubleClick()
    {
        return getStore().getBoolean("AllowDoubleClick");
    }

    public static boolean shouldDeleteRealEventHandlersOnDelete()
    {
        return getStore().getBoolean("P_KEEP_REAL_EVENT_HANDLERS");
    }

    public static boolean isAutoSizeEnabled()
    {
        return getStore().getBoolean("P_AUTOMATIC_AUTOSIZE");
    }

    public static boolean canShowTextInControlTree()
    {
        return getStore().getBoolean("P_SHOW_TEXT_IN_CONTROL_TREE");
    }

    public static boolean canShowTitlePrefixInControlTree()
    {
        return getStore().getBoolean("P_SHOW_TITLE_PREFIX_IN_CONTROL_TREE");
    }

    public static boolean showPlatformWidgets()
    {
        return getStore().getBoolean("P_SHOW_PLATFORM_WIDGETS");
    }

    public static int getEventCodeType()
    {
        return getStore().getInt("P_EVENT_CODE_TYPE");
    }

    public static int getEventInnerClassPosition()
    {
        return getStore().getInt("P_EVENT_INNER_CLASS_POSITION");
    }

    public static boolean useSeparateEventTable()
    {
        return getStore().getBoolean("P_SEPARATE_EVENT_TABLE");
    }

    public static boolean useExpandEvents()
    {
        return getStore().getBoolean("P_EXPAND_EVENT_TABLE");
    }

    public static boolean isNlsIncludeDefaultValuesEnabled()
    {
        return getStore().getBoolean("P_NLS_INCLUDE_DEFAULT_VALUES");
    }

    public static boolean isNlsAutoExternalizeEnabled()
    {
        return getStore().getBoolean("P_NLS_AUTO_EXTERNALIZE");
    }

    public static boolean isNlsCodeStyleEclipse()
    {
        return "V_NLS_CODE_STYLE_ECLIPSE".equals(getStore().getString("P_NLS_CODE_STYLE"));
    }

    public static boolean isNlsCodeStyleJava()
    {
        return "V_NLS_CODE_STYLE_JAVA".equals(getStore().getString("P_NLS_CODE_STYLE"));
    }

    public static boolean showTreeInPreferencePage()
    {
        return getStore().getBoolean("P_PREFERENCE_SHOW_TREE");
    }

    public static int getFieldEditorsInnerControlMode()
    {
        return getStore().getInt("P_FIELD_EDITORS_INNER_CONTROLS");
    }

    public static boolean getFormLayoutAlignKeepStyle()
    {
        return getStore().getBoolean("P_FORM_LAYOUT_ALIGN_KEEP_STYLE");
    }

    public static String getFormLayoutAlignmentFigures()
    {
        return getStore().getString("P_FORM_LAYOUT_ALIGNMENT_FIGURES");
    }

    public static RGB getFormLayoutPercentColor()
    {
        return PreferenceConverter.getColor(getStore(), "P_FORM_LAYOUT_PERCENT_COLOR");
    }

    public static RGB getFormLayoutOffsetColor()
    {
        return PreferenceConverter.getColor(getStore(), "P_FORM_LAYOUT_OFFSET_COLOR");
    }

    public static int getFormLayoutSnapSensitivity()
    {
        return getStore().getInt("P_FORM_LAYOUT_SNAP_SENSITIVITY");
    }

    public static int getFormLayoutHMargin()
    {
        return getStore().getInt("P_FORM_LAYOUT_H_MARGIN");
    }

    public static int getFormLayoutVMargin()
    {
        return getStore().getInt("P_FORM_LAYOUT_V_MARGIN");
    }

    public static int getFormLayoutPHMargin()
    {
        return getStore().getInt("P_FORM_LAYOUT_PH_MARGIN");
    }

    public static int getFormLayoutPVMargin()
    {
        return getStore().getInt("P_FORM_LAYOUT_PV_MARGIN");
    }

    public static int getFormLayoutWHMargin()
    {
        return getStore().getInt("P_FORM_LAYOUT_WIDGET_H_STEP");
    }

    public static int getFormLayoutWVMargin()
    {
        return getStore().getInt("P_FORM_LAYOUT_WIDGET_V_STEP");
    }

    public static int getFormLayoutMode()
    {
        if(!LicenseManager.SWT_PRODUCT.isProOrEvalMode())
            return 1;
        else
            return getStore().getInt("P_FORM_LAYOUT_MODE");
    }

    public static ArrayList getFormLayoutHPercents()
    {
        return parseIntegerList(getStore().getString("P_FORM_LAYOUT_H_PERCENTS"));
    }

    public static ArrayList getFormLayoutVPercents()
    {
        return parseIntegerList(getStore().getString("P_FORM_LAYOUT_V_PERCENTS"));
    }

    public static ArrayList parseIntegerList(String s)
    {
        ArrayList result = new ArrayList();
        String parts[] = StringUtils.split(s);
        for(int i = 0; i < parts.length; i++)
        {
            String part = parts[i];
            result.add(Integer.valueOf(part));
        }

        return result;
    }

    public static boolean doSWTLayoutInheritance()
    {
        return getStore().getBoolean("P_LAYOUT_INHERIT");
    }

    public static int getDefaultSWTLayout()
    {
        try
        {
            String s = getStore().getString("P_LAYOUT_TYPE");
            return Integer.parseInt(s);
        }
        catch(NumberFormatException e)
        {
            return 0;
        }
    }

    public static String getGridLayoutEditPolicyType()
    {
        return getStore().getString("P_GRID_LAYOUT_EDIT_POLICY");
    }

    public static boolean isGridLayoutEditPolicyTypeFree()
    {
        return "V_GRID_POLICY_FREE".equals(getGridLayoutEditPolicyType());
    }

    public static String getGridLayoutAlignmentFigures()
    {
        return getStore().getString("P_GRID_ALIGNMENT_FIGURES");
    }

    public static boolean getGridLayoutUseOldCodeStyle()
    {
        return getStore().getBoolean("P_GRID_OLD_CODE_STYLE");
    }

    public static boolean getGridLayoutGrabText()
    {
        return getStore().getBoolean("P_GRID_LAYOUT_GRAB_TEXT");
    }

    public static boolean getGridLayoutGrabTable()
    {
        return getStore().getBoolean("P_GRID_LAYOUT_GRAB_TABLE");
    }

    public static boolean getGridLayoutRightLabel()
    {
        return getStore().getBoolean("P_GRID_LAYOUT_RIGHT_LABEL");
    }

    public static LinkedList getRecentFrameClassNames()
    {
        String value = getStore().getString("P_FRAMES_RECENT");
        LinkedList list = new LinkedList();
        CollectionUtils.addAll(list, StringUtils.split(value, ","));
        return list;
    }

    public static void addRecentFrameClassName(String name)
    {
        LinkedList list = getRecentFrameClassNames();
        list.remove(name);
        for(; list.size() > 10; list.removeLast());
        list.addFirst(name);
        getStore().setValue("P_FRAMES_RECENT", StringUtils.join(list.toArray(), ","));
    }

    public static LinkedList getRecentSwtBeanClassNames()
    {
        String value = getStore().getString("P_SWT_BEANS_RECENT");
        LinkedList list = new LinkedList();
        CollectionUtils.addAll(list, StringUtils.split(value, ","));
        return list;
    }

    public static void addRecentSwtBeanClassName(String name)
    {
        LinkedList list = getRecentSwtBeanClassNames();
        list.remove(name);
        for(; list.size() > 10; list.removeLast());
        list.addFirst(name);
        getStore().setValue("P_SWT_BEANS_RECENT", StringUtils.join(list.toArray(), ","));
    }

    public static boolean isSwingBorderLayoutCtrlForDirectionIndependent()
    {
        return getStore().getBoolean("P_SWING_BORDER_LAYOUT_CTRL_FOR_DIRECTION_INDEPENDENT");
    }

    public static void setSwingBorderLayoutCtrlForDirectionIndependent(boolean value)
    {
        getStore().setValue("P_SWING_BORDER_LAYOUT_CTRL_FOR_DIRECTION_INDEPENDENT", value);
    }

    public static boolean doSwingLayoutInheritance()
    {
        return getStore().getBoolean("P_SWING_LAYOUT_INHERIT");
    }

    public static void setDoSwingLayoutInheritance(boolean value)
    {
        getStore().setValue("P_SWING_LAYOUT_INHERIT", value);
    }

    public static int getDefaultSwingLayout()
    {
        try
        {
            String s = getStore().getString("P_SWING_LAYOUT_DEFAULT");
            return Integer.parseInt(s);
        }
        catch(NumberFormatException e)
        {
            return 0;
        }
    }

    public static void setDefaultSwingLayout(String value)
    {
        getStore().setValue("P_SWING_LAYOUT_DEFAULT", value);
    }

    public static boolean isSwingGBLEmptyConstructor()
    {
        return getStore().getBoolean("P_SWING_GBL_EMPTY_CONSTRUCTOR");
    }

    public static void setSwingGBLEmptyConstructor(boolean value)
    {
        getStore().setValue("P_SWING_GBL_EMPTY_CONSTRUCTOR", value);
    }

    public static boolean isSwingGBLShowSpanHandles()
    {
        return getStore().getBoolean("P_SWING_GBL_SHOW_SPAN_HANDLES");
    }

    public static void setSwingGBLShowSpanHandles(boolean value)
    {
        getStore().setValue("P_SWING_GBL_SHOW_SPAN_HANDLES", value);
    }

    public static boolean isSwingGBLShowInsetHandles()
    {
        return getStore().getBoolean("P_SWING_GBL_SHOW_INSET_HANDLES");
    }

    public static void setSwingGBLShowInsetHandles(boolean value)
    {
        getStore().setValue("P_SWING_GBL_SHOW_INSET_HANDLES", value);
    }

    public static boolean isSwingGBLShowFillHandles()
    {
        return getStore().getBoolean("P_SWING_GBL_SHOW_FILL_HANDLES");
    }

    public static void setSwingGBLShowFillHandles(boolean value)
    {
        getStore().setValue("P_SWING_GBL_SHOW_FILL_HANDLES", value);
    }

    public static boolean isSwingGBLShowHandlesToolTips()
    {
        return getStore().getBoolean("P_SWING_GBL_SHOW_HANDLES_TOOLTIPS");
    }

    public static void setSwingGBLShowHandlesToolTips(boolean value)
    {
        getStore().setValue("P_SWING_GBL_SHOW_HANDLES_TOOLTIPS", value);
    }

    public static boolean isJSpinnerEmptyConstructor()
    {
        return getStore().getBoolean("P_SWING_JSPINNER_EMPTY_CONSTRUCTOR");
    }

    public static void setJSpinnerEmptyConstructor(boolean value)
    {
        getStore().setValue("P_SWING_JSPINNER_EMPTY_CONSTRUCTOR", value);
    }

    public static boolean shouldSetSizeToPreferredOnMove()
    {
        return getStore().getBoolean("P_SWING_SET_PREFERRED_SIZE_ON_MOVE");
    }

    public static void setSetSizeToPreferredOnMove(boolean value)
    {
        getStore().setValue("P_SWING_SET_PREFERRED_SIZE_ON_MOVE", value);
    }

    public static String getSpringLayoutAlignmentFigures()
    {
        return getStore().getString("P_SPRING_LAYOUT_ALIGNMENT_FIGURES");
    }

    public static boolean getSpringLayoutAlignKeepStyle()
    {
        return getStore().getBoolean("P_SPRING_LAYOUT_ALIGN_KEEP_STYLE");
    }

    public static RGB getSpringLayoutPercentColor()
    {
        return PreferenceConverter.getColor(getStore(), "P_SPRING_LAYOUT_PERCENT_COLOR");
    }

    public static RGB getSpringLayoutOffsetColor()
    {
        return PreferenceConverter.getColor(getStore(), "P_SPRING_LAYOUT_OFFSET_COLOR");
    }

    public static int getSpringLayoutSnapSensitivity()
    {
        return getStore().getInt("P_SPRING_LAYOUT_SNAP_SENSITIVITY");
    }

    public static int getSpringLayoutHMargin()
    {
        return getStore().getInt("P_SPRING_LAYOUT_H_MARGIN");
    }

    public static int getSpringLayoutVMargin()
    {
        return getStore().getInt("P_SPRING_LAYOUT_V_MARGIN");
    }

    public static int getSpringLayoutWHMargin()
    {
        return getStore().getInt("P_SPRING_LAYOUT_WIDGET_H_STEP");
    }

    public static int getSpringLayoutWVMargin()
    {
        return getStore().getInt("P_SPRING_LAYOUT_WIDGET_V_STEP");
    }

    public static int getSpringLayoutMode()
    {
        if(!LicenseManager.SWING_PRODUCT.isProOrEvalMode())
            return 1;
        else
            return getStore().getInt("P_SPRING_LAYOUT_MODE");
    }

    public static String getJGFLAlignmentFigures()
    {
        return getStore().getString("P_JGFL_ALIGNMENT_FIGURES");
    }

    public static int getJGFLDefaultColumnCount()
    {
        return getStore().getInt("P_JGFL_DEFAULT_COLUMN_COUNT");
    }

    public static int getJGFLDefaultRowCount()
    {
        return getStore().getInt("P_JGFL_DEFAULT_ROW_COUNT");
    }

    public static boolean getJGFLShowPrompter()
    {
        return getStore().getBoolean("P_JGFL_SHOW_PROMPTER");
    }

    public static void setJGFLShowPrompter(boolean value)
    {
        getStore().setValue("P_JGFL_SHOW_PROMPTER", value);
    }

    public static String getJGCFPaletteMode()
    {
        return getStore().getString("P_JGCF_PALETTE");
    }

    public static boolean getExtStringsCurrentForm()
    {
        return getStore().getBoolean("P_EXT_STRINGS_CURRENT");
    }

    public static void setExtStringsCurrentForm(boolean value)
    {
        getStore().setValue("P_EXT_STRINGS_CURRENT", value);
    }

    public static boolean getFormsPaintBorders()
    {
        return getStore().getBoolean("P_FORMS_PAINT_BORDERS");
    }

    public static boolean getFormsAutoAdaptControls()
    {
        return getStore().getBoolean("P_FORMS_AUTO_ADAPT_CONTROLS");
    }

    public static LinkedList getRecentPanelClassNames()
    {
        String value = getStore().getString("P_PANELS_RECENT");
        LinkedList list = new LinkedList();
        CollectionUtils.addAll(list, StringUtils.split(value, ","));
        return list;
    }

    public static void addRecentPanelClassName(String name)
    {
        LinkedList list = getRecentPanelClassNames();
        list.remove(name);
        for(; list.size() > 10; list.removeLast());
        list.addFirst(name);
        getStore().setValue("P_PANELS_RECENT", StringUtils.join(list.toArray(), ","));
    }

    public static LinkedList getRecentBeanClassNames()
    {
        String value = getStore().getString("P_BEANS_RECENT");
        LinkedList list = new LinkedList();
        CollectionUtils.addAll(list, StringUtils.split(value, ","));
        return list;
    }

    public static void addRecentBeanClassName(String name)
    {
        LinkedList list = getRecentBeanClassNames();
        list.remove(name);
        for(; list.size() > 10; list.removeLast());
        list.addFirst(name);
        getStore().setValue("P_BEANS_RECENT", StringUtils.join(list.toArray(), ","));
    }

    public static int getAlignmentWindowX()
    {
        return getStore().getInt("P_ALIGMENT_WINDOW_X");
    }

    public static void setAlignmentWindowX(int value)
    {
        getStore().setValue("P_ALIGMENT_WINDOW_X", value);
    }

    public static int getAlignmentWindowY()
    {
        return getStore().getInt("P_ALIGMENT_WINDOW_Y");
    }

    public static void setAlignmentWindowY(int value)
    {
        getStore().setValue("P_ALIGMENT_WINDOW_Y", value);
    }

    public static String getActionDeleteId()
    {
        String id;
        try
        {
            id = ActionFactory.DELETE.getId();
        }
        catch(NoClassDefFoundError e)
        {
            id = deprecatedGetActionDeleteId();
        }
        return id;
    }

    /**
     * @deprecated Method deprecatedGetActionDeleteId is deprecated
     */

    private static String deprecatedGetActionDeleteId()
    {
        return "delete";
    }

    public static String getActionCopyId()
    {
        String id;
        try
        {
            id = ActionFactory.COPY.getId();
        }
        catch(NoClassDefFoundError e)
        {
            id = deprecetedGetActionCopyId();
        }
        return id;
    }

    /**
     * @deprecated Method deprecetedGetActionCopyId is deprecated
     */

    private static String deprecetedGetActionCopyId()
    {
        return "copy";
    }

    public static String getActionCutId()
    {
        String id;
        try
        {
            id = ActionFactory.CUT.getId();
        }
        catch(NoClassDefFoundError e)
        {
            id = deprecetedGetActionCutId();
        }
        return id;
    }

    /**
     * @deprecated Method deprecetedGetActionCutId is deprecated
     */

    private static String deprecetedGetActionCutId()
    {
        return "cut";
    }

    public static String getActionPasteId()
    {
        String id;
        try
        {
            id = ActionFactory.PASTE.getId();
        }
        catch(NoClassDefFoundError e)
        {
            id = deprecetedGetActionPasteId();
        }
        return id;
    }

    /**
     * @deprecated Method deprecetedGetActionPasteId is deprecated
     */

    private static String deprecetedGetActionPasteId()
    {
        return "paste";
    }

    public static int getEditorCanvasPosition()
    {
        return getStore().getInt("DESIGNER_EDITOR_CANVAS_POSITION");
    }

    public static void setEditorCanvasPosition(int value)
    {
        getStore().setValue("DESIGNER_EDITOR_CANVAS_POSITION", value);
    }

    public static int getEditorLayout()
    {
        return getStore().getInt("DESIGNER_EDITOR_LAYOUT");
    }

    public static void setEditorLayout(int value)
    {
        getStore().setValue("DESIGNER_EDITOR_LAYOUT", value);
    }

    public static boolean isPagesMode()
    {
        return getEditorLayout() == 0 || isDesignPageFirst();
    }

    public static boolean isDesignPageFirst()
    {
        return getEditorLayout() == 3;
    }

    public static int getEditorSyncDelay()
    {
        return getStore().getInt("DESIGNER_EDITOR_SYNC_DELAY");
    }

    public static void setEditorSyncDelay(int value)
    {
        getStore().setValue("DESIGNER_EDITOR_SYNC_DELAY", value);
    }

    public static String getHideBegin()
    {
        return getStore().getString("HIDE_BEGIN");
    }

    public static String getHideEnd()
    {
        return getStore().getString("HIDE_END");
    }

    public static String getHideSingle()
    {
        return getStore().getString("HIDE_SINGLE");
    }

    public static String getUnparsedString()
    {
        return getStore().getString("P_UNPARSED_STRING");
    }

    public static boolean getEvaluateComplexExpressions()
    {
        return getStore().getBoolean("P_EVALUATE_COMPLEX_EXPRESSIONS");
    }

    public static boolean getListenForCustomComponentsChange()
    {
        return getStore().getBoolean("P_LISTEN_FOR_CUSTOM_COMPONENTS_CHANGE");
    }

    public static int getMaximumBeanInstantiationTime()
    {
        return getStore().getInt("P_MAXIMUM_BEAN_INSTANTIATION_TIME");
    }

    public static void setMaximumBeanInstantiationTime(int value)
    {
        getStore().setValue("P_MAXIMUM_BEAN_INSTANTIATION_TIME", value);
    }

    public static boolean getDisplayExpertSwingProperties()
    {
        return getStore().getBoolean("displayExpertSwingProperties");
    }

    public static void setDisplayExpertSwingProperties(boolean display)
    {
        getStore().setValue("displayExpertSwingProperties", display);
    }

    public static boolean showSwingContainersPalette()
    {
        return getStore().getBoolean("P_SHOW_SWING_CONTAINER_PALETTE");
    }

    public static boolean showSwingLayoutsPalette()
    {
        return getStore().getBoolean("P_SHOW_SWING_LAYOUTS_PALETTE");
    }

    public static boolean showSwingControlsPalette()
    {
        return getStore().getBoolean("P_SHOW_SWING_CONTROLS_PALETTE");
    }

    public static boolean showJGoodiesControlsPalette()
    {
        return getStore().getBoolean("P_SHOW_JGOODIES_CONTROLS_PALETTE");
    }

    public static boolean showSwingMenusPalette()
    {
        return getStore().getBoolean("P_SHOW_SWING_MENUS_PALETTE");
    }

    public static boolean showAWTPalette()
    {
        return getStore().getBoolean("P_SHOW_AWT_PALETTE");
    }

    public static boolean showSWTCompositesPalette()
    {
        return getStore().getBoolean("P_SHOW_SWT_COMPOSITES_PALETTE");
    }

    public static boolean showSWTLayoutsPalette()
    {
        return getStore().getBoolean("P_SHOW_SWT_LAYOUTS_PALETTE");
    }

    public static boolean showSWTControlsPalette()
    {
        return getStore().getBoolean("P_SHOW_SWT_CONTROLS_PALETTE");
    }

    public static boolean showJFaceViewersPalette()
    {
        return getStore().getBoolean("P_SHOW_JFACE_VIEWERS_PALETTE");
    }

    public static boolean showSWTMenusPalette()
    {
        return getStore().getBoolean("P_SHOW_SWT_MENUS_PALETTE");
    }

    public static boolean showSWTAWTPalette()
    {
        return getStore().getBoolean("P_SHOW_SWT_AWT_PALETTE");
    }

    public static boolean isShowSwingBordersInTree()
    {
        return getStore().getBoolean("P_SHOW_SWING_BORDERS_IN_TREE");
    }

    public static void setShowSwingBordersInTree(boolean value)
    {
        getStore().setValue("P_SHOW_SWING_BORDERS_IN_TREE", value);
    }

    public static boolean isShowObjectProperties()
    {
        return getStore().getBoolean("P_SHOW_OBJECT_PROPERTIES");
    }

    public static void setShowObjectProperties(boolean value)
    {
        getStore().setValue("P_SHOW_OBJECT_PROPERTIES", value);
    }

    public static boolean usePropertyManager()
    {
        return getStore().getBoolean("usePropertyManager");
    }

    public static void setUsePropertyManager(boolean value)
    {
        getStore().setValue("usePropertyManager", value);
    }

    public static boolean isPropertiesShouldTracked()
    {
        return getStore().getBoolean("shouldTrackProperties");
    }

    public static void setPropertiesShouldTracked(boolean value)
    {
        getStore().setValue("shouldTrackProperties", value);
    }

    public static void setTrackingThreshold(int value)
    {
        getStore().setValue("trackThreshold", value);
    }

    public static int getTrackingThreshold()
    {
        return getStore().getInt("trackThreshold");
    }

    public static boolean isImportantShouldMovedTop()
    {
        return getStore().getBoolean("moveImportantToTop");
    }

    public static void setImportantShouldMovedTop(boolean value)
    {
        getStore().setValue("moveImportantToTop", value);
    }

    public static int getPropertyManagerBoldFontUsage()
    {
        return getStore().getInt("paintImportantBold");
    }

    public static void setPropertyManagerBoldFontUsage(int value)
    {
        getStore().setValue("paintImportantBold", value);
    }

    public static boolean getUseExistingBlockStyle()
    {
        return getStore().getBoolean("useExistingBlockStyle");
    }

    public static void setUseExistingBlockStyle(boolean useExistingStyle)
    {
        getStore().setValue("useExistingBlockStyle", useExistingStyle);
    }

    public static boolean getCreateComponentsInBlocks()
    {
        return getStore().getBoolean("createComponentsInBlocks");
    }

    public static void setCreateComponentsInBlocks(boolean createComponentsInBlocks)
    {
        getStore().setValue("createComponentsInBlocks", createComponentsInBlocks);
    }

    public static boolean getComponentsAsFields()
    {
        return getStore().getBoolean("createComponentsAsFields");
    }

    public static void setComponentsAsFields(boolean createComponentsAsFields)
    {
        getStore().setValue("createComponentsAsFields", createComponentsAsFields);
    }

    public static boolean getNonVisualBeansAsFields()
    {
        return getStore().getBoolean("createNonVisualBeansAsFields");
    }

    public static void setNonVisualBeansAsFields(boolean createNonVisualBeansAsFields)
    {
        getStore().setValue("createNonVisualBeansAsFields", createNonVisualBeansAsFields);
    }

    public static boolean getIncludeComponentPrefix()
    {
        return getStore().getBoolean("includeComponentPrefix");
    }

    public static void setIncludeComponentPrefix(boolean includeComponentPrefix)
    {
        getStore().setValue("includeComponentPrefix", includeComponentPrefix);
    }

    public static String getComponentPrefix()
    {
        return getStore().getString("componentPrefix");
    }

    public static String getMethodNameForNewStatements()
    {
        return getStore().getString("methodNameForNewStatements");
    }

    public static boolean shouldWrapMethodForNewStatementsInTry()
    {
        return getStore().getBoolean("methodNameForNewStatementsWrapInTry");
    }

    public static void setComponentPrefix(String componentPrefix)
    {
        getStore().setValue("componentPrefix", componentPrefix);
    }

    public static boolean getCreateStubEventHandlerMethods()
    {
        return getStore().getBoolean("createStubEventHandlerMethods");
    }

    public static void setCreateStubEventHandlerMethods(boolean createStubEventHandlerMethods)
    {
        getStore().setValue("createStubEventHandlerMethods", createStubEventHandlerMethods);
    }

    public static String getStubEventHandlerMethodNameTemplate()
    {
        return getStore().getString("stubEventHandlerMethodNameTemplate");
    }

    public static void setStubEventHandlerMethodNameTemplate(String stubEventHandlerMethodNameTemplate)
    {
        getStore().setValue("stubEventHandlerMethodNameTemplate", stubEventHandlerMethodNameTemplate);
    }

    public static boolean getAutoCreateResourceManager()
    {
        return getStore().getBoolean("autoCreateResourceManager");
    }

    public static void setAutoCreateResourceManager(boolean value)
    {
        getStore().setValue("autoCreateResourceManager", value);
    }

    public static String[] getCustomSwingBeanClassNames()
    {
        return getClassNames("P_CUSTOM_SWING_BEANS");
    }

    public static void addCustomSwingBeanClassName(String className)
    {
        addClassName(className, "P_CUSTOM_SWING_BEANS");
    }

    public static void removeCustomSwingBeanClassName(String className)
    {
        removeClassName(className, "P_CUSTOM_SWING_BEANS");
    }

    public static boolean getAutoAddCustomSwingBeans()
    {
        return getStore().getBoolean("P_AUTO_ADD_CUSTOM_SWING_BEANS");
    }

    public static void setAutoAddCustomSwingBeans(boolean autoAdd)
    {
        getStore().setValue("P_AUTO_ADD_CUSTOM_SWING_BEANS", autoAdd);
    }

    public static String[] getCustomSWTBeanClassNames()
    {
        return getClassNames("P_CUSTOM_SWT_BEANS");
    }

    public static void addCustomSWTBeanClassName(String className)
    {
        addClassName(className, "P_CUSTOM_SWT_BEANS");
    }

    public static void removeCustomSWTBeanClassName(String className)
    {
        removeClassName(className, "P_CUSTOM_SWT_BEANS");
    }

    public static boolean getAutoAddCustomSWTBeans()
    {
        return getStore().getBoolean("P_AUTO_ADD_CUSTOM_SWT_BEANS");
    }

    public static void setAutoAddCustomSWTBeans(boolean autoAdd)
    {
        getStore().setValue("P_AUTO_ADD_CUSTOM_SWT_BEANS", autoAdd);
    }

    public static boolean getCheckCustomBeansDependences()
    {
        return getStore().getBoolean("P_CHECK_CUSTOM_BEANS_DEPENDENCES");
    }

    public static void setCheckCustomBeansDependences(boolean check)
    {
        getStore().setValue("P_CHECK_CUSTOM_BEANS_DEPENDENCES", check);
    }

    public static String[] getClassesWithOptions()
    {
        return getClassNames("classesWithOptions");
    }

    public static String[] getDefaultClassesWithOptions()
    {
        if(LicenseManager.SWT_PRODUCT.isProOrEvalMode())
            if(LicenseManager.SWING_PRODUCT.isProOrEvalMode())
                return (new String[] {
                    "Combo", "CCombo", "List", "StyledText", "Table", "TableTree", "Text", "Tree", "JComboBox", "JList", 
                    "JTable", "JTextField", "JPasswordField", "JFormattedTextField", "JTextArea", "JTree", "JSpinner"
                });
            else
                return (new String[] {
                    "Combo", "CCombo", "List", "StyledText", "Table", "TableTree", "Text", "Tree"
                });
        if(LicenseManager.SWING_PRODUCT.isProOrEvalMode())
            return (new String[] {
                "JComboBox", "JList", "JTable", "JTextField", "JPasswordField", "JFormattedTextField", "JTextArea", "JTree", "JSpinner"
            });
        else
            return new String[0];
    }

    public static void addClassWithOptions(String className)
    {
        addClassName(className, "classesWithOptions");
    }

    public static void removeClassWithOptions(String className)
    {
        removeClassName(className, "classesWithOptions");
        getStore().setToDefault("createAsFieldByDefault." + className);
        getStore().setToDefault("defaultComponentName." + className);
    }

    private static String[] getClassNames(String preferenceKey)
    {
        String classList = getStore().getString(preferenceKey);
        if(classList == null || classList.length() == 0)
            return new String[0];
        else
            return StringUtilities.splitString(classList, ",", false);
    }

    private static void addClassName(String className, String preferenceKey)
    {
        String classList = getStore().getString(preferenceKey);
        int index = classList.indexOf(className);
        if(index >= 0)
            return;
        if(classList.length() > 0)
            classList = classList + ',' + className;
        else
            classList = className;
        getStore().setValue(preferenceKey, classList);
    }

    private static void removeClassName(String className, String preferenceKey)
    {
        String classList = getStore().getString(preferenceKey);
        int firstIndex = classList.indexOf(className);
        if(firstIndex < 0)
            return;
        int lastIndex = classList.indexOf(',', firstIndex);
        if(firstIndex == 0)
        {
            if(lastIndex < 0)
                classList = "";
            else
                classList = classList.substring(lastIndex + 1);
        } else
        if(lastIndex < 0)
            classList = classList.substring(0, firstIndex - 1);
        else
            classList = classList.substring(0, firstIndex) + classList.substring(lastIndex + 1);
        getStore().setValue(preferenceKey, classList);
    }

    public static boolean getCreateAsFieldByDefault(String className)
    {
        return getStore().getBoolean("createAsFieldByDefault." + className);
    }

    public static boolean getCreateAsFieldByDefault(JavaInfo info)
    {
        if(getComponentsAsFields() && (info instanceof IComponentInfo))
            return true;
        else
            return getCreateAsFieldByDefault(info.getVariableType());
    }

    public static boolean getDefaultCreateAsFieldByDefault(String className)
    {
        String defaultClasses[] = getDefaultClassesWithOptions();
        for(int i = 0; i < defaultClasses.length; i++)
            if(defaultClasses[i].equals(className))
                return true;

        return false;
    }

    public static void setCreateAsFieldByDefault(String className, boolean createAsField)
    {
        getStore().setValue("createAsFieldByDefault." + className, createAsField);
    }

    public static String getDefaultComponentName(String className)
    {
        IPreferenceStore store = getStore();
        String defaultName = store.getString("defaultComponentName." + className);
        if(defaultName.length() <= 0)
        {
            defaultName = getDefaultDefaultComponentName(className);
            store.setDefault("defaultComponentName." + className, defaultName);
        }
        return defaultName;
    }

    public static String getDefaultDefaultComponentName(String className)
    {
        int index = className.lastIndexOf('.');
        String defaultName;
        if(index < 0)
            defaultName = className;
        else
            defaultName = className.substring(index + 1);
        if(Character.isLowerCase(defaultName.charAt(0)))
        {
            defaultName = defaultName + '_';
            getStore().setDefault("defaultComponentName." + className, defaultName);
            return defaultName;
        }
        int maxIndex = defaultName.length() - 1;
        for(index = 0; index < maxIndex && Character.isUpperCase(defaultName.charAt(index)); index++);
        if(--index <= 0)
            defaultName = StringUtils.uncapitalize(defaultName);
        else
            defaultName = StringUtils.uncapitalize(defaultName.substring(index));
        return defaultName;
    }

    public static void setDefaultComponentName(String className, String defaultName)
    {
        getStore().setValue("defaultComponentName." + className, defaultName);
    }

    public static int getDeclarationStyle()
    {
        return getStore().getInt("declarationStyle");
    }

    public static void setDeclarationStyle(int declarationStyle)
    {
        getStore().setValue("declarationStyle", declarationStyle);
    }

    public static boolean getMakeDeclarationsFinal()
    {
        return getStore().getBoolean("makeDeclarationsFinal");
    }

    public static void setMakeDeclarationsFinal(boolean makeDeclarationsFinal)
    {
        getStore().setValue("makeDeclarationsFinal", makeDeclarationsFinal);
    }

    public static boolean getShareVariablesIfPossible()
    {
        return getStore().getBoolean("shareVariablesIfPossible");
    }

    public static void setShareVariablesIfPossible(boolean shareVariables)
    {
        getStore().setValue("shareVariablesIfPossible", shareVariables);
    }

    public static boolean getCapitalizeFirstLetterOfField()
    {
        return getStore().getBoolean("CAPITALIZE_FIRST_LETTER_OF_FIELD");
    }

    public static boolean getPrefixFieldWithThis()
    {
        return getStore().getBoolean("PREFIX_FIELD_WITH_THIS");
    }

    public static boolean getCreateAccessorsForFields()
    {
        return getStore().getBoolean("createAccessorsForFields");
    }

    public static void setCreateAccessorsForFields(boolean createAccessors)
    {
        getStore().setValue("createAccessorsForFields", createAccessors);
    }

    public static boolean getReferenceFieldUsingMethod()
    {
        return getFieldReferenceStyle() == 1;
    }

    public static int getFieldReferenceStyle()
    {
        return getStore().getInt("fieldReferenceStyle");
    }

    public static void setFieldReferenceStyle(int style)
    {
        getStore().setValue("fieldReferenceStyle", style);
    }

    public static boolean getUseLocalVariableForAccessedFields()
    {
        return getStore().getBoolean("useLocalVariableForAccessedFields");
    }

    public static void setUseLocalVariableForAccessedFields(boolean useLocal)
    {
        getStore().setValue("useLocalVariableForAccessedFields", useLocal);
    }

    public static int getAccessorMethodUseLimit()
    {
        return getStore().getInt("accessorMethodUseLimit");
    }

    public static void setAccessorMethodUseLimit(int limit)
    {
        getStore().setValue("accessorMethodUseLimit", limit);
    }

    public static int getFieldInitializationLocation()
    {
        return getStore().getInt("fieldInitializationLocation");
    }

    public static void setFieldInitializationLocation(int location)
    {
        getStore().setValue("fieldInitializationLocation", location);
    }

    public static boolean getExplicitlyInitializeFields()
    {
        return getFieldInitializationLocation() != 0 && getStore().getBoolean("explicitlyInitializeFields");
    }

    public static void setExplicitlyInitializeFields(boolean initialize)
    {
        getStore().setValue("explicitlyInitializeFields", initialize);
    }

    public static String getComponentNameAcronym(String className)
    {
        IPreferenceStore store = getStore();
        String acronym = store.getString("defaultComponentNameAcronym." + className);
        if(acronym.length() == 0)
        {
            acronym = generateDefaultComponentNameAcronym(className);
            store.setDefault("defaultComponentNameAcronym." + className, acronym);
        }
        return acronym;
    }

    public static String generateDefaultComponentNameAcronym(String className)
    {
        String acronym = StringUtilities.stripLeadingUppercaseChars(className, 1).toLowerCase();
        String specialCase = processSpecialCases(acronym);
        if(specialCase != null)
            return specialCase;
        acronym = StringUtils.replaceChars(acronym, "aeiouy", null);
        char acronymChars[] = acronym.toCharArray();
        StringBuffer buffer = new StringBuffer();
        for(int i = 0; i < acronymChars.length - 1; i++)
            if(acronymChars[i] != acronymChars[i + 1])
                buffer.append(acronymChars[i]);

        buffer.append(acronymChars[acronymChars.length - 1]);
        if(buffer.length() == 0)
            return className;
        else
            return new String(buffer);
    }

    private static String processSpecialCases(String className)
    {
        if(className.indexOf("tree") != -1)
            return "tree";
        if(className.indexOf("format") != -1)
            return "fmt";
        if(className.indexOf("string") != -1)
            return "str";
        if(className.indexOf("password") != -1)
            return "pwd";
        else
            return null;
    }

    public static void setComponentNameAcronym(String className, String acronymName)
    {
        getStore().setValue("defaultComponentNameAcronym." + className, acronymName);
    }

    public static int getTextComponentVariableWordsLimit()
    {
        return getStore().getInt("textComponentVariableNamesWordsLimit");
    }

    public static int getRenameTextComponentVariableNames()
    {
        return getStore().getInt("renameTextComponentVariableNames");
    }

    public static String getTextComponentVariableNamesTemplate()
    {
        return getStore().getString("textComponentVariableNamesTemplate");
    }

    public static boolean getUseExistingContentPane()
    {
        return getStore().getBoolean("useExistingContentPane");
    }

    public static void setUseExistingContentPane(boolean useExisting)
    {
        getStore().setValue("useExistingContentPane", useExisting);
    }

    public static boolean getDiagnosticsEnabled()
    {
        return getStore().getBoolean("diagnosticsEnabled");
    }

    public static void setDiagnosticsEnabled(boolean enableDiagnostics)
    {
        getStore().setValue("diagnosticsEnabled", enableDiagnostics);
    }

    static 
    {
        if(SystemUtils.OS_NAME.startsWith("Windows"))
        {
            IS_WINDOWS = true;
            IS_UNIX = false;
            IS_MAC = false;
            IS_OS2 = false;
            IS_LINUX = false;
        } else
        if(SystemUtils.OS_NAME.startsWith("SunOS"))
        {
            IS_WINDOWS = false;
            IS_UNIX = true;
            IS_MAC = false;
            IS_OS2 = false;
            IS_LINUX = false;
        } else
        if(SystemUtils.OS_NAME.startsWith("Solaris"))
        {
            IS_WINDOWS = false;
            IS_UNIX = true;
            IS_MAC = false;
            IS_OS2 = false;
            IS_LINUX = false;
        } else
        if(SystemUtils.OS_NAME.startsWith("Linux"))
        {
            IS_WINDOWS = false;
            IS_UNIX = true;
            IS_MAC = false;
            IS_OS2 = false;
            IS_LINUX = true;
        } else
        if(SystemUtils.OS_NAME.startsWith("HP-UX"))
        {
            IS_WINDOWS = false;
            IS_UNIX = true;
            IS_MAC = false;
            IS_OS2 = false;
            IS_LINUX = false;
        } else
        if(SystemUtils.OS_NAME.startsWith("AIX"))
        {
            IS_WINDOWS = false;
            IS_UNIX = true;
            IS_MAC = false;
            IS_OS2 = false;
            IS_LINUX = false;
        } else
        if(SystemUtils.OS_NAME.startsWith("Irix"))
        {
            IS_WINDOWS = false;
            IS_UNIX = true;
            IS_MAC = false;
            IS_OS2 = false;
            IS_LINUX = false;
        } else
        if(SystemUtils.OS_NAME.startsWith("Digital Unix"))
        {
            IS_WINDOWS = false;
            IS_UNIX = true;
            IS_MAC = false;
            IS_OS2 = false;
            IS_LINUX = false;
        } else
        if(SystemUtils.OS_NAME.startsWith("OS/400"))
        {
            IS_WINDOWS = false;
            IS_UNIX = true;
            IS_MAC = false;
            IS_OS2 = false;
            IS_LINUX = false;
        } else
        if(SystemUtils.OS_NAME.startsWith("OS/2"))
        {
            IS_WINDOWS = false;
            IS_UNIX = false;
            IS_MAC = false;
            IS_OS2 = true;
            IS_LINUX = false;
        } else
        if(SystemUtils.OS_NAME.startsWith("Mac OS X"))
        {
            IS_WINDOWS = false;
            IS_UNIX = true;
            IS_MAC = true;
            IS_OS2 = false;
            IS_LINUX = false;
        } else
        if(SystemUtils.OS_NAME.startsWith("Mac"))
        {
            IS_WINDOWS = false;
            IS_UNIX = false;
            IS_MAC = true;
            IS_OS2 = false;
            IS_LINUX = false;
        } else
        {
            IS_WINDOWS = false;
            IS_UNIX = false;
            IS_MAC = false;
            IS_OS2 = false;
            IS_LINUX = false;
        }
    }



}
