// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseByEmailWizardPageX.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseInfo;
import com.instantiations.common.core.LicenseUtil;
import com.swtdesigner.ResourceManager;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

// Referenced classes of package com.instantiations.common.ui:
//            LicenseWizardPageX, OpenBrowserAction

public class LicenseByEmailWizardPageX extends LicenseWizardPageX
{

    private CLabel _emailAddressLabel;
    private Text _emailBodyField;

    public LicenseByEmailWizardPageX()
    {
        super("wizardPage");
        setTitle("Email Registration");
        setDescription("Register and activate this product by email");
    }

    public void createControl(Composite parent)
    {
        Composite container = new Composite(parent, 0);
        container.setLayout(new FormLayout());
        Label label_1 = new Label(container, 0);
        FormData formData = new FormData();
        formData.top = new FormAttachment(0, 5);
        formData.left = new FormAttachment(0, 5);
        label_1.setLayoutData(formData);
        label_1.setText("To complete registration and activation of this product,\nplease email this to:");
        Composite composite_1 = new Composite(container, 0);
        formData = new FormData();
        formData.right = new FormAttachment(100, -5);
        formData.top = new FormAttachment(label_1, 5, 1024);
        formData.left = new FormAttachment(label_1, 0, 16384);
        composite_1.setLayoutData(formData);
        GridLayout gridLayout = new GridLayout();
        gridLayout.marginHeight = 0;
        gridLayout.marginWidth = 0;
        gridLayout.numColumns = 3;
        composite_1.setLayout(gridLayout);
        _emailAddressLabel = new CLabel(composite_1, 0);
        _emailAddressLabel.setLayoutData(new GridData(576));
        _emailAddressLabel.setForeground(ResourceManager.getColor(128, 0, 64));
        _emailAddressLabel.setText("registration@swt-designer.com");
        Composite composite = new Composite(composite_1, 0);
        GridLayout gridLayout_1 = new GridLayout();
        gridLayout_1.makeColumnsEqualWidth = true;
        gridLayout_1.marginHeight = 0;
        gridLayout_1.marginWidth = 0;
        gridLayout_1.numColumns = 2;
        composite.setLayout(gridLayout_1);
        Button button = new Button(composite, 0);
        button.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                createEmail();
            }

        });
        button.setLayoutData(new GridData(256));
        button.setText("Create email");
        button = new Button(composite, 0);
        button.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                copyEmailToClipboard();
            }

        });
        button.setText("Copy to Clipboard");
        _emailBodyField = new Text(container, 2824);
        formData = new FormData();
        formData.bottom = new FormAttachment(100, 0);
        formData.right = new FormAttachment(composite_1, 0, 0x20000);
        formData.top = new FormAttachment(composite_1, 5, 1024);
        formData.left = new FormAttachment(composite_1, 0, 16384);
        _emailBodyField.setLayoutData(formData);
        _emailBodyField.setText("");
        setControl(container);
    }

    public String getEmailAddress()
    {
        return _emailAddressLabel.getText();
    }

    public void setEmailAddress(String string)
    {
        _emailAddressLabel.setText(string);
    }

    public String getEmailBody()
    {
        return _emailBodyField.getText();
    }

    public void setEmailBody(String string)
    {
        _emailBodyField.setText(string);
    }

    protected void createEmail()
    {
        StringBuffer buf = new StringBuffer(1000);
        buf.append("mailto:");
        buf.append(getEmailAddress());
        buf.append("?Subject=" + LicenseInfo.getProductGenericName() + " Activation Request");
        buf.append("&Body=");
        buf.append(LicenseUtil.replace(getEmailBody(), LicenseUtil.LINE_END, "%0A"));
        OpenBrowserAction action = new OpenBrowserAction("Activation Request", null, buf.toString());
        action.run();
    }

    protected void copyEmailToClipboard()
    {
        StringBuffer buf = new StringBuffer(1000);
        buf.append("To: ");
        buf.append(getEmailAddress());
        buf.append(LicenseUtil.LINE_END);
        buf.append("Subject: " + LicenseInfo.getProductGenericName() + " Activation Request");
        buf.append(LicenseUtil.LINE_END);
        buf.append(LicenseUtil.LINE_END);
        buf.append(getEmailBody());
        Clipboard clipboard = new Clipboard(getShell().getDisplay());
        clipboard.setContents(new Object[] {
            buf.toString()
        }, new Transfer[] {
            TextTransfer.getInstance()
        });
        clipboard.dispose();
    }
}
