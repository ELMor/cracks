// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseMultiActivationWizardPageX.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseRemoteMachine;
import com.instantiations.common.core.LicenseSerialNumber;
import com.swtdesigner.table.TableViewerSorter;
import java.util.Comparator;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

// Referenced classes of package com.instantiations.common.ui:
//            LicenseWizardPageX, LicenseMultiActivationLabelProvider, LicenseMultiActivationContentProvider, LicenseMultiActivationDialog

public class LicenseMultiActivationWizardPageX extends LicenseWizardPageX
{

    private Button _addButton;
    private CheckboxTableViewer _tableViewer;
    private TableColumn _nameColumn;
    private TableColumn _remarkColumn;
    private LicenseSerialNumber _serialNumber;
    private LicenseMultiActivationContentProvider _contentProvider;

    public LicenseMultiActivationWizardPageX()
    {
        super("wizardPage");
        setTitle("Multiple Activations");
        setDescription("Select one or more machines to be activated");
    }

    public void createControl(Composite parent)
    {
        Composite container = new Composite(parent, 0);
        GridLayout gridLayout = new GridLayout();
        gridLayout.marginWidth = 0;
        gridLayout.marginHeight = 0;
        container.setLayout(gridLayout);
        _tableViewer = CheckboxTableViewer.newCheckList(container, 0x10800);
        _tableViewer.addCheckStateListener(new ICheckStateListener() {

            public void checkStateChanged(CheckStateChangedEvent e)
            {
                if(e.getChecked())
                    loadAddress((LicenseRemoteMachine)e.getElement());
            }

        });
        _tableViewer.setLabelProvider(new LicenseMultiActivationLabelProvider());
        _contentProvider = new LicenseMultiActivationContentProvider();
        _tableViewer.setContentProvider(_contentProvider);
        Table table = _tableViewer.getTable();
        table.setHeaderVisible(true);
        table.setLayoutData(new GridData(1808));
        _nameColumn = new TableColumn(table, 0);
        _nameColumn.setWidth(200);
        _nameColumn.setText("Computer Name");
        _remarkColumn = new TableColumn(table, 0);
        _remarkColumn.setWidth(300);
        _remarkColumn.setText("Remark");
        _tableViewer.setSorter(new TableViewerSorter(_tableViewer, new TableColumn[] {
            _nameColumn, _remarkColumn
        }, new Comparator[] {
            new Comparator() {

                public int compare(Object o1, Object o2)
                {
                    LicenseRemoteMachine m1 = (LicenseRemoteMachine)o1;
                    LicenseRemoteMachine m2 = (LicenseRemoteMachine)o2;
                    return m1.getName().compareToIgnoreCase(m2.getName());
                }

            }, new Comparator() {

                public int compare(Object o1, Object o2)
                {
                    LicenseRemoteMachine m1 = (LicenseRemoteMachine)o1;
                    LicenseRemoteMachine m2 = (LicenseRemoteMachine)o2;
                    return m1.getRemark().compareToIgnoreCase(m2.getRemark());
                }

            }
        }));
        setControl(container);
        _addButton = new Button(container, 0);
        _addButton.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                openAddNamesDialog();
            }

        });
        _addButton.setLayoutData(new GridData());
        _addButton.setText("Enter new machine information");
    }

    private void openAddNamesDialog()
    {
        LicenseMultiActivationDialog dialog = new LicenseMultiActivationDialog(getShell());
        if(dialog.open() != 0)
            return;
        LicenseRemoteMachine machines[] = dialog.getMachines();
        _contentProvider.add(machines);
        for(int i = 0; i < machines.length; i++)
            _tableViewer.setChecked(machines[i], true);

    }

    public void setVisible(boolean visible)
    {
        super.setVisible(visible);
        if(visible)
            _tableViewer.getTable().setFocus();
    }

    protected void loadAddress(LicenseRemoteMachine machine)
    {
        machine.loadAddress();
    }

    public LicenseRemoteMachine[] getCheckedMachines()
    {
        Object checkedElements[] = _tableViewer.getCheckedElements();
        LicenseRemoteMachine checkedMachines[] = new LicenseRemoteMachine[checkedElements.length];
        System.arraycopy(((Object) (checkedElements)), 0, checkedMachines, 0, checkedElements.length);
        return checkedMachines;
    }

    public void setMachines(LicenseRemoteMachine machines[])
    {
        _tableViewer.setInput(machines);
    }

    public LicenseSerialNumber getSerialNumber()
    {
        return _serialNumber;
    }

    public void setSerialNumber(LicenseSerialNumber serialNumber)
    {
        _serialNumber = serialNumber;
    }

}
