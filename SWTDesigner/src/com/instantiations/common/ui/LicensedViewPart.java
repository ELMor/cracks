// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicensedViewPart.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseProduct;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.part.ViewPart;

// Referenced classes of package com.instantiations.common.ui:
//            LicensedPart, LicenseEmbeddedParent

public abstract class LicensedViewPart extends ViewPart
    implements LicensedPart
{

    private LicenseEmbeddedParent _mainPanel;

    public LicensedViewPart()
    {
    }

    public final void createPartControl(Composite parent)
    {
        _mainPanel = new LicenseEmbeddedParent(parent, 0, getProducts());
        if(_mainPanel.shouldCreateContent())
            createPartContent(_mainPanel);
    }

    protected abstract LicenseProduct[] getProducts();

    protected abstract void createPartContent(Composite composite);

    public LicenseProduct getLicenseProduct()
    {
        return null;
    }

    public void checkLicenseState()
    {
        if(_mainPanel.updateLicenseState())
        {
            createPartContent(_mainPanel);
            _mainPanel.layout();
            getViewSite().getActionBars().updateActionBars();
        }
    }

    public void showLicenseActivationRequired()
    {
    }
}
