// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   DialogImage.java

package com.instantiations.common.ui;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

public final class DialogImage
{

    public static final DialogImage INFO = new DialogImage("INFO", Display.getCurrent().getSystemImage(2));
    public static final DialogImage WARNING = new DialogImage("WARNING", Display.getCurrent().getSystemImage(8));
    public static final DialogImage ERROR = new DialogImage("ERROR", Display.getCurrent().getSystemImage(1));
    public static final DialogImage QUESTION = new DialogImage("QUESTION", Display.getCurrent().getSystemImage(4));
    private final String printName;
    private Image image;

    private DialogImage(String name, Image image)
    {
        printName = name;
        this.image = image;
    }

    public String toString()
    {
        return "DialogImage." + printName;
    }

    public Image getImage()
    {
        return image;
    }

}
