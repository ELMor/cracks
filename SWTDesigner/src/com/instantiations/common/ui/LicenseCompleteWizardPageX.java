// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseCompleteWizardPageX.java

package com.instantiations.common.ui;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class LicenseCompleteWizardPageX extends WizardPage
{

    private Text _textField;

    public LicenseCompleteWizardPageX()
    {
        super("wizardPage");
        setTitle("Registration Information");
        setDescription("Information concerning the registration and activation process...");
    }

    public void createControl(Composite parent)
    {
        Composite container = new Composite(parent, 0);
        container.setLayout(new GridLayout());
        setControl(container);
        _textField = new Text(container, 2826);
        _textField.setLayoutData(new GridData(1808));
    }

    public void setTransactionMessage(String message)
    {
        _textField.setText(message);
    }
}
