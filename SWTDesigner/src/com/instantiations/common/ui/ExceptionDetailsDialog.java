// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   ExceptionDetailsDialog.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseInfo;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

// Referenced classes of package com.instantiations.common.ui:
//            AbstractDetailsDialog, DialogImage

public class ExceptionDetailsDialog extends AbstractDetailsDialog
{

    private final Object _details;

    public ExceptionDetailsDialog(Shell parentShell, String title, String message, Object details)
    {
        this(parentShell, title, getDialogImage(details), message, details);
    }

    public ExceptionDetailsDialog(Shell parentShell, String title, DialogImage image, String message, Object details)
    {
        this(parentShell, title, image == null ? null : image.getImage(), message, details);
    }

    public ExceptionDetailsDialog(Shell parentShell, String title, Image image, String message, Object details)
    {
        super(parentShell, getTitle(title, details), image, getMessage(message, details));
        _details = details;
    }

    protected Control createDetailsArea(Composite parent)
    {
        Composite panel = new Composite(parent, 0);
        panel.setLayoutData(new GridData(1808));
        GridLayout layout = new GridLayout();
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        panel.setLayout(layout);
        createProductInfoArea(panel);
        createDetailsViewer(panel);
        return panel;
    }

    protected Composite createProductInfoArea(Composite parent)
    {
        Composite composite = new Composite(parent, 0);
        composite.setLayoutData(new GridData());
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.marginWidth = convertHorizontalDLUsToPixels(7);
        composite.setLayout(layout);
        (new Label(composite, 0)).setText("Provider:");
        (new Label(composite, 0)).setText("Instantiations, Inc.");
        (new Label(composite, 0)).setText("Plug-in Name:");
        (new Label(composite, 0)).setText(LicenseInfo.getProductGenericName());
        (new Label(composite, 0)).setText("Plug-in ID:");
        (new Label(composite, 0)).setText(LicenseInfo.getProductIdentifier());
        (new Label(composite, 0)).setText("Version:");
        (new Label(composite, 0)).setText(LicenseInfo.getProductVersionString());
        return composite;
    }

    protected Control createDetailsViewer(Composite parent)
    {
        if(_details == null)
            return null;
        Text text = new Text(parent, 2826);
        text.setLayoutData(new GridData(1808));
        StringWriter writer = new StringWriter(1000);
        if(_details instanceof Throwable)
            appendException(new PrintWriter(writer), (Throwable)_details);
        else
        if(_details instanceof IStatus)
            appendStatus(new PrintWriter(writer), (IStatus)_details, 0);
        text.setText(writer.toString());
        return text;
    }

    public static String getTitle(String title, Object details)
    {
        if(title != null)
            return title;
        if(details instanceof Throwable)
        {
            Throwable e;
            for(e = (Throwable)details; e instanceof InvocationTargetException; e = ((InvocationTargetException)e).getTargetException());
            String name = e.getClass().getName();
            return name.substring(name.lastIndexOf('.') + 1);
        } else
        {
            return "Exception";
        }
    }

    public static DialogImage getDialogImage(Object details)
    {
        if(details instanceof IStatus)
            switch(((IStatus)details).getSeverity())
            {
            case 4: // '\004'
                return DialogImage.ERROR;

            case 2: // '\002'
                return DialogImage.WARNING;

            case 1: // '\001'
                return DialogImage.INFO;

            case 0: // '\0'
                return null;
            }
        return DialogImage.ERROR;
    }

    public static String getMessage(String message, Object details)
    {
        if(details instanceof Throwable)
        {
            Throwable e;
            for(e = (Throwable)details; e instanceof InvocationTargetException; e = ((InvocationTargetException)e).getTargetException());
            if(message == null)
                return e.toString();
            else
                return MessageFormat.format(message, new Object[] {
                    e.toString()
                });
        }
        if(details instanceof IStatus)
        {
            String statusMessage = ((IStatus)details).getMessage();
            if(message == null)
                return statusMessage;
            else
                return MessageFormat.format(message, new Object[] {
                    statusMessage
                });
        }
        if(message != null)
            return message;
        else
            return "An Exception occurred.";
    }

    public static void appendException(PrintWriter writer, Throwable ex)
    {
        if(ex instanceof CoreException)
        {
            appendStatus(writer, ((CoreException)ex).getStatus(), 0);
            writer.println();
        }
        appendStackTrace(writer, ex);
        if(ex instanceof InvocationTargetException)
            appendException(writer, ((InvocationTargetException)ex).getTargetException());
    }

    public static void appendStatus(PrintWriter writer, IStatus status, int nesting)
    {
        for(int i = 0; i < nesting; i++)
            writer.print("  ");

        writer.println(status.getMessage());
        IStatus children[] = status.getChildren();
        for(int i = 0; i < children.length; i++)
            appendStatus(writer, children[i], nesting + 1);

    }

    public static void appendStackTrace(PrintWriter writer, Throwable ex)
    {
        ex.printStackTrace(writer);
    }

    public static void test()
    {
        (new ExceptionDetailsDialog(null, "Test without exception", DialogImage.ERROR, "Test this dialog with a null exception", null)).open();
        (new ExceptionDetailsDialog(null, null, ((Image) (null)), null, null)).open();
        try
        {
            throw new RuntimeException("Test " + com.instantiations.common.ui.ExceptionDetailsDialog.class.getName());
        }
        catch(Exception e)
        {
            (new ExceptionDetailsDialog(null, "Test with exception", DialogImage.ERROR, "Test this dialog with an exception.  The exception is {0}", e)).open();
            (new ExceptionDetailsDialog(null, null, DialogImage.ERROR, null, e)).open();
            try
            {
                throw new InvocationTargetException(e);
            }
            catch(Exception e2)
            {
                (new ExceptionDetailsDialog(null, null, DialogImage.ERROR, null, e2)).open();
            }
            return;
        }
    }

    public static IAction getTestAction()
    {
        return new Action("Test " + com.instantiations.common.ui.ExceptionDetailsDialog.class.getName()) {

            public void run()
            {
                ExceptionDetailsDialog.test();
            }

        };
    }
}
