// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseMessageDialog.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseFile;
import com.instantiations.common.core.LicenseManager;
import com.instantiations.common.core.LicenseProduct;
import com.instantiations.common.core.LicenseUtil;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

// Referenced classes of package com.instantiations.common.ui:
//            LicenseWizardActionX, OpenBrowserAction

public class LicenseMessageDialog extends Dialog
{

    private static final String REMINDER_KEY_SUFFIX = ".reminder";
    private static final long THREE_MONTHS_IN_MILLISECONDS = 0x1cf7c5800L;
    public static final int PURCHASE_BUTTON_ID = 11;
    public static final int ACTIVATE_BUTTON_ID = 10;
    private LicenseProduct _product;
    private String _messageText;
    private String _activateButtonText;
    private static boolean wasDialogShown;

    public LicenseMessageDialog(Shell parentShell, LicenseProduct product, String messageText, String activateButtonText)
    {
        super(parentShell);
        _product = product;
        _messageText = messageText;
        _activateButtonText = activateButtonText;
    }

    public static void openOnStartup(Shell shell)
    {
        openIfNecessary0(shell, LicenseManager.PRODUCTS_IN_VALIDATE_ORDER, true);
    }

    public static boolean openIfNecessary(Shell shell, LicenseProduct products[])
    {
        return openIfNecessary0(shell, products, false);
    }

    public static boolean openIfNecessary0(Shell shell, LicenseProduct products[], boolean isStartup)
    {
        wasDialogShown = false;
        LicenseProduct product = LicenseManager.getProduct(products);
        int daysUntilExpiration = product.getDaysUntilExpiration();
        String messageText;
        String activateButtonText;
        boolean result;
        if(product.isProMode())
        {
            if(daysUntilExpiration > 5 || daysUntilExpiration < -3)
                return true;
            messageText = appendProDaysLeftMessage(new StringBuffer(200), product, daysUntilExpiration).toString();
            activateButtonText = "Activate License...";
            result = true;
        } else
        if(product.isEvalMode())
        {
            if(daysUntilExpiration > 5)
                return true;
            messageText = appendEvalDaysLeftMessage(new StringBuffer(200), product, daysUntilExpiration).toString();
            activateButtonText = "Activate License...";
            result = true;
        } else
        if(product.isFreeMode())
        {
            if(daysUntilExpiration > 5)
                return true;
            messageText = appendFreeDaysLeftMessage(new StringBuffer(200), product, daysUntilExpiration).toString();
            activateButtonText = "Activate " + product.getShortProName() + "...";
            result = true;
        } else
        if(product.wasProMode())
        {
            if(isStartup && !isTimeForReminder(product))
                return false;
            messageText = appendProExpiredMessage(new StringBuffer(200), product).toString();
            activateButtonText = "Activate License...";
            result = false;
        } else
        if(product.wasEvalMode())
        {
            if(isStartup && !isTimeForReminder(product))
                return false;
            messageText = appendEvalExpiredMessage(new StringBuffer(200), product).toString();
            activateButtonText = "Activate License...";
            result = false;
        } else
        {
            if(isStartup && !isTimeForReminder(product))
                return false;
            messageText = appendUnlicensedMessage(new StringBuffer(200), product).toString();
            activateButtonText = "Activate " + product.getShortProName() + "...";
            result = false;
        }
        wasDialogShown = true;
        (new LicenseMessageDialog(shell, product, messageText, activateButtonText)).open();
        return result;
    }

    public static boolean getWasDialogShown()
    {
        return wasDialogShown;
    }

    private static boolean isTimeForReminder(LicenseProduct product)
    {
        LicenseFile licenseFile = product.getLicenseFile();
        String reminderKey = product.getName() + ".reminder";
        long lastTime = licenseFile.getLongValue(reminderKey, 0L);
        long currentTime = System.currentTimeMillis();
        long delta = currentTime - lastTime;
        if(delta > 0x1cf7c5800L)
        {
            licenseFile.putValue(reminderKey, currentTime);
            return true;
        } else
        {
            return false;
        }
    }

    public static boolean embedIfNotActivated(Composite parent, LicenseProduct products[])
    {
        if(LicenseUtil.isActivated(products))
            return true;
        LicenseProduct product = LicenseManager.getProduct(products);
        int daysUntilExpiration = product.getDaysUntilExpiration();
        String messageText = "license message";
        if(product.wasProMode())
            messageText = appendProExpiredMessage(new StringBuffer(200), product).toString();
        else
        if(product.wasEvalMode())
            messageText = appendEvalExpiredMessage(new StringBuffer(200), product).toString();
        else
            messageText = appendUnlicensedMessage(new StringBuffer(200), product).toString();
        Label label = new Label(parent, 0);
        label.setText(messageText);
        label.setLayoutData(new GridData(1808));
        return false;
    }

    protected void configureShell(Shell newShell)
    {
        super.configureShell(newShell);
        newShell.setText(_product.getName() + " License");
    }

    protected Control createDialogArea(Composite parent)
    {
        Composite container = (Composite)super.createDialogArea(parent);
        GridLayout gridLayout = new GridLayout();
        gridLayout.marginWidth = 20;
        gridLayout.marginHeight = 20;
        container.setLayout(gridLayout);
        Label messageLabel = new Label(container, 0);
        messageLabel.setText(_messageText);
        return container;
    }

    protected void createButtonsForButtonBar(Composite parent)
    {
        ((GridLayout)parent.getLayout()).makeColumnsEqualWidth = false;
        createButton(parent, 10, _activateButtonText, false);
        createButton(parent, 11, "Buy It Now!", false);
        final Button okButton = createButton(parent, 0, "Close", true);
        Display.getDefault().asyncExec(new Runnable() {

            public void run()
            {
                okButton.setFocus();
            }

        });
    }

    protected void buttonPressed(int buttonId)
    {
        if(10 == buttonId)
            activatePressed();
        else
        if(11 == buttonId)
            purchasePressed();
        else
            super.buttonPressed(buttonId);
    }

    private void activatePressed()
    {
        Display.getDefault().asyncExec(new Runnable() {

            public void run()
            {
                (new LicenseWizardActionX(getParentShell())).run();
            }

        });
        setReturnCode(10);
        close();
    }

    private void purchasePressed()
    {
        Display.getDefault().asyncExec(new Runnable() {

            public void run()
            {
                (new OpenBrowserAction("BuyItNow", _product.getBuyItNowURL())).run();
            }

        });
        setReturnCode(11);
        close();
    }

    private static StringBuffer appendProDaysLeftMessage(StringBuffer buf, LicenseProduct product, int daysUntilExpiration)
    {
        buf.append("Thank you for using ");
        buf.append(product.getShortProName());
        buf.append("!");
        buf.append(LicenseUtil.LINE_END);
        buf.append(LicenseUtil.LINE_END);
        buf.append("Your support period ");
        if(daysUntilExpiration > 0)
        {
            buf.append("will expire in ");
            buf.append(daysUntilExpiration);
            buf.append(daysUntilExpiration != 1 ? " days" : " day");
        } else
        {
            buf.append("has expired");
        }
        buf.append(".");
        buf.append(LicenseUtil.LINE_END);
        buf.append(LicenseUtil.LINE_END);
        appendPurchaseMessage(buf, product);
        return buf;
    }

    private static StringBuffer appendEvalDaysLeftMessage(StringBuffer buf, LicenseProduct product, int daysUntilExpiration)
    {
        buf.append("Thank you for evaluating ");
        buf.append(product.getName());
        buf.append("!");
        buf.append(LicenseUtil.LINE_END);
        buf.append(LicenseUtil.LINE_END);
        buf.append("The ");
        buf.append(product.getName());
        buf.append(" features");
        buf.append(LicenseUtil.LINE_END);
        buf.append("will no longer be available after ");
        buf.append(daysUntilExpiration);
        buf.append(daysUntilExpiration != 1 ? " days" : " day");
        buf.append(".");
        buf.append(LicenseUtil.LINE_END);
        buf.append(LicenseUtil.LINE_END);
        appendPurchaseMessage(buf, product);
        return buf;
    }

    private static StringBuffer appendFreeDaysLeftMessage(StringBuffer buf, LicenseProduct product, int daysUntilExpiration)
    {
        buf.append("Thank you for using ");
        buf.append(product.getShortName());
        buf.append(" Free Edition!");
        buf.append(LicenseUtil.LINE_END);
        buf.append(LicenseUtil.LINE_END);
        buf.append("To continue using ");
        buf.append(product.getShortName());
        buf.append(LicenseUtil.LINE_END);
        buf.append("you will need to reactivate within ");
        buf.append(daysUntilExpiration);
        buf.append(daysUntilExpiration != 1 ? " days" : " day");
        buf.append(".");
        buf.append(LicenseUtil.LINE_END);
        buf.append(LicenseUtil.LINE_END);
        appendPurchaseMessage(buf, product);
        return buf;
    }

    private static StringBuffer appendProExpiredMessage(StringBuffer buf, LicenseProduct product)
    {
        buf.append("Thank you for using ");
        buf.append(product.getName());
        buf.append("!");
        buf.append(LicenseUtil.LINE_END);
        buf.append(LicenseUtil.LINE_END);
        buf.append("Your ");
        buf.append(product.getShortProName());
        buf.append(" support period has expired.");
        buf.append(LicenseUtil.LINE_END);
        buf.append(LicenseUtil.LINE_END);
        appendPurchaseMessage(buf, product);
        return buf;
    }

    private static StringBuffer appendEvalExpiredMessage(StringBuffer buf, LicenseProduct product)
    {
        buf.append("Thank you for evaluating ");
        buf.append(product.getShortProName());
        buf.append("!");
        buf.append(LicenseUtil.LINE_END);
        buf.append(LicenseUtil.LINE_END);
        buf.append("Your evaluation period has expired.");
        buf.append(LicenseUtil.LINE_END);
        buf.append(LicenseUtil.LINE_END);
        appendPurchaseMessage(buf, product);
        return buf;
    }

    private static StringBuffer appendUnlicensedMessage(StringBuffer buf, LicenseProduct product)
    {
        buf.append("Thank you for your interest in ");
        buf.append(product.getName());
        buf.append("!");
        buf.append(LicenseUtil.LINE_END);
        buf.append(LicenseUtil.LINE_END);
        buf.append("You need to activate ");
        buf.append(product.getName());
        buf.append(" before you can use it.");
        buf.append(LicenseUtil.LINE_END);
        buf.append(LicenseUtil.LINE_END);
        appendPurchaseOrEvaluateMessage(buf, product);
        return buf;
    }

    private StringBuffer appendReminderMessage(StringBuffer buf, LicenseProduct product)
    {
        buf.append("You are using a promotional copy of ");
        buf.append(product.getName());
        buf.append("(Javadoc Repair).");
        buf.append(LicenseUtil.LINE_END);
        buf.append(LicenseUtil.LINE_END);
        appendPurchaseMessage(buf, product);
        return buf;
    }

    private static StringBuffer appendPurchaseMessage(StringBuffer buf, LicenseProduct product)
    {
        buf.append("To purchase ");
        buf.append(product.getName());
        buf.append(":");
        buf.append(LicenseUtil.LINE_END);
        appendContactOptionMessage(buf, product);
        return buf;
    }

    private static StringBuffer appendPurchaseOrEvaluateMessage(StringBuffer buf, LicenseProduct product)
    {
        buf.append("To purchase ");
        buf.append(product.getName());
        buf.append(" or request an evaluation:");
        buf.append(LicenseUtil.LINE_END);
        appendContactOptionMessage(buf, product);
        return buf;
    }

    private static StringBuffer appendContactOptionMessage(StringBuffer buf, LicenseProduct product)
    {
        buf.append("- Send e-mail to ");
        buf.append(product.getSalesEmailAddress());
        buf.append(", or");
        buf.append(LicenseUtil.LINE_END);
        buf.append("- Call 800-808-3737 (in North America), or");
        buf.append(LicenseUtil.LINE_END);
        buf.append("- Browse to ");
        buf.append(product.getInfoURL());
        return buf;
    }


}
