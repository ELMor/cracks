// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseEmbeddedParent.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseProduct;
import com.instantiations.common.core.LicenseUtil;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

// Referenced classes of package com.instantiations.common.ui:
//            LicenseEmbeddedPanel

public class LicenseEmbeddedParent extends Composite
{

    private final LicenseProduct _products[];
    private LicenseEmbeddedPanel _licensePanel;
    private boolean _contentCreated;

    public LicenseEmbeddedParent(Composite parent, int style, LicenseProduct products[])
    {
        super(parent, style);
        _licensePanel = null;
        _contentCreated = false;
        _products = products;
        GridLayout layout = new GridLayout();
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        layout.numColumns = 1;
        setLayout(layout);
        if(shouldShowLicenseState())
        {
            _licensePanel = new LicenseEmbeddedPanel(this, 0);
            _licensePanel.setLayoutData(new GridData(768));
            _licensePanel.updateMessage(products);
        }
    }

    private boolean shouldShowLicenseState()
    {
        return !LicenseUtil.isProMode(_products);
    }

    public boolean shouldCreateContent()
    {
        if(!_contentCreated && LicenseUtil.isActivated(_products))
        {
            _contentCreated = true;
            return true;
        } else
        {
            return false;
        }
    }

    public boolean updateLicenseState()
    {
        if(_licensePanel == null)
            return false;
        if(shouldShowLicenseState())
        {
            _licensePanel.updateMessage(_products);
        } else
        {
            _licensePanel.dispose();
            _licensePanel = null;
            layout();
        }
        return shouldCreateContent();
    }
}
