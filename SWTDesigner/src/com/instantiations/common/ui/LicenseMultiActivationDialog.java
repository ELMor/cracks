// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseMultiActivationDialog.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseHardwareAddress;
import com.instantiations.common.core.LicenseRemoteMachine;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class LicenseMultiActivationDialog extends Dialog
{

    private Text _text;
    private LicenseRemoteMachine _machines[];

    public LicenseMultiActivationDialog(Shell parentShell)
    {
        super(parentShell);
    }

    protected Control createDialogArea(Composite parent)
    {
        Composite container = (Composite)super.createDialogArea(parent);
        container.setLayout(new GridLayout());
        Label label = new Label(container, 0);
        label.setText("Enter a list of machines to be activated.\nEach line must contain a machine name and the MAC address of that machine.\nThe MAC address may be upper or lower case and contain either - or : separators.\nFor example:\n    my.machine.name   00-F3-A7-21-EF-42\n    my.machine.2   00-f3-a7-21-ef-42\n    00:f3:a7:21:ef:42      my.machine.3");
        _text = new Text(container, 2050);
        GridData gridData = new GridData(1808);
        gridData.heightHint = 200;
        _text.setLayoutData(gridData);
        return container;
    }

    protected void createButtonsForButtonBar(Composite parent)
    {
        createButton(parent, 0, IDialogConstants.OK_LABEL, true);
        createButton(parent, 1, IDialogConstants.CANCEL_LABEL, false);
    }

    protected void configureShell(Shell newShell)
    {
        super.configureShell(newShell);
        newShell.setText("Multiple Activations");
    }

    protected void okPressed()
    {
        List machines = new ArrayList();
        LineNumberReader reader = new LineNumberReader(new StringReader(_text.getText()));
        do
        {
            String line;
            try
            {
                line = reader.readLine();
            }
            catch(IOException e)
            {
                break;
            }
            if(line == null)
                break;
            LicenseRemoteMachine each = parseMachine(line);
            if(each != null)
                machines.add(each);
        } while(true);
        _machines = (LicenseRemoteMachine[])machines.toArray(new LicenseRemoteMachine[machines.size()]);
        super.okPressed();
    }

    public static LicenseRemoteMachine parseMachine(String line)
    {
        int length = line.length();
        int start = 0;
        do
        {
            if(start == length)
                return null;
            if(!Character.isWhitespace(line.charAt(start)))
                break;
            start++;
        } while(true);
        int end = start;
        do
        {
            if(end == length)
                return null;
            if(Character.isWhitespace(line.charAt(end)))
                break;
            end++;
        } while(true);
        String part1 = line.substring(start, end);
        String part2 = line.substring(end).trim();
        String name = part1;
        String hwAddrs[] = LicenseHardwareAddress.findMacAddresses(part2);
        if(hwAddrs.length == 0)
        {
            name = part2;
            hwAddrs = LicenseHardwareAddress.findMacAddresses(part1);
            if(hwAddrs.length == 0)
                return null;
        }
        return new LicenseRemoteMachine(name, "", hwAddrs[0]);
    }

    public LicenseRemoteMachine[] getMachines()
    {
        return _machines;
    }
}
