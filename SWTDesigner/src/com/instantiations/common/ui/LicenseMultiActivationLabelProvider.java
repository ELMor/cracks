// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseMultiActivationLabelProvider.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseRemoteMachine;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

class LicenseMultiActivationLabelProvider extends LabelProvider
    implements ITableLabelProvider
{

    LicenseMultiActivationLabelProvider()
    {
    }

    public String getColumnText(Object element, int columnIndex)
    {
        if(element instanceof LicenseRemoteMachine)
        {
            LicenseRemoteMachine machine = (LicenseRemoteMachine)element;
            switch(columnIndex)
            {
            case 0: // '\0'
                return machine.getName();

            case 1: // '\001'
                return machine.getRemark();
            }
        }
        if(element == null)
            return "<null>";
        else
            return element.toString();
    }

    public Image getColumnImage(Object element, int columnIndex)
    {
        return null;
    }
}
