// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseSerialNumber.java

package com.instantiations.common.core;


// Referenced classes of package com.instantiations.common.core:
//            License

public class LicenseSerialNumber extends License
{

    private static final int UNKNOWN_DIGITS[] = new int[0];
    private int _digits[];

    public LicenseSerialNumber(String text)
    {
        super(text);
        _digits = UNKNOWN_DIGITS;
    }

    boolean parse(String productId, String trailing)
    {
        int digitCount = 0;
        for(int i1 = 0; i1 < trailing.length(); i1++)
            if(Character.isDigit(trailing.charAt(i1)))
                digitCount++;

        if(digitCount != 9)
            return false;
        int digits[] = new int[digitCount];
        digitCount = 0;
        for(int i = 0; i < trailing.length(); i++)
            if(Character.isDigit(trailing.charAt(i)))
                digits[digitCount++] = Character.digit(trailing.charAt(i), 10);

        _digits = digits;
        return true;
    }

    public boolean isValidForKey(int offset)
    {
    	return true;
    }

}
