// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseProduct.java

package com.instantiations.common.core;

import com.swtdesigner.DesignerPlugin;
import org.eclipse.core.runtime.PluginVersionIdentifier;

// Referenced classes of package com.instantiations.common.core:
//            LicenseInfo, LicenseFile, LicenseActivationKey, LicenseSerialNumber, 
//            LicenseFileListener, LicenseFileEvent

public class LicenseProduct
{

    private static final PluginVersionIdentifier VERSION_IDENTIFIER = LicenseInfo.getProductVersion();
    public static final String LINE_END = System.getProperty("line.separator");
    private static final int INIT_MODE = -1;
    public static final int NEW_MODE = 0;
    public static final int FREE_MODE = 1;
    public static final int EVAL_MODE = 2;
    public static final int PRO_MODE = 3;
    public static final String INVALID = "invalid";
    public static final String MISSING = "missing";
    private final LicenseProduct _parent;
    private final String _pluginId;
    private final String _name;
    private final String _shortName;
    private final String _fullName;
    private final String _description;
    private final String _supportEmailAddress;
    private final String _infoEmailAddress;
    private final String _salesEmailAddress;
    private final String _buyItNowURL;
    private final String _bannerImageName;
    private final String _proProductIds[];
    private final String _nonCommercialProductIds[];
    private final String _evalProductId;
    private final String _freeProductId;
    private final int _proSerialNumKey;
    private final int _nonCommercialSerialNumKey;
    private final int _listPrice;
    private final String _helpPathName;
    private final LicenseFile _licenseFile;
    private int _mode;
    private boolean _isNonCommercial;
    private boolean _canEvaluate;
    private boolean _canBeFree;
    private LicenseActivationKey _activationKey;
    private LicenseSerialNumber _serialNumber;

    public LicenseProduct(LicenseProduct parent, String pluginId, String name, String shortName, String fullName, String description, String supportEmailAddress, 
            String infoEmailAddress, String salesEmailAddress, String buyItNowURL, String bannerImageName, String proProductIds[], String nonCommercialProductIds[], String evalProductId, 
            String freeProductId, int proSerialNumKey, int nonCommercialSerialNumKey, int listPrice, String helpPathName)
    {
        this(parent, pluginId, name, shortName, fullName, description, supportEmailAddress, infoEmailAddress, salesEmailAddress, buyItNowURL, bannerImageName, proProductIds, nonCommercialProductIds, evalProductId, freeProductId, proSerialNumKey, nonCommercialSerialNumKey, listPrice, helpPathName, LicenseFile.getInstance());
    }

    public LicenseProduct(LicenseProduct parent, String pluginId, String name, String shortName, String fullName, String description, String supportEmailAddress, 
            String infoEmailAddress, String salesEmailAddress, String buyItNowURL, String bannerImageName, String proProductIds[], String nonCommercialProductIds[], String evalProductId, 
            String freeProductId, int proSerialNumKey, int nonCommercialSerialNumKey, int listPrice, String helpPathName, LicenseFile licenseFile)
    {
        _mode = -1;
        _isNonCommercial = false;
        _parent = parent;
        _pluginId = pluginId;
        _name = name;
        _shortName = shortName;
        _fullName = fullName;
        _description = description;
        _supportEmailAddress = supportEmailAddress;
        _infoEmailAddress = infoEmailAddress;
        _salesEmailAddress = salesEmailAddress;
        _buyItNowURL = buyItNowURL;
        _bannerImageName = bannerImageName;
        _proProductIds = proProductIds;
        _nonCommercialProductIds = nonCommercialProductIds;
        _evalProductId = evalProductId;
        _freeProductId = freeProductId;
        _proSerialNumKey = proSerialNumKey;
        _nonCommercialSerialNumKey = nonCommercialSerialNumKey;
        _listPrice = listPrice;
        _helpPathName = helpPathName;
        _licenseFile = licenseFile;
        _licenseFile.addLicenseFileListener(new LicenseFileListener() {

            public void changed(LicenseFileEvent event)
            {
                _mode = -1;
                _activationKey = null;
                _serialNumber = null;
            }

        });
    }

    public LicenseFile getLicenseFile()
    {
        return _licenseFile;
    }

    public LicenseProduct getParent()
    {
        return _parent;
    }

    public String getFeatureId()
    {
        return getPluginId();
    }

    public String getPluginId()
    {
        return _pluginId;
    }

    public String getName()
    {
        return _name;
    }

    public String getShortName()
    {
        return _shortName;
    }

    public String getFullName()
    {
        return _fullName;
    }

    public String getDescription()
    {
        return _description;
    }

    public String[] getProProductIds()
    {
        return _proProductIds;
    }

    public String[] getNonCommercialIds()
    {
        return _nonCommercialProductIds;
    }

    public String getEvalProductId()
    {
        return _evalProductId;
    }

    public String getFreeProductId()
    {
        return _freeProductId;
    }

    public boolean isProProductId(String prodId)
    {
        for(int i = 0; i < _proProductIds.length; i++)
        {
            String each = _proProductIds[i];
            if(each.equalsIgnoreCase(prodId))
                return true;
        }

        for(int i = 0; i < _nonCommercialProductIds.length; i++)
        {
            String each = _nonCommercialProductIds[i];
            if(each.equalsIgnoreCase(prodId))
                return true;
        }

        return false;
    }

    public LicenseActivationKey getPriorActivationKey()
    {
        LicenseActivationKey activationKeys[] = _licenseFile.getActivationKeys();
        for(int i = activationKeys.length; --i >= 0;)
        {
            LicenseActivationKey eachKey = activationKeys[i];
            String prodId = eachKey.getProductId();
            if(isProProductId(prodId))
                return eachKey;
            if(prodId.equalsIgnoreCase(_evalProductId))
                return eachKey;
            if(prodId.equalsIgnoreCase(_freeProductId))
                return eachKey;
        }

        return null;
    }

    public LicenseActivationKey getValidProOrEvalActivationKey()
    {
        LicenseActivationKey activationKey = getValidActivationKey();
        if(activationKey == null)
            return null;
        if(activationKey.getProductId().equals(_freeProductId))
            return null;
        else
            return activationKey;
    }

    public LicenseActivationKey getValidFreeActivationKey()
    {
        return _licenseFile.getValidActivationKey(_freeProductId, VERSION_IDENTIFIER, null);
    }

    public LicenseActivationKey getValidActivationKey()
    {
        cacheState();
        return _activationKey;
    }

    public LicenseSerialNumber getSerialNumber()
    {
        cacheState();
        return _serialNumber;
    }

    public int getDaysUntilExpiration()
    {
        LicenseActivationKey activationKey = getValidActivationKey();
        int daysLeft = activationKey == null ? -1 : activationKey.getDaysUntilExpiration();
        if(_parent != null && _parent.getMode() >= getMode())
        {
            int parentDaysLeft = _parent.getDaysUntilExpiration();
            if(daysLeft < parentDaysLeft)
                daysLeft = parentDaysLeft;
        }
        return daysLeft;
    }

    private void cacheState()
    {
        if(_mode != -1)
            return;
        _serialNumber = null;
        _activationKey = null;
        _isNonCommercial = false;
        LicenseSerialNumber validSerialNumbers[] = _licenseFile.getValidSerialNumbers(_proProductIds, _proSerialNumKey);
        for(int i = 0; i < validSerialNumbers.length; i++)
        {
            LicenseSerialNumber eachSerialNum = validSerialNumbers[i];
            if(_serialNumber == null)
                _serialNumber = eachSerialNum;
            _mode = 3;
            _activationKey = _licenseFile.getValidActivationKey(_proProductIds, VERSION_IDENTIFIER, LicenseInfo.getProductBuildDate());
            if(_activationKey == null)
                continue;
            _serialNumber = eachSerialNum;
            break;
        }

        if(_activationKey == null)
        {
            validSerialNumbers = _licenseFile.getValidSerialNumbers(_nonCommercialProductIds, _nonCommercialSerialNumKey);
            for(int i = 0; i < validSerialNumbers.length; i++)
            {
                LicenseSerialNumber eachSerialNum = validSerialNumbers[i];
                if(_serialNumber == null)
                    _serialNumber = eachSerialNum;
                _mode = 3;
                _activationKey = _licenseFile.getValidActivationKey(_nonCommercialProductIds, VERSION_IDENTIFIER, LicenseInfo.getProductBuildDate());
                if(_activationKey == null)
                    continue;
                _serialNumber = eachSerialNum;
                _isNonCommercial = true;
                break;
            }

        }
        if(_activationKey == null)
        {
            _mode = 2;
            _activationKey = _licenseFile.getValidActivationKey(_evalProductId, VERSION_IDENTIFIER, null);
        }
        if(_activationKey == null)
        {
            _mode = 1;
            _activationKey = _licenseFile.getValidActivationKey(_freeProductId, VERSION_IDENTIFIER, null);
        }
        if(_activationKey == null)
            _mode = 0;
        if(_parent != null)
        {
            int parentMode = _parent.getMode();
            if(_mode != 3)
                if(parentMode == 3)
                    _mode = 3;
                else
                if(_mode != 2)
                    if(parentMode == 2)
                        _mode = 2;
                    else
                    if(_mode != 1 && parentMode == 1)
                        _mode = 1;
        }
        LicenseActivationKey activationKeys[] = _licenseFile.getActivationKeys();
        _canEvaluate = false;
        if(_mode == 0 || _mode == 1)
        {
            _canEvaluate = true;
            for(int i = 0; i < activationKeys.length; i++)
            {
                LicenseActivationKey eachKey = activationKeys[i];
                if(!eachKey.getProductId().equals(_evalProductId) || !eachKey.getVersion().equals(VERSION_IDENTIFIER))
                    continue;
                _canEvaluate = false;
                break;
            }

        }
        _canBeFree = _freeProductId != null;
    }

    public boolean isValid(LicenseActivationKey activationKey)
    {
        return validate(activationKey) == null;
    }

    public boolean wasValid(LicenseActivationKey activationKey)
    {
        String result = validate(activationKey);
        return !"missing".equals(result) && !"invalid".equals(result);
    }

    public String validate(LicenseActivationKey activationKey)
    {
        if(activationKey == null)
            return "missing";
        String prodId = activationKey.getProductId();
        if(isProProductId(prodId))
        {
            if(getSerialNumber() == null)
                return "missing or invalid serial number";
            if(!VERSION_IDENTIFIER.equals(activationKey.getVersion()))
                return "for a different version";
            else
                return null;
        }
        if(prodId.equalsIgnoreCase(_evalProductId))
        {
            if(!VERSION_IDENTIFIER.equals(activationKey.getVersion()))
                return "for a different version";
            if(activationKey.getDaysUntilExpiration() < 0)
                return "expired";
            else
                return null;
        }
        if(prodId.equalsIgnoreCase(_freeProductId))
        {
            if(!VERSION_IDENTIFIER.equals(activationKey.getVersion()))
                return "for a different version";
            if(activationKey.getDaysUntilExpiration() < 0)
                return "expired";
            else
                return null;
        } else
        {
            return "invalid";
        }
    }

    public boolean isValid(LicenseSerialNumber serialNumber)
    {
        if(serialNumber == null)
            return false;
        String prodId = serialNumber.getProductId();
        for(int i = 0; i < _proProductIds.length; i++)
        {
            String each = _proProductIds[i];
            if(each.equalsIgnoreCase(prodId))
                return serialNumber.isValidForKey(_proSerialNumKey);
        }

        for(int i = 0; i < _nonCommercialProductIds.length; i++)
        {
            String each = _nonCommercialProductIds[i];
            if(each.equalsIgnoreCase(prodId))
                return serialNumber.isValidForKey(_nonCommercialSerialNumKey);
        }

        return false;
    }

    public boolean isFreeMode()
    {
        if(DesignerPlugin.testFreeMode())
            return true;
        return getMode() == 1;
    }

    public boolean isEvalMode()
    {
    	return false;
        //return getMode() == 2;
    }

    public boolean isProMode()
    {
        //return getMode() == 3;
    	return true;
    }

    public boolean isNonCommercial()
    {
        cacheState();
        //return _isNonCommercial;
        return false;
    }

    public boolean isProOrEvalMode()
    {
        if(DesignerPlugin.testFreeMode())
            return false;
        else
            return isProOrEvalModePrim();
    }

    public boolean isProOrEvalModePrim()
    {
        int mode = getMode();
        return mode == 3 || mode == 2;
    }

    public boolean isActivated()
    {
        int mode = getMode();
        return mode == 3 || mode == 2 || mode == 1;
    }

    public boolean wasProMode()
    {
        LicenseActivationKey activationKeys[] = _licenseFile.getActivationKeys();
        for(int i = 0; i < activationKeys.length; i++)
        {
            LicenseActivationKey actKey = activationKeys[i];
            String prodId = actKey.getProductId();
            if(isProProductId(prodId))
                return true;
        }

        LicenseSerialNumber serialNumbers[] = _licenseFile.getSerialNumbers();
        for(int i = 0; i < serialNumbers.length; i++)
        {
            LicenseSerialNumber serNum = serialNumbers[i];
            String prodId = serNum.getProductId();
            if(isProProductId(prodId))
                return true;
        }

        return false;
    }

    public boolean wasEvalMode()
    {
        LicenseActivationKey activationKeys[] = _licenseFile.getActivationKeys();
        for(int i = 0; i < activationKeys.length; i++)
        {
            LicenseActivationKey actKey = activationKeys[i];
            String prodId = actKey.getProductId();
            if(prodId.equals(_evalProductId))
                return true;
        }

        return false;
    }

    public boolean wasProOrEvalModeAtLeast10Days()
    {
        LicenseActivationKey activationKeys[] = _licenseFile.getActivationKeys();
        for(int i = 0; i < activationKeys.length; i++)
        {
            LicenseActivationKey actKey = activationKeys[i];
            String prodId = actKey.getProductId();
            if(isProProductId(prodId))
                return true;
            if(prodId.equals(_evalProductId) && actKey.getDaysUntilExpiration() < 4)
                return true;
        }

        return false;
    }

    public boolean canEvaluate()
    {
        cacheState();
        return _canEvaluate;
    }

    public boolean canBeFree()
    {
        cacheState();
        return _canBeFree;
    }

    public int getMode()
    {
        cacheState();
        _mode=3;
        return _mode;
    }

    public String getModeName()
    {
        switch(getMode())
        {
        case 3: // '\003'
            return "Pro";

        case 2: // '\002'
            return "Evaluation";

        case 1: // '\001'
            return "Free";

        case 0: // '\0'
            return "New";
        }
        return "Unknown";
    }

    public void appendLicenseDescription(StringBuffer buf)
    {
        appendLicenseDescription(buf, LINE_END);
    }

    public void appendLicenseDescription(StringBuffer buf, String lineEnd)
    {
        LicenseSerialNumber serialNum = getSerialNumber();
        LicenseActivationKey activationKey = getValidActivationKey();
        if(activationKey == null)
            activationKey = getPriorActivationKey();
        String indent = "    ";
        if(serialNum != null || activationKey != null)
        {
            buf.append(getName());
            buf.append(lineEnd);
        }
        if(serialNum != null)
        {
            buf.append("    ");
            buf.append(serialNum.getText());
            buf.append(lineEnd);
        }
        if(activationKey != null)
        {
            int daysUntilExpiration = activationKey.getDaysUntilExpiration();
            buf.append("    ");
            buf.append(activationKey.getText());
            buf.append(lineEnd);
            switch(getMode())
            {
            default:
                break;

            case 3: // '\003'
                if(daysUntilExpiration < 15)
                {
                    buf.append("    ");
                    buf.append("    ");
                    if(daysUntilExpiration < 1)
                    {
                        buf.append("expired");
                    } else
                    {
                        buf.append(daysUntilExpiration);
                        buf.append(" days left in support period");
                    }
                    buf.append(lineEnd);
                }
                break;

            case 2: // '\002'
                buf.append("    ");
                buf.append("    ");
                if(daysUntilExpiration < 1)
                {
                    buf.append("expired");
                } else
                {
                    buf.append(daysUntilExpiration);
                    buf.append(" days left in the evaluation period");
                }
                buf.append(lineEnd);
                break;

            case 1: // '\001'
                if(daysUntilExpiration >= 15)
                    break;
                buf.append("    ");
                buf.append("    ");
                if(daysUntilExpiration < 1)
                {
                    buf.append("expired");
                } else
                {
                    buf.append(daysUntilExpiration);
                    buf.append(" days left before new activation key is needed");
                }
                buf.append(lineEnd);
                break;

            case 0: // '\0'
                buf.append("    ");
                buf.append("    ");
                if(isProProductId(activationKey.getProductId()))
                {
                    if(serialNum == null)
                    {
                        buf.append("missing serial number");
                    } else
                    {
                        buf.append("for version ");
                        buf.append(activationKey.getVersion());
                        buf.append(" or before and support period has expired");
                    }
                } else
                {
                    buf.append("expired");
                }
                buf.append(lineEnd);
                break;
            }
        }
    }

    public String getBannerImageName()
    {
        return _bannerImageName;
    }

    public String getSupportEmailAddress()
    {
        return _supportEmailAddress;
    }

    public String getInfoEmailAddress()
    {
        return _infoEmailAddress;
    }

    public String getSalesEmailAddress()
    {
        return _salesEmailAddress;
    }

    public String getBuyItNowURL()
    {
        return _buyItNowURL;
    }

    public int getListPrice()
    {
        return _listPrice;
    }

    public boolean removeFreeKeys()
    {
        if(_freeProductId == null)
            return false;
        boolean changed = false;
        LicenseActivationKey activationKeys[] = _licenseFile.getActivationKeys();
        for(int j = 0; j < activationKeys.length; j++)
        {
            LicenseActivationKey eachKey = activationKeys[j];
            if(_freeProductId.equals(eachKey.getProductId()))
            {
                _licenseFile.remove(eachKey);
                changed = true;
            }
        }

        return changed;
    }

    public String getShortProName()
    {
        String shortProName = getShortName();
        if(shortProName.indexOf("Pro") == -1)
            shortProName = shortProName + " Pro";
        return shortProName;
    }

    public String getInfoURL()
    {
        return "http://www.instantiations.com/codepro/ws";
    }

    public String getHelpPathName()
    {
        return _helpPathName;
    }




}
