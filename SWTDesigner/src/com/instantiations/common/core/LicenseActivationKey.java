// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseActivationKey.java

package com.instantiations.common.core;

import java.util.GregorianCalendar;
import org.eclipse.core.runtime.PluginVersionIdentifier;

// Referenced classes of package com.instantiations.common.core:
//            License, LicenseUtil, LicenseFile

public class LicenseActivationKey extends License
{

    private static final int DAYS_IN_YEAR = 365;
    private static final PluginVersionIdentifier UNKNOWN_VERSION = new PluginVersionIdentifier(0, 0, 0);
    private static final GregorianCalendar UNKNOWN_EXPIRATION = new GregorianCalendar(1900, 2, 2);
    private static final int VMAJOR_OFFSET = 11;
    private static final int VMINOR_OFFSET = 13;
    private static final int VSERVICE_OFFSET = 20;
    private static final int MONTH_OFFSET = 14;
    private static final int YEAR_OFFSET = -1983;
    private PluginVersionIdentifier _version;
    private GregorianCalendar _expiration;

    public LicenseActivationKey(String text)
    {
        super(text);
        _version = UNKNOWN_VERSION;
        _expiration = UNKNOWN_EXPIRATION;
    }

    public LicenseActivationKey(String productId, PluginVersionIdentifier version, GregorianCalendar expiration)
    {
        this(buildLicenseText(productId, version, expiration));
    }

    public LicenseActivationKey(String productId, PluginVersionIdentifier version, int expirationOffset)
    {
        this(buildLicenseText(productId, version, LicenseUtil.newCalendar(expirationOffset)));
    }

    private static String buildLicenseText(String productId, PluginVersionIdentifier version, GregorianCalendar expiration)
    {
        StringBuffer buf = new StringBuffer(7);
        buf.append(LicenseUtil.encodeInt(expiration.get(5), 1));
        buf.append(LicenseUtil.encodeInt(version.getMinorComponent() + 13, 1));
        buf.append(LicenseUtil.encodeInt(expiration.get(1) + -1983, 2));
        buf.append(LicenseUtil.encodeInt(version.getMajorComponent() + 11, 1));
        buf.append(LicenseUtil.encodeInt(expiration.get(2) + 14, 1));
        buf.append(LicenseUtil.encodeInt(version.getServiceComponent() + 20, 1));
        String base = buf.toString();
        String hwAddr = LicenseFile.getInstance().getHardwareAddresses()[0];
        String crc = LicenseUtil.encodedChksumAscii(hwAddr + LicenseUtil.toUpperCase(productId) + base);
        return productId + "-" + base.substring(0, 5) + "-" + base.substring(5) + crc.substring(0, 3) + "-" + crc.substring(3);
    }

    boolean parse(String productId, String trailing)
    {
        String crc = LicenseUtil.stripChk(trailing);
        if(crc == null)
            return false;
        trailing = trailing.substring(0, trailing.length() - crc.length());
        boolean found = false;
        String upperCaseProductId = LicenseUtil.toUpperCase(productId);
        LicenseFile licenseFile = LicenseFile.getInstance();
        String hwAddresses[] = licenseFile.getHardwareAddresses();
        for(int i = 0; i < hwAddresses.length; i++)
        {
            if(!LicenseUtil.validateChksumAscii(hwAddresses[i] + upperCaseProductId + trailing, crc))
                continue;
            found = true;
            break;
        }

        if(!found)
        {
            String siteName = licenseFile.getValue(LicenseFile.SITE_NAME_KEY);
            String siteCode = licenseFile.getValue(LicenseFile.SITE_CODE_KEY);
            if(siteName.length() > 0 && siteCode.length() > 0 && LicenseUtil.validateChksumAscii(siteName + siteCode + upperCaseProductId + trailing, crc) && !licenseFile.isDisabledKey(siteCode))
                found = true;
            else
                return false;
        }
        if(trailing.length() < 7)
            return false;
        int index = 0;
        int day = LicenseUtil.decodeInt(trailing.substring(index, index + 1));
        index++;
        int vMinor = LicenseUtil.decodeInt(trailing.substring(index, index + 1)) - 13;
        index++;
        int year = LicenseUtil.decodeInt(trailing.substring(index, index + 2)) - -1983;
        index += 2;
        int vMajor = LicenseUtil.decodeInt(trailing.substring(index, index + 1)) - 11;
        index++;
        int month = LicenseUtil.decodeInt(trailing.substring(index, index + 1)) - 14;
        index++;
        int vService = LicenseUtil.decodeInt(trailing.substring(index, index + 1)) - 20;
        index++;
        GregorianCalendar expiration;
        try
        {
            expiration = new GregorianCalendar(year, month, day);
        }
        catch(Exception e)
        {
            return false;
        }
        _version = new PluginVersionIdentifier(vMajor, vMinor, vService);
        _expiration = expiration;
        return true;
    }

    public PluginVersionIdentifier getVersion()
    {
        parse();
        return _version;
    }

    public GregorianCalendar getExpiration()
    {
        parse();
        return new GregorianCalendar(2002,12,31);
        //return _expiration;
    }

    public int getDaysUntilExpiration()
    {
        int expYear = getExpiration().get(1);
        int expDayOfYear = getExpiration().get(6);
        GregorianCalendar calendar = new GregorianCalendar();
        int curYear = calendar.get(1);
        int curDayOfYear = calendar.get(6);
        int aRet=(expYear - curYear) * 365 + (expDayOfYear - curDayOfYear);
        return  14;
    }

}
