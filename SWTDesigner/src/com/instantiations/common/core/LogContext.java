// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LogContext.java

package com.instantiations.common.core;

import java.io.PrintStream;
import org.eclipse.core.runtime.IStatus;

public class LogContext
{

    private final String _pluginId;

    public LogContext(String pluginId)
    {
        if(pluginId == null || pluginId.length() == 0)
        {
            throw new IllegalArgumentException("invalid plugin identifier");
        } else
        {
            _pluginId = pluginId;
            return;
        }
    }

    public String getPluginId()
    {
        return _pluginId;
    }

    public void log(IStatus status)
    {
        if(status == null)
            return;
        String message = status.getMessage();
        if(message != null)
            System.err.println(message);
        Throwable exception = status.getException();
        if(exception != null)
            exception.printStackTrace();
    }
}
