// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   PluginLogContext.java

package com.instantiations.common.core;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.Bundle;

// Referenced classes of package com.instantiations.common.core:
//            LogContext

public class PluginLogContext extends LogContext
{

    private final Plugin _plugin;

    public PluginLogContext(Plugin plugin)
    {
        super(getPluginID(plugin));
        _plugin = plugin;
    }

    private static String getPluginID(Plugin plugin)
    {
        return plugin.getBundle().getSymbolicName();
    }

    public void log(IStatus status)
    {
        _plugin.getLog().log(status);
    }
}
