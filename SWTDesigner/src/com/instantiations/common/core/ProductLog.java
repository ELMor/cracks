// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   ProductLog.java

package com.instantiations.common.core;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

// Referenced classes of package com.instantiations.common.core:
//            LogContext

public class ProductLog
{

    private static final LogContext DEFAULT_LOG_CONTEXT;
    private static LogContext _logContext;

    private ProductLog()
    {
    }

    static LogContext getLogContext()
    {
        return _logContext;
    }

    public static void setLogContext(LogContext context)
    {
        _logContext = context == null ? DEFAULT_LOG_CONTEXT : context;
    }

    public static Status createStatus(int severity, String message, Throwable exception)
    {
        return new Status(severity, _logContext.getPluginId(), 0, message, exception);
    }

    public static void log(IStatus status)
    {
        _logContext.log(status);
    }

    public static void logInfo(String message)
    {
        log(createStatus(1, message, null));
    }

    public static void logError(String message)
    {
        log(createStatus(4, message, null));
    }

    public static void logError(Throwable exception)
    {
        log(createStatus(4, "Unexpected Exception: " + exception.getMessage(), exception));
    }

    public static void logError(String message, Throwable exception)
    {
        _logContext.log(createStatus(4, message, exception));
    }

    static 
    {
        DEFAULT_LOG_CONTEXT = new LogContext("no-plugin-id");
        _logContext = DEFAULT_LOG_CONTEXT;
    }
}
