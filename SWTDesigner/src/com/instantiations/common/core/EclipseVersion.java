// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   EclipseVersion.java

package com.instantiations.common.core;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.HashMap;
import java.util.Map;

public class EclipseVersion
{

    public static final EclipseVersion UNKNOWN = new EclipseVersion(0, 0, 0, null);
    public static final String UNKNOWN_VERSION_TEXT = "???";
    private static Map buildIdMap;
    private final int vMajor;
    private final int vMinor;
    private final int vService;
    private final String vBuild;
    private final String actualBuild;

    public EclipseVersion(int vMajor, int vMinor, int vService, String vBuild)
    {
        this.vMajor = vMajor;
        this.vMinor = vMinor;
        this.vService = vService;
        actualBuild = vBuild;
        this.vBuild = getMilestone(vBuild);
    }

    public int getMajor()
    {
        return vMajor;
    }

    public int getMinor()
    {
        return vMinor;
    }

    public int getService()
    {
        return vService;
    }

    public String getBuild()
    {
        return vBuild;
    }

    public String getActualBuild()
    {
        return actualBuild;
    }

    public int hashCode()
    {
        return vMajor * 10000 + vMinor * 1000 + vService * 100 + (vBuild != null ? vBuild.hashCode() % 100 : 0);
    }

    public String getInstallKey()
    {
        switch(vMajor)
        {
        default:
            break;

        case 2: // '\002'
            if(vMinor == 0)
                return "2.0";
            if(vMinor == 1)
                return "2.1";
            break;

        case 3: // '\003'
            if(vMinor == 0)
                return "3.0";
            if(vMinor == 1)
                return "3.1";
            if(vMinor == 2)
                return "3.1";
            break;
        }
        return "???";
    }

    public boolean isCompatible(EclipseVersion version)
    {
        if(vMajor != version.vMajor || vMinor != version.vMinor)
            return false;
        if(vService > 0 || vBuild == null)
            return version.vService > 0 || version.vBuild == null;
        return vService == version.vService && (vBuild != null ? vBuild.equals(version.vBuild) : version.vBuild == null);
    }

    public String toString()
    {
        if(vMajor == 0)
            return "???";
        StringBuffer buf = new StringBuffer(10);
        buf.append(vMajor);
        buf.append(".");
        buf.append(vMinor);
        buf.append(".");
        buf.append(vService);
        if(vService == 0 && vBuild != null)
        {
            buf.append(" ");
            buf.append(vBuild);
        }
        return buf.toString();
    }

    public static String getMilestone(String buildId)
    {
        if(buildId == null)
            return null;
        if(buildIdMap == null)
        {
            buildIdMap = new HashMap();
            buildIdMap.put("200312182000", "M6");
            buildIdMap.put("200402122000", "M7");
            buildIdMap.put("200403261517", "M8");
            buildIdMap.put("200405211200", "M9");
            buildIdMap.put("200405290105", "RC1");
            buildIdMap.put("200406111814", "RC2");
            buildIdMap.put("200406192000", "RC3");
            buildIdMap.put("200406251208", "");
            buildIdMap.put("200409161125", "");
            buildIdMap.put("200503110845", "");
            buildIdMap.put("200408122000", "M1");
            buildIdMap.put("200409240800", "M2");
            buildIdMap.put("200411050810", "M3");
            buildIdMap.put("200412162000", "M4");
            buildIdMap.put("I20050218-1600", "M5");
            buildIdMap.put("200502181600", "M5");
            buildIdMap.put("I20050219-1500", "M5a");
            buildIdMap.put("200502191500", "M5a");
            buildIdMap.put("I20050401-1645", "M6");
            buildIdMap.put("200504011645", "M6");
            buildIdMap.put("I20050513-1415", "");
            buildIdMap.put("200505131415", "");
            buildIdMap.put("I20050527-1300", "");
            buildIdMap.put("200505271300", "");
            buildIdMap.put("I20050610-1757", "");
            buildIdMap.put("200506101757", "");
            buildIdMap.put("I20050617-1618", "");
            buildIdMap.put("200506171618", "");
            buildIdMap.put("I20050624-1300", "");
            buildIdMap.put("200506241300", "");
            buildIdMap.put("I20050627-1435", "");
            buildIdMap.put("200506271435", "");
            buildIdMap.put("M20050929-0840", "");
            buildIdMap.put("200509290840", "");
            buildIdMap.put("I20050811-1530", "");
            buildIdMap.put("200508111530", "");
            buildIdMap.put("I20050923-1000", "");
            buildIdMap.put("200509231000", "");
            buildIdMap.put("I20051102-1600", "");
            buildIdMap.put("200511021600", "");
        }
        String milestone = (String)buildIdMap.get(buildId);
        if(milestone == null)
            return buildId;
        if(milestone.length() == 0)
            return null;
        else
            return milestone;
    }

    public static String readBuildId(InputStream stream)
        throws IOException
    {
        LineNumberReader reader = new LineNumberReader(new InputStreamReader(stream));
        String line;
        do
        {
            line = reader.readLine();
            if(line == null)
                return "???";
        } while(!line.startsWith("0="));
        return line.substring(2).trim();
    }

}
