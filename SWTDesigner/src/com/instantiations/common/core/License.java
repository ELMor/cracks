// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   License.java

package com.instantiations.common.core;


// Referenced classes of package com.instantiations.common.core:
//            LicenseUtil

public abstract class License
{

    private static final String UNKNOWN_LICENSE_FORMAT = LicenseUtil.decode("CU2TXRYPXM3JSGBBMBEA6866D4G200VVYV4U5SYQHM1K");
    private final String _text;
    private String _productId;

    License(String text)
    {
        _productId = null;
        _text = text;
    }

    boolean parse()
    {
        if(_productId != null)
            return _productId != UNKNOWN_LICENSE_FORMAT;
        _productId = UNKNOWN_LICENSE_FORMAT;
        int index = _text.indexOf('-');
        if(index == -1)
            return false;
        String prodId = _text.substring(0, index);
        String trailing = LicenseUtil.stripNonEncoded(_text.substring(index + 1));
        if(!parse(prodId, trailing))
        {
            return false;
        } else
        {
            _productId = prodId;
            return true;
        }
    }

    abstract boolean parse(String s, String s1);

    public String getText()
    {
        return _text;
    }

    public String getProductId()
    {
        parse();
        return _productId;
    }

    public boolean equals(Object obj)
    {
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if(!getClass().equals(obj.getClass()))
            return false;
        else
            return getText().equals(((License)obj).getText());
    }

    public int hashCode()
    {
        return getText().hashCode();
    }

    public String toString()
    {
        String name = getClass().getName();
        name = name.substring(name.lastIndexOf('.') + 1);
        return name + "[" + getText() + "]";
    }

}
