// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   SerialNumber.java

package com.elm;


public class crkOtrix
{

    public static final int RADIX = 36;
    public static final int SERIAL_LENGTH = 24;
    private String serialNumber;

    public static void main(String args[]){
    	crkOtrix co=new crkOtrix(0);
    	System.out.println(co.serialNumber);
    	System.out.println(co.isValid());
    	System.out.println(co.isValid("webstudio"));
    }
    
    
    public crkOtrix(int prodId){
    	String []prods={"webstudio","webtree"};
    	String padder="000000000000";
    	String toconvert=prods[prodId]+padder.substring(0,12-prods[prodId].length());
    	long left =Long.parseLong("elmelmelmelm",36);
    	long prod=Long.parseLong(toconvert,36);
    	long right=left ^ prod;
    	String val=Long.toString(left,36)+Long.toString(right,36);
    	
    	
        long sum = 0L;
        for(int i = val.length()-1; i >= 0; i--)
        {
            int digit = Integer.parseInt(val.substring(i, i + 1), 36);
            int weight = val.length() - i ;
            sum += digit * (1+weight);
        }
        val += Long.toString(36-(sum%36),36);
        serialNumber=val;
    }
    
    public crkOtrix(String serialNumber)
    {
        this.serialNumber = null;
        this.serialNumber = serialNumber;
    }

    public String getSerial()
    {
        return serialNumber;
    }

    public boolean isValid()
    {
        boolean result = false;
        if(serialNumber != null && serialNumber.length() == 25)
        {
            long sum = 0L;
            for(int i = serialNumber.length() - 1; i >= 0; i--)
            {
                int digit = Integer.parseInt(serialNumber.substring(i, i + 1), 36);
                int weight = serialNumber.length() - i;
                sum += digit * weight;
            }

            result = sum % 36L == 0L;
        }
        return result;
    }

    public boolean isValid(String productName)
    {
        boolean result = false;
        if(productName != null && serialNumber != null && isValid())
        {
            String productNameInSerial = getProductName();
            result = productName.equalsIgnoreCase(productNameInSerial);
        }
        return result;
    }

    public String getProductName()
    {
        String result = null;
        if(isValid())
        {
            String tmp1 = serialNumber.substring(0, 12);
            String tmp2 = serialNumber.substring(12, serialNumber.length() - 1);
            if(tmp1 != null && tmp2 != null)
            {
                long serial1 = Long.parseLong(tmp1, 36);
                long serial2 = Long.parseLong(tmp2, 36);
                long xor = serial1 ^ serial2;
                result = Long.toString(xor, 36);
                int endofproductname = result.indexOf('0');
                if(endofproductname >= 0)
                    result = result.substring(0, endofproductname);
            }
        }
        return result;
    }
}
