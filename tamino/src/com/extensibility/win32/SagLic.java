// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   SagLic.java

package com.extensibility.win32;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class SagLic
{

    public static final int LIC_OK = 0;
    public static final int LIC_FILENOTFOUND = 1;
    public static final int LIC_NOKEY = 2;
    public static final int LIC_KEYINVALID = 3;
    public static final int LIC_PARAMNOTFOUND = 4;
    public static final int LIC_COMPONENTNOTFOUND = 5;
    public static final int LIC_MISSINGPARAMS = 6;
    public static final int LIC_NOSOCKETFOUND = 7;
    public static final int LIC_SOCKETWRONGVER = 8;
    public static final int LIC_SOCKETERROR = 9;
    public static final int LIC_HOSTNOTFOUND = 10;
    public static final int LIC_ERRORHOSTOPEN = 11;
    public static final int LIC_HTTPERROR = 12;
    public static final int LIC_EXTERNALBUILD = 13;
    public static final int LIC_ALLOCERROR = 14;
    public static final int LIC_EXPIRED = 15;
    public static final int LIC_INVALIDOS = 16;
    public static final int LIC_REGCERROR = 17;
    public static final int LIC_EXTENDED = 18;
    public static final int LIC_SYSTEMERROR = 19;
    public static final int LIC_FILEREADERROR = 20;
    public static final int LIC_90DAYSCHECKNOTVALID = 21;
    public static final int LIC_CONVERSIONERROR = 22;
    public static final int LIC_SECURITYERROR = 23;
    public static final int LIC_FILEWRITEERROR = 24;
    private static final String LIC_MESSAGES[] = {
        Messages.getString("SagLic.LICENSE_OK_1"), Messages.getString("SagLic.ERROR_-_LICENSE_KEY_FILE_NOT_FOUND_2"), Messages.getString("SagLic.ERROR_-_INVALID_(TAMPERED)_LICENSE_KEY_3"), Messages.getString("SagLic.ERROR_-_INVALID_(TAMPERED)_LICENSE_KEY_4"), Messages.getString("SagLic.ERROR_-_PARAMETER_NOT_FOUND_IN_LICENSE_KEY_5"), Messages.getString("SagLic.ERROR_-_COMPONENT_NOT_FOUND_IN_LICENSE_KEY_6"), Messages.getString("SagLic.ERROR_-_MISSING_PARAMETER(S)_-_SEE_FUNCTION_USAGE_7"), Messages.getString("SagLic.ERROR_-_MISSING_WINSOCK_DLL_ON_WIN32_8"), Messages.getString("SagLic.ERROR_-_WRONG_WINSOCK_DLL_(BELOW_1.0___)_ON_WIN32_9"), Messages.getString("SagLic.ERROR_-_SOCKET_CANNOT_BE_OPENED_10"),  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$
        Messages.getString("SagLic.ERROR_-_HOST_CANNOT_BE_FOUND_IN_HTTP_ADDRESS_11"), Messages.getString("SagLic.ERROR_-_HOST_CANNOT_BE_OPENED_IN_HTTP_ADDRESS_12"), Messages.getString("SagLic.ERROR_-_HOST_CANNOT_BE_OPENED_IN_HTTP_ADDRESS_13"), Messages.getString("SagLic.ERROR_-_CANNOT_CREATE_LICENSE_USING_EXTERNAL_LIC_VERSION_14"), Messages.getString("SagLic.ERROR_-_CANNOT_ALLOCATE_MEMORY_15"), Messages.getString("SagLic.ERROR_-_LICENSE_EXPIRED_16"), Messages.getString("SagLic.ERROR_-_INVALID_LICENSE_OS_17"), Messages.getString("SagLic.ERROR_-_CANNOT_CREATE_FIRST_WINDOWS_REGISTRY_KEY___software_ag___18"), Messages.getString("SagLic.ERROR_-_CANNOT_ACCESS_WINDOWS_REGISTRY_19"), Messages.getString("SagLic.ERROR_-_SYSTEM_COMMAND_(I.E.,_UNAME)_20"),  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$
        Messages.getString("SagLic.ERROR_-_CANNOT_READ_LICENSE_KEY_FILE_21"), Messages.getString("SagLic.ERROR_-_90_DAYS_CHECK_NOT_VALID_FOR_XML_CONTENTS_PASSED_22"), Messages.getString("SagLic.ERROR_-_CONVERTING_ASCII-TO-EBCDIC_(OR_VICE-VERSA)_23"), Messages.getString("SagLic.ERROR_-_CANNOT_READ_SAG_SECURITY_PROFILE_24"), Messages.getString("SagLic.ERROR_-_CANNOT_WRITE_LICENSE_KEY_FILE_25") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
    };
    protected static boolean noLibrary = false;

    public SagLic()
    {
        try
        {
            System.loadLibrary(Messages.getString("SagLic.saglicJNIWrapper_26")); //$NON-NLS-1$
        }
        catch(Throwable throwable)
        {
            System.out.println(Messages.getString("SagLic._nPublic___27") + throwable.getMessage()); //$NON-NLS-1$
            noLibrary = true;
        }
    }

    public SagLic(String s)
    {
        try
        {
            if(s.equalsIgnoreCase(Messages.getString("SagLic.private_28"))) //$NON-NLS-1$
                System.loadLibrary(Messages.getString("SagLic.saglicJNIWrapperPrv_29")); //$NON-NLS-1$
            else
                System.loadLibrary(Messages.getString("SagLic.saglicJNIWrapper_30")); //$NON-NLS-1$
        }
        catch(Throwable throwable)
        {
            System.out.println(Messages.getString("SagLic._n_31") + s + Messages.getString("SagLic.___32") + throwable.getMessage()); //$NON-NLS-1$ //$NON-NLS-2$
            noLibrary = true;
        }
    }

    public static boolean libraryFound()
    {
        return !noLibrary;
    }

    public native String GetProductInfo();

    public native String WrapLICgenSignature(String s);

    public native int WrapLICcheckSignature(String s);

    public native String WrapLICreadParameter(String s, String s1, String s2);

    public native int WrapLICcheckExpiration(String s, String s1);

    public native int WrapLICcheckOS(String s, String s1);

    public static void printLICStatus(int i)
    {
        String s = Messages.getString("SagLic._33"); //$NON-NLS-1$
        try
        {
            ResourceBundle resourcebundle = ResourceBundle.getBundle(Messages.getString("SagLic.com.softwareag.common.lic.SagLicErrors_34")); //$NON-NLS-1$
            s = resourcebundle.getString(Integer.toString(i));
        }
        catch(MissingResourceException missingresourceexception)
        {
            System.out.println(Messages.getString("SagLic._nERROR____35") + missingresourceexception.getMessage()); //$NON-NLS-1$
            if(i >= 0 && i < LIC_MESSAGES.length)
                s = LIC_MESSAGES[i];
            else
                s = Messages.getString("SagLic.ERROR___LIC_ERROR_CODE_RETURNED____36") + i; //$NON-NLS-1$
        }
        System.out.println(Messages.getString("SagLic._n_37") + i + Messages.getString("SagLic.____38") + s); //$NON-NLS-1$ //$NON-NLS-2$
    }

}
