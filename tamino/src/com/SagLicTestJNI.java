// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   SagLicTestJNI.java

package com;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.softwareag.common.lic.SagLic;
import com.softwareag.common.lic.SagVersion;

public class SagLicTestJNI
{

    public static final String VERSION = Messages.getString("SagLicTestJNI.1.4.0.0_1"); //$NON-NLS-1$
    public static final String COPYRIGHT = Messages.getString("SagLicTestJNI.Copyright_Software_AG_1995-2001._All_rights_reserved._2"); //$NON-NLS-1$

    public SagLicTestJNI()
    {
    }

    public static void main(String args[])
    {
        String s = Messages.getString("SagLicTestJNI._nUSAGE__n_n____java_com.SagLicTestJNI_1_<xmlfile_in>_<component>_<parameter>_n___________WrapLICread_parameter()_called_with_the_last_3_parameters_n_n____java_com.SagLicTestJNI_2_<xmlfile_in>_<xmlfile_out>_n___________WrapLICgenSignature()_called_with_the_last_2_parameters_n___________(WrapLICgenSignature()_is_ONLY_AVAILABLE_in_the_PRIVATE_build.)_n_n____java_com.SagLicTestJNI_3_<xmlfile_in>_n___________WrapLICcheckSignature()_called_with_the_last_parameter_n_n____java_com.SagLicTestJNI_4_<xmlfile_in>_<component>_n___________WrapLICcheckExpiration()_called_with_the_last_2_parameters_n_n____java_com.SagLicTestJNI_5_<xmlfile_in>_<component>_n___________WrapLICcheckOS()_called_with_the_last_2_parameters_n_nFor_example__n_n____java_com.SagLicTestJNI_5_ino223.xml_TaminoProduct_n_3"); //$NON-NLS-1$
        try
        {
            SagVersion sagversion = new SagVersion(Messages.getString("SagLicTestJNI.Java_Package_4")); //$NON-NLS-1$
            SagLic saglic = new SagLic();
            if(args.length < 1)
            {
                System.out.println(Messages.getString("SagLicTestJNI._n_5") + SagVersion.getVersionString()); //$NON-NLS-1$
                System.out.println(s);
                System.exit(0);
            }
            String s1 = Messages.getString("SagLicTestJNI._6"); //$NON-NLS-1$
            String s2 = Messages.getString("SagLicTestJNI._7"); //$NON-NLS-1$
            String s3 = Messages.getString("SagLicTestJNI._8"); //$NON-NLS-1$
            String s4 = Messages.getString("SagLicTestJNI._9"); //$NON-NLS-1$
            String s5 = Messages.getString("SagLicTestJNI._10"); //$NON-NLS-1$
            switch(args.length)
            {
            case 4: // '\004'
                s4 = args[3];
                // fall through

            case 3: // '\003'
                s3 = args[2];
                s2 = args[2];
                // fall through

            case 2: // '\002'
                s1 = args[1];
                // fall through

            case 1: // '\001'
                s5 = args[0];
                break;

            default:
                System.out.println(s);
                break;
            }
            File file = new File(s1);
            byte byte0 = -1;
            if(s5.equals(Messages.getString("SagLicTestJNI.1_11"))) //$NON-NLS-1$
            {
                System.out.println(Messages.getString("SagLicTestJNI._n___WrapLICreadParameter()_n_n___License_File_(In)____12") + file.getName() + Messages.getString("SagLicTestJNI._n___Component_Name_______13") + s3 + Messages.getString("SagLicTestJNI._n___Parameter_Name_______14") + s4); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                String s6 = saglic.WrapLICreadParameter(s1, s3, s4);
                if(s6.length() > 0)
                {
                    if(s6.indexOf(Messages.getString("SagLicTestJNI.____15")) == 0) //$NON-NLS-1$
                        System.out.println(Messages.getString("SagLicTestJNI._n_16") + s6); //$NON-NLS-1$
                    else
                        System.out.println(Messages.getString("SagLicTestJNI._n___Return____17") + s6); //$NON-NLS-1$
                } else
                {
                    System.out.println(s);
                }
            } else
            if(s5.equals(Messages.getString("SagLicTestJNI.2_18")) || s5.equals(Messages.getString("SagLicTestJNI.3_19"))) //$NON-NLS-1$ //$NON-NLS-2$
            {
                FileReader filereader = new FileReader(s1);
                BufferedReader bufferedreader = new BufferedReader(filereader);
                String s7 = Messages.getString("SagLicTestJNI._20"); //$NON-NLS-1$
                for(boolean flag = false; !flag;)
                {
                    String s8 = bufferedreader.readLine();
                    if(s8 == null)
                        flag = true;
                    else
                        s7 = s7 + s8 + Messages.getString("SagLicTestJNI._n_21"); //$NON-NLS-1$
                }

                bufferedreader.close();
                if(s5.equals(Messages.getString("SagLicTestJNI.2_22"))) //$NON-NLS-1$
                {
                    File file1 = new File(s2);
                    System.out.println(Messages.getString("SagLicTestJNI._n___WrapLICgenSignature()_n_n___License_File_(In)_____23") + file.getName() + Messages.getString("SagLicTestJNI._n___License_File_(Out)____24") + file1.getName()); //$NON-NLS-1$ //$NON-NLS-2$
                    String s9 = saglic.WrapLICgenSignature(s7);
                    if(s9.length() > 0)
                    {
                        FileWriter filewriter = new FileWriter(s2);
                        BufferedWriter bufferedwriter = new BufferedWriter(filewriter);
                        bufferedwriter.write(s9, 0, s9.length());
                        bufferedwriter.close();
                        System.out.println(Messages.getString("SagLicTestJNI._n_25") + file1.getName() + Messages.getString("SagLicTestJNI._has_been_signed_and_written_out._26")); //$NON-NLS-1$ //$NON-NLS-2$
                    } else
                    {
                        SagLic.printLICStatus(13);
                        System.out.println(s);
                    }
                } else
                if(s5.equals(Messages.getString("SagLicTestJNI.3_27"))) //$NON-NLS-1$
                {
                    System.out.println(Messages.getString("SagLicTestJNI._n___WrapLICcheckSignature()_n_n___License_File_(In)_____28") + file.getName()); //$NON-NLS-1$
                    int i = saglic.WrapLICcheckSignature(s7);
                    SagLic.printLICStatus(i);
                }
            } else
            if(s5.equals(Messages.getString("SagLicTestJNI.4_29"))) //$NON-NLS-1$
            {
                System.out.println(Messages.getString("SagLicTestJNI._n___WrapLICcheckExpiration()_n_n___License_File_(In)____30") + file.getName() + Messages.getString("SagLicTestJNI._n___Component_Name_______31") + s3); //$NON-NLS-1$ //$NON-NLS-2$
                int j = saglic.WrapLICcheckExpiration(s1, s3);
                SagLic.printLICStatus(j);
                if(j == 6)
                    System.out.println(s);
            } else
            if(s5.equals(Messages.getString("SagLicTestJNI.5_32"))) //$NON-NLS-1$
            {
                System.out.println(Messages.getString("SagLicTestJNI._n___WrapLICcheckOS()_n_n___License_File_(In)____33") + file.getName() + Messages.getString("SagLicTestJNI._n___Component_Name_______34") + s3); //$NON-NLS-1$ //$NON-NLS-2$
                int k = saglic.WrapLICcheckOS(s1, s3);
                SagLic.printLICStatus(k);
                if(k == 6)
                    System.out.println(s);
            } else
            {
                System.out.println(s);
            }
        }
        catch(FileNotFoundException filenotfoundexception)
        {
            System.out.println(Messages.getString("SagLicTestJNI._njava_com.SagLicTestJNI___Error_-_File_NOT_Found_-__35") + filenotfoundexception.getMessage()); //$NON-NLS-1$
            System.out.println(s);
        }
        catch(IOException ioexception)
        {
            System.out.println(Messages.getString("SagLicTestJNI._njava_com.SagLicTestJNI___Error_-_I/O_-__36") + ioexception.getMessage()); //$NON-NLS-1$
            System.out.println(s);
        }
        catch(Exception exception)
        {
            System.out.println(Messages.getString("SagLicTestJNI._njava_com.SagLicTestJNI___Error_-__37") + exception.getMessage()); //$NON-NLS-1$
            System.out.println(s);
        }
    }
}
