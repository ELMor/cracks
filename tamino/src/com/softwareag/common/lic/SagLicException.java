// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   SagLicException.java

package com.softwareag.common.lic;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class SagLicException extends Exception
{

    public static final String VERSION = Messages.getString("SagLicException.1.4.0.0_1"); //$NON-NLS-1$
    public static final String COPYRIGHT = Messages.getString("SagLicException.Copyright_Software_AG_1995-2001._All_rights_reserved._2"); //$NON-NLS-1$
    public static final int LIC_OK = 0;
    public static final int LIC_FILENOTFOUND = 1;
    public static final int LIC_NOKEY = 2;
    public static final int LIC_KEYINVALID = 3;
    public static final int LIC_PARAMNOTFOUND = 4;
    public static final int LIC_COMPONENTNOTFOUND = 5;
    public static final int LIC_MISSINGPARAMS = 6;
    public static final int LIC_NOSOCKETFOUND = 7;
    public static final int LIC_SOCKETWRONGVER = 8;
    public static final int LIC_SOCKETERROR = 9;
    public static final int LIC_HOSTNOTFOUND = 10;
    public static final int LIC_ERRORHOSTOPEN = 11;
    public static final int LIC_HTTPERROR = 12;
    public static final int LIC_EXTERNALBUILD = 13;
    public static final int LIC_ALLOCERROR = 14;
    public static final int LIC_EXPIRED = 15;
    public static final int LIC_INVALIDOS = 16;
    public static final int LIC_REGCERROR = 17;
    public static final int LIC_EXTENDED = 18;
    public static final int LIC_FILEREADERROR = 19;
    public static final int LIC_90DAYSCHECKNOTVALID = 20;
    public static final int LIC_CONVERSIONERROR = 21;
    public static final int LIC_SYSTEMERROR = 22;
    public static final int LIC_SECURITYERROR = 23;
    public static final int LIC_FILEWRITEERROR = 24;
    private static final String LIC_MESSAGES[] = {
        Messages.getString("SagLicException.LICENSE_OK_3"), Messages.getString("SagLicException.ERROR_-_LICENSE_KEY_FILE_NOT_FOUND_4"), Messages.getString("SagLicException.ERROR_-_INVALID_(TAMPERED)_LICENSE_KEY_5"), Messages.getString("SagLicException.ERROR_-_INVALID_(TAMPERED)_LICENSE_KEY_6"), Messages.getString("SagLicException.ERROR_-_PARAMETER_NOT_FOUND_IN_LICENSE_KEY_7"), Messages.getString("SagLicException.ERROR_-_COMPONENT_NOT_FOUND_IN_LICENSE_KEY_8"), Messages.getString("SagLicException.ERROR_-_MISSING_PARAMETER(S)_-_SEE_FUNCTION_USAGE_9"), Messages.getString("SagLicException.ERROR_-_MISSING_WINSOCK_DLL_ON_WIN32_10"), Messages.getString("SagLicException.ERROR_-_WRONG_WINSOCK_DLL_(BELOW_1.0___)_ON_WIN32_11"), Messages.getString("SagLicException.ERROR_-_SOCKET_CANNOT_BE_OPENED_12"),  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$
        Messages.getString("SagLicException.ERROR_-_HOST_CANNOT_BE_FOUND_IN_HTTP_ADDRESS_13"), Messages.getString("SagLicException.ERROR_-_HOST_CANNOT_BE_OPENED_IN_HTTP_ADDRESS_14"), Messages.getString("SagLicException.ERROR_-_HOST_CANNOT_BE_OPENED_IN_HTTP_ADDRESS_15"), Messages.getString("SagLicException.ERROR_-_CANNOT_CREATE_LICENSE_USING_EXTERNAL_LIC_VERSION_16"), Messages.getString("SagLicException.ERROR_-_CANNOT_ALLOCATE_MEMORY_17"), Messages.getString("SagLicException.ERROR_-_LICENSE_EXPIRED_18"), Messages.getString("SagLicException.ERROR_-_INVALID_LICENSE_OS_19"), Messages.getString("SagLicException.ERROR_-_CANNOT_CREATE_FIRST_WINDOWS_REGISTRY_KEY___software_ag___20"), Messages.getString("SagLicException.ERROR_-_CANNOT_ACCESS_WINDOWS_REGISTRY_21"), Messages.getString("SagLicException.ERROR_-_READING_LICENSE_KEY_FILE_22"),  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$
        Messages.getString("SagLicException.ERROR_-_TRIAL_LICENSE_KEY_FILE_must_NOT_be_WRITE-PROTECTED_23"), Messages.getString("SagLicException.ERROR_-_CONVERTING_ASCII-TO-EBCDIC_(OR_VICE-VERSA)_24"), Messages.getString("SagLicException.ERROR_-_SYSTEM_COMMAND_(I.E.,_UNAME)_25"), Messages.getString("SagLicException.ERROR_-_CANNOT_READ_LOCAL_SAG_SECURITY_PROFILE_26"), Messages.getString("SagLicException.ERROR_-_WRITING_to_LICENSE_KEY_FILE_27") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
    };
    private static final String resourceBundleClass = Messages.getString("SagLicException.com.softwareag.common.messages.SagLicErrors_28"); //$NON-NLS-1$
    private static int saglicErrorCode;

    public SagLicException(int i)
    {
        super(setMessage(i, Messages.getString("SagLicException._29"))); //$NON-NLS-1$
        saglicErrorCode = i;
    }

    public SagLicException(int i, String s)
    {
        super(setMessage(i, s));
        saglicErrorCode = i;
    }

    public int getErrorCode()
    {
        return saglicErrorCode;
    }

    private static String setMessage(int i, String s)
    {
        String s1 = s;
        try
        {
            ResourceBundle resourcebundle = ResourceBundle.getBundle(Messages.getString("SagLicException.com.softwareag.common.messages.SagLicErrors_30")); //$NON-NLS-1$
            if(s.length() > 0)
                s1 = resourcebundle.getString(Integer.toString(i)) + Messages.getString("SagLicException._-__31") + s; //$NON-NLS-1$
            else
                s1 = resourcebundle.getString(Integer.toString(i));
        }
        catch(MissingResourceException missingresourceexception)
        {
            System.out.println(Messages.getString("SagLicException._nERROR____32") + missingresourceexception.getMessage()); //$NON-NLS-1$
            if(i >= 0 && i < LIC_MESSAGES.length)
                s1 = LIC_MESSAGES[i];
            else
                s1 = Messages.getString("SagLicException.ERROR___LIC_ERROR_CODE_RETURNED____33") + i; //$NON-NLS-1$
        }
        return s1;
    }

}
