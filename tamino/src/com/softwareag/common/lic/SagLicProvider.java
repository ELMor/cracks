// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   SagLicProvider.java

package com.softwareag.common.lic;

import java.security.Provider;

public class SagLicProvider extends Provider
{

    public static final String VERSION = Messages.getString("SagLicProvider.1.4.0.0_1"); //$NON-NLS-1$
    public static final String COPYRIGHT = Messages.getString("SagLicProvider.Copyright_Software_AG_1995-2001._All_rights_reserved._2"); //$NON-NLS-1$

    public SagLicProvider()
    {
        super(Messages.getString("SagLicProvider.MD5_3"), 1.0D, Messages.getString("SagLicProvider.Software_AG_MD5_Security_Provider_v1.0_4")); //$NON-NLS-1$ //$NON-NLS-2$
        put(Messages.getString("SagLicProvider.MessageDigest.MD5_5"), Messages.getString("SagLicProvider.com.softwareag.common.lic.SagLicMD5_6")); //$NON-NLS-1$ //$NON-NLS-2$
    }
}
