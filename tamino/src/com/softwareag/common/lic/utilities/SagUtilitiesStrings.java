// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   SagUtilitiesStrings.java

package com.softwareag.common.lic.utilities;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

public class SagUtilitiesStrings
{

    public static final String VERSION = Messages.getString("SagUtilitiesStrings.1.4.0.0_1"); //$NON-NLS-1$
    public static final String COPYRIGHT = Messages.getString("SagUtilitiesStrings.Copyright_Software_AG_1995-2001._All_rights_reserved._2"); //$NON-NLS-1$
    private static final String encodingName = Messages.getString("SagUtilitiesStrings.US-ASCII_3"); //$NON-NLS-1$

    public SagUtilitiesStrings()
    {
    }

    public static boolean isHashableChar(byte byte0)
    {
        return byte0 != 32 && byte0 != 26 && byte0 != 9 && byte0 != 13 && byte0 != 10;
    }

    public static int indexStrInStr(String s, String s1, int i, boolean flag)
        throws NullPointerException
    {
        String s2 = Messages.getString("SagUtilitiesStrings.indexStrInStr()_4"); //$NON-NLS-1$
        int j = -1;
        if(flag)
        {
            String s3 = s.toUpperCase(Locale.ENGLISH);
            String s4 = s1.toUpperCase(Locale.ENGLISH);
            j = s3.indexOf(s4, i);
        } else
        {
            j = s.indexOf(s1, i);
        }
        return j;
    }

    public static String replaceStrOnce(String s, String s1, String s2, int i, boolean flag)
        throws NullPointerException, UnsupportedEncodingException
    {
        String s3 = Messages.getString("SagUtilitiesStrings.replaceStrOnce()_5"); //$NON-NLS-1$
        try
        {
            int j = 0;
            boolean flag1 = false;
            int l2 = indexStrInStr(s, s1, i, flag);
            if(l2 < 0)
                return s;
            byte abyte0[] = s.getBytes(Messages.getString("SagUtilitiesStrings.US-ASCII_6")); //$NON-NLS-1$
            byte abyte1[] = s1.getBytes(Messages.getString("SagUtilitiesStrings.US-ASCII_7")); //$NON-NLS-1$
            byte abyte2[] = s2.getBytes(Messages.getString("SagUtilitiesStrings.US-ASCII_8")); //$NON-NLS-1$
            int k2 = abyte2.length - abyte1.length;
            if(k2 == 0)
            {
                j = l2;
                for(int k = 0; k < abyte2.length; k++)
                {
                    abyte0[j] = abyte2[k];
                    j++;
                }

                byte abyte3[] = new byte[abyte0.length];
                for(int l = 0; l < abyte0.length; l++)
                    abyte3[l] = abyte0[l];

                String s4 = new String(abyte3);
                return s4;
            }
            if(k2 < 0)
            {
                j = l2;
                for(int i1 = 0; i1 < abyte2.length; i1++)
                {
                    abyte0[j] = abyte2[i1];
                    j++;
                }

                byte abyte4[] = new byte[abyte0.length + k2];
                for(int j1 = 0; j1 < j; j1++)
                    abyte4[j1] = abyte0[j1];

                for(int k1 = j; k1 < abyte4.length; k1++)
                    abyte4[k1] = abyte0[k1 - k2];

                String s5 = new String(abyte4);
                return s5;
            }
            byte abyte5[] = new byte[abyte0.length + k2];
            for(int l1 = 0; l1 < l2; l1++)
                abyte5[l1] = abyte0[l1];

            j = l2;
            for(int i2 = 0; i2 < abyte2.length; i2++)
            {
                abyte5[j] = abyte2[i2];
                j++;
            }

            for(int j2 = abyte0.length - 1; j2 >= j - k2; j2--)
                abyte5[j2 + k2] = abyte0[j2];

            String s6 = new String(abyte5);
            return s6;
        }
        catch(NullPointerException nullpointerexception)
        {
            System.out.println(Messages.getString("SagUtilitiesStrings._n_9") + s3 + Messages.getString("SagUtilitiesStrings._-_ERROR_-_NULL_POINTER_10")); //$NON-NLS-1$ //$NON-NLS-2$
        }
        throw new NullPointerException();
    }

    public static String replaceStrInStr(String s, String s1, String s2, int i, boolean flag, boolean flag1)
        throws NullPointerException, UnsupportedEncodingException
    {
        String s3 = Messages.getString("SagUtilitiesStrings.replaceStrInStr()_11"); //$NON-NLS-1$
        try
        {
            String s4 = new String(s);
            String s5;
            do
            {
                s5 = new String(s4);
                s4 = replaceStrOnce(s5, s1, s2, i, flag);
            } while(!flag1 && indexStrInStr(s2, s1, 0, flag) < 0 && !s4.equals(s5));
            return s4;
        }
        catch(NullPointerException nullpointerexception)
        {
            System.out.println(Messages.getString("SagUtilitiesStrings._n_12") + s3 + Messages.getString("SagUtilitiesStrings._-_ERROR_-_NULL_POINTER_13")); //$NON-NLS-1$ //$NON-NLS-2$
        }
        throw new NullPointerException();
    }

    public static String restoreMarkupChars(String s)
        throws NullPointerException, UnsupportedEncodingException
    {
        String s1 = Messages.getString("SagUtilitiesStrings.restoreMarkupChars()_14"); //$NON-NLS-1$
        try
        {
            String s2 = Messages.getString("SagUtilitiesStrings.&quot;_15"); //$NON-NLS-1$
            String s3 = Messages.getString("SagUtilitiesStrings.&amp;_16"); //$NON-NLS-1$
            String s4 = Messages.getString("SagUtilitiesStrings.&lt;_17"); //$NON-NLS-1$
            String s5 = Messages.getString("SagUtilitiesStrings.&gt;_18"); //$NON-NLS-1$
            String s6 = new String(s);
            s6 = replaceStrInStr(s6, s2, Messages.getString("SagUtilitiesStrings.__19"), 0, false, false); //$NON-NLS-1$
            s6 = replaceStrInStr(s6, s3, Messages.getString("SagUtilitiesStrings.&_20"), 0, false, false); //$NON-NLS-1$
            s6 = replaceStrInStr(s6, s4, Messages.getString("SagUtilitiesStrings.<_21"), 0, false, false); //$NON-NLS-1$
            s6 = replaceStrInStr(s6, s5, Messages.getString("SagUtilitiesStrings.>_22"), 0, false, false); //$NON-NLS-1$
            return s6;
        }
        catch(NullPointerException nullpointerexception)
        {
            System.out.println(Messages.getString("SagUtilitiesStrings._n_23") + s1 + Messages.getString("SagUtilitiesStrings._-_ERROR_-_NULL_POINTER_24")); //$NON-NLS-1$ //$NON-NLS-2$
        }
        throw new NullPointerException();
    }
}
