// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   SagVer.java

package com;

import com.softwareag.common.lic.SagVersion;

public class SagVer
{

    public static final String VERSION = Messages.getString("SagVer.1.0.0.0_1"); //$NON-NLS-1$
    public static final String COPYRIGHT = Messages.getString("SagVer.Copyright_Software_AG_1995-2001._All_rights_reserved._2"); //$NON-NLS-1$

    public SagVer()
    {
    }

    public static void main(String args[])
    {
        String s = Messages.getString("SagVer.main()_3"); //$NON-NLS-1$
        String s1 = Messages.getString("SagVer._nUSAGE__n_n____java_[-classpath_<CLASSPATH>]_com.SagVer_n_4"); //$NON-NLS-1$
        try
        {
            for(int i = 0; i < args.length; i++)
            {
                System.out.println(Messages.getString("SagVer._nargs[_5") + i + Messages.getString("SagVer.]____6") + args[i]); //$NON-NLS-1$ //$NON-NLS-2$
                SagVersion sagversion = new SagVersion(Messages.getString("SagVer.Java_Package_7")); //$NON-NLS-1$
                System.out.println(Messages.getString("SagVer._n_8") + SagVersion.getVersionString()); //$NON-NLS-1$
            }

            System.exit(0);
        }
        catch(Exception exception)
        {
            System.out.println(Messages.getString("SagVer._njava_com.SagVer___Error_-__9") + exception.getMessage()); //$NON-NLS-1$
            System.out.println(s1);
        }
    }
}
