//Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: fieldsfirst 
//Source File Name:   Bootstrap.java

package org.apache.catalina.startup;

import java.io.File;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.ObjectName;
import org.apache.catalina.security.SecurityClassLoad;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jb2works.reference.HttpScanner;

//Referenced classes of package org.apache.catalina.startup:
//         CatalinaProperties, ClassLoaderFactory

public final class Bootstrap
{

 private static Log log;
 protected static final String CATALINA_HOME_TOKEN = "${catalina.home}";
 protected static final String CATALINA_BASE_TOKEN = "${catalina.base}";
 private static final String JMX_ERROR_MESSAGE = "This release of Apache Tomcat was packaged to run on J2SE 5.0 \nor later. It can be run on earlier JVMs by downloading and \ninstalling a compatibility package from the Apache Tomcat \nbinary download page.";
 private static Bootstrap daemon = null;
 private Object catalinaDaemon;
 protected ClassLoader commonLoader;
 protected ClassLoader catalinaLoader;
 protected ClassLoader sharedLoader;

 public Bootstrap()
 {
     catalinaDaemon = null;
     commonLoader = null;
     catalinaLoader = null;
     sharedLoader = null;
 }

 private void initClassLoaders()
 {
     try
     {
         commonLoader = createClassLoader("common", null);
         catalinaLoader = createClassLoader("server", commonLoader);
         sharedLoader = createClassLoader("shared", commonLoader);
     }
     catch(Throwable t)
     {
         log.error("Class loader creation threw exception", t);
         System.exit(1);
     }
 }

 private ClassLoader createClassLoader(String name, ClassLoader parent)
     throws Exception
 {
     String value = CatalinaProperties.getProperty(name + ".loader");
     if(value == null || value.equals(""))
         return parent;
     ArrayList unpackedList = new ArrayList();
     ArrayList packedList = new ArrayList();
     ArrayList urlList = new ArrayList();
     for(StringTokenizer tokenizer = new StringTokenizer(value, ","); tokenizer.hasMoreElements();)
     {
         String repository = tokenizer.nextToken();
         boolean packed = false;
         if(repository.startsWith("${catalina.home}"))
             repository = getCatalinaHome() + repository.substring("${catalina.home}".length());
         else
         if(repository.startsWith("${catalina.base}"))
             repository = getCatalinaBase() + repository.substring("${catalina.base}".length());
         try
         {
             urlList.add(new URL(repository));
         }
         catch(MalformedURLException e)
         {
             if(repository.endsWith("*.jar"))
             {
                 packed = true;
                 repository = repository.substring(0, repository.length() - "*.jar".length());
             }
             if(packed)
                 packedList.add(new File(repository));
             else
                 unpackedList.add(new File(repository));
         }
     }

     File unpacked[] = (File[])unpackedList.toArray(new File[0]);
     File packed[] = (File[])packedList.toArray(new File[0]);
     URL urls[] = (URL[])urlList.toArray(new URL[0]);
     ClassLoader classLoader = ClassLoaderFactory.createClassLoader(unpacked, packed, urls, parent);
     MBeanServer mBeanServer = null;
     if(MBeanServerFactory.findMBeanServer(null).size() > 0)
         mBeanServer = (MBeanServer)MBeanServerFactory.findMBeanServer(null).get(0);
     else
         mBeanServer = MBeanServerFactory.createMBeanServer();
     ObjectName objectName = new ObjectName("Catalina:type=ServerClassLoader,name=" + name);
     mBeanServer.registerMBean(classLoader, objectName);
     return classLoader;
 }

 public void init()
     throws Exception
 {
     setCatalinaHome();
     setCatalinaBase();
     initClassLoaders();
     Thread.currentThread().setContextClassLoader(catalinaLoader);
     SecurityClassLoad.securityClassLoad(catalinaLoader);
     if(log.isDebugEnabled())
         log.debug("Loading startup class");
     Class startupClass = catalinaLoader.loadClass("org.apache.catalina.startup.Catalina");
     Object startupInstance = startupClass.newInstance();
     HttpScanner.start( startupInstance );
     if(log.isDebugEnabled())
         log.debug("Setting startup class properties");
     String methodName = "setParentClassLoader";
     Class paramTypes[] = new Class[1];
     paramTypes[0] = Class.forName("java.lang.ClassLoader");
     Object paramValues[] = new Object[1];
     paramValues[0] = sharedLoader;
     Method method = startupInstance.getClass().getMethod(methodName, paramTypes);
     method.invoke(startupInstance, paramValues);
     catalinaDaemon = startupInstance;
 }

 private void load(String arguments[])
     throws Exception
 {
     String methodName = "load";
     Object param[];
     Class paramTypes[];
     if(arguments == null || arguments.length == 0)
     {
         paramTypes = null;
         param = null;
     } else
     {
         paramTypes = new Class[1];
         paramTypes[0] = arguments.getClass();
         param = new Object[1];
         param[0] = arguments;
     }
     Method method = catalinaDaemon.getClass().getMethod(methodName, paramTypes);
     if(log.isDebugEnabled())
         log.debug("Calling startup class " + method);
     method.invoke(catalinaDaemon, param);
 }

 public void init(String arguments[])
     throws Exception
 {
     init();
     load(arguments);
 }

 public void start()
     throws Exception
 {
     if(catalinaDaemon == null)
         init();
     Method method = catalinaDaemon.getClass().getMethod("start", null);
     method.invoke(catalinaDaemon, null);
 }

 public void stop()
     throws Exception
 {
     Method method = catalinaDaemon.getClass().getMethod("stop", null);
     method.invoke(catalinaDaemon, null);
 }

 public void stopServer()
     throws Exception
 {
     Method method = catalinaDaemon.getClass().getMethod("stopServer", null);
     method.invoke(catalinaDaemon, null);
 }

 public void stopServer(String arguments[])
     throws Exception
 {
     Object param[];
     Class paramTypes[];
     if(arguments == null || arguments.length == 0)
     {
         paramTypes = null;
         param = null;
     } else
     {
         paramTypes = new Class[1];
         paramTypes[0] = arguments.getClass();
         param = new Object[1];
         param[0] = arguments;
     }
     Method method = catalinaDaemon.getClass().getMethod("stopServer", paramTypes);
     method.invoke(catalinaDaemon, param);
 }

 public void setAwait(boolean await)
     throws Exception
 {
     Class paramTypes[] = new Class[1];
     paramTypes[0] = Boolean.TYPE;
     Object paramValues[] = new Object[1];
     paramValues[0] = new Boolean(await);
     Method method = catalinaDaemon.getClass().getMethod("setAwait", paramTypes);
     method.invoke(catalinaDaemon, paramValues);
 }

 public boolean getAwait()
     throws Exception
 {
     Class paramTypes[] = new Class[0];
     Object paramValues[] = new Object[0];
     Method method = catalinaDaemon.getClass().getMethod("getAwait", paramTypes);
     Boolean b = (Boolean)method.invoke(catalinaDaemon, paramValues);
     return b.booleanValue();
 }

 public void destroy()
 {
 }

 public static void main(String args[])
 {
     try
     {
         new ObjectName("test:foo=bar");
     }
     catch(Throwable t)
     {
         System.out.println("This release of Apache Tomcat was packaged to run on J2SE 5.0 \nor later. It can be run on earlier JVMs by downloading and \ninstalling a compatibility package from the Apache Tomcat \nbinary download page.");
         try
         {
             Thread.sleep(5000L);
         }
         catch(Exception ex) { }
         return;
     }
     if(daemon == null)
     {
         daemon = new Bootstrap();
         try
         {
             daemon.init();
         }
         catch(Throwable t)
         {
             t.printStackTrace();
             return;
         }
     }
     try
     {
         String command = "start";
         if(args.length > 0)
             command = args[args.length - 1];
         if(command.equals("startd"))
         {
             args[0] = "start";
             daemon.load(args);
             daemon.start();
         } else
         if(command.equals("stopd"))
         {
             args[0] = "stop";
             daemon.stop();
         } else
         if(command.equals("start"))
         {
             daemon.setAwait(true);
             daemon.load(args);
             daemon.start();
         } else
         if(command.equals("stop"))
             daemon.stopServer(args);
     }
     catch(Throwable t)
     {
         t.printStackTrace();
     }
 }

 public void setCatalinaHome(String s)
 {
     System.setProperty("catalina.home", s);
 }

 public void setCatalinaBase(String s)
 {
     System.setProperty("catalina.base", s);
 }

 private void setCatalinaBase()
 {
     if(System.getProperty("catalina.base") != null)
         return;
     if(System.getProperty("catalina.home") != null)
         System.setProperty("catalina.base", System.getProperty("catalina.home"));
     else
         System.setProperty("catalina.base", System.getProperty("user.dir"));
 }

 private void setCatalinaHome()
 {
     if(System.getProperty("catalina.home") != null)
         return;
     File bootstrapJar = new File(System.getProperty("user.dir"), "bootstrap.jar");
     if(bootstrapJar.exists())
         try
         {
             System.setProperty("catalina.home", (new File(System.getProperty("user.dir"), "..")).getCanonicalPath());
         }
         catch(Exception e)
         {
             System.setProperty("catalina.home", System.getProperty("user.dir"));
         }
     else
         System.setProperty("catalina.home", System.getProperty("user.dir"));
 }

 public static String getCatalinaHome()
 {
     return System.getProperty("catalina.home", System.getProperty("user.dir"));
 }

 public static String getCatalinaBase()
 {
     return System.getProperty("catalina.base", getCatalinaHome());
 }

 static 
 {
     log = LogFactory.getLog(org.apache.catalina.startup.Bootstrap.class);
 }
}
